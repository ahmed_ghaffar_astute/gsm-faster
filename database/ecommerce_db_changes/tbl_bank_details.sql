-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2020 at 02:28 PM
-- Server version: 5.6.37
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gf_tbl_bank_details`
--

CREATE TABLE IF NOT EXISTS `tbl_gf_tbl_bank_details` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `bank_holder_name` varchar(150) NOT NULL,
  `bank_holder_phone` varchar(20) NOT NULL,
  `bank_holder_email` varchar(150) NOT NULL,
  `account_no` varchar(100) NOT NULL,
  `account_type` varchar(50) NOT NULL,
  `bank_ifsc` varchar(20) NOT NULL,
  `bank_name` varchar(150) NOT NULL,
  `is_default` int(1) NOT NULL DEFAULT '0',
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_gf_tbl_bank_details`
--
ALTER TABLE `tbl_gf_tbl_bank_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_gf_tbl_bank_details`
--
ALTER TABLE `tbl_gf_tbl_bank_details`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
