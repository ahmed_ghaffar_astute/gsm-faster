-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 28, 2020 at 07:51 PM
-- Server version: 5.6.37
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gf_tbl_offers`
--

CREATE TABLE IF NOT EXISTS `tbl_gf_tbl_offers` (
  `id` int(10) NOT NULL,
  `offer_title` varchar(150) NOT NULL,
  `offer_slug` varchar(150) NOT NULL,
  `offer_desc` longtext NOT NULL,
  `offer_percentage` int(10) NOT NULL,
  `offer_image` text NOT NULL,
  `created_at` varchar(150) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_gf_tbl_offers`
--

INSERT INTO `tbl_gf_tbl_offers` (`id`, `offer_title`, `offer_slug`, `offer_desc`, `offer_percentage`, `offer_image`, `created_at`, `status`) VALUES
(1, 'MEGA SALE !', 'mega-sale', '<p>Mega Sale on Clothes for Mens and Womens</p>\r\n\r\n<p><strong>50% OFF </strong>on clothes.</p>\r\n', 50, '26102019112146_26747.png', '1569648061', 1),
(2, 'Our Biggest Weekend Sale', 'our-biggest-weekend-sale', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n', 40, '26102019112945_52597.png', '1570163984', 1),
(3, 'Grand Offer for 50% Off', 'grand-offer-for-50-off', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n', 50, '11012020101654_16931.png', '1572069709', 1),
(4, 'Your Day Just Got Better', 'your-day-just-got-better', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 5, '11012020101952_34074.png', '1578718192', 1),
(5, 'Flat 20% OFF Super Sale (Limited Time Offer)', 'flat-20-off-super-sale-limited-time-offer', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 20, '11012020102342_61736.png', '1578718422', 1),
(6, 'Sale 30% OFF', 'sale-30-off', '', 30, '11012020110932_78016.png', '1578721172', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_gf_tbl_offers`
--
ALTER TABLE `tbl_gf_tbl_offers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_gf_tbl_offers`
--
ALTER TABLE `tbl_gf_tbl_offers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;

ALTER TABLE `tbl_gf_tbl_product` ADD `deleted` INT(11) NOT NULL DEFAULT '0' AFTER `status`;
