<?
include "template$THEME/header.php";
$authToken = $purifier->purify(check_input($_REQUEST['auth_token'], $objDBCD14->dbh));
?>
<div role="main" class="main">
    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Easy Paisa</h1>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-md-12" align="center">
                <img src="<? echo $BASE_URL?>images/payment/loading-img.gif" style="margin-top: 40px">
                <form action="https://easypay.easypaisa.com.pk/easypay/Confirm.jsf " method="POST" id="easyPayAuthForm">                
                    <input name="auth_token" value="<?php echo $authToken; ?>" hidden = "true"/>
                    <!--<input name="postBackURL" value="<? echo $BASE_URL?>ezpaisa-ipn.php" hidden = "true"/>-->
                    <input name="postBackURL" value="<? echo $BASE_URL?>order_placed.php" hidden = "true"/>
                    <input value="confirm" type = "submit" name= "pay" hidden = "true"/> 
                </form>
            </div>
        </div>
    </div>
</div>
<? include "template$THEME/footer.php"; ?>
<script>
(function() {
  document.getElementById("easyPayAuthForm").submit();
})();
</script>