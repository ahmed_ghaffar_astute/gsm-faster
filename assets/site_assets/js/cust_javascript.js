// Doument ready stage
$(window).load(function() {
    // Animate loader off screen
    $(".se-pre-con").fadeOut("slow");
});


$(document).ready(function(event) {

   // var base_url = Settings.base_url;
    var currency_code = '$';//Settings.currency_code;

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    // for checkout page

    $("input[name='payment_method']").click(function(e) {
        var _val = $(this).val();
        $(".payment_method .pay-box").hide();
        $(this).parent(".payment_method").find(".pay-box").slideDown("100");
    });

    $(".btn_wishlist").click(function(e) {
        e.preventDefault();
        var btn = $(this);
        var _id = $(this).data("id");

        href = base_url + 'site/wishlist_action';

        $.ajax({
            url: href,
            data: {
                "product_id": _id
            },
            type: 'post',
            success: function(res) {
                if ($.trim(res) == 'login_now') {
                    window.location.href = base_url + "login-register";
                } else {
                    var obj = $.parseJSON(res);

                    if (obj.is_favorite) {
                        btn.css("background-color", "#ff5252");
                        btn.attr('data-original-title', 'Remove to Wishlist');

                        $('.notifyjs-corner').empty();
                        $.notify(
                            "Product is added to you wishlist...", {
                                position: "top right",
                                className: 'success'
                            }
                        );

                    } else {
                        btn.css("background-color", "#363F4D");
                        btn.attr('data-original-title', 'Add to Wishlist');
                        $('.notifyjs-corner').empty();
                        $.notify(
                            "Product is removed to you wishlist...", {
                                position: "top right",
                                className: 'success'
                            }
                        );
                    }
                }
            },
            error: function(res) {
                alert("error");
            }

        });
    });

    // add to cart
    $(".btn_cart").click(function(e) {
        e.preventDefault();
        var btn = $(this);
        var _id = $(this).data("id");
        var _maxunit = $(this).data("maxunit");

        href = base_url + 'site/cart_action';

        $.ajax({
            url: href,
            data: {
                "product_id": _id
            },
            type: 'post',
            success: function(res) {
                if ($.trim(res) == 'login_now') {
                    window.location.href = base_url + "login-register";
                } else {
                    var obj = $.parseJSON(res);

                    $("#cartModal .modal-body").html(obj.html_code);
                    $("#cartModal").modal("show");

                    $('.radio-group .radio_btn').click(function() {
                        $(this).parent().find('.radio_btn').removeClass('selected');
                        $(this).addClass('selected');
                        var val = $(this).attr('data-value');
                        $(this).parent().find('input').val(val);
                    });

                    $("input[name='product_qty']").blur(function() {
                        if ($(this).val() <= 0) {
                            $(this).val(1);
                        } else if ($(this).val() > _maxunit) {
                            alert("You cannot buy more than " + _maxunit + " items in single order !!!");
                            $(this).val(_maxunit);
                        }
                    });

                    $("#cartForm").submit(function(event) {
                        event.preventDefault();
                        $(".process_loader").show();

                        var formData = $(this).serialize();
                        var _form = $(this);

                        $.ajax({
                                type: 'POST',
                                url: $(this).attr('action'),
                                data: formData
                            })
                            .done(function(response) {

                                var res = $.parseJSON(response);
                                $(".process_loader").hide();

                                if (res.success == '1') {

                                    $("#cartModal").modal("hide");
                                    swal({
                                        title: "Done!",
                                        text: res.msg,
                                        type: "success"
                                    }, function() {
                                        location.reload();
                                    });
                                } else {

                                }

                            })
                            .fail(function(response) {
                                $(".process_loader").hide();
                                swal("Something gone wrong!", res);
                            });

                    });

                }
            },
            error: function(res) {
                alert("error");
            }
        });
    });

    $(".btn_remove_cart").click(function(e) {
        e.preventDefault();
        var href = $(this).attr("href");

        swal({
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger btn_edit",
                cancelButtonClass: "btn-warning btn_edit",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false,
                showLoaderOnConfirm: true
            },
            function(isConfirm) {
                if (isConfirm) {

                    $.ajax({
                            type: 'POST',
                            url: href
                        })
                        .done(function(response) {
                            var res = $.parseJSON(response);
                            $(".process_loader").hide();

                            if (res.success == '1') {
                                swal({
                                    title: "Done!",
                                    text: res.msg,
                                    type: "success"
                                }, function() {
                                    location.reload();
                                });
                            } else {
                                $(".process_loader").hide();
                                swal("Something gone wrong!", res);
                            }

                        })
                        .fail(function(response) {
                            $(".process_loader").hide();
                            swal("Something gone wrong!", res);
                        });

                } else {
                    swal.close();
                }
            });

    });

    // to open address form

    $(".address_radio").click(function(e) {
        $("input[name='order_address']").val($(this).val());
        $(".ceckout-form").hide();
        $(".bank_form").hide();
    });


    $(".btn_new_address").click(function(e) {
        e.preventDefault();
        $(".ceckout-form").toggle();
        $(".bank_form").toggle();
    });

    $(".btn_new_account").click(function(e) {
        e.preventDefault();
        $(".bank_form_new").toggle();
        $(".bank_form").toggle();
    });

    // remove bank account
    $(".btn_remove_bank").click(function(e) {
        e.preventDefault();

        var _id = $(this).data("id");

        swal({
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger btn_edit",
                cancelButtonClass: "btn-warning btn_edit",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false,
                showLoaderOnConfirm: true
            },
            function(isConfirm) {
                if (isConfirm) {

                    href = base_url + 'site/remove_bank_account';

                    $.ajax({
                        type: 'POST',
                        url: href,
                        data: {
                            bank_id: _id
                        },
                        success: function(res) {
                            var obj = $.parseJSON(res);
                            if (obj.success == '1') {
                                swal({
                                    title: "Deleted!",
                                    text: obj.message,
                                    type: "success"
                                }, function() {
                                    location.reload();
                                });
                            } else {
                                swal("Something gone wrong!", obj.msg);
                            }
                        }
                    });
                } else {
                    swal.close();
                }

            });

    });

    // for contact form
    $("#contact_form").submit(function(e) {
        e.preventDefault();

        $(".process_loader").show();

        $('.btn_send').attr("disabled", true);

        var formData = $(this).serialize();

        var _form = $(this);

        $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: formData
            })
            .done(function(response) {

                var res = $.parseJSON(response);

                $('.notifyjs-corner').empty();

                $(".process_loader").hide();

                if (res.success == '1') {

                    _form.find("input").val('');
                    _form.find("textarea").val('');

                    _form.find("select").val('');

                    swal({
                        title: "Done!",
                        text: res.msg,
                        type: "success"
                    }, function() {
                        location.reload();
                    });
                } else {

                    swal("Something gone wrong!", res.msg);
                    $('.btn_send').attr("disabled", false);
                }

            })
            .fail(function(data) {

            });

    });

    // remove review
    $(".btn_remove_review").click(function(e) {
        e.preventDefault();

        var _id = $(this).data("id");

        swal({
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger btn_edit",
                cancelButtonClass: "btn-warning btn_edit",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false,
                showLoaderOnConfirm: true
            },
            function(isConfirm) {
                if (isConfirm) {

                    href = base_url + 'site/remove_review';

                    $.ajax({
                        type: 'POST',
                        url: href,
                        data: {
                            review_id: _id
                        },
                        success: function(res) {
                            var obj = $.parseJSON(res);
                            if (obj.success == '1') {
                                swal({
                                    title: "Deleted!",
                                    text: obj.message,
                                    type: "success"
                                }, function() {
                                    location.reload();
                                });
                            } else {
                                swal("Something gone wrong!", obj.message);
                            }
                        }
                    });
                } else {
                    swal.close();
                }
            });
    });

    $(".btn_cancel_form").click(function(e) {
        e.preventDefault();
        $(".bank_form").hide();
    });



    // to close address form
    $(".close_form").click(function(e) {
        e.preventDefault();
        $('form[name="address_form"]')[0].reset();
        $(".add_addresss_block").hide();
    });

    // get data by pincode
    $("input[name='pincode']").blur(function(e) {
        if ($(this).val() != '') {
            var href;
            var _pincode = $(this).val();

            href = base_url + 'site/get_pincode_data';

            $(".process_loader").show();

            $.ajax({
                type: 'POST',
                url: href,
                data: {
                    "pincode": _pincode
                },
                success: function(res) {
                    var obj = $.parseJSON(res);
                    $(".process_loader").hide();
                    if (obj.status == '1') {
                        $("input[name='city']").val(obj.city);
                        $("input[name='district']").val(obj.district);
                        $('#state option[value="' + obj.state + '"]').prop('selected', true);
                    }
                }
            });
        } else {
            $("input[name='city']").val('');
            $("input[name='district']").val('');
            $('#state option[value="0"]').prop('selected', true);
        }
    });

    $(".btn_delete_address").click(function(e) {

        e.preventDefault();

        var _id = $(this).data("id");

        swal({
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger btn_edit",
                cancelButtonClass: "btn-warning btn_edit",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false,
                showLoaderOnConfirm: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    href = base_url + 'delete_address/' + _id;
                    window.location.href = href;
                } else {
                    swal.close();
                }
            });

    });

    // for rating
    $('#stars li').on('mouseover', function() {
        var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

        // Now highlight all the stars that's not after the current hovered star
        $(this).parent().children('li.star').each(function(e) {
            if (e < onStar) {
                $(this).addClass('hover');
            } else {
                $(this).removeClass('hover');
            }
        });

    }).on('mouseout', function() {
        $(this).parent().children('li.star').each(function(e) {
            $(this).removeClass('hover');
        });
    });


    /* 2. Action to perform on click */
    $('#stars li').on('click', function() {

        $(".inp_rating").val(parseInt($(this).data('value'), 10));

        var onStar = parseInt($(this).data('value'), 10); // The star currently selected
        var stars = $(this).parent().children('li.star');

        for (i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass('selected');
        }

        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass('selected');
        }

    });
    // end

    // for coupon code

    function apply_coupon(_coupon_code, _cart_amt, _delivery, href) {
        $.ajax({
            url: href,
            data: {
                "coupon_code": _coupon_code,
                "cart_amt": _cart_amt,
                "delivery": _delivery
            },
            type: 'post',
            success: function(res) {

                $(".code_err").hide();

                var obj = $.parseJSON(res);

                if (obj.success == '1') {
                    $("#coupons_detail").modal("hide");
                    $('.notifyjs-corner').empty();
                    $.notify(
                        obj.msg, {
                            position: "top right",
                            className: 'success'
                        }
                    );

                    $(".discount").find("span").html(currency_code + ' ' + obj.discount_amt);

                    $(".discount").show();

                    $(".order-total").find("span").html(currency_code + ' ' + obj.payable_amt);

                    $(".apply_msg").show();

                    $(".msg_2").html(obj.you_save_msg);

                    $("input[name='coupon_id']").val(obj.coupon_id);
                    $("input[name='discount']").val(obj.discount);
                    $("input[name='discount_amt']").val(obj.discount_amt);
                    $("input[name='payable_amt']").val(obj.payable_amt);

                    $(".apply_button").hide();
                    $(".remove_coupon").show();

                } else {

                    $(".code_err").html('<i class="fa fa-exclamation-circle"></i> ' + obj.msg).slideDown();
                }

            },
            error: function(res) {
                alert("error");
            }

        });
    }

    $(".btn_apply_coupon").click(function(e) {

        e.preventDefault();

        href = base_url + 'site/apply_coupon';

        var _coupon_code = $(this).data("coupon");
        var _cart_amt = $(this).data("cartamt");
        var _delivery = $(this).data("delivery");
        apply_coupon(_coupon_code, _cart_amt, _delivery, href);

    });

    // for remove coupon code

    $(".remove_coupon a").click(function(e) {
        e.preventDefault();

        if (confirm("Are you sure to remove?")) {

            $('.notifyjs-corner').empty();
            $.notify(
                'Coupon code is removed...', {
                    position: "top right",
                    className: 'success'
                }
            );

            $(".apply_button").show();
            $(".remove_coupon").hide();

            $(".apply_msg").hide();

            $("input[name='coupon_id']").val('0');
            $("input[name='discount']").val('0');
            $("input[name='discount_amt']").val('0');
            $("input[name='payable_amt']").val((parseFloat($("input[name='total_amt']").val()) + parseFloat($("input[name='delivery_charge']").val())));

            $(".order-total").find("span").html(currency_code + ' ' + (parseFloat($("input[name='total_amt']").val()) + parseFloat($("input[name='delivery_charge']").val())));
        }

    });


    // place order
    $(".btn_place_order").on("click", function(e) {
        e.preventDefault();

        var _count = 0;

        var _formData = $("form[name='place_order']").serializeArray();

        if (_formData[1]['value'] != '0') {

            var btn = $(this)

            var _payment_method = _formData[8]['value'];

            if (_payment_method == 'cod') {

                btn.attr("disabled", true);

                if ($(".input_txt").val() != '') {
                    btn.attr("disabled", false);
                    _count = 0;
                    $(".input_txt").css("border-color", "#ccc");
                    var _sum = parseInt($("._lblnum1").text()) + parseInt($("._lblnum2").text());
                    if (parseInt($(".input_txt").val()) != _sum) {
                        swal("Enter correct value !!!");

                        var x = Math.floor((Math.random() * 10) + 1);
                        var y = Math.floor((Math.random() * 10) + 1);

                        $("._lblnum1").text(x);
                        $("._lblnum2").text(y);
                        $(".input_txt").val('');
                        _count++;
                    }
                } else {
                    _count++;
                    $(".input_txt").css("border-color", "red");
                    btn.attr("disabled", false);
                }

                if (_count == 0) {
                    $(".process_loader").show();

                    href = base_url + 'site/place_order';

                    $.ajax({
                        type: 'POST',
                        url: href,
                        data: $("form[name='place_order']").serialize(),
                        success: function(res) {

                            btn.attr("disabled", false);
                            var obj = $.parseJSON(res);

                            $(".process_loader").hide();

                            if (obj.success == '1') {


                                $("#orderConfirm .ord_no_lbl").text(obj.order_unique_id);

                                $("#orderConfirm .btn_track").click(function(e) {
                                    window.location.href = base_url + 'my-orders/' + obj.order_unique_id;
                                });

                                $("#orderConfirm").fadeIn();
                            } else if (obj.success == '-1') {
                                swal({
                                    title: "Opps!",
                                    text: obj.msg,
                                    type: "error"
                                }, function() {
                                    location.reload();
                                });
                            } else {
                                $(".process_loader").hide();
                                swal("Something gone wrong!", res);
                            }

                        }
                    });
                }
            } else if (_payment_method == 'paypal') {
                $(this).attr("disabled", true);
                $(".process_loader").show();

                $(".input_txt").css("border-color", "#ccc");
                href = base_url + 'paypal/create_payment_with_paypal';
                $("form[name='place_order']").attr("action", href).submit();
            } else if (_payment_method == 'stripe') {
                btn.attr("disabled", false);
                $("#stripeModal").modal("show");
            }
        } else {
            swal("Please create or select your shipping address..");
        }

    });

    // download invoice button
    $(".btn_download").click(function(e) {
        e.preventDefault();
        var _id = $(this).data("id");

        href = base_url + 'download-invoice/' + _id;

        window.open(href);
    });


    // for order progress timeline
    $(".dot").click(function(e) {

        if ($(this).data("status") != 'disable_dot') {
            $(this).addClass('active_dot').siblings().removeClass('active_dot');
            $(this).nextAll().addClass('deactive_dot');

            $(this).removeClass('deactive_dot');
            $(this).prevAll().removeClass('deactive_dot');
            $(this).prevAll().addClass('active_dot');
            $(this).addClass('active_dot');
        }
    });

    // for cancel order
    $('#orderCancel, #claimRefund').on('hidden.bs.modal', function() {
        $("body").css("overflow-y", "auto");
        $(".bank_form").hide();
        $(".bank_details").hide();
        $(".msg_holder").html('');
        $("textarea[name='reason']").css("border-color", "#ccc");
        $("textarea").val('');
    });

    $(".product_cancel").click(function(e) {

        e.preventDefault();

        if ($(this).data("gateway") != 'cod') {
            $(".bank_details").show();
        } else {
            $(".bank_details").hide();
        }

        var _title = 'Are you sure to cancel selected product order ?';

        if ($(this).data("product") == '0') {
            _title = 'Are you sure to cancel selected order ?';
        }

        $("#orderCancel .cancelTitle").text(_title);
        $("#orderCancel .order_unique_id").text($(this).data("unique"));
        $("#orderCancel").modal("show");

        $("body").css("overflow-y", "hidden");

        var order_id = $(this).data("order");
        var product_id = $(this).data("product");

        $("#orderCancel input[name='order_id']").val(order_id);
        $("#orderCancel input[name='product_id']").val(product_id);

        $("#orderCancel input[name='gateway']").val($(this).data("gateway"));

    });


    $(".cancel_order").click(function(e) {
        e.preventDefault();
        var _btn = $(this);

        _btn.attr("disabled", true);

        _btn.text("Please wait...");

        var _reason = $("textarea[name='reason']").val();

        var _bank_id = $(this).parents("#orderCancel").find("input[name='bank_acc_id']:checked").val();

        var flag = false;
        $('.notifyjs-corner').empty();

        if (_reason == '') {
            $("textarea[name='reason']").css("border-color", "red");
            flag = true;

            $.notify(
                "Please enter reason for cancellation", {
                    position: "top right",
                    className: 'danger'
                }
            );
        }

        if ((_bank_id == '' || typeof _bank_id == "undefined") && $(this).parents("#orderCancel").find("input[name='gateway']").val() != 'cod') {
            flag = true;

            $.notify(
                "Please select bank for refund", {
                    position: "top right",
                    className: 'danger'
                }
            );
        }

        if (flag) {
            _btn.attr("disabled", false);
            _btn.text("Cancel Order");
        }

        if (!flag) {

            var order_id = $(this).parents("#orderCancel").find("input[name='order_id']").val();
            var product_id = $(this).parents("#orderCancel").find("input[name='product_id']").val();


            var href = base_url + 'site/cancel_product';


            $.ajax({
                type: 'POST',
                url: href,
                data: {
                    "order_id": order_id,
                    "product_id": product_id,
                    'reason': _reason,
                    'bank_id': _bank_id
                },
                success: function(res) {

                    var obj = $.parseJSON(res);

                    _btn.attr("disabled", false);
                    _btn.text("Cancel Order");

                    if (obj.success == 1) {

                        $("#orderCancel").modal("hide");
                        swal({
                            title: "Cancelled",
                            text: obj.msg,
                            type: "success"
                        }, function() {
                            location.reload();
                        });
                    }
                }
            });
        }
    });


    $(".btn_claim").click(function(e) {
        e.preventDefault();

        if ($(this).data("gateway") != 'cod') {
            $(".bank_details").show();
        } else {
            $(".bank_details").hide();
        }

        $("#claimRefund").modal("show");

        var order_id = $(this).data("order");
        var product_id = $(this).data("product");

        $("#claimRefund input[name='order_id']").val(order_id);
        $("#claimRefund input[name='product_id']").val(product_id);

        $("body").css("overflow-y", "hidden");

    });

    $(".claim_refund").click(function(e) {
        e.preventDefault();

        var _btn = $(this);

        _btn.attr("disabled", true);

        _btn.text("Please wait...");

        var _bank_id = $(this).parents("#claimRefund").find("input[name='bank_acc_id']:checked").val();

        var flag = false;

        $('.notifyjs-corner').empty();

        if (!flag) {

            var order_id = $(this).parents("#claimRefund").find("input[name='order_id']").val();
            var product_id = $(this).parents("#claimRefund").find("input[name='product_id']").val();

            var href = base_url + 'site/claim_refund';


            $.ajax({
                type: 'POST',
                url: href,
                data: {
                    "order_id": order_id,
                    "product_id": product_id,
                    'bank_id': _bank_id
                },
                success: function(res) {

                    var obj = $.parseJSON(res);

                    _btn.attr("disabled", false);
                    _btn.text("Claim Refund");

                    if (obj.success == 1) {

                        $("#claimRefund").modal("hide");
                        swal({
                            title: "Done !",
                            text: obj.msg,
                            type: "success"
                        }, function() {
                            location.reload();
                        });
                    }
                }
            });
        }
    });


    // for quick view

    $(".btn_quick_view").click(function(e) {

        e.preventDefault();

        var btn = $(this);
        var _id = $(this).data("id");

        href = base_url + 'site/quick_view';

        $.ajax({
            url: href,
            data: {
                "product_id": _id
            },
            type: 'post',
            success: function(res) {

                var obj = $.parseJSON(res);

                $("#productQuickView .modal-body").html(obj.html_code);
                $("#productQuickView").modal("show");

                $('.modal-tab-menu-active').slick({
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    arrows: false,
                    dots: true,
                    loop: true
                });


                $('.modal').on('shown.bs.modal', function(e) {
                    $('.modal-tab-menu-active').resize();
                });

                $(".img_click > a").click(function(e) {
                    var _id = $(this).attr("href");

                    $("#productQuickView").find(".tab-pane").removeClass("active");
                    $("#productQuickView").find(".tab-pane").removeClass("in");

                    $("#productQuickView").find(_id).addClass("active");
                    $("#productQuickView").find(_id).addClass("in");

                });

            }
        });

    });


    // for product page

    $(".brand_sort").click(function(e) {
        $("#brand_sort").submit();
    });


    $(".list_order").change(function(e) {
        $("#sort_filter_form").submit();
    });

    $('.shop-tab a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        localStorage.setItem('activeTabProduct', $(e.target).attr('href'));
    });

    var activeTabProduct = localStorage.getItem('activeTabProduct');

    if (activeTabProduct) {
        $('.shop-tab a[href="' + activeTabProduct + '"]').tab('show');
        $(".shop-product-area .tab-content").find("div").removeClass("active");
        $(".shop-product-area .tab-content").find(activeTabProduct).addClass('active');
    }


    $("#login_form input").blur(function(e) {
        if ($(this).val() != '') {
            $(this).next("p").fadeOut();
        } else {
            $(this).next("p").fadeIn();
        }
    });

    function removeURLParameter(url, parameter) {
        //prefer to use l.search if you have a location/link object
        var urlparts = url.split('?');
        if (urlparts.length >= 2) {

            var prefix = encodeURIComponent(parameter) + '=';
            var pars = urlparts[1].split(/[&;]/g);

            //reverse iteration as may be destructive
            for (var i = pars.length; i-- > 0;) {
                //idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                    pars.splice(i, 1);
                }
            }

            url = urlparts[0] + '?' + pars.join('&');
            return url;
        } else {
            return url;
        }
    }


    $(".remove_filter").click(function(e) {
        e.preventDefault();

        var uri = window.location.toString();

        var action = $(this).data("action");

        var url = '';

        if (action == 'sort') {
            url = removeURLParameter(uri, 'sort');
            window.location.href = url;
        } else if (action == 'price') {
            url = removeURLParameter(uri, 'price_filter');
            window.location.href = url;
        } else if (action == 'brands') {
            id = $(this).data("id");
            $('.brand_sort[value=' + id + ']').prop('checked', false);

            $("#brand_sort").submit();

        }

    });
});
