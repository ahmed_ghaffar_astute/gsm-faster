$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtVal").val()=='')
			errors = "-Please enter Value<br />";
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#btnSave").click(function(){
		if(validate())
		{			
			$('#dvLoader').ajaxStart(function() {
				$(this).show();
			}).ajaxStop(function() {
				$(this).hide();
			});
			var disable = 0;
			if(document.getElementById('chkDisable').checked)
				disable = 1;
			$.post(baseurl+"admin/settings/ajxregfieldval",
			   {
				   val:$('#txtVal').val(), 
				   disable:disable,
				   purpose:'save',
				   fId:$('#fId').val(), 
				   id:$('#id').val()
			   },
				function(response){
					if(response)
					{
						arrResponse = response.split('~');
						if(arrResponse[1] == 1)
						{
						   $('#txtVal').val('');
						}
						showStickyErrorToast(arrResponse[0], '', 'success');
					}
				});	
		}
	});
});
