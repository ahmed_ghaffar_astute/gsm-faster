$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtQuestion").val()=='')
		{
			errors += "- Please enter 'Question'<br />";
			$("#txtQuestion").focus();
		}
		if ($("#txtAnswer").val()=='')
		{
			errors += "- Please enter 'Answer'<br />";
			$("#txtAnswer").focus();
		}
		if (errors=='')
			return true;
		else
		{
			$("#errors_div").html("<b>Errors:</b><br />"+errors);
			$("#errors_div").fadeIn("slow");
			return false;
		}
	}
	$("#frm").submit(function(){
		return validate();
	});
});
