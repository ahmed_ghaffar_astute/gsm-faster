$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtCurrency").val() == '')
			errors += "- "+BE_35+"<br />";
		if ($("#txtAbb").val() == '')
			errors += "- "+BE_70+"<br />";
		if ($("#txtSymbol").val() == '')
			errors += "- "+BE_36+"<br />";
		if ($("#txtRate").val() == '')
			errors += "- "+BE_71+"<br />";
		if (!isFloat($("#txtRate").val()))
			errors += "- "+BE_72;
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#btnSave").click(function(){
		if(validate())
		{			
			$('#statusLoader').ajaxStart(function() {
				$(this).show();
			}).ajaxStop(function() {
				$(this).hide();
			});
			var defaultCur = 0;
			var retailCurr = 0;
			var disable = 0;
			if(document.getElementById('defaultCur').checked)
				defaultCur = 1;
			if(document.getElementById('chkRetailCurr').checked)
				retailCurr = 1;
			if(document.getElementById('disable').checked)
				disable = 1;
			$.post("ajxcurrency",
			   {
				   currency:$('#txtCurrency').val(), 
				   abb:$('#txtAbb').val(), 
				   symbol:$('#txtSymbol').val(), 
				   rate:$('#txtRate').val(), 
				   defaultCur:defaultCur, 
				   retailCurr:retailCurr, 
				   disable:disable, 
				   purpose:'save',
				   id:$('#id').val()
			   },
				function(response){
					if(response)
					{
						arrResponse = response.split('~');
						showStickyErrorToast(arrResponse[0], '', 'success');
					}
				});	
		}
	});
	$("#btnRefreshPrices").click(function(){
		$('#statusLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		$.post("ajxcurrency",
		   {
			   purpose:'refreshprices',
			   id:$('#id').val()
		   },
			function(response){
				if(response)
				{
					showStickyErrorToast(response, '', 'success');
				}
			});	
	});
});
