$(document).ready(function()
{
	$("#btnSaveG").click(function(){
		updateRecords(1);
	});
	$("#btnSaveR").click(function(){
		updateRecords(0);
	});
	function updateRecords(i)
	{
		$('#statusLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		$.post(base_url+"admin/services/ajxsmsintegration", 
		   {
			   purpose:'sms',
			   val:i,
			   type:$("#type").val()
		   },
			function(response){
				if(response)
				{
					showStickyErrorToast(response, '', 'success');
				}
			});			
	}
});