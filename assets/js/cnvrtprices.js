$(document).ready(function()
{
	$("#btnSave").click(function(){
		$('#statusLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		var baseurl=document.getElementById("baseurl").innerHTML;

		$.post(baseurl+"admin/settings/cnvrtprices",
		   {
			   currencyId:$('#currencyId').val(), 
			   purpose:'save',
			   id:$('#id').val()
		   },
			function(response){
				if(response)
				{
					showStickyErrorToast(response, '', 'success');
				}
			});	
	});
});
