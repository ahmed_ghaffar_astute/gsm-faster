$.fetchServiceData = function(srvcId, packPrice) { // has to be defined as a function, does not need to be inside a nested document ready function
	$('#dvLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		document.getElementById('packageId').value = srvcId;
		$.post(base_url+"page/ajxfileorder", 
		   {
			   packId:srvcId, 
			   purpose:'getData'
		   },
			function(response){
			if(response)
			{
				arrResponse = response.split('~');
				if(Trim(arrResponse[3]) != '')
				{
					window.open(arrResponse[3], '_blank');
				}
				document.getElementById('cldFrm').value = '0';
		
				if(document.getElementById('service_Details2'))
				{
					$('#service_Details2').hide();
					$('#serviceDetails2').show();
				}
				$('#tblInfo').show();
				if(arrResponse[0] != '')
				{
					$("#dvSN").html(arrResponse[0]);
				}
				if(arrResponse[1] != '')
				{
					$("#dvTT").html('<b>Delivery Time: </b>'+arrResponse[1]);
				}
			
				if(arrResponse[2] != '')
				{
					$("#dvMR").html("<b>SERVICE DETAILS:</b><br />"+arrResponse[2]);
				}
				if(Trim(arrResponse[4]) != '')
				{
					$('#dvFeatures').html(Trim(arrResponse[4]));
					$('#dvFeatures').show();
				}
				else
				{
					$('#dvFeatures').html('');
					$('#dvFeatures').hide();
				}
				if(packPrice != '')
				{
					$("#lblCustPrice").html($('#currSymbol').val()+packPrice);
					$("#lblCustPrice").show();
				}
			}
		});
};

$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#ddType").val()=='1' && $("#categoryId").val()=='0')
			errors += "- Please select Category.<br />";
		if ($("#packageId").val()=='0')
			errors += "- "+CUST_CODE_1+".<br />";
		if(document.getElementById("rdFileType1").checked)
		{
		
			if ($("#txtFile").val()=='')
				errors += "- "+CUST_LBL_1+".<br />";
		}
		else if(document.getElementById("rdFileType2").checked)
		{
			if ($("#txtZipFile").val()=='')
				errors += "- "+CUST_LBL_10+".<br />";
		}
		else if(document.getElementById("rdFileType3").checked)
		{
			if ($("#txtBulkFiles").val()=='')
				errors += "- "+CUST_LBL_11+".<br />";
		}
		if (errors=='')
		{
			$("#submitButton").attr("disabled", true);
			$("#submitButton").text('Please Wait...');
			return true;
		}
		else
		{
			$("#dvError").html(errors);
			$("#dvError").show();
			return false;
		}
	}

	$("#categoryId").change(function()
	{
		setValue('0');
		document.getElementById('frm').submit();
	});
	$("#packageId").change(function()
	{
		setValue('0');
		$('#dvLoader').ajaxStart(function() {
				$(this).show();
			}).ajaxStop(function() {
				$(this).hide();
			});
			$.post(base_url+"page/ajxfileorder", 
			   {
				   packId:$("#packageId").val(), 
				   purpose:'getData'
			   },
				function(response){
				if(response)
				{
					arrResponse = response.split('~');
					if(Trim(arrResponse[3]) != '')
					{
						window.open(arrResponse[3], '_blank');
					}
					document.getElementById('cldFrm').value = '0';
					if(document.getElementById('service_Details2'))
					{
						$('#service_Details2').hide();
						$('#serviceDetails2').show();
					}
			
					$('#tblInfo').show();
					if(arrResponse[0] != '')
					{
						$("#dvSN").html(arrResponse[0]);
					}
					if(arrResponse[1] != '')
					{
						$("#dvTT").html('<b>Delivery Time:</b> '+arrResponse[1]);
					}
					if(arrResponse[2] != '')
					{
						$("#dvMR").html("<b>SERVICE DETAILS:</b><br />"+arrResponse[2]);
					}
					if(Trim(arrResponse[4]) != '')
					{
						$('#dvFeatures').html(Trim(arrResponse[4]));
						$('#dvFeatures').show();
					}
					else
					{
						$('#dvFeatures').html('');
						$('#dvFeatures').hide();
					}
					var vPr = $('#packageId').find('option:selected').attr('details2');
					if(vPr != '')
					{
						$("#lblCustPrice").html($('#currSymbol').val()+vPr);
						$("#lblCustPrice").show();
					}
				}
			});
		
	});
	$("#frm").submit(function(){
		return validate();
	});
	$("#rdFileType1").click(function(){
		if(document.getElementById('rdFileType1').checked)
		{
			$("#dvSingleFile").show();
			$("#dvZipFile").hide();
			$("#dvBulkFiles").hide();
		}
	});
	$("#rdFileType2").click(function(){
		if(document.getElementById('rdFileType2').checked)
		{
			$("#dvZipFile").show();
			$("#dvSingleFile").hide();
			$("#dvBulkFiles").hide();
		}
	});
	$("#rdFileType3").click(function(){
		if(document.getElementById('rdFileType3').checked)
		{
			$("#dvBulkFiles").show();
			$("#dvZipFile").hide();
			$("#dvSingleFile").hide();
		}
	});
});

function setValue(i)
{
	if($("#packageId").val() != '0')
	{
		document.getElementById('cldFrm').value = i;
	}
}