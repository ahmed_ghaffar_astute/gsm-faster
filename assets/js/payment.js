$(document).ready(function()
{
	function validateAddPmnt()
	{
		var errors = '';
		if ($("#txtAmount1").val()=='')
			errors += "- "+BE_28+"<br />";
		if (!isFloat($("#txtAmount1").val()))
			errors += "- "+BE_29+"<br />";
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#frm1").submit(function(){
		return validateAddPmnt();
	});
	$("#btnSave").click(function(){
		$('#statusLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		var transferCredits = 0;
		var notify = 0;
		var notificationType = 0;
		if(document.getElementById('chkNotify').checked)
			notify = 1;
		if(document.getElementById('chkNotificationType2').checked)
			notificationType = 1;
		if(document.getElementById('chkTransferCredits'))
		{
			if(document.getElementById('chkTransferCredits').checked)
				transferCredits = 1;
		}
		$.post(base_url+"admin/clients/ajxpayment", 
		   {
			   transactionId:$('#txtTransactionId').val(), 
			   pMethodId:$('#pMethodId').val(), 
			   pStatusId:$('#pStatusId').val(), 
			   comments:$('#txtComments').val(), 
			   pEmail:$('#txtPEmail').val(), 
			   transferCredits:transferCredits, 
			   notify:notify, 
			   notificationType:notificationType, 
			   purpose:'save',
			   id:$('#id').val()
		   },
			function(response){
				if(response)
				{
					showStickyErrorToast(response, '', 'success');
				}
			});	
	});
	$("#chkNotify").click(function()
	{
		if(document.getElementById('chkNotify').checked)
			$("#dvNType").show();
		else
			$("#dvNType").hide();
	});
	
});