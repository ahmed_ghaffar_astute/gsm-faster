function savePrices()
{
	$("#hdSavePrices").val('1');
}
function selectAllBrands(totalBrands)
{
	selectAllChxBxs('chkSelect', 'chkBrand', totalBrands);
}
function selectAllModels(totalModels)
{
	selectAllChxBxs('chkSelect1', 'chkModel', totalModels);
}
function calculatePrices()
{
	if($("#id").val() == '0')
	{
		convertPrices();
	}
}
function convertPrices()
{
	if (isFloat($("#txtPrice").val()))
	{
		var totalCurrencies = $("#totalCurrencies").val();
		var price = $("#txtPrice").val();
		var conversionRate = 0;
		var i = 0;
		for(i = 0; i < totalCurrencies; i++)
		{
			var converterPrice = 0;
			converstionRate = $("#converstionRate"+i).val();
			converterPrice = parseFloat(price * converstionRate);
			$("#txtCurrPrice"+i).val(Round(converterPrice));	
		}
	}
}
function showHideNewTpl()
{
	if (document.getElementById('rdNewTplType0').checked)
		$("#dvNewOrderTpl").hide();
	else if (document.getElementById('rdNewTplType1').checked)
		$("#dvNewOrderTpl").show();
}
function showHideSucTpl()
{
	if (document.getElementById('rdSuccessTplType0').checked)
		$("#dvSucOrderTpl").hide();
	else if (document.getElementById('rdSuccessTplType1').checked)
		$("#dvSucOrderTpl").show();
}
function showHideCanTpl()
{
	if (document.getElementById('rdCanTplType0').checked)
		$("#dvCanOrderTpl").hide();
	else if (document.getElementById('rdCanTplType1').checked)
		$("#dvCanOrderTpl").show();
}

$(document).ready(function()
{
	// $("#sortableLst").sortable({
	// 	update : function () {
	// 	var order = $('#sortableLst').sortable('serialize');
	// 	$("#dvMsg").show();
	// 	$("#dvMsg").load("include/ajax/modules/process-sortable.php?i=11&id="+$('#id').val()+"&sc="+$('#fs').val()+"&"+order);
	// 	}
	// });
	if($('#id').val() > 0 && $('#hdAPIIdForBrand').val() > 0)
	{
		getAPIBrands($('#hdAPIIdForBrand').val(), 1);
	}
	function validate()
	{
		var errors = '';
		if ($("#txtTitle").val()=='')
			errors += "- "+BE_3+"<br />";
		if($("#dupSrvcId").val() == '0')
		{
			if ($("#categoryId").val()=='0')
				errors += "- "+BE_4+"<br />";
			if ($("#txtPrice").val()=='')
				errors += "- "+BE_5+"<br />";
			if (!isFloat($("#txtPrice").val()))
				errors += "- "+BE_6+"<br />";
			if(document.getElementById('rdServiceType0'))
			{
				if(document.getElementById('rdServiceType0').checked)
				{
					if ($("#txtResDelayTm").val()=='')
						errors += "- Please Enter Response Delay Time<br />";
					if (!isDigit($("#txtResDelayTm").val()))
						errors += "- Invalid value of Response Delay Time";
				}
			}
			if ($("#txtCancelTime").val() != '')
			{
				if (!isDigit($("#txtCancelTime").val()))
					errors += "- Invalid value for Time to Cancel Orders<br />";
			}
			if ($("#txtVerifyTime").val() != '')
			{
				if (!isDigit($("#txtVerifyTime").val()))
					errors += "- Invalid value for Time to Verfiy Orders";
			}
		}
		if($("#id").val() > 0)
		{
			if ($("#txtCronTiming").val() != '')
			{
				if (!isDigit($("#txtCronTiming").val()))
					errors += "- Invalid value of Cron Delay Time";
			}
		}
		if (errors=='')
		{
			return true;
		}
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	function getBrandIdsAndModels()
	{
		var apiId = $("#apiIdForBrand").val();
		var totalBrands = document.getElementById('totalBrands').value;
		var brandIds = '0';
		for(i = 0; i < totalBrands; i++)
		{
			if(document.getElementById('chkBrand'+i) && document.getElementById('chkBrand'+i).checked)
			{
				brandIds += ", " + document.getElementById('chkBrand'+i).value;
			}
		}
		getBrandModels(apiId, brandIds);
	}
	$("#chkCnvrtPrice").click(function()
	{
		if(document.getElementById('chkCnvrtPrice').checked)
		{
			var totalGroups = $("#totalGroups").val();
			var currencyCount = $("#currencyCount").val();
			var j = 0;
			for(j = 0; j < totalGroups; j++)
			{
				var price = Trim($("#txtPrice"+j+"_0").val());
				for(var z = 1; z <= currencyCount; z++)
				{
					var cRate = Trim($("#cRt"+j+"_"+z).val());
					if(isFloat(price) && isFloat(cRate))
					{
						var convertedPrice = Round(price * cRate);
						$("#txtOtherPrice"+j+"_"+z).val(convertedPrice);
					}
				}
			}
			
		}
	});
	$("#btnPriceEml").click(function()
	{
		$('#imgEmlLdr').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		$.post(base_url+"admin/services/ajxpackage", 
		   {
			   id:$("#id").val(), 
			   sc:$("#fs").val(), 
			   notes:$("#txtUserNotes").val(), 
			   groupId:$("#uNplanId").val(),
			   purpose:'sendsrvcemail'
		   },
			function(response){
				if(response)
				{
					showStickyErrorToast(response, '', 'success');
				}
			});	
	});
	$("#rdIMEIFType1").click(function()
	{
		if(document.getElementById('rdIMEIFType1').checked)
		{
			$("#dvCustFld").hide();
		}
	});
	$("#rdIMEIFType2").click(function()
	{
		if(document.getElementById('rdIMEIFType2').checked)
		{
			$("#dvCustFld").hide();
		}
	});
	$("#rdIMEIFType3").click(function()
	{
		if(document.getElementById('rdIMEIFType3').checked)
		{
			$("#dvCustFld").hide();
		}
	});
	$("#rdIMEIFType4").click(function()
	{
		if(document.getElementById('rdIMEIFType4').checked)
		{
			$("#dvCustFld").show();
		}
	});
	$("#apiId").change(function()
	{
		var arr = $("#apiId").val().split('~');
		var apiId = arr[0];
		var extId = arr[1];
		var apiType = arr[2];
		$("#dvExtNetworkId").show();
		getSupplierServices(apiId, $("#fs").val());
	});
	$("#dupSrvcId").change(function()
	{
		if($("#dupSrvcId").val() == '0')
			$("#dvGeneral").show();
		else
			$("#dvGeneral").hide();
	});
	$("#apiIdForBrand").change(function()
	{
		var apiId = $("#apiIdForBrand").val();
		$("#btnModels").hide();
		$("#dvModels").html('');
		$("#dvModels").hide();
		getAPIBrands(apiId, 0);
	});
	$("#btnModels").click(function()
	{
		getBrandIdsAndModels();
	});
	$("#chkRtl").click(function(){
		if(document.getElementById('chkRtl').checked)
		{
			$('#dvRtlSName').show();
			$('#dvRtlPrice').show();
			$('#dvDelTime').show();
			$('#dvRtlTN').show();
			$('#dvRtlImage').show();
			$('#dvRtlDetail').show();
		}
		else
		{
			$('#dvRtlSName').hide();
			$('#dvRtlPrice').hide();
			$('#dvDelTime').hide();
			$('#dvRtlTN').hide();
			$('#dvRtlImage').hide();
			$('#dvRtlDetail').hide();
		}
	});
	$("#rdServiceType0").click(function(){
		if(document.getElementById('rdServiceType0').checked)
		{
			$('#dvDelayTm').show();
		}
		else
		{
			$('#dvDelayTm').hide();
		}
	});
	$("#rdServiceType1").click(function(){
		if(document.getElementById('rdServiceType1').checked)
		{
			$('#dvDelayTm').hide();
		}
		else
		{
			$('#dvDelayTm').hide();
		}
	});
	$("#frm").submit(function(){
		return validate();
	});
	function validatePDP()
	{
		var errors = '';
		if ($("#packageId").val() == '0')
			errors += "- "+BE_9;
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	function validateCP()
	{
		var errors = '';
		if ($("#packageId").val() == '0')
			errors += "- "+BE_9+"<br />";
		if ($("#txtPrice").val()=='')
			errors += "- "+BE_5+"<br />";
		if (!isFloat($("#txtPrice").val()))
			errors += "- "+BE_6+"<br />";
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	function getSupplierServices(apiId, serviceType)
	{
		$('#imgSrvcLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		$.post(base_url+"admin/services/ajxpackage", 
		   {
			   apiId:apiId, 
			   serviceType:serviceType, 
			   purpose:'getsuppliersrvcs'
		   },
			function(response){
				if(response)
				{
					$("#supplierPackId").html(response);
				}
			});	
		
	}
	function getAPIBrands(apiId, z)
	{
		$('#imgBrndLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		$.post(base_url+"admin/services/ajxpackage", 
		   {
			   apiId:apiId, 
			   id:$('#id').val(),
			   hdAPIIdForBrand:$('#hdAPIIdForBrand').val(),  
			   purpose:'getapibrands'
		   },
			function(response){
				if(response)
				{
					arrResponse = response.split('~~~');
					document.getElementById('totalBrands').value = arrResponse[1];
					$("#dvBrands").html(arrResponse[2]);
					if(z == '1')
						getBrandIdsAndModels();
					$("#dvBrands").show();
					$("#btnModels").show();
				}
			});	
		
	}
	function getBrandModels(apiId, brandIds)
	{
		$('#imgBrndLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		$.post(base_url+"admin/services/ajxpackage", 
		   {
			   apiId:apiId, 
			   id:$('#id').val(),
			   brandIds:brandIds,
			   purpose:'getbrandmodels'
		   },
			function(response){
				if(response)
				{
					$("#dvModels").html(response);
					$("#dvModels").show();
				}
			});	
		
	}
	$("#btnSave").click(function(){
		if(validatePDP())
		{			
			$('#statusLoader').ajaxStart(function() {
				$(this).show();
			}).ajaxStop(function() {
				$(this).hide();
			});
			$.post(base_url+"admin/services/ajxpackage", 
			   {
				   packageId:$('#packageId').val(), 
				   purpose:'pdp'
			   },
				function(response){
					if(response)
					{
						arrResponse = response.split('~');
						if(arrResponse[1] == 1)
						{
						   $('#packageId').val('0');
						}
						showStickyErrorToast(arrResponse[0], '', 'success');
					}
				});	
		}
	});
	$("#btnSaveCustomPrice").click(function(){
		if(validateCP())
		{			
			$('#statusLoader').ajaxStart(function() {
				$(this).show();
			}).ajaxStop(function() {
				$(this).hide();
			});
			$.post(base_url+"admin/services/ajxpackage", 
			   {
				   packageId:$('#packageId').val(), 
				   price:$('#txtPrice').val(), 
				   purpose:'pcp'
			   },
				function(response){
					if(response)
					{
						arrResponse = response.split('~');
						if(arrResponse[1] == 1)
						{
						   $('#packageId').val('0');
						}
						showStickyErrorToast(arrResponse[0], '', 'success');
					}
				});	
		}
	});
	$("#tbDiscClients").click(function()
	{
		$('#dvDiscClientsLdr').ajaxStart(function() {
			$(this).show();
		})
		$.post(base_url+"admin/services/ajxpackage", 
		   {
			   packageId:$('#id').val(), 
			   packPrice:$("#txtPrice").val(),
			   purpose:'dc'
		   },
			function(response){
				if(response)
				{
				   $('#dvDiscClientsData').html(response);
				   $('#dvDiscClientsLdr').hide();
				}
			});	
	});
	$('#chkFetchCPr').click(function () {
		if ($(this).attr('checked'))
		{
			$('#imgCPLdr').ajaxStart(function() {
				$('#imgCPLdr').show();
			}).ajaxStop(function() {
				$('#imgCPLdr').hide();
			});
			$.post("include/ajax/modules/ajxcostprice.php", 
			   {
				   apiId:$('#exAPIId').val(), 
				   srvcId:$("#exAPIServiceId").val(),
				   serviceType:$("#fs").val(),
				   packageId:$("#id").val(),
				   purpose:'getCP'
			   },
				function(response){
					if(response)
					{
					   $('#txtCostPrice').val(response);
					}
				});	
		}
	})
});