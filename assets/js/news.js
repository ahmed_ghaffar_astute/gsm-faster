$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtTitle").val()=='')
		{
			errors += "- Please enter 'News Title'<br />";
			$("#txtTitle").focus();
		}
		if ($("#txtShortDesc").val()=='')
		{
			errors += "- Please enter 'Short Desc'<br />";
			$("#txtShortDesc").focus();
		}
		if ($("#txtNews").val()=='')
		{
			errors += "- Please enter 'News Detail'<br />";
			$("#txtNews").focus();
		}
		if (errors=='')
			return true;
		else
		{
			$("#errors_div").html("<b>Errors:</b><br />"+errors);
			$("#errors_div").fadeIn("slow");
			return false;
		}
	}
	$("#frm").submit(function(){
		return validate();
	});
});
