$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#groupId").val() == '0')
			errors += "- Please Choose a Client Group<br />";
		if ($("#txtPrice").val() == '')
			errors += "- Please Enter Price<br />";
		if ($("#txtPrice").val() != '')
		{
			if(!isFloat($("#txtPrice").val()))
				errors += "- Invalid value of Price<br />";
		}
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#btnSave").click(function(){
		if(validate())
		{			
			$('#statusLoader').ajaxStart(function() {
				$(this).show();
			}).ajaxStop(function() {
				$(this).hide();
			});
			var type = 0;
			var pricing = 0;
			if(document.getElementById('rdPrType1').checked)
				type = 1;
			if(document.getElementById('rdPricing1').checked)
				pricing = 1;
			$.post(base_url+"admin/services/ajxpricing", 
			   {
				   price:$('#txtPrice').val(), 
				   type:type, 
				   pricing:pricing, 
				   purpose:'update_g',
				   groupId:$('#groupId').val(), 
				   srvc:$('#srvc').val(), 
				   id:$('#id').val()
			   },
				function(response){
					if(response)
					{
						showStickyErrorToast(response, '', 'success');
					}
				});	
		}
	});
});