$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtReply").val()=='')
			errors += "- Please enter Reply<br />";
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#frmReply").submit(function(){
		return validate();
	});

	function validateStngs()
	{
		var errors = '';
		if ($("#txtSubject").val()=='')
			errors += "- Please enter Subject";
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#btnStngs").click(function(){
		if(validateStngs())
		{
			$('#imgStngs').ajaxStart(function() {
				$(this).show();
	
			}).ajaxStop(function() {
				$(this).hide();
			});
			$.post(base_url+"admin/tickets/ajxticket", 
			{
				   purpose:'update', 
				   subject:$('#txtSubject').val(), 
				   dId:$('#dId').val(), 
				   cId:$('#cId').val(), 
				   pId:$('#pId').val(), 
				   stId:$('#stId').val(), 
				   notes:$('#txtNotes').val(), 
				   tcktNo:$('#tcktNo').val()
			}, 
				function(response){
					if(response)
					{
						if(response)
							showStickyErrorToast(response, '', 'success');
					}
				});	
		}
	});

});