$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtUName").val()=='')
			errors += "- "+BE_13+"<br />";
		if ($("#txtEmail").val()=='')
			errors += "- "+BE_190+"<br />";
		if ($("#txtEmail").val()!='')
		{
			if (!isValidEmail($("#txtEmail").val()))
				errors += "- "+BE_191+"<br />";
		}
		if ($("#id").val() == 0)
		{
			if ($("#txtPass").val()=='')
				errors += "- "+BE_14+"<br>";
			if ($("#txtPass").val().length < 8)
				errors += "- "+BE_195+"<br>";
			if ($("#txtCPass").val()=='')
				errors += "- "+BE_15+"<br>";
			if ($("#txtCPass").val().length < 8)
				errors += "- "+BE_196+"<br>";
			if ($("#txtPass").val() != $("#txtCPass").val())
				errors += "- "+BE_16+"<br>";
		}
		else
		{
			if ($("#txtPass").val()!='')
			{
				if ($("#txtPass").val().length < 8)
					errors += "- "+BE_195+"<br>";
				if ($("#txtCPass").val()=='')
					errors += "- "+BE_15+"<br>";
				if ($("#txtCPass").val().length < 8)
					errors += "- "+BE_196+"<br>";
				if ($("#txtPass").val() != $("#txtCPass").val())
					errors += "- "+BE_16+"<br>";
			}
		}
		if ($("#txtFName").val()=='')
			errors += "- "+BE_17+"<br />";
		if ($("#txtLName").val()=='')
			errors += "- "+BE_18+"<br />";
		if ($("#txtPhone").val()=='')
			errors += "- "+BE_19+"<br />";
/*		if ($("#countryId").val()=='-1')
			errors += "- "+BE_20+"<br />";*/
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	function validatePassword()
	{
		var errors = '';
		if ($("#txtOldPass").val()=='')
			errors += "- "+BE_22+"<br />";
		if ($("#txtNewPass").val()=='')
			errors += "- "+BE_23+"<br>";
		if ($("#txtConfirmPass").val()=='')
			errors += "- "+BE_21+"<br>";
		if ($("#txtNewPass").val() != $("#txtConfirmPass").val())
			errors += "- "+BE_16+"<br>";
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	
	$("#chkDisable").click(function(){
		if(document.getElementById('chkDisable').checked)
		{
			$('#dvReason').show();
		}
		else
			$('#dvReason').hide();
	});
	$("#frm").submit(function(){
		return validate();
	});
	$("#btnEnDis").click(function(){
		$('#statusULdr').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});

		$.post(base_url+"admin/clients/ajxuser", 
		   {
			   uStatus:$('#hdUStatus').val(), 
			   purpose:'updStatus',
			   id:$('#id').val()
		   },
			function(response){
				if(response)
				{
					showStickyErrorToast(response, '', 'success');
					if($('#hdUStatus').val() == '1')
					{
						$('#fntStatus').html('Disable');
						$('#hdUStatus').val('0');
					}
					else if($('#hdUStatus').val() == '0')
					{
						$('#fntStatus').html('Enable');
						$('#hdUStatus').val('1');
					}
				}
			});	
	});
	

	$("#rdCrdType1").click(function(){
		$("#dvPS").show();
		$("#dvTrans").show();
		if($("#pStatusId").val() == '2')
			$("#scPMethod").show();
		else
			$("#scPMethod").hide();
	});
	$("#rdCrdType2").click(function(){
		$("#dvPS").hide();
		$("#dvTrans").hide();
		$("#scPMethod").hide();
	});
	$("#frmAchangepass").submit(function(){
		return validatePassword();
	});
	function validateAddCredits()
	{
		var errors = '';
		if ($("#txtCredits").val()=='')
			errors = "- "+BE_23+"<br />";
		if (!isFloat($("#txtCredits").val()))
			errors += "- "+BE_24+"<br />";
		if(document.getElementById("rdCrdType1").checked)
		{
			if ($("#pStatusId").val() == '0')
				errors += "- "+BE_65+"<br />";
			if ($("#pMethodId").val() == '0' && $("#pStatusId").val() == '2')
				errors += "- "+BE_66+"<br />";
		}
/*		if ($("#txtAuthKey").val()=='')
			errors += "- "+BE_57+"<br />";*/
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#frmAddCredits").submit(function()
	{
		return validateAddCredits();
	});
	$("#currencyId").change(function()
	{
		$("#hdCurrency").val($("#currencyId option:selected").text());
	});
	$("#pStatusId").change(function()
	{
		if($("#pStatusId").val() == '2')
			$("#scPMethod").show();		
		else
			$("#scPMethod").hide();
		$("#hdPSTXT").val($("#pStatusId option:selected").text());
	});
	
});