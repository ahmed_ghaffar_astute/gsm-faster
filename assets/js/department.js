$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtDept").val()=='')
			errors += "- Please enter Department'";
		if ($("#txtEmail").val()=='')
			errors += "- Please enter Email'";
		if (isEmail($("#txtEmail").val()) !=true)
			errors += "- Please enter valid Email'";
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}

	$("#btnSave").click(function(){
		if(validate())
		{			
			$('#statusLoader').ajaxStart(function() {
				$(this).show();
	
			}).ajaxStop(function() {
				$(this).hide();
			});
			var disableVal = 0;
			if(document.getElementById('disable').checked)
				disableVal = 1;
			$.post(base_url+"admin/tickets/ajxdepartment", 
			   {
				   purpose:'save', 
				   dept:$('#txtDept').val(), 
				   email:$('#txtEmail').val(), 
				   desc:$('#txtDesc').val(), 
				   disableVal:disableVal, 
				   id:$('#id').val()
				}, 
				function(response){
					if(response)
					{
						arrResponse = response.split('~');
						if(arrResponse[1] == 1)
						{
							$('#txtDept').val('');
							$('#txtEmail').val('');
							$('#txtDesc').val('');
						}
						$('#txtDept').focus();
						showStickyErrorToast(arrResponse[0], '', 'success');
					}
				});	
		}
	});
});
function isEmail(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
}
