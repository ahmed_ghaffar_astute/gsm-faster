$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtEmail").val()=='')
			errors += "- Please enter Username<br />";
		/*if ($("#roleId").val() == '0')
			errors += "- Please Select Admin Role<br />";*/
		if ($("#id").val() == 0)
		{
			if ($("#txtPass").val()=='')
				errors += "- Please enter Password<br>";
			if ($("#txtPass").val().length < 8)
				errors += "- Password should be atleast of 8 characters<br>";
			if ($("#txtCPass").val()=='')
				errors += "- Please enter Confirm Password<br>";
			if ($("#txtPass").val() != $("#txtCPass").val())
				errors += "- Passwords do not match<br>";
		}
		else
		{
			if ($("#txtPass").val()!='')
			{
				if ($("#txtPass").val().length < 8)
					errors += "- Password should be atleast of 8 characters<br>";
				if ($("#txtCPass").val()=='')
					errors += "- Please enter Confirm Password<br>";
				if ($("#txtPass").val() != $("#txtCPass").val())
					errors += "- Passwords do not match<br>";
			}
		}
		if ($("#txtFName").val()=='')
			errors += "- Please enter First Name<br />";
		if ($("#txtLName").val()=='')
			errors += "- Please enter Last Name<br />";
		if ($("#txtPhone").val()=='')
			errors += "- Please enter Phone<br />";
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, 'Errors', 'error');
			return false;
		}
	}
	$("#chkGAuth").click(function(){
		if(document.getElementById('chkGAuth').checked)
		{
			$('#imgGAuth').show();
		}
		else
			$('#imgGAuth').hide();
	});
	$("#btnSave").click(function(){
		if(validate())
		{			
			$('#statusLoader').ajaxStart(function() {
				$(this).show();
			}).ajaxStop(function() {
				$(this).hide();
			});
			var chkBxVal = 0;
			var chkGAuth = 0;
			if(document.getElementById('chkDisable').checked)
				chkBxVal = 1;
			if(document.getElementById('chkGAuth').checked)
				chkGAuth = 1;
				
			$.post(base_url+"admin/system/ajxsubadmin", 
			   {
				   email:$('#txtEmail').val(), 
				   fName:$('#txtFName').val(), 
				   lName:$('#txtLName').val(), 
				   pass:$('#txtPass').val(), 
				   phone:$('#txtPhone').val(), 
				   roleId:$('#roleId').val(), 
				   chkBxVal:chkBxVal,
				   chkGAuth:chkGAuth,
				   gAuthSecret:$('#gAuthSecret').val(), 
				   purpose:'save',
				   id:$('#id').val()
			   },
				function(response){
					if(response)
					{
						arrResponse = response.split('~');
						if(arrResponse[1] == 1)
						{
						   $('#txtEmail').val('');
						   $('#txtFName').val('');
						   $('#txtLName').val('');
						   $('#txtPhone').val('');
						   document.getElementById('chkDisable').checked = false;
						}
						showStickyErrorToast(arrResponse[0], '', 'success');
					}
				});	
		}
	});
});