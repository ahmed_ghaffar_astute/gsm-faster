// When the document is ready set up our sortable with it's inherant function(s)
$(document).ready(function() {
var HW = $('#hw').val();
var fs = $('#fs').val();
$("#sortableLst").sortable({
	update : function () {
	var order = $('#sortableLst').sortable('serialize');
	$("#dvMsg").show();
	$("#dvMsg").load(base_url+"admin/services/processsortable?hw="+HW+"&fs="+fs+"&"+order);
	}
});
});