$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtTitle").val()=='')
		{
			errors += "- "+BE_40+"<br />";
		}
		if ($("#id").val() == '0')
		{
			if ($("#txtBanner").val()=='')
				errors += "- "+BE_61;
		}
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#frm").submit(function(){
		return validate();
	});

	var type = $('#type').val();
	$("#sortableLst").sortable({
		update : function () {
		var order = $('#sortableLst').sortable('serialize');
		$("#dvMsg").show();
		$("#dvMsg").load("include/ajax/modules/process-sortable.php?i=12&type="+type+"&"+order);
		}
	});

});