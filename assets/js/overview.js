$(document).ready(function()
{
	function validatePassword()
	{
		var errors = '';
		if ($("#txtPass").val()=='')
		{
			errors += "- Please Enter Password<br />";
			$("#txtPass").focus();
		}
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	function validateNL()
	{
		var errors = '';
		if ($("#ltrId").val() == '0')
		{
			errors += "- Please Select Newsletter<br />";
		}
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	function validateCE()
	{
		var errors = '';
		if (Trim($("#txtCEmailSubj").val()) == '')
		{
			errors += "- Please Enter Subject<br />";
		}
		if (Trim($("#txtCEmailSubj").val()) == '')
		{
			errors += "- Please Enter Message<br />";
		}
		txtCEmailMsg
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#btnChPwd").click(function(){
		$('#statusLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		$.post(base_url+"admin/services/ajxoverview", 
		   {
			   purpose:'chPwd',
			   id:$('#id').val()
		   },
			function(response){
				if(response)
				{
					showStickyErrorToast(response, '', 'success');
				}
			});	
	});
	$("#btnChPin").click(function(){
		$('#statusLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		$.post(base_url+"admin/services/ajxoverview", 
		   {
			   purpose:'chPin',
			   id:$('#id').val()
		   },
			function(response){
				if(response)
				{
					showStickyErrorToast(response, '', 'success');
				}
			});	
	});
	$("#btnSave").click(function(){
		$('#statusLoader99').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		var allowCrdsTrnsfr = 0;
		var autoFllCrdts = 0;
		var rmvAllSrvcs = 0;
		var allowAPI = 0;
		var disableUser = 0;
		var buyCr = 0;
		if(document.getElementById('rdAllowCrdtTransfr1').checked)
			allowCrdsTrnsfr = 1;
		if(document.getElementById('rdAutoFillCrdts1').checked)
			autoFllCrdts = 1;
		if(document.getElementById('rdRmvSrvcs1').checked)
			rmvAllSrvcs = 1;
		if(document.getElementById('rdAPIAllowed1').checked)
			allowAPI = 1;
		if(document.getElementById('rdActivate2').checked)
			disableUser = 1;
		if(document.getElementById('rdBuyCr1').checked)
			buyCr = 1;
		$.post(base_url+"admin/services/ajxoverview", 
		   {
			   purpose:'save',
			   autoFllCrdts:autoFllCrdts, 
			   allowCrdsTrnsfr:allowCrdsTrnsfr, 
			   rmvAllSrvcs:rmvAllSrvcs, 
			   allowAPI:allowAPI, 
			   disableUser:disableUser, 
			   buyCr:buyCr, 
			   planId:$('#planId').val(), 
			   reason:$('#txtReason').val(), 
			   id:$('#id').val()
		   },
			function(response){
				if(response)
				{
					showStickyErrorToast(response, '', 'success');
				}
			});	
	});
	$("#btnChangePwd").click(function(){
		if(validatePassword())
		{
			$('#statusLoader98').ajaxStart(function() {
				$(this).show();
			}).ajaxStop(function() {
				$(this).hide();
			});
			$.post(base_url+"admin/services/ajxoverview", 
			   {
				   purpose:'changePwd',
				   pwd:$("#txtPass").val(), 
				   id:$('#id').val()
			   },
				function(response){
					if(response)
					{
						showStickyErrorToast(response, '', 'success');
					}
				});	
		}
	});
	$("#btnSendEml").click(function(){
		if(validateNL())
		{
			$('#statusLoader97').ajaxStart(function() {
				$(this).show();
			}).ajaxStop(function() {
				$(this).hide();
			});
			$.post(base_url+"admin/services/ajxoverview", 
			   {
				   purpose:'sendEml',
				   ltrId:$("#ltrId").val(), 
				   userId:$('#id').val()
			   },
				function(response){
					if(response)
					{
						showStickyErrorToast(response, '', 'success');
					}
				});	
		}
	});
	$("#btnSendCEml").click(function(){
		if(validateCE())
		{
			$('#statusLoader99').ajaxStart(function() {
				$('#statusLoader99').show();
			}).ajaxStop(function() {
				$('#statusLoader99').hide();
			});
			$.post(base_url+"admin/services/ajxoverview", 
			   {
				   purpose:'sendCEml',
				   subject:$("#txtCEmailSubj").val(), 
				   msg:$('#txtCEmailMsg').val(), 
				   userId:$('#userId').val()
			   },
				function(response){
					if(response)
					{
						showStickyErrorToast(response, '', 'success');
					}
				});	
		}
	});
	$("#btnSendEml_rtl").click(function(){
		if(validateNL())
		{
			$('#statusLoader97').ajaxStart(function() {
				$(this).show();
			}).ajaxStop(function() {
				$(this).hide();
			});
			$.post(base_url+"admin/services/ajxoverview", 
			   {
				   purpose:'sendEml_rtl',
				   ltrId:$("#ltrId").val(), 
				   email:$('#email').val()
			   },
				function(response){
					if(response)
					{
						showStickyErrorToast(response, '', 'success');
					}
				});	
		}
	});
	$("#btnSendCEml_rtl").click(function(){
		if(validateCE())
		{
			$('#statusLoader99').ajaxStart(function() {
				$('#statusLoader99').show();
			}).ajaxStop(function() {
				$('#statusLoader99').hide();
			});
			$.post(base_url+"admin/services/ajxoverview", 
			   {
				   purpose:'sendCEml_rtl',
				   subject:$("#txtCEmailSubj").val(), 
				   msg:$('#txtCEmailMsg').val(), 
				   email:$('#email').val()
			   },
				function(response){
					if(response)
					{
						showStickyErrorToast(response, '', 'success');
					}
				});	
		}
	});
});
function showReason()
{
	if(document.getElementById('rdUserStatus1').checked)
	{
		$('#rwReason').hide();
	}
	else
		$('#rwReason').show();
}