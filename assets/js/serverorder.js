$(document).ready(function()
{
	if($('#cldFrm').val() != '2')
		fetchData();
	function validate()
	{
		var errors = '';
		if ($("#ddType").val()=='1' && $("#categoryId").val()=='0')
			errors += "- Please select Category.<br />";
		if ($("#packageId").val()=='0')
			errors += "- "+CUST_LBL_2+"<br />";
		/*
		var fType = $("#fType").val();
		if(fType == '1')
		{
			if ($("#txtSerial").val() == '')
				errors += "- "+CUST_LBL_4+"<br />";
		}
		else if(fType == '2')
		{
			if ($("#txtBoxUN").val() == '')
				errors += "- "+CUST_LBL_5+"<br />";
		}
		else if(fType == '4')
		{
			if ($("#txtUN").val() == '')
				errors += "- "+CUST_LBL_12+"<br />";
		}
		else if(fType == '5')
		{
			if ($("#txtQty").val() == '')
				errors += "- "+CUST_LBL_17+"<br />";
		}
		else if(fType == '3')
		{
			if ($("#txtSerial").val() == '')
				errors += "- "+CUST_LBL_4+"<br />";
			if ($("#txtBoxUN").val() == '')
				errors += "- "+CUST_LBL_5+"<br />";
			if ($("#txtUN").val() == '')
				errors += "- "+CUST_LBL_12;
		}*/
		if (errors=='')
		{
			$("#submitButton").attr("disabled", true);
			$("#submitButton").text('Please Wait...');
			return true;
		}
		else
		{
			$("#dvError").html(errors);
			$("#dvError").show();
			return false;
		}
	}
	$("#packageId").change(function()
	{
		fetchData();
		$("#dvError").hide();
	});
	$("#categoryId").change(function()
	{
		setValue('0');
		document.getElementById('frm').submit();
	});
	$("#frm").submit(function(){
		return validate();
	});
	function fetchData()
	{
		if($('#packageId').val() > 0)
		{
			$('#spnLoader').ajaxStart(function() {
				$(this).show();
			}).ajaxStop(function() {
				$(this).hide();
			});
			$.post(base_url+"page/ajxserverorder", 
			   {
				   packId:$('#packageId').val(), 
				   themeStyle:$('#themeStyle').val(),
				   grp:$('#ppl').val(), 
				   purpose:'getData'
			   },
				function(response){
				if(response)
				{
					/**************************/
					$("#lblCustPrice").html('');
					$("#lblQtyPrice").html('');
					$("#lblQtyPrice").hide();
					$("#lblCustPrice").hide();
					$('#spnQtyLdr').hide();
					/**************************/
					arrResponse = response.split('~');
					if(Trim(arrResponse[5]) != '')
					{
						window.open(arrResponse[5], '_blank');
					}
					
					//document.getElementById('cldFm').value = '0';
					$('#dvCstmFlds').html(arrResponse[2]);
					var mustRead = arrResponse[0];
					if(mustRead != '')
					{
						$('#dvMR').html(mustRead);
						$('#dvMR').show();
					}
					$('#dvTT').html('Delivery Time: '+arrResponse[1]);
					$('#tblInfo').show();
					if(document.getElementById('service_Details2'))
					{
						$('#service_Details2').hide();
						$('#serviceDetails2').show();
					}
					$('#hdPreOrder').val(arrResponse[3]);
					$('#min_qty').val(arrResponse[8]);
					if(arrResponse[4] != '')
					{
						$("#dvSN").html(arrResponse[4]);
					}
					$("#dvBlkQty").html(arrResponse[6]);

					if(Trim(arrResponse[7]) != '')
					{
						$('#dvFeatures').html(Trim(arrResponse[7]));
						$('#dvFeatures').show();
					}
					else
					{
						$('#dvFeatures').html('');
						$('#dvFeatures').hide();
					}
					setPrice();
					
				}
			});	
		}
	}

});
function setPrice()
{
	if($("#packageId").val() != '0')
	{
		var vPr = $('#packageId').find('option:selected').attr('details2');
		if(vPr != '')
		{
			//$("#lblCustPrice").html($('#currSymbol').val()+vPr+$('#currAbb').val());
			$("#lblCustPrice").html($('#currSymbol').val()+vPr);
			$("#lblCustPrice").show();
		}
	}
}
function setValue(i)
{
	document.getElementById('cldFrm').value = i;
}
function calculateAmountPerQty()
{
	if ($("#ordrQty").val()=='' || !isDigit($("#ordrQty").val()) || parseFloat($("#ordrQty").val()) == 0)
	{
		$("#ordrQty").val('1');
	}
	$('#spnQtyLdr').show();
	$.post(base_url+"page/ajxserverorder", 
	   {
		   packId:$('#packageId').val(), 
		   qty:$('#ordrQty').val(), 
		   purpose:'getQtyPrice'
	   },
		function(response){
		if(response)
		{
			var str = response.replace(new RegExp('\n','g'), '');
			response = str;
			if(response == '0')
				var vPr = $('#packageId').find('option:selected').attr('details2');
			else
				var vPr = response;
			if(vPr != '')
			{
				var price = vPr * document.getElementById('ordrQty').value;
				$("#lblCustPrice").html($('#currSymbol').val() + Round(price));
				$("#lblQtyPrice").html('Your price per Item is '+$('#currSymbol').val() + vPr);
				$("#lblQtyPrice").show();
				$("#lblCustPrice").show();
				$('#spnQtyLdr').hide();
			}
		}
	});	
}
function calculateAmountAsPerQty(obj)
{
	var QTY = 1;
	if($("#ddOrdrQty").val() > 0)
		QTY = $("#ddOrdrQty").val();
	$('#spnDDQtyLdr').show();
	$.post(base_url+"page/ajxserverorder", 
	   {
		   packId:$('#packageId').val(), 
		   qty:QTY, 
		   purpose:'getQtyPrice'
	   },
		function(response){
		if(response)
		{
			if(response == '0')
				var vPr = $('#packageId').find('option:selected').attr('details2');
			else
				var vPr = response;
			if(vPr != '')
			{
				var price = vPr * QTY;
				$("#lblCustPrice").html($('#currSymbol').val() + Round(price));
				$("#lblQtyPrice").html('Your price per Item is '+$('#currSymbol').val() + vPr);
				$("#lblQtyPrice").show();
				$("#lblCustPrice").show();
				$('#spnDDQtyLdr').hide();
				//$("#hdDDQty").val(QTY);
			}
		}
	});	
}