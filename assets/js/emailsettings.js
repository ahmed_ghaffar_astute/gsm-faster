$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtCompany").val() == '')
			errors += "- "+BE_47+"<br />";
		if ($("#txtFName").val() == '')
			errors += "- "+BE_48+"<br />";
		if ($("#txtFEmail").val() == '')
			errors += "- "+BE_49+"<br />";
		if ($("#txtTEmail").val() == '')
			errors += "- Please Enter To Email<br />";
		if ($("#txtCUEmail").val() == '')
			errors += "- Please Enter Contact Us Email<br />";
		if ($("#txtURL").val() == '')
			errors += "- "+BE_50+"<br />";
		if (Trim($("#txtGCSiteKey").val()) == '')
			errors += "- Please enter value of Google Captcha Site Key<br />";
		if (Trim($("#txtGCSecKey").val()) == '')
			errors += "- Please enter value of Google Captcha Security Key<br />";
		if ($("#txtMinCredits").val() != '')
		{
			if (!isFloat($("#txtMinCredits").val()))
				errors += "- "+BE_192+"<br />";
		}
		if ($("#txtMaxCredits").val() != '')
		{
			if (!isFloat($("#txtMaxCredits").val()))
				errors += "- "+BE_193+"<br />";
		}
		if ($("#txtCrdtsTrsfrFee").val() == '')
			errors += "- Please Enter Credits Transfer Fee<br />";
		if ($("#txtCrdtsTrsfrFee").val() != '')
		{
			if (!isFloat($("#txtCrdtsTrsfrFee").val()))
				errors += "- Invalid value of Credits Transfer Fee<br />";
		}
/*
		if ($("#txtMinCredits").val() != '' && $("#txtMaxCredits").val() != '')
		{
			if(parseFloat($("#txtMinCredits").val()) > parseFloat($("#txtMaxCredits").val()))
				errors += "- "+BE_192+"<br />";
		}*/
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	function saveData(i)
	{
		if(validate())
		{
			if(i == '0')
			{
				$("#dvLoader").ajaxStart(function() {
				  $(this).show();
				}).ajaxStop(function() {
				  $(this).hide();
				});
			}
			else if(i == '1')
			{
				$("#dvLoader1").ajaxStart(function() {
				  $(this).show();
				}).ajaxStop(function() {
				  $(this).hide();
				});
			}
			else if(i == '2')
			{
				$("#dvLoader2").ajaxStart(function() {
				  $(this).show();
				}).ajaxStop(function() {
				  $(this).hide();
				});
			}
			else if(i == '3')
			{
				$("#dvLdLayOut").ajaxStart(function() {
				  $(this).show();
				}).ajaxStop(function() {
				  $(this).hide();
				});
			}
			else if(i == '4')
			{
				$("#dvLoaderNL").ajaxStart(function() {
				  $(this).show();
				}).ajaxStop(function() {
				  $(this).hide();
				});
			}
			else if(i == '5')
			{
				$("#dvLrdSrvEml").ajaxStart(function() {
				  $(this).show();
				}).ajaxStop(function() {
				  $(this).hide();
				});
			}
			var chkSm = 0;
			var pinCode = 0;
			var design = '1';
			var adminArchvd = 0;
			var txtDir = 0;
			var imeiFType = 0;
			var imeiServices = '0';
			var fileServices = '0';
			var serverServices = '0';
			var showPrices = 0;
			var showEPrices = 0;
			var newClients = 0;
			var cancelIMEIOrdrs = 0;
			var cancelFileOrdrs = 0;
			var cancelServerOrdrs = 0;
			var imeiOrdrEmail = 0;
			var fileOrdrEmail = 0;
			var srvrOrdrEmail = 0;
			var imeiScsOrdrEmail = 0;
			var fileScsOrdrEmail = 0;
			var srvrScsOrdrEmail = 0;
			var imeiRejOrdrEmail = 0;
			var fileRejOrdrEmail = 0;
			var srvrRejOrdrEmail = 0;
			var sendNL = 0;
			var knowledgeBase = 0;
			var ticketSystem = 0;
			var faqs = 0;
			var videos = 0;
			var sendEmails = 0;
			var retail = 0;
			var autoCredits = 0;
			var canBuyCr = 0;
			var adminCaptcha = 0;
			var userCaptcha = 0;
			var gTransU = 0;
			var gTransS = 0;
			var rgstrCaptcha = 0;
			var cntctCaptcha = 0;
			if(document.getElementById('chkKB').checked)
				knowledgeBase = 1;
			if(document.getElementById('chkTcktSys').checked)
				ticketSystem = 1;
			if(document.getElementById('chkFAQs').checked)
				faqs = 1;
			if(document.getElementById('chkVdeos').checked)
				videos = 1;
			if(document.getElementById('chkPinCode').checked)
				pinCode = 1;
			if(document.getElementById('chkSm').checked)
				chkSm = 1;
			var theme = $('input[name=rdTheme]:checked', '#frmSettings').val();

			if(document.getElementById('rdIMEIFType2').checked)
				imeiFType = '1';
			else if(document.getElementById('rdIMEIFType3').checked)
				imeiFType = '2';
			if(document.getElementById('rdIMEIOrderDD0').checked)
				imeiDDType = '0';
			else if(document.getElementById('rdIMEIOrderDD1').checked)
				imeiDDType = '1';
			else if(document.getElementById('rdIMEIOrderDD2').checked)
				imeiDDType = '2';
			if(document.getElementById('rdFileOrderDD0').checked)
				fileDDType = '0';
			else if(document.getElementById('rdFileOrderDD1').checked)
				fileDDType = '1';
			else if(document.getElementById('rdFileOrderDD2').checked)
				fileDDType = '2';
			if(document.getElementById('rdServerOrderDD0').checked)
				serverDDType = '0';
			else if(document.getElementById('rdServerOrderDD1').checked)
				serverDDType = '1';
			else if(document.getElementById('rdServerOrderDD2').checked)
				serverDDType = '2';
			if(document.getElementById('chkRtl').checked)
				retail = 1;
			if(document.getElementById('chkShowPrices').checked)
				showPrices = '1';
			if(document.getElementById('chkShowEPrices').checked)
				showEPrices = '1';
			if(document.getElementById('rdIMEI').checked)
				imeiServices = '1';
			if(document.getElementById('rdFile').checked)
				fileServices = '1';
			if(document.getElementById('rdServer').checked)
				serverServices = '1';
			if(document.getElementById('rdNewClients1').checked)
				newClients = '1';

			if(document.getElementById('rdAdminCaptcha1').checked)
				adminCaptcha = '1';
			if(document.getElementById('rdUserCaptcha1').checked)
				userCaptcha = '1';
			if(document.getElementById('rdRgstrCaptcha1').checked)
				rgstrCaptcha = '1';
			if(document.getElementById('rdCntctCaptcha1').checked)
				cntctCaptcha = '1';

			if(document.getElementById('chkIMEIOrderEmail').checked)
				imeiOrdrEmail = 1;
			if(document.getElementById('chkFileOrderEmail').checked)
				fileOrdrEmail = 1;
			if(document.getElementById('chkSrvrOrderEmail').checked)
				srvrOrdrEmail = 1;
			if(document.getElementById('chkSNL').checked)
				sendNL = 1;
			if(document.getElementById('chkIMEIScsOrderEml').checked)
				imeiScsOrdrEmail = 1;
			if(document.getElementById('chkFileScsOrderEml').checked)
				fileScsOrdrEmail = 1;
			if(document.getElementById('chkSrvrScsOrderEml').checked)
				srvrScsOrdrEmail = 1;
			if(document.getElementById('chkIMEIRejOrderEml').checked)
				imeiRejOrdrEmail = 1;
			if(document.getElementById('chkFileRejOrderEml').checked)
				fileRejOrdrEmail = 1;
			if(document.getElementById('chkSrvrRejOrderEml').checked)
				srvrRejOrdrEmail = 1;
			if(document.getElementById('chkSendEmails').checked)
				sendEmails = 1;
			var homePage = $('input[name=rdHomePage]:checked', '#frmSettings').val();
			if(document.getElementById('chkCanBuyCr').checked)
				canBuyCr = 1;
			if(document.getElementById('chkAutoCredits').checked)
				autoCredits = 1;
			if(document.getElementById('chkGTU').checked)
				gTransU = 1;
			if(document.getElementById('chkGTS').checked)
				gTransS = 1;
			$.post(baseurl + "admin/settings/ajxemailsettings",
			   {
				   url:$('#txtURL').val(),
				   company:$('#txtCompany').val(),
				   fEmail:$('#txtFEmail').val(),
				   tEmail:$('#txtTEmail').val(),
				   cuEmail:$('#txtCUEmail').val(),
				   fName:$('#txtFName').val(),
				   phone:$('#txtPhone').val(),
				   gCSiteKey:$('#txtGCSiteKey').val(),
				   gCSecKey:$('#txtGCSecKey').val(),
				   invsNotify:$('#invsNotify').val(),
				   rushPmnt:$('#rushPmnt').val(),
				   ffPwdChDays:$('#ffPwdChDays').val(),
				   faqs:faqs,
				   videos:videos,
				   imeiOrdrEmail:imeiOrdrEmail,
				   fileOrdrEmail:fileOrdrEmail,
				   srvrOrdrEmail:srvrOrdrEmail,
				   showPrices:showPrices,
				   showEPrices:showEPrices,
					dtTmDiffType:$('#dtTmDiffType').val(),
					dtTmDiffHours:$('#dtTmDiffHours').val(),
					dtTmDiffMins:$('#dtTmDiffMins').val(),
				   loginAttempts:$('#txtLoginAttempts').val(),
				   minCredits:$('#txtMinCredits').val(),
				   maxCredits:$('#txtMaxCredits').val(),
				   address:$('#txtAddress').val(),
				   payTo:$('#txtInvPayTo').val(),
				   customDsgn:$('#customDsgn').val(),
				   crdtsTrnsfrFee:$('#txtCrdtsTrsfrFee').val(),
				   signature:$('#txtSignature').val().replace( /\n/g, '<br />' ),
				   pinCode:pinCode,
				   imeiServices:imeiServices,
				   fileServices:fileServices,
				   serverServices:serverServices,
				   newClients:newClients,
				   lang:$('#language').val(),
				   imeiFType:imeiFType,
				   imeiDDType:imeiDDType,
				   fileDDType:fileDDType,
				   serverDDType:serverDDType,
				   retail:retail,
				   chkSm:chkSm,
				   sendEmails:sendEmails,
				   adminArchvd:adminArchvd,
				   theme: theme,
				   sendNL: sendNL,
				   imeiScsOrdrEmail: imeiScsOrdrEmail,
				   fileScsOrdrEmail: fileScsOrdrEmail,
				   srvrScsOrdrEmail: srvrScsOrdrEmail,
				   imeiRejOrdrEmail: imeiRejOrdrEmail,
				   fileRejOrdrEmail: fileRejOrdrEmail,
				   srvrRejOrdrEmail: srvrRejOrdrEmail,
				   knowledgeBase: knowledgeBase,
				   ticketSystem: ticketSystem,
				   homePage: homePage,
				   autoCredits:autoCredits,
				   canBuyCr:canBuyCr,
				   adminCaptcha:adminCaptcha,
				   userCaptcha:userCaptcha,
				   rgstrCaptcha:rgstrCaptcha,
				   cntctCaptcha:cntctCaptcha,
				   gTransU:gTransU,
				   gTransS:gTransS,
				   aTitle:$('#txtATitle').val(),
				   sTitle:$('#txtSTitle').val(),
				   shTitle:$('#txtShTitle').val(),
				   copyRights:$('#txtCopyright').val(),
				   purpose:'save',
				   id:$('#id').val()
			   },
				function(response)
				{
					if(response)
					{
						arrResponse = response.split('~');
						showStickyErrorToast(arrResponse[0], '', 'success');
					}
				});
		}
	}
	$("#btnGeneral").click(function(){
		saveData(0);
	});
	$("#btnSrvcs").click(function(){
		saveData(1);
	});
	$("#btnClients").click(function(){
		saveData(2);
	});
	$("#btnLayout").click(function(){
		saveData(3);
	});
	$("#btnSendNL").click(function(){
		saveData(4);
	});
	$("#btnSrvcEmls").click(function(){
		saveData(5);
	});
	$("#btnGeniePay").click(function(){
		var errors = '';
		if ($("#txtPPTrsfrFee").val() == '')
			errors += "- Please Enter Mass Payment Credits Fee<br />";
		if ($("#txtPPTrsfrFee").val() != '')
		{
			if (!isFloat($("#txtPPTrsfrFee").val()))
				errors += "- Invalid value of Mass Payment Credits Fee<br />";
		}
		if (errors == '')
		{
			$("#dvLoaderGP").ajaxStart(function() {
			  $(this).show();
			}).ajaxStop(function() {
			  $(this).hide();
			});
			$.post(baseurl + "admin/settings/ajxemailsettings",
			   {
				   ppTrnsfrFee:$('#txtPPTrsfrFee').val(),
				   purpose:'saveGP',
				   id:$('#id').val()
			   },
				function(response){
					if(response)
					{
						arrResponse = response.split('~');
						showStickyErrorToast(arrResponse[0], '', 'success');
					}
				});
		}
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	});
	$("#btnIPRestriction").click(function(){
		$("#dvLoaderIP").ajaxStart(function() {
		  $(this).show();
		}).ajaxStop(function() {
		  $(this).hide();
		});
		var ipRestriction = 0;
		var apiIPRstrct = 0;
		if(document.getElementById('chkIPRestriction').checked)
			ipRestriction = 1;
		if(document.getElementById('chkAPIIPRstrct').checked)
			apiIPRstrct = 1;
		$.post(baseurl + "admin/settings/ajxemailsettings",
		   {
			   ipRestriction:ipRestriction,
			   apiIPRstrct:apiIPRstrct,
			   purpose:'saveIPRes',
			   id:$('#id').val()
		   },
			function(response){
				if(response)
				{
					arrResponse = response.split('~');
					showStickyErrorToast(arrResponse[0], '', 'success');
				}
			});
		});
	$("#btnSMS").click(function(){
		$("#dvLdrSMS").ajaxStart(function() {
		  $(this).show();
		}).ajaxStop(function() {
		  $(this).hide();
		});
		var sendSMS = 0;
		if(document.getElementById('chkSMS').checked)
			sendSMS = 1;
		$.post(baseurl + "admin/settings/ajxemailsettings",
		   {
			   sendSMS:sendSMS,
			   purpose:'saveSMS'
		   },
			function(response){
				if(response)
				{
					arrResponse = response.split('~');
					showStickyErrorToast(arrResponse[0], '', 'success');
				}
			});
		});
	$("#btnSMTP").click(function(){
		$("#imgSMTP").ajaxStart(function() {
		  $(this).show();
		}).ajaxStop(function() {
		  $(this).hide();
		});
		var smtp = 0;
		var smtpAuth = 0;
		if(document.getElementById('chkSMTP').checked)
			smtp = 1;
		if(document.getElementById('chkSMTPAuth').checked)
			smtpAuth = 1;
		$.post(baseurl + "admin/settings/ajxemailsettings",
		   {
			   smtp:smtp, 
			   smtpAuth:smtpAuth, 
			   smtpHost:$('#txtSMTPHost').val(), 
			   smtpPort:$('#txtSMTPPort').val(), 
			   smtpUN:$('#txtSMTPUN').val(), 
			   smtpPwd:$('#txtSMTPPwd').val(), 
			   purpose:'saveSMTP'
		   },
			function(response){
				if(response)
				{
					arrResponse = response.split('~');
					showStickyErrorToast(arrResponse[0], '', 'success');
				}
			});	
		});
});
