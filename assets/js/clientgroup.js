$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtBx").val()=='')
			errors += "- Please Enter Client Group'";
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}

	$("#btnSave").click(function(){
		if(validate())
		{
			$('#statusLoader').ajaxStart(function() {
				$(this).show();
	
			}).ajaxStop(function() {
				$(this).hide();
			});
			var disableVal = 0;
			var iPrices = 0;
			var fPrices = 0;
			var sPrices = 0;
			if(document.getElementById('disable').checked)
				disableVal = 1;
			if(document.getElementById('iPrices').checked)
				iPrices = 1;
			if(document.getElementById('fPrices').checked)
				fPrices = 1;
			if(document.getElementById('sPrices').checked)
				sPrices = 1;
			$.post(base_url+"admin/clients/ajxclientgroup", 
				   {
					purpose:'save', 
					group:$('#txtBx').val(), 
					disableVal:disableVal, 
					iPrices:iPrices, 
					fPrices:fPrices, 
					sPrices:sPrices, 
					id:$('#id').val()
					}, 
				function(response){
					if(response)
					{
						arrResponse = response.split('~');
						if(arrResponse[1] == 1)
							$('#txtBx').val('');
						$('#txtBx').focus();
						showStickyErrorToast(arrResponse[0], '', 'success');
					}
				});	
		}
	});
});