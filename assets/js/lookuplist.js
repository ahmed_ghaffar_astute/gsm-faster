$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtBx").val()=='')
			errors += "- "+BE_32+" '"+$("#hdTdLbl").val()+"'";
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}

	$("#btnSave").click(function(){
		if(validate())
		{			
			$('#statusLoader').ajaxStart(function() {
				$(this).show();
	
			}).ajaxStop(function() {
				$(this).hide();
			});
			var disableVal = 0;
			var baseurl=document.getElementById("baseurl").innerHTML;

			if(document.getElementById('disable').checked)
				disableVal = 1;
			$.post(baseurl+"admin/miscellaneous/ajxlookuplist",
				   {
				   	//csrf_test_name:document.getElementsByName('csrf_test_name')[0].value,
				   	purpose:'add',
				   txtVal:$('#txtBx').val(), 
				   idField:$('#idField').val(), 
				   nameField:$('#nameField').val(), 
				   disableField:$('#disableField').val(), 
				   disableVal:disableVal, 
				   id:$('#id').val(), 
				   tblName:$('#tblName').val()}, 
				function(response){
					if(response)
					{
						arrResponse = response.split('~');
						if(arrResponse[1] == 1)
							$('#txtBx').val('');
						$('#txtBx').focus();
						showStickyErrorToast(arrResponse[0], '', 'success');
					}
				});	
		}
	});
});
