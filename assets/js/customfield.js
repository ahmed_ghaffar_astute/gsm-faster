$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtLbl").val()=='')
			errors = "-Please enter Field Label<br />";
		if($("#txtLbl").val() != '' && $('#id').val() == '0')
		{
			if(!alphanumeric($("#txtLbl").val(), true))
			{
				errors += "-ONLY USE CHARACTERS FROM A-Z OR DIGITS IN FIELD LABEL";
			}
		}
		if ($("#fieldType").val()=='0')
			errors += "-Please select Field Type";
		if($('#fieldType').val() == 'Text Box')
		{
			if (!isDigit($("#txtMinLen").val()))
				errors += "-Invalid value of Min Length<br />";
			if (!isDigit($("#txtMaxLen").val()))
				errors += "-Invalid value of Max Length<br />";
		}
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#fieldType").change(function(){
		if($('#fieldType').val() == 'Text Box')
		{
			$('#dvMaxLen').show();
			$('#dvMinLen').show();
		}
		else
		{
			$('#dvMaxLen').hide();
			$('#dvMinLen').hide();
		}
			
	});
	$("#btnSave").click(function(){
		if(validate())
		{			
			$('#dvLoader').ajaxStart(function() {
				$(this).show();
			}).ajaxStop(function() {
				$(this).hide();
			});
			var mandatory = 0;
			var disable = 0;
			var useAsQty = 0;
			if(document.getElementById('chkMandatory').checked)
				mandatory = 1;
			if(document.getElementById('chkDisable').checked)
				disable = 1;
			if(document.getElementById('chkAsQty').checked)
				useAsQty = 1;
				
			$.post(base_url+"admin/services/ajxcustomfield", 
			   {
				   fieldLbl:$('#txtLbl').val(), 
				   fieldType:$('#fieldType').val(), 
				   restriction:$('#restriction').val(), 
				   srvType:$('#srvType').val(), 
				   minLen:$('#txtMinLen').val(), 
				   maxLen:$('#txtMaxLen').val(), 
				   inst:$('#txtInst').val(), 
				   mandatory:mandatory,
				   useAsQty:useAsQty,
				   disable:disable,
				   purpose:'save',
				   id:$('#id').val()
			   },
				function(response){
					if(response)
					{
						arrResponse = response.split('~');
						if(arrResponse[1] == 1)
						{
						   $('#txtLbl').val('');
						   $('#fieldType').val('0');
						}
						showStickyErrorToast(arrResponse[0], '', 'success');
					}
				});	
		}
	});
});