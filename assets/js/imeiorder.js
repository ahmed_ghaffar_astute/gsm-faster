$(document).ready(function()
{
	fetchData();
	function calculateLastIMEIDigit(imei)
	{
		if(imei.length < 14)
			return '';
		if (!isDigit(imei))
			return '';
	
		var j = 0;
		var k = 0;
		var numbersLeft = '';
		var toDouble = '';
		var doubled = '';
		var totalSum = 0;
		for(var i=imei.length-1; i>=0; i--)
		{
			if(i%2 == 0)
			{
				numbersLeft+= imei[i];
				j++;
			}
			else
			{
				toDouble += imei[i];
				k++;
			}
		}
		for(var i=0; i<toDouble.length; i++)
		{
			doubled += toDouble[i] * 2;
		}
		for(var i=0; i<doubled.length; i++)
		{
			totalSum += parseInt(doubled[i]);
		}
		for(var i=0; i<numbersLeft.length; i++)
		{
			totalSum += parseInt(numbersLeft[i]);
		}
		var lastDigit = totalSum % 10;
		if(lastDigit > 0)
			lastDigit = parseInt(10 - lastDigit);
		return lastDigit;
	}
	function checkImei(val)
	{
		if(val.length < 15)
			return false;
		return true;
	}
	function checkImeiLesserThan14(val)
	{
		if(val.length < 14)
			return false;
		return true;
	}
	function pk_fixnewlines_textarea (val)
	{
		// Adjust newlines so can do correct character counting for MySQL. MySQL counts a newline as 2 characters.
		if (val.indexOf('\r\n')!=-1)
			val = ''; // this is IE on windows. Puts both characters for a newline, just what MySQL does. No need to alter
		else if (val.indexOf('\r')!=-1)
			val = val.replace (/\r/g, '');        // this is IE on a Mac. Need to add the line feed
		else if (val.indexOf('\n')!=-1)
			val = val.replace ( /\n/g, '');        // this is Firefox on any platform. Need to add carriage return
		else 
			val = '';                                           // no newlines in the textarea  
		return val;
	}

	function validate()
	{
		var errors = '';
		if ($("#ddType").val()=='1' && $("#categoryId").val()=='0')
			errors += "- Please select Category.<br />";
		if ($("#packageId").val()=='0')
			errors += "- "+CUST_CODE_1+".<br />";
		if($("#imeiFType").val() == '0')
		{
			if(document.getElementById('rdIMEIType1').checked)
			{
				if($("#chkSm").val() == '1')
					var myIMEI = $("#txtIMEI").val()+$("#txtIMEILastDigit").val();
				else
					var myIMEI = $("#txtIMEI").val();
				if (!checkImei(myIMEI))
					errors += "- "+CUST_CODE_3+".<br />";
				
				if(myIMEI != '')
				{
					if(!isDigit(myIMEI))
						errors += "- "+CUST_CODE_4+".<br />";
				}
			}
			else
			{
				if (!checkImei($("#imei").val()))
					errors += "- "+CUST_CODE_3+".<br />";
				var arrIMEI = $("#imei").val().split('\n');
				var counter = 1;
				var myIMEI = '';
				for(var j = 0; j < arrIMEI.length; j++)
				{
					var myIMEI = Trim(arrIMEI[j]);
					if(myIMEI != '')
					{
						if(!isDigit(myIMEI))
							errors += "- "+CUST_CODE_5+" "+counter+" "+CUST_CODE_6+".<br />";
						if(myIMEI.length != 15)
							errors += "- "+CUST_CODE_7+ " " +counter+" "+CUST_CODE_8+".<br />";
					}
					counter++;
				}
			}
		}
		else if($("#imeiFType").val() == '1')
		{
			if($("#chkSm").val() == '1')
				var myIMEI = $("#txtIMEI").val()+$("#txtIMEILastDigit").val();
			else
				var myIMEI = $("#txtIMEI").val();
			if (!checkImei(myIMEI))
				errors += "- "+CUST_CODE_3+".<br />";
			
			if(myIMEI != '')
			{
				if(!isDigit(myIMEI))
					errors += "- "+CUST_CODE_4+".<br />";
			}
		}
		else if($("#imeiFType").val() == '2')
		{
			if (!checkImei($("#imei").val()))
				errors += "- "+CUST_CODE_3+".<br />";
			var arrIMEI = $("#imei").val().split('\n');
			var counter = 1;
			var myIMEI = '';
			for(var j = 0; j < arrIMEI.length; j++)
			{
				var myIMEI = Trim(arrIMEI[j]);
				if(myIMEI != '')
				{
					if(!isDigit(myIMEI))
						errors += "- "+CUST_CODE_5+" "+counter+" "+CUST_CODE_6+".<br />";
					if(myIMEI.length != 15)
						errors += "- "+CUST_CODE_7+ " " +counter+" "+CUST_CODE_8+".<br />";
				}
				counter++;
			}
		}
		var totalFields = $("#hdTotalTextFields").val();
		for(i=0; i<totalFields; i++)
		{
			if ($("#txtPckgField"+i).val()=='')
				errors += "- "+CUST_CODE_9+" '"+$("#lblPckgField"+i).html()+"'.<br />";
		}
		if($("#brndVldtn").val() == '1')
		{
			if($("#brandId").val() == '0')
				errors += "- Please Select Brand<br />";			
		}
		if (errors=='')
		{
			$("#submitButton").attr("disabled", true);
			$("#submitButton").text('Please Wait...');
			return true;
		}
		else
		{
			$("#dvError").html(errors);
			$("#dvError").show();
			return false;
		}
	}
	$("#categoryId").change(function()
	{
		setValue('0');
		document.getElementById('frm').submit();
	});
	$("#frm").submit(function(){
		return validate();
	});
	$("#txtIMEI").keyup(function()
	{
		if($("#chkSm").val() == '1')
		{
			var lastDigit = calculateLastIMEIDigit($("#txtIMEI").val());
			$("#txtIMEILastDigit").val(lastDigit);
		}
	});
	$("#rdIMEIType1").click(function(){
		if(document.getElementById('rdIMEIType1').checked)
		{
			$("#dvSIMEI").show();
			$("#dvMIMEI").hide();
		}
	});
	$("#rdIMEIType2").click(function(){
		if(document.getElementById('rdIMEIType2').checked)
		{
			$("#dvMIMEI").show();
			$("#dvSIMEI").hide();
		}
	});
	$("#packageId").change(function()
	{
		if(document.getElementById('msg1'))
			$("#msg1").hide();
		if(document.getElementById('msg2'))
			$("#msg2").hide();
		if(document.getElementById('msg3'))
			$("#msg3").hide();
		if(document.getElementById('msg4'))
			$("#msg4").hide();
		fetchData();
		$("#dvError").hide();
	});
	$("#imei").blur(function()
	{
		var arrIMEI = $("#imei").val().split('\n');
		var counter = 1;
		var myIMEI = '';
		var strIMEIs = '';
		var lastDigit = '';
		for(var j = 0; j < arrIMEI.length; j++)
		{
			var myIMEI = Trim(arrIMEI[j]);
			if(myIMEI != '')
			{
				if(myIMEI.length == 14 && isDigit(myIMEI))
				{
					lastDigit = calculateLastIMEIDigit(myIMEI);
					strIMEIs += myIMEI+lastDigit+'\n';
				}
				else
					strIMEIs += myIMEI+'\n';
				
			}
			counter++;
			$("#imei").val(strIMEIs);
		}
	});
	$("#brandId").change(function()
	{
		$('#spnBrLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		$.post("include/ajax/modules/ajximeiorder.php", 
		   {
			   brandAPIId:$('#brandAPIId').val(), 
			   packId:$('#packageId').val(),
			   brandId:$('#brandId').val(), 
			   purpose:'getMdls'
		   },
			function(response){
			if(response)
			{
				arrResponse = response.split('~GSMF~');
				if(arrResponse[0] != '')
				{
					$("#modelId").html(arrResponse[0]);
					$('#mdlVal').val(arrResponse[1]);
					$("#dvModels").show();
				}
				else
				{
					$("#dvModels").hide();
				}
			}
		});	
	});
	$("#modelId").change(function()
	{
		$('#mdlVal').val(document.getElementById('modelId').options[document.getElementById('modelId').selectedIndex].text);
	});

	function fetchData()
	{
		if($('#packageId').val() > 0)
		{
			$('#spnLoader').ajaxStart(function() {
				$(this).show();
			}).ajaxStop(function() {
				$(this).hide();
			});
			$.post(base_url+"page/ajximeiorder", 
			   {
				   packId:$('#packageId').val(),
				   usrCurrId:$('#usrCurrId').val(),
				   cnvrsnRt:$('#cnvrsnRt').val(),
	   			   themeStyle:$('#themeStyle').val(),
				   purpose:'getData'
			   },
				function(response){
				if(response)
				{
					arrResponse = response.split('~');
					if(Trim(arrResponse[15]) != '')
					{
						var win = window.open(arrResponse[15], '_blank');
						//window.open(arrResponse[15], '_blank');
					}
					$('#dvCstmFlds').html(arrResponse[14]);
					document.getElementById('cldFm').value = '0';
					$('#tblInfo').show();

					var mustRead = arrResponse[0];
					if(mustRead != '')
					{
						$('#dvMR').html("<b>SERVICE DETAILS:</b><br />"+mustRead);
						$('#dvMR').show();
					}
					if(document.getElementById('service_Details2'))
					{
						$('#service_Details2').hide();
						$('#serviceDetails2').show();
					}
					$('#dvSN').html(arrResponse[3]);
					$('#dvTT').html('Delivery Time: '+arrResponse[1]);
					$('#hdAPIId').val(arrResponse[2]);
					//$('#hdPckTitle').val(arrResponse[3]);
					$('#hdDupIMEI').val(arrResponse[5]);
					$('#hdPreOrder').val(arrResponse[9]);
					$('#extNtwrkId').val(arrResponse[11]);
					$('#brandAPIId').val(arrResponse[12]);
					//$('#lblCustPrice').html($('#currSymbol').val()+arrResponse[13]+$('#currAbb').val());
					$('#lblCustPrice').html($('#currSymbol').val()+arrResponse[13]);
					if(arrResponse[6] == '0')
					{
						$('#dvIMEIOptions').show();
						$('#dvSIMEI').show();
						$('#dvMIMEI').hide();
						$('#dvCustomFld').hide();
					}
					else if(arrResponse[6] == '1')
					{
						$('#dvIMEIOptions').hide();
						$('#dvSIMEI').show();
						$('#dvMIMEI').hide();
						$('#dvCustomFld').hide();
					}
					else if(arrResponse[6] == '2')
					{
						$('#dvIMEIOptions').hide();
						$('#dvSIMEI').hide();
						$('#dvCustomFld').hide();
						$('#dvMIMEI').show();
					}
					else if(arrResponse[6] == '3' && arrResponse[4] != '')
					{
						$('#dvIMEIOptions').hide();
						$('#dvSIMEI').hide();
						$('#dvMIMEI').hide();
						$('#dvCustomFld').html(arrResponse[4]);
						$('#dvCustomFld').show();
					}
					if($('#chkSm').val() == '1')
						$('#txtIMEILastDigit').show();
					else
						$('#txtIMEILastDigit').hide();
					$('#imeiFType').val(arrResponse[6]);
					if(arrResponse[10] != '')
					{
						$("#brandId").html(arrResponse[10]);
						$("#dvBrands").show();
						$('#brndVldtn').val('1');
					}
					else
					{
						$("#dvBrands").hide();
						$('#brndVldtn').val('0');
					}
					if(arrResponse[7] != '')
					{
						document.getElementById('imgSrv').src = arrResponse[7];
						$('#dvSrvImg').show();
					}
					else
					{
						document.getElementById('imgSrv').src = '';
						$('#dvSrvImg').hide();
					}
					if(Trim(arrResponse[16]) == '1')// Terms of Conditions
					{
						$('#dvTOC').show();
					}
					else
					{
						document.getElementById('chkTOC').checked = false;
						$('#dvTOC').hide();
					}
					$('#hdTOC').val(arrResponse[16]);

					if(Trim(arrResponse[17]) != '')
					{
						$('#dvFeatures').html(Trim(arrResponse[17]));
						$('#dvFeatures').show();
					}
					else
					{
						$('#dvFeatures').html('');
						$('#dvFeatures').hide();
					}
					$('#customFldMaxLen').val(arrResponse[18]);
					$('#custFldRstrctn').val(arrResponse[19]);
					$('#customFldMinLen').val(arrResponse[20]);
				}
			});	
		}
	}

});
function setValue(i)
{
	document.getElementById('cldFrm').value = i;
}