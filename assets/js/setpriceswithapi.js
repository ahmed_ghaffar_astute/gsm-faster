$(document).ready(function()
{
	$("#btnSave").click(function(){
		$('#statusLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		var groups = $('#ttlGroups').val();
		var prices = Trim($('#txtPrice0').val());
		var plans = Trim($('#serviceId').val());
		var marginType = Trim($('#marginType0').val());
		for(var a=1; a<= groups; a++)
		{
			if(isFloat($('#txtPrice'+a).val()))
			{
				prices = prices + ',' + Trim($('#txtPrice'+a).val());
				plans = plans + ',' + Trim($('#groupId'+a).val());
				marginType = marginType + ',' + $('#marginType'+a).val();
			}
		}

		$.post(base_url+"admin/services/ajxpriceswithapi", 
		   {
			   srvcAPIId:$('#srvcAPIId').val(), 
			   serviceId:$('#serviceId').val(), 
			   sType:$('#sType').val(), 
			   plans:plans, 
			   prices:prices, 
			   marginType:marginType, 
			   dPrice:Trim($('#txtPrice0').val()), 
			   dMT:Trim($('#marginType0').val()), 
			   purpose:'save'
		   },
			function(response){
				if(response)
				{
					showStickyErrorToast(response, '', 'success');
				}
			});	
	});
});