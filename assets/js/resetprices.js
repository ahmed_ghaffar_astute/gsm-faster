$(document).ready(function()
{
	function validate()
	{
		var service = 'IMEI';
		if($('#srvc').val() == '1')
			service = 'FILE';
		else if($('#srvc').val() == '2')
			service = 'SERVER';
		return confirm('Are you sure you want to reset the '+service+' services prices?');
	}
	$("#chkGroupPrices").click(function()
	{
		if(document.getElementById('chkGroupPrices').checked)
			$("#dvGroups").show();
		else
			$("#dvGroups").hide();
	});
	$("#categoryId").change(function()
	{
		if($("#categoryId").val() > 0)
		{
			$("#dvServices").show();
		}
		else
		{
			$("#dvServices").hide();
		}
		$('#imgSrvcLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		$.post(base_url+"admin/services/ajxgeneral", 
		   {
			   categoryId:$("#categoryId").val(), 
			   sc:$("#srvc").val(), 
			   purpose:'getsrvcs'
		   },
			function(response){
				if(response)
				{
					$("#serviceId").html('<option value="0" selected>Please Choose Service</option>'+response);
				}
			});	
	});
	$("#btnSave").click(function(){
		if(validate())
		{			
			$('#statusLoader').ajaxStart(function() {
				$(this).show();
			}).ajaxStop(function() {
				$(this).hide();
			});
			var groupPrices = 0;
			if(document.getElementById('chkGroupPrices').checked)
				groupPrices = 1;
			$.post(base_url+"admin/services/ajxresetprices", 
			   {
				   groupPrices:groupPrices, 
				   purpose:'update',
				   categoryId:$('#categoryId').val(), 
				   serviceId:$('#serviceId').val(), 
				   groupId:$('#groupId').val(), 
				   srvc:$('#srvc').val()
			   },
				function(response){
					if(response)
					{
						showStickyErrorToast(response, '', 'success');
					}
				});	
		}
	});
});