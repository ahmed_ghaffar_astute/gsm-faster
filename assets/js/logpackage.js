function savePrices()
{
	$("#hdSavePrices").val('1');
}
function calculatePrices()
{
	if($("#id").val() == '0')
	{
		convertPrices();
	}
}
function convertPrices()
{
	if (isFloat($("#txtPrice").val()))
	{
		var totalCurrencies = $("#totalCurrencies").val();
		var price = $("#txtPrice").val();
		var conversionRate = 0;
		var i = 0;
		for(i = 0; i < totalCurrencies; i++)
		{
			var converterPrice = 0;
			converstionRate = $("#converstionRate"+i).val();
			converterPrice = parseFloat(price * converstionRate);
			$("#txtCurrPrice"+i).val(Round(converterPrice));	
		}
	}
}
function showHideNewTpl()
{
	if (document.getElementById('rdNewTplType0').checked)
		$("#dvNewOrderTpl").hide();
	else if (document.getElementById('rdNewTplType1').checked)
		$("#dvNewOrderTpl").show();
}
function showHideSucTpl()
{
	if (document.getElementById('rdSuccessTplType0').checked)
		$("#dvSucOrderTpl").hide();
	else if (document.getElementById('rdSuccessTplType1').checked)
		$("#dvSucOrderTpl").show();
}
function showHideCanTpl()
{
	if (document.getElementById('rdCanTplType0').checked)
		$("#dvCanOrderTpl").hide();
	else if (document.getElementById('rdCanTplType1').checked)
		$("#dvCanOrderTpl").show();
}
$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtTitle").val()=='')
			errors += "- "+BE_51+"<br />";
		if($("#dupSrvcId").val() == '0')
		{
			if ($("#categoryId").val() == '0')
				errors += "- "+BE_4+"<br />";
			if ($("#txtPrice").val()=='')
				errors += "- "+BE_52+"<br />";
			if ($("#txtPrice").val()!='')
			{
				if (!isFloat($("#txtPrice").val()))
					errors += "- "+BE_53;
			}
			if(document.getElementById('rdServiceType0').checked)
			{
				if ($("#txtResDelayTm").val()=='')
					errors += "- Please Enter Response Delay Time<br />";
				if (!isDigit($("#txtResDelayTm").val()))
					errors += "- Invalid value of Response Delay Time";
			}
			if ($("#txtCancelTime").val() != '')
			{
				if (!isDigit($("#txtCancelTime").val()))
					errors += "- Invalid value for Time to Cancel Orders<br />";
			}
			if ($("#txtVerifyTime").val() != '')
			{
				if (!isDigit($("#txtVerifyTime").val()))
					errors += "- Invalid value for Time to Verfiy Orders";
			}
		}
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#rdServiceType0").click(function(){
		if(document.getElementById('rdServiceType0').checked)
		{
			$('#dvDelayTm').show();
		}
		else
		{
			$('#dvDelayTm').hide();
		}
	});
	$("#rdServiceType1").click(function(){
		if(document.getElementById('rdServiceType1').checked)
		{
			$('#dvDelayTm').hide();
		}
		else
		{
			$('#dvDelayTm').hide();
		}
	});
	$("#frm").submit(function(){
		return validate();
	});
	$("#btnPriceEml").click(function()
	{
		$('#imgEmlLdr').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		$.post(base_url+"admin/services/ajxpackage", 
		   {
			   id:$("#id").val(), 
			   sc:2, 
			   notes:$("#txtUserNotes").val(),
			   groupId:$("#uNplanId").val(),
			   purpose:'sendsrvcemail'
		   },
			function(response){
				if(response)
				{
					showStickyErrorToast(response, '', 'success');
				}
			});	
	});
	$("#dupSrvcId").change(function()
	{
		if($("#dupSrvcId").val() == '0')
			$("#dvGeneral").show();
		else
			$("#dvGeneral").hide();
	});
	$("#apiId").change(function()
	{
		var arr = $("#apiId").val().split('~');
		var apiId = arr[0];
		var extId = arr[1];
		var apiType = arr[2];
		$("#dvExtNetworkId").show();
		getSupplierServices(apiId);
	});
	$("#supplierPackId").change(function()
	{
		var arr = $("#apiId").val().split('~');
		var apiId = arr[0];
//		$("#dvExtNetworkId").show();
		getServiceTypes(apiId);
	});
	$("#chkCnvrtPrice").click(function()
	{
		if(document.getElementById('chkCnvrtPrice').checked)
		{
			var totalGroups = $("#totalGroups").val();
			var currencyCount = $("#currencyCount").val();
			var j = 0;
			for(j = 0; j < totalGroups; j++)
			{
				var price = Trim($("#txtPrice"+j+"_0").val());
				for(var z = 1; z <= currencyCount; z++)
				{
					var cRate = Trim($("#cRt"+j+"_"+z).val());
					if(isFloat(price) && isFloat(cRate))
					{
						var convertedPrice = Round(price * cRate);
						$("#txtOtherPrice"+j+"_"+z).val(convertedPrice);
					}
				}
			}
			
		}
	});
	$("#lnkRmvAltAPI").click(function()
	{
		if(confirm ('Are you sure you want to remove alternate API?'))
		{
			$('#rmvAltAPILdr').ajaxStart(function() {
				$('#rmvAltAPILdr').show();
			}).ajaxStop(function() {
				$('#rmvAltAPILdr').hide();
			});
			$.post(base_url+"admin/services/ajxaltapi", 
			   {
				   id:$('#id').val(), 
				   sc:2,
				   purpose:'rmv'
			   },
				function(response){
					if(response)
					{
						showStickyErrorToast(response, '', 'success');
						$("#lnkRmvAltAPI").hide();
					}
				});	
		}
		else
			return;
	});
	$('#chkFetchCPr').click(function () {
		if ($(this).attr('checked'))
		{
			$('#imgCPLdr').ajaxStart(function() {
				$('#imgCPLdr').show();
			}).ajaxStop(function() {
				$('#imgCPLdr').hide();
			});
			$.post(base_url+"admin/services/ajxcostprice.php", 
			   {
				   apiId:$('#exAPIId').val(), 
				   srvcId:$("#exAPIServiceId").val(),
				   packageId:$("#id").val(),
				   serviceType:2,
				   purpose:'getCP'
			   },
				function(response){
					if(response)
					{
					   $('#txtCostPrice').val(response);
					}
				});	
		}
	})
	function getSupplierServices(apiId)
	{
		$('#imgSrvcLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		$.post(base_url+"admin/services/ajxpackage", 
		   {
			   apiId:apiId, 
			   serviceType:2, 
			   purpose:'getsuppliersrvcs'
		   },
			function(response){
				if(response)
				{
					$("#supplierPackId").html(response);
				}
			});	
		
	}
	function getServiceTypes(apiId)
	{
		$('#imgSrvcTypeLoader').ajaxStart(function() {
			$('#imgSrvcTypeLoader').show();
		}).ajaxStop(function() {
			$('#imgSrvcTypeLoader').hide();
		});
		$.post(base_url+"admin/services/ajxpackage", 
		   {
			   apiId:apiId, 
			   serviceType:2, 
			   serviceId:$("#supplierPackId").val(), 
			   purpose:'getservicetypes'
		   },
			function(response){
				if(response)
				{
					var arr = response.split('</script>');
					if(arr[1] != '0')
					{
						//$msg = "<option value='0'>Choose Service Type</option>";
						$("#apiServiceTypeId").html("<option value='0'>Choose Service Type</option>"+arr[1]);
						$('#dvAPIServiceType').show();
					}
					else
					{
						$('#dvAPIServiceType').hide();
					}
					
				}
			});	
		
	}
});