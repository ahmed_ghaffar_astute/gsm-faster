var CUST_ERR = "Fehler";

var CUST_CODE_1 = "Bitte wählen Dienst";
var CUST_CODE_2 = "Bitte geben Sie PIN-Code";
var CUST_CODE_3 = "Geben Sie 15-stellige IMEI #";
var CUST_CODE_4 = "Ungültige IMEI #";
var CUST_CODE_5 = "Geben Sie IMEI #";
var CUST_CODE_6 = "in Ziffern";
var CUST_CODE_7 = "Imei in Zeile eingegeben";
var CUST_CODE_8 = "ist 15-stellig";
var CUST_CODE_9 = "Bitte geben Sie";
var CUST_CODE_10 = "Bitte geben Sie Land";

var CUST_BC_1 = "Bitte geben Sie Credits";
var CUST_BC_2 = "Ungültigen Wert von Credits";

var CUST_PWD_1 = "Bitte altes Passwort";
var CUST_PWD_2 = "Bitte neues Passwort";
var CUST_PWD_3 = "Bitte geben Sie Kennwort bestätigen";
var CUST_PWD_4 = "Passwörter stimmen nicht überein";
//============
var CUST_LBL_1 = "Bitte wählen Sie Datei";
var CUST_LBL_2 = "Bitte wählen Sie Service-";
var CUST_LBL_3 = "Bitte geben Sie Box S / N";
var CUST_LBL_4 = "Bitte geben Sie Seriennumme";
var CUST_LBL_5 = "Bitte geben Sie Benutzername Box";
var CUST_LBL_6 = "Bitte geben Sie 'Credits'";
var CUST_LBL_7 = "Ungültige Wert von 'Credits'";
var CUST_LBL_8 = "Bitte wählen Sie 'Zahlungsstatus '";
var CUST_LBL_9 = "Please select 'Payment Method'";
var CUST_LBL_10 = "Bitte wählen Sie Zip-Datei";
var CUST_LBL_11 = "Bitte wählen Sie Bulk-Dateien";
var CUST_LBL_12 = "Bitte geben Sie Benutzername";
var CUST_LBL_13 = "Bitte geben Sie Adresse";
var CUST_LBL_14 = "Bitte geben Sie Stadt";
var CUST_LBL_15 = "Bitte geben Sie Staat";
var CUST_LBL_16 = "Bitte geben Sie Postleitzahl";