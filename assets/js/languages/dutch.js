var CUST_ERR = "fouten";

var CUST_CODE_1 = "Selecteer Pakket";
var CUST_CODE_2 = "Geef Model No.";
var CUST_CODE_3 = "Geef 15-cijferige Imei#";
var CUST_CODE_4 = "ongeldige Imei#";
var CUST_CODE_5 = "Geef Imei#";
var CUST_CODE_6 = "in cijfers";
var CUST_CODE_7 = "Imei die in lijn#";
var CUST_CODE_8 = "is niet van 15 cijfers";
var CUST_CODE_9 = "geef";
var CUST_CODE_10 = "Geef Country";

var CUST_BC_1 = "Geef Credits";
var CUST_BC_2 = "Ongeldige waarde van Credits";

var CUST_PWD_1 = "Vul Oud wachtwoord";
var CUST_PWD_2 = "Vul Nieuw wachtwoord";
var CUST_PWD_3 = "Vul Password Confirm";
var CUST_PWD_4 = "Wachtwoorden komen niet overeen";
var CUST_LBL_1 = "Selecteer Bestand";