var CUST_ERR = "خطاها";

var CUST_CODE_1 = "لطفا انتخاب کنید بسته بندی";
var CUST_CODE_2 = "لطفا شماره مدل را وارد کنید";
var CUST_CODE_3 = "را وارد کنید 15 رقمی IMEI #";
var CUST_CODE_4 = "نامعتبر # IMEI";
var CUST_CODE_5 = "را وارد کنید IMEI #";
var CUST_CODE_6 = "در رقم";
var CUST_CODE_7 = "IMEI وارد شده در خط #";
var CUST_CODE_8 = "از 15 رقم نیست";
var CUST_CODE_9 = "لطفا";
var CUST_CODE_10 = "لطفا برای ورود به کشور";

var CUST_BC_1 = "لطفا اعتبار";
var CUST_BC_2 = "مقدار نامعتبر اعتبارات";

var CUST_PWD_1 = "لطفا رمز عبور را وارد کنید";
var CUST_PWD_2 = "لطفا رمز عبور جدید را وارد کنید";
var CUST_PWD_3 = "تکرار رمز عبور را وارد کنید";
var CUST_PWD_4 = "رمزهای عبور با هم مطابقت ندارند";
//============
var CUST_LBL_1 = "لطفا برای انتخاب فایل";
var CUST_LBL_2 = "لطفا برای بسته بندی ورود را انتخاب کنید";
var CUST_LBL_3 = "لطفا جعبه S / N";