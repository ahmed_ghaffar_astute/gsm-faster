var CUST_ERR = "שגיאות";

var CUST_CODE_1 = "אנא בחר חבילה";
var CUST_CODE_2 = "אנא הכנס מספר דגם";
var CUST_CODE_3 = "הזן את 15 ספרות IMEI #";
var CUST_CODE_4 = "חוקי IMEI #";
var CUST_CODE_5 = "הזן IMEI #";
var CUST_CODE_6 = "ב ספרות";
var CUST_CODE_7 = "IMEI נכנס בתור #";
var CUST_CODE_8 = "הוא לא של 15 ספרות";
var CUST_CODE_9 = "אנא הכנס";
var CUST_CODE_10 = "אנא בחרו מדינה";

var CUST_BC_1 = "אנא הכנס קרדיטים";
var CUST_BC_2 = "ערך לא חוקי של קרדיטים";

var CUST_PWD_1 = "אנא הכנס סיסמה ישנה";
var CUST_PWD_2 = "אנא הכנס סיסמה חדשה";
var CUST_PWD_3 = "אנא הכנס אישור סיסמה";
var CUST_PWD_4 = "הסיסמאות לא תואמות";
//============
var CUST_LBL_1 = "נא לבחור קובץ";
var CUST_LBL_2 = "בבקשה בחר Package התחבר";