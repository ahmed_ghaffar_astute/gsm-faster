var CUST_ERR = "Errors";

var CUST_CODE_1 = "Xin vui lòng lựa chọn dịch vụ";
var CUST_CODE_2 = "Xin vui lòng nhập mã Pin";
var CUST_CODE_3 = "Nhập 15 chữ số của số Imei #";
var CUST_CODE_4 = "Số Imei không đúng#";
var CUST_CODE_5 = "Nhập số IMEI #";
var CUST_CODE_6 = "bằng các chữ số";
var CUST_CODE_7 = "Imei nhập vào theo hàng #";
var CUST_CODE_8 = "không đúng 15 chữ số";
var CUST_CODE_9 = "Xin hãy nhập vào";
var CUST_CODE_10 = "Xin hãy nhập vào tên quốc gia";

var CUST_BC_1 = "Xin hãy nhập vào số Credit";
var CUST_BC_2 = "Giá trị của Credit không đúng";

var CUST_PWD_1 = "Xin hãy nhập vào mật khẩu cũ";
var CUST_PWD_2 = "Xin hãy nhập vào mật khẩu mới";
var CUST_PWD_3 = "Xin hãy nhập vào mật khẩu xác nhận";
var CUST_PWD_4 = "Mật khẩu không khớp nhau";
//============
var CUST_LBL_1 = "Xin vui lòng chọn File";
var CUST_LBL_2 = "Xin vui lòng chọn dịch vụ";
var CUST_LBL_3 = "Xin vui lòng nhập vào số seri của Box";
var CUST_LBL_4 = "Xin vui lòng nhập số seri";
var CUST_LBL_5 = "Xin vui lòng nhập Tên đăng nhập của Box";
var CUST_LBL_6 = "Xin vui lòng nhập số'Credits'";
var CUST_LBL_7 = "Giá trị 'Credits' không đúng";
var CUST_LBL_8 = "Xin vui lòng chọn 'Trạng thái thanh toán'";
var CUST_LBL_9 = "Xin vui lòng chọn 'Phương thức thanh toán'";
var CUST_LBL_10 = "Xin vui lòng chọn file nén (Zip)";
var CUST_LBL_11 = "Xin vui lòng chọn nhiều file";
var CUST_LBL_12 = "Xin vui lòng nhập Tên đăng nhập";
var CUST_LBL_13 = "Xin vui lòng nhập Địa chỉ";
var CUST_LBL_14 = "Xin vui lòng nhập Thành phố";
var CUST_LBL_15 = "Xin vui lòng nhập thông tin Tỉnh";
var CUST_LBL_16 = "Xin vui lòng nhập mã vùng";
var CUST_LBL_17 = "Xin vui lòng nhập số lượng";