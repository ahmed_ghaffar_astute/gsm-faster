var CUST_ERR = "Fel";

var CUST_CODE_1 = "Välj service";
var CUST_CODE_2 = "Ange PIN-kod";
var CUST_CODE_3 = "Ange 15-siffriga IMEI#";
var CUST_CODE_4 = "Ogiltig IMEI#";
var CUST_CODE_5 = "Ange IMEI#";
var CUST_CODE_6 = "i siffror";
var CUST_CODE_7 = "IMEI trädde i linje#";
var CUST_CODE_8 = "är inte av 15 siffror";
var CUST_CODE_9 = "Ange";
var CUST_CODE_10 = "Ange Land";

var CUST_BC_1 = "Ange Credits";
var CUST_BC_2 = "Ogiltigt värde Credits";

var CUST_PWD_1 = "Ange Gammalt lösenord";
var CUST_PWD_2 = "Ange nytt lösenord";
var CUST_PWD_3 = "Ange Bekräfta lösenord";
var CUST_PWD_4 = "Lösenorden matchar inte";
//============
var CUST_LBL_1 = "Välj Arkiv";
var CUST_LBL_2 = "Välj service";
var CUST_LBL_3 = "Ange Box S / N";
var CUST_LBL_4 = "Ange Serienummer.";
var CUST_LBL_5 = "Ange Box Användarnamn";
var CUST_LBL_6 = "Ange Credits";
var CUST_LBL_7 = "Ogiltigt värde för Credits";
var CUST_LBL_8 = "Välj betalningsstatus";
var CUST_LBL_9 = "Välj Betalningsmetod";
var CUST_LBL_10 = "Välj Zip-fil";
var CUST_LBL_11 = "Välj Bulk filer";
var CUST_LBL_12 = "Ange användarnamn";
var CUST_LBL_13 = "Ange adress";
var CUST_LBL_14 = "Ange stad";
var CUST_LBL_15 = "Ange staten";
var CUST_LBL_16 = "Ange postnummer";
var CUST_LBL_17 = "Ange Kvantitet";