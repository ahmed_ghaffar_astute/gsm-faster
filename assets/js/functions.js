function isValidEmail(email)
{
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	if(reg.test(email) == false)
		return false;
   return true;
}
function checkEmailAjx(email)
{
	var msg = '';
	if(Trim(email)=="")
		msg = "Please enter e-mail address.";
	else
	{
		mail=email
		at_pos=mail.indexOf("@")
		dot_pos=mail.indexOf(".")
		if(at_pos<1 || dot_pos<1)
			msg = "Please enter valid email address.";
		else
		{
			mail=email
			condition="yes"
			var at_count=0
			var dot_count=0
			var temp=0
			for(var i=0;i<mail.length;i++)
			{
				if((mail.charCodeAt(i)>0 && mail.charCodeAt(i)<48)||(mail.charCodeAt(i)>57 && mail.charCodeAt(i)<65)||(mail.charCodeAt(i)>91 && mail.charCodeAt(i)<97)||mail.charCodeAt(i)>122)
				{
					if(mail.charAt(i)=="@"||mail.charAt(i)==".")
					{
							if(mail.charAt(i)=="@"){at_count++}else{dot_count++} // counts the no. of times @ and . appears in email
							if(dot_count>=1)
							{
								dot_pos=i
								if((dot_pos>at_pos) && temp==0)
								{
									pos=dot_pos-at_pos
									temp++
								}								
							}
					}
					else
					{
						condition="no"
						i=mail.length
					}
				}
			}
			if(condition=="no")
				msg = "Your email contains a blank space or special character.";
			else
			{
				if(at_count>1)
					msg = "E-mail contains extra @ ";
				else
				{
					if(pos<2)
					{
						msg = "Missing domain name between '@' and '.'";			
						i=mail.length
					}
					else
					{	
						count=dot_pos+1
						domain=""
						for(count;count<mail.length;count++)
						{
							domain=domain+mail.charAt(count)		
						}
						dom=new Array("au","com","net","org","edu","in","mil","gov","arpa","biz","aero","name","coop","info","pro","museum")
						error="yes"
						for(var k=0;k<dom.length;k++)
						{
							if(domain==dom[k])
							{
								k=dom.length
								error="no"
							}
						}
						if((error=="yes" && (domain.length>2)) || (domain.length<2))
							msg = "Domain name must end with well known domains \n or 2-lettered country name. eg com,edu,in etc.";
					}
				}
			}
		}
	}
	return msg;
}

function checkEmail(emailCntrl)
{
	ok=true
	if(emailCntrl.value=="")
	{
		alert("Please enter e-mail address.")
		emailCntrl.focus()
		ok=false
	}
	else
	{
		mail=emailCntrl.value
		at_pos=mail.indexOf("@")
		dot_pos=mail.indexOf(".")
		if(at_pos<1 || dot_pos<1)
		{
			alert("Please enter valid email address.")
			emailCntrl.focus()
			ok=false
		}
		else
		{
			mail=emailCntrl.value
			condition="yes"
			var at_count=0
			var dot_count=0
			var temp=0
			for(var i=0;i<mail.length;i++)
			{
				if((mail.charCodeAt(i)>0 && mail.charCodeAt(i)<48)||(mail.charCodeAt(i)>57 && mail.charCodeAt(i)<65)||(mail.charCodeAt(i)>91 && mail.charCodeAt(i)<97)||mail.charCodeAt(i)>122)
				{
					if(mail.charAt(i)=="@"||mail.charAt(i)==".")
					{
							if(mail.charAt(i)=="@"){at_count++}else{dot_count++} // counts the no. of times @ and . appears in email
							if(dot_count>=1)
							{
								dot_pos=i
								if((dot_pos>at_pos) && temp==0)
								{
									pos=dot_pos-at_pos
									temp++
								}								
							}
					}
					else
					{
						condition="no"
						i=mail.length
					}
				}
			}
			if(condition=="no")
			{
				alert("Your email contains a blank space or special character.")
				emailCntrl.focus()
				ok=false
			}
			else
			{
				if(at_count>1)
				{
					alert("E-mail contains extra @ ")
					emailCntrl.focus()
					ok=false
				}
				else
				{
					if(pos<2)
					{
						alert("Missing domain name between '@' and '.'")
						emailCntrl.focus()
						ok=false
						i=mail.length
					}
					else
					{	
						count=dot_pos+1
						domain=""
						for(count;count<mail.length;count++)
						{
							domain=domain+mail.charAt(count)		
						}
						dom=new Array("au","com","net","org","edu","in","mil","gov","arpa","biz","aero","name","coop","info","pro","museum")
						error="yes"
						for(var k=0;k<dom.length;k++)
						{
							if(domain==dom[k])
							{
								k=dom.length
								error="no"
							}
						}
						if((error=="yes" && (domain.length>2)) || (domain.length<2))
						{
							alert("Domain name must end with well known domains \n or 2-lettered country name. eg com,edu,in etc.")
							emailCntrl.focus()
							ok=false
						}								
					}
				}
			}
		}
	}
	return ok
}

function LTrim(String)
{
	var i = 0;
	var j = String.length - 1;

	if (String == null)
		return (false);

	for (i = 0; i < String.length; i++)
	{
		if (String.substr(i, 1) != ' ' &&
		    String.substr(i, 1) != '\t')
			break;
	}

	if (i <= j)
		return (String.substr(i, (j+1)-i));
	else
		return ('');
}

function RTrim(String)
{
	var i = 0;
	var j = String.length - 1;

	if (String == null)
		return (false);

	for(j = String.length - 1; j >= 0; j--)
	{
		if (String.substr(j, 1) != ' ' &&
			String.substr(j, 1) != '\t')
		break;
	}

	if (i <= j)
		return (String.substr(i, (j+1)-i));
	else
		return ('');
}

function Trim(String)
{
	if (String == null)
		return (false);

	return RTrim(LTrim(String));
}

function isName(value)
{
	for(i=0;i<value.length;i++)
	{			
		ch = value.charAt(i);
		if ( ((ch <= "z") && (ch >= "a")) || ((ch <= "Z") && (ch >= "A")) || (ch == ' ') || (ch == " ") ) {}
		else 
 			return false;
	}
 	return true;
}
function isDigit(value)
{
	var digit = Boolean(true);
    	for(var i=0;i<value.length;i++)
    	{
      	  var ch = value.charAt(i);
      	  if(!(ch == "0" || ch=="1" || ch=="2" || ch=="3" || ch=="4" || ch=="5" || ch=="6" || ch=="7" || ch=="8" || ch=="9"))
   	  	return false;
      	}
	return true;
}
function isFloat(field)
{
	var point=0;
	var i=0;
	var beforePoint=0;
	var afterPoint=0;
	for(var a=0;a<field.length;a++)
	{
    	if(!(field.charAt(a)=="." || field.charAt(a)=="0" || field.charAt(a)=="1" || field.charAt(a)=="2" || field.charAt(a)=="3" || field.charAt(a)=="4" || field.charAt(a)=="5" || field.charAt(a)=="6" || field.charAt(a)=="7" || field.charAt(a)=="8" || field.charAt(a)=="9"))
			return false;
		if(field.charAt(a)==".")
			point++;
	}
	if(point > 1)
		return false;
	if(point == 1)
	{
		while(field.charAt(i)!=".")
		{
			i++;
			beforePoint++;
		}
		afterPoint=field.length-(beforePoint+1);
/*		if(beforePoint > 3)
			return false;
		if(afterPoint > 4)
			return false;*/
		if(afterPoint==0 && beforePoint==0)
			return false;
	}
	return true;
}
function printReport()
{
	if(document.getElementById('btnUpdate'))
		document.getElementById('btnUpdate').style.visibility = 'hidden';
	if(document.getElementById('btnCancel'))
		document.getElementById('btnCancel').style.visibility = 'hidden';
	if(document.getElementById('btnPrint'))
		document.getElementById('btnPrint').style.visibility = 'hidden';
	window.print();
}
function getHTTPObject()
{
	var xmlhttp;
	/*@cc_on
	@if (@_jscript_version >= 5)
	try
	{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch (e)
	{
		try
		{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch (E)
		{
			xmlhttp = false;
		}
	}
	@else
	xmlhttp = false;
	@end @*/
	if (!xmlhttp && typeof XMLHttpRequest != 'undefined')
	{
	try {
		xmlhttp = new XMLHttpRequest();
	} catch (e) {
		xmlhttp = false;
	}
	}
	return xmlhttp;
}
function redirect(url)
{
	document.location = url;
}
function showHide(obj, val)
{
	document.getElementById(obj).style.display = val;
}
function validateControl(cntrl, cntrlName, cntrlType)
{
	if(cntrl)
	{
		cntrlValue = Trim(cntrl.value);
		switch(cntrlType)
		{
			case "text":
				if (cntrlValue.length < 1)
				{
					alert(cntrlName + " is required.");
					cntrl.focus();
					return false;
				}
				break;
			case "dd":
				if (cntrlValue <= 0)
				{
					alert(cntrlName + " is required.");
					cntrl.focus();
					return false;
				}
				break;
			case 'list':
				if(cntrl.selectedIndex == -1)
					var listId = -1;
				else
					var listId = parseInt(cntrl.options[cntrl.selectedIndex].value);
				if (listId == -1)
				{
					alert("Please Select "+cntrlName+".");
					return false;
				}
				break;
		}
	}
	return true;
}
function Cancel()
{
	document.location = 'home.php';
}
function cancel()
{
	document.location = 'home.php';
}
function selectAllChxBxs(idSelectAll, idChkBxs, totalRows)
{
	var chkAll = document.getElementById(idSelectAll);
	if(chkAll.checked == true)
	{
		for(var i = 0; i < totalRows; i++)
		{
			var obj = document.getElementById(idChkBxs+i);
			obj.checked = true;
		}
	}
	else
	{
		for(var i = 0; i < totalRows; i++)
		{
			var obj = document.getElementById(idChkBxs+i);
			obj.checked = false;
		}
	}
}
function getSelectedValuesFromList(objId)
{
	var cntrl = document.getElementById(objId);
	var ids = '0';
	for(var i=0; i<cntrl.length; i++)
	{
		if(cntrl.options[i].selected)
			ids = ids + ', ' + cntrl.options[i].value;
	}
	return ids;
}
function setSelectedValues(obj, findIn)
{
	for (j=0; j<obj.options.length; j++)
	{
		if(obj.options[j].value == -1)
			obj.options[j].selected = false;
		else
		{
			if(findIn.indexOf(obj.options[j].value) != -1)
				obj.options[j].selected = true;
		}
	}
}
function ToDecimal2(number)
{
	number = parseFloat(number.toString().replace(',', ''));
	var x = Math.round(number * 100) / 100;
	return x.toFixed(2);;
/*
	number = String(number);
	if (number.indexOf(".") > -1) {
		var parts = number.split(".");
		if (parts[1].length == 1) number += "0";
	}
	else
		number += ".00";
	alert('.............'+number);
	return number;
	*/
}
function Round(number)
{
	number = parseFloat(number.toString().replace(',', ''));
	var x = parseFloat(Math.round(number * 100) / 100).toFixed(2);
	return x;
}
function applyBGColor(obj, color)
{
	obj.style.backgroundColor = color;//'#909090';
}
function removeBGColor(obj, color)
{
	obj.style.backgroundColor = color;//'#b9b6b6';
}

function isAlphaNumeric(str) {
	var code, i, len;
  
	for (i = 0, len = str.length; i < len; i++) {
	  code = str.charCodeAt(i);
	  if (!(code > 47 && code < 58) &&  !(code > 64 && code < 91) && !(code > 96 && code < 123)) { // lower alpha (a-z)
		return false;
	  }
	}
	return true;
};

function alphanumeric(inputtxt, allowSpace)
{
	if(allowSpace)
		var letterNumber = /^[0-9a-zA-Z ]+$/;
	else
		var letterNumber = /^[0-9a-zA-Z]+$/;
	if(inputtxt.match(letterNumber)) 
	{
		return true;
	}
	else
	{ 
		return false; 
	}
}