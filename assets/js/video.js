$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtTitle").val()=='')
			errors += "- Please Enter Title<br />";
		if ($("#txtLnk").val()=='')
			errors += "- Please Enter Youtube Video Link<br />";
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#frm").submit(function(){
		return validate();
	});
});