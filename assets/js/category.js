$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtCategory").val() == '')
			errors += "- "+BE_1+"<br />";
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#frm").submit(function(){
		return validate();
	});
});