$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtName").val()=='')
			errors += "- Please enter Name<br />";
		if ($("#txtReview").val()=='')
			errors += "- Please enter Review<br />";
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#frm").submit(function(){
		return validate();
	});
});