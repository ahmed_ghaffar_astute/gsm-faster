$(document).ready(function()
{
	$("#txtAmount").focus();
	function validate()
	{
		var errors = '';
		if (Trim($("#txtAmount").val()) == '')
			errors += "- Please Enter Threshold Amount.<br />";
		if (!isFloat(Trim($("#txtAmount").val())))
			errors += "- Invalid value of Threshold Amount.<br />";
		if (errors=='')
			return true;
		else
		{
			$("#dvError").html(errors);
			$("#dvMsg").hide();
			$("#dvError").show();
			$("#txtAmount").focus();
			return false;
		}
	}
	$("#btnTSubmit").click(function(){
		if(validate())
		{
			$('#imgLdr').ajaxStart(function() {
				$(this).show();
			}).ajaxStop(function() {
				$(this).hide();
			});
			$.post(base_url+"page/ajx_threshold_amount", 
			   {
				   amount:$('#txtAmount').val(),
				   purpose:'thdamount'
			   },
				function(response){
				if(response)
				{
					$("#dvError").hide();
					$("#dvMsg").html(response);
					$("#dvMsg").show();
				}
			});	
		}
	});
});