$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		var arr = $("#apiId").val().split('~');
		var apiId = arr[0];
		var extId = arr[1];
		if ($("#txtTitle").val()=='')
			errors += "- "+BE_3+"<br />";
		if ($("#categoryId").val()=='0')
			errors += "- "+BE_4+"<br />";
		if ($("#txtPrice").val()=='')
			errors += "- "+BE_5+"<br />";
		if (!isFloat($("#txtPrice").val()))
			errors += "- "+BE_6+"<br />";
		if(document.getElementById('chkRtl').checked)
		{
			if ($("#txtRtlTitle").val()=='')
				errors += "- "+BE_199+"<br />";
			if ($("#txtRtlPrice").val()=='')
				errors += "- "+BE_197+"<br />";
			if (!isFloat($("#txtRtlPrice").val()))
				errors += "- "+BE_198+"<br />";
		}
		if (errors=='')
		{
			return true;
		}
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#apiId").change(function()
	{
		getSupplierServices($("#apiId").val(), $("#sc").val());
	});
	$("#frm").submit(function(){
		return validate();
	});
	function getSupplierServices(apiId, serviceType)
	{
		$('#imgSrvcLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		$.post("include/ajax/modules/ajxpackage.php", 
		   {
			   apiId:apiId, 
			   serviceType:serviceType, 
			   purpose:'getsuppliersrvcs'
		   },
			function(response){
				if(response)
				{
					$("#supplierPackId").html(response);
				}
			});	
		
	}
	$("#btnSendEmail").click(function(){
		var errors = '';
		if ($("#txtEmlAddrss").val()=='')
			errors += "- Please Enter Email Address<br />";
		if (errors=='')
		{
			$('#imgSendEmail').ajaxStart(function() {
				$(this).show();
			}).ajaxStop(function() {
				$(this).hide();
			});
			$.post("include/ajax/modules/ajxpackage.php", 
			   {
				   eml:$("#txtEmlAddrss").val(), 
				   ids:$("#acceptedIds").val(), 
				   sc:$("#sc").val(), 
				   purpose:'saoe'
			   },
				function(response){
					if(response)
					{
						$("#txtEmlAddrss").val('');
						showStickyErrorToast(response, '', 'success');
					}
				});	
		}
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	});
});
function removeProfit(orderNo, serviceType)
{
	if(confirm('Are you sure you want to remove profit for this order?'))
	{
		$.post("include/ajax/modules/ajxgeneral.php", 
		{
		   orderNo:orderNo, 
		   sc:serviceType, 
		   purpose:'rmvprf'
	   	},
		function(response){
			if(response)
			{
				showStickyErrorToast(response, '', 'success');
				var objSpn = $('#spnPrft_'+orderNo);
				if(objSpn)
					objSpn.html('0.00');
			}
		});		
	}
}