$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtGateway").val() == '')
			errors += "- "+BE_94+"<br />";
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#btnSave").click(function(){
		if(validate())
		{

			$("#dvLdr").ajaxStart(function() {
			  $(this).show();
			}).ajaxStop(function() {
			  $(this).hide();
			});
			var disable = 0;
			if(document.getElementById('chkDisable').checked)
				disable = 1;
			$.post($("#frm_smsgateway").attr('action'),
			   {
				   csrf_test_name:$( "input[name='csrf_test_name']" ).val(),
				   gateway:$('#txtGateway').val(),
				   uName:$('#txtUName').val(),
				   pwd:$('#txtPwd').val(), 
				   disable:disable, 
				   purpose:'save',
				   id:$('#id').val()
			   },
				function(response){
					if(response)
					{
						arrResponse = response.split('~');
						if(arrResponse[1] == '0')
							showStickyErrorToast(arrResponse[0], '', 'success');
						else
							showStickyErrorToast(arrResponse[0], '', 'error');
					}
				});	
		}
	});
});
