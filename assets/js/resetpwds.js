$(document).ready(function()
{
	$("#btnRSetPwd").click(function(){
		$('#statusLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		$.post(base_url+"admin/clients/ajxresetpwds", 
		   {
			   i:$('#i').val(), 
			   purpose:'rstPwds'
		   },
			function(response){
				if(response)
				{
					showStickyErrorToast(response, '', 'success');
				}
			});	
	});
	$("#btnRSetPins").click(function(){
		$('#statusLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		$.post(base_url+"admin/clients/ajxresetpwds", 
		   {
			   purpose:'rstPins'
		   },
			function(response){
				if(response)
				{
					showStickyErrorToast(response, '', 'success');
				}
			});	
	});
	$("#btnRSetPr1").click(function(){
		resetPrices('0');
	});
	$("#btnRSetPr2").click(function(){
		resetPrices('1');
	});
	$("#btnRSetPr3").click(function(){
		resetPrices('2');
	});
	function resetPrices(i)
	{
		$('#statusLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		$.post(base_url+"admin/clients/ajxresetpwds", 
		   {
			   purpose:'rstPrics', 
			   i:i
		   },
			function(response){
				if(response)
				{
					showStickyErrorToast(response, '', 'success');
				}
			});	
		
	}
});