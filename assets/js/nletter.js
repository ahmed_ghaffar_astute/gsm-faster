$(document).ready(function()
{
    function validate()
    {
        var errors = '';
        if ($("#ltrId").val()=='0')
            errors += "- Please Select Newsletter<br />";
        if (errors=='')
            return true;
        else
        {
            showStickyErrorToast(errors, BE_ERR, 'error');
            return false;
        }
    }

    var url = $('#link').val();
    $("#btnSave").click(function(){
        if(validate())
        {
            $('#statusLoader').ajaxStart(function() {
                $(this).show();
            }).ajaxStop(function() {
                $(this).hide();
            });
            $.post(url+"admin/Miscellaneous/ajxnletter",
                {
                    ltrId:$('#ltrId').val(),
                    planId:$('#planId').val(),
                    purpose:'snl'
                },
                function(response){
            		var jsonresult= JSON.parse(response);
                        console.log(jsonresult.msg);
                        if(jsonresult.status == '1'){
							showStickyErrorToast(jsonresult.msg, '', 'success');
						}
                        else if(jsonresult.status == '0'){
							showStickyErrorToast(jsonresult.msg, '', 'error');
						}

                });
        }else{
            console.log('error');
        }
    });
});
