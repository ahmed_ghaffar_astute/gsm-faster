$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtLnkTitle").val()=='')
			errors += "- "+BE_42+"<br />";
		if ($("#txtPageTitle").val()=='')
			errors += "- "+BE_43+"<br />";
/*		if ($("#txtPageText").val()=='')
			errors += "- "+BE_44+"<br />";*/
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#frm").submit(function(){
		return validate();
	});
});