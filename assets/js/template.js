$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtName").val()=='')
			errors += "- "+BE_188+"<br />";
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#frm").submit(function(){
		return validate();
	});
});
