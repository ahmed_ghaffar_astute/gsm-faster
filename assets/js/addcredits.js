$(document).ready(function()
{
	$(".select2").select2();
	function doCalculations()
	{
		if (document.getElementById('txtCredits').value && $('#txtCredits').val() != '' && $('#pMethod').val() != '')
		{
			$('#dvCalculations').show();
			var checkedValue = $('#pMethod').val();
			var fee = 0;
			$('#dvAddrInfo').hide();
			if(checkedValue == '1')
			{
				fee = $('#hdFee1').val();
				$('#dvAddrInfo').show();
			}
			else
			{
				fee = $('#hdFee'+checkedValue).val();
			}
			fee = ToDecimal2(fee);

			var vat = $('#hdVat'+checkedValue).val();
			if(Trim(vat) == '')
				vat = 0;
			vat = ToDecimal2(vat);
			
			var symbol = $('#currSymbol').val();
			/*$("#tdTotalPrice").html($('#txtCredits').val() + ' ' + symbol);
			$("#tdPaymentFee").html(fee + ' %');*/
			//var feePercent = Math.round(parseFloat(fee/100));
			var feePercent = ToDecimal2(parseFloat(fee/100));
			var percentage = ToDecimal2(parseFloat($('#txtCredits').val() * feePercent));
			/*$("#tdFeeAmount").html(ToDecimal2(Round(percentage)) + ' ' + symbol);*/
			var price = parseFloat($('#txtCredits').val());
			var totalToPay = parseFloat(percentage)+parseFloat(price);
			
			var current_balance = parseFloat($('.balance').text());			   
			var TOTAL_PAY = 0;
			var TAX_PERCENT = 0;
			var FEE_TAX = '';
			var current_balance = parseFloat($('.balance').text());
			var AMOUNT_WITH_FEE = 0;
			var TAX2 = '';
			var BALANCE_WILL_BE = current_balance + price;
			if (fee)  
			{
			  var tax_percent = parseFloat(fee/100) * price;
			  var total_pay = current_balance + price;
			  AMOUNT_WITH_FEE = parseFloat(price) + parseFloat(tax_percent);
			  arr = new Array(); 
			  TOTAL_PAY = Round(total_pay);	  
			  TAX_PERCENT = ToDecimal2(tax_percent);	
			  FEE_TAX = 'Transaction Tax : '+fee+'%';	
			  TAX2 = '0';	
			  //arr[4] = 'Tax Due';	 
			}
			else
			{
				var tax_percent = 0;
				var total_pay = current_balance + price;
				AMOUNT_WITH_FEE = parseFloat(price) + parseFloat(tax_percent);
				arr = new Array(); 
				TOTAL_PAY = ToDecimal2(total_pay);
				TAX_PERCENT = ToDecimal2(tax_percent);	
				FEE_TAX = 'Transaction Tax : 0%';	
				TAX2 = '0';	
				//arr[4] = 'Tax Due';	
			}
			var VAT_TAX = '';
			if(vat > 0)  
			{
			  var vat_percent = parseFloat(vat/100) * AMOUNT_WITH_FEE;
			  var total_pay = current_balance + AMOUNT_WITH_FEE;
			  TOTAL_PAY = ToDecimal2(parseFloat(total_pay) + parseFloat(vat_percent));
			  VAT_PERCENT = ToDecimal2(vat_percent);
			  VAT_TAX = 'VAT Tax : '+vat+'%';	
			  TAX2 = '0';	
			}
			else
			{
				var vat_percent = 0;
				var total_pay = current_balance + AMOUNT_WITH_FEE;
				TOTAL_PAY = ToDecimal2(total_pay);
				VAT_PERCENT = ToDecimal2(vat_percent);	
				VAT_TAX = 'VAT Tax : 0%';	
				TAX2 = '0';	
			}

			$('.total').html(ToDecimal2(BALANCE_WILL_BE));
			$('.tax').html(ToDecimal2(TAX_PERCENT));
			$('.taxname').html(FEE_TAX);
			$('.tax2').html(TAX2);
			$('.vatname').html(VAT_TAX);
			$('.vat').html(VAT_PERCENT);
			//$('.taxname2').html(arr[4]);
			$('.total-paynow').text(ToDecimal2(parseFloat($('#txtCredits').val())+parseFloat($('.tax').text())+parseFloat($('.tax2').text())+parseFloat($('.vat').text())));
			$("#grdTotal").val(ToDecimal2(parseFloat($('#txtCredits').val())+parseFloat($('.tax').text())+parseFloat($('.tax2').text())+parseFloat($('.vat').text())));
			
			if($('.updateno').val()==''){
				$('.updateno').val('0');
			}       
		}
		else
		{
			$('#dvCalculations').hide();	
		}
	}
	
	$("#txtCredits").keyup(function(){
		doCalculations();
	});
	function setPaymentDtls(i)
	{
		if(i == '')
		{
			$("#dvInfo").hide();
		}
		else
		{
			var dtls = $('#pMethod').find('option:selected').attr('details2');
			if(dtls == '') {
				$("#dvPDtls").html("No payment details!");
			}else{
			    var imgName = $('#pMethod').find('option:selected').attr('imgName');
                var conImgName = '';
                if(imgName)
                    conImgName = '<div><img src="'+imgName+'" style="display: block; margin-bottom: 10px;"/></div>';
				$("#dvPDtls").html(conImgName+dtls);
            }
			$("#dvInfo").show();
		}
	}
	$("#pMethod").change(function()
	{
		setPaymentDtls(this.value);
		doCalculations();
	});
	$("#frm").submit(function(){
		return validateMe();
	});
	$("#frmBO").submit(function(){
		return validateBO();
	});
	$("#btnSave").click(function(){
		if(validate())
		{			
			$('#statusLoader').ajaxStart(function() {
				$(this).show();
	
			}).ajaxStop(function() {
				$(this).hide();
			});
			if(document.getElementById('disableUser').checked)
				disableUser = 1;
			else
				disableUser = 0;			
			if(document.getElementById('isAdmin').checked)
				isAdmin = 1;
			else
				isAdmin = 0;			
			$.post("include/ajax/modules/ajxuser.php", 
			   {
				   fName:$('#txtFName').val(), 
				   lName:$('#txtLName').val(),
				   userName:$('#txtUserEmail').val(),
				   userPwd:$('#txtPwd').val(),
				   branchId:$('#branchId').val(),
				   disableUser:disableUser,
				   isAdmin:isAdmin,
				   comments:$('#txtComments').val(),
				   id:$('#id').val()
			   },
				function(response){
					if(response)
					{
						arrResponse = response.split('~');
						if(arrResponse[1] == 1)
						{
						   $('#txtFName').val('');
						   $('#txtLName').val('');
						   $('#txtUserEmail').val('');
						   $('#txtPwd').val('');						   
						}
						$('#txtFName').focus();
						$('#btnSave').disabled = false;
						$("#errors_div").html(arrResponse[0]);
						$("#errors_div").fadeIn("slow");
					}
				});	
		}
	});
});