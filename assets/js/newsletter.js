$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#categoryId").val()=='select')
			errors += "- Please select Category<br />";
		if ($("#txtTitle").val()=='')
			errors += "- "+BE_58+"<br />";
		if ($("#txtNewsLtr").val()=='')
			errors += "- "+BE_59+"<br />";
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#frm").submit(function(){
		return validate();
	});
});
