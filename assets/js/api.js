$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtTitle").val()=='')
			errors = "- "+BE_39+"<br />";
		if ($("#txtURL").val()=='')
			errors = "- Please Enter Supplier URL<br />";
		if (!isFloat($("#txtThresholdLimit").val()))
			errors += "- Please enter valid Credit Balance Threshold Limit<br />";
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#apiType").change(function()
	{
		if($("#apiType").val() == '0')
		{
			$("#dvRURL").show();
			$("#dvAction").show();
			$("#dvServiceId").show();
			$("#dvExtId").show();
		}
		else
		{
			$("#dvRURL").hide();
			$("#dvAction").hide();
			$("#dvServiceId").hide();
			$("#dvExtId").hide();
		}
	});
	/*
	$("#rdServices1").click(function()
	{
		$("#lnkSrvcs").hide();
	});
	$("#rdServices2").click(function()
	{
		$("#lnkSrvcs").show();
	});
	$("#rdSServices1").click(function()
	{
		$("#lnkSSrvcs").hide();
	});
	$("#rdSServices2").click(function()
	{
		$("#lnkSSrvcs").show();
	});*/
	$("#rdSyncType1").click(function()
	{
		$("#dvPrices").hide();
	});
	$("#rdSyncType2").click(function()
	{
		$("#dvPrices").show();
	});
	$("#rdSSyncType1").click(function()
	{
		$("#dvSPrices").hide();
	});
	$("#rdSSyncType2").click(function()
	{
		$("#dvSPrices").show();
	});
	$("#btnVC").click(function()
	{
		$('#vcLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		$.post(base_url + "admin/settings/services_sync",
		   {
			   purpose:'vc',
			   id:$('#id').val(),
			   apiType:$('#apiType').val()
		   },
			function(response)
			{
				if(response)
					showStickyErrorToast(response, '', 'success');
			});	
	});
	$("#btnSync").click(function()
	{
		$('#syncLoader').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
		var syncIMEIS = 0;
		var syncModels = 0;
		var SYNC = true;
		var setPrices = 0;
		if(document.getElementById('rdSyncType2').checked)
		{
			if(confirm ('Synching all groups and services will HIDE existing groups, services and will make new groups and services. Are sure you want to continue?'))
			{
				var errors = '';
				if ($("#txtPrice").val()=='')
					errors = "- Please Enter Price";
				if (!isFloat($("#txtPrice").val()))
					errors += "- Invalid value of Price";
				if (errors!='')
				{
					showStickyErrorToast(errors, BE_ERR, 'error');
					return false;
				}
				syncIMEIS = 1;
			}
			else
				SYNC = false;
		}
		var priceType = 0;
		var pricing = 0;
		var otherCurrencies = 0;
		if(document.getElementById('chkSyncModels') && document.getElementById('chkSyncModels').checked)
			syncModels = 1;
		if(document.getElementById('chkAttachSrv') && document.getElementById('chkAttachSrv').checked)
			attachSrv = 1;
		if(document.getElementById('rdPrType1').checked)
			priceType = 1;
		else if(document.getElementById('rdPrType1').checked)
			priceType = 2;
		if(document.getElementById('rdPricing1').checked)
			pricing = 1;
		if(document.getElementById('chkOtherCurrPrices').checked)
			otherCurrencies = 1;
		if(SYNC == true)
		{
			$.post(base_url + "admin/settings/services_sync",
			   {
				   purpose:'syncIMEI',
				   syncIMEIS:syncIMEIS, 
				   syncModels:syncModels, 
				   attachSrv:attachSrv, 
				   priceType:priceType,
				   pricing:pricing,
				   otherCurrencies:otherCurrencies,
				   price:$('#txtPrice').val(),
				   id:$('#id').val()
			   },
				function(response)
				{
					if(response)
						showStickyErrorToast(response, '', 'success');
				});	
		}
	});
	$("#btnServerSync").click(function()
	{
		$('#syncSLdr').ajaxStart(function() {
			$('#syncSLdr').show();
		}).ajaxStop(function() {
			$('#syncSLdr').hide();
		});
		var syncSRVCS = 0;
		var SYNC = true;
		var setPrices = 0;
		if(document.getElementById('rdSSyncType2').checked)
		{
			if(confirm ('Synching all groups and services will HIDE existing Server groups and services and will make new Server groups and services. Are sure you want to continue?'))
			{
				var errors = '';
				if ($("#txtSPrice").val()=='')
					errors = "- Please Enter Price";
				if (!isFloat($("#txtSPrice").val()))
					errors += "- Invalid value of Price";
				if (errors!='')
				{
					showStickyErrorToast(errors, BE_ERR, 'error');
					return false;
				}
				syncSRVCS = 1;
			}
			else
				SYNC = false;
		}
		var priceType = 0;
		var pricing = 0;
		var otherCurrencies = 0;
		if(document.getElementById('chkAttachSSrv') && document.getElementById('chkAttachSSrv').checked)
			attachSrv = 1;
		if(document.getElementById('rdSPrType1').checked)
			priceType = 1;
		else if(document.getElementById('rdSPrType1').checked)
			priceType = 2;
		if(document.getElementById('rdSPricing1').checked)
			pricing = 1;
		if(document.getElementById('chkOtherCurrSPrices').checked)
			otherCurrencies = 1;
		if(SYNC == true)
		{
			$.post(base_url + "admin/settings/services_sync",
			   {
				   purpose:'syncSS',
				   syncSRVCS:syncSRVCS, 
				   attachSrv:attachSrv, 
				   priceType:priceType,
				   pricing:pricing,
				   otherCurrencies:otherCurrencies,
				   price:$('#txtSPrice').val(),
				   id:$('#id').val()
			   },
				function(response)
				{
					if(response)
						showStickyErrorToast(response, '', 'success');
				});	
		}
	});
	$("#btnServerSync1").click(function()
	{
		$('#syncSLdr1').ajaxStart(function() {
			$('#syncSLdr1').show();
		}).ajaxStop(function() {
			$('#syncSLdr1').hide();
		});
		$.post(base_url + "admin/settings/services_sync",
		   {
			   purpose:'syncSS',
			   id:$('#id').val()
		   },
			function(response)
			{
				if(response)
					showStickyErrorToast(response, '', 'success');
			});	
	});
	$("#btnFileSync").click(function()
	{
		$('#syncFlLdr').ajaxStart(function() {
			$('#syncFlLdr').show();
		}).ajaxStop(function() {
			$('#syncFlLdr').hide();
		});
		$.post(base_url + "admin/settings/services_sync",
		   {
			   purpose:'syncFS',
			   id:$('#id').val()
		   },
			function(response)
			{
				if(response)
					showStickyErrorToast(response, '', 'success');
			});	
	});
	$("#frm").submit(function()
	{
		return validate();
	});
});
