$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtLbl").val()=='')
			errors = "-Please enter Field Label<br />";
		if ($("#filedType").val()=='0')
			errors += "- Please select Field Type";
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#btnSave").click(function(){
		if(validate())
		{
			$('#dvLoader').ajaxStart(function() {
				$(this).show();
			}).ajaxStop(function() {
				$(this).hide();
			});
			var mandatory = 0;
			var disable = 0;
			if(document.getElementById('chkMandatory').checked)
				mandatory = 1;
			if(document.getElementById('chkDisable').checked)
				disable = 1;

			//var baseurl=document.getElementById("baseurl").innerHTML;

			$.post(baseurl+"admin/settings/ajxregfield",
				{
					fieldLbl:$('#txtLbl').val(),
					fieldType:$('#filedType').val(),
					mandatory:mandatory,
					disable:disable,
					purpose:'save',
					id:$('#id').val()
				},
				function(response){
					if(response)
					{
						arrResponse = response.split('~');
						if(arrResponse[1] == 1)
						{
							$('#txtLbl').val('');
							$('#fieldType').val('0');
						}
						showStickyErrorToast(arrResponse[0], '', 'success');
					}
				});
		}
	});
});
