$(document).ready(function()
{
	function validate()
	{
		var errors = '';
		if ($("#txtAPIName").val() == '')
			errors += "- Please enter Provider Name <br />";
		if ($("#txtAPIKey").val() == '')
			errors += "- Please enter API Key <br />";
		if ($("#txtGroupId").val() == '')
			errors += "- Please enter Group/List Id <br />";		
		if (errors=='')
			return true;
		else
		{
			showStickyErrorToast(errors, BE_ERR, 'error');
			return false;
		}
	}
	$("#btnSave").click(function(){
		if(validate())
		{
			$("#dvLdr").ajaxStart(function() {
			  $(this).show();
			}).ajaxStop(function() {
			  $(this).hide();
			});
			$.post(base_url+"admin/clients/ajxemailint",
			   {
				   apiName:$('#txtAPIName').val(), 
				   apiKey:$('#txtAPIKey').val(), 
				   group:$('#txtGroupId').val(), 
				   purpose:'save',
				   id:$('#id').val()
			   },
				function(response){
					if(response)
					{
						arrResponse = response.split('~');
						if(arrResponse[1] == '0'){
							showStickyErrorToast(arrResponse[0], '', 'success');
						}
						else{
							showStickyErrorToast(arrResponse[0], '', 'error');
						}
						window.location.href  = base_url+'admin/clients/emailintegrations?frmId=235&fTypeId=5' ;
					}
				});	
		}
	});
	$("#btnSync").click(function(){
		if(validate())
		{
			$("#dvLdr").ajaxStart(function() {
			  $(this).show();
			}).ajaxStop(function() {
			  $(this).hide();
			});
			$.post(base_url+"admin/clients/ajxemailint",
			   {
				   planId:$('#planId').val(), 
				   purpose:'sync',
				   id:$('#id').val()
			   },
				function(response){
					if(response)
					{

						arrResponse = response.split('~');
						if(arrResponse[1] == '0')
							showStickyErrorToast(arrResponse[0], '', 'success');
						else
							showStickyErrorToast(arrResponse[0], '', 'error');
					}
				});	
		}
	});
});