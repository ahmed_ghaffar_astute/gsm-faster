<?
$file = fopen("style.txt","r");
$cntr = 0;
$arrColors = array();
$clrsHdr = array();
$clrsCnts = array();
$clrsFtr = array();
while(! feof($file))
{
  $arrColors[$cntr] = fgets($file);
  $cntr++;
}
fclose($file);
if(isset($arrColors[0]) && $arrColors[0] != '')
	$clrsHdr = explode('|', $arrColors[0]);
if(isset($arrColors[1]) && $arrColors[1] != '')
	$clrsCnts = explode('|', $arrColors[1]);
if(isset($arrColors[2]) && $arrColors[2] != '')
	$clrsFtr = explode('|', $arrColors[2]);
//============================================== HEADER =====================================================//
$topBarBG = $clrsHdr[0];
$topBarClr = $clrsHdr[1];
$topBarA = $clrsHdr[2];
$menuLnkClr = $clrsHdr[3];
$menuBGClr = $clrsHdr[4];
$logoBarBG = $clrsHdr[5];
$logoBarClr = $clrsHdr[6];
$navBarBG = $clrsHdr[7];
$navBarLnk = $clrsHdr[8];
$navBar2BG = $clrsHdr[9];
//============================================== HEADER =====================================================//

//============================================== CONTENTS ===================================================//
$gridBG = $clrsCnts[0];
$gridLnkClr = $clrsCnts[1];
$gridHdClr = $clrsCnts[2];
$tblHdrBG = $clrsCnts[3];
$tblHdrClr = $clrsCnts[4];
$pgTitleBG = $clrsCnts[5];
$pgTitleClr = $clrsCnts[6];
$tabsBGClr = $clrsCnts[7];
$currTabBG = $clrsCnts[8];
$currTabClr = $clrsCnts[9];
$tabTxtClr = $clrsCnts[10];
$btnBG = $clrsCnts[11];
$btnClr = $clrsCnts[12];
$pageSubHeading = $clrsCnts[13];
$btnBigBG = $clrsCnts[14];
$btnBigClr = $clrsCnts[15];
//============================================== CONTENTS ===================================================//

//============================================== FOOTER =====================================================//
$ftrBG = $clrsFtr[0];
$ftrLnk = $clrsFtr[1];
$ftrLnkHvr = $clrsFtr[2];
$ftrP = $clrsFtr[3];
//============================================== FOOTER ====================================================//
?>