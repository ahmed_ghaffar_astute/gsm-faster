<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['admin/coupon/edit/(:num)']="admin/coupon/coupon_form";
$route['admin/coupon/add'] = 'admin/coupon/coupon_form';
$route['admin/coupon/(:num)'] = 'admin/coupon';
$route['admin/coupon'] = 'admin/coupon';

$route['admin/banner/edit/(.+)']="admin/banner/banner_form";
$route['admin/banner/add'] = 'admin/banner/banner_form';
$route['admin/banner/(:num)'] = 'admin/banner';
$route['admin/banner'] = 'admin/banner';

$route['admin/products/duplicate-product/(.+)']="admin/product/clone_product";
$route['admin/products/edit/(:num)']="admin/product/product_form";
$route['admin/products/add'] = 'admin/product/product_form';
$route['admin/products/(:any)'] = 'admin/product';

$route['admin/products'] = 'admin/product';

$route['admin/brand/edit/(:num)']="admin/brand/brand_form";
$route['admin/brand/add'] = 'admin/brand/brand_form';
$route['admin/brand/(:num)'] = 'admin/brand';
$route['admin/brand'] = 'admin/brand';

$route['admin/sub-category/edit/(:num)'] = 'admin/SubCategory/sub_category_form';
$route['admin/sub-category/add'] = 'admin/SubCategory/sub_category_form';
$route['admin/sub-category/(:num)'] = 'admin/SubCategory';
$route['admin/sub-category'] = 'admin/SubCategory';

$route["admin/category/edit/(:num)"]="admin/category/category_form";
$route['admin/category/add'] = 'admin/category/category_form';
$route['admin/category/(:num)'] = 'admin/category';
$route['admin/category'] = 'admin/category';






$route['shop'] = 'site';
$route['download-invoice/(.+)'] = 'site/download_invoice';
$route['product-reviews/(:any)'] = 'site/product_reviews';
$route['product-reviews/(:any)/(:any)'] = 'site/product_reviews';

$route['cancellation'] = 'site/cancellation';
$route['refund-return-policy'] = 'site/refund_return_policy';
$route['privacy'] = 'site/privacy';
$route['terms-of-use'] = 'site/terms_of_use';
$route['payments'] = 'site/payments';
$route['faq'] = 'site/faq';
$route['about-us'] = 'site/about_us';
$route['contact-us'] = 'site/contact_us';

$route['my-reviews'] = 'site/my_reviews';
$route['saved-bank-accounts'] = 'site/saved_bank_accounts';
$route['my-addresses'] = 'site/my_addresses';
$route['my-account'] = 'site/my_account';

$route['search-result'] = 'site/search';

$route['search-result/(:any)'] = 'site/search';

$route['my-orders/(:any)'] = 'site/my_order/$1';

$route['my-orders'] = 'site/my_order';

$route['product/(:any)'] = 'site/single_product/$1';

$route['remove-to-cart/(:num)'] = 'site/remove_cart/$1';

$route['checkout'] = 'site/checkout';

$route['my-cart'] = 'site/my_cart';

$route['wishlist'] = 'site/get_wishlist';

$route['login-register'] = 'site/login_register_form';

$route['category/(:any)'] = 'site/sub_category';

$route['category/(:any)/(:any)/(:num)'] = 'site/cat_sub_product/$1/$2';

$route['category/(:any)/(:any)'] = 'site/cat_sub_product/$1/$2';

$route['category'] = 'site/category';

$route['offers/(:any)/(:any)'] = 'site/offer_products';
$route['offers/(:any)'] = 'site/offer_products';

$route['offers'] = 'site/offers';

$route['todays-deals/(:any)'] = 'site/todays_deals_list';
$route['todays-deals'] = 'site/todays_deals_list';

