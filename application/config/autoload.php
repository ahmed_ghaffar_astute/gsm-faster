<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| AUTO-LOADER
| -------------------------------------------------------------------
| This file specifies which systems should be loaded by default.
|
| In order to keep the framework as light-weight as possible only the
| absolute minimal resources are loaded by default. For example,
| the database is not connected to automatically since no assumption
| is made regarding whether you intend to use it.  This file lets
| you globally define which systems you would like loaded with every
| request.
|
| -------------------------------------------------------------------
| Instructions
| -------------------------------------------------------------------
|
| These are the things you can load automatically:
|
| 1. Packages
| 2. Libraries
| 3. Drivers
| 4. Helper files
| 5. Custom config files
| 6. Language files
| 7. Models
|
*/

/*
| -------------------------------------------------------------------
|  Auto-load Packages
| -------------------------------------------------------------------
| Prototype:
|
|  $autoload['packages'] = array(APPPATH.'third_party', '/usr/local/shared');
|
*/
$autoload['packages'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Libraries
| -------------------------------------------------------------------
| These are the classes located in system/libraries/ or your
| application/libraries/ directory, with the addition of the
| 'database' library, which is somewhat of a special case.
|
| Prototype:
|
|	$autoload['libraries'] = array('database', 'email', 'session');
|
| You can also supply an alternative library name to be assigned
| in the controller:
|
|	$autoload['libraries'] = array('user_agent' => 'ua');
*/

$autoload['libraries'] = array('CI_GoogleAuthenticator' ,'database', 'email', 'session' , 'form_validation', 'Foneshop_lib', 'CI_GFM_API', 'CI_GFM_API', 'CI_UnlockBase',
	/*'Bruteforce_mobi_api',*/ 'CI_mybruteforce_api', /*'CI_mgr_simunlocks',*/ 'CI_sunnysofts', 'CI_gsmunlockusa_api',
	'CI_gsmunlockusa_arraytoxml', 'CI_UnlockHK', 'CI_unlocktele_unlockhk', /*'CI_sl3team', 'CI_sl3bf',*/ 'CI_appvisi0n',
	'CI_chimeratoolapi', 'CI_unlockcodesource','CI_gsmkody', 'CI_lockpop_api', 'CI_z3x_team', 'CI_ucocustomapi', 'CI_unlockingsmart', 'CI_zzunlockcustomapi', 'CI_zzucustomapi_sl',
	'CI_furiousgold', /*'CI_dc_unlocker',*/ 'CI_samkey', 'CI_miracleserver', 'CI_samkey_us'/*,'CI_unlocksworld'*/,'CI_fusionclientapi','CI_fusionclientapi2',
	'CI_codeskpro', 'CI_gsmfusionapi', 'CI_nakshsoft'/*, 'HTMLPurifier'*/ );


/*
| -------------------------------------------------------------------
|  Auto-load Drivers
| -------------------------------------------------------------------
| These classes are located in system/libraries/ or in your
| application/libraries/ directory, but are also placed inside their
| own subdirectory and they extend the CI_Driver_Library class. They
| offer multiple interchangeable driver options.
|
| Prototype:
|
|	$autoload['drivers'] = array('cache');
|
| You can also supply an alternative property name to be assigned in
| the controller:
|
|	$autoload['drivers'] = array('cache' => 'cch');
|
*/
$autoload['drivers'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Helper Files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['helper'] = array('url', 'file');
*/
$autoload['helper'] = array('url', 'file', 'form', 'custom' , 'paypal' ,'string');

/*
| -------------------------------------------------------------------
|  Auto-load Config files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['config'] = array('config1', 'config2');
|
| NOTE: This item is intended for use ONLY if you have created custom
| config files.  Otherwise, leave it blank.
|
*/
$autoload['config'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Language files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['language'] = array('lang1', 'lang2');
|
| NOTE: Do not include the "_lang" part of your file.  For example
| "codeigniter_lang.php" would be referenced as array('codeigniter');
|
*/
$autoload['language'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Models
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['model'] = array('first_model', 'second_model');
|
| You can also supply an alternative model name to be assigned
| in the controller:
|
|	$autoload['model'] = array('first_model' => 'first');
*/
$autoload['model'] = array('Home_model','General_model', 'User_model', 'Pages_model', 'Util_model' , 'Admin_login_model' , 'Admin_dashboard_model' , 'Services_model', 'Settings_model' , 'Clients_model' , 'System_model' , 'Tickets_model' , 'Sales_model', 'common_model');
