<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public $data = array();
    private $keys = null;
    public $user_id = 0;
	public $UPLOAD_FOLDER;

	public function __construct() {
        parent::__construct();
        if($this->uri->segment(1) == 'admin'){
            checkAdminLoginState();

            $this->lang->load('labels', "english");
			$this->load->library('pagination');
			$this->UPLOAD_FOLDER = 'uplds';
            //$CURRENT_PAGE = basename($this->input->server('PHP_SELF'));
            $this->data['CURRENT_PAGE'] =$this->router->fetch_method();
            $this->data['QUERY_STRING'] = $this->input->server('QUERY_STRING');
			$this->data['CRON_FOLDER'] = 'cron';
          
            $this->data['rsStngs'] = $this->General_model->fetch_email_settings();
            if($this->data['rsStngs']->isv == '0')
                redirect(base_url('page/invalidsite'));
            
            $this->data['currDt'] = setDtTmWRTYourCountry();
            $row = $this->General_model->get_users_count($this->data['currDt']);
            
            $this->data['onlineUsers'] = $row->TotalUsers;

            $TICKETS = array();
            $this->data['TICKETS'] = $this->General_model->get_tickets_sum();
          
            //d($this->data['TICKETS']);

            $this->data['totalTickets'] = $this->data['TICKETS']->NEW +  $this->data['TICKETS']->CLOSED + $this->data['TICKETS']->ANSWERED +  $this->data['TICKETS']->CUSTOMERREPLIED;
            
            
            $this->data['fTypeId'] = $this->input->post_get('fTypeId') ? $this->input->post_get('fTypeId') : 0;
            $this->data['frmId'] =$this->input->post_get('frmId') ? $this->input->post_get('frmId') : 0;
            //============================================= ADMIN FORMS ================================================//
            $strAndAdminForms = '';
            $showTicketInHdr = false;
            $this->data['showSettingsInHdr'] = false;
            $showDashboard = false;
            $ARR_ADMIN_FORMS = array();
            if($this->session->userdata('IsAdmin') == '1')
            {
                $rsAdminForms = $this->General_model->fetch_formsdata();
            }
            else
            {
                $rsAdminForms = $this->General_model->fetch_formsdata_by_id();
            }
            $this->data['rsAdminForms'] = $rsAdminForms;
            $this->data['mn'] = $this->input->post_get('mn')?: '';
            $this->data['iFrm'] = $this->input->post_get('iFrm') ?: 0;
            $this->data['rsTcktStatus'] = $this->General_model->fetch_tckt_status();
            $this->data['IS_DEMO'] = false;
            
            if($rsAdminForms)
            {
                $prevFTypeId = 0;
                $strLeftMenu = '';
                $prevH_FTypeId = 0;
                $strTopMenu = '';

                foreach($rsAdminForms as $row)
                {
                    $arrForm = explode('?', $row->FormUrlNew);
                    if(isset($arrForm[0] )&& $arrForm[0] != '')
                        $ARR_ADMIN_FORMS[$row->FormId] = $arrForm[0];
                    if($row->FormTypeId == '1')
                        $showDashboard = true;
                    if($row->FormTypeId == '12')
                        $showTicketInHdr = true;
                    if($row->FormTypeId == '13')
                        $this->data['showSettingsInHdr'] = true;
                    if(InStr($row->FormUrlNew, '?')){
                        $formURL =  $row->FormUrlNew.'&frmId='.$row->FormId.'&fTypeId='.$row->FormTypeId;
                    }
                    else
                        $formURL = $row->FormUrlNew.'?frmId='.$row->FormId.'&fTypeId='.$row->FormTypeId;
                        
                    if(/*$row->FormTypeId != '1' && */ $row->HiddenForm == '0')
                    {
                        if($row->InHeader == '0')
                        {
                            if($prevFTypeId > 0)
                            {
                                if($row->FormTypeId != $prevFTypeId)
                                {
                                    $class = '';
                                    if($this->data['fTypeId'] == $row->FormTypeId) 
                                        $class = ' nav-item-expanded nav-item-open';
                                    $strLeftMenu .= '</ul></li>';
                                    $strLeftMenu .= '<li class="nav-item nav-item-submenu' . $class . '">
                                                        <a href="" class="nav-link' . $class . '">
                                                            <i class="icon-copy"></i><span>'.stripslashes($row->FormType).
                                                        '</span></a>
                                                        <ul class="nav nav-group-sub" data-submenu-title="Layouts">' ;
                                                    
                                }
                            }
                            else
                            {
                                $class = '';
                                if($this->data['fTypeId'] == $row->FormTypeId) 
                                    $class = ' nav-item-expanded nav-item-open';
                                $strLeftMenu .= '<li class="nav-item nav-item-submenu ' . $class . '">
                                                    <a href="javascript:;" class="nav-link' . $class . '">
                                                        <i class="icon-copy"></i><span>'.stripslashes($row->FormType).
                                                    '</span></a>
                                                    <ul class="nav nav-group-sub" data-submenu-title="Layouts">';
                            }
                            $prevFTypeId = $row->FormTypeId;
                            $liClass = '';
                            if($this->data['frmId'] == $row->FormId)
                                $liClass = ' active';
                            $strLeftMenu .=	'<li  class="nav-item">
                                            <a href="'.base_url().$formURL.'" class="nav-link' . $liClass . '">';
                            $strLeftMenu .= stripslashes($row->Form);
                            if($row->IsNew)
                            {
                                $strLeftMenu .=	'<span class="badge bg-transparent align-self-center ml-auto">new</span>';
                            }
                            $strLeftMenu .=	'</a></li>';
                        }
                        else if($row->InHeader == '1' && $row->HiddenForm == '0')
                        {
                            if($prevH_FTypeId > 0)
                            {
                                if($row->FormTypeId != $prevH_FTypeId)
                                {
                                    $class = '';
                                    $strTopMenu .= '</ul></li>';
                                    $strTopMenu .= '<li>
                                                        <a data-toggle="dropdown" data-hover="dropdown" data-close-others="true" class="dropdown-toggle" href="javascript:;">
                                                        <span class="selected"></span>'.stripslashes($row->FormType).' <i class="fa fa-angle-down"></i></a><ul class="dropdown-menu fix-li">';
                                }
                            }
                            else
                            {
                                $class = '';
                                $strTopMenu .= '<li>
                                                    <a data-toggle="dropdown" data-hover="dropdown" data-close-others="true" class="dropdown-toggle" href="javascript:;">
                                                    <span class="selected"></span>'.stripslashes($row->FormType).' <i class="fa fa-angle-down"></i></a><ul class="dropdown-menu fix-li">';
                            }
                            $prevH_FTypeId = $row->FormTypeId;
                            $liClass = '';
                            if($this->data['CURRENT_PAGE'] == 'category')
                                $liClass = 'active';
                            $strTopMenu .=	'<li style="width:240px;">
                                            <a href="'.base_url().$formURL.'">';
                            $strTopMenu.= stripslashes($row->Form) ;
                            if($row->IsNew)
                            {
                                $strTopMenu .=	'<span class="badge badge-roundless badge-warning">new</span>';
                            }
                            $strTopMenu .=	'</a></li>';			
                        }
                    }
                }
                $strLeftMenu .= '</ul></li>';
                $strTopMenu .= '</ul></li>';

            }
            $this->data['ARR_ADMIN_FORMS'] = $ARR_ADMIN_FORMS;
         
            $this->data['strLeftMenu'] = $strLeftMenu;
            $this->data['strTopMenu'] = $strTopMenu ;
        }
        else{
            
            
                checkUserLoginState();

                $this->user_id = $this->session->userdata('GSM_FUS_UserId');
                $this->data['userDetails'] = $this->User_model->getUserDetails($this->session->userdata('GSM_FUS_UserId'));

                $this->lang->load('labels', "english");
                
                $this->data['PACK_PRICES_USER'] = array();
                $this->data['PACK_PRICES_PLAN'] = array();
                $this->data['PACK_PRICES_BASE'] = array();

                if($this->input->post_get('sc')){
                    $sc = $this->input->post_get('sc') ?: 0;
                }else{
                    $sc = 1; 
                }
                $s_ids=getServiceIds($sc);
                $s_ids = explode(',' , $s_ids);
                $s_ids = array_map('trim', $s_ids);

                if($this->router->fetch_method('server_services') ||$this->router->fetch_method('file_orders') ){
                    $this->data['PACK_PRICES_USER'] = $this->User_model->get_log_pack_prices_user($this->user_id);
                    $this->data['PACK_PRICES_PLAN'] = $this->User_model->get_log_pack_prices_plan($this->user_id, $this->data['userDetails']->CurrencyId);
                    $this->data['PACK_PRICES_BASE'] = $this->User_model->get_log_pack_prices_base($this->data['userDetails']->CurrencyId);   
                }else{
                    $this->data['PACK_PRICES_USER'] = $this->User_model->get_pack_prices_user($this->user_id , $s_ids);
                    $this->data['PACK_PRICES_PLAN'] = $this->User_model->get_pack_prices_plan($sc , $s_ids , $this->data['userDetails']->CurrencyId);
                    $this->data['PACK_PRICES_BASE'] = $this->User_model->get_pack_prices_base($sc , $s_ids , $this->data['userDetails']->CurrencyId);   
                }
                $this->data['settings'] = $this->User_model->getSettings();
                if($this->data['userDetails']->Credits != '')
                {
                    $this->crypt_key((string)($this->session->userdata('GSM_FUS_UserId')));
                    $this->data['userDetails']->Credits = $this->decrypt($this->data['userDetails']->Credits);
                    $this->data['myCredits'] = number_format($this->data['userDetails']->Credits, 2, '.', '');
                }
                if($this->data['userDetails']->PinCode != '')
                {
                    $this->data['userDetails']->PinCode = $this->theString_Decrypt($this->data['userDetails']->PinCode);
                }
                if($this->data['userDetails']->APIKey != '')
                {
                    $this->data['userDetails']->APIKey = $this->theString_Decrypt($this->data['userDetails']->APIKey);
                }
            
        } 
    }

    

    public function crypt_key($ckey){
        $this->keys = array();

        $c_key = base64_encode(sha1(md5($ckey)));
        $c_key = substr($c_key, 0, round(ord($ckey[0])/5));

        $c2_key = base64_encode(md5(sha1($ckey)));
        $last = strlen($ckey) - 1;
        $c2_key = substr($c2_key, 1, round(ord($ckey[$last])/7));

        $c3_key = base64_encode(sha1(md5($c_key).md5($c2_key)));
        $mid = strval(round($last/2));
        $c3_key = substr($c3_key, 1, round(ord($ckey[$mid])/9));

        $c_key = $c_key.$c2_key.$c3_key;
        $c_key = base64_encode($c_key);

        for($i = 0; $i < strlen($c_key); $i++){
            $this->keys[] = $c_key[$i];
        }
    }

    public function encrypt($string){
        if($this->keys==null) {
            return 0;
        }
        $string = base64_encode($string);
        $keys = $this->keys;
        for($i = 0; $i < strlen($string); $i++){
            $id = $i % count($keys);
            $ord = ord($string[$i]);
            $ord = $ord OR ord($keys[$id]);
            $id++;
            $ord = $ord AND ord($keys[$id]);
            $id++;
            $ord = $ord XOR ord($keys[$id]);
            $id++;
            $ord = $ord + ord($keys[$id]);
            $string[$i] = chr($ord);
        }
        return base64_encode($string);
    }

    public function decrypt($string){
        if($this->keys==null) {
            return 0;
        }

        $string = base64_decode($string);
        $keys = $this->keys;
        for($i = 0; $i < strlen($string); $i++){
            $id = $i % count($keys);
            $ord = ord($string[$i]);
            $ord = $ord XOR ord($keys[$id]);
            $id++;
            $ord = $ord AND ord($keys[$id]);
            $id++;
            $ord = $ord OR ord($keys[$id]);
            $id++;
            $ord = $ord - ord($keys[$id]);
            $string[$i] = chr($ord);
        }
        return base64_decode($string);
    }

    public function theString_Decrypt($enc_text, $password = 'P@88W04D', $iv_len = 16)
    {
        $enc_text = base64_decode($enc_text);
        $n = strlen($enc_text);
        $i = $iv_len;
        $plain_text = '';
        $iv = substr($password ^ substr($enc_text, 0, $iv_len), 0, 512);
        while ($i < $n) {
            $block = substr($enc_text, $i, 16);
            $plain_text .= $block ^ pack('H*', md5($iv));
            $iv = substr($block . $iv, 0, 512) ^ $password;
            $i += 16;
        }
        return preg_replace('/\\x13\\x00*$/', '', $plain_text);
    }

}


class MY_Home_Controller extends CI_Controller {
    public function __construct() {
        parent::__construct();

        $SHOWEPRICESATWEB = '1';
        $this->data['rsStngs'] = $this->General_model->fetch_email_settings();
       
    
        if($this->data['rsStngs']->ShowEPricesAtWeb == '0')
        {
            if($this->session->userdata('GSM_FUS_ECustomerId'))
                $SHOWEPRICESATWEB = 0;
        }
       

        $this->data['BRANDING_NAME'] = '';
        $this->data['BRANDING_URL'] = '';
        if($this->data['rsStngs']->Brnd != '')
        {
            $ARR_BRND = explode('|', $this->data['rsStngs']->Brnd);
            if(isset($ARR_BRND[0]))
                $this->data['BRANDING_NAME'] = $ARR_BRND[0];
            if(isset($ARR_BRND[1]))
                $this->data['BRANDING_URL'] = $ARR_BRND[1];
        }
        $this->data['LOGO_PATH'] = '';
        $this->data['FAV_ICO'] = '';
        $rSetLogo = $this->General_model->fetch_logos_by_ids();
       
        foreach($rSetLogo as $rw )
        {
            if (isset($rw->LogoPath) && $rw->LogoPath != '')
            {
                if($rw->LogoId == '4')
                    $this->data['LOGO_PATH'] = base_url('assets/images/'.$rw->LogoPath);
                if($rw->LogoId == '6')
                    $this->data['FAV_ICO'] = base_url('assets/images/'.$rw->LogoPath);
            }
        }

        $this->data['LIVE_CHAT_CODE'] = '';
        $this->data['GANALYTICS_CODE'] = '';
        $this->data['MARQUEE_CODE'] = '';
        $this->data['AT_WEBSITE'] = 0;
        $this->data['IN_CLIENT_PANEL'] = 0;
        $this->data['MARQUEE_POS'] = 0;
        $rsLiveChat = $this->General_model->fetch_live_chat_data();
       
        if (isset($rsLiveChat->ScriptCode) && $rsLiveChat->ScriptCode != '')
        {
            $this->data['LIVE_CHAT_CODE'] = stripslashes($rsLiveChat->ScriptCode);
            $this->data['AT_WEBSITE'] = stripslashes($rsLiveChat->AtWebsite);
            $this->data['IN_CLIENT_PANEL'] = stripslashes($rsLiveChat->InClientPanel);
        }
        $rsAnalytics = $this->General_model->fetch_analytics();
        
        if (isset($rsAnalytics->Code) && $rsAnalytics->Code != '')
        {
            $this->data['GANALYTICS_CODE'] = stripslashes($rsAnalytics->Code);
        }
        $rsMarquee = $this->General_model->fetch_marquee();
        
        if (isset($rsMarquee->Code) && $rsMarquee->Code != '')
        {
            $this->data['MARQUEE_CODE'] = stripslashes($rsMarquee->Code);
            $this->data['MARQUEE_POS'] = $rsMarquee->Position;
        }
        $this->data['FLAG_COUNTER_CODE'] = '';
        $rsFC =  $this->General_model->fetch_flag_counter();
        
        if (isset($rsFC->ScriptCode) && $rsFC->ScriptCode != '')
            $this->data['FLAG_COUNTER_CODE'] = stripslashes($rsFC->ScriptCode);


        $rsSMLinks = $this->General_model->fetch_socialmedia_data();
        
        $SOCIAL_FACEBOOK = $SOCIAL_TWITTER = 0;
        $this->data['strSocialMedia'] = $FACEBOOK_LNK = $FACEBOOK_APPID = '';
        foreach($rsSMLinks as $rwSMLnk )
        {
            $css = '';
            
            if(Instr($rwSMLnk->Link, 'facebook'))
            {
                $css = 'facebook';
                $SOCIAL_FACEBOOK = '1';
                $FACEBOOK_LNK = $rwSMLnk->Link;
                $FACEBOOK_APPID = $rwSMLnk->APPID;
            }
            else if(Instr($rwSMLnk->Link, 'twitter'))
            {
                $css = 'twitter';
                $SOCIAL_TWITTER = 1;
            }
            else if(Instr($rwSMLnk->Link, 'linkedin'))
            {
                $css = 'linkedin';
            }
            else if(Instr($rwSMLnk->Link, 'instagram'))
            {
                $css = 'instagram';
            }
            else if(Instr($rwSMLnk->Link, 't.me'))
            {
                $css = 'telegram';
            }
            $this->data['strSocialMedia'] .= '<li class="social-icons-'.$css.'"><a href="'.$rwSMLnk->Link.'" target="_blank" title="'.stripslashes($rwSMLnk->Title).'"><i class="fa fa-'.$css.'"></i></a></li>';
        } 
        $this->data['EDIT_TXT'] = '';
        if($this->session->userdata('GSM_FSN_AdmId') && $this->session->userdata('GSM_FSN_AdmId') != '')
        {
            $this->data['EDIT_TXT'] = "data-popover='true' data-html='true' data-content='ANCHORLINK'";
        }

        $rsLinks = $this->General_model->fetch_pages();
        $this->data['META_KW_CNTNTS'] = $this->data['META_DESC_CNTNTS'] = $this->data['META_KW_CONTENTS'] = $this->data['META_DESC_CONTENTS'] = $this->data['strFtrLnks'] = $this->data['aboutUsTxt'] = $this->data['strHeaderLnks'] = $this->data['title1'] = $this->data['text1'] = $img1 = $lnk1 = $this->data['title2'] = $this->data['text2'] = $img2 = $lnk2 = $this->data['title3'] = $this->data['text3'] = $img3 = $lnk3 = $homeTitle = $homeTxt = $this->data['title4'] = $this->data['text4'] = $title5 = $text5 = $title6 = $text6 = $title7 = $this->data['text7'] = $textBT = $this->data['USERVICESTITLE'] =
        $this->data['img19'] = $this->data['strSubMenuLnks'] = $this->data['strFtrLnksNew'] = $this->data['strFtrLnksFooter'] = '';
        $this->data['USERVICES'] = '0,0,0';
        $i = 1;
        $THEME = 1 ;
    
        foreach($rsLinks as $row)
        {
            $target = '';
            if($row->FileName == '')
                $lnk = base_url()."home/page?id=".$row->PageId;
            if($row->SEOURLName != '')
                $lnk = base_url().'home/pages/'.$row->SEOURLName;
            if($row->FileName != '')
                $lnk = base_url().$row->FileName;
            if($row->URL != '')
            {
                $lnk = $row->URL;
                $target = 'target="_blank"';
            }
            if($row->PageId == '15')
            {
                $titleBT = stripslashes($row->PageTitle);
                $textBT = stripslashes($row->PageText);
                $imgBT = $row->Image;
            }
            else if($row->PageId == '12')
            {
                $this->data['USERVICESTITLE'] = stripslashes($row->LinkTitle);
                $this->data['USERVICES'] = trim(stripslashes(strip_tags($row->PageText)));
            }
            else if($row->PageId == '13')
            {
                $textBT2 = stripslashes($row->PageText);
            }
            else if($row->PageId == '16')
            {
                $this->data['title16'] = $row->PageTitle;
                $this->data['text16'] = stripslashes($row->PageText);
                $img16 = $row->Image;
            }
            else if($row->PageId == '18')
            {
                $this->data['text18'] = stripslashes($row->PageText);
            }
            else if($row->PageId == '19')
            {
                $title19 = $row->PageTitle;
                $text19 = substr(stripslashes($row->PageText), 0, 200);
                if(strlen($row->PageText) > 200)
                    $text19 .= '...</p>';
                if($row->FileName == '')
                    $lnk19 = "home/page?id=".$row->PageId;
                if($row->SEOURLName != '')
                    $lnk19 = $row->SEOURLName.'.xhtml';
                else if($row->FileName != '')
                    $lnk19 = $row->FileName;
                $this->data['img19'] = $row->Image;
                $lnkTitle19 = $row->LinkTitle;
            }
            else if($row->PageId == '20')
            {
                $title20 = $row->PageTitle;
                $text20 = substr(stripslashes($row->PageText), 0, 200);
                if(strlen($row->PageText) > 200)
                    $text20 .= '...</p>';
                if($row->FileName == '')
                    $lnk20 = "home/page?id=".$row->PageId;
                if($row->SEOURLName != '')
                    $lnk20 = $row->SEOURLName.'.xhtml';
                else if($row->FileName != '')
                    $lnk20 = $row->FileName;
                $img20 = $row->Image;
                $lnkTitle20 = $row->LinkTitle;
            }
            else if($row->PageId == '21')
            {
                $title21 = $row->PageTitle;
                $text21 = substr(stripslashes($row->PageText), 0, 200);
                if(strlen($row->PageText) > 200)
                    $text21 .= '...</p>';
                if($row->FileName == '')
                    $lnk21 = "home/page?id=".$row->PageId;
                if($row->SEOURLName != '')
                    $lnk21 = $row->SEOURLName.'.xhtml';
                else if($row->FileName != '')
                    $lnk21 = $row->FileName;
                $img21 = $row->Image;
                $lnkTitle21 = $row->LinkTitle;
            }
            else if($row->PageId == '3')
            {
                $titleTtm = $row->PageTitle;
                $imgTtm = $row->Image;
            }
            else if($row->PageId == '22')
            {
                $img22 = $row->Image;
            }
            else if($row->PageId == '23')
            {
                $img23 = $row->Image;
            }
            else if($row->PageId == '24')
            {
                $img24 = $row->Image;
            }
            else if($row->PageId == '25')
            {
                $this->data['title25'] = $row->PageTitle;
                $this->data['text25'] = stripslashes($row->PageText);
                $img25 = $row->Image;
            }
            else if($row->PageId == '26')
            {
                $title26 = $row->PageTitle;
                $text26 = stripslashes($row->PageText);
                $img26 = $row->Image;
                if($row->FileName == '')
                    $lnk26 = "home/page?id=".$row->PageId;
                if($row->SEOURLName != '')
                    $lnk26 = $row->SEOURLName.'.xhtml';
                else if($row->FileName != '')
                    $lnk26 = $row->FileName;
            }
            else if($row->PageId == '27')
            {
                $text27 = stripslashes($row->PageText);
                $img27 = $row->Image;
            }
            else if($row->PageId == '28')
            {
                $text28 = stripslashes($row->PageText);
                $img28 = $row->Image;
            }
            else if($row->PageId == '29')
            {
                $text29 = stripslashes($row->PageText);
                $img29 = $row->Image;
            }
            else if($row->PageId == '30')
            {
                $img30 = $row->Image;
            }
            else if($row->PageId == '31')
            {
                $img31 = $row->Image;
            }
            else if($row->PageId == '4')
            {
                $titleH4 = $row->PageTitle;
                $textH4 = stripslashes($row->PageText);
                $imgH4 = $row->Image;
                if($row->FileName == '')
                    $lnkH4 = "home/page?id=".$row->PageId;
                if($row->SEOURLName != '')
                    $lnkH4 = $row->SEOURLName.'.xhtml';
                else if($row->FileName != '')
                    $lnkH4 = $row->FileName;
            }
            else if($row->PageId == '6')
            {
                $this->data['title1'] = $row->PageTitle;
               
                $this->data['text1'] = stripslashes($row->PageText);
                if($row->FileName == '')
                    $lnk1 = "home/page?id=".$row->PageId;
                if($row->SEOURLName != '')
                    $lnk1 = $row->SEOURLName.'.xhtml';
                else if($row->FileName != '')
                    $lnk1 = $row->FileName;
                $img1 = $row->Image;
            }
            else if($row->PageId == '7')
            {
                $this->data['title2'] = $row->PageTitle;
                $this->data['text2'] = stripslashes($row->PageText);
                if($row->FileName == '')
                    $lnk2 = "home/page?id=".$row->PageId;
                if($row->SEOURLName != '')
                    $lnk2 = $row->SEOURLName.'.xhtml';
                else if($row->FileName != '')
                    $lnk2 = $row->FileName;
                $img2 = $row->Image;
            }
            else if($row->PageId == '8')
            {
                $this->data['title3'] = $row->PageTitle;
                $this->data['text3'] = stripslashes($row->PageText);
                if($row->FileName == '')
                    $lnk3 = "home/page?id=".$row->PageId;
                if($row->SEOURLName != '')
                    $lnk3 = $row->SEOURLName.'.xhtml';
                else if($row->FileName != '')
                    $lnk3 = $row->FileName;
                $img3 = $row->Image;
            }
            else if($row->PageId == '14')
            {
                $this->data['title4'] = $row->PageTitle;
                $this->data['text4'] = stripslashes($row->PageText);
                if($row->FileName == '')
                    $lnk4 = "home/page?id=".$row->PageId;
                if($row->SEOURLName != '')
                    $lnk4 = $row->SEOURLName.'.xhtml';
                else if($row->FileName != '')
                    $lnk4 = $row->FileName;
                $img4 = $row->Image;
            }
            else if($row->PageId == '2' || $row->PageId == '15')
            {
                $title5 = $row->PageTitle;
                $text5 = stripslashes($row->PageText);
                if($row->FileName == '')
                    $lnk5 = "home/page?id=".$row->PageId;
                if($row->SEOURLName != '')
                    $lnk5 = $row->SEOURLName.'.xhtml';
                else if($row->FileName != '')
                    $lnk5 = $row->FileName;
                $img5 = $row->Image;
            }
            else if($row->PageId == '9')
            {
                $title6 = $row->PageTitle;
                $text6 = stripslashes($row->PageText);
                if($row->FileName == '')
                    $lnk6 = "home/page?id=".$row->PageId;
                if($row->SEOURLName != '')
                    $lnk6 = $row->SEOURLName.'.xhtml';
                else if($row->FileName != '')
                    $lnk6 = $row->FileName;
                $img6 = $row->Image;
            }
            else if($row->PageId == '17')
            {
                $this->data['text7'] = stripslashes($row->PageText);
            }
            else if($row->PageId == '11')
            {
                $this->data['title11'] = $row->PageTitle;
                $this->data['text11'] = stripslashes($row->PageText);
                $img11 = $row->Image;
            }
            if($row->PageId == '1')
            {
                $homeTitle = $row->PageTitle;
                $homeTxt = stripslashes($row->PageText);
                $homeImg = $row->Image;
                $this->data['META_KW_CNTNTS'] = stripslashes($row->MetaKW);
                $this->data['META_DESC_CNTNTS'] = stripslashes($row->MetaTags);
                $TITLE = stripslashes($row->HTMLTitle);
            }
            if($row->HeaderLink == '1' && $row->PageId != '12')
            {
                $class = 'menu_links';
                if($THEME == '1')
                {
                    $edtLnk = '';
                    if(trim($this->data['EDIT_TXT']) != '') 
                        $edtLnk = str_replace('ANCHORLINK','<a href="'.base_url().'home/page?id='.$row->PageId.'&frmId=44&fTypeId=8" target="blank">Edit Link</a>', $this->data['EDIT_TXT']);
                    $this->data['strHeaderLnks'] = '<li '.$edtLnk.'><a '.$target.' href="'.$lnk.'"><span>'.$row->LinkTitle.'</span></a></li>';
                }
                else if($THEME == '4')
                {
                    $edtLnk = '';
                    $this->data['strHeaderLnks'] = '<li><a '.$target.' href="'.$lnk.'">'.$row->LinkTitle.'</a></li>';
                }
                else if($THEME == '5')
                {
                    $edtLnk = '';
                    $this->data['strHeaderLnks'] = '<li><a '.$target.' href="'.$lnk.'"><span>'.$row->LinkTitle.'</span></a></li>';
                }
            }
            if($row->FooterLink == '1')
            {
                $edtLnk = '';
                if(trim($this->data['EDIT_TXT']) != '') 
                    $edtLnk = str_replace('ANCHORLINK', '<a style="color:#000;" href="'.base_url().'home/page?id='.$row->PageId.'&frmId=44&fTypeId=8" target="blank">Edit Link</a>', $this->data['EDIT_TXT']);
                $this->data['strFtrLnks'] .= '<li '.$edtLnk.'><a '.$target.' href="'.$lnk.'">'.$row->LinkTitle.'</a></li>';
                $this->data['strFtrLnksNew'] .= '<li '.$edtLnk.'><a '.$target.' href="'.$lnk.'"><span>'.$row->LinkTitle.'</span></a></li>';
                $this->data['strFtrLnksFooter'] .= '<li style="margin-bottom:10px" '.$edtLnk.'><span style="color:#f94203;margin-right:10px">→</span><a '.$target.' href="'.$lnk.'">'.$row->LinkTitle.'</a></li>';
            }
            if($row->SubMenuLink == '1')
            {
                $edtLnk = '';
                if(trim($this->data['EDIT_TXT']) != '') 
                    $edtLnk = str_replace('ANCHORLINK','<a href="'.base_url().'home/page?id='.$row->PageId.'&frmId=44&fTypeId=8" target="blank">Edit Link</a>', $this->data['EDIT_TXT']);
                $this->data['strSubMenuLnks'] .= '<li '.$edtLnk.' class="nav-item"><a class="nav-link" style="color:#FFF;" '.$target.' href="'.$lnk.'">'.$row->LinkTitle.'</a></li>';
            }
            $i++;
        }
        $this->data['rsBanners'] = $this->General_model->fetch_banners();
        $this->data['rsProds'] = $this->General_model->fetch_products();
        $this->data['rsSrvcs'] = $this->General_model->fetch_retail_services();
        

        $row = $this->General_model->fetch_page_by_id();

        if($row->MetaKW != '')
            $this->data['META_KW_CONTENTS'] = stripslashes($row->MetaKW);
        if($row->MetaTags != '')
            $this->data['META_DESC_CONTENTS'] = stripslashes($row->MetaTags);
        if($row->HTMLTitle != '')
            $this->data['SITE_TITLE'] = stripslashes($row->HTMLTitle);

        $currDt = setDtTmWRTYourCountry();    
        $row = $this->General_model->fetch_userlog_count($currDt); 
        
        $onlineUsers = $row->TotalUsers;
        $rsHBrands = $this->General_model->fetch_manufacturer();
            
    }
    

   
}
