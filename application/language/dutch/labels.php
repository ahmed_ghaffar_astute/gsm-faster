<?
$lang['CUST_INDEX_USRNAME'] = " Gebruikersnaam " ;
$lang['CUST_INDEX_PWD'] = 'Wachtwoord' ;
$lang['CUST_DB_HEADER'] = "Dashboard " ;
$lang['CUST_DB_YPR'] = "Uw Services Verzoeken " ;
$lang['CUST_DB_WELCOME'] = " Welkom " ;
$lang['CUST_DB_CRDTS'] = " Your Credits" ;
$lang['CUST_MENU_CODES'] = " Plaats uw bestelling " ;
$lang['CUST_MENU_CREDITS'] = " Credits" ;
$lang['CUST_MENU_STATS'] = "Orders geschiedenis " ;
$lang['CUST_MENU_PWD'] = " wachtwoord";
$lang['CUST_MENU_CODES_1'] = " Plaats IMEI Order" ;
$lang['CUST_MENU_CODES_2'] = " IMEI Bestellingen geschiedenis " ;
$lang['CUST_MENU_CREDITS_1'] = " Credits kopen " ;
$lang['CUST_MENU_CREDITS_2'] = " Credits Geschiedenis " ;
$lang['CUST_MENU_STATS_1'] = " Statistieken " ;
$lang['CUST_MENU_PWD_1'] = " Wachtwoord wijzigen " ;
$lang['CUST_DB_ICON1'] = " Mijn Account" ;
$lang['CUST_DB_ICON1_1'] = " Beheer uw account gegevens " ;
$lang['CUST_DB_ICON2'] = " Plaats uw bestelling " ;
$lang['CUST_DB_ICON2_1'] = " Plaats Bestellingen" ;
$lang['CUST_DB_ICON3'] = "Orders geschiedenis " ;
$lang['CUST_DB_ICON3_1'] = "Orders Vraag Geschiedenis " ;
$lang['CUST_DB_ICON4'] = " Credits Geschiedenis " ;
$lang['CUST_DB_ICON4_1'] = "Uw Credits Vraag Geschiedenis " ;
$lang['CUST_DB_ICON5'] = "Statistieken" ;
$lang['CUST_DB_ICON5_1'] = " Statistieken " ;
$lang['CUST_DB_ICON6'] = " Credits kopen " ;
$lang['CUST_DB_ICON6_1'] = " Krediet Kopen " ;
$lang['CUST_DB_ICON7'] = " wachtwoord";
$lang['CUST_DB_ICON7_1'] = " Uw wachtwoord wijzigen " ;
$lang['CUST_CODE_HEADING'] = " Plaats uw bestelling " ;
$lang['CUST_CODE_1'] = " Credits beschikbaar" ;
$lang['CUST_CODE_2'] = "Service" ;
$lang['CUST_CODE_3'] = " IMEI Type" ;
$lang['CUST_CODE_4'] = " IMEI " ;
$lang['CUST_CODE_5'] = " Model No " ;
$lang['CUST_CODE_6'] = " Credits Verplicht " ;
$lang['CUST_CODE_7'] = " Gemaakt " ;
$lang['CUST_CODE_8'] = " Belangrijke informatie " ;
$lang['CUST_CODE_9'] = "Land " ;
$lang['CUST_CODE_10'] = " Info Reacties " ;
$lang['CUST_CODE_11'] = " Commentaar" ;
$lang['CUST_CODE_MSG1'] = " IMEI bestaat al in ons systeem . " ;
$lang['CUST_CODE_MSG2'] = "U hebt onvoldoende credits aan te schaffen Schaf credits! . " ;
$lang['CUST_CODE_MSG3'] = " Uw bestelling is succesvol ingediend! " ;

$lang['CUST_CH_HEADING'] = "Orders geschiedenis " ;
$lang['CUST_CH_1'] = " IMEI # " ;
$lang['CUST_CH_2'] = "Service" ;
$lang['CUST_CH_3'] = " Order Status " ;
$lang['CUST_CH_4'] = "model" ;
$lang['CUST_CH_5'] = " Status " ;
$lang['CUST_CH_6'] = "Datum" ;
$lang['CUST_CH_7'] = " Extra informatie " ;
$lang['CUST_CH_BTN'] = "Zoeken " ;

$lang['CUST_PWD_HEADING'] = " Wachtwoord wijzigen " ;
$lang['CUST_PWD_1'] = " Gebruikersnaam " ;
$lang['CUST_PWD_2'] = " Oud wachtwoord " ;
$lang['CUST_PWD_3'] = " Nieuw wachtwoord " ;
$lang['CUST_PWD_4'] = "Bevestig wachtwoord";

$lang['CUST_ST_HEADING'] = " Mijn statistieken " ;
$lang['CUST_ST_1'] = " Type melding " ;
$lang['CUST_ST_2'] = " Grafische Report" ;
$lang['CUST_ST_3'] = "Tekst Report" ;
$lang['CUST_ST_4'] = " Van Datum " ;
$lang['CUST_ST_5'] = " Van Datum " ;
$lang['CUST_ST_6'] = "To Date " ;
$lang['CUST_ST_7'] = "Rapport" ;
$lang['CUST_ST_8'] = " Rapport van het Krediet " ;
$lang['CUST_ST_9'] = "Services Report" ;
$lang['CUST_ST_10'] = "Orders Report" ;
$lang['CUST_ST_11'] = " Totaal Bestellingen Report" ;

$lang['CUST_CRP_HEADING'] = "Uw krediet geschiedenis " ;
$lang['CUST_CRP_1'] = "Beschrijving " ;
$lang['CUST_CRP_2'] = " IMEI " ;
$lang['CUST_CRP_3'] = " Credits" ;
$lang['CUST_CRP_4'] = " Credits Links " ;
$lang['CUST_CRP_5'] = "Datum" ;

$lang['CUST_BC_HEADING'] = " Credits kopen " ;
$lang['CUST_BC_1'] = " Credits" ;
$lang['CUST_BC_2'] = " Credit Prijs " ;
$lang['CUST_BC_3'] = " Totale Prijs " ;
$lang['CUST_BC_4'] = " Betaling Fee " ;
$lang['CUST_BC_5'] = " Fee Bedrag" ;
$lang['CUST_BC_6'] = " Totaal te betalen " ;
$lang['CUST_BC_7'] = " . Zelfs als je niet een PayPal- account U kunt hier betalen met een creditcard. " ;
$lang['CUST_BC_8'] = " Als u al een Premiur of Business- Paypal-account , kunt u betalen uit uw saldo zonder enige vergoeding . " ;
$lang['CUST_BC_9'] = " Bulk Credit aanbiedingen" ;

$lang['CUST_ACC_HEADING'] = " Mijn Account" ;
$lang['CUST_ACC_1'] = " Userame " ;
$lang['CUST_ACC_2'] = " Voornaam " ;
$lang['CUST_ACC_3'] = " Achternaam " ;
$lang['CUST_ACC_4'] = " Bedrijf" ;
$lang['CUST_ACC_5'] = "Adres " ;
$lang['CUST_ACC_6'] = "Land " ;
$lang['CUST_ACC_7'] = "City " ;
$lang['CUST_ACC_8'] = " Staat / Regio " ;
$lang['CUST_ACC_9'] = " Zip / Postal Order" ;
$lang['CUST_ACC_10'] = " Fax " ;
$lang['CUST_ACC_MSG'] = " Uw account informatie is bijgewerkt ! " ;

$lang['CUST_GEN_MSG'] = "Operatie is succesvol afgerond ! " ;
$lang['CUST_GEN_MSG1'] = " ! Fout tijdens de interactie met de database " ;
$lang['CUST_GEN_MSG2'] = " Er bestaat al selecteer een andere waarde . " ;
$lang['CUST_GEN_MSG3'] = " Oud wachtwoord is niet correct! " ;
$lang['CUST_GEN_MSG4'] = "Operatie is succesvol afgerond ! " ;


$lang['CUST_MENU_CODES_3'] = " File Services - SL3LBF " ;
$lang['CUST_LBL_1'] = " Plaats SL3 LBF Order" ;
$lang['CUST_LBL_2'] = " Bestand " ;
$lang['CUST_LBL_3'] = " PLEASE WAIT" ;
$lang['CUST_LBL_4'] = "Voer een IMEI per regel . " ;
$lang['CUST_LBL_5'] = " Als u meerdere IMEI druk op enter enter om naar de volgende regel en schrijf dan IMEI . " ;
$lang['CUST_LBL_6'] = " Single IMEI " ;
$lang['CUST_LBL_7'] = " Meerdere IMEIs " ;
$lang['CUST_LBL_8'] = " SL3 LBF Bestellingen geschiedenis " ;
$lang['CUST_LBL_9'] = "Weergave " ;
$lang['CUST_LBL_10'] = " ! Geen gevonden record " ;

$lang['CUST_LBL_11'] = " Serviceaanvragen " ;
$lang['CUST_LBL_12'] = " Plaats Server Service Order " ;
$lang['CUST_LBL_13'] = " Server service bestellingen Geschiedenis " ;
$lang['CUST_LBL_14'] = " SL3 BF - BB5 RPL " ;
$lang['CUST_LBL_15'] = " Nieuwsbericht " ;
$lang['CUST_LBL_16'] = " Nieuws bestaat niet ! " ;
$lang['CUST_LBL_17'] = " XML API " ;
$lang['CUST_LBL_18'] = " Genereer API Key" ;
$lang['CUST_LBL_19'] = " ! Genereren van nieuwe API-sleutel wordt de eerder gegenereerde sleutel inactief direct te maken " ;
$lang['CUST_LBL_20'] = " Download Voorbeeld Order " ;
$lang['CUST_LBL_21'] = " Download API Document " ;
$lang['CUST_LBL_22'] = "Rapporten" ;
$lang['CUST_LBL_23'] = " IP Log Report" ;
$lang['CUST_LBL_24'] = " IP " ;
$lang['CUST_LBL_25'] = "Login Date - Van " ;
$lang['CUST_LBL_26'] = "Login Date - To " ;
$lang['CUST_LBL_27'] = "Login Tijd " ;
$lang['CUST_LBL_28'] = " logout tijd " ;
$lang['CUST_LBL_29'] = "Zoeken " ;
$lang['CUST_LBL_30'] = "Laatste 10 Logins " ;
$lang['CUST_LBL_31'] = "Selecteer een record! " ;
$lang['CUST_LBL_32'] = " Gearchiveerd IMEI Bestellingen geschiedenis " ;
$lang['CUST_LBL_33'] = " Export Orders" ;
$lang['CUST_LBL_34'] = " Archief Bestellingen" ;
$lang['CUST_LBL_35'] = "Un - Archief bestellingen" ;
$lang['CUST_LBL_36'] = " Gearchiveerd SL3 LBF Bestellingen geschiedenis " ;
$lang['CUST_LBL_37'] = " uw valuta " ;
$lang['CUST_LBL_38'] = " Afmelden " ;
$lang['CUST_LBL_39'] = " Show" ;
$lang['CUST_LBL_40'] = " verbergen " ;
$lang['CUST_LBL_41'] = " Verzenden" ;
$lang['CUST_LBL_42'] = "Uw wachtwoord is met succes gewijzigd ! " ;
$lang['CUST_LBL_43'] = " ! Ongeldig Oud wachtwoord " ;
$lang['CUST_LBL_44'] = "Nieuwe en bevestigen wachtwoorden komen niet overeen ! " ;
$lang['CUST_LBL_45'] = "U hebt niet voldoende kredieten om een bestelling te plaatsen ! " ;
$lang['CUST_LBL_46'] = " Order" ;
$lang['CUST_LBL_47'] = " Server Bestellingen" ;
$lang['CUST_LBL_48'] = " Server-service " ;
$lang['CUST_LBL_49'] = "Vak S / N " ;
$lang['CUST_LBL_50'] = " Server Bestellingen" ;
$lang['CUST_LBL_51'] = " Betaling Type" ;
$lang['CUST_LBL_52'] = " Export Credit Geschiedenis " ;
$lang['CUST_LBL_53'] = " Usage Rate" ;
$lang['CUST_LBL_54'] = " Aantal Orders" ;
$lang['CUST_LBL_55'] = "Beschikbaar" ;
$lang['CUST_LBL_56'] = "Niet beschikbaar" ;
$lang['CUST_LBL_57'] = " In Process " ;
$lang['CUST_LBL_58'] = "Beschikbaarheid Rate" ;
$lang['CUST_LBL_59'] = "Klanten " ;
$lang['CUST_LBL_60'] = " Klant toevoegen " ;
$lang['CUST_LBL_61'] = "Update prijzen" ;
$lang['CUST_LBL_62'] = "Weet u zeker dat u de API- sleutel te genereren ? " ;
$lang['CUST_LBL_63'] = " . Het zal alle records met betrekking tot deze gebruiker te verwijderen Weet u zeker dat u deze gebruiker wilt verwijderen?" ;
$lang['CUST_LBL_64'] = "Record is successfuly verwijderd";
$lang['CUST_LBL_65'] = "Operatie is succesvol afgerond ! " ;
$lang['CUST_LBL_66'] = " Credits kunnen alleen tussen 0 en 1000 ! " ;
$lang['CUST_LBL_67'] = " Ongeldige Prijs voor " ;
$lang['CUST_CH_59'] = "Orders statussen " ;
$lang['CUST_CH_60'] = " Totaal aantal bestellingen" ;
$lang['CUST_CH_61'] = "Orders beschikbaar" ;
$lang['CUST_CH_62'] = "Orders niet beschikbaar " ;
$lang['CUST_CH_63'] = " Recent Nieuws " ;
$lang['CUST_CH_64'] = " Export Orders" ;
$lang['CUST_CH_65'] = " uw diensten " ;
$lang['CUST_CH_66'] = " Prijs " ;
$lang['CUST_CH_67'] = "Categorie " ;
$lang['CUST_CH_68'] = " Levertijd " ;
$lang['CUST_CH_69'] = " Prijs For You " ;
$lang['CUST_CH_70'] = " Veelgestelde vragen " ;
$lang['CUST_CH_71'] = " Lijst Alle diensten " ;
$lang['CUST_CH_72'] = " Download " ;
$lang['CUST_CH_73'] = " API " ;
$lang['CUST_CH_74'] = " API Key" ;
$lang['CUST_CH_75'] = " Download API Document " ;
$lang['CUST_CH_76'] = " Download Sample Code " ;
$lang['CUST_CH_77'] = " API Key" ;
$lang['CUST_CH_78'] = "Uw API Key is " ;
$lang['CUST_CH_79'] = " Valuta " ;
$lang['CUST_CH_80'] = " Zoeken IMEI ... " ;
$lang['CUST_CH_81'] = " Serienummer " ;
$lang['CUST_CH_82'] = " Box Gebruikersnaam" ;
$lang['CUST_CH_83'] = " Hash " ;
$lang['CUST_CH_84'] = " ! Invalid file Selecteer een geldige hash file ! " ;
$lang['CUST_CH_85'] = " Selecteer een bestand " ;
$lang['CUST_CH_86'] = "Controleer Iphone " ;
$lang['CUST_CH_87'] = " IMEI-nummer ( s ) indienen " ;
$lang['CUST_CH_88'] = " Lijst IMEI-nummer ( s ) " ;
$lang['CUST_CH_89'] = " Archief IMEI-nummer ( s ) " ;
$lang['CUST_CH_90'] = "Uw IMEI-nummer ( s) zijn succesvol ingediend! " ;
$lang['CUST_CH_91'] = " Credits Verplicht Per IMEI " ;
$lang['CUST_CH_92'] = " Aantal Records " ;
$lang['CUST_CH_93'] = " Zoek IMEI-nummer ( s ) " ;
$lang['CUST_CH_94'] = " Lock Status " ;
$lang['CUST_CH_95'] = "Carrier Info" ;
$lang['CUST_CH_96'] = " gevraagd op " ;
$lang['CUST_CH_97'] = " Time Return" ;
$lang['CUST_CH_98'] = " Alternate E-mail" ;
$lang['CUST_CH_99'] = "Details " ;
$lang['CUST_CH_100'] = "Verify " ;
$lang['CUST_CH_101'] = " Beheer Cliënten " ;
$lang['CUST_CH_102'] = "Klanten " ;
$lang['CUST_CH_103'] = " zoeken Client ( s ) " ;
$lang['CUST_CH_104'] = "Klanten " ;
$lang['CUST_CH_105'] = "Naam" ;
$lang['CUST_CH_106'] = "Uitgeschakeld ";
$lang['CUST_CH_107'] = " Bewerken" ;
$lang['CUST_CH_108'] = " service prijzen" ;
$lang['CUST_CH_109'] = " Server-service prijzen" ;
$lang['CUST_CH_110'] = "Stel Packs " ;
$lang['CUST_CH_111'] = " Verwijderen" ;
$lang['CUST_CH_112'] = " Klant toevoegen " ;
$lang['CUST_CH_113'] = "Client " ;
$lang['CUST_CH_114'] = " Telefoon " ;
$lang['CUST_CH_115'] = " Prijs Plan" ;
$lang['CUST_CH_116'] = "Client List " ;
$lang['CUST_CH_117'] = "Land " ;
$lang['CUST_CH_118'] = " IP ( s ) " ;
$lang['CUST_CH_119'] = " uitschakelen " ;
$lang['CUST_CH_120'] = " Toevoegen / Aftrekken Credits" ;
$lang['CUST_CH_121'] = "Huidige Credits" ;
$lang['CUST_CH_122'] = " Credit Type" ;
$lang['CUST_CH_123'] = "Toevoegen" ;
$lang['CUST_CH_124'] = " Trek " ;
$lang['CUST_CH_125'] = " Payment Date " ;
$lang['CUST_CH_126'] = " Status betaling " ;
$lang['CUST_CH_127'] = " Payment Method " ;
$lang['CUST_CH_128'] = " -transactie-id " ;
$lang['CUST_CH_129'] = "Client " ;
$lang['CUST_CH_130'] = " Prijs Plan" ;
$lang['CUST_CH_131'] = " service prijzen" ;
$lang['CUST_CH_132'] = " Dienst titel " ;
$lang['CUST_CH_133'] = " Prijs " ;
$lang['CUST_CH_134'] = " Server-service prijzen" ;
$lang['CUST_CH_135'] = "Log Dienst titel " ;
$lang['CUST_CH_136'] = "Stel Services voor klanten " ;
$lang['CUST_CH_137'] = " Alles selecteren" ;
$lang['CUST_CH_138'] = " Ongeldige Prijs voor " ;
$lang['CUST_CH_139'] = "Het kan niet minder zijn dan" ;
$lang['CUST_CH_140'] = "Selecteer deze packs , die u wilt verwijderen tegen deze geselecteerde klant en klik op Verzenden De geselecteerden zullen worden verwijderd uit de lijst packs in login deze specifieke klant. ";
$lang['CUST_CH_141'] = " Ongeldige tegoed! " ;
$lang['CUST_CH_142'] = " Credits kunnen alleen tussen 0 en 1000 ! " ;
$lang['CUST_CH_143'] = " Credit Geschiedenis " ;
$lang['CUST_CH_144'] = " Credits kunnen niet groter zijn dan" ;
$lang['CUST_CH_145'] = "Instellingen " ;
$lang['CUST_CH_146'] = " IMEI Services" ;
$lang['CUST_CH_147'] = " File Services " ;
$lang['CUST_CH_148'] = " Server Services " ;
$lang['CUST_LBL_149'] = " Order volgen " ;
$lang['CUST_LBL_150'] = " Reviews " ;
$lang['CUST_LBL_151'] = "Plaats een beoordeling" ;
$lang['CUST_LBL_152'] = " Dashboard / Orders " ;
$lang['CUST_LBL_153'] = " Bekijk Dashboard " ;
$lang['CUST_LBL_154'] = " Plaats IMEI Service Order " ;
$lang['CUST_LBL_155'] = " Plaats Sl3 LBF Order" ;
$lang['CUST_LBL_156'] = " Plaats Server Service Order " ;
$lang['CUST_LBL_157'] = " Top 10 Dienst Verzoeken " ;
$lang['CUST_LBL_158'] = " File Option : * " ;
$lang['CUST_LBL_159'] = " Zangeres Zonder Naam " ;

$lang['CUST_LBL_160'] = " Kies een categorie ... " ;
$lang['CUST_LBL_161'] = "Kies een service ... " ;
$lang['CUST_LBL_162'] = "Kies een service ... " ;
$lang['CUST_LBL_163'] = " Kies Order Status ... " ;
$lang['CUST_LBL_164'] = " Andere " ;
$lang['CUST_LBL_165'] = " Lijst Alle IMEI Services " ;
$lang['CUST_LBL_166'] = " Toon Alle File Services " ;
$lang['CUST_LBL_167'] = " Lijst Alle Server Services " ;
$BE_LBL_274 = "Kies een service ... " ;
$lang['CUST_LBL_171'] = " Bekende formaten: " ;
$lang['CUST_LBL_172'] = " Bulk File " ;
$lang['CUST_LBL_173'] = " Onze diensten " ;
$lang['CUST_LBL_174'] = "Categorie " ;
$lang['CUST_LBL_175'] = " Aankoopprijs" ;
$lang['CUST_LBL_176'] = "Diensten" ;
$lang['CUST_LBL_177'] = " IMEI service" ;
$lang['CUST_LBL_178'] = " Weigeren " ;
$lang['CUST_LBL_179'] = " IMEI Bestellingen" ;
$lang['CUST_LBL_180'] = "Antwoorden " ;
$lang['CUST_LBL_181'] = " Andere informatie " ;
$lang['CUST_LBL_182'] = "Serial # " ;
$lang['CUST_LBL_183'] = " Uitbetaling Bedrag" ;
$lang['CUST_LBL_184'] = " API-instellingen " ;
$lang['CUST_LBL_185'] = " API Access Key" ;
$lang['CUST_LBL_186'] = " API- IP ( s ) " ;
$lang['CUST_LBL_187'] = "Toon API Key" ;
$lang['CUST_LBL_188'] = "Reset IP " ;
$lang['CUST_LBL_189'] = " API Key is succesvol gegenereerd ! " ;

$BE_LBL_285 = "nieuwe bestellingen" ;
$BE_LBL_286 = "Quick Accept" ;
$BE_LBL_287 = " Service verstandig Accept" ;
$BE_LBL_288 = " Antwoord Bestellingen" ;
$BE_LBL_289 = "Handmatig antwoord" ;
$BE_LBL_290 = " Bulk antwoord" ;
$BE_LBL_291 = "Orders geschiedenis " ;
$BE_LBL_292 = "Orders Verificatie " ;
$BE_CODE_6 = "Code" ;
$BE_LBL_40 = " Download IMEIs " ;
$BE_LBL_304 = " Accepteer geselecteerd" ;
$BE_LBL_305 = " Alle Accept" ;
$BE_LBL_306 = " Weigeren geselecteerd" ;
$BE_LBL_307 = " Alle Weigeren " ;
$BE_GNRL_9 = " ! Geen gevonden record " ;
$BE_LBL_72 = " Verzenden" ;
$BE_LBL_231 = "Nieuwe IMEI Bestellingen In afwachting van actie" ;
$BE_LBL_232 = " IMEI bestellingen in uitvoering " ;
$BE_LBL_233 = " IMEI Bestellingen In afwachting van verificatie " ;
$BE_LBL_247 = " Toegelaten IMEI Bestellingen" ;
$BE_LBL_248 = " IMEI Order History " ;
$BE_LBL_233 = " IMEI Bestellingen In afwachting van verificatie " ;
$BE_LBL_234 = " Nieuw bestand Bestellingen In afwachting van actie" ;
$BE_LBL_250 = " Accepted File Bestellingen" ;
$BE_LBL_251 = "File Order History " ;
$BE_LBL_236 = "Bestand Bestellingen In afwachting van verificatie " ;
$BE_LBL_237 = " Nieuwe server Bestellingen In afwachting van actie" ;
$BE_LBL_253 = " Accepted Server Bestellingen" ;
$BE_LBL_254 = " Server Order History " ;
$BE_LBL_239 = " Server Bestellingen In afwachting van verificatie " ;
$BE_LBL_183 = "Orders worden gecontroleerd " ;
$BE_LBL_69 = " Selecteer status" ;
$BE_LBL_1 = "File Services Order" ;
?>