<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Template_model extends CI_Model {
	public function getAllTemplates(){
		$query = $this->db->query("SELECT TemplateId, TemplateName FROM tbl_gf_email_templates ORDER BY TemplateName");
		return $query->result();
	}
	public function getTemplateIdByName($name,$id){
		$query = $this->db->query("SELECT TemplateId FROM tbl_gf_email_templates WHERE TemplateName = '$name' AND TemplateId <> $id");
		return $query->result();
	}
	public function getTemplateById($id){
		$query = $this->db->query("SELECT * FROM tbl_gf_email_templates WHERE TemplateId = '$id'");
		return $query->row();
	}
	public function updateTemplate($name,$subject,$contents,$sendCopy,$id){
		$this->db->set("TemplateName",$name);
		$this->db->set('Subject',$subject);
		$this->db->set('Contents',$contents);
		$this->db->set('SendCopyToAdmin',$sendCopy);
		$this->db->where('TemplateId',$id);
		$this->db->update('tbl_gf_email_templates');
		return true;
	}
}
