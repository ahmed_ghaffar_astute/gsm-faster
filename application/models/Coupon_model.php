<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**

*

*/

class Coupon_model extends CI_Model
{

    public function coupon_list($sortBy='id', $sort='ASC', $limit='', $start='', $keyword=''){

      $this->db->select('*');
      $this->db->from('tbl_coupon'); 
      if($limit!=''){
        $this->db->limit($limit, $start);
      }
      if($keyword!=''){
        $this->db->like('coupon_code',$keyword);
      }
      
      $this->db->order_by($sortBy,$sort);
      return $this->db->get()->result();
    }

    public function single_coupon($id){

      $this->db->select('*');
      $this->db->from('tbl_coupon');
      $this->db->where('id', $id); 
      $this->db->limit(1);
      $query = $this->db->get();
      if($query -> num_rows() == 1){                 
          return $query->result();
      }
      else{
          return false;
      }
    }

    public function delete($id){

      $this->db->select('*');
      $this->db->from('tbl_coupon');
      $this->db->where('id', $id); 
      $this->db->limit(1);
      $query = $this->db->get();
      if($query -> num_rows() == 1){                 
          $row=$query->result();

          if(file_exists('assets/images/coupons/'.$row[0]->coupon_image))  
            unlink('assets/images/coupons/'.$row[0]->coupon_image);

          $this->db->where('id', $id);
          $this->db->delete('tbl_coupon');
          return 'success';
      }
      else{
          return 'failed';
      } 
    }

}