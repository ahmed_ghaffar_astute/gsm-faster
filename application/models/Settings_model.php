<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends CI_Model
{
	public function deleteAPI($apiId)
	{
			$query = $this->db->query("SELECT APIId FROM tbl_gf_api WHERE SystemAPI = 0 AND APIId = '$apiId'");
			$row = $query->row();
			if (isset($row->APIId) && $row->APIId != '')
			{
				$this->db->query("UPDATE tbl_gf_packages SET APIId = 0 WHERE APIId = '".$row->APIId."'");
				$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '".$row->APIId."'");
				$this->db->query("DELETE FROM tbl_gf_api WHERE APIId = '".$row->APIId."'");
			}
	}
	public function getApis(){
		$rsAPIs = $this->db->query('SELECT APIId, APITitle, AccountId, ServerURL, DisableAPI, SystemAPI FROM tbl_gf_api ORDER BY APITitle');

		return $rsAPIs->result();
	}
	public function getSyncIMEI($id)
	{
		$rsAPIs = $this->db->query("SELECT APITitle, APIKey, ServerURL, AccountId, ServiceId, APIType FROM tbl_gf_api WHERE APIId = '$id'");

		return $rsAPIs->result();
	}
	//		$count = $objDBCD14->numRows($rsAPIs);

	public function SetApiInUse($id)
	{
		$this->db->query("UPDATE tbl_gf_api SET APIInUse = '1' WHERE APIId = '$id'");
	}

	public function getApi($apiTitle, $id)
	{
		$query = "SELECT APIId FROM tbl_gf_api WHERE APITitle = '$apiTitle'";
		if($id > 0)
			$query .= " AND APIId <> '$id'";
		$row = $this->db->query($query);
		return $row->result();

	}
	public function getNextApiID()
	{
		$result = $this->db->query("SELECT MAX(APIId) AS CurrAPIId FROM tbl_gf_api WHERE APIId < 201");

		return $result->row();

	}

	public function addApi($query)
	{
		$this->db->query($query);
	}

	public function getApiForID($id)
	{
		$query = "SELECT * FROM tbl_gf_api WHERE ";
		$query .= " APIId = '$id'";
		$row = $this->db->query($query);
		return $row->row();

	}

	public function updateApi($query)
	{
		$this->db->query($query);
	}

	public function archiveServicesPackage1()
	{
		$this->db->query("UPDATE tbl_gf_package_category SET ArchivedCategory = '1' WHERE SL3BF = '0'");
	}

	public function addServicesPackage($value)
	{
		$this->db->query("INSERT INTO tbl_gf_package_category (Category, DisableCategory, SL3BF) VALUES ('" . check_input($value, $this->db->conn_id) . "', 0, 0)");
	}

	public function addServicesPackage2($value)
	{
		$this->db->query("INSERT INTO tbl_gf_package_category (CategoryId, Category, DisableCategory, SL3BF) VALUES $value");
	}

	public function deleteSupplierServicesZero($id)
	{
		$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
	}

	public function deleteSupplierServicesOne($id)
	{
		$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '1'");
	}

	public function deleteSupplierServicesWithoutServiceType($id)
	{
		$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id'");
	}

	public function addSupplierServices($str)
	{
		$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES " . $str);
	}
	public function addSupplierServices2($str)
	{
		$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceType) VALUES " . $str);
	}
						

	public function deletePlanPackagesPrices()
	{
		$this->db->query("DELETE FROM tbl_gf_plans_packages_prices WHERE ServiceType = '0'");
	}

	public function deleteUsersPackagesPrices()
	{
		$this->db_>query("DELETE FROM tbl_gf_users_packages_prices");
	}
	public function deleteUsersPackages()
	{
		$this->db_>query("DELETE FROM tbl_gf_user_packages WHERE ServiceType = '0'");
	}
	public function archivePackages()
	{
		$this->db->query("UPDATE tbl_gf_packages SET ArchivedPack = '1' WHERE sl3lbf = '0'");
	}
	public function addPackages($strServices)
	{
		$this->db->query("INSERT INTO tbl_gf_packages (CategoryId, PackageTitle, PackagePrice, TimeTaken, MustRead, DisablePackage, sl3lbf) VALUES " . $strServices);
	}

	public function addPackages2($strServices)
	{
		$this->db->query("INSERT INTO tbl_gf_packages (CategoryId, PackageTitle, PackagePrice, MustRead, DisablePackage, sl3lbf) VALUES " . $strServices);
	}
	public function deleteApiBrands($id)
	{
		$this->db->query("DELETE FROM tbl_gf_api_brands WHERE APIId = '$id'");
	}

	public function addApiBrands($strBrands)
	{
		$this->db->query("INSERT INTO tbl_gf_api_brands (APIId, BrandId, Brand) VALUES " . $strBrands);
	}

	public function deleteApiModels($id)
	{
		$this->db->query("DELETE FROM tbl_gf_api_models WHERE APIId = '$id'");		
	}
	
	public function addApiModels($strModels)
	{
		$this->db->query("INSERT INTO tbl_gf_api_models (APIId, BrandId, ModelId, Model) VALUES " . $strModels);
	}

	public function getRecords($idCol, $textCol, $disableCol, $tbl)
	{
		$rsLists = $this->db->query("SELECT $idCol, $textCol, $disableCol FROM $tbl ORDER BY $textCol");
		return $rsLists->result();
	}

	public function getRecordsagainstId($idCol, $textCol, $disableCol, $tbl, $id)
	{
		$rsLists = $this->db->query("SELECT $idCol, $textCol, $disableCol FROM $tbl WHERE $idCol = $id");
		return $rsLists->result();
	}
	public function updateMaintenanceTable($text, $disable)
	{
		$query = "UPDATE tbl_gf_maintenance SET MaintenanceText = '$text', DisableMaintenance = '$disable' WHERE Id = 1";
		$this->db->query($query);
	}

	public function getMaintenanceTable()
	{
		$rsMaintenance = $this->db->query("SELECT * FROM tbl_gf_maintenance WHERE Id = 1");
		return $rsMaintenance->result();
	}

	public function getCurrencies($query)
	{
		$rsResults = $this->db->query($query);
		return $rsResults->result();
	}

	public function getConversionRate($query)
	{
		$rsResults = $this->db->query($query);
		return $rsResults->row();
	}
	public function updateImeiAndFilePrices($query)
	{
		$this->db->query($query);
	}

	public function updateServerPrices($query)
	{
		$this->db->query($query);
	}

	public function updateUserImeiAndFilePrices($query)
	{
		$this->db->query($query);
	}

	public function updateUserServerPrices($query)
	{
		$this->db->query($query);
	}

	public function updatePlansForImeiAndFilePrices($query)
	{
		$this->db->query($query);
	}

	public function updatePlansForServerPrices($query)
	{
		$this->db->query($query);
	}
/*
	public function getCurrencies($query)
	{
		$rsCurrencies = $this->db->query($query);
		return $rsCurrencies->result();
	}
*/
	public function getCurrency($query)
	{
		$rsCurrencies = $this->db->query($query);
		return $rsCurrencies->row();
	}
	public function fusionclientapiInsertData($query)
	{
		$this->db->query($query);
		return $this->db->insert_id();
	}





	public function executeQuery($query)
	{
		$this->db->query($query);
	}

	public function getRow($query)
	{
		$rsCurrencies = $this->db->query($query);
		return $rsCurrencies->row();
	}

	public function selectData($query)
	{
		$rsCurrencies = $this->db->query($query);
		return $rsCurrencies->result();
	}

}
