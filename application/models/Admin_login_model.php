<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_login_model extends CI_Model {

    public function fetch_email_settings(){
        
        $this->db->select('Theme, LocalLanguage, AdminLoginCaptcha, AdminTitle, Copyrights, GoogleCaptchaSiteKey, GoogleCaptchaSecretKey');
        $this->db->from('tbl_gf_email_settings');
        $this->db->where('Id' , 1);

        $result=$this->db->get();
        return $result->row();
    }


    public function fetch_logopath(){
        
        $this->db->select('LogoPath');
        $this->db->from('tbl_gf_logo');
        $this->db->where('ForAdminPanel' , 1);
        $this->db->where('DisableLogo' , 0);
        $this->db->order_by('LogoId' , 'DESC');

       
        $result=$this->db->get();
        return $result->row();
    }

    public function fetch_logpath_by_id(){
     
        $this->db->select('LogoPath');
        $this->db->from('tbl_gf_logo');
        $this->db->where('DisableLogo' , 0);
        $this->db->order_by('LogoId' , 6);

        $result=$this->db->get();
        return $result->row();
    }

    public function fetch_admin_details_by_username($userName){
        
        $this->db->select("AdminId, CONCAT(FirstName,' ', LastName) AS FullName, AdminUserName, IsAdmin, RoleId, AdminUserPassword");
        $this->db->from('tbl_gf_admin');
        $this->db->where('AdminUserName' , $userName);
        $this->db->where('DisableAdmin' , 0);

        $result=$this->db->get();
        return $result->row();
    }

    public function add_admin_details($data){
       
        $this->db->insert('tbl_gf_admin_login_log' , $data);
        $id = $this->db->insert_id() ;  
        return $id ;     
    }
}