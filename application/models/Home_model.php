<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {

    public function fetch_package_category_data($strWhere){
        $query=$this->db->query("SELECT HTMLTitle, MetaKW, MetaTags FROM tbl_gf_package_category WHERE $strWhere");

        return $query->row();
       
    }

    public function fetch_pages_data(){
        $query=$this->db->query("SELECT HTMLTitle, MetaKW, MetaTags FROM tbl_gf_pages WHERE PageId = 2");
        return $query->row();
    }

    public function fetch_package_cat_by_slbf(){
     
        $this->db->select('CategoryId, Category');
        $this->db->from('tbl_gf_package_category');
        $this->db->where('DisableCategory' , 0);
        $this->db->where('SL3BF' , 0);
        $this->db->where('ArchivedCategory' , 0);
        $this->db->order_by('Category');

        $result=$this->db->get();
        return $result->result();
    }

    public function fetch_package_category_by_disablecategory(){
        
        $this->db->select('CategoryId, Category');
        $this->db->from('tbl_gf_package_category');
        $this->db->where('DisableCategory' , 0);
        $this->db->where('SL3BF' , 1);
        $this->db->where('ArchivedCategory' , 0);
        $this->db->order_by('Category');

        $result=$this->db->get();
        return $result->result();
      
    }

    public function fetch_package_category_packages($strWhere){
        $query=$this->db->query("SELECT A.CategoryId, Category, PackageId, PackageTitle, PackagePrice, A.SEOURLName, PackageImage,
				IF(TimeTaken IS NULL OR TimeTaken = '', '-', TimeTaken) AS TimeTaken FROM tbl_gf_package_category Cat, tbl_gf_packages A WHERE Cat.CategoryId = A.CategoryId AND 
				DisablePackage = 0 AND DisableCategory = 0 AND sl3bf = 0 AND ArchivedPack = '0' AND ArchivedCategory = 0 $strWhere 
                ORDER BY OrderBy, Category, PackOrderBy, PackageTitle");
        return $query->result();
    }

    public function fetch_price_plans($strGWC){
        $query=$this->db->query("SELECT PricePlanId, PricePlan FROM tbl_gf_price_plans WHERE DisablePricePlan = 0 $strGWC");
        return $query->result();
    }

    public function fetch_plans_packages_prices_currency($SERVICE_TYPE){
        $query=$this->db->query("SELECT A.PackageId, A.PlanId, A.Price FROM tbl_gf_plans_packages_prices A, tbl_gf_currency B WHERE A.CurrencyId = B.CurrencyId AND DefaultCurrency = 1 AND ServiceType = '$SERVICE_TYPE' ORDER BY PlanId");
        return $query->result();
    
    }

    public function fetch_packages_data($strAnd){
        $query=$this->db->query("SELECT PackageId, PackageTitle, MustRead, PackagePrice, A.HTMLTitle, A.MetaKW, A.MetaTags, TimeTaken, PackageImage 
                    FROM tbl_gf_packages A WHERE DisablePackage = 0 AND HideServiceAtWeb = 0 AND $strAnd");
        return $query->row();
    }

    public function fetch_currency_data(){
        $query=$this->db->query('SELECT CurrencySymbol FROM tbl_gf_currency WHERE DefaultCurrency = 1');
        return $query->row();
    }

    public function package_category_and_packages($strWhere){
        $query=$this->db->query("SELECT A.CategoryId, Category, PackageId, PackageTitle, PackagePrice, A.SEOURLName, PackageImage, 
        IF(TimeTaken IS NULL OR TimeTaken = '', '-', TimeTaken) AS TimeTaken FROM tbl_gf_package_category Cat, tbl_gf_packages A WHERE Cat.CategoryId = A.CategoryId 
        AND ArchivedPack = '0' AND DisablePackage = 0 AND DisableCategory = 0 AND sl3bf = 1 AND ArchivedCategory = 0 $strWhere 
        ORDER BY OrderBy, Category, PackOrderBy, PackageTitle");

        return $query->result();
    }

    public function fetch_log_package_cat_data($strWhere){
        $query=$this->db->query("SELECT HTMLTitle, MetaKW, MetaTags FROM tbl_gf_log_package_category WHERE $strWhere");
        return $query->row();
    }

    public function fetch_log_pack_cat_by_disable_cat(){
        $query=$this->db->query("SELECT CategoryId, Category FROM tbl_gf_log_package_category Cat WHERE DisableCategory = 0 ORDER BY Category");
        return $query->result();
    }

    public function fetch_log_pack_cat_and_packages($strWhere){
        $query=$this->db->query("SELECT A.CategoryId, Category, LogPackageId AS PackageId, LogPackageTitle AS PackageTitle, LogPackagePrice AS PackagePrice, A.SEOURLName, 
        IF(DeliveryTime IS NULL OR DeliveryTime = '', '-', DeliveryTime) AS TimeTaken FROM tbl_gf_log_package_category Cat, tbl_gf_log_packages A WHERE 
        Cat.CategoryId = A.CategoryId AND DisableCategory = 0 AND DisableLogPackage = 0 AND ArchivedPack = '0' AND ArchivedCategory = 0 $strWhere ORDER BY OrderBy, Category, PackOrderBy, LogPackageTitle");
        return $query->result();
    }

    public function fetch_log_packages_by_disablepack($strAnd){
        $query=$this->db->query("SELECT LogPackageTitle, LogPackageDetail, LogPackagePrice, A.HTMLTitle, A.MetaKW, A.MetaTags, DeliveryTime 
						FROM tbl_gf_log_packages A WHERE DisableLogPackage = 0 AND HideServiceAtWeb = 0 AND $strAnd");
        return $query->row();
    
    }

    public function fetch_page_data_by_id($strAnd){
        $query=$this->db->query("SELECT PageTitle, PageText, HTMLTitle, MetaKW, MetaTags, Image, ActivateRetail FROM tbl_gf_pages WHERE DisablePage = 0 AND $strAnd");
        return $query->row();
    }

    public function fetch_banner_data(){
        $query=$this->db->query("SELECT BannerId, BannerTitle, Detail, BannerPath FROM tbl_gf_banner WHERE DisableBanner = 0 AND BannerType = 0 ORDER BY RAND()");
        return $query->result();
    }

    public function fetch_faq_data(){
        $query=$this->db->query('SELECT Question, Answer FROM tbl_gf_faqs WHERE DisableFAQ = 0');
        return $query->result();
    }

   

}