<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_model extends CI_Model {

	public function getTodayReport($today){
		$query = $this->db->query("SELECT SUM( IF( CodeStatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( CodeStatusId = '2', 1, 0 ) ) AS `CA`,  SUM( IF( CodeStatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( CodeStatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_codes WHERE DATE(RequestedAt) = '$today'");

		return $query->row();
	}
	public function getTodayFilesReport($today){
		$query = $this->db->query("SELECT SUM( IF( CodeStatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( CodeStatusId = '2', 1, 0 ) ) AS `CA`,  SUM( IF( CodeStatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( CodeStatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_codes_slbf WHERE DATE(RequestedAt) = '$today'");

		return $query->row();
	}
	public function getTodayserverReport($today){
		$query = $this->db->query("SELECT SUM( IF( StatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( StatusId = '2', 1, 0 ) ) AS `CA`,  
					SUM( IF( StatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( StatusId = '4', 1, 0 ) ) AS `IP` 
						FROM tbl_gf_log_requests WHERE DATE(RequestedAt) = '$today'");

		return $query->row();
	}
	public function getTodayIMEIOrdersReport($today){
		$query = $this->db->query("SELECT PackageTitle, SUM( IF( CodeStatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( CodeStatusId = '2', 1, 0 ) ) AS `CA`,  
				SUM( IF( CodeStatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( CodeStatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_codes A, tbl_gf_packages B 
				WHERE A.PackageId = B.PackageId AND DATE(RequestedAt) = '$today' GROUP BY A.PackageId ORDER BY PackOrderBy, PackageTitle");

		return $query->row();
	}
	public function getTodayFileOrdersReport($today){
		$query = $this->db->query("SELECT PackageTitle, SUM( IF( CodeStatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( CodeStatusId = '2', 1, 0 ) ) AS `CA`,  
				SUM( IF( CodeStatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( CodeStatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_codes_slbf A, tbl_gf_packages B 
				WHERE A.PackageId = B.PackageId AND DATE(RequestedAt) = '$today' GROUP BY A.PackageId ORDER BY PackOrderBy, PackageTitle");

		return $query->row();
	}
	public function getTodayServerOrdersReport($today){
		$query = $this->db->query("SELECT LogPackageTitle, SUM( IF( StatusId = '1', 1, 0 ) ) AS `P`, 
				SUM( IF( StatusId = '2', 1, 0 ) ) AS `CA`,  SUM( IF( StatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( StatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_log_requests A, tbl_gf_log_packages B WHERE A.LogPackageId = B.LogPackageId AND DATE(RequestedAt) = '$today' GROUP BY A.LogPackageId ORDER BY PackOrderBy, LogPackageTitle");

		return $query->row();
	}
	public function getTodayIMEIAllOrdersReport($today){
		$query = $this->db->query("SELECT SUM( IF( CodeStatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( CodeStatusId = '2', 1, 0 ) ) AS `CA`,  SUM( IF( CodeStatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( CodeStatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_codes");

		return $query->row();
	}
	public function getTodayFileAllOrdersReport($today){
		$query = $this->db->query("SELECT SUM( IF( CodeStatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( CodeStatusId = '2', 1, 0 ) ) AS `CA`,  SUM( IF( CodeStatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( CodeStatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_codes_slbf");

		return $query->row();
	}
	public function getTodayServerAllOrdersReport($today){
		$query = $this->db->query("SELECT SUM( IF( StatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( StatusId = '2', 1, 0 ) ) AS `CA`,  SUM( IF( StatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( StatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_log_requests");

		return $query->row();
	}
	public function getIMEIOrdersAllReport($today){
		$query = $this->db->query("SELECT PackageTitle, SUM( IF( CodeStatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( CodeStatusId = '2', 1, 0 ) ) AS `CA`,  SUM( IF( CodeStatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( CodeStatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_codes A, tbl_gf_packages B WHERE A.PackageId = B.PackageId GROUP BY A.PackageId ORDER BY PackOrderBy, PackageTitle");

		return $query->result();
	}
	public function getFileOrdersAllReport($today){
		$query = $this->db->query("SELECT PackageTitle, SUM( IF( CodeStatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( CodeStatusId = '2', 1, 0 ) ) AS `CA`,  SUM( IF( CodeStatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( CodeStatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_codes_slbf A, tbl_gf_packages B WHERE A.PackageId = B.PackageId GROUP BY A.PackageId ORDER BY PackOrderBy, PackageTitle");

		return $query->result();
	}
	public function getServerOrdersAllReport($today){
		$query = $this->db->query("SELECT LogPackageTitle, SUM( IF( StatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( StatusId = '2', 1, 0 ) ) AS `CA`,  SUM( IF( StatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( StatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_log_requests A, tbl_gf_log_packages B WHERE A.LogPackageId = B.LogPackageId GROUP BY A.LogPackageId ORDER BY PackOrderBy, LogPackageTitle");

		return $query->result();
	}
	public function getIMEISrchd($dtTo,$dtFrom){
		$strWhere='';
		if($dtFrom != '' && $dtTo != '')
			$strWhere .= " And DATE(RequestedAt) >= '$dtFrom' AND DATE(RequestedAt) <= '$dtTo'";
		else
		{
			if($dtFrom != '')
				$strWhere .= " AND DATE(RequestedAt) >= '$dtFrom'";
			if($dtTo!= '')
				$strWhere .= " AND DATE(RequestedAt) <= '$dtTo'";
		}
		$query = $this->db->query("SELECT SUM( IF( CodeStatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( CodeStatusId = '2', 1, 0 ) ) AS `CA`,  SUM( IF( CodeStatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( CodeStatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_codes WHERE (1) $strWhere");

		return $query->row();
	}
	public function getFileSrchd($dtTo,$dtFrom){
		$strWhere='';
		if($dtFrom != '' && $dtTo != '')
			$strWhere .= " And DATE(RequestedAt) >= '$dtFrom' AND DATE(RequestedAt) <= '$dtTo'";
		else
		{
			if($dtFrom != '')
				$strWhere .= " AND DATE(RequestedAt) >= '$dtFrom'";
			if($dtTo!= '')
				$strWhere .= " AND DATE(RequestedAt) <= '$dtTo'";
		}
		$query = $this->db->query("SELECT SUM( IF( CodeStatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( CodeStatusId = '2', 1, 0 ) ) AS `CA`,  SUM( IF( CodeStatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( CodeStatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_codes_slbf WHERE (1) $strWhere");

		return $query->row();
	}
	public function getServerSrchd($dtTo,$dtFrom){
		$strWhere='';
		if($dtFrom != '' && $dtTo != '')
			$strWhere .= " And DATE(RequestedAt) >= '$dtFrom' AND DATE(RequestedAt) <= '$dtTo'";
		else
		{
			if($dtFrom != '')
				$strWhere .= " AND DATE(RequestedAt) >= '$dtFrom'";
			if($dtTo!= '')
				$strWhere .= " AND DATE(RequestedAt) <= '$dtTo'";
		}
		$query = $this->db->query("SELECT SUM( IF( StatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( StatusId = '2', 1, 0 ) ) AS `CA`,  
					SUM( IF( StatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( StatusId = '4', 1, 0 ) ) AS `IP` 
					FROM tbl_gf_log_requests WHERE (1) $strWhere");

		return $query->row();
	}
	public function getIMEIOrdersSrchd($dtTo,$dtFrom){
		$strWhere='';
		if($dtFrom != '' && $dtTo != '')
			$strWhere .= " And DATE(RequestedAt) >= '$dtFrom' AND DATE(RequestedAt) <= '$dtTo'";
		else
		{
			if($dtFrom != '')
				$strWhere .= " AND DATE(RequestedAt) >= '$dtFrom'";
			if($dtTo!= '')
				$strWhere .= " AND DATE(RequestedAt) <= '$dtTo'";
		}
		$query = $this->db->query("SELECT PackageTitle, SUM( IF( CodeStatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( CodeStatusId = '2', 1, 0 ) ) AS `CA`,  SUM( IF( CodeStatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( CodeStatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_codes A, tbl_gf_packages B WHERE A.PackageId = B.PackageId $strWhere GROUP BY A.PackageId ORDER BY PackOrderBy, PackageTitle");

		return $query->result();
	}
	public function getFileOrdersSrchd($dtTo,$dtFrom){
		$strWhere='';
		if($dtFrom != '' && $dtTo != '')
			$strWhere .= " And DATE(RequestedAt) >= '$dtFrom' AND DATE(RequestedAt) <= '$dtTo'";
		else
		{
			if($dtFrom != '')
				$strWhere .= " AND DATE(RequestedAt) >= '$dtFrom'";
			if($dtTo!= '')
				$strWhere .= " AND DATE(RequestedAt) <= '$dtTo'";
		}
		$query = $this->db->query("SELECT PackageTitle, SUM( IF( CodeStatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( CodeStatusId = '2', 1, 0 ) ) AS `CA`,  SUM( IF( CodeStatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( CodeStatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_codes_slbf A, tbl_gf_packages B WHERE A.PackageId = B.PackageId $strWhere GROUP BY A.PackageId ORDER BY PackOrderBy, PackageTitle");

		return $query->result();
	}
	public function getServerOrdersSrchd($dtTo,$dtFrom){
		$strWhere='';
		if($dtFrom != '' && $dtTo != '')
			$strWhere .= " And DATE(RequestedAt) >= '$dtFrom' AND DATE(RequestedAt) <= '$dtTo'";
		else
		{
			if($dtFrom != '')
				$strWhere .= " AND DATE(RequestedAt) >= '$dtFrom'";
			if($dtTo!= '')
				$strWhere .= " AND DATE(RequestedAt) <= '$dtTo'";
		}
		
		$query = $this->db->query("SELECT LogPackageTitle, SUM( IF( StatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( StatusId = '2', 1, 0 ) ) AS `CA`,  SUM( IF( StatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( StatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_log_requests A, tbl_gf_log_packages B WHERE A.LogPackageId = B.LogPackageId $strWhere GROUP BY A.LogPackageId ORDER BY PackOrderBy, LogPackageTitle");

		return $query->result();
	}
	public function getData($string){
		$query = $this->db->query($string);
		return $query->result();
	}
	public function getRowData($string){
		$query = $this->db->query($string);
		//var_dump($query->row()->TotalRecs);die;
		return $query->row()->TotalRecs;
	}
	public function getSingleRowData($string){
		$query = $this->db->query($string);
		if($query->num_rows()>0){
			return $query->row();
		}else{
			return false;
		}
	}
	public function getNumData($string){
		$query = $this->db->query($string);
		return $query->num_rows();
	}
	public function delteData($string){
		$query = $this->db->query($string);
		return true;
	}
	public function insertData($string){
		$query = $this->db->query($string);
		return true;
	}
	public function updateData($string){
		$query = $this->db->query($string);
		return true;
	}
}

