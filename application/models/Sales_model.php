<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_model extends CI_Model {

    public function update_countries($update_data , $where){
        if($where){
            $this->db->where($where);
        }
        $this->db->update('tbl_gf_countries' , $update_data);
    }

    public function fetch_country_data_by_id($id){
       
        $this->db->select('BlockedPMethodIds');
        $this->db->from('tbl_gf_countries');
        $this->db->where('CountryId' , $id);

        $result = $this->db->get();
        return $result->row();

    }

    public function fetch_countries_data(){
       
        $this->db->select('CountryId, BlockedPMethodIds, Country');
        $this->db->from('tbl_gf_countries');
        $this->db->where('DisableCountry' , 0);
        $this->db->where('BlockedPMethodIds !=' , '');
        $this->db->order_by('Country');

        $result=$this->db->get();
        return $result->result();
    }

    public function fetch_payment_methods(){
       
        $this->db->select('PaymentMethodId, PaymentMethod');
        $this->db->from('tbl_gf_payment_methods');
        $this->db->order_by('PaymentMethod');

        $result=$this->db->get();
        return $result->result();
    }

    public function del_payments_by_ids($ids){
        $this->db->where_in('PaymentId' , $ids);
        $this->db->delete('tbl_gf_payments');
    }

    public function update_payments($update_data , $where = array() ,  $where_in = array()){
       
        if($where){
            $this->db->where($where);
        }
        if($where_in){
            $this->db->where_in($where_in);
        }

        $this->db->update('tbl_gf_payments' , $update_data);
    }

    public function fetch_users_payment_methods($strWherePmnts , $strWhere, $eu, $limit){
        $query=$this->db->query("SELECT PaymentId, A.UserId, Amount, B.UserName, A.Credits, CONCAT(B.FirstName, ' ', B.LastName) AS Name, D.PaymentStatus, C.PaymentMethod, D.PaymentStatusId, 
                    DATE(PaymentDtTm) AS PaymentDtTm, Currency, IF(TransactionId IS NULL OR TransactionId = '', '-', TransactionId) AS TransactionId, CreditsTransferred, A.Vat, 
                    ShippingAddress, IF(PayerEmail IS NULL OR PayerEmail = '', '-', PayerEmail) AS PayerEmail FROM tbl_gf_users B, tbl_gf_payment_status D, tbl_gf_payments A  
                    LEFT JOIN tbl_gf_payment_methods C ON (A.PaymentMethod = C.PaymentMethodId) WHERE A.UserId = B.UserId $strWherePmnts
                    AND A.PaymentStatus = D.PaymentStatusId $strWhere ORDER BY PaymentId DESC LIMIT $eu, $limit");
        return $query->result();
    }

    public function del_pp_receivers($id){
        $this->db->where('ReceiverId' , $id);
        $this->db->delete('tbl_gf_pp_receivers');
    }

    public function fetch_payment_method_types(){

        $this->db->select('PaymentMethodId, PaymentMethod, DisablePaymentMethod, Fee, RetailFee, PayMethodType');
        $this->db->from('tbl_gf_payment_methods A, tbl_gf_pay_method_types B');
        $this->db->where(' A.PayMethodTypeId = B.PayMethodTypeId ');
        $this->db->order_by('OrderBy');

        $result=$this->db->get();
        return $result->result();
    }


    public function fetch_pp_receivers(){
       
        $this->db->select('ReceiverId, Username, DisablePP');
        $this->db->from('tbl_gf_pp_receivers');
        $this->db->order_by('Username');

        $result = $this->db->get();
        return $result->result();

    }

    public function fetch_payment_method_by_id($id ,  $pMethod , $disable , $typeId){
    
        $this->db->select(' PaymentMethodId ');
        $this->db->from('tbl_gf_payment_methods');
        $this->db->where('PaymentMethod' , $pMethod);
        $this->db->where('DisablePaymentMethod' , $disable);
        $this->db->where('PayMethodTypeId' , $typeId);

        if($id > 0){
            $this->db->where('PaymentMethodId' , $id);
        }

    
        $result = $this->db->get();
        return $result->row();
    }

    public function insert_payment_methods($insert_data){
        $this->db->insert('tbl_gf_payment_methods' , $insert_data);
        return $this->db->insert_id();
    }

    public function update_payment_methods($update_data ,  $where = array() , $where_in = array()){
        if($where){
            $this->db->where($where);
        }

        if($where_in){
            $this->db->where_in($where_in);
        }

        $this->db->update('tbl_gf_payment_methods',$update_data);
    }

    public function fetch_all_payment_data_by_id($id){
       
        $this->db->select('*');
        $this->db->from('tbl_gf_payment_methods');
        $this->db->where('PaymentMethodId' , $id);

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_pp_receivers_by_username($userName , $disable ,$id){
        
        $this->db->select('ReceiverId');
        $this->db->from('tbl_gf_pp_receivers');
        $this->db->where('Username' , $userName);
        $this->db->where('DisablePP' , $disable);

        if($id > 0){
            $this->db->where('ReceiverId' , $id);
        }

        $result = $this->db->get();
        return $result->row();
    }

    public function insert_pp_receivers($insert_data){
        $this->db->insert('tbl_gf_pp_receivers', $insert_data);
    }

    public function update_pp_receivers($update_data ,  $where = array()){
        if($where){
            $this->db->where($where);
        }

        $this->db->update('tbl_gf_pp_receivers' , $update_data);
    }

    public function fetch_all_receivers_data_by_id($id){
       

        $this->db->select('*');
        $this->db->from('tbl_gf_pp_receivers');
        $this->db->where('ReceiverId' , $id);

        $result = $this->db->get();
        return $result->row();
    }

}