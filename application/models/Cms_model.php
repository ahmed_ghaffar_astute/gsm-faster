<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cms_model extends CI_Model {
	public function getAllLogos(){
		$query = $this->db->query("SELECT Id, BrandImage FROM tbl_gf_brand_logos ORDER BY Id");
		return $query->result();
	}
	public function getAllPages($type){
		$query = $this->db->query("SELECT PageId, LinkTitle, PageTitle, HeaderLink, FooterLink, DisablePage FROM tbl_gf_pages WHERE PageType = '$type' ORDER BY LinkTitle");
		return $query->result();
	}
	public function getPageImgByID($id){
		$query = $this->db->query("SELECT * FROM tbl_gf_pages WHERE PageId = '$id'");
		return $query->row();
	}
	public function insertPage($lnkTitle,$pageTitle, $fileName, $metaTags, $pageText, $headerLnk, $footerLnk, $headerLnkCP, $footerLnkCP,$disablePage,$seoName,$metaKW,$htmlTitle,$type, $actRtl, $url,$subMenuLnk){
		$query = $this->db->query("INSERT INTO tbl_gf_pages (LinkTitle, PageTitle, FileName, MetaTags, PageText, HeaderLink, FooterLink, HeaderLinkCP, FooterLinkCP, DisablePage, SEOURLName, MetaKW, HTMLTitle, PageType, ActivateRetail, URL, SubMenuLink) VALUES ('$lnkTitle', '$pageTitle', '$fileName', '$metaTags', '$pageText', '$headerLnk', '$footerLnk', '$headerLnkCP', '$footerLnkCP', '$disablePage', '$seoName', '$metaKW', '$htmlTitle', '$type', '$actRtl', '$url', '$subMenuLnk')");
		return $this->db->insert_id();
	}
	public function updatePage($lnkTitle,$pageTitle, $fileName, $metaTags, $pageText, $headerLnk, $footerLnk, $headerLnkCP, $footerLnkCP,$disablePage,$seoName,$metaKW,$htmlTitle, $actRtl, $url,$subMenuLnk,$id){

		$query = $this->db->query("UPDATE tbl_gf_pages SET LinkTitle = '$lnkTitle', FileName = '$fileName', PageTitle = '$pageTitle', MetaTags = '$metaTags', URL = '$url', PageText = '$pageText', HeaderLink = '$headerLnk', FooterLink = '$footerLnk', HeaderLinkCP = '$headerLnkCP', FooterLinkCP = '$footerLnkCP', DisablePage = '$disablePage', MetaKW = '$metaKW', SEOURLName = '$seoName', HTMLTitle = '$htmlTitle', ActivateRetail = '$actRtl', SubMenuLink = '$subMenuLnk' WHERE PageId = '$id'");
		return true;
	}
	public function updatePageImgByID($file,$pageId){

		$query = $this->db->query("UPDATE tbl_gf_pages SET Image = '$file' WHERE PageId = '$pageId'");
		return true;
	}
	public function insertBrandLogo($files){
		$this->db->set("BrandImage",$files);
		$this->db->insert("tbl_gf_brand_logos");
		return $this->db->insert_id();
	}
	public function updateBrandLogo($files,$id){
		$this->db->set("BrandImage",$files);
		$this->db->where("Id",$id);
		$this->db->update("tbl_gf_brand_logos");
		return true;
	}
	public function getBrandLogoById($id){
		$this->db->select("*");
		$this->db->where("Id",$id);
		$query = $this->db->get("tbl_gf_brand_logos");
		return $query->row();
	}
	public function getSocailLinksBytype($type){
		$this->db->select("Id, Title, Link, DisableRecord");
		$this->db->where("MediaType",$type);
		$this->db->order_by("Title");
		$query = $this->db->get("tbl_gf_socialmedia");
		return $query->result();
	}
	public function deleteLogos($id){
		$this->db->where("Id",$id);
		$this->db->delete("tbl_gf_brand_logos");
		return true;
	}
	public function deleteSocailLinks($id){
		$this->db->where("Id",$id);
		$this->db->delete("tbl_gf_socialmedia");
		return true;
	}
	public function getPageByData($lnkTitle,$disablePage,$type,$id){
		$this->db->select("PageId");
		$this->db->where("LinkTitle",$lnkTitle);
		$this->db->where("DisablePage",$disablePage);
		$this->db->where("PageType",$type);
		if($id > 0)
			$this->db->where("PageId",'<>',$id);
		$query = $this->db->get("tbl_gf_pages");
		if($query->num_rows() > 0){
			return $query->row()->PageId;
		}else{
			return false;
		}
	}
	public function getData($data){
		$query = $this->db->query($data);
		return $query->result();
	}
	public function gatRowData($data){
		$query = $this->db->query($data);
		return $query->row();
	}
	public function deleteReview($data){
		$this->db->query($data);
		return true;
	}
	public function insertDataGetID($data){
		$this->db->query($data);
		return $this->db->insert_id();
	}
	public function insertData($data){
		$this->db->query($data);
		return true;
	}
	public function UpdateData($data){
		$this->db->query($data);
		return true;
	}

	public function getSocialMediaById($id)
	{
		$this->db->select("*");
		$this->db->where("Id",$id);
		$query = $this->db->get("tbl_gf_socialmedia");
		return $query->row();
	}
	public function getSocialMediaByTitleAndType($title, $type, $id)
	{
		$this->db->select("Id")
		->where("Title", $title)
		->where("MediaType", $type);
		if($id != 0 ) {
			$this->db->where(" Id <>", $id);
		}
		return $this->db->get("tbl_gf_socialmedia")->row();

	}



}
