<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients_model extends CI_Model {

    public function fetch_users_count_by_id($uName , $id){
      
        $this->db->select("Count(UserId) AS TotalRecs");
        $this->db->from('tbl_gf_users');
        $this->db->where('UserName' , $uName);
        if($id > 0){
            $this->db->where('UserId <>' , $id);
        }
        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_users_by_email($email , $id){
       
        $this->db->select("UserId");
        $this->db->from('tbl_gf_users');
        $this->db->where('UserEmail' , $email);
        if($id > 0){
            $this->db->where('UserId <>' , $id);
        }
        $result = $this->db->get();
        return $result->row();
    }

    public function insert_users_data($insert_array){
        $this->db->insert('tbl_gf_users' , $insert_array);
        return $this->db->insert_id();
    }

    public function update_users_custom_query($allowCrdsTrnsfr , $cols , $id){
        $this->db->query("UPDATE tbl_gf_users SET TransferCredits = '$allowCrdsTrnsfr' $cols WHERE UserId = '$id'");
    } 

    public function update_users($update_data , $id , $where_in=array()){
        if($id){
            $this->db->where('UserId' , $id);
        }

        if($where_in){
            $this->db->where_in($where_in);
        }
        $this->db->update('tbl_gf_users',$update_data);
    }

    public function del_user_login_countries($id){
       
        $this->db->where('UserId' , $id);
        $this->db->delete('tbl_gf_user_login_countries');
    }


    public function insert_login_countries($strData){
        $this->db->query("INSERT INTO tbl_gf_user_login_countries (UserId, ISO) VALUES $strData");
    }

    public function fetch_user_by_id($id){
       
        $this->db->select('*');
        $this->db->from('tbl_gf_users');
        $this->db->where('UserId' , $id);

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_registration_fields(){
        
        $this->db->select('FieldId, FieldLabel, FieldType, FieldColName, Mandatory');
        $this->db->from('tbl_gf_registration_fields');
        $this->db->where('DisableField' , 0);

        $result = $this->db->get();
        return $result->result();
    }

    public function fetch_users_price_plans_currency($strWhere , $eu, $limit){
        $query=$this->db->query("SELECT UserId, UserName, UserEmail, Credits, CONCAT(A.FirstName, ' ', A.LastName) AS Name, Phone, DisableUser, APIKey, CurrencyAbb, 
        PricePlan FROM tbl_gf_users A LEFT JOIN tbl_gf_price_plans C ON (A.PricePlanId = C.PricePlanId) LEFT JOIN tbl_gf_currency B ON 
        (A.CurrencyId = B.CurrencyId) WHERE UserArchived = 0 $strWhere ORDER BY UserId DESC LIMIT $eu, $limit");

        return $query->result();
    }

    public function fetch_price_plans(){
     
        $this->db->select('PricePlanId, PricePlan');
        $this->db->from('tbl_gf_price_plans');
        $this->db->where('DisablePricePlan' , 0);
        $this->db->order_by('PricePlan');

        $result = $this->db->get();
        return $result->result();

    }

    public function fetch_users_count_by_archived($strWhere){
        $query=$this->db->query("SELECT COUNT(UserId) AS TotalRecs FROM tbl_gf_users A WHERE UserArchived = 0 $strWhere");
        return $query->row();
    }

    public function fetch_users_countries_currency($strAnd){
        $query=$this->db->query("SELECT UserId, UserEmail, UserName, Credits, Phone, Country, CurrencyAbb FROM tbl_gf_users A, tbl_gf_countries B, tbl_gf_currency C 
                    WHERE A.CountryId = B.CountryId AND A.CurrencyId = C.CurrencyId AND DisableUser = 0 AND UserArchived = 0 $strAnd ORDER BY UserEmail");
        return $query->result();
    }

    public function fetch_users_by_userid($userId){
      
        $this->db->select("UserId, CONCAT(FirstName,' ', LastName) AS FullName, UserName, UserEmail, UserType");
        $this->db->from('tbl_gf_users');
        $this->db->where('UserId' , $userId);

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_maintenance_count(){
       
        $this->db->select("COUNT(Id) AS MaintenanceMode");
        $this->db->from('tbl_gf_maintenance');
        $this->db->where('Id' , 1);
        $this->db->where('DisableMaintenance', 0);

        $result = $this->db->get();
        return $result->row();

    }

    public function fetch_email_settings(){
        $this->db->select('isv, Theme, ToEmail, TextDirection, AllowNewUserToLogIn, Phone, AutoCredits, ShowPricesAtWeb');
        $this->db->from('tbl_gf_email_settings');
        $this->db->where('Id' , 1);

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_logo_details(){
        $this->db->select('LogoId, LogoPath');
        $this->db->from('tbl_gf_logo');
        $this->db->where('DisableLogo' , 0);
        $this->db->where_in('LogoId' , array(2 ,6));

        $result = $this->db->get();
        return $result->result();
    }

    public function fetch_credit_history($userId ,  $strWhere , $eu, $limit){
        $query=$this->db->query("SELECT IMEINo, HistoryDtTm, Description, Credits, CreditsLeft, Comments, IP, FeePercentage, LogRequestId, OType FROM tbl_gf_credit_history 
                                    WHERE UserId = '$userId' $strWhere ORDER BY HistoryId DESC limit $eu, $limit");
        return $query->result();
    }

    public function insert_payments($strCol, $strCol1 ,$encNewCredits, $userId,$pMethodId, $pStatusId, $currDtTm, $transId, $comments, $curr, $strVal  ,$strVal1){
        $this->db->query("INSERT INTO tbl_gf_payments (Credits, Amount, UserId, PaymentMethod, PaymentStatus, PaymentDtTm, TransactionId, 
        CreditsTransferred, Comments, Payable, Currency, InvoiceAddedBy, ByAdmin $strCol $strCol1) 
        VALUES ('$encNewCredits', '$encNewCredits', '$userId', '$pMethodId', '$pStatusId', '$currDtTm', '$transId', 1, '$comments', 0, '$curr', 
        '".$_SESSION['GSM_FSN_AdmId']."', 1 $strVal $strVal1)");

        return $this->db->insert_id();
    }

    public function insert_credit_history($insert_array){
       
        $this->db->insert('tbl_gf_credit_history' ,$insert_array);
    }

    public function fetch_payment_methods_by_id($pMethodId){
       
        $this->db->select('PaymentMethod');
        $this->db->from('tbl_gf_payment_methods');
        $this->db->where('DisablePaymentMethod' , 0);
        $this->db->where('PaymentMethodId' , $pMethodId);

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_users_currency_data($userId){
      
        $this->db->select("UserId, UserEmail, CONCAT(FirstName, ' ', LastName) AS Name, Credits, Currency, AllowNegativeCredits, OverdraftLimit");
        $this->db->from('tbl_gf_users A');
        $this->db->join('tbl_gf_currency B' , 'A.CurrencyId = B.CurrencyId' , 'left');
        $this->db->where('UserId' , $userId);

        $result = $this->db->get();
        return $result->row();
    }

    public function delete_user_api_ips($userId){
        $this->db->where('UserId' , $userId);
        $this->db->delete('tbl_gf_user_api_ips');
    }

    public function insert_api_ips($strIPs){
        $this->db->query("INSERT INTO tbl_gf_user_api_ips (UserId, IP) VALUES ".$strIPs);
        
    }

    public function fetch_users_concatenated_data($userId){

        $this->db->select("CONCAT(FirstName, ' ', LastName) AS CustomerName ,UserEmail, APIKey");
        $this->db->from('tbl_gf_users');
        $this->db->where('UserId', $userId);
        
        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_user_api_ips($userId){
        $this->db->select('*');
        $this->db->from('tbl_gf_user_api_ips');
        $this->db->where('UserId' , $userId);
        $this->db->order_by('UserAPIId');
       
        $result = $this->db->get();
        return $result->result();
    }

    public function del_payments($ids){
        $this->db->where_in('PaymentId' , $ids);
        $this->db->delete('tbl_gf_payments');
    }

    public function update_payments($update_data , $where = array() , $where_in = array()){
     
        if($where_in){
            $this->db->where_in($where_in);
        }

        if($where){
            $this->db->where($where);
        }

        $this->db->update('tbl_gf_payments' , $update_data);
    }

    public function fetch_users_status_and_payments($strWherePmnts , $strWhere , $eu, $limit){
        $query=$this->db->query("SELECT PaymentId, A.UserId, Amount, B.UserName, A.Credits, CONCAT(B.FirstName, ' ', B.LastName) AS Name, D.PaymentStatus, C.PaymentMethod, D.PaymentStatusId, 
        DATE(PaymentDtTm) AS PaymentDtTm, Currency, IF(TransactionId IS NULL OR TransactionId = '', '-', TransactionId) AS TransactionId, CreditsTransferred, A.Vat, 
        ShippingAddress, IF(PayerEmail IS NULL OR PayerEmail = '', '-', PayerEmail) AS PayerEmail FROM tbl_gf_users B, tbl_gf_payment_status D, tbl_gf_payments A  
        LEFT JOIN tbl_gf_payment_methods C ON (A.PaymentMethod = C.PaymentMethodId) WHERE A.UserId = B.UserId $strWherePmnts
        AND A.PaymentStatus = D.PaymentStatusId $strWhere ORDER BY PaymentId DESC LIMIT $eu, $limit");

        return $query->result();
    }

    public function insert_payment_details($insert_data){
        
        $this->db->insert('tbl_gf_payment_details',$insert_data);

    }

    public function fetch_payments_users_data($id){
           
        $this->db->select('A.UserId, A.Credits, PaymentMethod, PayableAmount, PaymentStatus, TransactionId, A.Comments, CreditsTransferred, B.UserName, ByAdmin, PaidDtTm, Currency, PayerEmail');
        $this->db->from('tbl_gf_payments A, tbl_gf_users B');
        $this->db->where('A.UserId = B.UserId');
        $this->db->where('PaymentId' , $id);

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_users_countries($strWhere , $eu, $limit){
        
        $result=$this->db->query("SELECT UserId, UserName, UserEmail, Credits, CONCAT(FirstName, ' ', LastName) AS Name, IP, AutoFillCredits, CanAddCredits, Country, AddedAt 
        FROM tbl_gf_users A, tbl_gf_countries B WHERE A.CountryId = B.CountryId AND DisableUser = 0 AND UserArchived = 0 $strWhere ORDER BY UserId DESC LIMIT $eu, $limit");
  
        return $result->result();

    }

    public function fetch_price_plans_by_id($id){
      
        $this->db->select('*');
        $this->db->from('tbl_gf_price_plans');
        $this->db->where('PricePlanId' , $id);

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_price_plans_by_id_and_group($group , $id){
    
        $this->db->select('PricePlanId');
        $this->db->from('tbl_gf_price_plans');
        $this->db->where('PricePlan' , $group);

        if($id > 0){
            $this->db->where('PricePlanId !=' , $id);
        }
       
        $result = $this->db->get();
        return $result->row();

    }

    public function insert_price_plans($insert_data){
        $this->db->insert('tbl_gf_price_plans' , $insert_data);
    }

    public function update_price_plans($update_data , $where = array() , $where_in = array()){
        if($where){
            $this->db->where($where);
        }
        if($where_in){
            $this->db->where_in($where_in);
        }

        $this->db->update('tbl_gf_price_plans',$update_data);
    }


    public function deletePricePlan($planId)
    {
        $this->db->query("UPDATE tbl_gf_users SET PricePlanId = 0 WHERE PricePlanId = '$planId'");
        $this->db->query("DELETE FROM tbl_gf_plans_packages_prices WHERE PlanId = '$planId'");
        $this->db->query("DELETE FROM tbl_gf_price_plans WHERE PricePlanId = '$planId'");
    }

    public function fetch_all_price_plans(){
        
        $this->db->select('*');
        $this->db->from('tbl_gf_price_plans');
        $this->db->order_by('PricePlan');

        $result = $this->db->get();
        return $result->result();
    }

    public function fetch_currency_data(){
     
        $this->db->select('CurrencyId, CurrencyAbb, ConversionRate, DefaultCurrency ');
        $this->db->from('tbl_gf_currency');
        $this->db->where('DisableCurrency' , 0);
        $this->db->order_by('CurrencyAbb');

        $result = $this->db->get();
        return $result->result();
    }

    public function fetch_rspacks($colId , $colTitle , $tblPckName, $strPacks){
        $query=$this->db->query("SELECT $colId AS PackageId, $colTitle AS PackageTitle FROM $tblPckName WHERE $colId IN ($strPacks)");
        return $query->result();
    }

    public function del_plans_packages_prices($planId , $sc , $strPacks){
        $this->db->query("DELETE FROM tbl_gf_plans_packages_prices WHERE PlanId = '$planId' AND ServiceType = '$sc' AND PackageId IN ($strPacks)");
    }

    public function insert_plans_packages_prices($str){
        $this->db->query("INSERT INTO tbl_gf_plans_packages_prices (PlanId, CurrencyId, PackageId, Price, ServiceType) VALUES $str");

    }

    public function del_plans_prices_email($planId , $sc , $strPacks){
        $this->db->query("DELETE FROM tbl_gf_plans_prices_email WHERE PlanId = '$planId' AND ServiceType = '$sc' AND PackageId IN ($strPacks)");

    }

    public function insert_plans_prices_email($strPrices){
        $this->db->query("INSERT INTO tbl_gf_plans_prices_email (PlanId, CurrencyId, PackageId, PackTitle, Price, ServiceType) VALUES $strPrices");
    }

    public function del_group_prices_notice($planId , $sc , $strPacks){
        $this->db->query("DELETE FROM tbl_gf_group_prices_notice WHERE PlanId = '$planId' AND ServiceType = '$sc' AND PackageId IN ($strPacks)");
    }

    public function fetch_users_by_planid($planId){
     
        $this->db->select('UserId');
        $this->db->from('tbl_gf_users');
        $this->db->where('PricePlanId' , $planId);
        
        $result = $this->db->get();
        
        return $result->result();
    }

    public function insert_group_prices_notice($strPriceNoticeNew){
        $this->db->query("INSERT INTO tbl_gf_group_prices_notice (PlanId, CurrencyId, PackageId, PackTitle, Price, ServiceType, UserId) VALUES $strPriceNoticeNew");
    }

    public function fetch_product_categories_prices($planId , $defaultCurrencyId ,$sc , $strWhere){
        $query = $this->db->query("SELECT A.ProductId AS PackageId, ProductName AS PackageTitle, IF(Price IS NULL, ProductPrice, Price) AS Price, ProductPrice AS OriginalPrice FROM tbl_gf_product_categories C, tbl_gf_products A LEFT JOIN tbl_gf_plans_packages_prices B 
        ON (A.ProductId = B.PackageId AND PlanId = '$planId' AND CurrencyId = '$defaultCurrencyId' AND B.ServiceType = '$sc') WHERE 
        A.ProductId = C.ProductId AND DisableProduct = 0 $strWhere ORDER BY ProductName");

        return $query->result();
    }

    public function fetch_packages_plans_packages_prices($colId ,$colTitle , $colPrice , $tblPckName , $planId , $defaultCurrencyId , $sc , $colDisable, $strWhere){
        $query=$this->db->query("SELECT A.$colId AS PackageId, $colTitle AS PackageTitle, IF(Price IS NULL, $colPrice, Price) AS Price, 
        $colPrice AS OriginalPrice FROM $tblPckName A LEFT JOIN tbl_gf_plans_packages_prices B 
        ON (A.$colId = B.PackageId AND PlanId = '$planId' AND CurrencyId = '$defaultCurrencyId' AND B.ServiceType = '$sc') WHERE 
        $colDisable = 0 AND ArchivedPack=0 $strWhere ORDER BY PackOrderBy, $colTitle");

        return $query->result();
    }

    public function fetch_suppliers_users($strIdCol , $strECol , $strTbl , $strDCol){
       
        $this->db->select("$strIdCol, FirstName, LastName, $strECol");
        $this->db->from("$strTbl");
        $this->db->where("$strDCol" , 0);

        $result = $this->db->get();
        return $result->result();
    }

    public function update_suppliers_users($strTbl ,  $strPCol , $encPwd , $strIdCol , $UserId){
        $this->db->set($strPCol , $encPwd);
        $this->db->where($strIdCol , $UserId);
        $this->db->update($strTbl);
    }

    public function insert_newsletter($strQry){
        $this->db->query("INSERT INTO tbl_gf_newsletter_to_send (Title, Newsletter, Email) VALUES $strQry");
    }

    public function fetch_users_by_disable(){
        $this->db->select("UserId, FirstName, LastName, UserEmail");
        $this->db->from("tbl_gf_users");
        $this->db->where("DisableUser" , 0);

        $result = $this->db->get();
        return $result->result();
    }


    public function del_users_packages_prices($i){
        if($i == '0')
            $qry = "DELETE FROM tbl_gf_users_packages_prices WHERE PackageId IN (SELECT PackageId FROM tbl_gf_packages WHERE sl3lbf = '0')";
        else if($i == '1')
            $qry = "DELETE FROM tbl_gf_users_packages_prices WHERE PackageId IN (SELECT PackageId FROM tbl_gf_packages WHERE sl3lbf = '1')";
        else if($i == '2')
            $qry = "DELETE FROM tbl_gf_users_packages_prices";
        
        $this->db->query($qry);
        
    }

    public function update_user_news_popup($update_data){
       
        $this->db->where('Id' , 1);
        $this->db->update('tbl_gf_user_news_popup' , $update_data);
    }

    public function fetch_user_news_popup(){
       
        $this->db->select('*');
        $this->db->from('tbl_gf_user_news_popup');
        $this->db->where('Id' , 1);

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_email_integrations(){
       
        $this->db->select('*');
        $this->db->from('tbl_gf_email_integrations');
        $this->db->order_by('APIName' );

        $result = $this->db->get();
        return $result->result();
    }

    public function fetch_email_integrations_by_id($id){
        $this->db->select('*');
        $this->db->from('tbl_gf_email_integrations');
        $this->db->where('Id' , $id);

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_email_integrations_by_api($apiName , $id){
        
        $this->db->select('Id');
        $this->db->from('tbl_gf_email_integrations');
        $this->db->where('APIName' , $apiName);
        $this->db->where('Id !=' , $id);

        $result = $this->db->get();
        return $result->row();
    }

    public function update_email_integrations($update_data , $id){
      
        $this->db->where('Id' , $id);
        $this->db->update('tbl_gf_email_integrations',$update_data);
    }

    public function fetch_users_by_userarchived($strWhere){
        $query=$this->db->query("SELECT UserId, UserEmail , FirstName, LastName FROM tbl_gf_users WHERE DisableUser = 0 AND UserArchived = 0 $strWhere");
        return $query->result();
    }

    public function fetch_rsClients($strWhere){
        $query=$this->db->query("SELECT UserId, UserName, UserEmail, Credits, CONCAT(A.FirstName, ' ', A.LastName) AS Name, Phone, DisableUser, APIKey, CurrencyAbb, 
        PricePlan FROM tbl_gf_users A LEFT JOIN tbl_gf_price_plans C ON (A.PricePlanId = C.PricePlanId) LEFT JOIN tbl_gf_currency B ON 
        (A.CurrencyId = B.CurrencyId) WHERE UserArchived = 1 $strWhere ORDER BY UserName");
        return $query->result();
    }

    public function fetch_payment_details_method($id){
        $query=$this->db->query("SELECT InvAmount, InvTransactionId, InvDtTm, InvComments, PaymentMethod FROM tbl_gf_payment_details A, 
        tbl_gf_payment_methods B WHERE A.InvPaymentMethodId = B.PaymentMethodId AND InvoiceId = '$id' ORDER BY InvDtlId");

        return $query->result();
    }

    public function fetch_payments_by_id($id){
        $query=$this->db->query("SELECT UserId, CreditsTransferred, Credits FROM tbl_gf_payments WHERE PaymentId = '$id'");

        return $query->row();

    }

    public function fetch_users_credits(){
      
        $this->db->select('Credits');
        $this->db->from('tbl_gf_users');
        $this->db->where('UserId' , $userId);

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_payments_users_methods($id){
        $result=$this->db->query("SELECT A.UserId, A.Credits, CodeId, B.UserName, UserEmail, Amount, Currency, DATE(PaymentDtTm) AS PaymentDtTm, C.PaymentMethod 
        FROM tbl_gf_payments A, tbl_gf_users B, tbl_gf_payment_methods C WHERE A.UserId = B.UserId AND A.PaymentMethod = C.PaymentMethodId AND PaymentId = '$id'");

        return $result->row();
    }

    

}
