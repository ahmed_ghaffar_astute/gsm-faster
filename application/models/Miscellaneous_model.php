<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Miscellaneous_model extends CI_Model {

    public function getNewsCat(){
        $query = $this->db->query("SELECT CategoryId, Category, DisableCategory FROM tbl_gf_news_cat ORDER BY OrderBy");

        return $query->result();
    }
    public function countNewsCat(){
        $query = $this->db->query("SELECT CategoryId, Category, DisableCategory FROM tbl_gf_news_cat ORDER BY OrderBy");

        return $query->num_rows();
    }
    //ahmed
    public function fetch_ajaxlookuplist($id,$idCol,$tbl,$textCol,$txtBx,$disableCol,$disableVal){

        $query = $this->db->query("SELECT $idCol FROM $tbl WHERE $textCol = '$txtBx' AND $disableCol = '$disableVal'");
        /*if($id > 0)
            $query = " AND $idCol <> '$id'";*/
        $row = $query->row();
        if (isset($row->$idCol) && $row->$idCol != '')
        {
            $msg = $txtBx." ".$this->lang->line('BE_GNRL_10')."~0";
        }
        else
        {
            if($id == 0)
            {
                $data_insert = array(
                    $textCol => $txtBx,
                    $disableCol => $disableVal
                );
                $this->db->insert($tbl, $data_insert);
                $inserted_id=$this->db->insert_id();
                $msg =  $this->lang->line('BE_GNRL_11')."~1";
            }
            else
            {
                $this->db->set($textCol,$txtBx);
                $this->db->set($disableCol , $disableVal);
                $this->db->where($idCol, $id);
                $this->db->update($tbl);
                $msg = $this->lang->line('BE_GNRL_11')."~0";
            }
        }

        return $msg;

    }
    public function getCategories($id,$tbl,$idCol){
        $this->db->select('*');
        $this->db->from($tbl);
        $this->db->where($idCol, $id);
        $result=$this->db->get();
        return $result->row();
    }
    //ahmed
    public function getAllNews($type){
        $query=$this->db->query("SELECT NewsId, NewsTitle, ShortDescription, DisableNews, A.AddedAt, Category FROM tbl_gf_news A LEFT JOIN tbl_gf_news_cat B ON 
				(A.CategoryId = B.CategoryId) WHERE NewsType = '$type' ORDER BY NewsTitle");
        return $query->result();
    }
    public function getNewsById($id){
        $this->db->select('*');
        $this->db->from('tbl_gf_news');
        $this->db->where('NewsId',$id);
        $result=$this->db->get();
        return $result->result();
    }
    public function deleteNews($id){
        $this->db->where('NewsId', $id);
        $this->db->delete('tbl_gf_news');
    }
    public function addNews($newsTitle,$news,$shortDesc,$url,$disableNews,$dtTm,$seoName,$metaKW,$htmlTitle,$fileName,$metaTags,$categoryId,$type,$atWebsite,$atIMEI,$atFile,$atServer){

        $this->db->set('NewsTitle',$newsTitle);
        $this->db->set('News',$news);
        $this->db->set('ShortDescription',$shortDesc);
        $this->db->set('ImageURL',$url);
        $this->db->set('DisableNews',$disableNews);
        $this->db->set('AddedAt',$dtTm);
        $this->db->set('SEOURLName',$seoName);
        $this->db->set('MetaKW',$metaKW);
        $this->db->set('HTMLTitle',$htmlTitle);
        $this->db->set('FileName',$fileName);
        $this->db->set('MetaTags',$metaTags);
        $this->db->set('CategoryId',$categoryId);
        $this->db->set('NewsType',$type);
        $this->db->set('AtWebsite',$atWebsite);
        $this->db->set('AtIMEI',$atIMEI);
        $this->db->set('AtFile',$atFile);
        $this->db->set('AtServer',$atServer);
        $this->db->insert('tbl_gf_news');
        return $this->db->insert_id();
    }
    public function updateNews($newsTitle,$news,$shortDesc,$disableNews,$metaTags,$metaKW,$seoName,$htmlTitle,$fileName,$categoryId,$atWebsite,$atIMEI,$atFile,$atServer,$id){
        $this->db->set('NewsTitle',$newsTitle);
        $this->db->set('News',$news);
        $this->db->set('ShortDescription',$shortDesc);
        $this->db->set('DisableNews',$disableNews);
        $this->db->set('SEOURLName',$seoName);
        $this->db->set('MetaKW',$metaKW);
        $this->db->set('HTMLTitle',$htmlTitle);
        $this->db->set('FileName',$fileName);
        $this->db->set('MetaTags',$metaTags);
        $this->db->set('CategoryId',$categoryId);
        $this->db->set('AtWebsite',$atWebsite);
        $this->db->set('AtIMEI',$atIMEI);
        $this->db->set('AtFile',$atFile);
        $this->db->set('AtServer',$atServer);
        $this->db->where('NewsId', $id);
        $this->db->update('tbl_gf_news');
        $msg = 'true';
        return $msg;
    }
    public function getNewsByTitle($title,$type,$id){
        $this->db->select('*');
        $this->db->from('tbl_gf_news');
        $this->db->where('NewsTitle',$title);
        $this->db->where('NewsType',$type);
        if($id > 0){
            $this->db->where('NewsId <>',$id);
        }

        $result=$this->db->get();
        //var_dump($id,$this->db->last_query());
        return $result->row();
    }
    public function updateNewsImage($file,$id){
        $this->db->set('NewsImage',$file);
        $this->db->where('NewsId', $id);
        $this->db->update('tbl_gf_news');
    }
    public function deletFaqs($id){
        $this->db->where('FAQId', $id);
        $this->db->delete('tbl_gf_faqs');
    }
    public function getAllFaqs(){
        $this->db->select('FAQId, Question, Answer, DisableFAQ');
        $this->db->from('tbl_gf_faqs');
        $this->db->order_by('Question');
        $result=$this->db->get();
        return $result->result();
    }
    public function insertFaqs($question,$answer,$disableFaq){

        $this->db->set('Question',$question);
        $this->db->set('Answer',$answer);
        $this->db->set('DisableFAQ',$disableFaq);
        $this->db->insert('tbl_gf_faqs');
        return $this->db->insert_id();
    }
    public function getFaqsById($id){
        $this->db->select('*');
        $this->db->from('tbl_gf_faqs');
        $this->db->where('FAQId',$id);
        $result=$this->db->get();
        return $result->row();
    }
    public function updateFaqs($question,$answer,$disableFaq,$id){
        $this->db->set('Question',$question);
        $this->db->set('Answer',$answer);
        $this->db->set('DisableFAQ',$disableFaq);
        $this->db->where('FAQId',$id);
        $this->db->update('tbl_gf_faqs');
        return true;
    }
    public function getKnowledgeBaseCats(){
        $this->db->select('CategoryId, Category, DisableCategory');
        $this->db->from('tbl_gf_knowledgebase_cat');
        $this->db->order_by('OrderBy');
        $result=$this->db->get();
        return $result->result();
    }
    public function getKnowledgeBaseContents(){
        $query = $this->db->query("SELECT Id, Title, Category, DisableKB FROM tbl_gf_knowledgebase A, tbl_gf_knowledgebase_cat B WHERE A.CategoryId = B.CategoryId ORDER BY Title");
        return $query->result();
    }
    public function delKnowledgeBaseCats($id){
        $this->db->where('Id', $id);
        $this->db->delete('tbl_gf_knowledgebase');
    }
    public function checkKnBaseContentsTitle($title,$id){
        $this->db->select('Id');
        $this->db->from("tbl_gf_knowledgebase");
        $this->db->where('Title',$title);
        if($id > 0)
            $this->db->where('Id <>',$id);

        $result=$this->db->get();
        return $result->row();
    }
    public function insertKnBaseContents($title,$contents,$categoryId,$disable){
        $this->db->set('Title',$title);
        $this->db->set('Contents',$contents);
        $this->db->set('CategoryId',$categoryId);
        $this->db->set('DisableKB',$disable);
        $this->db->insert('tbl_gf_knowledgebase');
        return true;
    }
    public function updateKnBaseContents($title,$contents,$categoryId,$disable,$id){
        $this->db->set('Title',$title);
        $this->db->set('Contents',$contents);
        $this->db->set('CategoryId',$categoryId);
        $this->db->set('DisableKB',$disable);
        $this->db->where('Id',$id);
        $this->db->update('tbl_gf_knowledgebase');
        return true;

    }
    public function getKnowledgeBaseContentsById($id){
        $this->db->select('*');
        $this->db->from('tbl_gf_knowledgebase');
        $this->db->where('Id',$id);
        $result = $this->db->get();
         return $result->row();
    }
    public function getAllNewsLtrCats(){
        $this->db->select('CategoryId, Category, DisableCategory');
        $this->db->from('tbl_gf_newsltrs_cat');
        $this->db->order_by('OrderBy');
        $result = $this->db->get();
        return $result->result();
    }
    public function getAllNewsletters(){
        $query = $this->db->query('SELECT NewsLtrId, NewsLtrTitle, Category, DisableNewsLtr FROM tbl_gf_news_letter A LEFT JOIN tbl_gf_newsltrs_cat B ON (A.CategoryId = B.CategoryId) 
				ORDER BY NewsLtrTitle');
        return $query->result();
    }
    public function deleteNewsletters($id){
        $this->db->where('Id', $id);
        $this->db->delete('tbl_gf_knowledgebase');
    }
    public function getNewsLtrById($id){
        $this->db->select('*');
        $this->db->from('tbl_gf_news_letter');
        $this->db->where('NewsLtrId',$id);
        $result = $this->db->get();
        return $result->row();
    }
    public function addNewsLtr($newsLtrTitle,$newsLtr,$categoryId,$disableNewsLtr){
        $this->db->set('NewsLtrTitle',$newsLtrTitle);
        $this->db->set('NewsLtr',$newsLtr);
        $this->db->set('CategoryId',$categoryId);
        $this->db->set('DisableNewsLtr',$disableNewsLtr);
        $this->db->insert('tbl_gf_news_letter');
        return true;
    }
    public function updateNewsLtr($newsLtrTitle,$newsLtr,$categoryId,$disableNewsLtr,$id){
        $this->db->set('NewsLtrTitle',$newsLtrTitle);
        $this->db->set('NewsLtr',$newsLtr);
        $this->db->set('CategoryId',$categoryId);
        $this->db->set('DisableNewsLtr',$disableNewsLtr);
        $this->db->where('NewsLtrId',$id);
        $this->db->update('tbl_gf_news_letter');
        return true;
    }
    public function getNewsLtrByTitle($title,$id){
        $this->db->select('NewsLtrId');
        $this->db->from('tbl_gf_news_letter');
        $this->db->where('NewsLtrTitle',$title);
        if($id > 0)
            $this->db->where('NewsLtrId','<>',$id);
        $result = $this->db->get();
        return $result->row();
    }
    public function getEmailSettingById($id){
        $this->db->select('SendNLViaCron');
        $this->db->from('tbl_gf_email_settings');
        $this->db->where('Id',$id);
        $result = $this->db->get();
        return $result->row();
    }
    public function getUserDetail($id){
        $this->db->select('UserId, UserEmail');
        $this->db->from('tbl_gf_users');
        $this->db->where('DisableUser',0);
        $this->db->where('UserArchived',0);
        $this->db->where('Subscribed',1);
        if($id !='0')
            $this->db->where('PricePlanId',$id);
        $result = $this->db->get();
        return $result->result();
    }
    public function getSubscriptions(){
        $this->db->select('Id, Email');
        $this->db->from('tbl_gf_subscriptions');
        $result = $this->db->get();
        if($result->num_rows() > 0){
            return $result->result();
        }else{
            return false;
        }
    }
    public function insertNewsltrToSend($newsLtrTitle,$Newsletter,$Email){
        $this->db->set('Title',$newsLtrTitle);
        $this->db->set('Newsletter',$Newsletter);
        $this->db->set('Email',$Email);
        $this->db->insert('tbl_gf_newsletter_to_send');
        return true;
    }
    public function deleteVideos($id){
        $this->db->where('VideoId', $id);
        $this->db->delete('tbl_gf_videos');
    }
	public function getAllVideos(){
		$this->db->select('VideoId, Title, DisableVideo, YouTubeLnk');
		$this->db->from('tbl_gf_videos');
		$this->db->order_by('Title');
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return $result->result();
		}else{
			return false;
		}
	}
	public function getVideosByTitle($title,$id){
		$this->db->select('VideoId');
		$this->db->from('tbl_gf_videos');
		$this->db->where('Title',$title);
		if($id > 0)
			$this->db->where('VideoId',$id);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return $result->row()->VideoId;
		}else{
			return false;
		}
	}
	public function getVideosById($id){
		$this->db->select('*');
		$this->db->from('tbl_gf_videos');
		$this->db->where('VideoId',$id);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return $result->row();
		}else{
			return false;
		}
	}
	public function insertVideos($title,$desc,$lnk,$disable){
		$this->db->set('Title',$title);
		$this->db->set('Description',$desc);
		$this->db->set('YouTubeLnk',$lnk);
		$this->db->set('DisableVideo',$disable);
		$this->db->insert('tbl_gf_videos');
		return true;
	}
	public function updateVideos($title,$desc,$lnk,$disable,$id){
		$this->db->set('Title',$title);
		$this->db->set('Description',$desc);
		$this->db->set('YouTubeLnk',$lnk);
		$this->db->set('DisableVideo',$disable);
		$this->db->where('VideoId',$id);
		$this->db->update('tbl_gf_videos');
		return true;
	}
	public function getFlagCounersById($id){
		$this->db->select('*');
		$this->db->from('tbl_gf_flag_counter');
		$this->db->where('Id',$id);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return $result->row();
		}else{
			return false;
		}
	}
	public function updateFalgCounters($script,$disable,$id){
		$this->db->set('ScriptCode',$script);
		$this->db->set('DisableFCounter',$disable);
		$this->db->where('Id',$id);
		$this->db->update('tbl_gf_flag_counter');
		return true;
	}
	public function getAnalyticsById($id){
		$this->db->select('*');
		$this->db->from('tbl_gf_analytics');
		$this->db->where('Id',$id);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return $result->row();
		}else{
			return false;
		}
	}
	public function updateAnalytics($code,$disable,$id){
		$this->db->set('Code',$code);
		$this->db->set('DisableCode',$disable);
		$this->db->where('Id',$id);
		$this->db->update('tbl_gf_analytics');
		return true;
	}
	public function getMarqueeById($id){
		$this->db->select('*');
		$this->db->from('tbl_gf_marquee');
		$this->db->where('Id',$id);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return $result->row();
		}else{
			return false;
		}
	}
	public function updateMarquee($code,$position,$disable,$id){
		$this->db->set('Code',$code);
		$this->db->set('Position',$position);
		$this->db->set('DisableMarquee',$disable);
		$this->db->where('Id',$id);
		$this->db->update('tbl_gf_marquee');
		return true;
	}
	public function delSubscription($id){
		$this->db->where('Id', $id);
		$this->db->delete('tbl_gf_subscriptions');
	}
	public function getSubscription(){
		$this->db->select('*');
		$this->db->from('tbl_gf_subscriptions');
		$this->db->order_by('Id','DESC');
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return $result->result();
		}else{
			return false;
		}
	}
	public function delDownloads($id){
		$this->db->where('DownloadId', $id);
		$this->db->delete('tbl_gf_downloads');
	}
	public function getDownloads(){
		$this->db->select('*');
		$this->db->from('tbl_gf_downloads');
		$this->db->order_by('DownloadId','DESC');
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return $result->result();
		}else{
			return false;
		}
	}
	public function insertDownloads($title,$disable,$file){
		$this->db->set('Title',$title);
		$this->db->set('DisableDownload',$disable);
		$this->db->set('FileURL',$file);
		$this->db->insert('tbl_gf_downloads');
		return $this->db->insert_id();
	}
	public function updateDownloads($title='',$disable='',$file='',$id=''){
		if($title!='' && $disable!=''){
			$this->db->set('Title',$title);
			$this->db->set('DisableDownload',$disable);
		}
		if($file!=''){
			$this->db->set('FileURL',$file);
		}
		$this->db->where('DownloadId',$id);
		$query = $this->db->update('tbl_gf_downloads');
		return true;
	}
	public function getDownloadsById($id){
		$this->db->select('*');
		$this->db->from('tbl_gf_downloads');
		$this->db->where('DownloadId',$id);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return $result->row();
		}else{
			return false;
		}
	}
	public function getDownloadsByTitle($title,$id=''){
		$this->db->select('DownloadId');
		$this->db->from('tbl_gf_downloads');
		$this->db->where('Title',$title);
		if($id > 0)
			$this->db->where('DownloadId','<>',$title);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return $result->row()->DownloadId;
		}else{
			return 0;
		}
	}
}
