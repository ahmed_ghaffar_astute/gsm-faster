<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sms_gateway_model extends CI_Model {
	public function getSmsGatwayById($id){
		$query = $this->db->query("SELECT * FROM tbl_gf_sms_gateways WHERE SMSGatewayId = $id");

		return $query->row();
	}
	public function getSmsGatwayId($gateway,$id){
		$query = $this->db->query("SELECT SMSGatewayId FROM tbl_gf_sms_gateways WHERE SMSGateway = '$gateway' AND SMSGatewayId <> '$id'");

		return $query->row();
	}
	public function getSmsGatway(){
		$query = $this->db->query("SELECT * FROM tbl_gf_sms_gateways ORDER BY SMSGateway");

		return $query->result();
	}
	public function updateSmsGateway($data,$id){
		$this->db->set($data);
		$this->db->where('SMSGatewayId',$id);
		$this->db->update('tbl_gf_sms_gateways');
		return true;
	}
}
