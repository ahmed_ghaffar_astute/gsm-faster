<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_dashboard_model extends CI_Model {

    public function fetch_currency_data(){
      
        $this->db->select('CurrencyId, CurrencyAbb, CurrencySymbol ');
        $this->db->from('tbl_gf_currency');
        $this->db->where('DisableCurrency' , 0);
        $this->db->where('DefaultCurrency' , 1);

        $result = $this->db->get();
        return $result->row();


    }

    public function fetch_payments_and_currency_data($DEFAULT_CURR_ID){
        $query = $this->db->query("SELECT UserId, B.CurrencyId, Credits FROM tbl_gf_payments A, tbl_gf_currency B WHERE DATE(PaymentDtTm) = CURDATE() AND A.Currency = B.CurrencyAbb AND PaymentStatus = 2 AND B.CurrencyId = '$DEFAULT_CURR_ID'");
        return $query->result();
    }

    public function fetch_payments_and_currency_by_PaymentDtTm($DEFAULT_CURR_ID){
        $query  = $this->db->query("SELECT UserId, B.CurrencyId, Credits FROM tbl_gf_payments A, tbl_gf_currency B WHERE PaymentDtTm > (NOW() - INTERVAL 1 MONTH)  AND A.Currency = B.CurrencyAbb AND PaymentStatus = 2 AND B.CurrencyId = '$DEFAULT_CURR_ID'");
        return $query->result();
    }

    public function fetch_payments_and_currency_by_interval($DEFAULT_CURR_ID){
        $query = $this->db->query("SELECT UserId, B.CurrencyId, Credits FROM tbl_gf_payments A, tbl_gf_currency B WHERE PaymentDtTm > (NOW() - INTERVAL 12 MONTH)  AND A.Currency = B.CurrencyAbb AND PaymentStatus = 2 AND B.CurrencyId = '$DEFAULT_CURR_ID'");
        return $query->result();
    }

    public function fetch_code_by_verify(){
       
        $this->db->select('COUNT(CodeId) as V');
        $this->db->from('tbl_gf_codes');
        $this->db->where('Verify' , 1);

        $result = $this->db->get();
        return $result->row();

    
    }

    public function fetch_code_by_codestatusid($currDt){
     
         $this->db->select('COUNT(CodeId) AS Orders');
         $this->db->from('tbl_gf_codes');
         $this->db->where('CodeStatusId' , 2);
         $this->db->where('MONTH(RequestedAt)' , date('m' , strtotime($currDt)));
         $this->db->where('YEAR(RequestedAt)' , date('Y' ,strtotime($currDt)));
 
         $result = $this->db->get();
         return $result->row();

       
    }

    public function fetch_code_by_year($currDt){
      
        $this->db->select('COUNT(CodeId) AS Orders');
        $this->db->from('tbl_gf_codes');
        $this->db->where('CodeStatusId' , 2);
        $this->db->where('YEAR(RequestedAt)' , date('Y' ,strtotime($currDt)));

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_codes_sum(){
        $result = $this->db->query("SELECT SUM( IF( CodeStatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( CodeStatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_codes");
        return $result->row();
    }

    public function fetch_codes_sum_by_ids($currDt){
        $result = $this->db->query("SELECT SUM( IF( CodeStatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( CodeStatusId = '2', 1, 0 ) ) AS `CA`, 
                        SUM( IF( CodeStatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( CodeStatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_codes
                        WHERE DATE(RequestedAt) = DATE('$currDt')");
         return $result->row();
        
    }

    public function fetch_code_by_verify_slbf(){
     
        $this->db->select('COUNT(CodeId) AS V');
        $this->db->from('tbl_gf_codes_slbf');
        $this->db->where('Verify=' ,1);

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_code_slbf_by_codestatusid($currDt){
        $query=$this->db->query("SELECT COUNT(CodeId) AS Orders FROM tbl_gf_codes_slbf WHERE CodeStatusId = '2' AND MONTH(RequestedAt) = MONTH('$currDt') AND YEAR(RequestedAt) = YEAR('$currDt')");
        return $query->row();
    }

    public function fetch_code_slbf_by_year($currDt){
        
        $this->db->select('COUNT(CodeId) AS Orders');
        $this->db->from('tbl_gf_codes_slbf');
        $this->db->where('CodeStatusId' , 2);
        $this->db->where('YEAR(RequestedAt)' , date('Y' ,strtotime($currDt)) );

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_codes_slbf_sum(){
        $query = $this->db->query("SELECT SUM( IF( CodeStatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( CodeStatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_codes_slbf");
        return $query->row();
    }

    public function fetch_codes_slbf_sum_by_ids($currDt){
        $query=$this->db->query("SELECT SUM( IF( CodeStatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( CodeStatusId = '2', 1, 0 ) ) AS `CA`, SUM( IF( CodeStatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( CodeStatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_codes_slbf WHERE DATE(RequestedAt) = DATE('$currDt')");
        return $query->row();
    }

    public function fetch_log_requests_by_id(){
       
        $this->db->select('COUNT(LogRequestId) AS V');
        $this->db->from('tbl_gf_log_requests');
        $this->db->where('Verify' , 1);

        $result = $this->db->get();
        return $result->row();

    }

    public function fetch_log_requests_by_month($currDt){
        $query=$this->db->query("SELECT COUNT(LogRequestId) AS Orders FROM tbl_gf_log_requests WHERE StatusId = '2' AND MONTH(RequestedAt) = MONTH('$currDt') AND YEAR(RequestedAt) = YEAR('$currDt')");
        return $query->row();
    }


    public function fetch_log_requests_by_year($currDt){
      
        $this->db->select('COUNT(LogRequestId) AS Orders');
        $this->db->from('tbl_gf_log_requests');
        $this->db->where('StatusId' , 2);
        $this->db->where('YEAR(RequestedAt)' , date('Y' ,strtotime($currDt)) );

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_log_requests_sum_by_id(){
        $query = $this->db->query("SELECT SUM( IF( StatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( StatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_log_requests");
        return $query->row();
    }

    public function fetch_log_requests_sum_by_ids($currDt){
        $query=$this->db->query("SELECT SUM( IF( StatusId = '1', 1, 0 ) ) AS `P`, SUM( IF( StatusId = '2', 1, 0 ) ) AS `CA`, SUM( IF( StatusId = '3', 1, 0 ) ) AS `NA`, SUM( IF( StatusId = '4', 1, 0 ) ) AS `IP` FROM tbl_gf_log_requests WHERE DATE(RequestedAt) = DATE('$currDt')");
        return $query->row();
    }

    public function fetch_codes_packages_and_category(){
       
        $this->db->select("COUNT(A.PackageId) AS TotalRequests, CONCAT(Category,' - ', PackageTitle) AS PackageTitle");
        $this->db->from('tbl_gf_codes A,tbl_gf_packages B, tbl_gf_package_category C');
        $this->db->where('A.PackageId = B.PackageId');
        $this->db->where('B.CategoryId = C.CategoryId');
        $this->db->group_by('A.PackageId');
        $this->db->order_by('COUNT(A.PackageId)' , 'DESC');
        $this->db->limit(10 , 0);

        $result=$this->db->get();
        return $result->result();

    }

    public function fetch_slbf_codes_packages_and_category(){
       
        $this->db->select("COUNT(A.PackageId) AS TotalRequests, CONCAT(Category,' - ', PackageTitle) AS PackageTitle");
        $this->db->from('tbl_gf_codes_slbf A,tbl_gf_packages B, tbl_gf_package_category C');
        $this->db->where('A.PackageId = B.PackageId');
        $this->db->where('B.CategoryId = C.CategoryId');
        $this->db->group_by('A.PackageId');
        $this->db->order_by('COUNT(A.PackageId)' , 'DESC');
        $this->db->limit(10 , 0);

        $result=$this->db->get();
        return $result->result();

        $objDBCD14->query("SELECT COUNT(A.PackageId) AS TotalRequests, CONCAT(Category,' - ', PackageTitle) AS PackageTitle FROM tbl_gf_codes_slbf A,
				tbl_gf_packages B, tbl_gf_package_category C WHERE A.PackageId = B.PackageId AND B.CategoryId = C.CategoryId GROUP BY A.PackageId ORDER BY 
				COUNT(A.PackageId) DESC LIMIT 0, 10");

    }

    public function fetch_log_requests_packages_and_category(){
       

        $this->db->select(" COUNT(A.LogPackageId) AS TotalRequests, CONCAT(Category,' - ', LogPackageTitle) AS PackageTitle");
        $this->db->from('tbl_gf_log_requests A, tbl_gf_log_packages B, tbl_gf_log_package_category C');
        $this->db->where('A.LogPackageId = B.LogPackageId');
        $this->db->where('B.CategoryId = C.CategoryId');
        $this->db->group_by('A.LogPackageId');
        $this->db->order_by('COUNT(A.LogPackageId)' , 'DESC');
        $this->db->limit(10 , 0);

        $result=$this->db->get();
        return $result->result();
        
    }

    public function fetch_precodes_logpackages(){
       
        $this->db->select("COUNT(PreCodeId) AS TotalPCs, LogPackageTitle");
        $this->db->from('tbl_gf_precodes A, tbl_gf_log_packages B');
        $this->db->where('A.ServiceId = B.LogPackageId');
        $this->db->where('A.ServiceType' , 2);
        $this->db->where('Assigned' , 0);
        $this->db->group_by(array("ServiceId" , "A.ServiceType"));
        $this->db->order_by('LogPackageTitle');
     
        $result=$this->db->get();
        return $result->result();

    }

    public function fetch_users_payments_record($DEFAULT_CURR_ID){
       
        $this->db->select("A.Amount, ByAdmin, A.UserId");
        $this->db->from('tbl_gf_payments A, tbl_gf_users B');
        $this->db->where('A.UserId = B.UserId');
        $this->db->where('CurrencyId' , $DEFAULT_CURR_ID);
        $this->db->where('PaymentStatus' , 1);

        $result=$this->db->get();
        return $result->result();

    }

    public function fetch_disable_and_archived_users(){
        
        $this->db->select("COUNT(UserId) AS PU");
        $this->db->from('tbl_gf_users');
        $this->db->where('DisableUser' , 1);
        $this->db->where('UserArchived' , 0);
      
        $result=$this->db->get();
        return $result->row();
        
    }

    public function fetch_sum_codes(){
      
        $this->db->select("SUM( IF( CodeStatusId = '1', 1, 0 ) ) AS P");
        $this->db->from('tbl_gf_codes');
     
        $result=$this->db->get();
        return $result->row();
    }

    public function fetch_sum_codesslbf(){
       
        $this->db->select("SUM( IF( CodeStatusId = '1', 1, 0 ) ) AS P");
        $this->db->from('tbl_gf_codes_slbf');
     
        $result=$this->db->get();
        return $result->row();
    }

    public function fetch_sum_logrequests(){
     
        $this->db->select("SUM( IF( StatusId = '1', 1, 0 ) ) AS P");
        $this->db->from('tbl_gf_log_requests');
     
        $result=$this->db->get();
        return $result->row();
    }

    public function fetch_user_invoices_sum(){
       
        $this->db->select("SUM( IF( ByAdmin = '1', 1, 0 ) ) AS AdminInvoices, SUM( IF( ByAdmin = '0', 1, 0 ) ) AS UserInvoices");
        $this->db->from('tbl_gf_payments');
        $this->db->where('PaymentStatus' , 1);

        $result=$this->db->get();
        return $result->row();

    }

    public function fetch_admin_pass_by_id(){
       
        $this->db->select('AdminUserPassword');
        $this->db->from('tbl_gf_admin');
        $this->db->where('AdminId' , $this->session->userdata('GSM_FSN_AdmId'));

        $result = $this->db->get();
        return $result->row();
    }

    public function update_admin_password($update_data , $where= array()){
        if($where){
            $this->db->where($where);
        }

        $this->db->update('tbl_gf_admin' , $update_data);
    }

    public function update_codes($update_data, $where = array() , $where_in = array()){

        if($where){
            $this->db->where($where);
        }

        if($where_in){
            $this->db->where_in($where_in);
        }

        $this->db->update('tbl_gf_codes' , $update_data);

      
    }

    public function update_codes_slbf($update_data, $where = array() , $where_in = array()){

        if($where){
            $this->db->where($where);
        }

        if($where_in){
            $this->db->where_in($where_in);
        }

        $this->db->update('tbl_gf_codes_slbf' , $update_data);   
    }

    public function update_log_requests($update_data, $where = array() , $where_in = array()){

        if($where){
            $this->db->where($where);
        }

        if($where_in){
            $this->db->where_in($where_in);
        }

        $this->db->update('tbl_gf_log_requests' , $update_data);   
    }

    public function update_admin_log($update_data, $where = array() , $where_in = array()){

        if($where){
            $this->db->where($where);
        }

        if($where_in){
            $this->db->where_in($where_in);
        }

        $this->db->update('tbl_gf_admin_login_log' , $update_data);   
    }

    public function fetch_users_log($currDt){
        $query=$this->db->query("SELECT UserName, A.IP, LogDate, CONCAT(FirstName, ' ', LastName) AS UName FROM tbl_gf_userlog A, tbl_gf_users B WHERE A.UserId = B.UserId AND 
                            LogDate >= DATE_SUB('$currDt', INTERVAL 10 MINUTE)");

        return $query->result();
    
        
    }

    public function fetch_users_log_count($currDt){
        $query=$this->db->query("SELECT COUNT(LogId) AS TotalRecs FROM tbl_gf_userlog A, tbl_gf_users B WHERE A.UserId = B.UserId AND 
        LogDate >= DATE_SUB('$currDt', INTERVAL 10 MINUTE)");

        return $query->row();
    }



}