<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tickets_model extends CI_Model {

    public function del_tickets($ids){
        $this->db->where_in('TicketNo' , $ids);
        $this->db->delete('tbl_gf_tickets');
    }


    public function update_tickets($update_data  ,  $where = array() ,$where_in = array()){
        if($where){
            $this->db->where($where);
        }
        if($where_in){
            $this->db->where_in($where_in);
        }

        $this->db->update('tbl_gf_tickets',$update_data);
    }

    public function fetch_tickets_by_ids($ids){
        
        $this->db->select('UserId');
        $this->db->from('tbl_gf_tickets');
        $this->db->where_in('TicketNo' , $ids);

        $result = $this->db->get();
        return $result->result();
        
    }

    public function fetch_tickets_data_and_users($strWhere , $eu , $limit){
        $query=$this->db->query("SELECT TicketId, Name, F.UserName, UserName AS ClientName, DeptName, TcktCategory, TcktPriority, TicketNo, TcktStatus, 
        Subject, DtTm, A.IP, ReadByAdmin, A.PriorityId FROM tbl_gf_tckt_dept B, tbl_gf_tckt_priority C, tbl_gf_tckt_status D, tbl_gf_tckt_category E, tbl_gf_tickets A 
        LEFT JOIN tbl_gf_users F ON (A.UserId = F.UserId) WHERE A.DepartmentId = B.DeptId AND A.PriorityId = C.TcktPriorityId AND A.StatusId = D.TcktStatusId AND 
        A.CategoryId = E.TcktCategoryId AND AId = 0 $strWhere ORDER BY TicketId DESC LIMIT $eu, $limit");

        return $query->result();
    }

    public function fetch_tckt_dept(){
       
        $this->db->select('DeptId AS Id, DeptName AS Value');
        $this->db->from('tbl_gf_tckt_dept');
        $this->db->where('DisableDept' , 0);
        $this->db->order_by('DeptName');

        $result = $this->db->get();
        return $result->result();
    }

    public function fetch_tckt_cat(){
     
        $this->db->select('TcktCategoryId AS Id, TcktCategory AS Value');
        $this->db->from('tbl_gf_tckt_category');
        $this->db->where('DisableTcktCategory' , 0);
        $this->db->order_by('TcktCategory');

        $result = $this->db->get();
        return $result->result();
    }

    public function fetch_tckt_priority(){
       
        $this->db->select('TcktPriorityId AS Id, TcktPriority AS Value');
        $this->db->from('tbl_gf_tckt_priority');
        $this->db->where('DisableTcktPriority' , 0);
        $this->db->order_by('TcktPriorityId');

        $result = $this->db->get();
        return $result->result();
    }

    public function fetch_tickets_data_and_users_count($strWhere , $eu , $limit){
        $query=$this->db->query("SELECT COUNT(TicketId) AS TotalRecs FROM tbl_gf_tckt_dept B, tbl_gf_tckt_priority C, tbl_gf_tckt_status D, 
                        tbl_gf_tckt_category E, tbl_gf_tickets A LEFT JOIN tbl_gf_users F ON (A.UserId = F.UserId) WHERE A.DepartmentId = B.DeptId AND A.PriorityId = 		
                        C.TcktPriorityId AND A.StatusId = D.TcktStatusId AND A.CategoryId = E.TcktCategoryId AND AId = 0 $strWhere");

        $row = $query->row();
        return $row->TotalRecs;
    }

    public function fetch_ticket_data($id){
       
        $this->db->select('*');
        $this->db->from('tbl_gf_tickets');
        $this->db->where('TicketId' , $id);
       
        $result = $this->db->get();
        return $result->row();
    }

    public function insert_ticket_data($insert_data){
        $this->db->insert('tbl_gf_tickets' , $insert_data);
        return $this->db->insert_id();
    }

    public function fetch_tickets_dept_users($tcktNo){
      
        $this->db->select('TicketId, Name, F.UserName, F.UserName AS ClientName, A.StatusId, Message, DtTm, A.IP, ByAdmin, DeptName');
        $this->db->from('tbl_gf_tckt_dept B, tbl_gf_tickets A ');
        $this->db->join('tbl_gf_users F' , 'A.UserId = F.UserId' , 'left');
        $this->db->where('TicketNo' , $tcktNo);
        $this->db->where('A.DepartmentId = B.DeptId');
        $this->db->order_by('TicketId' , 'DESC');

        $result = $this->db->get();
        return $result->result();

    } 

    public function fetch_tckt_status(){
      
        $this->db->select(' TcktStatusId AS Id, TcktStatus AS Value');
        $this->db->from('tbl_gf_tckt_status');
        $this->db->where('DisableTcktStatus' , 0);
        $this->db->order_by('TcktStatusId');

        $result = $this->db->get();
        return $result->result();
    }

    public function fetch_tckt_department(){
      
        $this->db->select("DeptId, DeptName, DisableDept, DeptEmail");
        $this->db->from('tbl_gf_tckt_dept');
        $this->db->order_by('DeptName');

        $result = $this->db->get();
        return $result->result();
    }

    public function fetch_department_data($id){
      
        $this->db->select("*");
        $this->db->from('tbl_gf_tckt_dept');
        $this->db->where('DeptId' , $id);

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_tckt_dept_by_id($dept ,$disableVal , $id){
    
        $this->db->select('DeptId');
        $this->db->from('tbl_gf_tckt_dept');
        $this->db->where('DeptName' , $dept);
        $this->db->where('DisableDept' , $disableVal);
        if($id > 0){
            $this->db->where('DeptId' , $id);
        }

        $result = $this->db->get();
        return $result->row();
        
    }

    public function insert_tckt_dept($insert_data){
     
        $this->db->insert('tbl_gf_tckt_dept' , $insert_data);
    }

    public function update_tckt_dept($update_data , $where = array() , $where_in = array()){
     
        if($where){
            $this->db->where($where);
        }
        if($where_in){
            $this->db->where_in($where_in);
        }

        $this->db->update('tbl_gf_tckt_dept',$update_data);
    }
}