<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Services_model extends CI_Model {

    public function update_gf_codes($update_data , $where_data = array() , $where_in = array()){
     

        if($where_data){
            $this->db->where($where_data);
        }
        if($where_in){
            $this->db->where_in($where_in);
        }
        
        $this->db->update('tbl_gf_codes',$update_data);

    }

    public function update_slbf_codes($update_data , $where_data = array() , $where_in = array()){
        if($where_data){
            $this->db->where($where_data);
        }
        if($where_in){
            $this->db->where_in($where_in);
        }
        
        $this->db->update('tbl_gf_codes_slbf',$update_data);
    }


    public function update_log_requests($update_data, $where_data = array() , $where_in = array()){
        if($where_data){
            $this->db->where($where_data);
        }
        if($where_in){
            $this->db->where_in($where_in);
        }
        
        $this->db->update('tbl_gf_log_requests',$update_data);
    }

    public function update_packages($update_data, $where_data = array() , $where_in = array()){
        if($where_data){
            $this->db->where($where_data);
        }
        if($where_in){
            $this->db->where_in($where_in);
        }
        
        $this->db->update('tbl_gf_packages',$update_data);
    }

    public function fetch_packages_codes_by_date($strWhere){
        $query = $this->db->query("SELECT A.PackageId, PackageTitle, COUNT(A.CodeId) AS TotalOrders, APITitle FROM tbl_gf_codes A, tbl_gf_packages B LEFT JOIN tbl_gf_api C 
        ON (B.APIId = C.APIId) WHERE A.PackageId = B.PackageId AND AdminArchived = 0 AND CodeStatusId = 1 AND sl3lbf = 0 $strWhere 
        GROUP BY PackageId ORDER BY PackOrderBy, PackageTitle");

        return $query->result();
    }

    public function fetch_codes_by_date($strWhere){
        $query = $this->db->query("SELECT PackageId, MessageFromServer, RequestedAt, CronDelayTm FROM tbl_gf_codes WHERE CodeStatusId = 1 AND OrderAPIId > 0 
        AND CodeSentToOtherServer = '0' AND HiddenStatus = '1' $strWhere");

        return $query->result();

    }
	public function fetch_codes_by_id($query_db,$id){
		$query = $this->db->query($query_db);
		return $query->row();

	}

    public function fetch_codes_by_statusId_packageId($SERVICE_ID , $IMEI){
       
        $this->db->select('CodeId');
        $this->db->from('tbl_gf_codes');
        $this->db->where('IMEINo' , $IMEI);
        $this->db->where('PackageId' , $SERVICE_ID);
        $this->db->where_in('CodeStatusId',array(2,4));

        $result=$this->db->get();
        return $result->row();

    }

    public function fetch_packages_by_ids($packIds){
      
        $this->db->select('PackageId, PackageTitle');
        $this->db->from('tbl_gf_packages');
        $this->db->where_in('PackageId',$packIds);

        $result=$this->db->get();
        return $result->result();

    }
    public function fetch_packages_by_Archived($packIds){

        $this->db->select('PackageId, sl3lbf');
        $this->db->from('tbl_gf_packages');
        $this->db->where('ArchivedPack',$packIds);
        $result=$this->db->get();
        return $result->result();

    }
    public function fetch_log_packages_by_Archived($packIds){

        $this->db->select('LogPackageId');
        $this->db->from('tbl_gf_log_packages');
        $this->db->where('ArchivedPack',$packIds);
        $result=$this->db->get();
        return $result->result();

    }

    public function delete_order_emails($SELECTED_IDS , $STATUS_ID){
       
        $this->db->where_in('OrderIds', $SELECTED_IDS);
        $this->db->where('OrderStatus' , $STATUS_ID);
        $this->db->delete('tbl_gf_order_emails');
    }

    public function insert_order_emails_data($SELECTED_IDS ,$STATUS_ID){
        

        $this->db->set('OrderIds' , $SELECTED_IDS);
        $this->db->set('sc' , 0);
        $this->db->set('OrderStatus' , $STATUS_ID);
        $this->db->insert('tbl_gf_order_emails');

    }

    public function fetch_api_data_by_id($apiId){
        
        $this->db->select('APIId, APIKey, APITitle, APIType, ServerURL, AccountId');
        $this->db->from('tbl_gf_api');
        $this->db->where('APIId',$apiId);
        $this->db->where('DisableAPI' , 0);

        $result=$this->db->get();
        return $result->row();
    }

    public function select_general_query($idCol ,$tblName , $imeiCol , $IMEI, $serviceCol , $SERVICE_ID , $statusCol, $ORDER_ID){
        
        $this->db->select($idCol);
        $this->db->from($tblName);
        $this->db->where($imeiCol , $IMEI);
        $this->db->where($serviceCol , $SERVICE_ID);
        $this->db->where($statusCol.' <>' , 3);
        $this->db->where($idCol.' <>', $ORDER_ID);

        $result=$this->db->get();
        return $result->row();

    }

    public function update_general_query($tblName , $statusCol , $rwCode ,$idCol ,$ORDER_ID){
       
        $this->db->set($statusCol , 4);
        $this->db->set('LinkedOrderId' , $rwCode);
        $this->db->where($idCol , $ORDER_ID);
        $this->db->update($tblName);

    }


    public function update_query($tblName ,$apiId , $serverURL ,$apiUserName ,$apiType ,$apiKey,$supplierPackId , $apiName ,$idCol ,$ORDER_ID){
    
        $this->db->set('OrderAPIId' , $apiId);
        $this->db->set('OrderAPIURL' , $serverURL);
        $this->db->set('OrderAPIUserName' , $apiUserName);
        $this->db->set('OrderAPIType' , $apiType);
        $this->db->set('OrderAPIKey' , $apiKey);
        $this->db->set('OrderAPIServiceId' , $supplierPackId);
        $this->db->set('OrderType' , 1);
        $this->db->set('OrderAPIName' , $apiName);
        $this->db->set('HiddenStatus' , 0);
        $this->db->set('MessageFromServer' , '');
        $this->db->where($idCol , $ORDER_ID);
        $this->db->update($tblName);

    }

    public function fetch_codes_packages_users_currency_data($strWhere ,$orderBy , $eu, $limit){
        $query=$this->db->query("SELECT A.PackageId, CodeId, A.Credits, Code, A.CodeStatusId, CodeStatus, IMEINo, RequestedAt, IF(ReplyDtTm IS NULL, '-', ReplyDtTm) AS ReplyDtTm, PackageTitle,	TimeTaken, 
        A.UserId, UserName, A.Credits, If(A.OrderAPIId = '' OR A.OrderAPIId = '0' OR A.OrderAPIId = '-1', '-', 'API') AS API, CodeSentToOtherServer, MessageFromServer, OrderAPIId, 
        A.IP, CurrencyAbb, Profit, LinkedOrderId, OrderCostPrice, OrderType FROM tbl_gf_codes A, tbl_gf_packages B, tbl_gf_users D, tbl_gf_currency C, tbl_gf_code_status E 
        WHERE A.PackageId = B.PackageId AND A.UserId = D.UserId AND D.CurrencyId = C.CurrencyId AND A.CodeStatusId = E.CodeStatusId AND AdminArchived = 0 $strWhere 
        ORDER BY CodeId $orderBy LIMIT $eu, $limit");

        return $query->result();

    }

    public function fetch_codes_packages_users_data($strWhere ,$eu, $limit){
        $query=$this->db->query("SELECT A.PackageId, CodeId, A.Credits, Code, IMEINo, RequestedAt, Mep, SerialNo, OtherValue, PackageTitle, Verify, A.UserId, UserName, VerifyData 
        FROM tbl_gf_codes A, tbl_gf_packages B, tbl_gf_users D WHERE A.PackageId = B.PackageId AND A.UserId = D.UserId AND Verify = 1 $strWhere ORDER BY CodeId DESC LIMIT $eu, $limit");

        return $query->result();

    }

    public function fetch_codes_slbf_by_statusId_packageId($IMEI , $SERVICE_ID){
       
        $this->db->select('CodeId');
        $this->db->from('tbl_gf_codes_slbf');
        $this->db->where('IMEINo' , $IMEI);
        $this->db->where('PackageId' , $SERVICE_ID);
        $this->db->where_in('CodeStatusId',array(2,4));

        $result=$this->db->get();
        return $result->row();
    
    }

    public function fetch_codes_slbf_packages_user($strWhere ,$eu, $limit){
        $query=$this->db->query("SELECT A.PackageId, IMEINo, CodeId, Code, RequestedAt, IF(ReplyDtTm IS NULL, '-', ReplyDtTm) AS ReplyDtTm, A.CodeStatusId, PackageTitle, UserName, 
                A.Credits, A.UserId, Profit, If(A.OrderAPIId = '' OR A.OrderAPIId = '0' OR A.OrderAPIId = '-1' , '-', '<b>API</b>') AS API, A.IP, CurrencyAbb, CodeStatus, TimeTaken,
                OrderCostPrice, LinkedOrderId, CodeSentToOtherServer, MessageFromServer, OrderAPIId, OrderType FROM tbl_gf_codes_slbf A, tbl_gf_packages B, tbl_gf_users D, 
                tbl_gf_currency C, tbl_gf_code_status E WHERE A.PackageId = B.PackageId AND A.UserId = D.UserId AND D.CurrencyId = C.CurrencyId AND AdminArchived = 0 AND 
                A.CodeStatusId = E.CodeStatusId $strWhere ORDER BY CodeId DESC LIMIT $eu, $limit");
        return $query->result();
    }

    public function fetch_codes_slbf_packages($strWhere){
        $query=$this->db->query("SELECT A.PackageId, PackageTitle, COUNT(A.CodeId) AS TotalOrders, APITitle FROM tbl_gf_codes_slbf A, tbl_gf_packages B LEFT JOIN tbl_gf_api C 
        ON (B.APIId = C.APIId) WHERE A.PackageId = B.PackageId AND AdminArchived = 0 AND CodeStatusId = 1 AND sl3lbf = 1 $strWhere 
        GROUP BY PackageId ORDER BY PackOrderBy, PackageTitle");

        return $query->result();
    }

    public function fetch_codes_slbf($strWhere){
        $query=$this->db->query("SELECT PackageId, MessageFromServer, RequestedAt, CronDelayTm FROM tbl_gf_codes_slbf WHERE CodeStatusId = 1 AND OrderAPIId > 0 
        AND CodeSentToOtherServer = '0' AND HiddenStatus = '1' $strWhere");

        return $query->result();
    }


    public function fetch_codes_slbf_packages_users_by_strwhere($strWhere ,$eu, $limit){
        $query=$this->db->query("SELECT A.PackageId, CodeId, A.Credits, Code, IMEINo, RequestedAt, PackageTitle, Verify, A.UserId, UserName, A.VerifyIP FROM tbl_gf_codes_slbf A, tbl_gf_packages B, tbl_gf_users D WHERE A.PackageId = B.PackageId AND A.UserId = D.UserId AND Verify = 1 $strWhere ORDER BY CodeId DESC LIMIT $eu, $limit");

        return $query->result();
        
    }


    public function fetch_codefile_from_codes_slbf($id){
       
        $this->db->select('CodeFile');
        $this->db->from('tbl_gf_codes_slbf');
        $this->db->where('CodeId' , $id);

        $result=$this->db->get();
        return $result->row();
    }

    public function fetch_custom_fields(){
      
        $this->db->select('FieldLabel, FieldColName');
        $this->db->from('tbl_gf_custom_fields');
        $this->db->where('ServiceType' , 2);
        $this->db->order_by('FieldLabel');

        $result=$this->db->get();
        return $result->result();

    }

    public function fetch_log_request_packages_users_status($strWhere , $strCustomCols ,$eu, $limit){
        $query=$this->db->query("SELECT A.*, LogPackageTitle, C.CodeStatus, D.UserName, If(A.OrderAPIId = '' OR A.OrderAPIId = '0' OR A.OrderAPIId = '-1', '-', 'API') AS API, A.IP, CodeSentToOtherServer, MessageFromServer, OrderAPIId, CurrencyAbb, Profit, OrderCostPrice, LinkedOrderId, DeliveryTime $strCustomCols FROM tbl_gf_log_requests A, 
                    tbl_gf_log_packages B, tbl_gf_code_status C, tbl_gf_users D, tbl_gf_currency E WHERE A.LogPackageId = B.LogPackageId AND A.UserId = D.UserId AND D.CurrencyId = E.CurrencyId AND A.StatusId = C.CodeStatusId $strWhere ORDER BY LogRequestId DESC LIMIT $eu, $limit");

        return $query->result();
    }


    public function fetch_distinct_log_package_category_request(){
       
        $this->db->distinct('A.CategoryId, Category, A.LogPackageId AS PackageId, LogPackageTitle, DisableLogPackage');
        $this->db->from('tbl_gf_log_package_category Cat, tbl_gf_log_packages A, tbl_gf_log_requests B');
        $this->db->where('Cat.CategoryId = A.CategoryId');
        $this->db->where('A.LogPackageId = B.LogPackageId');
        $this->db->where('Verify' , 1);
        $this->db->order_by('OrderBy, Category, PackOrderBy, LogPackageTitle');

        $result=$this->db->get();
        return $result->result();
    }

    public function fetch_log_package_category_request(){
        $this->db->select('A.CategoryId, Category, A.LogPackageId AS PackageId, LogPackageTitle, DisableLogPackage');
        $this->db->from('tbl_gf_log_package_category Cat, tbl_gf_log_packages A');
        $this->db->where('Cat.CategoryId = A.CategoryId');
        $this->db->order_by('OrderBy, Category, PackOrderBy, LogPackageTitle');

        $result=$this->db->get();
        return $result->result();


    }


    public function fetch_log_requests_packages_api($strWhere){
        $query=$this->db->query("SELECT A.LogPackageId, LogPackageTitle, COUNT(A.LogRequestId) AS TotalOrders, APITitle FROM tbl_gf_log_requests A, tbl_gf_log_packages B 
        LEFT JOIN tbl_gf_api C ON (B.APIId = C.APIId) WHERE A.LogPackageId = B.LogPackageId AND StatusId = 1 $strWhere 
        GROUP BY LogPackageId ORDER BY PackOrderBy, LogPackageTitle");

        return $query->result();
    }

    public function fetch_log_requests_by_strwhere($strWhere){
        $query=$this->db->query("SELECT LogPackageId, MessageFromServer, RequestedAt, CronDelayTm FROM tbl_gf_log_requests WHERE StatusId = 1 AND OrderAPIId > 0 AND CodeSentToOtherServer = '0' AND HiddenStatus = '1' $strWhere");

        return $query->result();
    }

    public function fetch_retail_payment_and_orders($colId ,$tblOrders ,$stColId ,$packColId){
       
        $this->db->select("$colId AS OrderId, MessageFromServer, OrderAPIName");
        $this->db->from("$tblOrders A, tbl_gf_retail_payments B");
        $this->db->where('A.PaymentId = D.RetailPaymentId');
        $this->db->where('PaymentStatus' , 2);
        $this->db->where("$stColId" , 1);
        $this->db->where('OrderAPIId >' , 0);
        $this->db->where('CodeSentToOtherServer' , 0);
        $this->db->where('HiddenStatus' , 1);
        $this->db->where("$packColId" , $MY_SERVICE_ID);

        $result=$this->db->get();
        return $result->result();
    }


    public function generel_reprocess_orders_query($colId ,$tblOrders ,$stColId , $packColId , $MY_SERVICE_ID){
    
        $this->db->select("$colId AS OrderId, MessageFromServer, OrderAPIName");
        $this->db->from("$tblOrders");
        $this->db->where('A.PaymentId = D.RetailPaymentId');
        $this->db->where("$stColId" , 1);
        $this->db->where('OrderAPIId >' , 0);
        $this->db->where('CodeSentToOtherServer' , 0);
        $this->db->where('HiddenStatus' , 1);
        $this->db->where("$packColId" , $MY_SERVICE_ID);

        $result=$this->db->get();
        return $result->result();
    }


    public function fetch_log_requests_packages_users($strWhere , $eu, $limit){
        $query=$this->db->query("SELECT A.*, LogPackageTitle, UserName FROM tbl_gf_log_requests A, tbl_gf_log_packages B, tbl_gf_users D WHERE A.LogPackageId = B.LogPackageId AND A.UserId = D.UserId AND Verify = 1 $strWhere ORDER BY LogRequestId DESC LIMIT $eu, $limit");

        return $query->result();
    }

    public function fetch_codes_packages_api($strWhere){
        $query=$this->db->query("SELECT A.PackageId, PackageTitle, COUNT(A.CodeId) AS TotalOrders, APITitle FROM tbl_gf_codes A, tbl_gf_packages B LEFT JOIN tbl_gf_api C 
        ON (B.APIId = C.APIId) WHERE A.PackageId = B.PackageId AND AdminArchived = 0 AND CodeStatusId = 4 AND sl3lbf = 0 $strWhere 
        GROUP BY PackageId ORDER BY PackageTitle");

        return $query->result();
    }


    public function fetch_codes_packages_users($strWhere ,$eu, $limit){
        $query=$this->db->query("SELECT A.PackageId, CodeId, A.Credits, IMEINo, RequestedAt, PackageTitle, UserName FROM tbl_gf_codes A, tbl_gf_packages B, tbl_gf_users D WHERE A.PackageId = B.PackageId 
        AND A.UserId = D.UserId AND OrderAPIId > 0 AND OrderIdFromServer <> '' AND CodeStatusId = '4' $strWhere ORDER BY CodeId DESC LIMIT $eu, $limit");

        return $query->result();


    }

    public function  update_code_custom($codeStatusId , $code , $notes , $comments , $historyData , $currDtTm , $strDeduct , $col_val , $id){
        $this->db->query("UPDATE tbl_gf_codes SET CodeStatusId = '$codeStatusId', Code = '$code', Comments = '$notes', Comments1 = '$comments', 
		OrderData = '".addslashes($historyData)."', ReplyDtTm = '$currDtTm', LastUpdatedBy = '".$this->session->userdata('AdminUserName')."' $strDeduct $col_val WHERE CodeId = '$id'");
    }

    public function fetch_codes_users_status($id){
      
        $this->db->select('UserName AS CustomerName, UserEmail, AlternateEmail, CodeStatus, RequestedAt, A.UserId, A.Comments, B.SendSMS AS SMS_User, B.Phone');
        $this->db->from('tbl_gf_codes A, tbl_gf_users B, tbl_gf_code_status C');
        $this->db->where('A.UserId = B.UserId');
        $this->db->where(' A.CodeStatusId = C.CodeStatusId');
        $this->db->where('CodeId' , $id);

        $result=$this->db->get();
        return $result->row();
        
    }

    public function users_codes_packages_api($id){
        $this->db->select('A.*, PackageTitle, UserName, B.SendSMS, CodeStatusId');
        $this->db->from('tbl_gf_users D, tbl_gf_codes A, tbl_gf_packages B');
        $this->db->join('tbl_gf_api C' , 'B.APIId = C.APIId' , 'left');
        $this->db->where('A.PackageId = B.PackageId');
        $this->db->where('A.UserId = D.UserId');
        $this->db->where('CodeId' , $id);

        $result=$this->db->get();
        return $result->row();
       
    }

    public function fetch_custom_fields_by_service_type(){
       
        $this->db->select('FieldId, FieldLabel, FieldType, Mandatory, FieldColName');
        $this->db->from('tbl_gf_custom_fields');
        $this->db->where('ServiceType' , 0);
        $this->db->where('DisableField' , 0);
        $this->db->order_by('FieldLabel');

        $result=$this->db->get();
        return $result->result();
        
    }


    public function fetch_api_data($apiId){
       
        $this->db->select('APIId, APIKey, APIType, ServerURL, AccountId, APITitle');
        $this->db->from('tbl_gf_api');
        $this->db->where('APIId' , $apiId);
        $this->db->where('DisableAPI' , 0);

        $result = $this->db->get();
        return $result->row();
    }


    public function pending_order_update_general_query($tblName , $apiId , $serverURL ,$apiUserName , $apiType , $apiKey , $supplierPackId , $apiName , $idCol , $selectedOrders){
    
        $this->db->set('OrderAPIId' , $apiId);
        $this->db->set('OrderAPIURL' ,$serverURL);
        $this->db->set('OrderAPIUserName' ,$apiUserName);
        $this->db->set('OrderAPIType' , $apiType);
        $this->db->set('OrderAPIKey' , $apiKey);
        $this->db->set('OrderAPIServiceId' , $supplierPackId);
        $this->db->set('OrderType' , 1);
        $this->db->set('OrderAPIName' , $apiName);
        $this->db->where_in($idCol , $selectedOrders);

        $this->db->update($tblName);
    }

    public function fetch_custom_fields_by_type($sc){
        $this->db->select('FieldLabel, FieldColName');
        $this->db->from('tbl_gf_custom_fields');
        $this->db->where('ServiceType' , $sc);
        $this->db->order_by('FieldLabel');

        $result = $this->db->get();
        return $result->result();
    }


    public function fetch_pending_orders_custom_data($serviceCol ,$idCol ,  $packCol ,$strCustomCols ,$tblName ,$tblPack, $statusCol , $strWhere , $eu , $limit){
    
        $query = $this->db->query("SELECT A.$serviceCol AS PackageId, $idCol AS CodeId, A.Credits, RequestedAt, $packCol AS PackageTitle, UserName $strCustomCols FROM $tblName A, 
        $tblPack B,	tbl_gf_users D WHERE A.$serviceCol = B.$serviceCol AND A.UserId = D.UserId AND $statusCol = '1' $strWhere ORDER BY CodeId DESC 
        LIMIT $eu, $limit");

        return $query->result();
    }
    public function fetch_custom_pending_tbl_users($idCol , $tblName , $statusCol , $strWhere){
        $query=$this->db->query("SELECT COUNT($idCol) AS TotalRecs FROM $tblName A, tbl_gf_users D WHERE A.UserId = D.UserId 
        AND OrderAPIId = 0 AND $statusCol = '1' $strWhere");

        return $query->row();
    }

    public function fetch_log_requests_packages_api_by_statusId($strWhere){
        $query=$this->db->query("SELECT A.LogPackageId, LogPackageTitle, COUNT(A.LogRequestId) AS TotalOrders, APITitle FROM tbl_gf_log_requests A, tbl_gf_log_packages B 
        LEFT JOIN tbl_gf_api C ON (B.APIId = C.APIId) WHERE A.LogPackageId = B.LogPackageId AND StatusId = 4 $strWhere 
        GROUP BY LogPackageId ORDER BY PackOrderBy, LogPackageTitle");

        return $query->result();
    }


    public function delete_data_user_packages($selectedPackages){
        $this->db->where_in('PackageId', $selectedPackages);
        $this->db->delete('tbl_gf_user_packages'); 

        
        
    }

    public function delete_user_packages_by_userids($selectedPackages , $strUserIds){
    
        $this->db->where_in('PackageId', $selectedPackages);
        $this->db->where_in('UserId', $UserId);
        $this->db->delete('tbl_gf_user_packages');

    }

    public function fetch_users(){
    
        $this->db->select('UserId');
        $this->db->from('tbl_gf_users');
        $this->db->where('UserArchived' , 0);
        $this->db->where('DisableUser' , 0);

        $result=$this->db->get();
        return $result->result();

    }


    public function fetch_users_by_archived($groupId){
      
        $this->db->select('UserId');
        $this->db->from('tbl_gf_users');
        $this->db->where('PricePlanId' , $groupId);
        $this->db->where('UserArchived' , 0);
        $this->db->where('DisableUser' , 0);
        $result=$this->db->get();
        return $result->result();
    }

    public function fetch_users_by_price_plan_id($groupId){
        $this->db->select('UserId');
        $this->db->from('tbl_gf_users');
        $this->db->where('PricePlanId' , $groupId);
      
        $result=$this->db->get();
        return $result->result();
    }


    public function insert_user_packages($strQry){
        $this->db->query("INSERT INTO tbl_gf_user_packages (UserId, PackageId, ServiceType) VALUES $strQry");
    }


    public function fetch_package_category_api($strWhere ,$eu, $limit){
        $query=$this->db->query("SELECT PackageId, PackageTitle, PackagePrice, If(A.APIId = '-1', '-', APITitle) AS API, 
        DisablePackage, Category FROM tbl_gf_package_category D, tbl_gf_packages A LEFT JOIN tbl_gf_api B ON (A.APIId = B.APIId) WHERE A.CategoryId = 
        D.CategoryId AND ArchivedPack = '0' $strWhere ORDER BY OrderBy, Category, PackOrderBy, PackageTitle LIMIT $eu, $limit");

        return $query->result();
    }


    public function del_service_api_pricing($id ,$PMAPIId , $fs){
        
        $this->db->where('ServiceId', $id);
        $this->db->where('ServiceAPIId', $PMAPIId);
        $this->db->where('ServiceType', $fs);
        $this->db->delete('tbl_gf_service_api_pricing');

    }

    public function fetch_pacakges($packageTitle , $fs , $id){
        
        $this->db->select('PackageId');
        $this->db->from('tbl_gf_packages');
        $this->db->where('PackageTitle' , $packageTitle);
        $this->db->where('sl3lbf' , $fs);
        $this->db->where('ArchivedPack' , 0);
        $this->db->where('DisablePackage' , 0);

        if($id > 0){
            $this->db->where('PackageId' , $id);
        }

        $result=$this->db->get();
        return $result->row();
        
    }

    public function insert_packages_data($insert_data){
        $this->db->insert('tbl_gf_packages',$insert_data);
        return $this->db->insert_id();
    }

    public function update_packages_general_query($packageTitle ,$price ,$extNetworkId ,$apiId ,$categoryId ,$fs ,$timeTaken ,$mustRead ,$metaKW ,$disablePackage , $dupIMEI ,$toolForUB ,$metaTags ,$seoName ,$htmlTitle ,$imeiFType ,$fName ,$redirect,$srvType ,$newTplType , $sucTplType ,$seriesId ,$canTplType ,$costPrice ,$customFldId ,$currDtTm ,$supplier , $calPreCodes , $sendSMS ,$responseDelayTm ,$cronDelayTm , $cancelOrders ,$verifyOrders ,$emailIDs ,$cancelMins , $verifyMins , $toc ,$costPrFrmAPI , $strTpls ,$id){
        $this->db->query("UPDATE tbl_gf_packages SET PackageTitle = '$packageTitle', PackagePrice = '$price', ExternalNetworkId = '$extNetworkId', APIId = '$apiId',
        CategoryId = '$categoryId', sl3lbf = '$fs', TimeTaken = '$timeTaken', MustRead = '$mustRead',	MetaKW = '$metaKW', DisablePackage = $disablePackage, 
        DuplicateIMEIsNotAllowed = '$dupIMEI', ToolForUnlockBase = '$toolForUB', MetaTags = '$metaTags', SEOURLName = '$seoName', HTMLTitle = '$htmlTitle', 
        IMEIFieldType = '$imeiFType', FileName = '$fName', RedirectionURL = '$redirect', ServiceType = '$srvType', NewTplType= '$newTplType', 
        SuccessTplType = '$sucTplType', SeriesId = '$seriesId', CancelledTplType = '$canTplType', CostPrice = '$costPrice', CustomFieldId = '$customFldId', 
        EditedAt = '$currDtTm', Supplier = '$supplier', CalculatePreCodes = '$calPreCodes', SendSMS = '$sendSMS', ResponseDelayTm = '$responseDelayTm', 
        CronDelayTm = '$cronDelayTm', CancelOrders = '$cancelOrders', VerifyOrders = '$verifyOrders', NewOrderEmailIDs = '$emailIDs', OrderCancelMins = '$cancelMins', 
        OrderVerifyMins = '$verifyMins', TOCs = '$toc', CostPriceFromAPI = '$costPrFrmAPI' $strTpls WHERE PackageId = '$id'");
    }


    public function del_packages_currencies($id){
     
        $this->db->where('PackageId', $id);
        $this->db->delete('tbl_gf_packages_currencies');
    }


    public function insert_packages_currencies($strCurrencies){
        $this->db->query("INSERT INTO tbl_gf_packages_currencies (PackageId, CurrencyId, Price) VALUES ".$strCurrencies);

    }

    public function fetch_users_packages_prices($id){
        $query=$this->db->query("SELECT DISTINCT UserId FROM tbl_gf_users WHERE UserId NOT IN (SELECT UserId FROM tbl_gf_users WHERE PricePlanId > '0' 
                                        UNION SELECT UserId FROM tbl_gf_users_packages_prices WHERE PackageId = '$id')");
        return $query->result();                   
    }

    public function del_default_prices_notice($fs , $id){
        $this->db->where('ServiceType', $fs);
        $this->db->where('PackageId' , $id);
        $this->db->delete('tbl_gf_default_prices_notice');

    }

    public function insert_prices_notice($strDefPriceNoticeNew){
        $query=$this->db->query("INSERT INTO tbl_gf_default_prices_notice (CurrencyId, PackageId, PackTitle, Price, ServiceType, UserId) VALUES $strDefPriceNoticeNew");
       
        
    }

    public function del_package_custom_fields($id , $fs){
        $this->db->where('PackageId' , $id);
        $this->db->where('ServiceType', $fs);
        $this->db->delete('tbl_gf_package_custom_fields');
    }


    public function insert_package_custom_fields($strData){
        $this->db->query("INSERT INTO tbl_gf_package_custom_fields (PackageId, FieldId, ServiceType) VALUES $strData");
    }

    public function del_plans_packages_prices($id , $fs){
        $this->db->where('PackageId' , $id);
        $this->db->where('ServiceType', $fs);
        $this->db->delete('tbl_gf_plans_packages_prices');
       
    }

    public function insert_plans_packages_prices($str){
        $this->db->query("INSERT INTO tbl_gf_plans_packages_prices (PackageId, CurrencyId, PlanId, Price, ServiceType) VALUES ".$str);
    }

    public function del_plans_prices_email($planId ,$fs ,$id){
 
        $this->db->where('PlanId' , $planId);
        $this->db->where('ServiceType', $fs);
        $this->db->where('PackageId', $id);
        $this->db->delete('tbl_gf_plans_prices_email');
        
    }

    public function insert_plans_prices_email($strPrices){
        $this->db->query("INSERT INTO tbl_gf_plans_prices_email (PlanId, CurrencyId, PackageId, PackTitle, Price, ServiceType) VALUES $strPrices");

    }

    public function fetch_userIds(){
        $this->db->select('UserId');
        $this->db->from('tbl_gf_users');
        $this->db->where('PricePlanId >' , 0);
        $result = $this->db->get();
        return $result->result();
    }

    public function del_group_prices_notice($fs ,$id){
        $this->db->where('ServiceType', $fs);
        $this->db->where('PackageId' , $id);
        $this->db->delete('tbl_gf_group_prices_notice');
    }


    public function insert_group_prices_notice($strPriceNoticeNew){
        $this->db->query("INSERT INTO tbl_gf_group_prices_notice (PlanId,CurrencyId,PackageId,PackTitle,Price,ServiceType,UserId) VALUES $strPriceNoticeNew");
    }


    public function del_users_packages_prices($id){
     
        $this->db->where('PackageId' , $id);
        $this->db->delete('tbl_gf_group_prices_notice');
    }

    public function del_precodes($id ,$fs){
       
        $this->db->where('ServiceId' , $id);
        $this->db->where('ServiceType' , $fs);
        $this->db->delete('tbl_gf_precodes');
    }

    public function insert_precodes($strPreCodes) {
        $this->db->query("INSERT INTO tbl_gf_precodes (ServiceId, PreCode, ServiceType) VALUES ".$strPreCodes);
    }


    public function del_packs_models($id ,$apiIdForBrand ,$fs){

        $this->db->where('ServiceId' , $id);
        $this->db->where('APIId' , $apiIdForBrand);
        $this->db->where('ServiceType' , $fs);
        $this->db->delete('tbl_gf_packs_models');
    }


    public function insert_packs_models($strBrandsQry){
        $this->db->query("INSERT INTO tbl_gf_packs_models (ServiceId, BrandId, ModelId, APIId, ServiceType) VALUES $strBrandsQry");
    }

    public function fetch_packages_api($id){
        
        $this->db->select(' A.*, SendExternalId, APIType ');
        $this->db->from('tbl_gf_packages A ');
        $this->db->join('tbl_gf_api B' , 'A.APIId = B.APIId' , 'left');
        $this->db->where('PackageId' , $id);

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_precodes($id ,$fs){
        
        $this->db->select('PreCode');
        $this->db->from('tbl_gf_precodes');
        $this->db->where('ServiceId' , $id);
        $this->db->where('ServiceType' , $fs);

        $result = $this->db->get();
        return $result->result();

    }

    public function fetch_packs_models($id ,$fs){
        
        $this->db->select('APIId');
        $this->db->from('tbl_gf_packs_models');
        $this->db->where('ServiceId' , $id);
        $this->db->where('ServiceType' , $fs);

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_packages_currencies($id){
        $query = $this->db->query("SELECT A.CurrencyId, CurrencyAbb, ConversionRate, Price, DefaultCurrency FROM tbl_gf_currency A LEFT JOIN 
        tbl_gf_packages_currencies B ON (A.CurrencyId = B.CurrencyId AND PackageId = '$id') WHERE DisableCurrency = 0 ORDER BY DefaultCurrency DESC");

        return $query->result();
       
    }

    public function del_pack_selected_features($id ,$srvcType){
        
        $this->db->where('ServiceId' , $id);
        $this->db->where('ServiceType' , $srvcType);
        $this->db->delete('tbl_gf_pack_selected_features');
    }


    public function insert_pack_selected_features($strData){
        $this->db->query("INSERT INTO tbl_gf_pack_selected_features (ServiceId, FeatureId, ServiceType) VALUES $strData");
    }

    public function fetch_service_features($id ,$srvcType){
        $result = $this->db->query("SELECT A.FeatureId, Feature, IF(B.FeatureId IS NULL, '0', '1') AS Checked FROM tbl_gf_service_features A LEFT JOIN tbl_gf_pack_selected_features B 
        ON (A.FeatureId = B.FeatureId AND ServiceId = '$id' AND ServiceType = '$srvcType')");
        return $result->result();
    
    }


    public function del_alternate_apis($altId , $id , $fs){
    
        $this->db->where('AltAPISrvcId' , $altId);
        $this->db->where('AltServiceId' , $id);
        $this->db->where('AltServiceType' , $fs);
        $this->db->delete('tbl_gf_alternate_apis');

    }

    public function fetch_alternate_apis_upplier_services($id ,$fs){
       
        $this->db->select('AltAPISrvcId, AltAPIId, AltAPIServiceId, APITitle, ServiceName, ServicePrice');
        $this->db->from('tbl_gf_alternate_apis A, tbl_gf_supplier_services B, tbl_gf_api C');
        $this->db->where('A.AltAPIId = B.APIId');
        $this->db->where('B.APIId = C.APIId');
        $this->db->where('A.AltAPIServiceId = B.ServiceId');
        $this->db->where('AltServiceId' , $id);
        $this->db->where('A.AltServiceType' , $fs);
        $this->db->where('B.ServiceType' , $fs);
        $this->db->order_by('AltAPISortBy');

        $result = $this->db->get();
        return $result->result();

    }

    public function fetch_srv_history_supplier_services($id ,$fs){
       
        $this->db->select('A.APIId, A.APIServiceId, APITitle, ServiceName, ServicePrice');
        $this->db->from('tbl_gf_api_srv_history A, tbl_gf_supplier_services B, tbl_gf_api C');
        $this->db->where('A.APIId = B.APIId');
        $this->db->where('B.APIId = C.APIId');
        $this->db->where('A.APIServiceId = B.ServiceId');
        $this->db->where('PackageId' , $id);
        $this->db->where('A.ServiceType' , $fs);
        $this->db->where('B.ServiceType' , $fs);
        $this->db->order_by('A.Id' , 'DESC');

        $result = $this->db->get();
        return $result->result();
    }

    public function fetch_packages_by_id($packageId){
   
        $this->db->select('PackagePrice');
        $this->db->from('tbl_gf_packages');
        $this->db->where('PackageId' , $packageId);

        $result = $this->db->get();
        return $result->row();
    }

    public function update_users_packages_prices($price ,$packageId){
      
        $this->db->set('Price' ,$price);
        $this->db->where('PackageId' , $packageId);

        $this->db->update('tbl_gf_users_packages_prices');
    }

    public function update_plans_packages_prices($price ,$packageId){
        
        $this->db->set('Price' ,$price);
        $this->db->where('PackageId' , $packageId);

        $this->db->update('tbl_gf_plans_packages_prices');
    }

    public function fetch_packs_models_by_api_id($id ,$hdAPIIdForBrand , $serviceType){
        
        $this->db->distinct('BrandId');
        $this->db->from('tbl_gf_packs_models');
        $this->db->where('ServiceId', $id);
        $this->db->where('APIId', $hdAPIIdForBrand);
        $this->db->where('ServiceType', $serviceType);

        $result=$this->db->get();
        return $result->result();
    }

    public function fetch_api_brands($apiId){

        $this->db->distinct('BrandId AS Id, Brand AS Value');
        $this->db->from('tbl_gf_api_brands');
        $this->db->where('APIId', $hdAPIIdForBrand);
        $this->db->order_by('Brand');
        $result=$this->db->get();
        return $result->result();
    }

    public function fetch_packs_models_by_concat($id ,$apiId , $brandIds ,$serviceType){

        $this->db->select("CONCAT(BrandId, ',', ModelId) AS ModelId");
        $this->db->from('tbl_gf_packs_models');
        $this->db->where('ServiceId', $id);
        $this->db->where('APIId', $apiId);
        $this->db->where_in('BrandId', $brandIds);
        $this->db->where('ServiceType', $serviceType);

        $result=$this->db->get();
        return $result->result();
       
    }

    public function fetch_api_models_brands($apiId ,$brandIds){
        
        $this->db->select("CONCAT(A.BrandId, ',', ModelId) AS ModelId, CONCAT(Brand, '-', Model) AS Model");
        $this->db->from('tbl_gf_api_models A, tbl_gf_api_brands B');
        $this->db->where('A.BrandId = B.BrandId');
        $this->db->where('A.APIId = B.APIId');
        $this->db->where('A.APIId', $apiId);
        $this->db->where_in('B.BrandId', $brandIds);
        $this->db->order_by('Model');

        $result=$this->db->get();
        return $result->result();


        
    }

    public function fetch_tool_mobiles($apiId , $serviceId){
        
         $this->db->select("MobileId AS Id, MobileName AS Value");
         $this->db->from('tbl_gf_tool_mobiles');
         $this->db->where('APIId', $apiId);
         $this->db->where_in('ServiceId', $serviceId);
         $this->db->order_by('MobileName');
 
         $result=$this->db->get();
         return $result->result();

    }


    public function fetch_currency_users_price_plans(){
      
        
        $this->db->select("UserId, UserName, CONCAT(A.FirstName, ' ', A.LastName) AS Name, CurrencySymbol, PricePlan, A.CurrencyId, A.PricePlanId");
        $this->db->from('tbl_gf_currency B, tbl_gf_users A');
        $this->db->join('tbl_gf_price_plans C', 'A.PricePlanId = C.PricePlanId' , 'left');
        $this->db->where('A.CurrencyId = B.CurrencyId');
        $this->db->where('DisableUser', 0);
        $this->db->where('UserArchived', 0);
        $this->db->order_by('UserName, FirstName, LastName');

        $result=$this->db->get();
        return $result->result();

    }

    public function fetch_plans_packages_prices($packageId){
        
        $this->db->select('PlanId, Price, CurrencyId');
        $this->db->from('tbl_gf_plans_packages_prices');
        $this->db->where('PackageId' , $packageId);
        $this->db->where('ServiceType' , 0);

        $result=$this->db->get();
        return  $result->result();
    }

    public function fetch_users_packages_prices_by_package_id($packageId){
       
        $this->db->select(' UserId, Price');
        $this->db->from('tbl_gf_users_packages_prices');
        $this->db->where('PackageId' , $packageId);
       
        $result=$this->db->get();
        return $result->result();
    }

    public function custom_query_packages($col1, $col2, $col3, $col5 ,$tbl , $col4 ,$id){
        

        $this->db->select($col1, $col2, $col3, $col5);
        $this->db->from($tbl);
        $this->db->where($col4 , $id);
       
        $result=$this->db->get();
        return $result->row();
        
    }

    public function custom_users_packages_prices($tbl , $idCol ,$serviceId){
       
        $this->db->select('UserId, Price');
        $this->db->from($tbl);
        $this->db->where($idCol , $serviceId);
       
        $result=$this->db->get();
        return $result->result();
    }

    public function fetch_plans_packages_prices_by_package_id($sc , $serviceId){
        

        $this->db->select('PlanId, Price, CurrencyId');
        $this->db->from('tbl_gf_plans_packages_prices');
        $this->db->where('ServiceType' , $sc);
        $this->db->where('PackageId' , $serviceId);
       
        $result=$this->db->get();
        return $result->result();
    }

    public function fetch_users_currency($strAND){
        $result = $this->db->query("SELECT UserId, CONCAT(FirstName, ' ', LastName) AS ClientName, UserEmail, A.CurrencyId, CurrencyAbb, ConversionRate, PricePlanId 
        FROM tbl_gf_users A, tbl_gf_currency B WHERE A.CurrencyId = B.CurrencyId AND DisableUser = 0 AND UserArchived = 0 $strAND");

        return $result->result();
    }

    public function del_service_price_email($id ,$sc){
        
        $this->db->where('PackageId' , $id);
        $this->db->where('ServiceType' , $sc);

        $this->db->delete('tbl_gf_service_price_email');
    }

    public function insert_service_price_email($strPrices){
        $this->db->query("INSERT INTO tbl_gf_service_price_email (PackageId, ClientName, ClientEmail, Price, ServiceType, UserNotes, ServiceName, DeliveryTime, Description, Features) VALUES $strPrices");
    }

    public function fetch_service_service_types($apiId ,$serviceId , $serviceType){
       
        $this->db->distinct('ServiceTypeId AS Id, ServiceTypeName, ServiceTypePrice');
        $this->db->from('tbl_gf_service_service_types');
        $this->db->where('APIId', $apiId);
        $this->db->where('ServiceType', $serviceType);
        $this->db->where('ServiceId', $serviceId);
        $this->db->order_by('ServiceTypeName');
        
        $result=$this->db->get();
        return $result->result();
    }

    public function update_custom_services_quickedit($tblPckName , $colTitle ,$pack ,$colTime , $time ,$currDtTm ,$col1 ,$col2 ,$col3 , $colId , $packId){
        $this->db->query("UPDATE $tblPckName SET $colTitle = '$pack', $colTime = '$time', EditedAt = '$currDtTm' $col1 $col2 $col3 WHERE $colId ='$packId'");
        
    }

    public function del_plans_packages_prices_by_package_ids($sc , $strPackIds){
      
        $this->db->where('ServiceType' , $sc);
        $this->db->where_in('PackageId' , $strPackIds);

        $this->db->update('tbl_gf_plans_packages_prices');
    }

    public function fetch_custom_data($colId, $colTitle, $colPrice, $disableCol, $colTime,$tblPckName , $strWhereSrv, $strWhere  , $eu, $limit){
        $query=$this->db->query("SELECT $colId, $colTitle, $colPrice, $disableCol, CostPrice, $colTime, CronDelayTm FROM $tblPckName WHERE $disableCol = 0 AND ArchivedPack = '0' 
            $strWhereSrv $strWhere ORDER BY PackOrderBy, $colTitle LIMIT $eu, $limit");
        return $query->result();
    }

    public function fetch_category_data($tblName , $strWhere){
        $query=$this->db->query("SELECT CategoryId, Category, DisableCategory FROM $tblName WHERE ArchivedCategory = '0' $strWhere ORDER BY OrderBy");
        return $query->result();
    }


    public function update_sortable_table($tblName , $position , $item , $strWhere){
        $this->db->query("UPDATE $tblName SET OrderBy = '$position' WHERE CategoryId = '$item' $strWhere");
    }

    public function update_payment_methods($position , $item){
       
        $this->db->set('OrderBy' , $position);
        $this->db->where('PaymentMethodId' , $item);
        $this->db->update('tbl_gf_payment_methods');
    }

    public function update_models($position ,$item){
     
        $this->db->set('OrderBy' , $position);
        $this->db->where('ModelId' , $item);
        $this->db->update('tbl_gf_models');
    }

    public function update_make_country($position , $item){
      
        $this->db->set('OrderBy' , $position);
        $this->db->where('CountryId' , $item);
        $this->db->update('tbl_gf_make_country');
    }

    public function update_retail_services($position , $item){
        $this->db->set('OrderBy' , $position);
        $this->db->where('PackageId' , $item);
        $this->db->update('tbl_gf_retail_services');
    }
    public function update_category($position , $item){
      
        $this->db->set('OrderBy' , $position);
        $this->db->where('CategoryId' , $item);
        $this->db->update('tbl_gf_category');
    }

    public function update_carriers($position , $item){
    
        $this->db->set('OrderBy' , $position);
        $this->db->where('CarrierId' , $item);
        $this->db->update('tbl_gf_carriers');
    }

    public function update_products($position , $item){ 
        $this->db->set('POrderBy' , $position);
        $this->db->where('ProductId' , $item);
        $this->db->update('tbl_gf_products');
    }

    public function update_eheader_cats($position , $item){ 
        $this->db->set('EOrderBy' , $position);
        $this->db->where('ECategoryId' , $item);
        $this->db->update('tbl_gf_eheader_cats');
    }


    public function update_custom_fields($position , $item , $sc){
      
        $this->db->set('FieldOrderBy' , $position);
        $this->db->where('FieldId' , $item);
        $this->db->where('ServiceType' , $sc);
        $this->db->update('tbl_gf_custom_fields');
    }

    public function update_retail_services_packages($tblPckName , $sortCol , $position , $colId , $item){
        $this->db->set($sortCol , $position);
        $this->db->where($colId , $item);
        $this->db->update($tblPckName);
    }

    public function update_alternate_apis($position , $item , $id , $sc){
        
        $this->db->set('AltAPISortBy' , $position);
        $this->db->where('AltAPISrvcId' , $item);
        $this->db->where('AltServiceId' , $id);
        $this->db->where('AltServiceType' , $sc);
        $this->db->update('tbl_gf_alternate_apis');
       
    }

    public function update_banner($position , $item , $type){
       
        $this->db->set('OrderBy' , $position);
        $this->db->where('BannerId' , $item);
        $this->db->where('BannerType' , $type);
        $this->db->update('tbl_gf_alternate_apis');

    }


    public function fetch_category_home_ecategory($tblName , $category ,$id){
      
        $this->db->select('CategoryId');
        $this->db->from($tblName);
        $this->db->where('Category' , $category);
        if($id > 0){
            $this->db->where('CategoryId !=' , $id);
        }

        $result = $this->db->get();
        return $result->row();
    }

    public function insert_category_home_ecategory($insert_array , $tblName){
        $this->db->insert($tblName , $insert_array);
    }

    public function update_category_home_ecategory($tblName , $id , $update_array){
        $this->db->where('CategoryId' , $id);
        $this->db->update($tblName ,  $update_array);
    }

    public function fetch_category_home_ecategory_by_id($tblName ,$id){
       
        $this->db->select('*');
        $this->db->from($tblName);
        $this->db->where('CategoryId' , $id);

        $result = $this->db->get();
        return $result->row();


    }

    public function update_pricing($tbl , $col , $setClause , $where , $strCategory){
        $this->db->query("UPDATE $tbl SET $col = $setClause $where $strCategory");
    }


    public function del_packages_currencies_and_packages($strCurrencies , $srvc , $strCategory){
        $this->db->query("DELETE FROM tbl_gf_packages_currencies WHERE CurrencyId IN ($strCurrencies) AND PackageId IN (SELECT PackageId FROM tbl_gf_packages 
        WHERE sl3lbf = '$srvc' $strCategory)");
    }

    public function del_log_packages_currencies($strCurrencies , $strLogCategory){
        $this->db->query("DELETE FROM tbl_gf_log_packages_currencies WHERE CurrencyId IN ($strCurrencies) $strLogCategory");
    }

    public function del_plans_packages_prices_and_packages($strCurrencies , $srvc , $strPlanCategory){
        $this->db->query("DELETE FROM tbl_gf_plans_packages_prices WHERE CurrencyId IN ($strCurrencies) AND ServiceType = '$srvc' $strPlanCategory");
    }

    public function fetch_users_by_currencyId($strCurrencies){
        $query=$this->db->query("SELECT UserId FROM tbl_gf_users WHERE CurrencyId IN ($strCurrencies)");

        return $query->result();
    }

    public function del_users_packages_prices_by_userid($strUserIds , $strUserCategory){
        $this->db->query("DELETE FROM tbl_gf_users_packages_prices WHERE UserId IN ($strUserIds) $strUserCategory");
    }

    public function del_users_log_packages_prices_by_userid($strUserIds , $strUserCategory){
        $this->db->query("DELETE FROM tbl_gf_users_log_packages_prices WHERE UserId IN ($strUserIds) $strUserCategory");
    }

    public function fetch_log_packages_by_slbf($colId , $col , $tbl , $where , $colD ){
        $query = $this->db->query("SELECT $colId AS PackId, $col AS PackPrice FROM $tbl $where AND $colD = '0' AND ArchivedPack = '0'");
        return $query->result();
    }

    public function del_plans_packages_prices_by_planId($groupId , $srvc){
        $this->db->query("DELETE FROM tbl_gf_plans_packages_prices WHERE PlanId = '$groupId' AND ServiceType = '$srvc'");
    }

    public function insert_plans_packages_prices2($str){
        $this->db->query("INSERT INTO tbl_gf_plans_packages_prices (PlanId, CurrencyId, PackageId, Price, ServiceType) VALUES $str");
    }


    public function fetch_custom_fields_by_servicetype($serviceType){
       
        $this->db->select(' FieldId, FieldLabel, FieldType, DisableField, Mandatory, SystemField');
        $this->db->from('tbl_gf_custom_fields');
        $this->db->where('ServiceType' , $serviceType);
        $this->db->order_by('FieldOrderBy');

        $result = $this->db->get();
        return $result->result();
    }

    public function fetch_custom_fields_by_fieldid($id){
      
        $this->db->select('*');
        $this->db->from('tbl_gf_custom_fields');
        $this->db->where('FieldId' , $id);

        $result = $this->db->get();
        return $result->row();


    }

    public function fetch_custom_fields_by_fieldlabel($fieldLbl , $id , $srvType){
        
        $this->db->select("Count(FieldId) AS TotalRecs");
        $this->db->from('tbl_gf_custom_fields');
        $this->db->where('FieldLabel' ,$fieldLbl );
        $this->db->where('ServiceType' , $srvType);
        if($id > 0){
            $this->db->where('FieldId !=' , $id);
        }

        $result = $this->db->get();
        return $result->row();
    }

    public function insert_custom_fields($insert_data){
        
        $this->db->insert('tbl_gf_custom_fields' , $insert_data);
    }

    
    public function alter_table_add_col($tbl , $colName){
        $this->db->query("ALTER TABLE $tbl ADD $colName VARCHAR( 255 ) NULL");
    }

    public function alter_table_text_col($tbl , $colName){
        $this->db->query("ALTER TABLE $tbl ADD $colName TEXT NULL");
    }

    public function update_custom_fields_by_fieldid($update_data , $id){
        
        $this->db->where('FieldId' , $id);
        $this->db->update('tbl_gf_custom_fields' , $update_data);
    }

    public function fetch_custom_field_values($id){
       
        $this->db->select("RegValueId, FieldId, RegValue, DisableRegValue");
        $this->db->from('tbl_gf_custom_field_values' );
        $this->db->where('FieldId' , $id);
        $this->db->order_by('RegValue');
        

        $result = $this->db->get();
        return $result->result();
    }

    public function fetch_custom_field_values_by_reg_id($id){
        
        $this->db->select('*');
        $this->db->from('tbl_gf_custom_field_values');
        $this->db->where('RegValueId' , $id);

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_regvalueid_count($val , $fId , $id){
      
        $this->db->select("Count(RegValueId) AS TotalRecs");
        $this->db->from('tbl_gf_custom_field_values');
        $this->db->where('RegValue' , $val);
        $this->db->where('FieldId' , $fId);
        
        if($id > 0 ){
            $this->db->where('RegValueId !=', $id);
        }

        $result = $this->db->get();
        return $result->row();

    }

    public function insert_custom_fields_values($insert_data){
        $this->db->insert('tbl_gf_custom_field_values' , $insert_data);
    }


    public function update_custom_fields_by_regvalueid($update_data , $id){
      
        $this->db->where('RegValueId' , $id);
        $this->db->update('tbl_gf_custom_field_values' , $update_data);
    }


    public function smsajax_custom_query($tbl , $val , $strWhere){
        $this->db->query("UPDATE $tbl SET SendSMS = '$val' $strWhere");
    }

    public function fetch_retail_services_packages($colId ,  $colTitle , $tblPckName , $disableCol , $strWhere , $categoryId , $sortCol){
        $query = $this->db->query("SELECT $colId AS Id, $colTitle AS Value FROM $tblPckName WHERE $disableCol = 0 $strWhere AND CategoryId = '$categoryId' ORDER BY $sortCol, $colTitle");
        return $query->result();
    }


    public function update_codes_slbf_requests($tbl , $colId , $orderNo){
        $this->db->set('Profit' , 0);
        $this->db->where($colId , $orderNo);
        $this->db->update($tbl);
    }

    public function del_users_packages_prices_log($tblUserPr , $strWhere){
        $this->db->query("DELETE FROM $tblUserPr $strWhere");
    }
    
    public function del_plans_packages_prices_by_servicetype($srvc , $strWhere1){
        $this->db->query("DELETE FROM tbl_gf_plans_packages_prices WHERE ServiceType = '$srvc' $strWhere1");
    }

    public function update_log_packages($tblPckName , $categoryId){
       
        $this->db->set('HideServiceAtWeb' , 0);
        $this->db->where('CategoryId' , $categoryId);
        $this->db->update($tblPckName);
    }  
    
    public function update_log_packages_by_id($tblPckName , $colId , $selectedPackages){
        $this->db->set('HideServiceAtWeb' , 0);
        $this->db->where_in($colId , $selectedPackages);
        $this->db->update($tblPckName);

    }

    public function fetch_log_package_category($colId , $colTitle , $tblName , $tblPckName , $disableCol , $categoryId , $strWhere){
        $query = $this->db->query("SELECT $colId AS PackageId, $colTitle AS PackageTitle, HideServiceAtWeb, Category FROM $tblName D, $tblPckName A WHERE A.CategoryId = D.CategoryId 
        AND $disableCol = 0 AND ArchivedPack = '0' AND A.CategoryId = '$categoryId' $strWhere ORDER BY OrderBy, Category, PackOrderBy, $colTitle");

        return $query->result();
    }

    public function update_log_packages_by_archicvedpack($tblPckName ,$colId, $selectedPackages){
        
        $this->db->set('ArchivedPack' , 0);
        $this->db->where_in($colId , $selectedPackages);
        $this->db->update($tblPckName);
    }


    public function fetch_log_packages_category_by_cat_id($colId , $colTitle, $tblName , $tblPckName , $strWhere){
        $query = $this->db->query("SELECT $colId AS PackageId, $colTitle AS PackageTitle, Category FROM $tblName D, $tblPckName A WHERE A.CategoryId = D.CategoryId AND ArchivedPack = '1' $strWhere ORDER BY OrderBy, Category, PackOrderBy, $colTitle");
        return $query->result();
    }

    public function updateLogPack($packId){
        $this->db->set('ArchivedPack' , 1);
        $this->db->where('LogPackageId' , $packId);
        $this->db->update('tbl_gf_log_packages');

    }

    public function update_log_packages_by_currpackIds($currDtTm , $currpackIds){
        $this->db->set('DisableLogPackage' , 0);
        $this->db->set('EditedAt' , $currDtTm);
        $this->db->where_in('LogPackageId' , $currpackIds);
        $this->db->update('tbl_gf_log_packages');
    }

    public function update_log_packages_by_selectedPackages($currDtTm , $selectedPackages){
       
        $this->db->set('DisableLogPackage' , 1);
        $this->db->set('EditedAt' , $currDtTm);
        $this->db->where_in('LogPackageId' , $selectedPackages);
        $this->db->update('tbl_gf_log_packages');
    }

    public function update_log_packages_by_key($packagePrice ,$currDtTm , $key){
        
        $this->db->set('LogPackagePrice' , $packagePrice);
        $this->db->set('EditedAt' ,$currDtTm);
        $this->db->where('LogPackageId' , $key);
        $this->db->update('tbl_gf_log_packages');
    }

    public function fetch_log_package_category_and_api($strWhere , $eu, $limit){
        $query=$this->db->query("SELECT LogPackageId AS PackageId, LogPackageTitle AS PackageTitle, LogPackagePrice AS PackagePrice, If(A.APIId = '-1', '-', APITitle) AS API, 
					DisableLogPackage AS DisablePackage, Category FROM tbl_gf_log_package_category D, tbl_gf_log_packages A LEFT JOIN tbl_gf_api B ON (A.APIId = B.APIId) 
					WHERE A.CategoryId = D.CategoryId AND ArchivedPack = '0' $strWhere ORDER BY OrderBy, Category, PackOrderBy, LogPackageTitle LIMIT $eu, $limit");
        return $query->result();
    }


    public function fetch_precodes_by_type(){
      
        $this->db->select("ServiceId, COUNT(PreCodeId) AS TotalPCs");
        $this->db->from('tbl_gf_precodes');
        $this->db->where('ServiceType' , 2);
        $this->db->where('Assigned' , 0);

        $result = $this->db->get();
        return $result->result();

    }

    public function export_services_custom_query($ids ,$idCol, $packCol, $priceCol, $timeCol, $detCol , $tblCat , $tblSrv , $disCol , $strWhere){
    
        $this->db->select("$idCol, $packCol, $priceCol, Category, $timeCol, $detCol");
        $this->db->from("$tblCat D, $tblSrv A");
        $this->db->where('A.CategoryId = D.CategoryId');
        $this->db->where('ArchivedPack' , 0);
        $this->db->where("$disCol" , 0);
        $this->db->where('DisableCategory' , 0);
        if($ids != '0'){
            $this->db->where_in("$idCol" , $ids);
        }

        $result = $this->db->get();
        return $result->result();
    }


    public function fetch_log_packages_by_title($packageTitle , $id ){
        
        $this->db->select('LogPackageId');
        $this->db->from('tbl_gf_log_packages');
        $this->db->where('LogPackageTitle' , $packageTitle);
        $this->db->where('DisableLogPackage' , 0);
        $this->db->where('ArchivedPack' , 0);

        if($id > 0){
            $this->db->where('LogPackageId !=' , $id);
        }

        $result = $this->db->get();
        return $result->result();


    }

    public function replicateSrvrService($serviceId, $newTitle)
    {
        $query=$this->db->query("INSERT INTO tbl_gf_log_packages (LogPackageTitle, CategoryId, LogPackageDetail, LogPackagePrice, DisableLogPackage, DeliveryTime, CronDelayTm,
                            CostPrice, EditedAt, Supplier, CalculatePreCodes, SendSMS, PricePerQuantity, ServiceType, CancelOrders, VerifyOrders, OrderVerifyMins, RedirectionURL, 
                            NewOrderEmailIDs) SELECT '$newTitle' AS LogPackageTitle, CategoryId, LogPackageDetail, LogPackagePrice, DisableLogPackage, DeliveryTime, CronDelayTm,
                            CostPrice, EditedAt, Supplier, CalculatePreCodes, SendSMS, PricePerQuantity, ServiceType, CancelOrders, VerifyOrders, OrderVerifyMins, RedirectionURL, 
                            NewOrderEmailIDs FROM tbl_gf_log_packages WHERE LogPackageId = '$serviceId'");
        
        return $this->db->insert_id();
    }

    public function insert_log_packages($insert_data){
        $this->db->insert($insert_data , tbl_gf_log_packages);

        return $this->db->insert_id();
    } 

    public function update_log_packages_by_logpackageid($update_data , $id){
        
        $this->db->where('LogPackageId' , $id);
        $this->db->update('tbl_gf_log_packages' , $update_data);

    }

    public function del_users_log_packages_prices($id){
        $this->db->where('LogPackageId' , $id);
        $this->db->delete('tbl_gf_users_log_packages_prices');
    }


    public function del_precodes_by_ids($strPCIds){
        
        $this->db->where_in('PreCodeId' , $strPCIds);
        $this->db->update('tbl_gf_precodes');
    }

    public function update_precode($currDt ,$strPCIds){
        
        $this->db->set('Assigned' , 1);
        $this->db->set('AssignedDtTm' ,$currDt);
        $this->db->where_in('PreCodeId' , $strPCIds);
        $this->db->update('tbl_gf_precodes');
    }


    public function del_log_packages_currencies_by_pack_id($id){
        $this->db->where('PackageId' , $id);
        $this->db->delete('tbl_gf_log_packages_currencies');
    }


    public function insert_log_packages_currencies($strCurrencies){
        $this->db->query("INSERT INTO tbl_gf_log_packages_currencies (PackageId, CurrencyId, Price) VALUES ".$strCurrencies);

    }

    public function fetch_distinct_users_prices($id){
        $query=$this->db->query("SELECT DISTINCT UserId FROM tbl_gf_users WHERE UserId NOT IN (SELECT UserId FROM tbl_gf_users WHERE PricePlanId > '0' UNION SELECT UserId FROM tbl_gf_users_log_packages_prices WHERE LogPackageId = '$id')");
        return $query->result();
    }

    public function del_prices_notice($id){
      
        $this->db->where('ServiceType' , 2);
        $this->db->where('PackageId' , $id);
        $this->db->delete('tbl_gf_default_prices_notice');
    }

    public function update_log_packages_by_logpackageid_custom($newTplType ,$sucTplType, $canTplType ,$strTpls , $id){
        $this->db->query("UPDATE tbl_gf_log_packages SET NewTplType= '$newTplType', SuccessTplType = '$sucTplType', CancelledTplType = '$canTplType' $strTpls WHERE LogPackageId = '$id'");
    }

    public function fetch_precodes_by_service_type($value , $id){
     
        $this->db->select('PreCodeId');
        $this->db->from('tbl_gf_precodes');
        $this->db->where('PreCode' , $value);
        $this->db->where('ServiceId' , $id);
        $this->db->where('Assigned' , 0);
        $this->db->where('ServiceType' , 2);

        $result = $this->db->get();
        return $result->row();
        
    }

    public function insert_precodes_by_srv_id($insert_data){
        $this->db->insert('tbl_gf_precodes' , $insert_data);
    }

    public function del_server_bulk_prices($id){
        $this->db->where('ServiceId' , $id);
        $this->db->delete('tbl_gf_server_bulk_prices');
    }

    public function del_server_bulk_group_prices($id){
        $this->db->where('ServiceId' , $id);
        $this->db->delete('tbl_gf_server_bulk_group_prices');
    }

    public function insert_server_bulk_prices($strData){
        $this->db->query("INSERT INTO tbl_gf_server_bulk_prices (ServiceId, MinQty, MaxQty, Price) VALUES $strData");
    }

    public function insert_server_bulk_group_prices($strGData){
        $this->db->query("INSERT INTO tbl_gf_server_bulk_group_prices (ServiceId, MinQty, MaxQty, GroupId, Price) VALUES $strGData");

    }

    public function fetch_log_packages_and_api_data($id){
    
        $this->db->select('A.* , SendExternalId, APIType'); 
        $this->db->from('tbl_gf_log_packages A');
        $this->db->join('tbl_gf_api B' , 'A.APIId = B.APIId' , 'left');
        $this->db->where('LogPackageId' , $id);
        
        $result=$this->db->get();
        return $result->row();
    }

    public function fetch_currency_log_packages($id){
        $query=$this->db->query("SELECT A.CurrencyId, CurrencyAbb, ConversionRate, Price, DefaultCurrency FROM tbl_gf_currency A LEFT JOIN tbl_gf_log_packages_currencies B ON (A.CurrencyId = B.CurrencyId AND PackageId = '$id') WHERE DisableCurrency = 0 ORDER BY DefaultCurrency DESC");

        return $query->result();
    }

    public function del_precodes_by_precodeId(){
        $this->db->where('PreCodeId' , $rId);
        $this->db->delete('tbl_gf_precodes');
    }


    public function fetch_service_pack_selected_features($id , $srvcType){
        $query=$this->db->query("SELECT A.FeatureId, Feature, IF(B.FeatureId IS NULL, '0', '1') AS Checked FROM tbl_gf_service_features A LEFT JOIN tbl_gf_pack_selected_features B 
        ON (A.FeatureId = B.FeatureId AND ServiceId = '$id' AND ServiceType = '$srvcType')");
        return $query->result();
    }

    public function fetch_precodes_custom_query($id ,$strPrCdWhr,$eu, $limit){
        $query=$this->db->query("SELECT PreCodeId, PreCode, OrderId, UserName, Assigned, AssignedDtTm, UserId FROM tbl_gf_precodes WHERE ServiceId = '$id' 
        AND ServiceType = '2' $strPrCdWhr ORDER BY PreCodeId DESC LIMIT $eu, $limit");

        return $query->result();
    }


    public function fetch_precodes_counts($strPrCdWhr , $id){
        $query=$this->db->query("SELECT COUNT(PreCodeId) AS TotalPCs FROM tbl_gf_precodes WHERE ServiceId = '$id' AND ServiceType=2 
        AND Assigned = 0 $strPrCdWhr GROUP BY ServiceId, ServiceType");
        return $query->row();
    }

    public function fetch_server_bulk_prices($id){
        
        $this->db->select('MinQty, MaxQty, Price');
        $this->db->from('tbl_gf_server_bulk_prices');
        $this->db->where('ServiceId' , $id);
        $this->db->order_by('Id');
        
        $result = $this->db->get();
        return $result->result();
    }

    public function fetch_logpackageprice($id){
        $this->db->select('LogPackagePrice');
        $this->db->from('tbl_gf_log_packages');
        $this->db->where('LogPackageId' , $id);

        $result=$this->db->get();
        return $result->row();
    }

    public function update_log_requests_by_id($statusId , $code , $notes , $comments , $currDtTm , $historyData , $strDeduct , $col_val , $id){
        $this->db->query("UPDATE tbl_gf_log_requests SET StatusId = '$statusId', Code = '$code', Comments = '$notes', Comments1 = '$comments', ReplyDtTm = '$currDtTm', 
            OrderData = '".addslashes($historyData)."', LastUpdatedBy = ".$this->session->userdata('AdminUserName')."  $strDeduct $col_val WHERE LogRequestId = '$id'");
    }


    public function fetch_log_requests_users_code_status($id){
        $this->db->select('UserName AS CustomerName, UserEmail, AlternateEmail, SUsername, CodeStatus, A.UserId, OrderData, RequestedAt, A.Comments,  B.SendSMS AS SMS_User, B.Phone');
        $this->db->from(' tbl_gf_log_requests A, tbl_gf_users B, tbl_gf_code_status C');
        $this->db->where('A.UserId = B.UserId');
        $this->db->where('A.StatusId = C.CodeStatusId');
        $this->db->where('LogPackageId' , $id);

        $result=$this->db->get();
        return $result->row();

    }

    public function fetch_log_requests_packages_users_by_id($id){
      
        $this->db->select(' A.*, LogPackageTitle, B.SendSMS, UserName');
        $this->db->from('tbl_gf_log_requests A, tbl_gf_log_packages B, tbl_gf_users D');
        $this->db->where('A.LogPackageId = B.LogPackageId');
        $this->db->where('A.UserId = D.UserId');
        $this->db->where('LogRequestId' , $id);

        $result=$this->db->get();
        return $result->row();
    }

    public function fetch_custom_field_by_disablefield(){
       
        $this->db->select('FieldId, FieldLabel, FieldType, Mandatory, FieldColName ');
        $this->db->from('tbl_gf_custom_fields');
        $this->db->where('ServiceType' , 2);
        $this->db->where('DisableField' , 0);

        $result=$this->db->get();
        return $result->result();
    }


    public function fetch_newsltrs_cat_news_letter(){
      

        $this->db->select('A.CategoryId, Category, A.NewsLtrId, NewsLtrTitle');
        $this->db->from('tbl_gf_newsltrs_cat Cat , tbl_gf_news_letter A');
        $this->db->where('Cat.CategoryId = A.CategoryId');
        $this->db->where('DisableNewsLtr' , 0);
        $this->db->order_by('OrderBy');
        $this->db->order_by('Category');
        $this->db->order_by('NewsLtrTitle');

        $result=$this->db->get();
        return $result->result();


    }


    public function fetch_alternate_apis_by_altapiid($apiId , $id , $srvcId , $sc){
       
        $this->db->select('AltAPISrvcId');
        $this->db->from('tbl_gf_alternate_apis');
        $this->db->where('AltAPIId' , $apiId);
        $this->db->where('AltServiceId' , $id);
        $this->db->where('AltAPIServiceId' , $srvcId);
        $this->db->where('AltServiceType' , $sc);
        
        if($altAPISrvcId > 0){
            $this->db->where('AltAPISrvcId' , $altAPISrvcId);
        }

        $result = $this->db->get();
        return $result->row();
        
    }

    public function fetch_alternate_apis_max(){
    
        $this->db->select('MAX(AltAPISortBy) AS AltAPISortBy');
        $this->db->from('tbl_gf_alternate_apis');

        $result = $this->db->get();
        return $result->row();
    }

    public function insert_alternate_apis($insert_data){
        $this->db->insert($insert_data);
    }

    public function update_alternate_apis_by_altapi($altAPISrvcId){
        $this->db->where('AltAPISrvcId' , $altAPISrvcId);
        $this->db->update('tbl_gf_alternate_apis');
    }

    public function fetch_alternate_api_by_srvcid($altAPISrvcId){
        
        $this->db->select('AltAPIId, AltAPIServiceId');
        $this->db->from('tbl_gf_alternate_apis');
        $this->db->where('AltAPISrvcId', $altAPISrvcId);

        $result = $this->db->get();
        return $result->row();
    }

    public function insert_blocked_ips($strData){
        $this->db->query("INSERT INTO tbl_gf_blocked_ips (IP, DtTm, Comments, UserId) VALUES ".$strData);
    }

    public function del_user_payment_methods($USER_ID_BTNS){
      
        $this->db->where('UserId' , $USER_ID_BTNS);
        $this->db->delete('tbl_gf_user_payment_methods');
    }

    public function insert_user_payment_methods($strData){
        $this->db->query("INSERT INTO tbl_gf_user_payment_methods (UserId, PaymentMethodId) VALUES $strData");
    }

    public function fetch_countries_users_currency($USER_ID_BTNS){
       
        
        $this->db->select("Credits, UserName, CONCAT(FirstName, ' ', LastName) AS Name, Phone, AddedAt, Country, Comments, AutoFillCredits, AllowAPI, 
        PricePlanId, DisableUser, CurrencySymbol, CurrencyAbb, UserEmail, TransferCredits, RemoveAllServices, CanAddCredits");
        $this->db->from('tbl_gf_countries B, tbl_gf_users A');
        $this->db->join('tbl_gf_currency C' , 'A.CurrencyId = C.CurrencyId' , 'left');
        $this->db->where('A.CountryId = B.CountryId');
        $this->db->where('UserId', $USER_ID_BTNS);

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_codes_packages_count($USER_ID_BTNS){
    
        $this->db->select("COUNT(A.PackageId) AS TotalOrders, PackageTitle, A.PackageId");
        $this->db->from('tbl_gf_codes A, tbl_gf_packages B');
        $this->db->where('A.PackageId = B.PackageId');
        $this->db->where('A.UserId', $USER_ID_BTNS);
        $this->db->group_by('A.PackageId, A.UserId');
        $this->db->order_by("COUNT(A.PackageId)" , 'DESC');

        $result = $this->db->get();
        return $result->result();
    }

    public function fetch_codes_slbf_packages_count($USER_ID_BTNS){
       
        $this->db->select(" COUNT(A.PackageId) AS TotalOrders, PackageTitle, A.PackageId");
        $this->db->from('tbl_gf_codes_slbf A, tbl_gf_packages B');
        $this->db->where('A.PackageId = B.PackageId');
        $this->db->where('A.UserId', $USER_ID_BTNS);
        $this->db->group_by('A.PackageId, A.UserId');
        $this->db->order_by("COUNT(A.PackageId)" , 'DESC');

        $result = $this->db->get();
        return $result->result();
    }


    public function fetch_log_requests_packages_count($USER_ID_BTNS){
   

        $this->db->select("COUNT(A.LogPackageId) AS TotalOrders, LogPackageTitle AS PackageTitle, A.LogPackageId");
        $this->db->from('tbl_gf_log_requests A, tbl_gf_log_packages B');
        $this->db->where('A.LogPackageId = B.LogPackageId');
        $this->db->where('A.UserId', $USER_ID_BTNS);
        $this->db->group_by('A.LogPackageId, A.UserId');
        $this->db->order_by("COUNT(A.LogPackageId)" , 'DESC');

        $result = $this->db->get();
        return $result->result();

    }

    public function fetch_payments_users($USER_ID_BTNS){
       
        $this->db->select(" Amount, PaymentStatus");
        $this->db->from('tbl_gf_payments');
        $this->db->where('ByAdmin' , 0);
        $this->db->where_in('PaymentStatus', '(1,2)');
        $this->db->where('UserId' , $USER_ID_BTNS);
        
        $result = $this->db->get();
        return $result->result();

    }


    public function fetch_payments_admin($USER_ID_BTNS){
      
        $this->db->select("Amount, PaymentStatus");
        $this->db->from('tbl_gf_payments');
        $this->db->where('ByAdmin' , 1);
        $this->db->where_in('PaymentStatus', '(1,2)');
        $this->db->where('UserId' , $USER_ID_BTNS);
        
        $result = $this->db->get();
        return $result->result();
    }


    public function fetch_codes_packages_count_sl3lbf($USER_ID_BTNS){
       
        $this->db->select("COUNT(A.PackageId) AS TotalServices");
        $this->db->from('tbl_gf_codes A, tbl_gf_packages B');
        $this->db->where('A.PackageId = B.PackageId');
        $this->db->where('A.UserId', $USER_ID_BTNS);
        $this->db->where('sl3lbf' , 0);

        $result = $this->db->get();
        return $result->row();
    }


    public function fetch_codes_slbf_packages_sl3lbf($USER_ID_BTNS){
       
        $this->db->select("COUNT(A.PackageId) AS TotalServices");
        $this->db->from('tbl_gf_codes_slbf A, tbl_gf_packages B');
        $this->db->where('A.PackageId = B.PackageId');
        $this->db->where('UserId' , $USER_ID_BTNS);
        $this->db->where('sl3lbf' , 1);

        $result = $this->db->get();
        return $result->row();
    }


    public function fetch_count_log_requests($USER_ID_BTNS){
       
        $this->db->select("COUNT(LogPackageId) AS TotalServices");
        $this->db->from('tbl_gf_log_requests');
        $this->db->where('UserId' , $USER_ID_BTNS);
        
        $result = $this->db->get();
        return $result->row();
    }


    public function fetch_packages_count(){
        
        $this->db->select("COUNT(PackageId) AS TotalServices");
        $this->db->from('tbl_gf_packages');
        $this->db->where('sl3lbf' , 0);
        
        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_packages_count_by_sl3lbf(){

        $this->db->select("COUNT(PackageId) AS TotalServices");
        $this->db->from('tbl_gf_packages');
        $this->db->where('sl3lbf' , 1);
        
        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_log_packages_count(){
        $this->db->select("COUNT(LogPackageId) AS TotalServices");
        $this->db->from('tbl_gf_log_packages');
        
        $result = $this->db->get();
        return $result->row();
    }


    public function fetch_email_settings_by_id(){
      
        $this->db->select('MinCredits');
        $this->db->from('tbl_gf_email_settings');
        $this->db->where('Id' , 1);

        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_payments_by_id($USER_ID_BTNS){
     
        $this->db->select('Amount');
        $this->db->from('tbl_gf_payments');
        $this->db->where('UserId' , $USER_ID_BTNS); 
        $result = $this->db->get();
        return $result->result();
    }

    public function update_users_data($USER_ID_BTNS){
      
        $this->db->set('DisableUser' , 0);
        $this->db->set('LoginAttempts' , 0);
        $this->db->set('Comments' , '');
        $this->db->where('UserId' , $USER_ID_BTNS);
        $this->db->update('tbl_gf_users');
    }

    public function del_blocked_ips($rid){
        $this->db->where('Id' , $rid);
        $this->db->from('tbl_gf_blocked_ips');
    }

    public function fetch_blocked_ips_by_userid($USER_ID_BTNS){
        
        $this->db->select('Id, IP, DtTm, Comments');
        $this->db->from('tbl_gf_blocked_ips');
        $this->db->where('UserId' , $USER_ID_BTNS); 
        $this->db->order_by('Id' , 'DESC'); 
        
        $result = $this->db->get();
        return $result->result();
    
    }

	public function executeQuery($query)
	{
		$this->db->query($query);
	}

	public function getRow($query)
	{
		$rsCurrencies = $this->db->query($query);
		return $rsCurrencies->row();
	}

	public function selectData($query)
	{
		$rsCurrencies = $this->db->query($query);
		return $rsCurrencies->result();
    }
    
    public function fetch_service_api_pricing($serviceId , $srvcAPIId , $sType){
        $query = $this->db->query("SELECT * FROM tbl_gf_service_api_pricing WHERE ServiceId = '$serviceId' AND ServiceAPIId = '$srvcAPIId' AND ServiceType = '$sType'");
        return $query->result();
    }

    public function fetch_price_by_disablePricePlan(){
        $query=$this->db->query("SELECT PricePlanId, PricePlan FROM tbl_gf_price_plans WHERE DisablePricePlan = 0 ORDER BY PricePlan");
        return $query->result();
    }

    public function delete_service_api_pricing($serviceId , $sType , $srvcAPIId){
        $this->db->query("DELETE FROM tbl_gf_service_api_pricing WHERE ServiceId = '$serviceId' AND ServiceType = '$sType' AND ServiceAPIId = '$srvcAPIId'");
    }

    public function insert_service_api_pricing($strQry){
        $this->db->query("INSERT INTO tbl_gf_service_api_pricing (ServiceId, ServiceAPIId, GroupId, PriceMargin, MarginType, ServiceType) VALUES $strQry");
    }
        

}
