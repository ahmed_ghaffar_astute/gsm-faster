<?php
/**
 * Created by PhpStorm.
 * User: shahidanwer
 * Date: 19/12/2019
 * Time: 12:27 AM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Util_model extends CI_Model {


    public function getLiveChat()
    {
        $condition = array("Id" => "1");
        $condition1 = array("InClientPanel" => "1");
        $this->db->select("ScriptCode");
        $this->db->from("tbl_gf_live_chat");
        $this->db->where($condition);
        $this->db->where($condition1);
        return $this->db->get()->row();

    }

}