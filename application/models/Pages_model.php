<?php
/**
 * Created by PhpStorm.
 * User: shahidanwer
 * Date: 19/12/2019
 * Time: 12:27 AM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Pages_model extends CI_Model {


    public function getHeaderFooterPages()
    {
        $condition = array("DisablePage" => "0");
        $condition1 = array("HeaderLinkCP" => "1");
        $condition2 = array("FooterLinkCP" => "1");
        $condition3 = array("PageId" => "18");
        $this->db->select("PageId, PageText, PageTitle, LinkTitle, HeaderLinkCP, FooterLinkCP, FileName, SEOURLName, HTMLTitle, MetaKW, MetaTags, Image");
        $this->db->from("tbl_gf_pages");
        $this->db->where($condition);
        $this->db->group_start()->where($condition1)->or_where($condition2)->or_where($condition3)->group_end();
        return $this->db->get();

    }

}