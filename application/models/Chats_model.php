<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Chats_model extends CI_Model {

	public function updateChat($script,$atWebsite,$inClient,$InAdminPanel,$id){
		$this->db->set('ScriptCode',$script);
		$this->db->set('AtWebsite',$atWebsite);
		$this->db->set('InClientPanel',$inClient);
		$this->db->set('InAdminPanel',$InAdminPanel);
		$this->db->where('Id', $id);
		$this->db->update('tbl_gf_live_chat');
		$msg = 'true';
		return $msg;
	}
	public function getChat($id){
		$this->db->select('*');
		$this->db->from('tbl_gf_live_chat');
		$this->db->where("Id", $id);
		$result=$this->db->get();
		return $result->row();
	}
}
