<?php
/**
 * Created by PhpStorm.
 * User: Shahid Anwar
 * Date: 18/12/2019
 * Time: 7:57 PM
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    public function getSettings() {
        $condition = array("Id" => "1");
        $this->db->select("Design, IMEIFieldType, CheckSumIMEI, IMEIOrderDropDownType, Copyrights, SiteTitle, Address, TicketSystem, 
			FileOrderDropDownType, ServerOrderDropDownType, IMEIServices, FileServices, ServerServices, MinCredits, MaxCredits, PinCode, Phone, GoogleCaptchaSecretKey, 
			ForcefullPwdChangeDays, PayTo, SendNewIMEIOrderEmail, KnowledgeBase, SendNewFileOrderEmail, SendNewServerOrderEmail, ToEmail, Company, FAQs, GoogleCaptchaSiteKey, 
			IMEIServices, FileServices, ServerServices, Videos, Theme, CreditsTransferFee");
        $this->db->from("tbl_gf_email_settings");
        $this->db->where($condition);
        return $this->db->get()->row();
    }

    public function getUserDetails($userId) {
        $condition = array("UserId" => "$userId");
        $this->db->select("Credits, CurrencyAbb, CurrencySymbol, ConversionRate, A.CurrencyId, DefaultCurrency, AllowAPI, PricePlanId, ThemeColor, PinCode, CPanel,
		    Phone, CountryId, UserLang, Address, City, State, Zip, Subscribed, APIKey, AllowNegativeCredits, CanAddCredits, ForcefullPwdChangeDt, TransferCredits, GoogleAuth");
        //$this->db->select("*");
        $this->db->from("tbl_gf_users as A");
        $this->db->join('tbl_gf_currency as B', 'A.CurrencyId =  B.CurrencyId');
        $this->db->where($condition);
        return $this->db->get()->row();
    }

    public function getUserReceipts($userId)
    {
        $condition = array("UserId" => "$userId");
        $this->db->select("Amount");
        $this->db->from("tbl_gf_payments");
        $this->db->where($condition);
        return $this->db->get();

    }
    public function getUserImeiOrdersCountByStatus($userId)
    {
        $condition = array("UserId" => "$userId");
        $this->db->select("CodeStatusId, IFNULL(count(*), 0) as OrdersCount");
        $this->db->from("tbl_gf_codes");
        $this->db->where($condition);
        $this->db->group_by("CodeStatusId");
        return $this->db->get();

    }
    public function getUserFileServiceOrdersCountByStatus($userId)
    {
        $condition = array("UserId" => "$userId");
        $this->db->select("CodeStatusId, IFNULL(count(*), 0) as OrdersCount");
        $this->db->from("tbl_gf_codes_slbf");
        $this->db->where($condition);
        $this->db->group_by("CodeStatusId");
        return $this->db->get();

    }
    public function getUserServerOrdersCountByStatus($userId)
    {
        $condition = array("UserId" => "$userId");
        $this->db->select("StatusId, IFNULL(count(*), 0) as OrdersCount");
        $this->db->from("tbl_gf_log_requests");
        $this->db->where($condition);
        $this->db->group_by("StatusId");
        return $this->db->get();

    }
    public function getUserLockedAmount($userId)
    {
        $condition = array("UserId" => "$userId");
        $statusCodes = array(1,4);
        $this->db->select("SUM(Credits) AS LockedAmount");
        $this->db->from("tbl_gf_codes");
        $this->db->where($condition);
        $this->db->where_in("CodeStatusId", $statusCodes);
        //$this->db->group_by("StatusId");
        $rowLockedImei = $this->db->get()->row();
        $rowLockedImeiAmount = $rowLockedImei->LockedAmount;


        $condition = array("UserId" => "$userId");
        $statucCodes = "1,4";
        //$condition1 = array("CodeStatusId" => $statucCodes);
        $this->db->select("SUM(Credits) AS LockedAmount");
        $this->db->from("tbl_gf_codes_slbf");
        $this->db->where($condition);
        $this->db->where_in("CodeStatusId", $statusCodes);
        //$this->db->group_by("StatusId");
        $rowLockedFile = $this->db->get()->row();
        $rowLockedFileAmount = $rowLockedFile->LockedAmount;

        $condition = array("UserId" => "$userId");
        $statucCodes = "1,4";
        //$condition1 = array("StatusId" => $statucCodes);
        $this->db->select("SUM(Credits) AS LockedAmount");
        $this->db->from("tbl_gf_log_requests");
        $this->db->where($condition);
        $this->db->where_in("StatusId", $statusCodes);
        //$this->db->group_by("StatusId");
        $rowLockedServer = $this->db->get()->row();
        $rowLockedServerAmount = $rowLockedServer->LockedAmount;

        $totalLockedAmount = $rowLockedImeiAmount + $rowLockedFileAmount + $rowLockedServerAmount;

        return $totalLockedAmount;


    }
    public function getUserLockedCount($userId)
    {
        $condition = array("UserId" => "$userId");
        $statucCodes = array(1,4);
        $this->db->select("COUNT(Credits) AS LockedCount");
        $this->db->from("tbl_gf_codes");
        $this->db->where($condition);
        $this->db->where_in("CodeStatusId", $statucCodes);
        //$this->db->group_by("StatusId");
        $rowLockedImei = $this->db->get()->row();
        $rowLockedImeiCount = $rowLockedImei->LockedCount;
        //last_query();

        $condition = array("UserId" => "$userId");
        $statucCodes = "1,4";
        $condition1 = array("CodeStatusId" => $statucCodes);
        $this->db->select("COUNT(Credits) AS LockedCount");
        $this->db->from("tbl_gf_codes_slbf");
        $this->db->where($condition);
        $this->db->where_in("CodeStatusId", $statucCodes);
        //$this->db->group_by("StatusId");
        $rowLockedFile = $this->db->get()->row();
        $rowLockedFileCount = $rowLockedFile->LockedCount;
        //last_query();

        $condition = array("UserId" => "$userId");
        $statucCodes = "1,4";
        $condition1 = array("StatusId" => $statucCodes);
        $this->db->select("COUNT(Credits) AS LockedCount");
        $this->db->from("tbl_gf_log_requests");
        $this->db->where($condition);
        $this->db->where_in("StatusId", $statucCodes);
        //$this->db->group_by("StatusId");
        $rowLockedServer = $this->db->get()->row();
        $rowLockedServerCount = $rowLockedServer->LockedCount;
        //last_query();

        $totalLockedCount = $rowLockedImeiCount + $rowLockedFileCount + $rowLockedServerCount;

        return $totalLockedCount;


    }
    public function getLatestFiveImeiOrders($userId)
    {
        $this->db->select("CodeId, IMEINo, SerialNo, A.CodeStatusId, Code, PackageTitle, CodeStatus, Credits, DATE(RequestedAt) AS RequestedAt ");
        $this->db->from("tbl_gf_codes A, tbl_gf_packages B, tbl_gf_code_status C");
        $this->db->where("A.PackageId = B.PackageId");
        $this->db->where("A.CodeStatusId = C.CodeStatusId");
        $this->db->where("Archived = 0");
        $this->db->where("UserId = $userId");
        $this->db->order_by("CodeId", "desc");
        $this->db->limit(5);
        $result = $this->db->get();
        return $result->result();
    }
    public function getLatestFiveTickets($userId)
    {
        $this->db->select("TicketId, TicketNo, TcktStatus, Subject, DATE(DtTm) AS DtTm, StatusId");
        $this->db->from("tbl_gf_tckt_status D, tbl_gf_tickets A");
        $this->db->where("A.StatusId = D.TcktStatusId ");
        $this->db->where("AId = 0");
        $this->db->where("UserId = $userId");
        $this->db->order_by("TicketId", "desc");
        $this->db->limit(5);
        $result = $this->db->get();
        return $result->result();
    }
    public function getUserTicket($userId, $ticketId)
    {
        $this->db->select("TicketId, TicketNo, TcktStatus, Subject, DATE(DtTm) AS DtTm, StatusId, DepartmentId, CategoryId, PriorityId");
        $this->db->from("tbl_gf_tckt_status D, tbl_gf_tickets A");
        $this->db->where("A.StatusId = D.TcktStatusId ");
        $this->db->where("AId = 0");
        $this->db->where("UserId = $userId");
        $this->db->where("TicketId = $ticketId");
        $result = $this->db->get()->row();
        return $result;
    }
    public function getTicketDepartmentDetails($userId, $ticketId)
    {
        $this->db->select("DepartmentId, CategoryId, PriorityId, TicketNo, Subject, Name, Email, UserId, TcktCategory, DtTm, DeptName");
        $this->db->from("tbl_gf_tickets A, tbl_gf_tckt_dept B, tbl_gf_tckt_category E");
        $this->db->where("A.DepartmentId = B.DeptId ");
        $this->db->where("A.CategoryId = E.TcktCategoryId");
        $this->db->where("UserId = $userId");
        $this->db->where("TicketId = '$ticketId'");
        $result = $this->db->get()->row();
        return $result;
    }
    public function getTicketReplies($userId, $ticketId){

        $this->db->select("TicketId, Name, F.UserName, CONCAT(F.FirstName, ' ', F.LastName) AS ClientName, A.StatusId, Message, DtTm, A.IP, TcktStatus, ByAdmin ");
        $this->db->from("tbl_gf_tckt_status B, tbl_gf_tickets A ");
        $this->db->join("tbl_gf_users F", "A.UserId = F.UserId", "left");
        $this->db->where("A.StatusId = B.TcktStatusId");
        $this->db->where("A.UserId = $userId");
        $this->db->where("TicketNo = $ticketId");
		$this->db->order_by("TicketId desc");
        $result = $this->db->get();
        return $result->result();

    }

    public function threshold_amount(){
    
        $user_id=$this->session->userdata('GSM_FUS_UserId');
        $this->db->select("ThresholdAmount");
        $this->db->from("tbl_gf_users");
        $this->db->where('UserId' , $user_id);
        $result=$this->db->get();
        return $result->row();
        
    }

    public function update_threshold_amount($amount , $user_id){
        $data=array('ThresholdAmount'=> $amount);
        $this->db->where('UserId',$user_id);
        return $this->db->update('tbl_gf_users',$data);
    }

    public function get_transfer_fee($id){
        $this->db->select("CreditsTransferFee");
        $this->db->from("tbl_gf_email_settings");
        $this->db->where("Id" , 1);
        $result = $this->db->get();
        return $result->row();
    }

    public function fetch_other_userdata($uName , $email){
        $user_id = $this->session->userdata('GSM_FUS_UserId');
        $this->db->select("UserId, Credits, UserName, UserEmail, CurrencyId");
        $this->db->from("tbl_gf_users");
        $this->db->where("UserName" , $uName);
        $this->db->where("UserEmail" ,$email);
        $this->db->where("UserId <>" , $user_id);
        $result = $this->db->get();
        return $result->row();
        
    }

    public function update_user_data($data , $id){
        $this->db->where('UserId' ,$id);
        return $this->db->update('tbl_gf_users' , $data);
    }

    public function add_credit_history($data){
        $this->db->insert('credit_history' , $data);
        return $this->db->insert_id();
    }

   

    public function get_product_and_services($sc , $categoryId){

        $strPackIds = getUserPacksIds($this->session->userdata('GSM_FUS_UserId') , $sc);
        $strPackIds = explode(',' , $strPackIds);
        $strPackIds = array_map('trim', $strPackIds);

        if($categoryId > 0){
            $this->db->where('A.CategoryId' , $categoryId);
        }
        switch($sc)
        {
            case '0': // IMEI services
                $this->db->where(array('sl3lbf'=>0 , 'ArchivedPack'=>0));  
                $this->db->where('SL3BF' , 0);
                $tblName = 'tbl_gf_package_category';
                $tblPckName = 'tbl_gf_packages';
                $colId = 'PackageId';
                $colTitle = 'PackageTitle';
                $disableCol = 'DisablePackage';
                $colTime = 'TimeTaken';
                $colPrice = 'PackagePrice';
                break;
            case '1': // File services
                $this->db->where(array('sl3lbf'=>1 , 'ArchivedPack'=>0));
                $this->db->where('SL3BF' , 1);
                $tblName = 'tbl_gf_package_category';
                $tblPckName = 'tbl_gf_packages';
                $colId = 'PackageId';
                $colTitle = 'PackageTitle';
                $disableCol = 'DisablePackage';
                $colTime = 'TimeTaken';
                $colPrice = 'PackagePrice';
                break;
            case '2': // Server services
                $this->db->where('ArchivedPack', 0);
                $tblName = 'tbl_gf_log_package_category';
                $tblPckName = 'tbl_gf_log_packages';
                $colId = 'LogPackageId';
                $colTitle = 'LogPackageTitle';
                $disableCol = 'DisableLogPackage';
                $colTime = 'DeliveryTime';
                $colPrice = 'LogPackagePrice';
                break;
        }


      
        
        $this->db->where_not_in("A.{$colId}", $strPackIds);
        
        // $rsPacks = $objDBCD14->query("SELECT A.CategoryId, Category, A.$colId AS PackageId, $disableCol, $colTitle AS PackageTitle, IF($colTime IS NULL OR $colTime = '', '-', $colTime) AS 
		// 	DeliveryTime FROM $tblName Cat, $tblPckName A WHERE Cat.CategoryId = A.CategoryId AND $disableCol = 0 $strWhereCat $strWhere 
        //     ".$strPackIds." ORDER BY Category, $colTitle");
        
        $select = array();
        $select[]= 'A.CategoryId';
        $select[] = 'Category';
        $select[]= "A.{$colId} AS PackageId";
        $select[]= "{$disableCol}";
        $select[] = "{$colTitle} AS PackageTitle" ;
        $select[]= "IF({$colTime} IS NULL OR {$colTime} = '', '-', {$colTime}) AS DeliveryTime";
        
        $this->db->select(implode(',' , $select));
        $this->db->from("{$tblName} Cat , {$tblPckName} A");
        $this->db->where('Cat.CategoryId = A.CategoryId');
        $this->db->where("A.CategoryId AND {$disableCol} = 0");
        $this->db->order_by("Category , {$colTitle}");

        $result=$this->db->get();
        return $result->result();
    }


    public function get_pack_prices_user($user_id , $s_ids){

        $PACK_PRICES_USER = array();

        $this->db->select('PackageId , Price');
        $this->db->from('tbl_gf_users_packages_prices');
        $this->db->where('UserId' , $user_id);
        $this->db->where_in('PackageId' , $s_ids);
        $result=$this->db->get();
        $rsPackPrices=$result->result();


        foreach($rsPackPrices as $row){
            $PACK_PRICES_USER[$row->PackageId] = roundMe($row->Price);
        }

        return $PACK_PRICES_USER;
    }

    public function get_pack_prices_plan($sc , $s_ids , $MY_CURRENCY_ID){
        $PACK_PRICES_PLAN = '';
        $USER_CURRENCY_RATE = 1;
        $user_id=$this->session->userdata('GSM_FUS_UserId');
       
        $this->db->select('ConversionRate');
        $this->db->from('tbl_gf_users A , tbl_gf_currency B');
        $this->db->where('A.CurrencyId = B.CurrencyId');
        $this->db->where('UserId' , $user_id);
        $result=$this->db->get();
        $rwUsrRate=$result->row();
        
        
        if(isset($rwUsrRate->ConversionRate) && $rwUsrRate->ConversionRate != ''){
            $USER_CURRENCY_RATE = $rwUsrRate->ConversionRate;
        }
   
        $this->db->select('PackageId , Price');
        $this->db->from('tbl_gf_users A , tbl_gf_plans_packages_prices B ,tbl_gf_currency C');
        $this->db->where('A.PricePlanId = B.PlanId');
        $this->db->where('B.CurrencyId = C.CurrencyId');
        $this->db->where('DefaultCurrency' , 1);
        $this->db->where('UserId' , $user_id);
        $this->db->where('ServiceType', $sc);
        $this->db->where_in('PackageId' , $s_ids);
        $result=$this->db->get();
        $rsPlanPr_DEFAULT=$result->result();
        

        foreach($rsPlanPr_DEFAULT as $row){
            $PACK_PRICES_PLAN[$row->PackageId] = roundMe($row->Price * $USER_CURRENCY_RATE);
        }


        $this->db->select('PackageId, Price');
        $this->db->from('tbl_gf_users A, tbl_gf_plans_packages_prices B');
        $this->db->where('A.PricePlanId = B.PlanId');
        $this->db->where('B.CurrencyId' , $MY_CURRENCY_ID);
        $this->db->where('ServiceType', $sc);
        $this->db->where('UserId' , $user_id);
        $this->db->where_in('PackageId' , $s_ids);
        $result=$this->db->get();
        $rsPlanPr = $result->result();

        foreach($rsPlanPr as $row){
            $PACK_PRICES_PLAN[$row->PackageId] = roundMe($row->Price);
        }

        return $PACK_PRICES_PLAN ;
    }


    public function get_pack_prices_base($sc , $s_ids , $MY_CURRENCY_ID){
        $CONVERSION_RATE = 1;
        $this->db->select(' A.PackageId, Price, PackagePrice');
        $this->db->from('tbl_gf_packages A');
        $this->db->join('tbl_gf_packages_currencies B' , "A.PackageId = B.PackageId AND CurrencyId = '{$MY_CURRENCY_ID}'" , 'left');
        $this->db->where('sl3lbf' , $sc);
        $this->db->where_in('A.PackageId' , $s_ids);
        $result =$this->db->get();
        $rsPackPrices = $result->result();
        
        foreach($rsPackPrices as $row){
            if($row->Price == ''){
                $PACK_PRICES_BASE[$row->PackageId] = roundMe($row->PackagePrice * $CONVERSION_RATE);
            }
            else{
                $PACK_PRICES_BASE[$row->PackageId] = roundMe($row->Price);
            }
        }

        return $PACK_PRICES_BASE;

    }
    //log prices 
    public function get_log_pack_prices_user($user_id){
        $PACK_PRICES_USER = array();

        $this->db->select('LogPackageId , Price');
        $this->db->from('tbl_gf_users_log_packages_prices');
        $this->db->where('UserId' , $user_id);
        $result=$this->db->get();
        $rsPackPrices=$result->result();


        foreach($rsPackPrices as $row){
            $PACK_PRICES_USER[$row->LogPackageId] = roundMe($row->Price);
        }

        return $PACK_PRICES_USER;
        
    }

    public function get_log_pack_prices_plan($user_id, $MY_CURRENCY_ID)
    {
        $PACK_PRICES_PLAN = '';
        $USER_CURRENCY_RATE = 1;
       
        $this->db->select('ConversionRate');
        $this->db->from('tbl_gf_users A , tbl_gf_currency B');
        $this->db->where('A.CurrencyId = B.CurrencyId');
        $this->db->where('UserId' , $user_id);
        $result=$this->db->get();
        $rwUsrRate=$result->row();
        
        
        if(isset($rwUsrRate->ConversionRate) && $rwUsrRate->ConversionRate != ''){
            $USER_CURRENCY_RATE = $rwUsrRate->ConversionRate;
        }

        $this->db->select('PackageId , Price');
        $this->db->from('tbl_gf_users A , tbl_gf_plans_packages_prices B ,tbl_gf_currency C');
        $this->db->where('A.PricePlanId = B.PlanId');
        $this->db->where('B.CurrencyId = C.CurrencyId');
        $this->db->where('DefaultCurrency' , 1);
        $this->db->where('UserId' , $user_id);
        $this->db->where('ServiceType', 2);
        $result=$this->db->get();
        $rsPlanPr_DEFAULT=$result->result();
        

        foreach($rsPlanPr_DEFAULT as $row){
            $PACK_PRICES_PLAN[$row->PackageId] = roundMe($row->Price * $USER_CURRENCY_RATE);
        }
       
        $this->db->select('PackageId, Price');
        $this->db->from('tbl_gf_users A, tbl_gf_plans_packages_prices B');
        $this->db->where('A.PricePlanId = B.PlanId');
        $this->db->where('B.CurrencyId' , $MY_CURRENCY_ID);
        $this->db->where('ServiceType', 2);
        $this->db->where('UserId' , $user_id);
        $result=$this->db->get();
        $rsPlanPr = $result->result();

        foreach($rsPlanPr as $row){
            $PACK_PRICES_PLAN[$row->PackageId] = roundMe($row->Price);
        }

        return $PACK_PRICES_PLAN ;
      
    }

    public function get_log_pack_prices_base($MY_CURRENCY_ID){
        
        $CONVERSION_RATE = 1;
        $rsPackPrices=$this->db->query("SELECT LogPackageId, Price, LogPackagePrice FROM tbl_gf_log_packages A LEFT JOIN tbl_gf_log_packages_currencies B ON (A.LogPackageId = B.PackageId AND CurrencyId = '$MY_CURRENCY_ID')")->result();
       
        foreach($rsPackPrices as $row){
            if($row->Price == ''){
                $PACK_PRICES_BASE[$row->LogPackageId] = roundMe($row->LogPackageId * $CONVERSION_RATE);
            }
            else{
                $PACK_PRICES_BASE[$row->LogPackageId] = roundMe($row->Price);
            }
        }

        return $PACK_PRICES_BASE;
    }



    public function fetch_login_history(){
       
        $this->db->select('Id , IP , LoginTime, LogoutTime');
        $this->db->from('tbl_gf_user_login_log');
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
        $this->db->order_by('Id DESC');
        $result=$this->db->get();
        
        return $result->result();

    }


    public function get_tickets($sId){
        if($sId > 0){
            $strWhere = " AND A.StatusId = '$sId'";
            $this->db->where('A.StatusId' , $sId);
        }
        $dtFrom = '';
        $dtTo = '';
        $txtlqry = '';
        $page_name = $this->input->server('PHP_SELF');

        if(!isset($start))
            $start = 0;

        if($this->input->get('start')){
            $start = $this->input->get('start');
        }

        $eu = ($start - 0);
        $limit = 50;
        $thisp = $eu + $limit;
        $back = $eu - $limit;
        $next = $eu + $limit;
      
        
        $this->db->select('TicketId, TicketNo, TcktStatus, Subject, DtTm, StatusId');
        $this->db->from('tbl_gf_tckt_status D, tbl_gf_tickets A');
        $this->db->where('A.StatusId = D.TcktStatusId');
        $this->db->where('AId', 0);
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
        $this->db->order_by('TicketId' , 'DESC');

        $result=$this->db->get();
        return $result->result();

    }


    public function get_ticket_info($id){
       
        $this->db->select('DepartmentId ,CategoryId, PriorityId, TicketNo, Subject, Name, Email, UserId, TcktCategory, DtTm, DeptName');
        $this->db->from('tbl_gf_tickets A, tbl_gf_tckt_dept B, tbl_gf_tckt_category E');
        $this->db->where('A.DepartmentId = B.DeptId');
        $this->db->where('A.CategoryId = E.TcktCategoryId');
        $this->db->where('TicketId', $id);

        $result=$this->db->get();
        return $result->row();


    }


    public function get_comments($ticketNo , $user_id){
       
        $this->db->select("TicketId, Name, F.UserName, CONCAT(F.FirstName, ' ', F.LastName) AS ClientName, A.StatusId, Message, DtTm, A.IP, TcktStatus, ByAdmin");
        $this->db->from("tbl_gf_tckt_status B, tbl_gf_tickets A");
        $this->db->join("tbl_gf_users F" , "A.UserId = F.UserId" , 'left');
        $this->db->where("A.StatusId = B.TcktStatusId");
        $this->db->where('A.UserId' , $user_id);
        $this->db->where('TicketNo' , $ticketNo);
        $this->db->order_by('TicketId' , 'DESC');

        $result=$this->db->get();
        return $result->result();
    }

    public function add_ticket_data($id , $message , $dtTm , $ip){

        $row = $this->get_ticket_info($id);
  
        $dId = $row->DepartmentId;
        $cId = $row->CategoryId;
        $pId = $row->PriorityId;
        $tcktNo = $row->TicketNo;
        $subject = stripslashes($row->Subject);
        $name = stripslashes($row->Name);
        $email = $row->Email;
        $userId = $row->UserId;
        $category = stripslashes($row->TcktCategory);
        $department = stripslashes($row->DeptName);
        $dt = $row->DtTm;


        $this->db->set('StatusId', 4);
        $this->db->set('ReadByAdmin' , 0);
        $this->db->where('TicketNo', $tcktNo);
        $this->db->update('tbl_gf_tickets');

     
        $data = array(
           'TicketNo' => $tcktNo,
           'UserId' => $userId ,
           'DepartmentId' => $dId,
           'CategoryId' => $cId,
           'PriorityId'=> $pId,
           'Name' => $name,
           'Email' => $email,
           'Subject' => $subject,
           'Message' => $message,
           'DtTm'=> $dtTm,
           'IP'=> $ip,
           'StatusId' => 4,
           'AId'=> 1,
           'ReadByAdmin' => 0 ,
           'ByAdmin' => 0
        );

        $this->db->insert('tbl_gf_tickets' , $data);
        return $this->db->insert_id();        
    }

    public function update_ticket_status($id){
        $row = $this->get_ticket_info($id);
       
        $this->db->set('StatusId', 2);
        $this->db->set('ReadByAdmin' , 0);
        $this->db->where('TicketNo', $row->TicketNo);
        $this->db->update('tbl_gf_tickets');
    }


    public function get_department(){
        $this->db->select('DeptId AS Id, DeptName AS Value');
        $this->db->from('tbl_gf_tckt_dept');
        $this->db->where(' DisableDept' , 0);
        $this->db->order_by('DeptName');

        $result=$this->db->get();
        return $result->result();
    }

    public function get_category(){
       
        $this->db->select('TcktCategoryId AS Id, TcktCategory AS Value');
        $this->db->from('tbl_gf_tckt_category');
        $this->db->where('DisableTcktCategory' , 0);
        $this->db->order_by('TcktCategory');

        $result=$this->db->get();
        return $result->result();

    }


    public function get_priority(){
       
        $this->db->select('TcktPriorityId AS Id, TcktPriority AS Value');
        $this->db->from('tbl_gf_tckt_priority');
        $this->db->where('DisableTcktPriority' , 0);
        $this->db->order_by('TcktPriorityId');

        $result=$this->db->get();
        return $result->result();
        
    }

    public function add_user_ticket($data){
        $this->db->insert('tbl_gf_tickets' , $data);
        return $this->db->insert_id();
    }

    public function get_all_statements(){
        
        $this->db->select('HistoryId, IMEINo , HistoryDtTm, Description, Credits, CreditsLeft, FeePercentage');
        $this->db->from('tbl_gf_credit_history');
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
        $this->db->order_by('HistoryId' , 'DESC');
        $result = $this->db->get();
        return $result->result();

    }

    public function get_files(){
    
        $this->db->select('*');
        $this->db->from('tbl_gf_downloads');
        $this->db->where('DisableDownload' , 0);
        $this->db->order_by('DownloadId' , 'DESC');

        $result=$this->db->get();
        return $result->result();

    }

    public function get_client_invoices(){
    
        $this->db->select('PaymentId, Amount, A.Credits, D.PaymentStatus, C.PaymentMethod, A.PaymentStatus AS PaymentStatusId, PaymentDtTm, Currency, A.Vat');
        $this->db->from('tbl_gf_payment_status D, tbl_gf_payments A');
        $this->db->join('tbl_gf_payment_methods C' , 'A.PaymentMethod = C.PaymentMethodId' , 'left');
        $this->db->where('A.PaymentStatus = D.PaymentStatusId');
        $this->db->where('ByAdmin' , 0);
        $this->db->where('A.UserId' , $this->session->userdata('GSM_FUS_UserId'));
        $this->db->order_by('PaymentId' , 'DESC');

        $result=$this->db->get();
        return $result->result();

    }

    public function get_admin_invoices(){
        $this->db->select('PaymentId, Amount, A.Credits, D.PaymentStatus, C.PaymentMethod, A.PaymentStatus AS PaymentStatusId, PaymentDtTm, Currency, A.Vat');
        $this->db->from('tbl_gf_payment_status D, tbl_gf_payments A');
        $this->db->join('tbl_gf_payment_methods C' , 'A.PaymentMethod = C.PaymentMethodId' , 'left');
        $this->db->where('A.PaymentStatus = D.PaymentStatusId');
        $this->db->where('ByAdmin' , 1);
        $this->db->where('A.UserId' , $this->session->userdata('GSM_FUS_UserId'));
        $this->db->order_by('PaymentId' , 'DESC');

        $result=$this->db->get();
        return $result->result();
    }

    public function get_payment_details($id){
           
        $this->db->select('PaymentId, UserId, Credits, B.PaymentMethod AS PaymentMode, Amount, PaymentStatus, PayerEmail, TransactionId, PayMethodTypeId, 
        Comments, A.Vat, DATE(PaymentDtTm) AS InvoiceDate, DATE(PaidDtTm) AS PaidDtTm, Currency, ByAdmin, PayableAmount, PaymentMethodId')   ;
        $this->db->from('tbl_gf_payments A'); 
        $this->db->join('tbl_gf_payment_methods B' , 'A.PaymentMethod = B.PaymentMethodId' , 'left');
        $this->db->where('PaymentId' , $id);
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
        
        $result=$this->db->get();
        return $result->row();

    }

    public function get_reciever_data($id = ''){
        if($id){
            $this->db->select('APIUsername, APIPassword, APISignature, Username, MassPaymentTime, Fee, FeeType');
            $this->db->from('tbl_gf_pp_receivers');
            $this->db->where('ReceiverId', $id);
            $result=$this->db->get();
            return $result->row();
        }else{
            $this->db->select('ReceiverId, Username');
            $this->db->from('tbl_gf_pp_receivers');
            $this->db->where('DisablePP', 0);
            $this->db->order_by('Username');
            $result=$this->db->get();
            return $result->result();
        }
    }

    public function get_payment_methods($pMethodId){
        $this->db->select('APIUsername, APIPassword, APISignature, Username, MassPaymentTime, Fee, FeeType');
        $this->db->from('tbl_gf_payment_methods');
        $this->db->where('PaymentMethodId' , $pMethodId);
        
        $result = $this->db->get();
        return $result->row();
    }


    public function get_payment_id($transactionId){
        $this->db->select('PaymentId');
        $this->db->from('tbl_gf_payments');
        $this->db->where('TransactionId' , $transactionId);

        $result=$this->db->get();
        return $result->row() ;
    }

    public function get_payment_detail($id){
      
        $this->db->select('PaymentId, Amount, Currency, DATE(PaymentDtTm) AS PaymentDtTm');
        $this->db->from('tbl_gf_payments');
        $this->db->where('PaymentId' , $id);
        $this->db->where('PaymentStatus' , 1);
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));

        $result = $this->db->get();
        return $result->row();
    }


    public function get_user_autofillcredits(){
       
        $this->db->select('AutoFillCredits');
        $this->db->from('tbl_gf_users');
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));

        $result=$this->db->get();
        return $result->row();
    }

    public function get_user_credits(){
       
        $this->db->select('Credits');
        $this->db->from('tbl_gf_users');
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));

        $result=$this->db->get();
        return $result->row();

    }

    public function update_user_credits($encCredits , $userId = ''){
        
        $this->db->set('Credits' , $encCredits);
        if($userId){
            $this->db->where('UserId' ,$userId);
        }else{
            $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
        }
        $this->db->update('tbl_gf_users');
        
    }
    
                                    
    public function insert_credit_history($data){
        $this->db->insert('tbl_gf_credit_history' , $data);
    }

    public function update_payments($receiver_email ,$payer_email , $transactionId , $dtTm , $encInvCrdts , $encInvAmnt , $id){
    $this->db->set('ReceiverEmail' , $receiver_email);
    $this->db->set('PayerEmail' , $payer_email);
    $this->db->set('TransactionId' , $transactionId);
    $this->db->set('PaymentStatus' , 2);
    $this->db->set('CreditsTransferred' , 1);
    $this->db->set('UpdatedAt' , $dtTm);
    $this->db->set('Credits' , $encInvCrdts);
    $this->db->set('Amount' , $encInvAmnt);
    $this->db->where('PaymentId' , $id);
    $this->db->update('tbl_gf_payments');

    }


    public function update_payment_2($receiver_email ,$payer_email , $transactionId , $dtTm , $encInvCrdts , $encInvAmnt ,$id){
     
    $this->db->set('ReceiverEmail' , $receiver_email);
    $this->db->set('PayerEmail' , $payer_email);
    $this->db->set('TransactionId' , $transactionId);
    $this->db->set('PaymentStatus' , 5);
    $this->db->set('UpdatedAt' , $dtTm);
    $this->db->set('Credits' , $encInvCrdts);
    $this->db->set('Amount' , $encInvAmnt);
    $this->db->where('PaymentId' , $id);
    $this->db->update('tbl_gf_payments');

    }

    public function get_userdata(){
        
        $this->db->select('UserId, UserEmail, FirstName, LastName, Phone, Address, Country, City, State, Zip');
        $this->db->from('tbl_gf_users A, tbl_gf_countries B');
        $this->db->where('A.CountryId = B.CountryId');
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
        
        $result = $this->db->get();
        return $result->row();
    }


    public function get_logopath(){
       
        $this->db->select('LogoPath');
        $this->db->from('tbl_gf_logo');
        $this->db->where('DisableLogo' , 0);
        $this->db->where('LogoId' , 4);

        $result=$this->db->get();
        return $result->row();
    }

    public function get_sum_payment($id){
        $this->db->select('SUM(InvAmount) AS AmountPaid');
        $this->db->from('tbl_gf_payment_details');
        $this->db->where('InvoiceId' , $id);
        $this->db->group_by('InvoiceId');

        $result=$this->db->get();
        return $result->result();
    }


    public function get_payment_details_2($id){
       
        $this->db->select('InvAmount, InvTransactionId, DATE(InvDtTm) AS InvDtTm, InvComments, PaymentMethod');
        $this->db->from('tbl_gf_payment_details A, tbl_gf_payment_methods B');
        $this->db->where('A.InvPaymentMethodId = B.PaymentMethodId');
        $this->db->where('InvoiceId' , $id);
        $this->db->order_by('InvDtlId');

        $result = $this->db->get();
        return $result->result();


    }

    public function get_imei_services(){
        

        $strPackIds = getUserPacksIds($this->session->userdata('GSM_FUS_UserId') , 0);
        $strPackIds = explode(',' , $strPackIds);
        $strPackIds = array_map('trim', $strPackIds);


        $this->db->select('A.CategoryId, Category, A.PackageId, PackageTitle');
        $this->db->from('tbl_gf_package_category Cat,tbl_gf_packages A');
        $this->db->where('Cat.CategoryId = A.CategoryId');
        $this->db->where('DisableCategory' , 0);
        $this->db->where('DisablePackage' , 0);
        $this->db->where('sl3lbf' , 0);
        $this->db->where('ArchivedPack' , 0);
        $this->db->where('ArchivedCategory' , 0);
        $this->db->where_not_in('PackageId',$strPackIds);
        $this->db->order_by('OrderBy, Category, PackOrderBy, PackageTitle');

        $result=$this->db->get();
        return $result->result();
    }


    public function get_packages($PCK_TITLE , $TIME_TAKEN ,$IMP_INFO ,$packId){
       
        $this->db->select("$PCK_TITLE, $TIME_TAKEN, $IMP_INFO , A.APIId, APIType, DuplicateIMEIsNotAllowed, IMEIFieldType, ExternalNetworkId, PackageImage, CalculatePreCodes, CustomFieldId, RedirectionURL, TOCs, ServiceType,VerifyOrders, CancelOrders");
        $this->db->from('tbl_gf_packages A');
        $this->db->join('tbl_gf_api B' , 'A.APIId = B.APIId' , 'left');
        $this->db->where('PackageId' , $packId);

        $result=$this->db->get();
        return $result->row();
    }

    public function get_theme(){
        
        $this->db->select('Theme');
        $this->db->from('tbl_gf_email_settings');
        $this->db->where('id', 1);
        
        $result=$this->db->get();
        return $result->row();
    }

    public function get_brands($packId){
      

        $this->db->distinct('A.BrandId AS Id, Brand AS Value, A.APIId');
        $this->db->from('tbl_gf_packs_models A, tbl_gf_api_brands B');
        $this->db->where('A.APIId = B.APIId');
        $this->db->where('A.BrandId = B.BrandId');
        $this->db->where('ServiceId', $packId);
        $this->db->order_by('Brand');

        $result=$this->db->get();
        return $result->result();
    }


    public function get_custom_fields($packId){
       
        $this->db->select('A.FieldId, FieldLabel, FieldType, Mandatory, FieldColName, MinLength, MaxLength, FInstructions, Restriction');
        $this->db->from('tbl_gf_package_custom_fields A, tbl_gf_custom_fields B');
        $this->db->where('A.FieldId = B.FieldId');
        $this->db->where('PackageId' , $packId);
        $this->db->where('A.ServiceType' ,0);
        $this->db->where('DisableField' , 0);
        $this->db->order_by('FieldOrderBy');

        $result = $this->db->get();
        return $result->result();

       // $objDBCD14->query("SELECT FieldId, FieldLabel, FieldType, Mandatory, FieldColName FROM tbl_gf_custom_fields WHERE ServiceType = '2' AND DisableField = 0
       //         ORDER BY FieldLabel");
    }


    public function get_fields_by_id($customFldId){
        //$objDBCD14->queryUniqueObject("SELECT FieldLabel, MinLength, MaxLength, Restriction FROM tbl_gf_custom_fields WHERE FieldId = '$customFldId'");
        
        $this->db->select('FieldLabel, MinLength, MaxLength, Restriction');
        $this->db->from('tbl_gf_custom_fields');
        $this->db->where('FieldId' , $customFldId);

        $result = $this->db->get();
        return $result->row();
    }


    public function get_models($brandId , $packId){
        //$objDBCD14->query("SELECT A.ModelId AS Id, Model AS Value FROM tbl_gf_packs_models A, tbl_gf_api_models B WHERE A.ModelId = B.ModelId AND A.BrandId = B.BrandId
        //                    AND A.BrandId = '$brandId' AND ServiceId = '$packId' ORDER BY Model");

        $this->db->select('A.ModelId AS Id, Model AS Value');
        $this->db->from('tbl_gf_packs_models A, tbl_gf_api_models B');
        $this->db->where('A.ModelId = B.ModelId');
        $this->db->where('A.BrandId = B.BrandId');
        $this->db->where('A.BrandId' , $brandId);
        $this->db->where('ServiceId' , $packId);
        $this->db->order_by('Model');

        $result = $this->db->get();
        return $result->result();
        
    }

    public function get_package_prices($USER_ID , $myNetworkId){
      
        $this->db->select('PackageId, Price');
        $this->db->from('tbl_gf_users_packages_prices');
        $this->db->where('UserId' , $USER_ID);
        $this->db->where('PackageId' , $myNetworkId);

        $result=$this->db->get();
        return $result->row();
    }

    public function get_package_prices_2($USER_ID , $myNetworkId ,$MY_CURRENCY_ID , $IMEI_TYPE){
                       
        $this->db->select('PackageId, Price');
        $this->db->from('tbl_gf_users A, tbl_gf_plans_packages_prices B');
        $this->db->where('A.PricePlanId = B.PlanId');
        $this->db->where('B.CurrencyId' ,$MY_CURRENCY_ID);
        $this->db->where('ServiceType' ,$IMEI_TYPE);
        $this->db->where('UserId' , $USER_ID);
        $this->db->where('PackageId' , $myNetworkId);

        $result=$this->db->get();
        return $result->row();
        
    }


    public function get_converstion_rate($USER_ID){
      
        $this->db->select('ConversionRate');
        $this->db->from('tbl_gf_users A, tbl_gf_currency B');
        $this->db->where('A.CurrencyId = B.CurrencyId');
        $this->db->where('UserId' , $USER_ID);
        $result = $this->db->get();

        return $result->row();

    }

    public function get_package_prices_3($USER_ID , $myNetworkId , $IMEI_TYPE){
        
        $this->db->select('PackageId, Price');
        $this->db->from('tbl_gf_users A, tbl_gf_plans_packages_prices B, tbl_gf_currency C');
        $this->db->where('A.PricePlanId = B.PlanId');
        $this->db->where('B.CurrencyId = C.CurrencyId');
        $this->db->where('DefaultCurrency = 1');
        $this->db->where('ServiceType' ,$IMEI_TYPE);
        $this->db->where('UserId' , $USER_ID);
        $this->db->where('PackageId' , $myNetworkId);

        $result = $this->db->get();
        return $result->row();
    }

    public function get_package_price_4($MY_CURRENCY_ID ,$myNetworkId){
    
        $query=$this->db->query("SELECT A.PackageId, Price, PackagePrice FROM tbl_gf_packages A LEFT JOIN tbl_gf_packages_currencies B ON 
        (A.PackageId = B.PackageId AND CurrencyId = '$MY_CURRENCY_ID') WHERE A.PackageId = '$myNetworkId'");

        $result = $query->row();
        return $result;

    }

    public function get_pincode(){
        //$objDBCD14->queryUniqueObject("SELECT PinCode FROM tbl_gf_users WHERE UserId = '".$_SESSION['GSM_FUS_UserId']."'");
        $this->db->select('PinCode');
        $this->db->from('tbl_gf_users');
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
     
        $result = $this->db->get();
        return $result->row();
    }

    public function get_package_data($myNetworkId){
       
        $this->db->select('PackageTitle, CostPrice, ServiceType, ResponseDelayTm, CronDelayTm, IMEISeries, NewOrderEmailIDs');
        $this->db->from('tbl_gf_packages A');
        $this->db->join('tbl_gf_imei_restricted_series B' , 'A.SeriesId = B.SeriesId' , 'left');
        $this->db->where('PackageId' , $myNetworkId);

        $result = $this->db->get();
        return $result->row();

    }

    public function get_api_data($myNetworkId){
        
        $this->db->select(' A.APIId, APIKey, APITitle, APIType, ServerURL, AccountId, ServiceId, APIAction, ResponseURL,ExternalNetworkId, PackageTitle');
        $this->db->from('tbl_gf_packages A, tbl_gf_api B');
        $this->db->where('A.APIId = B.APIId');
        $this->db->where('PackageId', $myNetworkId);
        $this->db->where('DisableAPI' , 0);

        $result = $this->db->get();
        return $result->row();
    }

    public function get_supplier_package($packageId,$serviceType){
   
        $this->db->select(' SupplierId, PackageId, PurchasePrice');
        $this->db->from('tbl_gf_supplier_packages');
        $this->db->where('PackageId' , $packageId);
        $this->db->where('ServiceType' ,$serviceType);

        $result = $this->db->get();
        return $result->row();
    }

    public function get_codes($myNetworkId, $STR_IMEIS){
      
        $this->db->select('CodeId, IMEINo');
        $this->db->from('tbl_gf_codes');
        $this->db->where_in('IMEINo' , $STR_IMEIS);
        $this->db->where('PackageId' ,$myNetworkId);
        $this->db->where('CheckDuplication' ,1);
        $ids = array('1','4');
        $this->db->where_in('CodeStatusId' , $ids);

        $result = $this->db->get();
        return $result->result();
    }

    public function fetch_negative_user_credits(){
   
        $this->db->select('AllowNegativeCredits, OverdraftLimit');
        $this->db->from('tbl_gf_users');
        $this->db->where('UserId' ,$this->session->userdata('GSM_FUS_UserId'));
      
        $result = $this->db->get();
        return $result->row();
    }


    public function add_codes_data($strInsertOrders , $cols_cfields){
        $query = $this->db->query("INSERT INTO tbl_gf_codes (PersonalRecord, CodeStatusId, UserId, Credits, PhoneLockedOn, IMEINo, PackageId, Comments, Comments1, 
			CountryId, RequestedAt, AlternateEmail, IP, OrderAPIId, OrderAPIName, OrderAPIURL, OrderAPIUserName, OrderAPIType, OrderAPIKey, OrderAPIServiceId, OrderIdFromServer, 
			CodeSentToOtherServer, ShowToSupplier, Payout, CalculatePreCode, SupplierId, OrderType, ResponseDelayTm, CostPrice, Profit, OrderCostPrice, CronDelayTm, 
			MobileId, ModelId, ModelNo, OrderData, OrderPlcdFrm $cols_cfields) 
            VALUES $strInsertOrders");
        
    }

    public function add_credit_history_iemi($strCreditHistory){
        $query = $this->db->query("INSERT INTO tbl_gf_credit_history (UserId, Credits, Description, IMEINo, PackageId, HistoryDtTm, CreditsLeft, Currency, IP) 
			VALUES $strCreditHistory");
    }

    public function getServiceFeatures($packId, $serviceType){
    
        $this->db->select(' Icon, Feature');
        $this->db->from('tbl_gf_service_features A, tbl_gf_pack_selected_features B ');
        $this->db->where('A.FeatureId = B.FeatureId');
        $this->db->where('ServiceId' , $packId);
        $this->db->where('ServiceType' , $serviceType);

        $result = $this->db->get();
        return $result->result();

    }


    public function update_logout_time($logoutDtTm){
        $this->db->set('LogoutTime' , $logoutDtTm);
        $this->db->where('Id' , $this->session->userdata('UserLoginId'));
        $this->db->update('tbl_gf_user_login_log');
    }


    public function get_order_codes($strWhere ,$strSortBy ,$strLimit){
        $query=$this->db->query("SELECT CodeId, IMEINo, SerialNo, Verify, CodeStatusId, Code, ReplyDtTm, RequestedAt, Credits, Comments, PackageId, 
		OrderAPIId, HiddenStatus, CodeSentToOtherServer FROM tbl_gf_codes A WHERE UserId = ".$this->session->userdata('GSM_FUS_UserId')." $strWhere $strSortBy $strLimit");
       
       return $query->result();
    }

    public function get_code_packages($codeId){
     
        $this->db->select('PackageTitle, RequestedAt, IMEINo, ReplyDtTm, OrderVerifyMins, Code, Credits');
        $this->db->from('tbl_gf_codes A, tbl_gf_packages B');
        $this->db->where('A.PackageId = B.PackageId');
        $this->db->where('CodeId' , $codeId);
        $this->db->where('VerifyOrders' , 1);

        $result = $this->db->get();
        return $result->row();
    }

    public function update_gf_codes($vData ,$ip ,$codeId){
        
        $this->db->set('Verify' , 1);
        $this->db->set('VerifyData' , $vData);
        $this->db->set('VerifyIP' , $ip);
        $this->db->where('CodeId' ,$codeId);
        $this->db->where('Verify' , 0);
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
        $this->db->update('tbl_gf_codes');


    }


    public function get_cancel_code_packages($orderId){
        
        $this->db->select('CodeId, IMEINo, PackageTitle, A.PackageId, Credits, Code, RequestedAt, AlternateEmail, OrderCancelMins');
        $this->db->from('tbl_gf_codes A, tbl_gf_packages B');
        $this->db->where('A.PackageId = B.PackageId');
        $this->db->where('CodeId' , $orderId);
        $this->db->where('UserId' ,$this->session->userdata('GSM_FUS_UserId'));
        $this->db->where('CodeStatusId' , 1);
        $this->db->where('CancelOrders' , 1);

        
        $result = $this->db->get();
        return $result->row();
    }

    public function update_cancel_codes($orderId){
        
        $this->db->set('CodeStatusId' , 3);
        $this->db->set('Refunded' , 1);
        $this->db->set('CancelledByUser' , 1);
        $this->db->set('CheckDuplication' , 0);
        $this->db->set('Code' , 'Cancelled By User');
        $this->db->where('CodeId' , $orderId);
        $this->db->where('UserId' ,$this->session->userdata('GSM_FUS_UserId'));
        $this->db->where('CodeStatusId' , 1);
        $this->db->update('tbl_gf_codes');

    }

    public function sl3lbf_packages(){
       
        $this->db->select('PackageId, PackageTitle, CancelOrders, VerifyOrders, OrderVerifyMins, OrderCancelMins');
        $this->db->from('tbl_gf_packages');
        $this->db->where('sl3lbf' , 0);

        $result=$this->db->get();
        return $result->result();

    }


    public function get_code_status(){
      
        $this->db->select('CodeStatusId, CodeStatus');
        $this->db->from('tbl_gf_code_status');
        $this->db->where('DisableCodeStatus' , 0);
        $this->db->order_by('CodeStatus');

        $result=$this->db->get();
        return $result->result();


    }


    public function get_codes_data($strWhere){
        $query = $this->db->query("SELECT IMEINo, Code, ModelNo, RequestedAt, PackageTitle, Comments, C.CodeStatus, Credits FROM tbl_gf_codes A, tbl_gf_packages B,
        tbl_gf_code_status C WHERE A.PackageId = B.PackageId AND A.CodeStatusId = C.CodeStatusId AND UserId = ".$this->session->userdata('GSM_FUS_UserId')." $strWhere 
        ORDER BY CodeId DESC");

        

        return $query->result();
    }

    public function update_download($ids){
     
        $this->db->set('Downloaded' , 1);
        $this->db->where_in('CodeId' , $ids);
        $this->db->update('tbl_gf_codes');

    }

    public function get_field_col(){
      
        $this->db->select('FieldColName');
        $this->db->from('tbl_gf_custom_fields');
        $this->db->where('ServiceType' , 0);
        $this->db->order_by('FieldLabel');

        $result = $this->db->get();
        return $result->result();

    }

    public function fetch_code_package_by_ids($strCustomCols , $ids){
        $query = $this->db->query("SELECT A.PackageId, PackageTitle, IMEINo, Code $strCustomCols FROM tbl_gf_codes A, tbl_gf_packages B WHERE A.PackageId = B.PackageId AND CodeId IN ($ids)
        ORDER BY PackOrderBy, PackageTitle, CodeId DESC");
        
        return $query->result();
    }


    public function get_code_data($id){
      
        $this->db->select(' A.* , PackageTitle ');
        $this->db->from('tbl_gf_codes A, tbl_gf_packages B');
        $this->db->where('A.PackageId = B.PackageId');
        $this->db->where('CodeId' , $id);

        $result = $this->db->get();
        return $result->row();
    }

    public function get_user_data(){
        $this->db->select('CurrencyAbb');
        $this->db->from('tbl_gf_users A, tbl_gf_currency B');
        $this->db->where('A.CurrencyId = B.CurrencyId');
        $this->db->where('UserId' ,$this->session->userdata('GSM_FUS_UserId'));

        $result= $this->db->get();
        return $result->row();
    }

    public function get_custom_field(){
      
        $this->db->select('FieldId, FieldLabel, FieldType, Mandatory, FieldColName');
        $this->db->from('tbl_gf_custom_fields');
        $this->db->where('ServiceType' , 0);
        $this->db->where('DisableField' , 0);

        $this->db->query("SELECT FieldId, FieldLabel, FieldType, Mandatory, FieldColName FROM tbl_gf_custom_fields WHERE ServiceType = '2' AND DisableField = 0 
                ORDER BY FieldLabel");
        $result= $this->db->get();
        return $result->result();
    }
    
    public function get_blocked_ips(){
    
        $this->db->select('BlockedPMethodIds');
        $this->db->from('tbl_gf_users A, tbl_gf_countries B');
        $this->db->where('A.CountryId = B.CountryId');
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));

        $result = $this->db->get();
        return $result->row();

    }

    public function get_payment_details_3($strBlockedPIds){
        $query=$this->db->query("SELECT PaymentMethodId, PaymentMethod, Fee, Vat, Description, PayMethodTypeId, PayImage FROM tbl_gf_payment_methods WHERE DisablePaymentMethod = 0 
				AND ForWholeSale = '1' AND PaymentMethodId NOT IN (SELECT PaymentMethodId FROM tbl_gf_user_payment_methods WHERE UserId = '".$this->session->userdata('GSM_FUS_UserId')."') 
                $strBlockedPIds	ORDER BY OrderBy");
        return $query->result();
    }

    public function get_payment_desc($pId){
    
        $this->db->select('Description');
        $this->db->from('tbl_gf_payment_methods');
        $this->db->where('PaymentMethodId' , $pId);

        $result = $this->db->get();
        return $result->row();

    }

    public function get_min_max_credits(){
       
        $this->db->select('MinCredits, MaxCredits');
        $this->db->from('tbl_gf_email_settings');
        $this->db->where('Id' , 1);
        $result = $this->db->get();
        return $result->row();
    }

    public function get_payment_and_method_detail($paymentMethod){
      
        $this->db->select('PaymentMethodId, Username, Fee, PayMethodTypeId, APIUsername, APIPassword, APISignature, PaymentMethod, Vat, CurrencyAbb, RestrictedCurrency');
        $this->db->from('tbl_gf_payment_methods A');
        $this->db->join('tbl_gf_currency B' , 'A.RestrictedCurrency = B.CurrencyId' , 'left');
        $this->db->where('PaymentMethodId' , $paymentMethod);
        $this->db->where('DisablePaymentMethod' , 0);

        $result = $this->db->get();
        return $result->row();
    }

    public function get_user_email_settings(){
     
        $this->db->select('FromName, SiteURL');
        $this->db->from('tbl_gf_email_settings');
        
        $result = $this->db->get();
        return $result->row();
    }

    public function insert_payment_details($insert_array){
        $this->db->insert('tbl_gf_payments' , $insert_array);
        return $this->db->insert_id();
    }

    public function get_autocredits(){
       
        $this->db->select('AutoFillCredits');
        $this->db->from('tbl_gf_users');
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));

        $result = $this->db->get();
        return $result->row();

    }


    public function get_payments_and_method($paymentId){
        
        $this->db->select('Credits, UserId, Amount, UserName, Currency, DATE(PaymentDtTm) AS PaymentDtTm, B.PaymentMethod, B.Username');
        $this->db->from('tbl_gf_payments A, tbl_gf_payment_methods B');
        $this->db->where('A.PaymentMethod = B.PaymentMethodId');
        $this->db->where('PaymentId' ,$paymentId);
        $this->db->where('PayMethodTypeId' , 1);
        $this->db->where('PaymentStatus' , 1);

        $result = $this->db->get();
        return $result->row();


    }

    public function get_user_details($userId){
      
        $this->db->select("UserEmail, UserName, CONCAT(FirstName, ' ', LastName) AS Name, Credits, AutoFillCredits");
        $this->db->from('tbl_gf_users');
        $this->db->where('UserId' , $userId);

        $result = $this->db->get();
        $result->row();
      
    }

    public function update_gf_payments($update_data,$paymentId){
        $this->db->where('PaymentId', $paymentId);
        $this->db->update('tbl_gf_payments', $update_data);
    }

    public function get_payer_email($userId , $paymentId){
                           
        $this->db->select('PayerEmail');
        $this->db->from('tbl_gf_payments A, tbl_gf_payment_methods B');
        $this->db->where('A.PaymentMethod = B.PaymentMethodId');
        $this->db->where('PayMethodTypeId' , 1);
        $this->db->where('PaymentId <' , $paymentId);
        $this->db->where('PaymentStatus' , 2);
        $this->db->where('UserId' , $userId);
        $this->db->where('PayerEmail !=' , '');

        $result=$this->db->get();
        return $result->row();
    }
    public function add_update_payment_details($insert_data , $payableAmount , $id){ 
        
        $this->db->insert('tbl_gf_payment_details' , $insert_data);

        
        $this->db->set('PayableAmount' , $payableAmount);
        $this->db->where('PaymentId' , $id);
        $this->db->update('tbl_gf_payments');
    }

    public function fetch_user_row($id){
       
        $this->db->select("A.UserId, A.Credits, PaymentMethod, PayableAmount, PaymentStatus, TransactionId, A.Comments, CreditsTransferred, B.UserName, 
        ByAdmin, PaidDtTm, Currency, PayerEmail");
        $this->db->from('tbl_gf_payments A, tbl_gf_users B');
        $this->db->where('A.UserId = B.UserId');
        $this->db->where('PaymentId' , $id);

        $result = $this->db->get();
        return $result->row();

    }

    public function get_user_payments($paymentId){
        
        $this->db->select(' Amount, A.UserId, UserEmail, B.UserName, Amount, Currency, A.Credits, DATE(PaymentDtTm) AS PaymentDtTm');
        $this->db->from('tbl_gf_payments A, tbl_gf_users B');
        $this->db->where('A.UserId = B.UserId');
        $this->db->where('PaymentId' , $paymentId);
        $this->db->where('PaymentStatus' , 1);

        $result = $this->db->get();
        return $result->row();

    }

    public function get_payments_detail($id){
       
        $this->db->select(' PaymentId, UserId, Credits, B.PaymentMethod AS PaymentMode, Amount, PaymentStatus, PayerEmail, TransactionId, PayMethodTypeId, 
        Comments, A.Vat, DATE(PaymentDtTm) AS InvoiceDate, DATE(PaidDtTm) AS PaidDtTm, Currency, ByAdmin, PayableAmount, PaymentMethodId');
        $this->db->from('tbl_gf_payments A');
        $this->db->join('tbl_gf_payment_methods B' , 'A.PaymentMethod = B.PaymentMethodId' , 'left');
        $this->db->where('PaymentId' , $id);
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));

        $result=$this->db->get();
        return $result->row();
    }

    public function get_alluser_data(){
        
        $this->db->select(' UserId, UserEmail, FirstName, LastName, Phone, Address, Country, City, State, Zip');
        $this->db->from('tbl_gf_users A, tbl_gf_countries B');
        $this->db->where('A.CountryId = B.CountryId');
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
        
        $result=$this->db->get();
        return $result->row();
    }

    public function get_logo_path(){
   
        $this->db->select('LogoPath');
        $this->db->from('tbl_gf_logo');
        $this->db->where('DisableLogo' , 0);
        $this->db->where('LogoId' , 4);

        $result=$this->db->get();
        return $result->row();
    }

    public function get_sum_invamount($id){
     
        $this->db->select('SUM(InvAmount) AS AmountPaid');
        $this->db->from('tbl_gf_payment_details');
        $this->db->where('InvoiceId' , $id);
        $this->db->group_by('InvoiceId');

        $result=$this->db->get();
        return $result->row();

    }

    public function get_payments_count($id){
       
        $this->db->select('InvAmount, InvTransactionId, DATE(InvDtTm) AS InvDtTm, InvComments, PaymentMethod');
        $this->db->from('tbl_gf_payment_details A, tbl_gf_payment_methods B');
        $this->db->where('A.InvPaymentMethodId = B.PaymentMethodId');
        $this->db->where('InvoiceId' , $id);
        $this->db->order_by('InvDtlId');

        $result = $this->db->get();
        return $result->result();
    } 

    public function get_reciever_info(){
        $this->db->select('ReceiverId, Username');
        $this->db->from('tbl_gf_pp_receivers');
        $this->db->where('DisablePP' , 0);
        $this->db->order_by('Username');
        
        $result = $this->db->get();
        return $result->result();
    }

    public function get_api_password($pMId){
     
        $this->db->select('APIPassword');
        $this->db->from('tbl_gf_payment_methods');
        $this->db->where('PaymentMethodId' , $pMId);

        $result = $this->db->get();
        return $result->row();
    }


    public function get_apiusername($pMId){
       
        $this->db->select('APIUserName');
        $this->db->from('tbl_gf_payment_methods');
        $this->db->where('PaymentMethodId' , $pMId);
        $result = $this->db->get();
        return $result->row();

    }

    public function get_payment_method_by_type_id($invId){
        
        $this->db->select('Credits, UserId, Amount, UserName, Currency, DATE(PaymentDtTm) AS PaymentDtTm, B.PaymentMethod, B.Username');
        $this->db->from('tbl_gf_payments A, tbl_gf_payment_methods B');
        $this->db->where('A.PaymentMethod = B.PaymentMethodId');
        $this->db->where('PaymentId' , $invId);
        $this->db->where('PayMethodTypeId' , 11);
        $this->db->where('PaymentStatus' , 1);

        $result = $this->db->get();
        return $result->row();
    }

    public function get_payment_on_status($invId){
        
        $this->db->select('PaymentId, Credits, Amount, Currency, DATE(PaymentDtTm) AS PaymentDtTm, A.PaymentMethod AS PaymentMethodId, B.PaymentMethod');
        $this->db->from('tbl_gf_payments A, tbl_gf_payment_methods B');
        $this->db->where('A.PaymentMethod = B.PaymentMethodId');
        $this->db->where('PaymentId' , $invId);
        $this->db->where('PaymentStatus', 1);

        $result=$this->db->get();
        return $result->row();
       
        }

    public function get_api_data_by_payment_id($PAYMENT_METHOD_ID){
      
        $this->db->select('APIUsername, APIPassword, APISignature');
        $this->db->from('tbl_gf_payment_methods');
        $this->db->where('PaymentMethodId' , $PAYMENT_METHOD_ID);

        $result = $this->db->get();
        return $result->row();

    }

    public function get_autofillcredits(){
       
        $this->db->select('Credits, AutoFillCredits');
        $this->db->from('tbl_gf_users');
        $this->db->where('UserId',$this->session->userdata('GSM_FUS_UserId'));

        $result = $this->db->get();
        return $result->row();
    }

    public function get_curreny_data($rstrctdCurrId){
      
        $this->db->select('CurrencyAbb, ConversionRate');
        $this->db->from('tbl_gf_currency');
        $this->db->where('CurrencyId' , $rstrctdCurrId);

        $result=$this->db->get();
        return $result->row();
    }

    public function get_credits_on_paymentstatus($paymentId){
    
        $this->db->select('Credits, UserId, Amount, Currency, DATE(PaymentDtTm) AS PaymentDtTm');
        $this->db->from('tbl_gf_payments');
        $this->db->where('PaymentId' , $paymentId);
        $this->db->where('PaymentStatus', 1);

        $result=$this->db->get();
        return $result->row();

    }

    public function get_payment_method(){
       
        $this->db->select('PaymentMethodId AS Id, PaymentMethod AS Value');
        $this->db->from('tbl_gf_payment_methods');
        $this->db->where('DisablePaymentMethod' , 0);
        $this->db->order_by('PaymentMethod');

        $result=$this->db->get();
        return $result->result();

        }

    public function get_payment_status(){
       
        $this->db->select('PaymentStatusId AS Id, PaymentStatus AS Value');
        $this->db->from('tbl_gf_payment_methods');
        $this->db->where('DisablePaymentStatus' , 0);
        $this->db->order_by('PaymentStatus');

        $result=$this->db->get();
        return $result->result();

    }
    public function get_payment_methods_and_detail(){
      
        $this->db->select('InvAmount, InvTransactionId, InvDtTm, InvComments, PaymentMethod');
        $this->db->from('tbl_gf_payment_details A , tbl_gf_payment_methods B');
        $this->db->where('A.InvPaymentMethodId = B.PaymentMethodId');
        $this->db->where('InvoiceId' , $id);
        $this->db->order_by('InvDtlId');

        $result=$this->db->get();
        return $result->result();
    }

    public function delete_user_login_countries($id){
        $this->db->delete('user_login_countries', array('ISO' => $id , 'UserId' => $this->session->userdata('GSM_FUS_UserId')));
    }

    public function insert_user_country_login($iso){
       
        $this->db->set('UserId' , $this->session->userdata('GSM_FUS_UserId') );
        $this->db->set('ISO' , $iso);
        $this->db->insert('tbl_gf_user_login_countries');
    }

    public function get_countries(){
       
        $this->db->select('A.ISO, Country');
        $this->db->from('tbl_gf_user_login_countries A, tbl_gf_countries B');
        $this->db->where('A.ISO = B.ISO');
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
        $this->db->order_by('Country');
        
        $result=$this->db->get();
        return $result->result();
    }

    public function get_all_countries(){
        
        $this->db->select('ISO AS Id, Country AS Value');
        $this->db->from('tbl_gf_countries');
        $this->db->where('DisableCountry' , 0);
        $this->db->order_by('Country');
        
        $result=$this->db->get();
        return $result->result();
    }

    public function get_user_pincode(){
        
        $this->db->select('PinCode');
        $this->db->from('tbl_gf_users');
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
        
        $result=$this->db->get();
        return $result->row();

    }

    public function fetch_package_by_id($myNetworkId){
        $this->db->select('PackageTitle, CostPrice, ServiceType, ResponseDelayTm, CronDelayTm, NewOrderEmailIDs');
        $this->db->from('tbl_gf_packages');
        $this->db->where('PackageId' , $myNetworkId);
         
        $result=$this->db->get();
        return $result->row();
    }

    public function get_packages_api_by_id($myNetworkId){
        
         $this->db->select('A.APIId, APIKey, APIType, ServerURL, APITitle, AccountId, ExternalNetworkId, APIKey2, APIPassword');
         $this->db->from('tbl_gf_packages A, tbl_gf_api B');
         $this->db->where('A.APIId = B.APIId');
         $this->db->where('A.PackageId' , $myNetworkId);
         $this->db->where('DisableAPI' , 0);
          
         $result=$this->db->get();
         return $result->row();
    }

    public function get_dupImeis($myNetworkId){
       
        $this->db->select('DuplicateIMEIsNotAllowed');
        $this->db->from('tbl_gf_packages');
        $this->db->where('PackageId' , $myNetworkId);
       
        $result=$this->db->get();
        return $result->row();
    }

    public function get_negative_credits(){
       
        $this->db->select('AllowNegativeCredits, OverdraftLimit');
        $this->db->from('tbl_gf_users');
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
       
        $result=$this->db->get();
        return $result->row();
    }

    public function insert_codes_slbf_data($hash, $packagePrice ,$imei, $file , $myNetworkId , $notes , $comments , $currDtTm , $altEmail , $apiId , $serverURL, $accountId, $apiName, $apiType , $apiKey, $extNwkId , $fileContents ,$SERVICE_TYPE , $DELAY_TIME , $packCostPrice , $profit , $CRON_DELAY_TIME , $packPriceToDefaultCurr ,$apiKey2 ,$apiPwd,$strInsert){
        
        $query=$this->db->query("INSERT INTO tbl_gf_codes_slbf SET HashValue = '$hash', CodeStatusId = '1', UserId = ".$this->session->userdata('GSM_FUS_UserId').",
		Credits = '$packagePrice', IMEINo = '$imei', CodeFile = '$file', PackageId = '$myNetworkId', Comments = '$notes', Comments1 = '$comments', RequestedAt = '$currDtTm',
		AlternateEmail = '$altEmail', IP = '".$this->session->userdata('CLIENT_GF_IP')."', OrderAPIId = '$apiId', OrderAPIURL = '$serverURL', OrderAPIUserName = '$accountId', 
		OrderAPIName = '$apiName', OrderAPIType = '$apiType', OrderAPIKey = '$apiKey', OrderAPIServiceId = '$extNwkId', OrderIdFromServer = 0, CodeSentToOtherServer = 0,
		FileContents = '$fileContents', OrderType = '$SERVICE_TYPE', ResponseDelayTm = '$DELAY_TIME', CostPrice = '$packCostPrice', Profit = '$profit', 
        CronDelayTm = '$CRON_DELAY_TIME', OrderCostPrice = '$packPriceToDefaultCurr', OrderAPIKey2 = '$apiKey2', OrderAPIPassword = '$apiPwd' $strInsert") ;
        
        $myID = $this->db->insert_id();
        return $myID;

    }

    public function update_userdata($encryptedCredits){
        
        $this->db->set('Credits' , $encryptedCredits);
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
        $this->db->update('tbl_gf_users');
    }

    public function get_shortdescription(){
       
        $this->db->select('ShortDescription');
        $this->db->from('tbl_gf_news');
        $this->db->where('DisableNews' , 0);
        $this->db->where('AtFile' , 1);
        $this->db->order_by('NewsId' , 'DESC');

        $result = $this->db->get();
        return $result->result();

    }

    public function get_packages_data($packId){
       
        $this->db->select('PackageTitle, TimeTaken, MustRead, RedirectionURL, ServiceType, VerifyOrders, CancelOrders');
        $this->db->from('tbl_gf_packages');
        $this->db->where('PackageId' , $packId);
        
        $result = $this->db->get();
        return $result->row();
    }

    public function get_api_and_packages($logPackageId){
        return  $this->db->query("SELECT A.APIId, APIKey, APIType, ServerURL, AccountId, ServiceId, APIAction, ResponseURL, APITitle, ExternalNetworkId, LogPackageTitle, ServiceTypeId FROM tbl_gf_log_packages A LEFT JOIN tbl_gf_api B ON (A.APIId = B.APIId AND DisableAPI = 0) WHERE LogPackageId = '$logPackageId'")->row();
        
    }

    public function get_log_packages($logPackageId){
       
        $this->db->select('LogPackageTitle, CostPrice, ServiceType, ResponseDelayTm, CronDelayTm, NewOrderEmailIDs');
        $this->db->from('tbl_gf_log_packages');
        $this->db->where('LogPackageId' , $logPackageId);
        
        $result = $this->db->get();
        return $result->row();
    }
   
    public function get_users_log_packages($USER_ID ,$logPackageId){
       
        $this->db->select('LogPackageId, Price');
        $this->db->from('tbl_gf_users_log_packages_prices');
        $this->db->where('UserId', $USER_ID);
        $this->db->where('LogPackageId', $logPackageId);

        $result = $this->db->get();
        return $result->row();

    }

    public function get_users_plans_packages($USER_ID , $logPackageId , $MY_CURRENCY_ID){
        
        $this->db->select('PackageId, Price');
        $this->db->from('tbl_gf_users A, tbl_gf_plans_packages_prices B');
        $this->db->where('A.PricePlanId = B.PlanId');
        $this->db->where('B.CurrencyId' , $MY_CURRENCY_ID);
        $this->db->where('ServiceType' , 2);
        $this->db->where('PackageId' , $logPackageId);
        $this->db->where('UserId' , $USER_ID);

        $result = $this->db->get();
        return $result->row();
    }

    public function get_user_packageprices_and_currency($USER_ID , $logPackageId){
       
        $this->db->select('PackageId, Price');
        $this->db->from('tbl_gf_users A, tbl_gf_plans_packages_prices B, tbl_gf_currency C');
        $this->db->where('A.PricePlanId = B.PlanId');
        $this->db->where('B.CurrencyId = C.CurrencyId');
        $this->db->where('DefaultCurrency' , 1);
        $this->db->where('UserId' , $USER_ID);
        $this->db->where('ServiceType' , 2);
        $this->db->where('PackageId' , $logPackageId);

        $result = $this->db->get();
        return $result->row();
    }

    public function get_packages_and_curencies($MY_CURRENCY_ID , $myNetworkId){
        return  $this->db->query("SELECT A.PackageId, Price, PackagePrice FROM tbl_gf_packages A LEFT JOIN tbl_gf_packages_currencies B ON 
                        (A.PackageId = B.PackageId AND CurrencyId = '$MY_CURRENCY_ID') WHERE A.PackageId = '$myNetworkId'")->row();
         
    }

    public function get_user_priceplan($userId){
      
        $this->db->select('PricePlanId');
        $this->db->from('tbl_gf_users');
        $this->db->where('UserId', $userId);

        $result = $this->db->get();
        return $result->row();
    }

    public function get_bulk_prices($packId ,$PricePlanId , $quantity){
    
        $this->db->select('Price');
        $this->db->from('tbl_gf_server_bulk_group_prices');
        $this->db->where('ServiceId', $packId);
        $this->db->where('GroupId', $PricePlanId);
        $this->db->where('MinQty <=', $quantity);
        $this->db->where('MaxQty >=', $quantity);

        $result = $this->db->get();
        return $result->row();
    }

    public function get_bulk_price_by_maxQty($packId , $PricePlanId , $quantity){
       
        $this->db->select('Price');
        $this->db->from('tbl_gf_server_bulk_group_prices');
        $this->db->where('ServiceId', $packId);
        $this->db->where('GroupId', $PricePlanId);
        $this->db->where('MaxQty <', $quantity);
        $this->db->order_by('Id' , 'DESC');

        $result = $this->db->get();
        return $result->row();
    }
    
    public function get_bulk_price_by_max_min_Qty($packId, $quantity){
     
        $this->db->select('Price');
        $this->db->from('tbl_gf_server_bulk_prices');
        $this->db->where('ServiceId' , $packId);
        $this->db->where('MinQty <=', $quantity);
        $this->db->where('MaxQty >=', $quantity);

        $result = $this->db->get();
        return $result->row();
    }


    public function get_bulk_price_without_group_id($packId, $quantity){
      
        $this->db->select('Price');
        $this->db->from('tbl_gf_server_bulk_prices');
        $this->db->where('ServiceId' , $packId);
        $this->db->where('MaxQty <', $quantity);

        $result = $this->db->get();
        return $result->row();
    }

    public function insert_log_data($serial , $boxUN , $uName , $packagePrice ,$logPackageId,$notes ,$comments,$currDtTm, $altEmail,$apiId,$serverURL, $apiName, $historyData , $accountId, $apiType ,$apiKey,$extNwkId,$PRE_ORDER,$packCostPrice,$profit,$SERVICE_TYPE,$DELAY_TIME,$packPriceToDefaultCurr,$CRON_DELAY_TIME,$SERVICE_TYPE_ID,$strInsert,$col_val){
        
    $this->db->insert("INSERT INTO tbl_gf_log_requests SET SerialNo = '$serial', BoxUserName = '$boxUN', SUsername = '$uName', StatusId = '1', 
	UserId = ".$this->session->userdata('GSM_FUS_UserId').", Credits = '$packagePrice', LogPackageId = '$logPackageId', Comments = '$notes', Comments1 = '$comments', 
	RequestedAt = '$currDtTm', AlternateEmail = '$altEmail', IP = '".$this->session->userdata('CLIENT_GF_IP')."', OrderAPIId = '$apiId', OrderAPIURL = '$serverURL', 
	OrderAPIName = '$apiName', OrderData = '$historyData', OrderPlcdFrm = '".$this->session->userdata('ORDERED_FROM')."', 
	OrderAPIUserName = '$accountId', OrderAPIType = '$apiType', OrderAPIKey = '$apiKey', OrderAPIServiceId = '$extNwkId', OrderIdFromServer = '0', 
	CodeSentToOtherServer = '0', CalculatePreCode = '$PRE_ORDER', CostPrice = '$packCostPrice', Profit = '$profit', OrderType = '$SERVICE_TYPE', 
	ResponseDelayTm = '$DELAY_TIME', OrderCostPrice = '$packPriceToDefaultCurr', CronDelayTm = '$CRON_DELAY_TIME', OrderServiceTypeId = '$SERVICE_TYPE_ID' 
    $strInsert $col_val");
    
    $myID=$this->db->insert_id();
    return $myID ;

    }

    public function get_ShortDescription_by_server(){
     
        $this->db->select('ShortDescription');
        $this->db->from('tbl_gf_news');
        $this->db->where('DisableNews' , 0);
        $this->db->where('AtServer', 1);
        $this->db->order_by('NewsId' , 'DESC');

        $result = $this->db->get();
        return $result->result();
    }

    public function get_rs_log_packages($packId){
         
        $this->db->select('LogPackageDetail, LogPackageTitle, DeliveryTime, CalculatePreCodes, RedirectionURL, ServiceType, VerifyOrders, CancelOrders, MinimumQty');
        $this->db->from('tbl_gf_log_packages');
        $this->db->where('LogPackageId' , $packId);
       
        $result = $this->db->get();
        return $result->row();
    }

    public function get_package_custom_field($packId){
      
        $this->db->select('A.FieldId, FieldLabel, FieldType, Mandatory, FieldColName, MaxLength, FInstructions, UseAsQuantity, Restriction');
        $this->db->from('tbl_gf_package_custom_fields A, tbl_gf_custom_fields B');
        $this->db->where('A.FieldId = B.FieldId');
        $this->db->where('PackageId' , $packId);
        $this->db->where('A.ServiceType' , 2);
        $this->db->where('DisableField' , 0);
        $this->db->order_by('FieldOrderBy');

        $result = $this->db->get();
        return $result->result();
    }


    public function get_regvalue($FieldId){
     
        $this->db->select('RegValue');
        $this->db->from('tbl_gf_custom_field_values');
        $this->db->where('DisableRegValue' , 0);
        $this->db->where('FieldId' , $FieldId);
        $this->db->order_by('RegValue');
        
        $result = $this->db->get();
        return $result->result();
      
    }

    public function get_min_max_prices_by_group_id($packId ,$groupId){
       
        $this->db->select('MinQty, MaxQty, Price');
        $this->db->from('tbl_gf_server_bulk_group_prices');
        $this->db->where('ServiceId' , $packId);
        $this->db->where('GroupId' , $groupId);
        $this->db->order_by('Id');
        
        $result = $this->db->get();
        return $result->result();
    }

    public function get_min_max_prices_by_service_id($packId){
        
        $this->db->select('MinQty, MaxQty, Price');
        $this->db->from('tbl_gf_server_bulk_group_prices');
        $this->db->where('ServiceId' , $packId);
        $this->db->order_by('Id');
        
        $result = $this->db->get();
        return $result->result();
    }

    public function get_slbf_code_by_id($codeId){
       
        $this->db->select('PackageTitle, RequestedAt, IMEINo, ReplyDtTm, OrderVerifyMins, Code, Credits');
        $this->db->from('tbl_gf_codes_slbf A, tbl_gf_packages B');
        $this->db->where('A.PackageId = B.PackageId');
        $this->db->where('CodeId' , $codeId);
        $this->db->where('VerifyOrders' , 1);
       
        $result = $this->db->get();
        return $result->row();
        
    }

    public function update_slbf_codes($ip , $codeId){
       
        $this->db->set('Verify', 1);
        $this->db->set('VerifyIP' , $ip);
        $this->db->where('CodeId' , $codeId);
        $this->db->where('verify' , 0);
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
        $this->db->update('tbl_gf_codes_slbf');

        
        
    }

    public function get_slbf_packages($orderId){
        
        $this->db->select('CodeId, IMEINo, PackageTitle, A.PackageId, Credits, Code, RequestedAt, AlternateEmail, OrderCancelMins');
        $this->db->from(' tbl_gf_codes_slbf A, tbl_gf_packages B');
        $this->db->where('A.PackageId = B.PackageId');
        $this->db->where('CodeId' , $orderId);
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
        $this->db->where('CodeStatusId' , 1);
        $this->db->where('CancelOrders' , 1);

        $result = $this->db->get();
        return $result->row();
    }

    public function update_slbf_codes_by_status($data , $orderId){
       
        $this->db->where('CodeId' , $orderId);
        $this->db->where('CodeStatusId' , 1);
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
        $this->db->update('tbl_gf_codes_slbf' , $data);
    }

    public function get_slbf_packages_and_code_data($strWhere ,$strPckWhere, $strSortBy , $eu, $limit){
        $result=$this->db->query("SELECT CodeId, APIId, CodeFile, Code, IMEINo, Verify, A.CodeStatusId, PackageTitle, Credits, CodeStatus, ReplyDtTm, CancelOrders, ReplyFile, 
		VerifyOrders, OrderVerifyMins, Comments, RequestedAt, OrderCancelMins, OrderAPIId, HiddenStatus, CodeSentToOtherServer FROM tbl_gf_codes_slbf A, tbl_gf_packages B,
		tbl_gf_code_status C WHERE A.PackageId = B.PackageId AND A.CodeStatusId = C.CodeStatusId AND Archived = 0 AND UserId = '".$_SESSION['GSM_FUS_UserId']."' 
        $strWhere $strPckWhere $strSortBy LIMIT $eu, $limit")->result();

        return $result;
    }

    public function get_slbf_and_packages($id){ 
          $this->db->select('IMEINo, Code, A.Credits, A.Comments1, A.Comments, CodeStatusId, PackageTitle, ReplyDtTm, RequestedAt');
          $this->db->from(' tbl_gf_codes_slbf A, tbl_gf_packages B');
          $this->db->where(' A.PackageId = B.PackageId');
          $this->db->where('CodeId' , $id);
  
          $result = $this->db->get();
          return $result->row();
    }

    public function get_currency_by_id(){
          $this->db->select('CurrencyAbb');
          $this->db->from('tbl_gf_users A, tbl_gf_currency B');
          $this->db->where('A.CurrencyId = B.CurrencyId');
          $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
         
         
          $result = $this->db->get();
          return $result->row();
    }

    public function get_replyfile($id){
        $this->db->select('ReplyFile');
        $this->db->where('CodeId' , $id);
        $this->db->from('tbl_gf_codes_slbf');
        
        $result = $this->db->get();
        return $result->row();
        
    }

    public function update_gf_codes_by_code_id($codeId){
    
        $this->db->set('LastUpdatedBy' , $this->session->userdata('AdminUserName'));
        $this->db->set('Verify' , 2);
        $this->db->where('CodeId' , $codeId);
        $this->db->update('tbl_gf_codes');
    }

    public function get_codes_packages_and_userdata($strWhere , $eu , $limit){
        $result=$this->db->query("SELECT A.PackageId, CodeId, A.Credits, Code, IMEINo, RequestedAt, Mep, SerialNo, OtherValue, PackageTitle, Verify, A.UserId, UserName, VerifyData 
		FROM tbl_gf_codes A, tbl_gf_packages B, tbl_gf_users D WHERE A.PackageId = B.PackageId AND A.UserId = D.UserId AND Verify = 1 $strWhere ORDER BY CodeId DESC LIMIT $eu, $limit")->result();
       
       return $result ;
     
    }

    public function get_codes_user_count($strWhere){
        return $this->db->query("SELECT COUNT(CodeId) AS TotalRecs FROM tbl_gf_codes A, tbl_gf_users D WHERE A.UserId = D.UserId AND Verify = 1 $strWhere")->row();
    }

    public function fetch_requests_and_packages($codeId){

        $this->db->select('LogPackageTitle, RequestedAt, ReplyDtTm, OrderVerifyMins, Code, Credits');
        $this->db->from('tbl_gf_log_requests A, tbl_gf_log_packages B');
        $this->db->where('A.LogPackageId = B.LogPackageId');
        $this->db->where('LogRequestId' , $codeId);
        $this->db->where('VerifyOrders' ,1);

        $result=$this->db->get();
        return $result->result();


    }

    public function update_log_request($codeId , $ip){
        $this->db->set('Verify' , 1);
        $this->db->set('VerifyIP' , $ip);
        $this->db->where('LogRequestId' , $codeId);
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
        $this->db->update('tbl_gf_log_requests');

    }

    public function fetch_log_requests_and_packages($orderId){
     
        
        $this->db->select(' LogRequestId, LogPackageTitle, A.LogPackageId, Credits, Code, RequestedAt, AlternateEmail, OrderData, OrderCancelMins');
        $this->db->from('tbl_gf_log_requests A, tbl_gf_log_packages B');
        $this->db->where('A.LogPackageId = B.LogPackageId');
        $this->db->where('LogRequestId' , $orderId);
        $this->db->where('StatusId' ,1);
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
        $this->db->where('CancelOrders' ,1);


        $result=$this->db->get();
        return $result->row();
    }

    public function update_log_request_by_orderID($orderId){
        
        $this->db->set('StatusId' , 3);
        $this->db->set('Refunded' , 1);
        $this->db->set('CancelledByUser' , 1);
        $this->db->set('Code' , 'Cancelled By User');
        $this->db->where('LogRequestId' , $orderId);
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
        $this->db->where('StatusId' ,1);
        $this->db->update('tbl_gf_log_requests');
    }


    public function get_fields_data(){
       

        $this->db->select('FieldLabel, FieldColName');
        $this->db->from('tbl_gf_custom_fields');
        $this->db->where('ServiceType' , 2);
        $this->db->order_by('FieldLabel');
        $result=$this->db->get();
        return $result->result();

    } 

    public function get_log_request_packages_status($strWhere  , $strSortBy , $strLimit , $strCustomCols){
        $result=$this->db->query("SELECT Verify, StatusId, LogRequestId, Code, LogPackageTitle, CodeStatus, ReplyDtTm, CancelOrders, VerifyOrders, Comments, OrderVerifyMins,
        OrderCancelMins, OrderAPIId, HiddenStatus, CodeSentToOtherServer, RequestedAt $strCustomCols FROM tbl_gf_log_requests A, tbl_gf_log_packages B, tbl_gf_code_status C 
        WHERE A.LogPackageId = B.LogPackageId AND A.StatusId = C.CodeStatusId	AND UserId = ".$this->session->userdata('GSM_FUS_UserId')." $strWhere $strSortBy $strLimit")->result();
        
        return $result ;
    }

    public function get_log_request_and_packages($id){
        
        $this->db->select('A.*, LogPackageTitle');
        $this->db->from('tbl_gf_log_requests A, tbl_gf_log_packages B');
        $this->db->where('A.LogPackageId = B.LogPackageId');
        $this->db->where('LogRequestId' , $id);
        $result=$this->db->get();
        return $result->row();
    }

    public function get_custom_field_by_servicetype(){
    
        $this->db->select('FieldId, FieldLabel, FieldType, Mandatory, FieldColName');
        $this->db->from('tbl_gf_custom_fields');
        $this->db->where('ServiceType' ,2);
        $this->db->where('DisableField' , 0);
        $this->db->order_by('FieldLabel');

        $result=$this->db->get();
        return $result->result();
    }

    public function get_code_status1(){
       
        $this->db->select('CodeStatusId AS Id, CodeStatus AS Value');
        $this->db->from('tbl_gf_code_status');
        $this->db->where('DisableCodeStatus' , 0);
        $this->db->order_by('CodeStatus');

        $result=$this->db->get();
        return $result->result();

    }

    public function get_log_request_packages_status_by_ids($strWhere){
        $result = $this->db->query("SELECT Code, SUsername, SerialNo, BoxUserName, RequestedAt, LogPackageTitle, C.CodeStatus, Credits FROM tbl_gf_log_requests A, 
        tbl_gf_log_packages B, tbl_gf_code_status C WHERE A.LogPackageId = B.LogPackageId AND A.StatusId = C.CodeStatusId AND 
        UserId = ".$this->session->userdata('GSM_FUS_UserId')." $strWhere ORDER BY LogRequestId DESC")->result();

        return $result;

    }


    public function get_userpassword(){
      
        $this->db->select('UserPassword');
        $this->db->from('tbl_gf_users');
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));
      
        $result=$this->db->get();
        return $result->row();
    }

    public function update_userpassword($encryptedPass,$dtTm){
      
        $this->db->set('UserPassword' ,  $encryptedPass);
        $this->db->set('UpdatedAt' ,  $dtTm);
        $this->db->set('ForcefullPwdChangeDt' ,  $dtTm);
        $this->db->update('tbl_gf_users');
    }

    public function update_userprofiledata($firstName , $lastName ,$phone ,$countryId ,$dtTm ,$lang ,$subscribe,$loginVerification, $cPanel ,$strThemeColor , $cols){
        $this->db->query("UPDATE tbl_gf_users SET FirstName = '$firstName', LastName = '$lastName', Phone = '$phone', CountryId = '$countryId', UpdatedAt = '$dtTm', 
        UserLang = '$lang', Subscribed = '$subscribe', TwoStepVerification = '$loginVerification', CPanel = '$cPanel' $strThemeColor $cols 
        WHERE UserId = ".$this->session->userdata('GSM_FUS_UserId')." ");
    }

    public function get_all_userdata(){
       
        $this->db->select('*');
        $this->db->from('tbl_gf_users');
        $this->db->where('UserId' , $this->session->userdata('GSM_FUS_UserId'));

        $result=$this->db->get();
        return $result->row();
    }

    public function get_registration_fields(){
  
        $this->db->select('FieldId, FieldLabel, FieldType, Mandatory, FieldColName');
        $this->db->from('tbl_gf_registration_fields');
        $this->db->where('DisableField' , 0);
        $this->db->order_by('FieldLabel');
        $result=$this->db->get();
        return $result->result();

    }

	public function update_ticket($id, $data){
		$this->db->where('UserId',$user_id);
    	return $this->db->update('tbl_gf_tickets' , $data);
	}

	public function executeQuery($query)
	{
		$this->db->query($query);
	}

	public function getRow($query)
	{
		$rsCurrencies = $this->db->query($query);
		return $rsCurrencies->row();
	}

	public function selectData($query)
	{
		$rsCurrencies = $this->db->query($query);
		return $rsCurrencies->result();
	}



}
