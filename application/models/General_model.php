<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General_model extends CI_Model {

	public function ipBlocked() {
		return $this->db->query("SELECT COUNT(Id) AS Data FROM tbl_gf_blocked_ips WHERE IP = '".$_SERVER['REMOTE_ADDR']."'")->row();
	}

	public function failedLoginAttempt($uName) {
		return $this->db->query("UPDATE tbl_gf_users SET LoginAttempts = LoginAttempts + 1 WHERE UserName = '$uName'");
	}

	public function getLoginAttempts() {
		return $this->db->query('SELECT LoginAttempts FROM tbl_gf_email_settings WHERE Id = 1')->row();
	}

	public function getUserByUsername($userName) {
		return $this->db->query("SELECT UserId, Comments, CONCAT(FirstName,' ', LastName) AS FullName, FirstName, UserName, UserEmail, UserPassword, UserType, 
			LoginAttempts, DisableUser, CanLoginFrmOtherCntry, UserLang, TwoStepVerification FROM tbl_gf_users WHERE UserArchived = '0' 
			AND UserName = '$userName' OR UserEmail = '$userName'")->row();
	}

	public function setDtTmWRTYourCountry() {
		return $this->db->query('SELECT TimeDifference FROM tbl_gf_email_settings WHERE Id = 1')->row();
	}

	public function updateUserLoginAttempts($userId) {
		return $this->db->query("UPDATE tbl_gf_users SET LoginAttempts = 0 WHERE UserId = '".$userId."'");
	}

	public function getEmailDetails() {
		return $this->db->query('SELECT FromName, FromEmail, ToEmail, Company, SiteURL, ContactUsEmail FROM tbl_gf_email_settings WHERE Id = 1')->row();
	}

	public function getUserCountry($userId , $countryISO) {
		return $this->db->query("SELECT COUNT(UserId) AS UserValidity FROM tbl_gf_user_login_countries WHERE ISO = '$countryISO' AND UserId = '".$userId."'")->row();
	}

	public function getEmailContents($id) {
		return $this->db->query("SELECT Subject, Contents, SendCopyToAdmin FROM tbl_gf_email_templates WHERE TemplateId = '$id'")->row();
	}

	public function getSMTPDetails() {
		return $this->db->query('SELECT SMTP, SMTPAuth, SMTPHost, SMTPPort, SMTPUN, SMTPPwd,EmailSignature,Theme,SiteURL, ToEmail FROM tbl_gf_email_settings WHERE Id=1')->row();
	}

	public function getLogoPath() {
		return $this->db->query("SELECT LogoPath FROM tbl_gf_logo WHERE DisableLogo = 0 AND LogoId = '4'")->row();
	}

	public function getLogIdByUserIdAndIP($IP) {
		return $this->db->query("SELECT LogId FROM tbl_gf_userlog where UserId = ".$this->session->userdata('GSM_FUS_UserId')." AND IP = '$IP'")->row();
	}

	public function saveUserLog() {
		$currDtTm = setDtTmWRTYourCountry();
		$IP = $_SERVER['REMOTE_ADDR'];
		$this->db->query("INSERT INTO tbl_gf_userlog SET UserId = ".$this->session->userdata('GSM_FUS_UserId').", LogDate = '$currDtTm', IP = '$IP'");
		return $this->db->insert_id();
	}

	public function saveUserLoginLog() {
		$currDtTm = setDtTmWRTYourCountry();
		$IP = $_SERVER['REMOTE_ADDR'];
		$this->db->query("INSERT INTO tbl_gf_user_login_log SET UserId = ".$this->session->userdata('GSM_FUS_UserId').", LoginTime='$currDtTm', IP = '$IP'");
		return $this->db->insert_id();
	}

	public function getUserInfoByUserName($userName) {
		return $this->db->query("SELECT UserId FROM tbl_gf_users WHERE UserName = '$userName'")->row();
	}

	public function getUserInfoByUserId($UserId) {
		return $this->db->query("SELECT *, CONCAT(FirstName,' ', LastName) AS FullName FROM tbl_gf_users WHERE UserId = $UserId")->row();
	}

	public function blockUser($userId) {
		return $this->db->query("UPDATE tbl_gf_users SET DisableUser = 1, Comments = 'Account Blocked due to failed login attempts!' WHERE UserId = '$userId'");
	}

	public function addBlockedIps($userId) {
		$currDtTm = setDtTmWRTYourCountry();
		$ip = $_SERVER['REMOTE_ADDR'];
		$this->db->query("INSERT INTO tbl_gf_blocked_ips SET UserId = '$userId', DtTm = '$currDtTm', IP = '$ip', Comments = 'IP Blocked due to failed login attempts!'");
		return $this->db->insert_id();
	}

	public function getEmailSettings() {
		return $this->db->query('SELECT isv, Theme, ToEmail, TextDirection, AllowNewUserToLogIn, Phone, AutoCredits, Retail, ShowPricesAtWeb, Address FROM tbl_gf_email_settings WHERE Id = 1')->row();
	}

	public function getLogoIdAndLogoPath() {
		return $this->db->query('SELECT LogoId, LogoPath FROM tbl_gf_logo WHERE DisableLogo = 0 AND LogoId IN (2, 6)')->result();
	}

	public function fetchCurrencies() {
		return $this->db->query('SELECT CurrencyId AS Id, Currency AS Value FROM tbl_gf_currency WHERE DisableCurrency = 0 ORDER BY Currency')->result();
	}

	public function fetchCountries() {
		return $this->db->query("SELECT CountryId, Country, ISO FROM tbl_gf_countries WHERE DisableCountry = 0 ORDER BY Country")->result();
	}

	public function addUser($data) {
		$this->db->query("INSERT INTO tbl_gf_users (UserName, UserEmail, FirstName, LastName, UserPassword, Phone, CountryId, AddedAt, UpdatedAt, DisableUser, CurrencyId, IP, UserLang, ForcefullPwdChangeDt) VALUES ('{$data['uName']}', '{$data['email']}', '{$data['firstName']}', '{$data['lastName']}', '{$data['password']}', '{$data['phone']}', '{$data['countryId']}', '{$data['dtTm']}', '{$data['dtTm']}', '1', {$data['currencyId']}, '{$data['ip']}', '{$data['lang']}', '{$data['dtTm']}')");
		return $this->db->insert_id();
	}

	public function fetchISOByCountryId($countryId) {
		return $this->db->query("SELECT ISO FROM tbl_gf_countries WHERE CountryId = '$countryId'")->row();
	}

	public function saveUserLoginCountries($userId, $iso) {
		$this->db->query("INSERT INTO tbl_gf_user_login_countries (UserId, ISO) VALUES ('$userId', '$iso')");
		return $this->db->insert_id();
	}

	public function deleteUserLoginCountries($userId, $iso) {
		return $this->db->query("DELETE FROM tbl_gf_user_login_countries WHERE UserId = '".$userId."' AND ISO = '$iso'");
	}

	public function fetchUserByUserIdAndUserName($user_Id, $user_Name) {
		return $this->db->query("SELECT UserId, AddedAt FROM tbl_gf_users WHERE UserId = '$user_Id' AND UserName = '$user_Name'")->row();
	}

	public function enableUser($userId) {
		return $this->db->query("UPDATE tbl_gf_users SET DisableUser = 0 WHERE UserId = '".$userId."'");
	}

	public function get_imeiOrderRefunded(){
		$query = $this->db->query("SELECT Refunded FROM tbl_gf_codes WHERE CodeId = '$orderId'");
		return $query->row();	

	}
	
	
	public function get_user_credits($userId){
		$query = $this->db->query("SELECT Credits FROM tbl_gf_users WHERE UserId = '$userId'");
		return $query->row();
	}

	public function update_user_credits($enc_points , $userId){
		$query = $this->db->query("UPDATE tbl_gf_users SET Credits = '$enc_points' WHERE UserId = '$userId'");
	}

	public function get_orders_custom($orderId){
		$query = $this->db->query("SELECT OrderData FROM $tblOrders WHERE $orderCol = '$orderId'");
		return $query->row();
	}
	public function insert_creditHistory($desc ,$orderCustomData , $currDtTm ,$enc_points , $packageId , $comments , $orderId){
		$this->db->query("INSERT INTO tbl_gf_credit_history SET ByAdmin = '$byAdmin', UserId = '$userId', Credits = '$amountToRefund', 
		Description = '$desc', IMEINo = ".addslashes($orderCustomData).", HistoryDtTm = '$currDtTm', 
		CreditsLeft = '$enc_points', PackageId = '$packageId', Comments = '$comments', LogRequestId = '$orderId', OType = '0'");
	}

	public function get_failure_email($colName){
		$query = $this->db->query("SELECT $colName FROM tbl_gf_email_settings WHERE Id = 1");
		return $query->row();
		
	}

	public function get_cancel_subject($serviceId){
		$query = $this->db->query("SELECT CancelTplSubject, CancelTplBody, SendRejectCopyToAdmin FROM tbl_gf_packages WHERE PackageId = '$serviceId' AND CancelledTplType = '1'");
		$query->row();
	}

	public function get_package_cat($CATEGORY ,$PCK_TITLE ,$strPckWhere){
		$query = $this->db->query("SELECT DISTINCT A.CategoryId, $CATEGORY, A.PackageId, $PCK_TITLE, DisablePackage FROM tbl_gf_package_category Cat, tbl_gf_packages A,  $tblOrders B WHERE Cat.CategoryId = A.CategoryId AND A.PackageId = B.PackageId AND Verify = 1 $strPckWhere ORDER BY OrderBy, $CATEGORY, $PCK_TITLE");
		return $query->result();
	}

	public function get_package_cat_2($CATEGORY ,$PCK_TITLE ,$strPckWhere,$strPackIds){
		$strPackIds_sql = '' ;
		if($strPackIds){
			$strPackIds_sql = "AND A.PackageId NOT IN ($strPackIds)";
		}
		$strPckWhere_sql = '';
		if($strPckWhere){
			$strPckWhere_sql = $strPckWhere;
		}
		$query = $this->db->query("SELECT A.CategoryId, $CATEGORY, A.PackageId, $PCK_TITLE, DisablePackage FROM tbl_gf_package_category Cat, tbl_gf_packages A WHERE Cat.CategoryId = A.CategoryId  AND ArchivedPack = '0' $strPckWhere $strPackIds_sql ORDER BY OrderBy, $CATEGORY, $PCK_TITLE");
		return $query->result();
	}

	public function fetch_email_settings(){
		$query =$this->db->query("SELECT * FROM tbl_gf_email_settings WHERE Id = 1");
		return $query->row();
	}

	public function get_users_count($currDt){
		$query = $this->db->query("SELECT COUNT(LogId) AS TotalUsers FROM tbl_gf_userlog WHERE LogDate >= DATE_SUB('$currDt', INTERVAL 10 MINUTE)");
		return $query->row();
	}

	public function get_tickets_sum(){
		$query = $this->db->query("SELECT SUM( IF( StatusId = '1', 1, 0 ) ) AS `NEW`, SUM( IF( StatusId = '2', 1, 0 ) ) AS `CLOSED`,	SUM( IF( StatusId = '3', 1, 0 ) ) AS `ANSWERED`, SUM( IF( StatusId = '4', 1, 0 ) ) AS `CUSTOMERREPLIED`, SUM( IF( StatusId = '5', 1, 0 ) ) AS `WAITING` FROM tbl_gf_tickets WHERE AId = 0");
		return $query->row();
	
	}

	public function fetch_formsdata(){
		return $this->db->query("SELECT A.FormTypeId, FormType, HiddenForm, A.FormId, Form, FormURL, FormUrlNew, IsNew, Class, InHeader FROM tbl_gf_forms A, tbl_gf_form_type B 
					WHERE A.FormTypeId = B.FormTypeId ORDER BY FTOrderBy, FormId")->result();

	}

	public function fetch_formsdata_by_id(){
		return $this->db->query("SELECT A.FormTypeId, HiddenForm, FormType, A.FormId, Form, FormURL, FormUrlNew IsNew, Class, InHeader FROM tbl_gf_forms A, tbl_gf_form_type B,
                tbl_gf_roles_forms C WHERE A.FormTypeId = B.FormTypeId AND A.FormId = C.FormId AND RoleId = ".$this->session->userdata('AdminRoleId')." ORDER BY FTOrderBy, FormId")->row();
	}

	public function fetch_tckt_status(){
		$query=$this->db->query('SELECT TcktStatusId, TcktStatus FROM tbl_gf_tckt_status WHERE DisableTcktStatus = 0 ORDER BY TcktStatusId');
		return $query->row();

	}

	public function fetch_pages(){
		$query=$this->db->query("SELECT PageId, Image, PageText, PageTitle, LinkTitle, HeaderLink, FooterLink, FileName, URL, SubMenuLink, SEOURLName, HTMLTitle, MetaKW, MetaTags 
		FROM tbl_gf_pages WHERE DisablePage = 0 AND PageType = 0 ORDER BY PageId");

		return $query->result();
	}

	public function fetch_banners(){
		$query = $this->db->query("SELECT BannerId, BannerTitle, Detail, BannerPath, URL FROM tbl_gf_banner WHERE DisableBanner = 0 AND BannerType = 0 ORDER BY OrderBy");
		return $query->result();
	}

	public function fetch_products(){
		$query=$this->db->query("SELECT ProductId, ProductName, IsNew, ProductPrice, Picture, PromoStartDate, PromoEndDate, PromoDiscount, SEOURLName FROM tbl_gf_products 
		WHERE DisableProduct = '0' AND ShowHome = '1' ORDER BY POrderBy, ProductName DESC");

		return $query->result();
	}

	public function fetch_retail_services(){
		$query = $this->db->query("SELECT PackageId, PackageTitle, PackagePrice, PackageImage, SEOURLName, PromoStartDate, PromoEndDate, PromoDiscount FROM tbl_gf_retail_services 
		WHERE DisablePackage = '0' AND ArchivedPack = 0 AND ShowAtHomePage = '1' ORDER BY OrderBy, PackageTitle");
	
		return $query->result();
	}

	public function fetch_page_by_id(){
		$query=$this->db->query("SELECT HTMLTitle, MetaKW, MetaTags FROM tbl_gf_pages WHERE PageId = '37'");
		return $query->row();
	}

	public function fetch_logos_by_ids (){
		$query=$this->db->query("SELECT LogoId, LogoPath FROM tbl_gf_logo WHERE DisableLogo = 0 AND LogoId IN (4, 6)");

		return $query->result();
	}

	public function fetch_live_chat_data(){
		$query=$this->db->query("SELECT ScriptCode, AtWebsite, InClientPanel FROM tbl_gf_live_chat WHERE Id = 1");

		return $query->row();
	}

	public function fetch_analytics(){
		$query=$this->db->query("SELECT Code FROM tbl_gf_analytics WHERE Id = 1 AND DisableCode = 0");
		return $query->row();
	}

	public function fetch_marquee(){
		$query=$this->db->query("SELECT Code, Position FROM tbl_gf_marquee WHERE Id = 1 AND DisableMarquee = 0");
		return $query->row();
	}

	public function fetch_flag_counter(){
		$query=$this->db->query("SELECT ScriptCode FROM tbl_gf_flag_counter WHERE DisableFCounter = 0");
		return $query->row();
	}

	public function fetch_socialmedia_data(){
		$query=$this->db->query("SELECT Image, Link, Title, APPID FROM tbl_gf_socialmedia WHERE DisableRecord = 0 ORDER BY Title");
		return $query->result();
	}

	public function fetch_userlog_count($currDt){
		$query=$this->db->query("SELECT COUNT(LogId) AS TotalUsers FROM tbl_gf_userlog WHERE LogDate >= DATE_SUB('$currDt', INTERVAL 10 MINUTE)");
		return $query->row();
	}

	public function fetch_manufacturer(){
		$query=$this->db->query("SELECT CategoryId, Category, SEOURLName FROM tbl_gf_manufacturer WHERE DisableCategory = 0 AND ArchivedCategory = 0 ORDER BY OrderBy");
        return $query->result();
	}
}
