<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class System_model extends CI_Model {

    public function fetch_all_admin_data($id){
       
        $this->db->select('*');
        $this->db->from('tbl_gf_admin');
        $this->db->where('AdminId' , $id);

        $result = $this->db->get();
        return $result->row();

    }

    public function fetch_roles(){ 
        $this->db->select('RoleId AS Id, Role AS Value');
        $this->db->from('tbl_gf_roles');
        $this->db->where('DisableRole' , 0);
        $this->db->order_by('Role');

        $result = $this->db->get();
        return $result->result();
    }

    public function fetch_admins_roles_data($strWhere){
        $query=$this->db->query("SELECT AdminId, AdminUserName, CONCAT(FirstName, ' ', LastName) AS Name, Phone, DisableAdmin, Role FROM tbl_gf_admin A LEFT JOIN tbl_gf_roles B 
        ON (A.RoleId = B.RoleId)  $strWhere ORDER BY AdminUserName");

        return $query->result(); 
    }

    public function update_admins_data($update_data , $where = array() , $where_in= array()){
        if($where){
            $this->db->where($where);
        }

        if($where_in){
            $this->db->where_in($where_in);
        }

        $this->db->update('tbl_gf_admin' , $update_data);
    }

    public function fetch_specific_role_by_id($aRole , $id){
      
        $this->db->select('RoleId');
        $this->db->from('tbl_gf_roles');
        $this->db->where('Role' , $aRole);
        
        if($id >0){
            $this->db->where('RoleId' , $id);
        }

        $result = $this->db->get();
        return $result->row();
    }

    public function insert_roles($insert_data){
       
        $this->db->insert('tbl_gf_roles' , $insert_data);
        return $this->db->insert_id();
    }


    public function update_roles($update_data , $where = array() , $where_in = array()){
      
        if($where){
            $this->db->where($where);
        }
        if($where_in){
            $this->db->where_in($where_in);
        }

        $this->db->update('tbl_gf_roles' , $update_data);
    }

    public function del_roles_forms($id){
   
        $this->db->where('RoleId' , $id);
        $this->db->delete('tbl_gf_roles_forms');
    }

    public function insert_roles_forms($strData){
        $this->db->query("INSERT INTO tbl_gf_roles_forms (RoleId, FormId) VALUES $strData");

    }

    public function fetch_role_by_id($id , $aRole){
       
        $this->db->select('*');
        $this->db->from('tbl_gf_roles');
        $this->db->where('Role' , $aRole);

        $result = $this->db->get();
        return $result->row();
    }

    public function del_roles_forms_by_formids($id , $ids){
     
        $this->db->where('RoleId' , $id);
        $this->db->where_in('FormId' , $ids);
        $this->db->delete('tbl_gf_roles_forms');
    }

    public function fetch_roles_forms_by_role_id($id){
        $this->db->select('*');
        $this->db->from('tbl_gf_roles_forms');
        $this->db->where('RoleId' , $id);

         $result = $this->db->get();
        return $result->result();
    }

    public function fetch_forms_type_data($formTypeId){
        $result = $this->db->query("SELECT FormId, Form, A.FormTypeId, FormType FROM tbl_gf_forms A, tbl_gf_form_type B WHERE A.FormTypeId = B.FormTypeId AND 
        A.FormTypeId = '$formTypeId' ORDER BY FormTypeId, FormId");

        return $result->result();

    }

    public function fetch_roles_data(){
       
        $this->db->select('*');
        $this->db->from('tbl_gf_roles');
        $this->db->order_by('Role');

        $result = $this->db->get();
        return $result->result();
    }

    public function fetch_admin_count($email , $id){
       
        $this->db->select("Count(AdminId) AS TotalRecs");
        $this->db->from('tbl_gf_admin');
        $this->db->where('AdminUserName' , $email);

        if($id > 0) {
            $this->db->where('AdminId' , $id);
        }
        $result = $this->db->get(); 
        return $result->row();
    }

    public function insert_admin_data($insert_data){
        $this->db->insert('tbl_gf_admin' , $insert_data);
    }

    public function delete_admins($id){
      
        $this->db->where('AdminId' , $id);
        $this->db->delete('tbl_gf_admin');
    }
}