<?php
if (!extension_loaded('curl'))
{
    trigger_error('cURL extension not installed', E_USER_ERROR);
}
class API_miracleserver
{
    var $xmlData;
    var $xmlResult;
    var $debug;
    var $action;
    function __construct()
    {
        $this->xmlData = new DOMDocument();
    }
    function getResult()
    {
        return $this->xmlResult;
    }
    function action($action, $password, $userName, $url)
    {
        if (is_string($action))
        {
			$posted = array("auth" => array('username' => $userName, 'password' => $password, 'actiontype' => $action));
			$data_string = json_encode($posted);
			$objCurl = curl_init();
			curl_setopt($objCurl, CURLOPT_HEADER, false);
			curl_setopt($objCurl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
			//curl_setopt($objCurl, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($objCurl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($objCurl, CURLOPT_URL, $url);
			curl_setopt($objCurl, CURLOPT_POST, true);
			curl_setopt($objCurl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($objCurl, CURLOPT_POSTFIELDS, $data_string);
			$response = curl_exec($objCurl);
			if (curl_errno($objCurl) != CURLE_OK)
			{
				echo curl_error($objCurl);
				curl_close($objCurl);
			}
			else
			{
				curl_close($objCurl);
				/*echo "<textarea rows='20' cols='200'> ";
				print_r($response);
				echo "</textarea>";*/
				return (json_decode($response, true));
			}
        }
        return false;
    }
}
?>
