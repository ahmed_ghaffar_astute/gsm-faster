<?php
// Check if cURL is installed or not
if (! extension_loaded('curl'))
{
	trigger_error('cURL extension not installed', E_USER_ERROR);
}
class API_Sunny
{
	var $gf_channel;
	var $result = array();
	function getResult()
	{
		return $this->result;
	}
    function doAction($apiKey, $url, $imei)
    {
		$this->gf_channel = curl_init( );
		curl_setopt( $this->gf_channel, CURLOPT_HEADER, false );
		curl_setopt( $this->gf_channel, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		curl_setopt( $this->gf_channel, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $this->gf_channel, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $this->gf_channel, CURLOPT_COOKIEJAR, 'cookie.txt');
		curl_setopt( $this->gf_channel, CURLOPT_COOKIEFILE, 'cookie.txt');
		curl_setopt( $this->gf_channel, CURLOPT_URL, $url.'/other/smchk.aspx?imei='.$imei.'&apikey='.$apiKey );
		curl_setopt( $this->gf_channel, CURLOPT_POSTFIELDS, $params );
		$this->result = curl_exec( $this->gf_channel );
		if (curl_errno($this->gf_channel) != CURLE_OK)
		{
			trigger_error(curl_error($this->gf_channel), E_USER_WARNING);
		}
		curl_close($this->gf_channel);
    }
}
