<?php
class Api {
	private $key;
	function doAction($qry, $url, $username, $apiKey, $param = array()) {
		$param['qry'] = $qry;
		$param['user'] = $username;
		$param['api_key'] = $apiKey;

		foreach($param as $key => $value)
			$data[] = "$key=".urlencode($value);

		$r = file_get_contents($url . "?" . join("&", $data));

		if ($r === false) {
			$result['errors'] = array("Couldn't connect to API!");
			return $result;
		}

		if (empty($r)) {
			$result['errors'] = array("No response from API server!");
			return $result;
		}
		return json_decode($r, true);
	}


    /**
     * $imei - 15 digit number
     * $hash - 40 digit hashkey
     * $service - xx digit number 
     * pls use service_name($s) to get service name from id
     * ex   1 - localbruteforce 
     *      2 - superbruteforce
     *      3 - bulkbruteforce
     * pls check this id's before production
     */
	public function upload($service, $imei, $hash) {
		return $this->go("upload", array('service'=>$service, "imei" => $imei, "hash" => $hash));
	}
    
    /* not implemented 
	public function delete($imei) {
		return $this->go("delete", array("imei" => $imei));
	}*/

    /**
     * get service information 
     * exemple of answer
     * credits : how many credits left
     * service info s : int   service id
     *              t : int   time to wait
     * {"credits":"731.75","services":[{"s":"1","t":"1430"},{"s":"2","t":"5"},{"s":"3","t":"5"},{"s":"4","t":"6"},{"s":"5","t":"2840"}]}
     */
	public function info() {
		return $this->go("info");
	}


    /**
     * Will return an array of all jobs not finished, imei + waiting time
     */
	public function myqueue()  {
		return $this->go("myqueue");
	}

    /**
     * Get info about specific job : waiting time  if job not finish, codes if job is finished
     */
	public function job($imei) {
		return $this->go("job", array("imei" => $imei));
	}
       
    /*
     * get info about services
     */
    public function service_name($s){
        return $this->go('service', array('no'=>$s));
    }
        
    /*
     * Upload file method
     * $service_id  @number   1-5 what service will process the file
     * $filename    @string   112341234124.bcl   filename must be complete including extension
     * $content    @binary file content
     *       $content = base64_encode(file_get_content($file));
     * 	     This encoding is designed to make binary data survive transport through transport 
     *	     layers that are not 8-bit clean, such as mail bodies.  
     */

    public function file($service_id, $filename, $content){
        return $this->go('file', array('service'=>$service_id, 'file'=>$filename, 'content'=>$content));
    } 
}
?>