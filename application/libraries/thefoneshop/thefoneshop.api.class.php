<?php
if (! extension_loaded('curl')){
	trigger_error('cURL extension not installed', E_USER_ERROR);
}

class TheFoneShop
{
	var $xmlData;
	var $xmlResult;
	function __construct(){
		$this->xmlData = new DOMDocument();
	}
	
	function getResult(){
		return $this->xmlResult;
	}
	
    function action($action, $apiKey, $username, $url, $arr = array())
	{
		if(is_string($action))
		{
			if(is_array($arr))
			{
				$action = strtoupper($action);
				$request = $this->xmlData->createElement("REQUEST");
				$act = $this->xmlData->createElement("ACTION",$action);
				$this->xmlData->appendChild($request);
				$request->appendChild($act);

				if(!empty($arr)){
	                foreach ($arr as $key => $val){
						$key = strtoupper($key);
						$request->appendChild($this->xmlData->createElement($key,$val));
					}
                }
				$posted = array(
					'USERNAME' => $username,
					'API_KEY' => $apiKey,
					'DATA' => $this->xmlData->saveHTML()
				);
				$crul = curl_init();
				// you might want the headers for http codes
				curl_setopt( $crul, CURLOPT_HEADER, false );
				// you may need to set the http useragent for curl to operate as
				curl_setopt( $crul, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
				// you wanna follow stuff like meta and location headers
				curl_setopt( $crul, CURLOPT_FOLLOWLOCATION, true );
				// you want all the data back to test it for errors
				curl_setopt( $crul, CURLOPT_RETURNTRANSFER, true );
				// setup the url to post / get from / to
				curl_setopt( $crul, CURLOPT_URL, $url.'sendbox/index.php');
				curl_setopt( $crul, CURLOPT_POST, true );
				curl_setopt($crul, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt( $crul, CURLOPT_POSTFIELDS, $posted );
				
				$this->xmlResult = curl_exec( $crul );
				if (curl_errno($crul) != CURLE_OK)
				{
					echo curl_error($crul);
				}
				curl_close($crul);
			}
		}
	}
	function XmlToArray($xml)
	{
	   $this->xml = $xml;	
	}

	function _struct_to_array($values, &$i)
	{
		$child = array(); 
		if (isset($values[$i]['value'])) array_push($child, $values[$i]['value']); 
		
		while ($i++ < count($values)) { 
			switch ($values[$i]['type']) { 
				case 'cdata': 
            	array_push($child, $values[$i]['value']); 
				break; 
				
				case 'complete': 
					$name = $values[$i]['tag']; 
					if(!empty($name)){
					$child[$name]= ($values[$i]['value'])?($values[$i]['value']):''; 
					if(isset($values[$i]['attributes'])) {					
						$child[$name] = $values[$i]['attributes']; 
					} 
				}	
          	break; 
				
				case 'open': 
					$name = $values[$i]['tag']; 
					$size = isset($child[$name]) ? sizeof($child[$name]) : 0;
					$child[$name][$size] = $this->_struct_to_array($values, $i); 
				break;
				
				case 'close': 
            	return $child; 
				break; 
			}
		}
		return $child; 
	}//_struct_to_array
	function createArray()
	{ 
		$xml    = $this->xml;
		$values = array(); 
		$index  = array(); 
		$array  = array(); 
		$parser = xml_parser_create(); 
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parse_into_struct($parser, $xml, $values, $index);
		xml_parser_free($parser);
		$i = 0; 
		$name = $values[$i]['tag']; 
		$array[$name] = isset($values[$i]['attributes']) ? $values[$i]['attributes'] : ''; 
		$array[$name] = $this->_struct_to_array($values, $i); 
		return $array; 
	}//createArray
}

?>