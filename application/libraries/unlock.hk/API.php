<?php
	require_once(APPPATH.'libraries/gsmunlockusa.com/nusoap/lib/nusoap.php');
	define('UNLOCKHK_API_DEBUG', false);
	define('UNLOCKHK_VARIABLE_ERROR',    '_UnlockHKError'    );
	define('UNLOCKHK_VARIABLE_ARRAY',    '_UnlockHKArray'    );
	define('UNLOCKHK_VARIABLE_POINTERS', '_UnlockHKPointers' );
	
	/* Check that cURL is installed */
	
	if (! extension_loaded('curl'))
	{
		trigger_error('cURL extension not installed', E_USER_ERROR);
	}
	
	/* Here comes the main class */
	
	class UnlockHK
	{
		/*
			mixed UnlockHK::CallAPI (string $Action, array $Parameters)
			Call the UnlockHK API.
			Returns the xml stream sent by the UnlockHK server
			Or false if an error occurs
		*/

		function CallAPI ($apiKey, $userName, $url, $Action, $Parameters = array() )
		{
			if (is_string($Action))
			{
				if (is_array($Parameters))
				{
					/* Add the API Key and the Action to the parameters */
					$Parameters['UserName'] = $userName;
					$Parameters['Sign'] = md5($userName.$apiKey);

					$client = new nusoap_client($url, true);
					$client->soap_defencoding = 'utf-8';
					$client->decode_utf8 = false;
					$client->xml_encoding = 'utf-8';					

					$Data = $client->call($Action, $Parameters);

					if(is_array($Data))
					{
						$Data = join($Data);
					}
				
					/* Return the data, or false if an error occurred */
					return $Data;
				}
				else trigger_error('Parameters must be an array', E_USER_WARNING);
			}
			else trigger_error('Action must be a string', E_USER_WARNING);
			
			return false;
		}

		/*
			mixed UnlockHK::ParseXML (string $XML)
			Parse an XML stream from the UnlockHK API.
			Returns an associative array of the parsed XML string
			Or false if an error occurs
		*/
		
		function ParseXML ( $XML )
		{
			if (! is_string($XML))
			{
				/* If the argument is not a string, report the error in debug mode & stop here */
				if (UNLOCKHK_API_DEBUG) trigger_error('Invalid argument supplied for UnlockHK::ParseXML()', E_USER_WARNING);
				return false;
			}

			$XML = simplexml_load_string($XML);
			$json  = json_encode($XML);
			$xmlArr = json_decode($json, true);
			return $xmlArr; 
		}

		function XML_Start ( $Parser, $Name, $Attributes )
		{
			/* Globalize variables */
			global ${UNLOCKHK_VARIABLE_ERROR};
			global ${UNLOCKHK_VARIABLE_ARRAY};
			global ${UNLOCKHK_VARIABLE_POINTERS};
			
			/* Do nothing if an error occurred previously */
			if (${UNLOCKHK_VARIABLE_ERROR}) return;

			if (count( ${UNLOCKHK_VARIABLE_POINTERS} ) == 0)
			{
				/* Root Element : create the first pointer to the array */
				${UNLOCKHK_VARIABLE_POINTERS}[] =& ${UNLOCKHK_VARIABLE_ARRAY};
			}
			else
			{
				/* Get the latest pointer */
				$Pointer =& ${UNLOCKHK_VARIABLE_POINTERS} [ count( ${UNLOCKHK_VARIABLE_POINTERS} ) -1 ];
				
				if (is_null($Pointer))
				{
					/* This is the first sub-tag with that name, create the new container array for it */
					$Pointer[] = array();
					
					/* Replace the latest pointer, point to the first item of the new container */
					${UNLOCKHK_VARIABLE_POINTERS}[ count(${UNLOCKHK_VARIABLE_POINTERS}) -1 ] =& $Pointer[0];
					$Pointer =& $Pointer[0];
				}
				elseif (is_array($Pointer))
				{
					if (isset($Pointer[$Name]))
					{
						if (! is_array($Pointer[$Name]))
						{
							/* Unrecognized XML stream */
							${UNLOCKHK_VARIABLE_ERROR} = true;
							return;
						}
						
						/* The tag is already known, add an item to the array and create a pointer to it */
						$Pointer[$Name][] = array();
						${UNLOCKHK_VARIABLE_POINTERS}[] =& $Pointer[$Name][ count($Pointer[$Name]) -1 ];
						return;
					}
				}
				else
				{
					/* Unrecognized XML stream */
					${UNLOCKHK_VARIABLE_ERROR} = true;
					return;
				}
				
				/* Set the default value and create a pointer to it */
				$Pointer[$Name] = NULL;
				${UNLOCKHK_VARIABLE_POINTERS}[] =& $Pointer[$Name];
			}
		}
		
		function XML_End ( $Parser, $Name )
		{
			/* Globalize variables */
			global ${UNLOCKHK_VARIABLE_ERROR};
			global ${UNLOCKHK_VARIABLE_POINTERS};

			/* Do nothing if an error occurred previously */
			if (${UNLOCKHK_VARIABLE_ERROR}) return;

			/* Remove the latest pointer */
			array_pop( ${UNLOCKHK_VARIABLE_POINTERS} );
		}
		
		function XML_CData ( $Parser, $Data )
		{
			/* Ignore whitespaces */
			if (rtrim($Data) == '') return;

			/* Globalize variables */
			global ${UNLOCKHK_VARIABLE_ERROR};
			global ${UNLOCKHK_VARIABLE_POINTERS};

			/* Do nothing if an error occurred previously */
			if (${UNLOCKHK_VARIABLE_ERROR}) return;
			
			/* Get the latest pointer */
			$Pointer =& ${UNLOCKHK_VARIABLE_POINTERS} [ count( ${UNLOCKHK_VARIABLE_POINTERS} ) -1 ];
			
			if (is_array($Pointer))
			{
				/* Unrecognized XML stream, should be null or string here */
				${UNLOCKHK_VARIABLE_ERROR} = true;
				return;
			}
			
			/* Append the character data */
			$Pointer .= $Data;
		}
	}
?>
