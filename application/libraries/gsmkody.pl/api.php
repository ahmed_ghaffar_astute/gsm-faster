<?php
class gsmkody
{
	var $objCurl;
	var $result = array();

	function getResult()
	{
		return json_decode($this->result, true);
	}
		
	function checkError($result)
	{
		if(isset($result['errors']))
		{
			if(isset($result['errors'][0]['message']))
			return 'API Error: ' . $result['errors'][0]['message'];
		}
		return '';
	}
    function doAction($action, $apiKey, $url, $method, $type = '', $parameters = array())
    {
		if (is_string($action))
		{
			$this->objCurl = curl_init();
			curl_setopt( $this->objCurl, CURLOPT_HEADER, false );
			curl_setopt( $this->objCurl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
			@curl_setopt( $this->objCurl, CURLOPT_FOLLOWLOCATION, true );
			curl_setopt( $this->objCurl, CURLOPT_RETURNTRANSFER, true );
			if($method == 'GET')
			{
				$url .= 'api/'.$action.'?key='.$apiKey;
				if($type != '')
					$url .= '&type='.$type;
				curl_setopt( $this->objCurl, CURLOPT_URL, $url);
			}
			else if($method == 'POST')
			{
				$parameters['key'] = $apiKey;
				$parameters['type'] = 'imei';
				curl_setopt( $this->objCurl, CURLOPT_URL, $url.'api/'.$action);
				curl_setopt( $this->objCurl, CURLOPT_POST, true );
				curl_setopt( $this->objCurl, CURLOPT_POSTFIELDS, $parameters);
			}
			$this->result = curl_exec( $this->objCurl );
			curl_close($this->objCurl);
		}
    }
}
?>