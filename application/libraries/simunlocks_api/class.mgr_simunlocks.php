<?
class mgr_simunlocks
{
    private $api_id = NULL;
    private $api_url = NULL;
    private $api_key = NULL;
    private $tools = NULL;
    private $params = NULL;
    
    public function __construct($params)
    {
//        parent::__construct($g);
//        $this->api_id = $id;
        $this->api_url = $params['url'];
        $this->api_key = $key['apiKey'];
//        $this->tools = new mgr_tools($this->g);
    }
    /* ---------------------------------------------------------------------------------------------------------------------------------------- */
    public function get_services()
    {
        $post = array(
            'key'       =>  $this->api_key,
        );
        $out = $this->get_url($this->api_url.'/get-tools', $post);
        $json = json_decode($out);
        if ($json->errno != 0)
			return $json->error;
		else
			return $json->out->tools;
/*
        foreach ($json->out->tools as $t)
        {
            // add group
            if (($gid = $this->tools->real_add_tool_group(0, $t->group_name)) === false) return 'Error adding group '.$t->group_name;
            // add tool
            if (($sid = $this->add_tool($t, $gid)) === false) return 'Error adding tool '.$t->name;
        }
*/
    }
    /* ---------------------------------------------------------------------------------------------------------------------------------------- */
    public function place_order($arr, $tool_id)
    {
        if (!isset($arr['imei'])) return 'Missing IMEI number';
        $post = array(
            'key'       =>  $this->api_key,
            'imei'      =>  $arr['imei'],
            'id'   =>  $tool_id
        );
        foreach ($arr as $key => $val) $post[$key] = $val;
        $out = $this->get_url($this->api_url.'/place-order', $post);
        $json = json_decode($out);
        if ($json->errno != 0) return $json->error;
        $api_order_id = $json->out->id;
        return $api_order_id;
    }
    /* ---------------------------------------------------------------------------------------------------------------------------------------- */
    public function get_order($dat, $api_order_id, &$codes = '', &$msg = '')
    {
        $post = array(
           'key' => $this->api_key, 
           'id' => $api_order_id
        );
        if (($out = $this->get_url($this->api_url.'/get-order', $post)) === false) return false;
        $json = json_decode($out);
        if ($json->errno != 0)
        {
            $msg = $json->error;
            return true;
        }
        if ($json->out->order->status == 'Processing') return false;
        if ($json->out->order->status != 'Delivered')
        {
            if ($json->out->order->reason == '') $msg = $json->out->order->status; else $msg = $json->out->order->reason;
            return true;
        }
        //
        $codes = $json->out->order->code;
        return true;
    }
    /* ---------------------------------------------------------------------------------------------------------------------------------------- */
    public function get_tool_networks($id)
    {
        if ($this->params == NULL) $this->params = $this->get_tool_params($id);
        if (!isset($this->params->network)) return array();
        return $this->params->network;
    }
    /* ---------------------------------------------------------------------------------------------------------------------------------------- */
    public function get_tool_mobiles($id)
    {
        if ($this->params == NULL) $this->params = $this->get_tool_params($id);
        if (!isset($this->params->mobile)) return array();
        return $this->params->mobile;
    }
    /* ---------------------------------------------------------------------------------------------------------------------------------------- */
    public function get_mep_codes($id)
    {
        if ($this->params == NULL) $this->params = $this->get_tool_params($id);
        if (!isset($this->params->mep)) return array();
        return $this->params->mep;
    }
    /* ---------------------------------------------------------------------------------------------------------------------------------------- */
    public function get_prd_codes($id)
    {
        if ($this->params == NULL) $this->params = $this->get_tool_params($id);
        if (!isset($this->params->prd)) return array();
        return $this->params->prd;
    }
    /* ---------------------------------------------------------------------------------------------------------------------------------------- */
    public function get_tool_params($id)
    {
        $post = array(
           'key' => $this->api_key, 
           'id' => $id
        );
        if (($out = $this->get_url($this->api_url.'/get-tool-params', $post)) === false) return false;
        $json = json_decode($out);
        if ($json == NULL) return array();
        $params = $json->out->params;
        unset($json);
        return $params;
    }
    /* ---------------------------------------------------------------------------------------------------------------------------------------- */
	private function get_url($url, $post = NULL)
	{
		$c = curl_init($url); 
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($c, CURLOPT_HEADER, false);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
		if ($post != NULL)
		{
			curl_setopt($c, CURLOPT_POST, true);
			curl_setopt($c, CURLOPT_POSTFIELDS, $post);
		}
		if (($out = curl_exec($c)) === false) $out = curl_error($c);
			curl_close($c);
		return $out;
	}
    /* ---------------------------------------------------------------------------------------------------------------------------------------- */
    public function credits_balance($msg)
    {
        $post = array(
           'key' => $this->api_key
        );
        if (($out = $this->get_url($this->api_url.'/credits-balance', $post)) === false) return false;
        $json = json_decode($out);
        if ($json == NULL)
        {
            $msg = 'Error parsing response';
            return false;
        }
        return $json->out->credits;
    }
    /* ---------------------------------------------------------------------------------------------------------------------------------------- */
    private function add_tool($t, $gid)
    {
        // exists?
        $query = "SELECT ID FROM tools WHERE apiID = ".$this->api_id." AND systemID = ".(int) $t->id;
        $rows = $this->g->db->select($query);
        if (!empty($rows))
        {
            $query = "UPDATE tools SET name='".$this->g->db->escape($t->name)."',credits=".(float) $t->credits.",delivery='".$this->g->db->escape($t->delivery)."' WHERE ID = ".$rows[0][0];
            $this->g->db->update($query);
            if ($this->g->db->get_errno() != 0) return false;
            // custom params
            if (!empty($t->custom))
            {
                $prm = array(array($t->custom[0]->label, json_encode($t->custom[0]->options)));
                $this->tools->real_add_tool_params_api($rows[0][0], $prm);
            }
            return $rows[0][0];
        }
        //
        $query = "INSERT INTO tools VALUES (NULL,".$this->api_id.",".$gid.",'".$this->g->db->escape($t->name)."','',".(float) $t->credits.",2,".(int) $t->id.",'".$this->g->db->escape($t->description)."','".$this->g->db->escape($t->delivery)."',".(int) $t->network.",".(int) $t->mobile.",".(int) $t->provider.",".(int) $t->pin.",".(int) $t->kbh.",".(int) $t->mep.",".(int) $t->prd.",0,0,0,0,1)";
        $lid = $this->g->db->insert($query);
        if ($this->g->db->get_errno() != 0) return false;
        // custom params
        if (!empty($t->custom))
        {
            $prm = array(array($t->custom[0]->label, json_encode($t->custom[0]->options)));
            $this->tools->real_add_tool_params_api($lid, $prm);
        }
        return $lid;
    }
}
?>
