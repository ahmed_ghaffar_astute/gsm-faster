<?php
if (!extension_loaded('curl'))
{
    trigger_error('cURL extension not installed', E_USER_ERROR);
}
class zzucustomapi_sl
{
    var $xmlData;
    var $xmlResult;
    var $debug;
    var $action;
    function action($apiKey, $url, $imei = '')
    {
		$post = array();
		$post["apikey"] = $apiKey;
		$post["ver"] = "1";
		$ch = curl_init();
		$timeout = 60;
		$url = $url.'long/apple?app=simlock&key='.$apiKey;
		if($imei != '')
			$url = '&sn='.$imei;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,$timeout);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$post);
		$data = curl_exec($ch);
		return $dataObj = json_decode($data, true);
    }
}
?>