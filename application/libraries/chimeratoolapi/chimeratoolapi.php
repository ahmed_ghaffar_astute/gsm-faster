<?php

class ChimeraApi {
	private function sendCmd($cmd, $apiKey, $userName, $url, $params=array()) {
		$params['username'] = $userName;
		$params['apikey'] = $apiKey;

		foreach($params as $key => $value)
			$up[] = $key."=".urlencode($value);

		//$r = file_get_contents(self::$url . $cmd . "?" . join("&", $up));
		$r = @file_get_contents($url . $cmd . "?" . join("&", $up));

		if($r === false)
			return array("success" => false, "code" => "000", "message" => "Couldn't connect to API!");

		if(empty($r))
			return array("success" => false, "code" => "001", "message" => "No response from API server!");

		return json_decode($r, true);
	}

	public function getTransferList($page = 1, $limit = 50) {
		return $this->sendCmd("list",array('page' => $page, 'limit' => $limit));
	}

	public function getInfo($apiKey, $userName, $url) {
		return $this->sendCmd("info", $apiKey, $userName, $url, array());
	}

	public function transferCredit($userId, $amount) {
		return $this->sendCmd("transfer",array('userId' => $userId, 'amount' => $amount));
	}

	public function revokeTransfer($transferId) {
		return $this->sendCmd("revoke",array('transferId' => $transferId));
	}

	public function checkUser($username, $email) {
		return $this->sendCmd("checkuser", array('user' => $username, 'email' => $email));
	}
	public function checkChimeraCard($serialnumber) {
		return $this->sendCmd("checkuser", array('serialnumber' => $serialnumber));
	}

	public function sellLicence($targetid, $targettype, $licencename) {
		return $this->sendCmd("selllicence", array('targetid' => $targetid, 'targettype' => $targettype, 'licencename' => $licencename));
	}

	public function extendLicence($userlicenceid) {
		return $this->sendCmd("extendlicence", array('userlicenceid' => $userlicenceid));
	}

	public function upgradeLicence($username, $email) {
		return $this->sendCmd("upgradelicence", array('userlicenceid' => $userlicenceid));
	}

	public function availableLicences($apiKey, $userName, $url) {
		return $this->sendCmd("availableLicences", $apiKey, $userName, $url, array());
	}

	public function getSoldLicenceList($page = 1, $limit = 50) {
		return $this->sendCmd("listsoldlicences", array('page' => $page, 'limit' => $limit));
	}

	public function revokeLicence($actionId) {
		return $this->sendCmd("revokelicence", array('actionId' => $actionId));
	}

	public function checkUserLicences($username, $email) {
		return $this->sendCmd("checkUserLicences", array('userlicenceid' => $userlicenceid));
	}
}