<?php
define('DLGSM_API_DEBUG', false);
// Check if cURL is installed or not
if (! extension_loaded('curl'))
{
	trigger_error('cURL extension not installed', E_USER_ERROR);
}
class DLGSM_API{
	var $gf_channel;
	var $result = array();
	function getResult()
	{
		return json_decode($this->result, true);
	}
		
	function checkError($result)
	{
		if(isset($result['ERR']))
		{
			return $result['ERR'];
		}
		else
			return '';
	}
	
    function doAction($action, $userName, $apiKey, $url, $params = array())
    {
		if (is_string($action))
		{
			if (is_array($params))
			{
				$params['USERNAME'] = $userName;
				$params['API_ACCESS_KEY'] = $apiKey;
				$params['action'] = $action;
				$this->gf_channel = curl_init( );
				curl_setopt( $this->gf_channel, CURLOPT_HEADER, false );
				curl_setopt( $this->gf_channel, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
				curl_setopt( $this->gf_channel, CURLOPT_FOLLOWLOCATION, true );
				curl_setopt( $this->gf_channel, CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $this->gf_channel, CURLOPT_COOKIEJAR, 'cookie.txt');
				curl_setopt( $this->gf_channel, CURLOPT_COOKIEFILE, 'cookie.txt');
				if( is_array( $params ) ):
					$vars = implode( '&', $params);
				endif;
				curl_setopt( $this->gf_channel, CURLOPT_URL, $url.'gsx/api_index.php' );
				curl_setopt( $this->gf_channel, CURLOPT_POST, true );
				curl_setopt( $this->gf_channel, CURLOPT_POSTFIELDS, $params );
				$this->result = curl_exec( $this->gf_channel );
				if (DLGSM_API_DEBUG && curl_errno($this->gf_channel) != CURLE_OK)
				{
					trigger_error(curl_error($this->gf_channel), E_USER_WARNING);
				}
				curl_close($this->gf_channel);
			}
		}
    }// End sendCommand
	
	function parse2Array($xml)
	{
		$xml_parser = xml_parser_create();
		xml_parse_into_struct($xml_parser, $xml, $vals, $index);
		xml_parser_free($xml_parser);
		
		$params = array();
		$level = array();
		foreach ($vals as $xml_elem)
		{
			if ($xml_elem['type'] == 'open')
			{
				if (array_key_exists('attributes',$xml_elem)) 
				{
					list($level[$xml_elem['level']],$extra) = array_values($xml_elem['attributes']);
				}
				else
				{
					$level[$xml_elem['level']] = $xml_elem['tag'];
				}
			}
			
			if ($xml_elem['type'] == 'complete')
			{
				$start_level = 1;
				$php_stmt = '$params';
				while($start_level < $xml_elem['level'])
				{
					$php_stmt .= '[$level['.$start_level.']]';
					$start_level++;
				}
				$php_stmt .= '[$xml_elem[\'tag\']] = $xml_elem[\'value\'];';
				eval($php_stmt);
			}
		}
		return $params;
	}// End parse2XML
}