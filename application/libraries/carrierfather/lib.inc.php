<?

/**
 * Description of Carrier Apple
 * Version 2.0
 * @author CuongNQ
 * // CODE| Message
  //  0   | Account info
  //  1   | API Key is invalid
  //  2   | DATA is invalid
  //  3   | Device is replaced use GSX check
  //  4   | Please check this imei later
  //  5   | Check Done
  //  6   | Not enough credit
 */
class Carrier_API {
    /**
     * API URL
     */

    const API_URL = "http://carrierapple.com/api/";

    /**
     * API Key
     */
    private $api_key;
    private $format = "json";

    /**
     * Constructor
     */
    function __construct($api_key) {
        $this->api_key = $api_key;
    }

    /**
     * Connect method
     * Param: method, params
     */
    private function connect($method, $params = array()) {
        foreach ($params as $key => $value)
            $up[] = "$key=" . urlencode($value);

        $r = file_get_contents(isset($up) ?
                        self::API_URL . $method . '?key=' . $this->api_key . '&format=' . $this->format . "&" .
                        implode("&", $up) :
                        self::API_URL . $method . '?key=' . $this->api_key . '&format=' . $this->format);

        if ($r === false)
            return array(
                "msg_code" => '-1',
                "message" => "Couldn't connect to API"
            );

        if (empty($r))
            return array(
                "msg_code" => '-2',
                "message" => "No response from API server"
            );
        return json_decode($r, true);
    }

    /**
     * Get account information
     * @return type
     */
    public function accountInfo() {
        return $this->connect("account-info");
    }

    /**
     * Check Pro status
     * @param type $imei
     * @return type
     */
    public function proCheck($imei) {
        return $this->connect("pro-check", array(
                    'imei' => $imei
        ));
    }

    public function simpleCheck($imei) {
        return $this->connect("simple-check", array(
                    'imei' => $imei
        ));
    }

    public function fmiCheck($imei) {
        return $this->connect("fmi-check", array(
                    'imei' => $imei
        ));
    }

    public function blacklistCheck($imei) {
        return $this->connect("blacklist-check", array(
                    'imei' => $imei
        ));
    }
    public function blacklistGet($id) {
        return $this->connect("blacklist-get", array(
                    'id' => $id
        ));
    }

}

?>