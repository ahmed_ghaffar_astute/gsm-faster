<?
class unlocksWorld
{
    function action($action, $apiKey, $url, $data = array())
    {
		$para = array("ApiKey" => $apiKey, "Data" => $data);
		$data_string = json_encode($para);
		$ch = curl_init('http://www.unlocksworld.com/api/functions/'.$action);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string))
		);
		$result = curl_exec($ch);
		curl_close($ch);
		return json_decode($result, true);
    }
}
?>