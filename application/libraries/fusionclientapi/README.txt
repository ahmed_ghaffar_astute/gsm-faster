================================================
DHRU FUSION CLIENT APi
================================================


    APi Release Version: 1.0 (Oct 30, 2011)

    PLEASE READ ALL THE ENCLOSED INSTRUCTIONS
    http://wiki.dhru.com/Client_Remote_APi


================================================
Available API Commands
================================================
accountinfo - To get Account Information
imeiservicelist - To get all IMEI service list
getimeiservicedetails - to get single service details , Parameters : ID
modellist - To get All Brands and Models 
providerlist - To get All Country and Provider 
meplist - To get All MEPs 
placeimeiorder - To Place new IMEI Order Parameters : ID as a Service id , IMEI , MODELID , PROVIDERID , MEP , PIN , KBH , PRD , TYPE , REFERENCE , LOCKS
getimeiorder - To check order status Parameters : IMEI , [STATUS : 0 = New, 1 = in process, 3 = rejected, 4 = process success]