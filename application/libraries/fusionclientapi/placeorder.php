<?php
	require_once('dhrufusionapi.class.php');
	$objFusion = new DhruFusion();
	$para['IMEI'] = $myIMEI;
	if($extNwkId != '')
		$para['ID'] = $extNwkId;
	if($modelId != '')
		$para['MODELID'] = $modelId;
	if($providerId != '')
		$para['PROVIDERID'] = $providerId;
	if($other != '')
		$para['CUSTOME_FIELD'] = $other;
	if($mepValue != '')
		$para['MEP'] = $mepValue;
	if($pin != '')
		$para['PIN'] = $pin;
	if($kbh != '')
		$para['KBH'] = $kbh;
	if($prd != '')
		$para['PRD'] = $prd;
	if($type != '')
		$para['TYPE'] = $type;
/*	if($prd != '')
		$para['REFERENCE'] = $prd;*/
	if($locks != '')
		$para['LOCKS'] = $locks;

	$RESPONSE_FU = $objFusion->action('placeimeiorder', $apiKey, $accountId, $serverURL, $para);
	if(isset($RESPONSE_FU['ERROR']))
	{
		if(is_array($RESPONSE_FU['ERROR'][0]))
		{
			$ifCodeSentToServer = 0;
			$msgFromServer = 'Error while sending request to server!';
			if(isset($RESPONSE_FU['ERROR'][0]['MESSAGE']))
				$msgFromServer = strip_tags($RESPONSE_FU['ERROR'][0]['MESSAGE']);
			if(isset($RESPONSE_FU['ERROR'][0]['FULL_DESCRIPTION']))
				$msgFromServer = strip_tags($RESPONSE_FU['ERROR'][0]['FULL_DESCRIPTION']);
		}
	}
	else if(isset($RESPONSE_FU['SUCCESS']))
	{
		$ifCodeSentToServer = 1;
		if(is_array($RESPONSE_FU['SUCCESS'][0]))
		{
			if(isset($RESPONSE_FU['SUCCESS'][0]['REFERENCEID']))
				$orderIdFromServer = $RESPONSE_FU['SUCCESS'][0]['REFERENCEID'];
		}
	}
?>