<?php
if(!extension_loaded('curl')){
	trigger_error('cURL extension must be loaded.',E_USER_ERROR);
}

if(!defined('VERSION'))
	define('VERSION','v1');

if(!defined('APIURL'))
	define('APIURL','http://api.unlockcodesource.com/'.VERSION.'/');

/**
 * Unlockcodesource API Wrapper
 * 
 * Use this class for a simplified version of the Unlockcodesource API. You must have curl installed to use this class.
 */
class UCS{
	private $apikey = null;

	/**
	 * Setting api key to communicate with Unlockcodesource server
	 * 
	 * @param string $apikey 
	 */
	public function setApikey($apikey){
		$this->apikey = $apikey;
	}

	/**
	 * Get account information
	 * 
	 * @return array
	 */
	public function getAccount(){
		return $this->curlRequest('GET',APIURL.'accounts');
	}

	/**
	 * Get unlock tools available for your account. The id can be specified
	 * to return a specific tool
	 *  
	 * @param int $id (optional) tool id
	 * @return array
	 */
	public function getTools($id = null){
		return ($id != null) ?
					$this->curlRequest('GET',APIURL.'tools/'.$id):
						$this->curlRequest('GET',APIURL.'tools');
	}

	/**
	 * Get pending orders 
	 * 
	 * @return array
	 */
	public function getPendingOrders(){
		return $this->curlRequest('GET',APIURL.'orders/pending');
	}

	/**
	 * Get order history, results are returned in descending order
	 *  
	 * @return array
	 */
	public function getOrderHistory(){
		return $this->curlRequest('GET',APIURL.'orders');
	}

	/**
	 * Get order information from order id
	 * 
	 * @param int $id order id
	 * @return array
	 */
	public function getOrder($id = null){
		if($id !== null)
			return $this->curlRequest('GET',APIURL.'orders/'.$id);
		return null;
	}

	/**
	 * Search an order based on field.
	 * Available fields: imei, orderId
	 * 
	 * Ex: searchOrder('orderId','11111');
	 *  
	 * @param string $field
	 * @param mixed $search
	 * @return array
	 */
	public function searchOrder($field, $search){
		return $this->curlRequest('GET',APIURL.'orders/search?'.$field.'='.$search);
	}

	/**
	 * Get instrucitons for a make
	 * 
	 * @param int $id (optional) make id
	 * @return array
	 */
	public function getInstructions($id = null){
		return ($id != null) ? 
			$this->curlRequest('GET',APIURL.'instructions/'.$id):
				$this->curlRequest('GET',APIURL.'instructions');
	}

	/**
	 * Submit an order using this method. Tool and imei must be entered. Params array may be 
	 * required depending on tool, please use 'getTools' method to verify.
	 *  
	 * @param int $toolId tool id
	 * @param array $imeis an array of imeis
	 * @param array $params make, model, etc
	 * @param string $notes (optional) any information you want to put in 
	 * @param string $optEmail 
	 * @return array
	 */
	public function submitOrder($toolId, $imeis, $params = NULL, $notes = NULL, $optEmail = NULL){
		$pass = true;
		if(!$toolId)
			$pass = false;

		if(!isset($imeis) || !$imeis){
			$pass = false;
		}

		if($pass){
			$data['toolId'] = $toolId;
			$data['imeis'] = $imeis;
			$data['params'] = $params;
			$data['notes'] = $notes;
			$data['optEmail'] = $optEmail;

			return $this->submitOrderData($data);
		}

		$status['status'] = 'ERROR';
		$status['message'] = 'There is an error with your order submission';
		return $status;
	}

	/**
	 * Submit an order using this method by submitting a data array. Tool and imei must be entered. 
	 * Other parameters maybe be required, use 'getTools' method to verify tool.
	 * Fields in array:
	 * toolId
	 * imeis
	 * notes		optional
	 * optEmail		optional (specify a email to send results to)
	 * params => 	(make,model, ...)
	 * 	 
	 * @param array $data order information 
	 * @return array
	 */
	public function submitOrderData($data){
		// check all fields are set
		$pass = true;
		if(!isset($data['toolId']) || $data['toolId'] == null)
			$pass = false;

		if(!isset($data['imeis']) || $data['imeis'] == null)
			$pass = false;

		if($pass){
			if(count($data['imeis']) == 1 && !is_array($data['imeis'])){
				$imei = $data['imeis'];
				unset($data['imeis']);
				$data['imeis'] = array($imei);
			}
			return $this->curlRequest('POST',APIURL.'orders',$data);
		}

		$status['status'] = 'ERROR';
		$status['message'] = 'There is an error with your order submission';
		return $status;
	}

	/**
	 * Request method
	 *  
	 * @param string $type http method
	 * @param string $url  
	 * @param array $data 
	 * @return array
	 */
	protected function curlRequest($type, $url, $data = null){
		if(!$this->apikey)
			return "You must enter a valid apikey";

		$url .= ('?apikey='.$this->apikey);
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		switch($type){
			case "POST":
				$json = json_encode($data);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($json))
				);
			break;

			case "PUT":
				$json = json_encode($data);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($json))
				);
			break;

			case "DELETE":
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				curl_setopt($ch, CURLOPT_POST, false);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
			break;
		}
		$response = json_decode(curl_exec($ch),true);
		curl_close ($ch);

		return $response;
	}
}

?>
