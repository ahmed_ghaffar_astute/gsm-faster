<?php
if (!extension_loaded('curl'))
{
    trigger_error('cURL extension not installed', E_USER_ERROR);
}
class API_samkey
{
    var $xmlData;
    var $xmlResult;
    var $debug;
    var $action;
    function __construct()
    {
        $this->xmlData = new DOMDocument();
    }
    function getResult()
    {
        return $this->xmlResult;
    }
    function action($action, $apiKey, $userName, $url, $credits = '')
    {
        if (is_string($action))
        {
			$posted = array('uName' => $userName, 'apiKey' => $apiKey, 'action' => $action, 'credits' => $credits, 'requestformat' => 'JSON');
			$crul = curl_init();
			curl_setopt($crul, CURLOPT_HEADER, false);
			curl_setopt($crul, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
			//curl_setopt($crul, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($crul, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($crul, CURLOPT_URL, $url . '/icell_api/index.php');
			curl_setopt($crul, CURLOPT_POST, true);
			curl_setopt($crul, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($crul, CURLOPT_POSTFIELDS, $posted);
			$response = curl_exec($crul);
			if (curl_errno($crul) != CURLE_OK)
			{
				echo curl_error($crul);
				curl_close($crul);
			}
			else
			{
				curl_close($crul);
				/*echo "<textarea rows='20' cols='200'> ";
				print_r($response);
				echo "</textarea>";*/
				return (json_decode($response, true));
			}
        }
        return false;
    }
}
?>
