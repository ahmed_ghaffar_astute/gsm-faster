<?php
/* Include the library */
require_once('API.php');

if($myNetworkId == 22)
	$mobileId = '8280';
else if($myNetworkId == 64)
	$mobileId = '7497';
else if($myNetworkId == 44)
	$mobileId = '9889';

if($myNetworkId == 48 || $myNetworkId == 26 || $myNetworkId == 64 || $myNetworkId == 44 || $myNetworkId == 22)
{
	if($mobileId == '')
		$mobileId = '1029';
}

$datarr = array(); //keys are xml elements and the values are value of the xml element
$datarr['Tool'] = $extNwkId;
$datarr['IMEI'] = $myIMEI;
if($cstmFld != '')
	$datarr['MEP'] = $cstmFld;
if($cstmFld != '')
	$datarr['PRD'] = $cstmFld;
if($mobileId != '')
	$datarr['Mobile'] = $mobileId;
/*
if($modelValue != '')
	$datarr['Mobile'] = $modelValue;
if($network != '')
	$datarr['Network'] = $network;
if($providerId != '')
	$datarr['Provider'] = $providerId;
if($pin != '')
	$datarr['PIN'] = $pin;
if($kbh != '')
	$datarr['KBH'] = $kbh;
if($type != '')
	$datarr['Type'] = $type;
if($mobileId != '')
	$datarr['mobileId'] = $mobileId;
if($locks != '')
	$datarr['Locks'] = $locks;

echo '<pre>';
print_r($datarr);
echo '</pre>';
exit;
*/
$XML = @UnlockBase::CallAPI($apiKey, $serverURL, 'PlaceOrder', $datarr);
/*
$XML = @UnlockBase::CallAPI($apiKey, $serverURL, 'PlaceOrder', array('Tool' => $toolForUB, 'IMEI' => $myIMEI, 'Mobile' => $modelValue, 'MEP' => $mepValue, 
							'Network' => $network, 'Provider' => $providerId, 'PIN' => $pin, 'KBH' => $kbh, 'PRD' => $prd, 'Type' => $type, 'mobileId' => $mobileId, 
							'Locks' => $locks));
*/
if (is_string($XML))
{
	/* Parse the XML stream */
	$Data = UnlockBase::ParseXML($XML);
	
	if (is_array($Data))
	{
		if (isset($Data['Error']))
		{
			/* The API has returned an error */
			$msgFromServer = 'API error : ' . htmlspecialchars($Data['Error']);
			$ifCodeSentToServer = 0;
		}
		else
		{
			$ifCodeSentToServer = 1;
			$msgFromServer = htmlspecialchars($Data['Success']);
		}
	}
	else
	{
		/* Parsing error */
		$ifCodeSentToServer = 0;
		$msgFromServer = 'Could not parse the XML stream';
	}
}
else
{
	/* Communication error */
	$ifCodeSentToServer = 0;
	$msgFromServer = 'Could not communicate with the api';
}
?>