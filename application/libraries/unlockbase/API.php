<?php
	/*
                      _/    _/            _/                      _/        _/_/_/
                     _/    _/  _/_/_/    _/    _/_/      _/_/_/  _/  _/    _/    _/    _/_/_/    _/_/_/    _/_/
                    _/    _/  _/    _/  _/  _/    _/  _/        _/_/      _/_/_/    _/    _/  _/_/      _/_/_/_/
                   _/    _/  _/    _/  _/  _/    _/  _/        _/  _/    _/    _/  _/    _/      _/_/  _/
                    _/_/    _/    _/  _/    _/_/      _/_/_/  _/    _/  _/_/_/      _/_/_/  _/_/_/      _/_/_/

                       AAA               PPPPPPPPPPPPPPPPP   IIIIIIIIII                      222222222222222
                      A:::A              P::::::::::::::::P  I::::::::I                     2:::::::::::::::22
                     A:::::A             P::::::PPPPPP:::::P I::::::::I                     2::::::222222:::::2
                    A:::::::A            PP:::::P     P:::::PII::::::II                     2222222     2:::::2
                   A:::::::::A             P::::P     P:::::P  I::::I    vvvvvvv           vvvvvvv      2:::::2
                  A:::::A:::::A            P::::P     P:::::P  I::::I     v:::::v         v:::::v       2:::::2
                 A:::::A A:::::A           P::::PPPPPP:::::P   I::::I      v:::::v       v:::::v     2222::::2
                A:::::A   A:::::A          P:::::::::::::PP    I::::I       v:::::v     v:::::v 22222::::::22
               A:::::A     A:::::A         P::::PPPPPPPPP      I::::I        v:::::v   v:::::v22::::::::222
              A:::::AAAAAAAAA:::::A        P::::P              I::::I         v:::::v v:::::v2:::::22222
             A:::::::::::::::::::::A       P::::P              I::::I          v:::::v:::::v2:::::2
            A:::::AAAAAAAAAAAAA:::::A      P::::P              I::::I           v:::::::::v 2:::::2
           A:::::A             A:::::A   PP::::::PP          II::::::II          v:::::::v  2:::::2       222222
          A:::::A               A:::::A  P::::::::P          I::::::::I           v:::::v   2::::::2222222:::::2
         A:::::A                 A:::::A P::::::::P          I::::::::I            v:::v    2::::::::::::::::::2
        AAAAAAA                   AAAAAAAPPPPPPPPPP          IIIIIIIIII             vvv     22222222222222222222


		This is a PHP 4/5 library for connecting the UnlockBase API (v2)

		It supposes that you have cURL installed.
		If it is not the case, please ask your system administrator.
		More information : http://www.php.net/curl

		If you need assistance, please contact tech@unlockbase.com
	*/

	/* Enter your API key here */

//	define('UNLOCKBASE_API_KEY', $apiKey);
	
	/* Set this value to true if something goes wrong and you want to display error messages */
	
	define('UNLOCKBASE_API_DEBUG', false);
	
	/* This is the url of the api, don't change it */

//	define('UNLOCKBASE_API_URL', $serverURL);
	
	/*
		To allow UnlockBase class functions to be called statically - e.g. UnlockBase::Function()
		and to keep backward compatibility with PHP4, some variables to be shared between
		member functions will be allocated globally. Their names are listed below.
		This is just for information, as they almost certainly won't interact in any way with your scripts.
		If you did not understand the above, this is not important :-)
	*/
	
	define('UNLOCKBASE_VARIABLE_ERROR',    '_UnlockBaseError'    );
	define('UNLOCKBASE_VARIABLE_ARRAY',    '_UnlockBaseArray'    );
	define('UNLOCKBASE_VARIABLE_POINTERS', '_UnlockBasePointers' );
	
	/* Check that cURL is installed */
	
	if (! extension_loaded('curl'))
	{
		trigger_error('cURL extension not installed', E_USER_ERROR);
	}
	
	/* Here comes the main class */
	
	class UnlockBase
	{
		/*
			mixed UnlockBase::CallAPI (string $Action, array $Parameters)
			Call the UnlockBase API.
			Returns the xml stream sent by the UnlockBase server
			Or false if an error occurs
		*/

		public static function CallAPI ($apiKey, $url, $Action, $Parameters = array() )
		{
			if (is_string($Action))
			{
				if (is_array($Parameters))
				{
					/* Add the API Key and the Action to the parameters */
					$Parameters['Key'] = $apiKey;
					$Parameters['Action'] = $Action;

					/* Prepare the cURL session */
					$Ch = curl_init($url);		
					curl_setopt($Ch, CURLOPT_CONNECTTIMEOUT, 10);
					curl_setopt($Ch, CURLOPT_TIMEOUT, 30);
					curl_setopt($Ch, CURLOPT_HEADER, false);
					curl_setopt($Ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($Ch, CURLOPT_ENCODING, '');
					curl_setopt($Ch, CURLOPT_POST, true);
					curl_setopt($Ch, CURLOPT_POSTFIELDS, UnlockBase::BuildQuery($Parameters));
					
					/* Perform the session */
					$Data = curl_exec($Ch);
					
					if (UNLOCKBASE_API_DEBUG && curl_errno($Ch) != CURLE_OK)
					{
						/* If an error occurred, report it in debug mode */
						trigger_error(curl_error($Ch), E_USER_WARNING);
					}
					
					/* Close the session */
					curl_close($Ch);
					
					/* Return the data, or false if an error occurred */
					return $Data;
				}
				else trigger_error('Parameters must be an array', E_USER_WARNING);
			}
			else trigger_error('Action must be a string', E_USER_WARNING);
			
			return false;
		}

		/*
			mixed UnlockBase::ParseXML (string $XML)
			Parse an XML stream from the UnlockBase API.
			Returns an associative array of the parsed XML string
			Or false if an error occurs
		*/
		
		public static function ParseXML ( $XML )
		{
			if (! is_string($XML))
			{
				/* If the argument is not a string, report the error in debug mode & stop here */
				if (UNLOCKBASE_API_DEBUG) trigger_error('Invalid argument supplied for UnlockBase::ParseXML()', E_USER_WARNING);
				return false;
			}
			$XML = simplexml_load_string($XML);
			$json  = json_encode($XML);
			$xmlArr = json_decode($json, true);
			return $xmlArr; 
		}

		/*
			bool UnlockBase::CheckEmail (string $Email)
			Check the validity of an email address
			This function is *not* RFC 2822 compliant, but instead reflects today's email reality
			Returns true if the email address seems correct, false otherwise
		*/
		
		public static function CheckEmail ( $Email )
		{
			return (bool) preg_match('/^[0-9a-z_\\-\\.]+@([0-9a-z][0-9a-z\\-]*[0-9a-z]\\.)+[a-z]{2,}$/i', $Email);
		}

		/*
			bool UnlockBase::CheckIMEI (string $IMEI, bool $Checksum)
			Check a 15-digit IMEI serial number.
			You are free to verify the checksum, or not;
			Bad checksums are 99% likely to provide unavailable unlock codes (exceptions exist, however)
			Returns true if the IMEI seems correct, false otherwise
		*/
		
		public static function CheckIMEI ( $IMEI, $Checksum = true )
		{
			if (is_string($IMEI))
			{
				if (ereg('^[0-9]{15}$', $IMEI))
				{
					if (! $Checksum) return true;

					for ($i = 0, $Sum = 0; $i < 14; $i++)
					{
						$Tmp = $IMEI[$i] * ( ($i % 2) + 1 );
						$Sum += ($Tmp % 10) + intval($Tmp / 10);
					}
					
					return ( ( ( 10 - ( $Sum % 10 ) ) % 10 ) == $IMEI[14] );
				}
			}
			
			return false;
		}
		
		/*
			bool UnlockBase::CheckProviderID (string $ProviderID)
			Verify an Alcatel Provider ID
			Returns true if the Provider ID seems correct, false otherwise
		*/
		
		public static function CheckProviderID ( $ProviderID )
		{
			return (is_string($ProviderID) && eregi('^[0-9a-z]{4,5}\\-[0-9a-z]{7}$', $ProviderID));
		}
		
		/*
			bool UnlockBase::CheckMEP_PRD (string $Type, string $String)
			Check a MEP/PRD number before submitting it to the API
			$Type is either 'MEP' or 'PRD'
			Returns true if the MEP/PRD seems correct, false otherwise
		*/
		
		public static function CheckMEP_PRD( $Type, $String )
		{
			return ereg('^' . $Type . '\\-[0-9]{5}\\-[0-9]{3}$', $String);
		}
	
		/* Internal functions - do not care */
		
		public static function BuildQuery ( $Parameters )
		{
			if (function_exists('http_build_query'))
			{ 
				/* PHP 5 */
				return http_build_query($Parameters);
			}
			else
			{
				/* PHP 4 */
				$Data = array();
				foreach ($Parameters as $Name => $Value) array_push($Data, urlencode($Name) . '=' . urlencode($Value));
				return implode('&', $Data);
			}
		}

		public static function XML_Start ( $Parser, $Name, $Attributes )
		{
			/* Globalize variables */
			global ${UNLOCKBASE_VARIABLE_ERROR};
			global ${UNLOCKBASE_VARIABLE_ARRAY};
			global ${UNLOCKBASE_VARIABLE_POINTERS};
			
			/* Do nothing if an error occurred previously */
			if (${UNLOCKBASE_VARIABLE_ERROR}) return;

			if (count( ${UNLOCKBASE_VARIABLE_POINTERS} ) == 0)
			{
				/* Root Element : create the first pointer to the array */
				${UNLOCKBASE_VARIABLE_POINTERS}[] =& ${UNLOCKBASE_VARIABLE_ARRAY};
			}
			else
			{
				/* Get the latest pointer */
				$Pointer =& ${UNLOCKBASE_VARIABLE_POINTERS} [ count( ${UNLOCKBASE_VARIABLE_POINTERS} ) -1 ];
				
				if (is_null($Pointer))
				{
					/* This is the first sub-tag with that name, create the new container array for it */
					$Pointer[] = array();
					
					/* Replace the latest pointer, point to the first item of the new container */
					${UNLOCKBASE_VARIABLE_POINTERS}[ count(${UNLOCKBASE_VARIABLE_POINTERS}) -1 ] =& $Pointer[0];
					$Pointer =& $Pointer[0];
				}
				elseif (is_array($Pointer))
				{
					if (isset($Pointer[$Name]))
					{
						if (! is_array($Pointer[$Name]))
						{
							/* Unrecognized XML stream */
							${UNLOCKBASE_VARIABLE_ERROR} = true;
							return;
						}
						
						/* The tag is already known, add an item to the array and create a pointer to it */
						$Pointer[$Name][] = array();
						${UNLOCKBASE_VARIABLE_POINTERS}[] =& $Pointer[$Name][ count($Pointer[$Name]) -1 ];
						return;
					}
				}
				else
				{
					/* Unrecognized XML stream */
					${UNLOCKBASE_VARIABLE_ERROR} = true;
					return;
				}
				
				/* Set the default value and create a pointer to it */
				$Pointer[$Name] = NULL;
				${UNLOCKBASE_VARIABLE_POINTERS}[] =& $Pointer[$Name];
			}
		}
		
		public static function XML_End ( $Parser, $Name )
		{
			/* Globalize variables */
			global ${UNLOCKBASE_VARIABLE_ERROR};
			global ${UNLOCKBASE_VARIABLE_POINTERS};

			/* Do nothing if an error occurred previously */
			if (${UNLOCKBASE_VARIABLE_ERROR}) return;

			/* Remove the latest pointer */
			array_pop( ${UNLOCKBASE_VARIABLE_POINTERS} );
		}
		
		public static function XML_CData ( $Parser, $Data )
		{
			/* Ignore whitespaces */
			if (rtrim($Data) == '') return;

			/* Globalize variables */
			global ${UNLOCKBASE_VARIABLE_ERROR};
			global ${UNLOCKBASE_VARIABLE_POINTERS};

			/* Do nothing if an error occurred previously */
			if (${UNLOCKBASE_VARIABLE_ERROR}) return;
			
			/* Get the latest pointer */
			$Pointer =& ${UNLOCKBASE_VARIABLE_POINTERS} [ count( ${UNLOCKBASE_VARIABLE_POINTERS} ) -1 ];
			
			if (is_array($Pointer))
			{
				/* Unrecognized XML stream, should be null or string here */
				${UNLOCKBASE_VARIABLE_ERROR} = true;
				return;
			}
			
			/* Append the character data */
			$Pointer .= $Data;
		}
	}
?>