<?php
require('API.php');
/* Call the API */
$XML = @UnlockBase::CallAPI($ub_apiKey, $ub_serverURL, 'GetOrders');
if (is_string($XML))
{
	/* Parse the XML stream */
	$Data = UnlockBase::ParseXML($XML);
	
	if (is_array($Data))
	{
		if (isset($Data['Error']))
		{
			/* The API has returned an error */
			print('API error : ' . htmlspecialchars($Data['Error']));
		}
		else
		{
			$UB_CODES = array();
			foreach ($Data['Order'] as $Order)
			{
				$UB_CODES[$Order['IMEI']] = $Order['Status'].'~'.$Order['Available'].'~'.$Order['Codes'];
			}
		}
	}
	else
	{
		/* Parsing error */
		print('Could not parse the XML stream');
	}
}
else
{
	/* Communication error */
	print('Could not communicate with the api');
}
?>