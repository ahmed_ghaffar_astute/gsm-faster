<?php
require_once "codeskpro/codeskapi.class.php";

class CI_codeskpro extends CodeskApi
{
    public function __construct() {
        parent::__construct();

    }
    public function action($action, $apiKey, $userName, $url, $arr = array()){
        $api = new CodeskApi();
        return $api->action($action, $apiKey, $userName, $url, $arr);
    }
}
