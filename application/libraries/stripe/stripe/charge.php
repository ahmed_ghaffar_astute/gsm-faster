<?php 
//let's say each article costs 15.00 bucks
include '../../header_includes.php';
$pMId = isset($_POST['pMId']) ? check_input($_POST['pMId'], $objDBCD14->dbh) : '0';
$amount = isset($_POST['amount']) ? check_input($_POST['amount'], $objDBCD14->dbh) : '0';
$currency = isset($_POST['currency']) ? check_input($_POST['currency'], $objDBCD14->dbh) : '';
$order_id = isset($_POST['order_id']) ? check_input($_POST['order_id'], $objDBCD14->dbh) : '0';
$invId = isset($_POST['invId']) ? check_input($_POST['invId'], $objDBCD14->dbh) : '0';

$apiKey = '';
$row = $objDBCD14->queryUniqueObject("SELECT APIUserName FROM tbl_gf_payment_methods WHERE PaymentMethodId = '$pMId'");
if (isset($row->APIUserName) && $row->APIUserName != '')
{
	$apiKey = $row->APIUserName;
}
try
{
	require_once('Stripe/lib/Stripe.php');
	Stripe::setApiKey($apiKey);
	$charge = Stripe_Charge::create(array(
        "amount" => (int)$amount * 100,
        "currency" => $currency,
        "card" => $_POST['stripeToken'],
        "description" => "Order #".$order_id
	));

	if(isset($_POST['customer_email']))
	{
		$email = isset($_POST['customer_email']) ? check_input($_POST['customer_email'], $objDBCD14->dbh) : '';
		$name = isset($_POST['name']) ? check_input($_POST['name'], $objDBCD14->dbh) : 0;

		$objDBCD14->execute("UPDATE tbl_gf_retail_payments SET PaymentStatus = 2 WHERE RetailPaymentId = '$invId'");
		$rs = $objDBCD14->queryUniqueObject("SELECT Email, Name, IMEINo, PackageTitle, IP, RequestedAt, A.PackageId, A.Price FROM tbl_gf_retail_orders A, 
				tbl_gf_retail_services B WHERE A.PackageId = B.PackageId AND PaymentId = '$invId'");
		if (isset($rs->Email) && $rs->Email != '')
		{
			newIMEIRetailOrderEmail($rs->Email, $rs->Name, $rs->PackageTitle, $rs->IMEINo, $rs->IP, $rs->RequestedAt, $rs->PackageId, $rs->Price, $objDBCD14);
		}
		echo "<script>window.location.href='../../order_placed.php?oId=".$order_id."';</script>";
	}
		  
}
	
catch(Stripe_CardError $e) {
 echo '<html><body><div align="center"><img src="close.png"/><br><p style="text-align:center;">Votre paiement pour cette transaction à échoué, SVP veuillez recommencer !</p><a class="center button" href="../../index.php">RECOMMENCER!</a></div></body></html>';
 
}

//catch the errors in any way you like

 catch (Stripe_InvalidRequestError $e) {
  // Invalid parameters were supplied to Stripe's API

echo '<html><body><div align="center"><img src="close.png"/><br><p style="text-align:center;">Votre paiement pour cette transaction à échoué, SVP veuillez recommencer !</p><a class="center button" href="../../index.php">RECOMMENCER!</a></div></body></html>';	
 
}
catch (Stripe_AuthenticationError $e) {
  // Authentication with Stripe's API failed
  // (maybe you changed API keys recently)

}
catch (Stripe_ApiConnectionError $e) {
  // Network communication with Stripe failed
echo '<html><body><div align="center"><img src="close.png"/><br><p style="text-align:center;">Votre paiement pour cette transaction à échoué, SVP veuillez recommencer !</p><a class="center button" href="../../index.php">RECOMMENCER!</a></div></body></html>';	
 
}
catch (Stripe_Error $e) {
 
echo '<html><body><div align="center"><img src="close.png"/><br><p style="text-align:center;">Votre paiement pour cette transaction à échoué, SVP veuillez recommencer !</p><a class="center button" href="../../index.php">RECOMMENCER!</a></div></body></html>';	
 
  // Display a very generic error to the user, and maybe send
  // yourself an email
}
catch (Exception $e) {
echo '<html><body><div align="center"><img src="close.png"/><br><p style="text-align:center;">Votre paiement pour cette transaction à échoué, SVP veuillez recommencer !</p><a class="center button" href="../../index.php">RECOMMENCER!</a></div></body></html>';	
  // Something else happened, completely unrelated to Stripe
}
?>