<?php 
//let's say each article costs 15.00 bucks
require('../../include/php/startsession.php');
require("../../include/php/checkexpiry.php");
@header('P3P: CP="CAO PSA OUR"');
include('../../include/php/config.php');
include('../../'.$BACKEND_FOLDER.'include/php/config.php');
include('../../'.$BACKEND_FOLDER.'include/php/functions.php');
include('../../'.$BACKEND_FOLDER."include/php/db_c_n_f_g.php");
include('../../'.$BACKEND_FOLDER."include/php/cd14db.class.php");
$objDBCD14 = new CD14DB($DB_BASE, $DB_SERVER, $DB_USER, $DB_PASS);
$pMId = isset($_POST['pMId']) ? check_input($_POST['pMId'], $objDBCD14->dbh) : '0';
$amount = isset($_POST['amount']) ? check_input($_POST['amount'], $objDBCD14->dbh) : '0';
$currency = isset($_POST['currency']) ? check_input($_POST['currency'], $objDBCD14->dbh) : '';
$invId = isset($_POST['invId']) ? check_input($_POST['invId'], $objDBCD14->dbh) : '0';

$apiKey = '';
$row = $objDBCD14->queryUniqueObject("SELECT APIUserName FROM tbl_gf_payment_methods WHERE PaymentMethodId = '$pMId'");
if (isset($row->APIUserName) && $row->APIUserName != '')
{
	$apiKey = $row->APIUserName;
}
try
{
	require_once('Stripe/lib/Stripe.php');
	Stripe::setApiKey($apiKey);
	$stripeResponse = Stripe_Charge::create(array(
        "amount" => (int)$amount * 100,
        "currency" => $currency,
        "card" => $_POST['stripeToken'],
        "description" => "Order #".$invId
	));
	echo '<pre>';
	print_r($stripeResponse);
	echo '</pre>';
	
	if(isset($_POST['invId']))
	{
	    if ($stripeResponse['amount_refunded'] == 0 && empty($stripeResponse['failure_code']) && $stripeResponse['paid'] == 1 && $stripeResponse['captured'] == 1 && $stripeResponse['status'] == 'succeeded')
		{
//        	$successMessage = "Stripe payment is completed successfully. The TXN ID is " . $stripeResponse["balance_transaction"];
			$transactionId = $stripeResponse["balance_transaction"];
			$rs = $objDBCD14->queryUniqueObject("SELECT Credits, UserId, Amount, UserName, Currency, DATE(PaymentDtTm) AS PaymentDtTm, B.PaymentMethod, B.Username 
				FROM tbl_gf_payments A, tbl_gf_payment_methods B WHERE A.PaymentMethod = B.PaymentMethodId AND PaymentId = '$invId' AND PayMethodTypeId = '11' 
				AND PaymentStatus = 1");
			if (isset($rs->UserId) && $rs->UserId != '')
			{
				include($BACKEND_FOLDER."include/php/crypt.php");
				$crypt = new crypt;
				$userId = $rs->UserId;
				$crypt->crypt_key($userId);
				$myCredits = $crypt->decrypt($rs->Credits);
				if($myCredits == '')
					$myCredits = '0';
				$INV_AMOUNT = $crypt->decrypt($rs->Amount);
				$INV_CURRENCY = stripslashes($rs->Currency);
				$PAYMENT_METHOD = stripslashes($rs->PaymentMethod);
				$INV_DT = $rs->PaymentDtTm;
				//sendMail22('hammad.akbar@gmail.com', 'PayPal IPN - GSMFASTEST', 'Support@gsmfastest.com', 'PayPal IPN POST Values', $INV_AMOUNT.'==='.$PAYMENT_AMOUNT);				
				if($INV_AMOUNT == $PAYMENT_AMOUNT && $receiver_email == $rs->UserName)
				{
					$credits = 0;
					$userName = '';
					$userEmail = '';
					$rsCredits = $objDBCD14->queryUniqueObject("SELECT UserEmail, UserName, CONCAT(FirstName, ' ', LastName) AS Name, Credits, AutoFillCredits FROM tbl_gf_users 
					WHERE UserId = '$userId'");
					if (isset($rsCredits->Credits) && $rsCredits->Credits != '')
					{
						$credits = $rsCredits->Credits;
						$userName = $rsCredits->Name;
						$userEmail = $rsCredits->UserEmail;
						$uName = $rsCredits->UserName;
						$autoFill = $rsCredits->AutoFillCredits;
					}
					if($autoFill == '1')
					{
						$decCredits = $crypt->decrypt($credits);
						$decCredits += $myCredits;
						$encCredits = $crypt->encrypt($decCredits);
					
						$objDBCD14->execute("UPDATE tbl_gf_users SET Credits = '$encCredits' WHERE UserId = '$userId'");
						$dtTm = setDtTmWRTYourCountry($objDBCD14);
					
						$hstDesc = "+ Add Funds (Invoice #$invId)";
						$objDBCD14->execute("INSERT INTO tbl_gf_credit_history SET UserId = '$userId', Credits = '$myCredits', 
						Description = '$hstDesc', IMEINo = '', HistoryDtTm = '$dtTm', CreditsLeft = '$encCredits', PaymentId = '$invId'");
					
						$objDBCD14->execute("UPDATE tbl_gf_payments SET ReceiverEmail = '$receiver_email', PayerEmail = '$payer_email', TransactionId='$transactionId', 
						PaymentStatus = 2, CreditsTransferred = 1, UpdatedAt = '$dtTm', ShippingAddress = '$shippingAddr' WHERE PaymentId = '$invId'");
						send_push_notification($myCredits.' Credits have been added in your account!', $userId, $objDBCD14);
	
						$arr = getEmailDetails($objDBCD14);
						//========================== CHECK LAST PAYER EMAIL ================================================//
						if($payer_email != '')
							checkLastPayerEmail($invId, $userId, $payer_email, $arr, $uName, $BACKEND_FOLDER, $objDBCD14);
						//========================== CHECK LAST PAYER EMAIL ================================================//
						if($userEmail != '')
						{
							invoiceEmail($userEmail, $userName, $invId, $myCredits, $INV_AMOUNT, $INV_CURRENCY, $INV_DT, $PAYMENT_METHOD, $objDBCD14, 1);
							invoiceEmail($arr[4], 'Admin', $invId, $myCredits, $INV_AMOUNT, $INV_CURRENCY, $INV_DT, $PAYMENT_METHOD, $objDBCD14, 1, $BACKEND_FOLDER, $uName);
						}
					}
				}
			}

/*
			$rs = $objDBCD14->queryUniqueObject("SELECT Credits, UserId FROM tbl_gf_payments WHERE PaymentId = '$invId' AND PaymentStatus = 1");
			if (isset($rs->UserId) && $rs->UserId != '')
			{
				include('../../'.$BACKEND_FOLDER."include/php/crypt.php");
				$crypt = new crypt;
				$userId = $rs->UserId;
				$crypt->crypt_key($userId);
				$myCredits = $crypt->decrypt($rs->Credits);
				if($myCredits == '')
					$myCredits = '0';
			
				$credits = 0;
				$userName = '';
				$userEmail = '';
				$rsCredits = $objDBCD14->queryUniqueObject("SELECT UserEmail, CONCAT(FirstName, ' ', LastName) AS Name, Credits FROM tbl_gf_users 
				WHERE UserId = '$userId'");
				if (isset($rsCredits->Credits) && $rsCredits->Credits != '')
				{
					$credits = $rsCredits->Credits;
					$userName = $rsCredits->Name;
					$userEmail = $rsCredits->UserEmail;
				}
				$decCredits = $crypt->decrypt($credits);
				$decCredits += $myCredits;
				$encCredits = $crypt->encrypt($decCredits);
			
				$objDBCD14->execute("UPDATE tbl_gf_users SET Credits = '$encCredits' WHERE UserId = '$userId'");
				$dtTm = setDtTmWRTYourCountry($objDBCD14);
			
				$hstDesc = "+ Add Funds (Invoice #$invId)";
				$objDBCD14->execute("INSERT INTO tbl_gf_credit_history SET UserId = '$userId', Credits = '$myCredits', 
				Description = '$hstDesc', IMEINo = '', HistoryDtTm = '$dtTm', CreditsLeft = '$encCredits', PaymentId = '$invId'");
			
				$objDBCD14->execute("UPDATE tbl_gf_payments SET ReceiverEmail = '$receiver_email', PayerEmail = '$payer_email', TransactionId = '$transactionId', 
				PaymentStatus = 2, CreditsTransferred = 1, UpdatedAt = '$dtTm', ShippingAddress = '$shippingAddr' WHERE PaymentId = '$invId'");
				
				if($userEmail != '')
				{
					$arr = getEmailDetails($objDBCD14);
					$emailMsg = '<p><b>Credits Added:</b>: '.$myCredits.'</p>
					<p><b>Current Balance:</b> '.number_format($decCredits, 2, '.', '').'</p>';
					sendGeneralEmail($userEmail, $arr[0], $arr[1], $userName, 'Your Account balance updated', 'Thank you for funding your account with '.$arr[2], $emailMsg, $objDBCD14, $arr[4]);
					//saveEmailLog($userId, 'Account Balance Updated', $emailMsg, $objDBCD14);
	
				}
			}*/
	    }

		$objDBCD14->close();				
		echo "<script>window.location.href='../../invoices.php';</script>";
	}
		  
}
	
catch(Stripe_CardError $e) {
 echo '<div class="center"><img src="close.png"/><br><p style="text-align:center;">Votre paiement pour cette transaction à échoué, SVP veuillez recommencer !</p><a class="center button" href="../../buycredits.php">Try Again!</a></div>';	
 
}

//catch the errors in any way you like

 catch (Stripe_InvalidRequestError $e) {
  // Invalid parameters were supplied to Stripe's API

echo '<div class="center"><img src="close.png"/><br><p style="text-align:center;">Votre paiement pour cette transaction à échoué, SVP veuillez recommencer !</p><a class="center button" href="../../buycredits.php">Please Try Again!</a></div>';	
 
}
catch (Stripe_AuthenticationError $e) {
  // Authentication with Stripe's API failed
  // (maybe you changed API keys recently)

}
catch (Stripe_ApiConnectionError $e) {
  // Network communication with Stripe failed
echo '<div class="center"><img src="close.png"/><br><p style="text-align:center;">Votre paiement pour cette transaction à échoué, SVP veuillez recommencer !</p><a class="center button" href="../../buycredits.php">Try Again3!</a></div>';	
 
}
catch (Stripe_Error $e) {
 
echo '<div class="center"><img src="close.png"/><br><p style="text-align:center;">Votre paiement pour cette transaction à échoué, SVP veuillez recommencer !</p><a class="center button" href="../../buycredits.php">Try Again4!</a></div>';	
 
  // Display a very generic error to the user, and maybe send
  // yourself an email
}
catch (Exception $e) {
echo '<div class="center"><img src="close.png"/><br><p style="text-align:center;">Votre paiement pour cette transaction à échoué, SVP veuillez recommencer !</p><a class="center button" href="../../buycredits.php">Try Again5!</a></div>';	
  // Something else happened, completely unrelated to Stripe
}
?>