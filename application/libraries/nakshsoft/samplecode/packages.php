<?php
	include 'header.htm';
	require_once('api/arw_api.php');
	
	$objARWAPI = new ARWAPI();
	$objARWAPI->doAction('getpackages', array());
	$objARWAPI->XmlToArray($objARWAPI->getResult());
	$arrayData = $objARWAPI->createArray();
	if(isset($arrayData['error']) && sizeof($arrayData['error']) > 0)
	{
		echo '<b>'.$arrayData['error'][0].'</b>';
		exit;
	}

	$RESPONSE_ARR = array();
	if(isset($arrayData['Packages']['Package']) && sizeof($arrayData['Packages']['Package']) > 0)
		$RESPONSE_ARR = $arrayData['Packages']['Package'];
	$total = count($RESPONSE_ARR);
	echo '<table align="center" width="60%" class="tbl"><tr class="header"><th width="50%">ID</th><th width="50%">Package</th></tr>';
	for($count = 0; $count < $total; $count++)
	{
		$cssClass = $cssClass == '' ? 'class="alt"' : '';
		echo '<tr '.$cssClass.'><td>' . $RESPONSE_ARR[$count]['PackageId'] . '</td><td>' . $RESPONSE_ARR[$count]['PackageTitle'] . '</td></tr>';		
	}
	echo '</table>';
	include 'footer.htm';
?>