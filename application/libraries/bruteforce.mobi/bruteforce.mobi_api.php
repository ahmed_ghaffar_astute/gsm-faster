<?
	///////////////////////////////////////////////////////////////////////////
	//Functions
	///////////////////////////////////////////////////////////////////////////
	//-----------------------------
	//Return current queue in hours
	//-----------------------------
    function GetBalance(
		$api_url,
		$brute_email,     //brute_email   - main registered email on www.bruteforce.mobi
		$brute_password,  //brute_password- password to account on www.bruteforce.mobi
		$key,             //key,          - key for Encrypt, same on site in section API, 16 chars
		$keyIV            //keyIV         - keyIV for Encrypt same on site in section API, 16 chars
    )
    {
		$client = new SoapClient($api_url);

		$inpu_hash = Encrypt($brute_password . '||', $key, $keyIV);
		$result = $client->GetBalance(array(
			"brute_email" => $brute_email,
			"input_hash" => $inpu_hash
		));
		return $result;
    }
    function GetQueueSlowFast(
		$api_url,
		$brute_email,     //brute_email   - main registered email on www.bruteforce.mobi
		$brute_password,  //brute_password- password to account on www.bruteforce.mobi
		$key,             //key,          - key for Encrypt, same on site in section API, 16 chars
		$keyIV            //keyIV         - keyIV for Encrypt same on site in section API, 16 chars
    )
    {
		$client = new SoapClient($api_url);

		$inpu_hash = Encrypt($brute_password . '||', $key, $keyIV);
		//Call soap service [GetQueueSlowFast]
		$result = $client->GetQueueSlowFast(array(
			"brute_email" => $brute_email,
			"input_hash" => $inpu_hash
		));
		return $result;
    }
	//-----------------------------

	//Add job for calculation
	//-----------------------------
    function SetJob(
		$api_url,
        $brute_email,     //brute_email   - main registered email on www.bruteforce.mobi
        $brute_password,  //brute_password- password to account on www.bruteforce.mobi
        $add_email,       //add_email     - additional email for send calc result
        $log_type,        //log_type      - two parametrs "Slow", "Fast"
        $hash,            //hash          - 60 hex digital
        $imei,            //imei          - 15 digital
        $key,             //key,          - key for Encrypt, same on site in section API, 16 chars
        $keyIV            //keyIV         - keyIV for Encrypt same on site in section API, 16 chars
    )
    {
		$client = new SoapClient($api_url);

		$inpu_hash = Encrypt(
			$brute_password . '||' . 
			$add_email . '||' . 
			$log_type . '||' . 
			$hash . '||' . 
			$imei . '||'
			, $key, $keyIV);
		//Call soap service [SetJob]
		$result = $client->SetJob(array(
			"brute_email" => $brute_email,
			"input_hash" => $inpu_hash
		));
		return $result;
    }

	//Supported file format
	//IMEI = 999999999999999 - 15 digit
	//HASH = FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF- 40 hex decimal

	//sha - old format
	//file contain IMEI and HASH
	//[imei]
	//999999999999999
	//[hash]
	//FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF

	//sha - new format
	//file name contain IME
	//999999999999999.sha
	//file contain HASH
	//FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF

	//bcl
	//file contain IMEI and HASH
	//[Log]
	//imei=999999999999999
	//hash=FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF first 40 hex digital

	//log
	//file contain IMEI and HASH
	//999999999999999
	//FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF

	//log - other format
	//file contain IMEI and HASH
	//IMEI=999999999999999
	//HASH=FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF first 40 hex digital

	//fnx
	//[LOG]
	//IMEI=999999999999999
	//HASH=FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF

	//bat
	//oclhashcat
	//File name contain IMEI
	//oclHashcat_999999999999999_64.bat
	//file contain HASH and IMEI 14 digital
	//oclHashcat-lite64 FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF:003528510517285000 -1 00010203040506070809 -m 1900 ?1?1?1?1?1?1?1?1?1?1?1?1?1?1?1 -n 160 --outfile-format=1 --outfile=352851051728509_COD.txt
	//pause

	//-----------------------------
	
	//Add job for calculation from file
	//---------------------------------
    function SetJobFile(
		$api_url,
        $brute_email,     //brute_email   - main registered email on www.bruteforce.mobi
        $brute_password,  //brute_password- password to account on www.bruteforce.mobi
        $add_email,       //add_email     - additional email for send calc result
        $log_type,        //log_type      - two parametrs "Slow", "Fast"
        $file_name,       //file_name     - file name 
        $file_data,       //file_data     - Base64string
        $key,             //key,          - key for Encrypt, same on site in section API, 16 chars
        $keyIV            //keyIV         - keyIV for Encrypt same on site in section API, 16 chars
    )
    {
		$client = new SoapClient($api_url);

		$inpu_hash = Encrypt(
			$brute_password . '||' . 
			$add_email . '||' . 
			$log_type . '||' . 
			$file_name . '||' . 
			$file_data . '||'
			, $key, $keyIV);
		//Call soap service [SetJobFile]
		$result = $client->SetJobFile(array(
			"brute_email" => $brute_email,
			"input_hash" => $inpu_hash
		));
		return $result;
    }
	//-----------------------------

	//Check job status and get result calculation use JOB_API_ID
	//----------------------------------------------------------
    function GetJob(
		$api_url,
        $brute_email,     //brute_email   - main registered email on www.bruteforce.mobi
        $brute_password,  //brute_password- password to account on www.bruteforce.mobi
        $job_api_id,      //job_api_id    - Remembered [job_api_id]
        $key,             //key,          - key for Encrypt, same on site in section API, 16 chars
        $keyIV            //keyIV         - keyIV for Encrypt same on site in section API, 16 chars
    )
    {
		$client = new SoapClient($api_url);

		$inpu_hash = Encrypt($brute_password . '||' . $job_api_id . '||', $key, $keyIV);
		//Call soap service [GetJob]
		$result = $client->GetJob(array(
			"brute_email" => $brute_email,
			"input_hash" => $inpu_hash
		));
		return $result;
    }
	//-----------------------------
	
	//Check job status and get result calculation use IMEI
	//----------------------------------------------------
    function CheckJob(
		$api_url,
        $brute_email,     // main registered email on www.bruteforce.mobi
        $brute_password,  // password to account on www.bruteforce.mobi
        $imei,            // 15 digital
        $key,             // key for Encrypt, same on site in section API, 16 chars
        $keyIV            // keyIV for Encrypt same on site in section API, 16 chars
    )
    {
		$client = new SoapClient($api_url);

		$inpu_hash = Encrypt($brute_password . '||' . $imei . '||', $key, $keyIV);
		//Call soap service [CheckJob]
		$result = $client->CheckJob(array(
			"brute_email" => $brute_email,
			"input_hash" => $inpu_hash
		));
		return $result;
    }
	//-----------------------------
	
	/// Del loaded job if not started calculation use JOB_API_ID
	//----------------------------------------------------------
    function DelJob(
		$api_url,
        $brute_email,     //brute_email   - main registered email on www.bruteforce.mobi
        $brute_password,  //brute_password- password to account on www.bruteforce.mobi
        $job_api_id,      //job_api_id    - Remembered [job_api_id]
        $key,             //key,          - key for Encrypt, same on site in section API, 16 chars
        $keyIV            //keyIV         - keyIV for Encrypt same on site in section API, 16 chars
    )
    {
		$client = new SoapClient($api_url);

		$inpu_hash = Encrypt($brute_password . '||' . $job_api_id . '||', $key, $keyIV);
		//Call soap service [DelJob]
		$result = $client->DelJob(array(
			"brute_email" => $brute_email,
			"input_hash" => $inpu_hash
		));
		return $result;
    }
	//-----------------------------
	
	//Encrypt text
    function Encrypt($PlainText, $key, $keyIV)
    {
		$cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
		if (mcrypt_generic_init($cipher, $key, $keyIV) != -1)
		{
			$cipherText = mcrypt_generic($cipher,$PlainText );
			mcrypt_generic_deinit($cipher);
		}		
		return base64_encode($cipherText);
    }
?>