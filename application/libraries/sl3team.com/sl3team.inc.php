<?

/**
 * Description of SL3
 * Version 1.0
 * Release June-21-2013
 * @author CuongNQ
 * // CODE| Message
  //  0   | Account info
  //  1   | API Key is invalid
  //  2   | DATA is invalid
  //  3   | Hash done with nck
  //  4   | Hash processing
  //  5   | Hash in queue
  //  6   | Hash not found
  //  7   | Add hash success
  //  8   | Add hash failed
  //  9   | Hash existed in system
  //  10  | Credit not enough to add hash
  //  11  | Hash not found in system
  //  12  | Hash detail
  //  13  | Hash list
  //  16  | Hash are running fast service, no credit cost
  //  17  | Fast service update success
  //  18  | Hash are processing, unable to delete
  //  19  | Delete hash success
  //  20  | Delete hash failed
  //  21  | Hash are processing, unable to fast
  //  22  | Hash have been deleted
 */
class SL3_API {
    /**
     * API URL
     */

    const API_URL = "http://sl3team.com/api/";

    /**
     * API Key
     */
    private $api_key;
    private $format = "json";

    /**
     * Constructor
     */
    function __construct($api_key) {
        $this->api_key = $api_key;
    }

    /**
     * Connect method
     * Param: method, params
     */
    private function connect($method, $params = array()) {
        foreach ($params as $key => $value)
            $up[] = "$key=" . urlencode($value);

        $r = file_get_contents(isset($up) ?
                        self::API_URL . $method . '?key=' . $this->api_key . '&format=' . $this->format . "&" .
                        implode("&", $up) :
                        self::API_URL . $method . '?key=' . $this->api_key . '&format=' . $this->format);

        if ($r === false)
            return array(
                "msg_code" => '-1',
                "message" => "Couldn't connect to API"
            );

        if (empty($r))
            return array(
                "msg_code" => '-2',
                "message" => "No response from API server"
            );
        return json_decode($r, true);
    }

    /**
     * Get account information
     * @return type
     */
    public function accountInfo() {
        return $this->connect("account-info");
    }

    /**
     * Check IMEI status
     * @param type $imei
     * @return type
     */
    public function checkIMEI($imei) {
        return $this->connect("check-imei", array(
                    'imei' => $imei
        ));
    }

    /**
     * Add Hash
     * @param type $imei
     * @param type $hash
     * @param type $part_order: 0 is 0..100, 1 is 100..0
     * @param type $emails: e.g: test1@me.com,test2@me.com
     * @return type
     */
    public function addHash($imei, $hash, $order, $emails) {
        return $this->connect("add-hash", array(
                    'imei' => $imei,
                    'hash' => $hash,
                    'part_order' => $order,
                    'emails' => $emails
        ));
    }

    /**
     * Use fast service
     * @param type $hash_id
     * @return type
     */
    public function fastService($hash_id) {
        return $this->connect("fast-service", array(
                    'hash_id' => $hash_id
        ));
    }

    /**
     * Delete hash
     * @param type $hash_id
     * @return type
     */
    public function deleteHash($hash_id) {
        return $this->connect("delete-hash", array(
                    'hash_id' => $hash_id
        ));
    }

    /**
     * Check hash status
     * @param type $hash_id
     * @return type
     */
    public function checkHash($hash_id) {
        return $this->connect("check-hash", array(
                    'hash_id' => $hash_id
        ));
    }

    /**
     * Get processing list
     * @return type
     */
    public function getList() {
        return $this->connect("get-list");
    }
    
    /**
     * Parser msg code to Message
     * @param type $msg_code
     * @return string
     */
    public function getMessage($msg_code){
        switch ($msg_code) {
            case 0:
                return "Account info";
                break;
            case 1:
                return "API Key is invalid";
                break;
            case 2:
                return "DATA is invalid";
                break;
            case 3:
                return "Hash done with nck";
                break;
            case 4:
                return "Hash processing";
                break;
            case 5:
                return "Hash in queue";
                break;
            case 6:
                return "Hash not found";
                break;
            case 7:
                return "Add hash success";
                break;
            case 8:
                return "Add hash failed";
                break;
            case 9:
                return "Hash existed in system";
                break;
            case 10:
                return "Credit not enough to add hash";
                break;
            case 11:
                return "Hash not found in system";
                break;
            case 12:
                return "Hash detail";
                break;
            case 13:
                return "Hash list";
                break;
            case 16:
                return "Hash are running fast service, no credit cost";
                break;
            case 17:
                return "Fast service update success";
                break;
            case 18:
                return "Hash are processing, unable to delete";
                break;
            case 19:
                return "Delete hash success";
                break;
            case 20:
                return "Delete hash failed";
                break;
            case 21:
                return "Hash are processing, unable to fast";
                break;
            case 22:
                return "Hash have been deleted";
                break;
        }
    }

}

?>