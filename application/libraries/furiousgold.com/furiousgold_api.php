<?php
class furiousgold_api
{
	public function call_api($method, $apiUser, $apikey, $apiURL, $parameters = array())
	{
		if (function_exists('curl_init')) {
			$fields = array(
				'API_KEY' => $apikey, // from config.php file
				'API_USER' => $apiUser, // from config.php file
				'METHOD' => $method,
				'PARAMETERS' => $parameters
			);

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $apiURL); // sets the API url
			curl_setopt($ch, CURLOPT_USERAGENT, 'FuriousGold API Client'); // sets the user agent
			curl_setopt($ch, CURLOPT_POST, TRUE); // set the request is post
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields)); // set post fields
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // instruct to return the response
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10); // the number of seconds to wait while trying to connect to API server
			curl_setopt($ch, CURLOPT_TIMEOUT, 60); // the number of seconds to wait for the transfer to finish from API server (for low bandwidth use higher value)

			// execute the request
			$result = curl_exec($ch); // execute the curl request
			$info = curl_getinfo($ch); // get the curl connection info
			$error = curl_error($ch); // get the curl error text
			$errorno = curl_errno($ch); // get the curl error number

			curl_close($ch);

			$unserialize = @unserialize($result);
			$result = $unserialize === FALSE ? '' : $unserialize; // make sure the result is serialized

			return array($result, $info, $error, $errorno);
		} else {
			die('You must have cURL installed on your server in order to make requests! Another option is to use php fsockopen, which is not implemented in these examples.');
		}
	}
}
?>
