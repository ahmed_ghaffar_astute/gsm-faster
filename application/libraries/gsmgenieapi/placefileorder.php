<?php
	require_once('gsmfusion_api.php');
	$objGSMFUSIONAPI = new GSMFUSIONAPI();

	if($row->ExternalNetworkId != '')
		$para['ID'] = $row->ExternalNetworkId;
	if($row->CodeFile != '')
	{
		$para['FILENAME'] = $row->CodeFile;
		$fileContents = file_get_contents('../../'.$row->CodeFile);
		$para['FILEDATA'] = base64_encode($fileContents);
	}

	if($row->CodeFile != '' && $row->ExternalNetworkId != '')
	{
		$objGSMFUSIONAPI->doAction('placefileorder', $apiKey, $serverURL, $accountId, $para);
		$objGSMFUSIONAPI->XmlToArray($objGSMFUSIONAPI->getResult());
		$arrayData = $objGSMFUSIONAPI->createArray();
		if(isset($arrayData['error']) && sizeof($arrayData['error']) > 0)
		{
			$ifCodeSentToServer = 0;
			$msgFromServer = $arrayData['error'][0];
		}
		else
		{
			$RESPONSE_ARR = array();
			if(isset($arrayData['result']['fileorder']) && sizeof($arrayData['result']['fileorder']) > 0)
				$RESPONSE_ARR = $arrayData['result']['fileorder'];
			$total = count($RESPONSE_ARR);
			if($total > 0)
			{
				$orderIdFromServer = $RESPONSE_ARR[0]['id'];
				$ifCodeSentToServer = 1;
			}
		}
	}
?>