<?php

/**
 * DC-Unlocker API
 * @author DC-unlocker.com
 * @copyright DC-unlocker.com
 */

require 'actions/abstract_action.class.php';
require 'actions/information.class.php';
require 'actions/activatedongle.class.php';
require 'actions/transfercredits.class.php';
require 'actions/createNewUser.class.php';
require 'actions/userblock.class.php';
require 'actions/donglesupport.class.php';
require 'actions/activatefeature.class.php';
require 'actions/activatelicense.class.php';
require 'message.class.php';

class DCUnlockerAPI {
    /**
     * @access private
     * @var Abstract_action Hold information about available actions
     */
    private $action = null;
    
    /**
     * Client API version number
     * @var integer 
     */ 
    private $version = 202;
    
    /**
     * DC-unlocker user
     * @var string 
     */
    private $username = '';
    
    /**
     * DC-unlocker user password
     * @var string 
     */
    private $password = '';
    
    /**
     * @var Message stores data for sending to API server, and gets response 
     */
    private $message = null;
    
    public function __construct($params) {
        $this->username = $params['accountId'];
        $this->password = $params['apiKey'];
        $this->message = new Message();
    }
       
    public function getUsername() {
        
        return $this->username;
    }
    
    /**
     *Creates ActionInformation object and returns it
     * @see ActionInformation
     * @return ActionInformation 
     */
    public function information (){
        
        $this->action = new ActionInformation($this);
        return $this->action;
    }
        
    /**
     *Creates ActivateDongle action with action to activate Vygis dongle
     * @return ActivateDongle 
     * @example ActiveDongleExample.php how to activate dongle 
     */
    public function activateVygisDongle (){
        
        $this->action = new ActivateDongle($this);
        $this->action->dongleType(2);
        return $this->action;
    }
    
    /**
     *Creates ActivateDongle action with action to activate Rocker dongle
     * @return ActivateDongle 
     * @example ActiveDongleExample.php how to activate dongle 
     */
    public function activateRockerDongle (){
        
        $this->action = new ActivateDongle($this);
        $this->action->dongleType(3);
        return $this->action;
    }
    
    /**
     *Creates ActivateDongle action with action to activate Infinity dongle
     * @return ActivateDongle 
     * @example ActiveDongleExample.php how to activate dongle 
     */
    public function activateInfinityDongle (){
        
        $this->action = new ActivateDongle($this);
        $this->action->dongleType(4);
        return $this->action;
    }
    
    /**
     *Creates ActivateDongle action with action to activate dongle with type auto detection
     * @return ActivateDongle 
     * @example ActiveDongleExample.php how to activate dongle 
     */
    public function activateDongle (){
        
        $this->action = new ActivateDongle($this);
        $this->action->dongleType(0);
        return $this->action;
    }
    
    /**
     *Invokes action to add credits to dongle
     * @param string $serial Valid and existing dongle's S/n
     * @param int $credits amount of credits add to dongle
     * @return action object
     * @example TransferCreditToDonglesExample.php how to add credits to dongle
     */
    public function addCreditsToDongle($serial = '00000000', $credits = 0){
        
        $this->action = new transferCredits($this);
        $this->action->toDongle($serial);
        $this->action->add($credits);
        return $this;
    }
    
    /**
     *Invokes action to add credits to User
     * @param string $name Valid and existing user name
     * @param int $credits credits amount to add to specified user
     * @example TransferaddCreditToUsersExample.php how to add credits to user
     */
    public function addCreditsToUser($name = '00000000', $credits = 0){
        
        $this->action = new transferCredits($this);
        $this->action->toUser($name);
        $this->action->add($credits);
        return $this;
    }
    
    /**
     *Invokes action to add deduct to User<br/>
     * $name will loose reseller permission if remaining credits drops below 200 after operation
     * @param string $name Valid and existing user name, which belongs to you
     * @param int $credits credits amount to add to specified user
     * @example TransfesubtractrCreditsFromUserExample.php how to remove credits from user
     */
    public function subtractCreditsFromUser($name = '00000000', $credits = 0){
        
        $this->action = new transferCredits($this);
        $this->action->toUser($name);
        $this->action->subtract($credits);
        return $this;
    }
    
    /**
     * Turns on testing mode 
     */
    public function TestModeOn(){
        $this->action->TestModeOn();
        return $this;
    }
    
    /**
     *Create new user
     * @param string $userName valid (a-zA-Z0-9._-) and not existing user name. 5-16 length
     * @param int $credits credits to add to new user
     * @param bool $reseller if true, new user will be reseller
     * @example createUserExample.php how to create user
     */
    public function createNewUser($userName, $credits, $reseller=false, $license=0){
        
        $this->action = new createNewUser($this);
        $this->action->setName($userName);
        $this->action->setCredits($credits);
        $this->action->setReseller($reseller);
        $this->action->setLicense($license);
        return $this;
    }
    
    /**
     *Invokes action to block selected user. Error code 208 raises when trying to block blocked user
     * @param string $name valid and existing username which ownership belongs to api user
     * @example BlockUserNameExample.php how to block user 
     */
    public function blockUser($name){
        
        $this->action = new userBlock($this);
        $this->action->setName($name);
        return $this;
    }
    
    /**
     * Creates DongleSupport action
     * @return object /DongleSupport
     * @example renewDongleSupport.php
     */
    public function renewDongleSupport() {
        
        $this->action = new DongleSupport($this);
        return $this->action;
    }
    
    /**
     * Creates ActivateFeature action
     * @return ActivateFeature 
     * @example ActivateFeatureExample.php how to activate dongle features
     */
    public function activateFeature (){
        
        $this->action = new ActivateFeature($this);
        return $this->action;
    }
    
    /**
     * Creates ActivateLicense action
     * @return ActivateLicense 
     * @example ActivateLicenseExample.php how to activate timed license
     */
    public function activateLicense (){
        
        $this->action = new ActivateLicense($this);
        return $this->action;
    }
    
    /**
     *Send created data to the API server
     * @return Message object 
     */
    public function submit (){
        $this->message->buildrequest($this->action, $this->username, $this->password, $this->version);
        return $this->message->send();
    } 
}
