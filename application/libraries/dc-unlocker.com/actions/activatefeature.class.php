<?php

/**
 * DC-Unlocker API
 * @author DC-unlocker.com
 * @copyright DC-unlocker.com
 */

/**
 *Activate Feature action. Needs dongle name (hex format) and feature id.
 * @example ActiveFeatureExample.php
 */
class ActivateFeature extends action{
    /**
     * Dongle name. Only in hex format (8 chars long)
     * @var string 
     * @see dongleName()
     */
    private $dongle_name = '';
    
    /**
     * Dongle feature
     * @var int
     */
    private $feature = 0;
 
    protected $action_name = 'activatefeature';
    
    public function __construct($API) {
        parent::__construct($API);
    }
    
    /**
     * Actions variables which be used in Message object
     * @return array 
     */
    public function getMsgVars(){
        return array(
            'action_name'=>urlencode($this->action_name),
            'dongle_name'=>urlencode($this->dongle_name),
            'dongle_feature'=>urlencode($this->feature),
            'test_mode'=>urlencode($this->testMode)
        );
    }
    
    /**
     *Sets dongle name. Valid dongle name are:<br/>
     * 8 chars length in hex [A-F0-9]<br/>
     * @param string $dongle_name
     * @return ActivateFeature
     */
    public function dongleName($dongle_name){
        $this->dongle_name = $dongle_name;
        return $this;
    }
       
    /**
     * Sets feature id
     * @return ActivateFeature 
     */
    public function featureId($feature_id){
        $this->feature = $feature_id;
        return $this;
    }
}
