<?php

/**
 * DC-Unlocker API
 * @author DC-unlocker.com
 * @copyright DC-unlocker.com
 */

class DongleSupport extends action {
    /**
     * Dongle name. Only in hex format (8 or 16 chars long)
     * 
     * @var string 
     * @see dongleName()
     */
    private $dongle_name = '';
    
    /**
     * Dongle support length
     * 
     * @var int <br/>
     * 1 = 1 year
     * 2 = 2 years
     */
    private $support_time = 1;
    
    protected $action_name = 'renewdonglesupport';
    
    public function __construct($API) {
        parent::__construct($API);
    }
    
    /**
     * Actions variables which be used in Message object
     * 
     * @return array 
     */
    public function getMsgVars() {
        
        return array(
            'action_name'=>urlencode($this->action_name),
            'dongle_name'=>urlencode($this->dongle_name),
            'support_type'=>$this->support_time,
            'test_mode'=>urlencode($this->testMode)
        );
    }
    
    /**
     * Sets dongle name. Valid dongle name are:<br/>
     * Vygis dongle: 8 chars length in hex [A-F0-9]<br/>
     * Infinity dongle: 10 chars length in hex [A-F0-9]<br/>
     * Rocker dongle: 16 chars length in hex [A-F0-9]<br/>
     * 
     * @param string $dongle_name
     * @return /ActivateDongle
     */
    public function dongleName($dongle_name) {
        
        $this->dongle_name = $dongle_name;
        
        return $this;
    }
    
    /**
     * Sets dongle support length
     * 
     * @param int $type 1 = 1 year, 2 = 2 years
     * @return \DongleSupport
     */
    public function supportType($type) {
        
        if ($type == 1 || $type == 2) {
            
            $this->support_time = $type;
        }
        
        return $this;
    }
}

