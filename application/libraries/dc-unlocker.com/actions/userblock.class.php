<?php

/**
 * DC-Unlocker API
 * @author DC-unlocker.com
 * @copyright DC-unlocker.com
 */

class userBlock extends action {
    /**
     *User specific action name
     * @var string 
     */
    protected $action_name = 'blockuser';
    
    protected $userName = null;
    
    public function __construct($API) {
        parent::__construct($API);
    }
    
    /**
     *Set user account name
     * @param string $name
     * @return Users 
     */
    public function setName ($name){
        $this->userName = $name;
        return $this;
    }
    
    /**
     *
     * @return array
     */
    public function getMsgVars(){
        return array(
            'action_name'=>urlencode($this->action_name),
            'user_name'=>urlencode($this->userName),
            'test_mode'=> urlencode($this->testMode)
        );
    }    
}
?>
