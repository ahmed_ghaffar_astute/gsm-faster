<?php

/**
 * DC-Unlocker API
 * @author DC-unlocker.com
 * @copyright DC-unlocker.com
 */

/**
 * Actions with credits transfers
 * @example TransferCreditsExample.php
 */
class transferCredits extends action {
    
    private $name = null;
    private $creditsAction = null;
    private $creditsAmount = 0;
    private $type = 0;
    
    protected $action_name = 'transfercredits';
        
    public function __construct($API) {
        parent::__construct($API);
    }
    
    public function getMsgVars(){
        return array(
            'action_name'=>urlencode($this->action_name),
            'user_name'=>urlencode($this->name),
            'type'=>urlencode($this->type),
            'credits_action'=>urlencode($this->creditsAction),
            'credits_amount'=>urlencode($this->creditsAmount),
            'test_mode'=>urlencode($this->testMode)
        );
    }
    
    /**
     *Set dongle name to whom credits will be added 
     * @param string $name valid and existing dongle name
     * @return transferCredits object
     */
    public function toDongle($name){
        $this->name = $name;
        $this->type = 2;
        return $this;
    }
    
    public function toUser($name){
        $this->name = $name;
        $this->type = 1;
        return $this;
    }
    
    /**
     *Command to add integer number $credits from selected user
     * @param int $credits positive number. Will be transfered from api user
     * account to selected user / dongle
     * @return transferCredits object
     */
    public function add ($credits){
        $this->creditsAction = 'add';
        $this->creditsAmount = (int) $credits;
        return $this;
    }
    
     /**
     *Command to subtract integer number $credits from selected user
     * @param int $credits positive number. Will be transfered from selected user
     * @return transferCredits object
     */
    public function subtract ($credits){
        $this->creditsAction = 'subtract';
        $this->creditsAmount = (int) $credits;
        return $this;
    }
}

?>
