<?php

/**
 * DC-Unlocker API
 * @author DC-unlocker.com
 * @copyright DC-unlocker.com
 */

/**
 *Activate License action. Needs user name and license id.
 * @example ActivateLicenseExample.php
 */
class ActivateLicense extends action{
    
    private $user_name = '';
    
    private $license = 0;
 
    protected $action_name = 'activatelicense';
    
    public function __construct($API) {
        parent::__construct($API);
    }
    
    /**
     * Actions variables which be used in Message object
     * @return array 
     */
    public function getMsgVars(){
        return array(
            'action_name'=>urlencode($this->action_name),
            'user_name'=>urlencode($this->user_name),
            'user_license'=>urlencode($this->license),
            'test_mode'=>urlencode($this->testMode)
        );
    }
    
    /**
     *Sets user name
     * @param string $user_name
     * @return ActivateLicense
     */
    public function userName($user_name){
        $this->user_name = $user_name;
        return $this;
    }
       
    /**
     * Sets license id 
     * @return ActivateLicense 
     */
    public function licenseId($license_id){
        $this->license = $license_id;
        return $this;
    }
}
