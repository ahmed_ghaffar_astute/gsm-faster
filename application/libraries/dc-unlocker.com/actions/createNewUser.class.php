<?php

/**
 * DC-Unlocker API
 * @author DC-unlocker.com
 * @copyright DC-unlocker.com
 */

class createNewUser extends action {
    
    protected $action_name = 'CreateNewUser';

    private $credits = null;
    
    protected $userName = null;
    
    private $reseller = 0;
    
    private $license = 0;
        
    public function __construct($API) {
        parent::__construct($API);
    }
    
    /**
     *Set user account name
     * @param string $name
     * @return Users 
     */
    public function setName ($name){
        $this->userName = $name;
        return $this;
    }
     
    public function getMsgVars() {
        return array(
            'action_name'=>urlencode($this->action_name),
            'user_name'=>urlencode($this->userName),
            'credits'=>urlencode($this->credits),
            'reseller'=>urlencode($this->reseller),
            'user_license'=>urlencode($this->license),
            'test_mode'=>urlencode($this->testMode)
        );
    }
   
    public function setCredits($credits){
        $this->credits = (int) $credits;
    }
    
    public function setReseller($reseller){
        $this->reseller = (int)$reseller;
    }
    
    public function setLicense($license){
        $this->license = (int)$license;
    }
}

?>
