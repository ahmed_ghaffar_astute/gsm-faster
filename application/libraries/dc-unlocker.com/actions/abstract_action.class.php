<?php

/**
 * DC-Unlocker API
 * @author DC-unlocker.com
 * @copyright DC-unlocker.com
 */

/**
 * Provides common function for actions classes
 */
abstract class action {
    /**
     * action name of the action object
     * @var string 
     */
    protected $action_name = '';
    /**
     * DCUnlockerAPI class handler
     * @var DCUnlockerAPI 
     */
    protected $APIhandler = null;
            
    /**
     *Test mode by default disabled
     * @var bool
     */
    protected $testMode = 0;
    
    /**
     * Checks if called $name function exists. If true proceeds with it, on false
     * returns action object.
     * @param string $name function name
     * @param string $arguments function arguments
     * @return action 
     */
    public function __call($name, $arguments)
    {
        if (function_exists($name))
            $this->$name($arguments);
        else
            return $this;
    }
     
    public function __construct($API) {
        $this->APIhandler = $API;
    }
    
    /**
     * return Array
     */
    abstract public function getMsgVars();
    
    /**
     *Submits action to be processed to API server
     * @return Message 
     */
    public function submit(){
        return $this->APIhandler->submit();
        
    }

    /**
     *Sets Test Mode On. Message will be sent to server, without data changes <br/>
     * For testing purpose
     */
    public function TestModeOn(){
        $this->testMode = 1;
        return $this;
    }
}
?>
