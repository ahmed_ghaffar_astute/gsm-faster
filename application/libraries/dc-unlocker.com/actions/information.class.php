<?php

/**
 * DC-Unlocker API
 * @author DC-unlocker.com
 * @copyright DC-unlocker.com
 */

/**
 * Information about user / dongle specific data.
 * @example checkCreditsExample.php example how to check remaining credits
 * @example checkAllDongleInfoExample.php example how to get all related dongle information 
 * @example checkUserNameExample.php example how to check existence of userName
 * @example checkDongleSnExample.php example how to check existence of Dongle Sn
 */
class ActionInformation extends action{
    
    protected $action_name = 'information';
    private $type = 1;
    
    /**
     * @var string User name or Dongle HID <br/>
     * default: API user username
     */
    private $checkUser = '';
    
    /**
     * @var string Specify what information to retrieve <br/>
     * 'credits' - returns user's remaining credit amount
     */
    private $operation = null;
        
    public function __construct($API) {
        parent::__construct($API);
    }
    
    public function getMsgVars(){
        
        if ($this->checkUser === '') {
            $this->checkUser = $this->APIhandler->getUsername();
        }
        
        return array(
            'action_name'=>urlencode($this->action_name),
            'type'=>urlencode($this->type),
            'checked_name'=>urlencode($this->checkUser),
            'operation'=>urlencode($this->operation),
            'test_mode'=>urlencode($this->testMode)
        );
    }
    
    /**
     *Set dongle hid on which obtain information <br/>
     * @param string $serial valid and existing Dongle S/n
     * @return ActionInformation object
     */
    public function checkDongle($serial){
        $this->checkUser = $serial;
        $this->type = 2;
        return $this;
    }
    
    public function checkUser($userName){
        $this->checkUser = $userName;
        $this->type = 1;
        return $this;
    }
    
    /**
     *Set user name and get information about existence and validity of it
     * @param string $userName 
     * @return ActionInformation object
     */
    public function checkUserName($userName){
        $this->checkUser = $userName;
        $this->operation = 'check_user_name';
        $this->type = 1;
        return $this;
    }
    
    /**
     * Check if dongle sn is on system. 
     * @param string $dongleSn
     */
    public function checkDongleSN($dongleSn){
        $this->checkUser = $dongleSn;
        $this->operation = 'check_dongle_name_exists';
        $this->type = 2;
        return $this;
    }
    
    /**
     *Set command to get remaining credit amount
     * @return ActionInformation object
     */
    public function creditsLeft(){
        $this->operation = 'view_credits';
        return $this;
    }
    
    /**
     * Set command to get all info about dongle:<br/>
     * * serial number, <br/>
     * * type ('Dc-unlocker', 'Vygis', 'Infinity','Rocker'), <br/>
     * * activated status. bool true or false, <br/>
     * * activated time (format 2012-05-31 10:16:23). only if activated status is true, <br/>
     * * blocked status. bool true or false, <br/>
     * * dongle credits left. integer number of dongle's remaining credits<br/>
     * * ...
     * @return ActionInformation object
     */
    public function getAllInfo(){ //getAllDongleInfo
        $this->operation = 'view_all_info';
        return $this;
    }
        
    /**
     * Sets command to get information about dongle support status
     * 
     * @return object \ActionInformation
     */
    public function getDongleSupportStatus(){
        $this->operation = 'view_support';
        return $this;
    }
    
    /**
     * Sets command to get information about user's timed license status
     * 
     * @return object \ActionInformation
     */
    public function getTimedLicenseStatus(){
        $this->operation = 'view_license';
        return $this;
    }
    
    /**
     * Sets command to get information about active dongle features
     * 
     * @return object \ActionInformation
     */
    public function getDongleActiveFeatures(){
        $this->operation = 'view_features';
        return $this;
    }
    
    /**
     * Sets command to get available dongle features
     * 
     * @return object \ActionInformation
     */
    public function getFeaturesList(){
        $this->operation = 'get_features_list';
        return $this;
    }
    
    /**
     * Sets command to get available timed licenses
     * 
     * @return object \ActionInformation
     */
    public function getTimedLicenseList(){
        $this->operation = 'get_license_list';
        return $this;
    }
    
    /**
     * Sets command to get supports
     * 
     * @return object \ActionInformation
     */
    public function getDongleSupportList(){
        $this->operation = 'get_support_list';
        return $this;
    }
    
    /**
     * Sets command to get dongle activations
     * 
     * @return object \ActionInformation
     */
    public function getDongleActivationList(){
        $this->operation = 'get_activation_list';
        return $this;
    }
}
?>
