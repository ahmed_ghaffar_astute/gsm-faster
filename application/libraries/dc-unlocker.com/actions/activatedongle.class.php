<?php

/**
 * DC-Unlocker API
 * @author DC-unlocker.com
 * @copyright DC-unlocker.com
 */

/**
 *Activate Dongle action. Needs dongle name (hex format), type for dongle
 * activation.
 * @example ActiveDongleExample.php
 */
class ActivateDongle extends action{
    /**
     *Dongle name. Only in hex format (8 or 16 chars long)
     * @var string 
     * @see dongleName()
     */
    private $dongle_name = '';
    
    /**
     *Dongle type
     * @var int 
     * @see dongleType()
     */
    private $dongleType = null;
    
    /**
     *Dongle activation type:
     * @var int <br/>
     * 1 Full
     * 2 Lite
     */
    private $activationType = 'full';
    
    protected $action_name = 'activatedongle';
    
    public function __construct($API) {
        parent::__construct($API);
    }
    
    /**
     * Actions variables which be used in Message object
     * @return array 
     */
    public function getMsgVars(){
        return array(
            'action_name'=>urlencode($this->action_name),
            'dongle_name'=>urlencode($this->dongle_name),
            'dongle_type'=>urlencode($this->dongleType),
            'act_type'=>urlencode($this->activationType),
            'test_mode'=>urlencode($this->testMode)
        );
    }
    
    /**
     *Sets dongle name. Valid dongle name are:<br/>
     * Vygis dongle: 8 chars length in hex [A-F0-9]<br/>
     * Infinity dongle: 10 chars length in hex [A-F0-9]<br/>
     * Rocker dongle: 16 chars length in hex [A-F0-9]<br/>
     * @param string $dongle_name
     * @return ActivateDongle
     */
    public function dongleName($dongle_name){
        $this->dongle_name = $dongle_name;
        return $this;
    }
    
    /**
     *Sets dongle type
     * @param int $dongleType activation type:<br/>
     * 2- Vygis dongle<br/>
     * 3- Rocker dongle<br/>
     * 4- Infinity dongle<br/>
     * @return ActivateDongle
     */
    public function dongleType ($dongleType){
        $this->dongleType = $dongleType;
        return $this;
    }
    
    /**
     *Sets activation type as Full
     * @return ActivateDongle 
     */
    public function activationFull(){
        $this->activationType = 1;
        return $this;
    }
    
    /**
     *Sets activation type as Lite
     * @return ActivateDongle 
     */
    public function activationLite(){
        $this->activationType = 2;
        return $this;
    }
}
?>
