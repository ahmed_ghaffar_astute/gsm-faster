<?php

/**
 *Creates, encrypts, sends and receives messages to and from DC-unlocker API server 
 */
class Message {
    /**
     *Stores \action object variable's values
     * @var array
     */
    private $message_vars = null;
    
    /**
     *Stores response message from DC-unlocker API server
     * @var JSON format 
     */
    private $response = null;
    
    /**
     *Message holds commands for DC-Unlocker API server
     * @var string 
     */
    private $message = null;
    
    /**
     * $message_vars are transfered to $message variable in JSON 
     * for later processing 
     */
    private function createmsg(){
        $this->message = json_encode($this->message_vars);
    }

    /**
     * Encrypts $message with given DC-Unlocker.com given public key
     */
    private  function encryptmsg(){        
        $plaintext = $this->message;
        
        $pkey = defined('DCU_API_PUBLIC_KEY') ? DCU_API_PUBLIC_KEY : dirname(__FILE__).'/public.key';
                
        if (!file_exists($pkey)) {
            die ('PUBLIC KEY FILE NOT FOUND');
        }
        
        $publicKey = openssl_pkey_get_public('file://'.$pkey);
        $encrypted = '';
        if (!openssl_public_encrypt($plaintext, $encrypted, $publicKey)){
            die('Failed to encrypt data');
        }
        openssl_free_key($publicKey);
        $encrypted = urlencode($encrypted);
        $encrypted = base64_encode($encrypted);
        $this->message = 'message='.$encrypted;        
    }

    /*
     * Sends $message to DC-Unlocker.com API server using curl method
     */
    private  function sendmsg(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.dc-unlocker.com/dcu_api/202.php');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->message); 
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /**
     * Returns DC-Unlocker API server message as array.<br/>
     * Array structure: <br/>
     * array (<br/>
     *      [server] => array (<br/>
     *          [time] => ''<br/>
     *      )<br/>
     *      [response] => array (<br/>
     *        [status] => ok<br/>
     *        [id] => ****************<br/>
     *        [action_name] => activatedongle<br/>
     *        [dongle_name] => 00000001<br/>
     *        [dongle_type] => 2<br/>
     *        [test_mode] => 0<br/>
     *        [username] => *****<br/>
     *        [credits_left] => 5<br/>
     *        [message] => ACTIVATED DONGLE<br/>
     *        [credits_used] => 99<br/>
     *      )<br/>
     * )    [error] => 0<br/>
     */
    public function get_response (){
        $out = json_decode($this->response, true);
        if ($out == null)
            return 'false:'.$this->response;
        else{
            return $out;
        }
    }
    
    /**
     * Fills request message 
     * @param Action $action 
     * @internal
     * @access private
     */
    public function buildrequest ($action, $username, $password, $version){
        $this->message_vars = $action->getMsgVars();
        $this->message_vars['username'] = $username; 
        $this->message_vars['pass'] = $password; 
        $this->message_vars['ip'] = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
        $this->message_vars['api'] = $version;
    }
    
    /**
     * @return Message 
     */
    public function send (){
        if (!empty($this->message_vars)){
            $this->createmsg();
            $this->encryptmsg();
            $this->response = $this->sendmsg();
            return $this;
        }
        return $this;
    }
}

?>
