<?php
/*
$type = ["place_order", "track_status", "get_services"]; - REQUIRED
$key = API KEY - REQUIRED
$imei = NUMERIC - OPTINAL
$service_id = NUMERIC. See service `ID` at 'get_services' method - OPTINAL
$order_id = NUMERIC - OPTINAL
*/
function api_request($type, $key, $imei = '', $service_id = '', $order_id = '')
{
	$add_url = '';
	if ($imei != '') $add_url .= "&imei={$imei}";
	if ($service_id != '') $add_url .= "&service_id={$service_id}";
	if ($order_id != '') $add_url .= "&order_id={$order_id}";
	$curl_url = "http://digitalunlocking.biz/apu_api.php?type={$type}&key={$key}{$add_url}";
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL,$curl_url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	$result = curl_exec($ch);
	return $result;
}
/*
// Call Place Order
echo api_request('place_order', 'PLACE_API_KEY_HERE', 'PLACE_IMEI_HERE', 'PLACE_MODE_HERE');

// Call Track Order Status
echo api_request('track_status', 'PLACE_API_KEY_HERE', '', '', 'PLACE_ORDER_ID_HERE');

// Get Services
echo api_request('get_services', 'PLACE_API_KEY_HERE');

// Get Account Info
echo api_request('account_info', 'PLACE_API_KEY_HERE');
*/
?>