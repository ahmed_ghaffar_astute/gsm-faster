<?php

/**
 * PHP Class for handling Google Authenticator 2-factor authentication
 *
 * @author Michael Kliewe
 * @copyright 2012 Michael Kliewe
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 * 
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once 'googleLib/GoogleAuthenticator.php';


class CI_GoogleAuthenticator extends GoogleAuthenticator
{
    
}
