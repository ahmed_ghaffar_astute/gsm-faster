<?php
	/*
 		GSMUNLOCKUSA API
		This is a PHP 5.3 library for connecting the GSMUNLOCKUSA API

		It supposes that you have NuSOAP library.
		More information : http://nusoap.sourceforge.net/

		//Change APIKey for your real data!
	*/
	define('GSMUNLOCKUSA_API_DEBUG', false);

	/* NuSOAP library include. */
	require_once('nusoap/lib/nusoap.php');

	class gsmunlockusa_api
	{

		function SetWSDL($url)
		{
			$cliente = new nusoap_client($url, true);
			return $cliente;
		}

		/*
		------------------------
			GetAccountInfo()
		------------------------
		*/
		function GetAccountInfo($API_KEY, $URL)
		{
			$cliente = SetWSDL($URL);
			$env = array('vAPIKey' => $API_KEY);
			$result = $cliente->call('GetAccountInfo', $env);

			if (GSMUNLOCKUSA_API_DEBUG) {
				echo clientDebug($cliente);
			}
			if (clientError($cliente, $result)) {
				return $result;
			}
		}

		/*
		------------------------
			GetMEP()
		------------------------
		*/
		function GetMEP($API_KEY, $URL)
		{
			$cliente = SetWSDL($URL);
			$env = array('vAPIKey' => $API_KEY);
			$result = $cliente->call('GetMEP', $env);

			if (GSMUNLOCKUSA_API_DEBUG) {
				echo clientDebug($cliente);
			}
			if (clientError($cliente, $result)) {
				return $result;
			}
		}

		/*
		------------------------
			GetOrder()
		------------------------
		*/
		function GetOrder($API_KEY, $URL, $vAPIOrderID)
		{
			$cliente = SetWSDL($URL);
			$env = array('vAPIKey' => $API_KEY, 'vAPIOrderID' => $vAPIOrderID);
			$result = $cliente->call('GetOrder', $env);

			if (GSMUNLOCKUSA_API_DEBUG) {
				echo clientDebug($cliente);
			}
			if (clientError($cliente, $result)) {
				return $result;
			}
		}

		/*
		------------------------
			GetService()
		------------------------
		*/
		function GetService($API_KEY, $URL)
		{
			$cliente = SetWSDL($URL);
			$env = array('vAPIKey' => $API_KEY);
			$result = $cliente->call('GetService', $env);

			if (GSMUNLOCKUSA_API_DEBUG) {
				echo clientDebug($cliente);
			}
			if (clientError($cliente, $result)) {
				return $result;
			}
		}

		/*
		------------------------
			GetServiceBrand()
		------------------------
		*/
		function GetServiceBrand($API_KEY, $URL, $vServiceID)
		{
			$cliente = SetWSDL($URL);
			$env = array('vAPIKey' => $API_KEY, 'vServiceID' => $vServiceID);
			$result = $cliente->call('GetServiceBrand', $env);

			if (GSMUNLOCKUSA_API_DEBUG) {
				echo clientDebug($cliente);
			}
			if (clientError($cliente, $result)) {
				return $result;
			}
		}

		/*
	   ------------------------
		   GetServiceCountry()
	   ------------------------
	   */
		function GetServiceCountry($API_KEY, $URL, $vServiceID)
		{
			$cliente = SetWSDL($URL);
			$env = array('vAPIKey' => $API_KEY, 'vServiceID' => $vServiceID);
			$result = $cliente->call('GetServiceCountry', $env);

			if (GSMUNLOCKUSA_API_DEBUG) {
				echo clientDebug($cliente);
			}
			if (clientError($cliente, $result)) {
				return $result;
			}
		}

		/*
	   ------------------------
		   GetServiceModel()
	   ------------------------
	   */
		function GetServiceModel($API_KEY, $URL, $vServiceID, $vBrandID)
		{
			$cliente = SetWSDL($URL);
			$env = array('vAPIKey' => $API_KEY, 'vServiceID' => $vServiceID, 'vBrandID' => $vBrandID);
			$result = $cliente->call('GetServiceModel', $env);

			if (GSMUNLOCKUSA_API_DEBUG) {
				echo clientDebug($cliente);
			}
			if (clientError($cliente, $result)) {
				return $result;
			}
		}

		/*
		------------------------
			GetServiceNetwork()
		------------------------
		*/
		function GetServiceNetwork($API_KEY, $URL, $vServiceID, $vCountryID)
		{
			$cliente = SetWSDL($URL);
			$env = array('vAPIKey' => $API_KEY, 'vServiceID' => $vServiceID, 'vCountryID' => $vCountryID);
			$result = $cliente->call('GetServiceNetwork', $env);

			if (GSMUNLOCKUSA_API_DEBUG) {
				echo clientDebug($cliente);
			}
			if (clientError($cliente, $result)) {
				return $result;
			}
		}

		/*
		------------------------
			PlaceOrder()
		------------------------
		*/
		function PlaceOrder($API_KEY, $URL, $vServiceID, $vIMEI, $vNetworkID, $vModelID, $vMEPID, $vPINNumber, $vProviderID, $vPRD, $vSN, $vRMType, $vKBHKRH, $vReference, $vServiceTag, $vRespondEmail, $vNotes)
		{
			$cliente = SetWSDL($URL);
			$env = array('vAPIKey' => $API_KEY, 'vServiceID' => $vServiceID, 'vIMEI' => $vIMEI, 'vNetworkID' => $vNetworkID, 'vModelID' => $vModelID, 'vMEPID' => $vMEPID, 'vPINNumber' => $vPINNumber, 'vProviderID' => $vProviderID, 'vPRD' => $vPRD, 'vSN' => $vSN, 'vRMType' => $vRMType, 'vKBHKRH' => $vKBHKRH, 'vReference' => $vReference, 'vServiceTag' => $vServiceTag, 'vRespondEmail' => $vRespondEmail, 'vNotes' => $vNotes);
			$result = $cliente->call('PlaceOrder', $env);

			if (UNLOCKUSA_API_DEBUG) {
				echo clientDebug($cliente);
			}
			if (clientError($cliente, $result)) {
				return $result;
			}
		}

		/*
		------------------------
			VerifyOrder()
		------------------------
		*/
		function VerifyOrder($API_KEY, $URL, $vAPIOrderID)
		{
			$cliente = SetWSDL($URL);
			$env = array('vAPIKey' => $API_KEY, 'vAPIOrderID' => $vAPIOrderID);
			$result = $cliente->call('VerifyOrder', $env);

			if (GSMUNLOCKUSA_API_DEBUG) {
				echo clientDebug($cliente);
			}
			if (clientError($cliente, $result)) {
				return $result;
			}
		}

		/*
		========================
		Display Debug Messages
		========================
		*/

		function clientDebug($cliente)
		{
			$debb = '<h2>Request</h2>';

			$text = htmlspecialchars($cliente->request, ENT_QUOTES);
			$remove = array("<", " x");
			$insert = array("<br><", "<br>    x");
			$text = str_replace($remove, $insert, $text);

			$debb .= '<pre>' . $text . '</pre>';
			$debb .= '<h2>Response</h2>';

			$text = htmlspecialchars($cliente->response, ENT_QUOTES);
			$remove = array("<", " x");
			$insert = array("<br><", "<br>  x");
			$text = str_replace($remove, $insert, $text);

			$debb .= '<pre>' . $text . '</pre>';

			// Display the debug messages
			$debb .= '<h2>Debug</h2>';
			$debb .= '<pre>' . htmlspecialchars($cliente->debug_str, ENT_QUOTES) . '</pre>';
			return $debb;
		}

		/*
		========================
		Display Error Messages
		========================
		*/
		function clientError($cliente, $result)
		{
			$bien = false;
			if ($cliente->fault) {
				$errors = '<p><b>Fault: ';
				$errors .= print_r($result, true);
				$errors .= '</b></p>';
				echo $errors;
			} else {

				$err = $cliente->getError();
				if ($err) {
					$errors = '<p><b>Error: ' . $err . '</b></p>';
					echo $errors;
				} else {
					$bien = true;
				}
			}
			return $bien;
		}

	}
