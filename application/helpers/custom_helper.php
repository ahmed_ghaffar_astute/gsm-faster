<?php

function last_query()
{
	$CI = &get_instance();
	echo $CI->db->last_query();
	//exit();
}

function checkUserLoginState()
{
	$CI = &get_instance();
	if (!$CI->session->userdata('GSM_FUS_UserId')) {
		$CI->session->sess_destroy();
		redirect(base_url('login'));
	}
}

function checkAdminLoginState()
{
	$CI = &get_instance();
	if (!$CI->session->userdata('GSM_FSN_AdmId')) {
		$CI->session->sess_destroy();
		redirect(base_url('admin/login'));
	}
}

function d($array)
{
	echo '<pre>';
	print_r($array);
	exit();
}

function ipBlocked()
{
	$CI = &get_instance();
	$rwIPInfo = $CI->General_model->ipBlocked();
	if ($rwIPInfo->Data > 0)
		return true;
	else
		return false;
}



function createIPHtaccess($backend = '')
{
	$CI = &get_instance();
	if ($backend == '') {
		if (!file_exists(".htaccess")) fopen(".htaccess", "w+");
	} else {
		if (!file_exists(BASEPATH . ".htaccess")) fopen(".htaccess", "w+");
	}
	$CI->db->select(' Id, IP');
	$CI->db->from('tbl_gf_allowed_ips');
	$CI->db->where('IPType', 0);
	$CI->db->order_by('Id', 'DESC');

	$result = $CI->db->get();
	$rsIPs = $result->result();


	$strIPs = $ip = '';
	$arrIPs = array();
	$z = 0;
	foreach ($rsIPs as $row) {
		$ip = stripslashes(theString_Decrypt($row->IP, 'WLIP', 10));
		$strIPs .= "allow from $ip\n";
		$arrIPs[$z] = $ip;
		$z++;
	}
	$strIPs = substr($strIPs, 0, -1);
	$strData = "<Limit GET POST>
                order deny,allow
                deny from all
                " . $strIPs . "
                </Limit>";
	if ($backend == '') {
		$fp = fopen(".htaccess", "w");
	} else {
		$fp = fopen($backend . ".htaccess", "w");
	}
	fwrite($fp, $strData);
	fclose($fp);
	//============================================STOP ACCESS VIA CODE AS WELL===================================/
	if (!in_array($CI->input->post_get('REMOTE_ADDR'), $arrIPs)) {
		$url = getDomainName() . 'admin/page/404';
		//$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		header("Location: $url");
	}
	//============================================STOP ACCESS VIA CODE AS WELL===================================/
}

function getDomainName()
{
	$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https" : "http";
	return $protocol . '://' . $_SERVER['HTTP_HOST'];
}

function failedLoginAttempt($uName)
{
	$CI = &get_instance();
	$CI->General_model->failedLoginAttempt($uName);
}

function daysBetweenTwoDates($dtTm1, $dtTm2)
{
	$start = strtotime($dtTm1);
	$end = strtotime($dtTm2);
	$days_between = ceil(abs($end - $start) / 86400);
	return $days_between;
}

function get_rnd_iv($iv_len)
{
	$iv = '';
	while ($iv_len-- > 0) {
		$iv .= chr(mt_rand() & 0xff);
	}
	return $iv;
}

function encryptThe_String($plain_text, $password = 'P@88W04D', $iv_len = 16)
{
	$plain_text .= "\x13";
	$n = strlen($plain_text);
	if ($n % 16) $plain_text .= str_repeat("\0", 16 - ($n % 16));
	$i = 0;
	$enc_text = get_rnd_iv($iv_len);
	$iv = substr($password ^ $enc_text, 0, 512);
	while ($i < $n) {
		$block = substr($plain_text, $i, 16) ^ pack('H*', md5($iv));
		$enc_text .= $block;
		$iv = substr($block . $iv, 0, 512) ^ $password;
		$i += 16;
	}
	return base64_encode($enc_text);
}


function setDtTmWRTYourCountry()
{
	$CI = &get_instance();
	$strTimeDiff = '';
	$row = $CI->General_model->setDtTmWRTYourCountry();
	if ($row->TimeDifference != '' || $row->TimeDifference != '0|0|0') {
		$arr = explode('|', $row->TimeDifference);
		if (is_array($arr)) {
			if ($arr[0] == '+' || $arr[0] == '-') {
				if ($arr[1] != '0') {
					$strTimeDiff = $arr[0] . $arr[1] . ' hour';
				}
				if ($arr[2] != '0') {
					if ($strTimeDiff == '')
						$strTimeDiff = $arr[0] . $arr[2] . ' minutes';
					else
						$strTimeDiff .= ' ' . $arr[0] . $arr[2] . ' minutes';
				}
				if ($strTimeDiff != '') {
					$serverDtTm = date('Y-m-d H:i:s');
					return date('Y-m-d H:i:s', strtotime($strTimeDiff, strtotime($serverDtTm)));
				}
			}
		}

	}
	return date('Y-m-d H:i:s');
}

function minutesDiffInDates($dtTm1, $dtTm2)
{

	$to_time = strtotime($dtTm1);
	$from_time = strtotime($dtTm2);
	return round(abs($to_time - $from_time) / 60, 2);

}

function isPasswordOK($enteredPwd, $storedHash)
{
	require_once(APPPATH . "/libraries/phpass/PasswordHash.php");
	$t_hasher = new PasswordHash(8, false);
	return $t_hasher->CheckPassword($enteredPwd, $storedHash);
}

function bFEncrypt($password)
{
	require_once(APPPATH . "/libraries/phpass/PasswordHash.php");
	$t_hasher = new PasswordHash(8, false);
	$hash = $t_hasher->HashPassword($password);
	return $hash;
}

function FillCombo($id, $data)
{
	$rs = $data;
	foreach ($rs as $row) {
		if ($row->Id == $id)
			echo "<option value='$row->Id' selected>$row->Value</option>";
		else
			echo "<option value='$row->Id' >$row->Value</option>";
	}
}

function decodeHTML($str)
{
	return htmlspecialchars_decode($str);
}

function getEmailDetails()
{
	$CI = &get_instance();
	$fromName = $fromEmail = $company = $url = '';
	$row = $CI->General_model->getEmailDetails();
	if (isset($row->FromName) && $row->FromName != '') {
		$fromName = stripslashes($row->FromName);
		$fromEmail = stripslashes($row->FromEmail);
		$toEmail = stripslashes($row->ToEmail);
		$company = stripslashes($row->Company);
		$url = $row->SiteURL;
		$contactEmail = stripslashes($row->ContactUsEmail);
	}

	return array(
		$fromName,
		$fromEmail,
		$company,
		$url,
		$toEmail,
		$contactEmail);
}

function getLocationInfoByIp($ip)
{
	$countryCode = '';
	$ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" .
		$ip));
	if ($ip_data && $ip_data->geoplugin_countryName != null) {
		$countryCode = $ip_data->geoplugin_countryCode;
	}
	return $countryCode;
}

function urlsafe_b64encode($string)
{
	$data = base64_encode($string);
	$data = str_replace(array(
		'+',
		'/',
		'='), array(
		'-',
		'_',
		'.'), $data);
	return $data;
}

function urlsafe_b64decode($string)
{
	$data = str_replace(array(
		'-',
		'_',
		'.'), array(
		'+',
		'/',
		'='), $string);
	$mod4 = strlen($data) % 4;
	if ($mod4) {
		$data .= substr('====', $mod4);
	}
	return base64_decode($data);
}

function getEmailContents($id)
{
	$CI = &get_instance();
	$subject = '';
	$contents = '';
	$sendCopy = 0;
	$row = $CI->General_model->getEmailContents($id);
	if (isset($row->Subject) && $row->Subject != '') {
		$subject = stripslashes($row->Subject);
		$contents = stripslashes($row->Contents);
		$sendCopy = $row->SendCopyToAdmin;
	}
	return array(
		$subject,
		$contents,
		$sendCopy);
}

function updateCreditsEmail($email, $name, $credits, $currCredits, $dt, $type)
{
	$arr = getEmailDetails();
	list($subject, $contents, $sendCopy) = getEmailContents(10);

	$placeholders = array(
		"#CUSTOMER_NAME#",
		"#CREDITS#",
		"#COMPANY_NAME#",
		"#COMPANY_EMAIL_ADDRESS#",
		"#CURRENT_CREDITS#",
		"#CREDITS_UPDATED_DATE#",
		"#ADDED_REBATED#");
	$replacedData = array(
		$name,
		$credits,
		stripslashes($arr[2]),
		'<a href="mailto:' . $arr[4] . '">' . $arr[4] . '</a>',
		$currCredits,
		$dt,
		$type);
	$emailMsg = str_replace($placeholders, $replacedData, $contents);
	sendMail($email, $arr[0], $arr[1], $subject, $emailMsg, '', $sendCopy == '1' ? $arr[4] :
		'');
}

function registrationEmail($email, $name, $phone, $userId, $uName, $allowUserToLogin,
						   $country, $ip)
{
	$arr = getEmailDetails();
	list($subject, $contents, $sendCopy) = getEmailContents($allowUserToLogin == '1' ?
		'1' : '23');
	$placeholders = array(
		"#CUSTOMER_NAME#",
		"#USERNAME#",
		"#PHONE#",
		"#COMPANY_NAME#",
		"#COMPANY_EMAIL_ADDRESS#",
		"#ACTIVATION_LINK#",
		"#EMAIL#",
		"#COUNTRY#",
		"#IP#");
	$replacedData = array(
		$name,
		$uName,
		$phone,
		stripslashes($arr[2]),
		'<a href="mailto:' . $arr[4] . '">' . $arr[4] . '</a>',
		'<a href="' . base_url('register/activateclient') . '?tkn1=' . urlsafe_b64encode
		($userId) . '&tkn2=' . urlsafe_b64encode($uName) . '">' . base_url('register/activateclient') .
		'?tkn1=' . urlsafe_b64encode($userId) . '&tkn2=' . urlsafe_b64encode($uName) .
		'</a>',
		$email,
		$country,
		$ip);
	$subject = str_replace($placeholders, $replacedData, $subject);
	$emailMsg = str_replace($placeholders, $replacedData, $contents);
	sendMail($email, $arr[0], $arr[1], $subject, $emailMsg, '', $sendCopy == '1' ? $arr[4] :
		'');
}

function differenceInDates($dtTm1, $dtTm2)
{
	$time1 = strtotime($dtTm1);
	$time2 = strtotime($dtTm2);
	$diff = abs($time2 - $time1);
	$years = floor($diff / (365 * 60 * 60 * 24));
	$months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
	$days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) /
		(60 * 60 * 24));
	$hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 *
			24 - $days * 60 * 60 * 24) / (60 * 60));
	return $days;
}

function sendGeneralEmail($email, $fromName, $fromEmail, $toName, $subject, $message1,
						  $message2, $bcc = '', $subInEmail = '')
{
	list($subject1, $contents, $sendCopy1) = getEmailContents('34');
	if ($email != '') {
		if ($subInEmail != '') {
			$subInEmail = '<div style="padding: 5px 0 15px 0; margin: 0"><p style="background: #bce8f1; border: 1px solid #bce8f1; font: normal 14px/20px Arial,Helvetica,sans-serif; color: #31708f; border-radius: 4px; margin: 0 0 20px 0; padding: 15px">' .
				$subInEmail . '</p></div>';
		}
		if ($message1 != '') {
			$message1 = '<div style="padding: 5px 0 15px 0; margin: 0"><p style="background: #d6e9c6; border: 1px solid #bce8f1; font: normal 14px/20px Arial,Helvetica,sans-serif; color: #31708f; border-radius: 4px; margin: 0 0 20px 0; padding: 15px">' .
				$message1 . '</p></div>';
		}
		if ($message2 != '') {
			$message2 = '<div style="padding: 5px 0 15px 0; margin: 0">' . $message2 .
				'</div>';
		}

		$placeholders = array(
			"#NAME#",
			"#CONTENTS1#",
			"#CONTENTS2#");
		$replacedData = array(
			$toName,
			$subInEmail . $message1,
			$message2);
		$emailMsg = str_replace($placeholders, $replacedData, $contents);
		sendMail($email, $fromName, $fromEmail, $subject, $emailMsg, '', $bcc);
	}
}

function replaceRN($message)
{
	return str_replace('\r\n', '<br />', $message);
}

function replaceBRTag($str)
{
	return str_replace('<br />', "\r\n", $str);
}

function fetch_newsltrs_cat_letter()
{

	$CI = &get_instance();
	$CI->db->select("A.CategoryId, Category, A.NewsLtrId, NewsLtrTitle");
	$CI->db->from('tbl_gf_newsltrs_cat Cat, tbl_gf_news_letter A');
	$CI->db->where('Cat.CategoryId = A.CategoryId');
	$CI->db->where('DisableNewsLtr', 0);
	$CI->db->order_by('OrderBy, Category, NewsLtrTitle');

	$result = $CI->db->get();
	return $result->result();

}

function getSMTPDetails()
{
	$CI = &get_instance();
	$smtp = '';
	$smtpAuth = '';
	$smtpHost = '';
	$smtpPort = '';
	$smtpUN = '';
	$smtpPwd = '';
	$row = $CI->General_model->getSMTPDetails();
	$smtp = $row->SMTP;
	$smtpAuth = $row->SMTPAuth;
	$smtpHost = stripslashes($row->SMTPHost);
	$smtpPort = stripslashes($row->SMTPPort);
	$smtpUN = stripslashes($row->SMTPUN);
	$smtpPwd = stripslashes($row->SMTPPwd);
	$signature = stripslashes($row->EmailSignature);
	$THEME = $row->Theme;
	$fromEmail = stripslashes($row->ToEmail);
	$url = $row->SiteURL;

	$rwLogo = $CI->General_model->getLogoPath();
	$LOGO_PATH = "uploads/$THEME/" . $rwLogo->LogoPath;
	return array(
		$smtp,
		$smtpAuth,
		$smtpHost,
		$smtpPort,
		$smtpUN,
		$smtpPwd,
		$signature,
		$LOGO_PATH,
		$fromEmail,
		$url);
}

function finalEmailMessage($message, $signature, $logoPath, $fromEmail, $url)
{
	$placeholders = array(
		"#LOGO_LINK#",
		"#LOGO#",
		"#LOGIN_LINK#",
		"#SIGNATURE#",
		"#COMPANY_EMAIL_ADDRESS#",
		"#UNSUBSCRIBE_LINK#");
	$replacedData = array(
		base_url(),
		base_url($logoPath),
		base_url('login'),
		$signature,
		'<a href="mailto:' . $fromEmail .
		'" style="color: #0f68b4; font-weight: bold; text-decoration: none">' . $fromEmail .
		'</a>',
		'http://' . $url . '/unsubscribe.php');
	return str_replace($placeholders, $replacedData, $message);

}

function sendMail($to, $fromName, $fromEmail, $subject, $message, $cc = '', $bcc =
'', $multipleBCC = '')
{
	$message = replaceRN($message);
	require_once(APPPATH . "/libraries/phpmailer/class.phpmailer.php");
	$mail = new PHPMailer();
	$mail->From = $fromEmail;
	$mail->FromName = $fromName;
	$mail->AddAddress($to, $fromName);
	if ($cc != '')
		$mail->AddCC($cc, $fromName);
	if ($bcc != '')
		$mail->AddBCC($bcc, $fromName);
	if ($multipleBCC != '') {
		$arrBCC = array();
		$arrBCC = explode(',', $multipleBCC);
		foreach ($arrBCC as $value) {
			$bccVal = trim($value);
			if ($bccVal != '') {
				$mail->AddBCC($bccVal, $fromName);
			}
		}
	}

	$mail->IsHTML(true);
	$mail->Subject = $subject;

	$arrSMTP = getSMTPDetails();
	if ($arrSMTP[0] == '1') {
		$mail->IsSMTP();
		$mail->SMTPSecure = 'tls';
		$mail->SMTPAuth = $arrSMTP[1] == '1' ? true : false; // enable SMTP authentication
		$mail->Host = $arrSMTP[2]; // sets the SMTP server
		$mail->Port = $arrSMTP[3]; // set the SMTP port for the GMAIL server
		$mail->Username = $arrSMTP[4]; // SMTP account username
		$mail->Password = $arrSMTP[5];
	}
	$signature = $arrSMTP[6];
	$finalEmailMsg = finalEmailMessage($message, $signature, $arrSMTP[7], $arrSMTP[8],
		$arrSMTP[9]);
	$mail->Body = $finalEmailMsg;
	if ($_SERVER['HTTP_HOST'] != 'localhost' || 1) {
		$mail->Send();
	}
}

function fetchCurrencies()
{
	$CI = &get_instance();
	return $CI->General_model->fetchCurrencies();
}

function fetchLanguages()
{
	return array(
		"en" => 'English',
		"fr" => 'French',
		"ru" => 'Russian',
		"sp" => 'Spanish',
		"hu" => 'Hungarian',
		"de" => 'German',
		"pl" => 'Polish',
		"pt" => 'Portuguese',
		"cns" => 'Chinese (Simplified)',
		"ro" => 'Romanian',
		"vn" => 'Vietnamese',
		"vn" => 'Italian');
}

function fetchCountries()
{
	$CI = &get_instance();
	return $CI->General_model->fetchCountries();
}

function send_push_notification($message = '', $userId)
{
	/*
    $rw = $objDBCD14->queryUniqueObject('SELECT MobileApp, Company FROM tbl_gf_email_settings WHERE Id = 1');
    if($rw->MobileApp == '1')
    {
    $title = stripslashes($rw->Company);
    $tokensQuery = $objDBCD14->query("SELECT token FROM tbl_gf_fcm_tokens WHERE user_id = '{$userId}'");
    $registration_ids = array();
    while($row = $objDBCD14->fetchNextObject($tokensQuery))
    $registration_ids[] = $row->token;
    //fcm_key
    define( 'API_ACCESS_KEY', '' );
    $fields = array(
    "registration_ids" => $registration_ids,
    "content_available" => true,
    "priority" => "high",
    "notification" => array
    (
    "title" => $title,
    "body" => $message,
    "sound" => "default"
    ),
    );


    $headers = array(
    'Authorization:key='.API_ACCESS_KEY,
    'Content-Type: application/json'
    );

    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    // Execute post
    $result = curl_exec ( $ch );
    //echo $result.'<br>';
    curl_close ( $ch );

    return $result;
    }*/
}

function numberOfDecimals($value)
{
	if ((int)$value == $value) {
		return 0;
	} else
		if (!is_numeric($value)) {
			// throw new Exception('numberOfDecimals: ' . $value . ' is not a number!');
			return false;
		}
	return strlen($value) - strrpos($value, '.') - 1;
}

function roundMe_2Digits($value)
{
	if ($value != '' && is_numeric($value)) {
		return number_format($value, 2, '.', '');
	} else
		return '0.00';
}

function roundMe($value)
{
	if ($value != '' && is_numeric($value)) {
		if (numberOfDecimals($value) <= 2)
			return number_format($value, 2, '.', ',');
		else
			if (numberOfDecimals($value) > 2)
				return number_format($value, 3, '.', ',');
	} else
		return '0.00';
}

function convertDate($datex)
{ //what is this function doing exactly?
	$vdate = 0;
	// $datex = date('Y-m-d', strtotime($datex));
	// $datex = explode("-", $datex);

	if ($datex[1] != '' && $datex[1] != '' && $datex[1] != '')
		$vdate = checkdate($datex[1], $datex[2], $datex[0]);
	if ($vdate == 1) {
		$vdate = mktime(00, 00, 00, $datex[1], $datex[2], $datex[0]);
		$vdate = @date("F d, Y", $vdate);
		return $vdate;
	} else
		return "-";
	return $datex;
}


function getUserPacksIds($userId, $serviceType)
{
	$strIds = '0';
	$CI = &get_instance();
	$CI->db->select('PackageId');
	$CI->db->from('tbl_gf_user_packages');
	$CI->db->where('UserId', $userId);
	$CI->db->where('ServiceType', $serviceType);
	$result = $CI->db->get();
	$rsUserPacks = $result->result();

	foreach ($rsUserPacks as $rsUserPack) {
		$strIds .= ', ' . $rsUserPack->PackageId;
	}
	return $strIds;
}

function getBaseUrl()
{
	// output: /myproject/index.php
	$currentPath = $_SERVER['PHP_SELF'];

	// output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index )
	$pathInfo = pathinfo($currentPath);

	// output: localhost
	$hostName = $_SERVER['HTTP_HOST'];

	// output: http://
	//$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';
	$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] ==
		443) ? "https" : "http";
	if ($pathInfo['dirname'] == '/')
		return $protocol = $protocol . '://' . $hostName . $pathInfo['dirname'];
	else
		return $protocol = $protocol . '://' . $hostName . $pathInfo['dirname'] . "/";
}

function getServiceIds($sType = 0)
{
	$strIds = '0';
	$CI = &get_instance();
	$CI->db->select('PackageId');
	$CI->db->from('tbl_gf_packages');
	$CI->db->where('sl3lbf', $sType);
	$CI->db->where('DisablePackage', 0);
	$result = $CI->db->get();
	$rsIds = $result->result();

	foreach ($rsIds as $row) {
		$strIds .= ', ' . $row->PackageId;
	}
	return $strIds;
}

function convertNextLineToBRTag($message)
{
	$message = nl2br($message);
	return trim($message);
}

function InStr($String, $Find, $CaseSensitive = false)
{
	$i = 0;
	while (strlen($String) >= $i) {
		unset($substring);
		if ($CaseSensitive) {
			$Find = strtolower($Find);
			$String = strtolower($String);
		}
		$substring = substr($String, $i, strlen($Find));
		if ($substring == $Find)
			return true;
		$i++;
	}
	return false;
}

function getTicketDetails($id)
{
	$userName = '';
	$userEmail = '';
	$subject = '';
	$tcktNo = '';
	$category = '';
	$priority = '';
	$department = '';
	$status = '';
	$date = '';
	$ip = '';
	$msg = '';
	$deptEmail = '';
	$name = '';


	$CI = &get_instance();
	$CI->db->select('UserName, UserEmail, Subject, TicketNo, TcktCategory, TcktPriority, DeptName, TcktStatus, DATE(DtTm) AS Dt, A.IP, Message, 
DeptEmail, F.FirstName, F.LastName');
	$CI->db->from('tbl_gf_tickets A, tbl_gf_tckt_category B, tbl_gf_tckt_priority C, tbl_gf_tckt_dept D, tbl_gf_tckt_status E, tbl_gf_users F');
	$CI->db->where('A.CategoryId = B.TcktCategoryId');
	$CI->db->where('A.PriorityId = C.TcktPriorityId');
	$CI->db->where('A.DepartmentId = D.DeptId');
	$CI->db->where('A.StatusId = E.TcktStatusId');
	$CI->db->where('A.UserId = F.UserId');
	$CI->db->where('TicketId', $id);
	$result = $CI->db->get();
	$row = $result->row();

	if (isset($row->TicketNo) && $row->TicketNo != '') {
		$userName = stripslashes($row->UserName);
		$userEmail = stripslashes($row->UserEmail);
		$name = stripslashes($row->FirstName) . ' ' . stripslashes($row->LastName);
		$subject = stripslashes($row->Subject);
		$tcktNo = $row->TicketNo;
		$category = stripslashes($row->TcktCategory);
		$priority = stripslashes($row->TcktPriority);
		$department = stripslashes($row->DeptName);
		$status = stripslashes($row->TcktStatus);
		$date = $row->Dt;
		$ip = $row->IP;
		$msg = stripslashes($row->Message);
		$deptEmail = stripslashes($row->DeptEmail);
	}
	return array(
		$userName,
		$userEmail,
		$subject,
		$tcktNo,
		$category,
		$priority,
		$department,
		$status,
		$date,
		$ip,
		$msg,
		$deptEmail,
		$name);
}

function ticketDetailsEmail($tcktId, $type)
{
	$arr = getEmailDetails();
	list($subject, $contents, $sendCopy) = getEmailContents(20);
	$welcomeMsg = '';
	list($userName, $userEmail, $subj, $tcktNo, $category, $priority, $department, $status,
		$date, $ip, $msg, $deptEmail, $name) = getTicketDetails($tcktId);

	if ($type == '0') {
		$subject = "New Ticket Created - [#$tcktNo]";
		$welcomeMsg = '<p style="background: #d9edf7; border: 1px solid #bce8f1; font: normal 14px/20px Arial,Helvetica,sans-serif; color: #31708f; border-radius: 4px; margin: 0 0 20px 0; padding: 15px">New ticket [#' .
			$tcktNo . '] has been created.</p>';
	} else
		if ($type == '1') {
			$subject = "New reply for the Ticket - [#$tcktNo]";
			$welcomeMsg = '<p style="background: #dff0d8; border: 1px solid #d6e9c6; font: normal 14px Arial,Helvetica,sans-serif; color: #3c763d; border-radius: 4px; margin: 0 0 20px 0; padding: 15px">New reply for the ticket [#' .
				$tcktNo . ']</p>';
		} else
			if ($type == '2') {
				$subject = "New reply for the Ticket - [#$tcktNo]";
				$welcomeMsg = '<p style="background: #dff0d8; border: 1px solid #d6e9c6; font: normal 14px Arial,Helvetica,sans-serif; color: #3c763d; border-radius: 4px; margin: 0 0 20px 0; padding: 15px">New reply for the ticket [#' .
					$tcktNo . ']</p>';
			}

	$placeholders = array(
		"#WELCOME_NOTE#",
		"#USER_NAME#",
		"#NAME#",
		"#TICKET_NO#",
		"#SUBJECT#",
		"#CATEGORY#",
		"#PRIORITY#",
		"#DEPARTMENT#",
		"#STATUS#",
		"#TICKET_DATE#",
		"#IP#",
		"#TICKET_MSG#");

	$replacedData = array(
		$welcomeMsg,
		$userName,
		$userName,
		$tcktNo,
		$subj,
		$category,
		$priority,
		$department,
		$status,
		$date,
		$ip,
		$msg);
	$emailMsg = str_replace($placeholders, $replacedData, $contents);
	if ($type == '1')
		$deptEmail = '';
	if ($type == '2') {
		$placeholders = array(
			"#WELCOME_NOTE#",
			"#USER_NAME#",
			"#NAME#",
			"#TICKET_NO#",
			"#SUBJECT#",
			"#CATEGORY#",
			"#PRIORITY#",
			"#DEPARTMENT#",
			"#STATUS#",
			"#TICKET_DATE#",
			"#IP#",
			"#TICKET_MSG#");

		$replacedData = array(
			$welcomeMsg,
			'Admin',
			$userName,
			$tcktNo,
			$subj,
			$category,
			$priority,
			$department,
			$status,
			$date,
			$ip,
			$msg);
		$emailMsg = str_replace($placeholders, $replacedData, $contents);
		sendMail($deptEmail, $arr[0], $arr[1], $subject, $emailMsg, '', $sendCopy == '1' ?
			$arr[4] : '');
	} else {
		sendMail($userEmail, $arr[0], $arr[1], $subject, $emailMsg, '', $sendCopy == '1' ?
			$arr[4] : '');
		$placeholders = array(
			"#WELCOME_NOTE#",
			"#USER_NAME#",
			"#NAME#",
			"#TICKET_NO#",
			"#SUBJECT#",
			"#CATEGORY#",
			"#PRIORITY#",
			"#DEPARTMENT#",
			"#STATUS#",
			"#TICKET_DATE#",
			"#IP#",
			"#TICKET_MSG#");
		$replacedData = array(
			$welcomeMsg,
			'Admin',
			$userName,
			$tcktNo,
			$subj,
			$category,
			$priority,
			$department,
			$status,
			$date,
			$ip,
			$msg);
		$emailMsg = str_replace($placeholders, $replacedData, $contents);
		sendMail($deptEmail, $arr[0], $arr[1], $subject, $emailMsg);
	}
}

function rand_integers($length)
{
	$chars = "0123456789";
	$str = '';
	$size = strlen($chars);
	for ($i = 0; $i < $length; $i++) {
		$str .= $chars[rand(0, $size - 1)];
	}
	return $str;
}

//ahmed
function rand_string($length)
{
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$str = '';
	$size = strlen($chars);
	for ($i = 0; $i < $length; $i++) {
		$str .= $chars[rand(0, $size - 1)];
	}
	return $str;
}

function crypt_key($ckey)
{
	$CI = &get_instance();
	$keys = array();

	$c_key = base64_encode(sha1(md5($ckey)));
	$c_key = substr($c_key, 0, round(ord($ckey{0}) / 5));

	$c2_key = base64_encode(md5(sha1($ckey)));
	$last = strlen($ckey) - 1;
	$c2_key = substr($c2_key, 1, round(ord($ckey{$last}) / 7));

	$c3_key = base64_encode(sha1(md5($c_key) . md5($c2_key)));
	$mid = strval(round($last / 2));
	$c3_key = substr($c3_key, 1, round(ord($ckey{$mid}) / 9));

	$c_key = $c_key . $c2_key . $c3_key;
	$c_key = base64_encode($c_key);

	for ($i = 0; $i < strlen($c_key); $i++) {
		$keys[] = $c_key[$i];
	}
	return $keys;
}

function encrypt($string, $keys = array())
{
	$CI = &get_instance();
	if (!$keys) {
		return 0;
	}
	$string = base64_encode($string);
	for ($i = 0; $i < strlen($string); $i++) {
		$id = $i % count($keys);
		$ord = ord($string{$i});
		$ord = $ord or ord($keys[$id]);
		$id++;
		$ord = $ord and ord($keys[$id]);
		$id++;
		$ord = $ord xor ord($keys[$id]);
		$id++;
		$ord = $ord + ord($keys[$id]);
		$string{$i} = chr($ord);
	}
	return base64_encode($string);
}

function decrypt($string, $keys = array())
{
	$CI = &get_instance();
	if (!$keys) {
		return 0;
	}

	$string = base64_decode($string);
	for ($i = 0; $i < strlen($string); $i++) {
		$id = $i % count($keys);
		$ord = ord($string{$i});
		$ord = $ord xor ord($keys[$id]);
		$id++;
		$ord = $ord and ord($keys[$id]);
		$id++;
		$ord = $ord or ord($keys[$id]);
		$id++;
		$ord = $ord - ord($keys[$id]);
		$string{$i} = chr($ord);
	}
	return base64_decode($string);
}


function theString_Decrypt($enc_text, $password = 'P@88W04D', $iv_len = 16)
{
	$CI = &get_instance();
	$enc_text = base64_decode($enc_text);
	$n = strlen($enc_text);
	$i = $iv_len;
	$plain_text = '';
	$iv = substr($password ^ substr($enc_text, 0, $iv_len), 0, 512);
	while ($i < $n) {
		$block = substr($enc_text, $i, 16);
		$plain_text .= $block ^ pack('H*', md5($iv));
		$iv = substr($block . $iv, 0, 512) ^ $password;
		$i += 16;
	}
	return preg_replace('/\\x13\\x00*$/', '', $plain_text);
}

function invoiceEmail($email, $name, $invoiceNo, $credits, $amount, $currency, $currDt,
					  $paymentMethod, $invType = 0, $BACKEND_FOLDER = '', $userName = '', $customMsg =
					  '')
{
	$lnk = '';
	$unRow = '';
	$msg = '';
	$arr = getEmailDetails();
	list($subject, $contents, $sendCopy) = getEmailContents(11);
	if ($userName != '') {
		$unRow = '<tr><td width="150" valign="top" align="left" style="font: bold 13px/20px Arial,Helvetica,sans-serif">
			<p style="padding: 0; margin: 0 0 15px 0; color: #8c8c8c; display: inline-block; width: 85%">UserName:</p>
		</td>
		<td valign="top" align="left">
			<p style="padding: 0; margin: 0 0 15px 0; color: #1a1a1a; font: normal 13px Arial,Helvetica,sans-serif; line-height: 20px">' .
			$userName . '</p>
		</td>
	</tr>';
	}
	if ($BACKEND_FOLDER != '') {
		if ($invType == '7') {
			$lnk = '<p style="font: normal 14px Arial,Helvetica,sans-serif; color: #3c763d">Please click <a href="http://' .
				$arr[3] . '/' . $BACKEND_FOLDER . 'rinvoice.php?id=' . $invoiceNo .
				'" style="color: #08a2f8" target="_blank">here</a> to update invoice.</p>';
		} else
			if ($invType == '8') {
				$lnk = '<p style="font: normal 14px Arial,Helvetica,sans-serif; color: #3c763d">Please click <a href="http://' .
					$arr[3] . '/' . $BACKEND_FOLDER . 'einvoice.php?id=' . $invoiceNo .
					'" style="color: #08a2f8" target="_blank">here</a> to update invoice.</p>';
			} else {
				$lnk = '<p style="font: normal 14px Arial,Helvetica,sans-serif; color: #3c763d">Please click <a href="http://' .
					$arr[3] . '/' . $BACKEND_FOLDER . 'payment.php?id=' . $invoiceNo .
					'" style="color: #08a2f8" target="_blank">here</a> to update his credits.</p>';
			}

	}
	if ($invType == '0') {
		$msg = '<p style="background: #d9edf7; border: 1px solid #bce8f1; font: normal 14px/20px Arial,Helvetica,sans-serif; color: #31708f; border-radius: 4px; margin: 0 0 20px 0; padding: 15px">New invoice has been added for invoice # <span style="font-weight: 700">' .
			$invoiceNo . '</span>.</p>';
	} else
		if ($invType == '1') //PAID
		{
			$msg = '<p style="background: #dff0d8; border: 1px solid #d6e9c6; font: normal 14px Arial,Helvetica,sans-serif; color: #3c763d; border-radius: 4px; margin: 0 0 20px 0; padding: 15px">Payment has been added successfully for Invoice # <span style="font-weight: 700">' .
				$invoiceNo . '</span>.</p>';
			$subject = 'Payment successfully added for Invoice # ' . $invoiceNo;
		} else
			if ($invType == '2') // UNPAID
			{
				$msg = '<p style="background: #dff0d8; border: 1px solid #d6e9c6; font: normal 14px Arial,Helvetica,sans-serif; color: #3c763d; border-radius: 4px; margin: 0 0 20px 0; padding: 15px">Unpaid [Manually process Invoice # <span style="font-weight: 700">' .
					$invoiceNo . '</span>].</p>';
				$subject = "Unpaid [Manually process Invoice # $invoiceNo]";
			} else
				if ($invType == '3') {
					$msg = '<p style="background: #dff0d8; border: 1px solid #d6e9c6; font: normal 14px Arial,Helvetica,sans-serif; color: #3c763d; border-radius: 4px; margin: 0 0 20px 0; padding: 15px">Payment has been received for Invoice # <span style="font-weight: 700">' .
						$invoiceNo . '</span></p>';
					$subject = "Payment received for Invoice # $invoiceNo";
				} else
					if ($invType == '4') {
						$msg = '<p style="background: #dff0d8; border: 1px solid #d6e9c6; font: normal 14px Arial,Helvetica,sans-serif; color: #3c763d; border-radius: 4px; margin: 0 0 20px 0; padding: 15px">Your Payment is Over Due for Invoice # <span style="font-weight: 700">' .
							$invoiceNo . '</span>].</p>';
						$subject = "Payment Over Due Reminder for Invoice # $invoiceNo";
					} else
						if ($invType == '5') // REVIEW
						{
							$msg = '<p style="background: #dff0d8; border: 1px solid #d6e9c6; font: normal 14px Arial,Helvetica,sans-serif; color: #3c763d; border-radius: 4px; margin: 0 0 20px 0; padding: 15px">Review [Manually process Invoice # <span style="font-weight: 700">' .
								$invoiceNo . '</span>].</p>';
							$subject = "Review [Manually process Invoice # $invoiceNo]";
						} else
							if ($invType == '6') // CUSTOM MESSAGE
							{
								$msg = '<p style="background: #dff0d8; border: 1px solid #d6e9c6; font: normal 14px Arial,Helvetica,sans-serif; color: #3c763d; border-radius: 4px; margin: 0 0 20px 0; padding: 15px">' .
									$customMsg . '</p>';
								//$subject = strip_tags($customMsg);
								$subject = "Invoice # $invoiceNo - Your Credits have been updated!";
							} else
								if ($invType == '7') // RETAIL
								{
									$msg = '<p style="background: #dff0d8; border: 1px solid #d6e9c6; font: normal 14px Arial,Helvetica,sans-serif; color: #3c763d; border-radius: 4px; margin: 0 0 20px 0; padding: 15px">Payment has been received for Invoice # <span style="font-weight: 700">' .
										$invoiceNo . '</span></p>';
									$subject = "Payment received for Invoice # $invoiceNo";
								} else
									if ($invType == '8') // E-Com REVIEW
									{
										$msg = '<p style="background: #dff0d8; border: 1px solid #d6e9c6; font: normal 14px Arial,Helvetica,sans-serif; color: #3c763d; border-radius: 4px; margin: 0 0 20px 0; padding: 15px">Review [Manually process Invoice # <span style="font-weight: 700">' .
											$invoiceNo . '</span>].</p>';
										$subject = "Review [Manually process Invoice # $invoiceNo]";
									}
	$fee = $amount - $credits;
	if ($paymentMethod == 'Mass PayPal')
		$fee = '-';
	$placeholders = array(
		"#CUSTOMER_NAME#",
		"#INVOICE_NO#",
		"#INVOICE_AMOUNT#",
		"#CURRENCY#",
		"#AMOUNT_PAID#",
		"#FEE#",
		"#NET_AMOUNT#",
		"#INVOICE_DATE#",
		"#PAYMENT_METHOD#",
		"#INVOICE_LINK#",
		"#INTRO_MSG#",
		"#USER_NAME#");
	$replacedData = array(
		$name,
		$invoiceNo,
		$credits,
		$currency,
		$credits,
		$fee,
		$amount,
		$currDt,
		$paymentMethod,
		$lnk,
		$msg,
		$unRow);
	$subject = str_replace($placeholders, $replacedData, $subject);
	$emailMsg = str_replace($placeholders, $replacedData, $contents);
	sendMail($email, $arr[0], $arr[1], $subject, $emailMsg, '', $sendCopy == '1' ? $arr[4] :
		'');
}

function masspayment($payer_email, $UserId, $Credits, $Amount, $transactionId, $pStatusId,
					 $pMethodTypeId, $pMethodId, $myCurrency, $id)
{
	$CI = &get_instance();

	$keys = crypt_key($UserId);

	$invCredits = decrypt($Credits, $keys);
	if ($invCredits == '')
		$invCredits = '0';

	$invAmount = decrypt($Amount, $keys);
	if ($invAmount == '')
		$invAmount = '0';


	$ppTrnsfrFee = $CI->input->post('ppTrnsfrFee') ?: '0';

	if ($CI->input->post('txtTransID') && $pStatusId == '1' && $pMethodTypeId == '2') {
		$transID = $CI->input->post('txtTransID') ?: '';
		$paypalId = $CI->input->post('paypalId') ?: '';
		$invDt = $CI->input->post('invDt') ?: '';
		$feeType = 0;
		$pMFee = 0;
		if ($transID != '') {
			if ($paypalId != '') {
				$row = $CI->User_model->get_reciever_data($paypalId);
				if (isset($row->Username) && $row->Username != '') {
					$accountId = $row->Username;
					$apiUN = stripslashes($row->APIUsername);
					$apiPwd = stripslashes($row->APIPassword);
					$apiSign = stripslashes($row->APISignature);
					$massPayDays = $row->MassPaymentTime;
					$pMFee = $row->Fee;
					$feeType = $row->FeeType;
				}
			} else {
				$row = $CI->User_model->get_payment_methods($pMethodId);
				if (isset($row->Username) && $row->Username != '') {
					$accountId = $row->Username;
					$apiUN = stripslashes($row->APIUsername);
					$apiPwd = stripslashes($row->APIPassword);
					$apiSign = stripslashes($row->APISignature);
					$massPayDays = $row->MassPaymentTime;
					$pMFee = $row->Fee;
					$feeType = $row->FeeType;
				}
			}
			$processPayment = true;
			$info = 'USER=' . $apiUN . '&PWD=' . $apiPwd . '&SIGNATURE=' . $apiSign .
				'&VERSION=94' . '&METHOD=GetTransactionDetails' .
				'&STARTDATE=2012-01-01T05:38:48Z' . '&ENDDATE=2030-01-01T05:38:48Z' .
				'&TRANSACTIONID=' . $transID;
			$curl = curl_init('https://api-3t.paypal.com/nvp');
			curl_setopt($curl, CURLOPT_FAILONERROR, true);
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $info);
			curl_setopt($curl, CURLOPT_HEADER, 0);
			curl_setopt($curl, CURLOPT_POST, 1);
			$rsBuyerStatus = curl_exec($curl);
			parse_str($rsBuyerStatus, $rsBuyerStatus);

			$transDtTm = str_replace('T', ' ', $rsBuyerStatus['ORDERTIME']);
			$transDtTm = str_replace('Z', '', $transDtTm);

			if ($massPayDays != '0') {
				$currDtTm = setDtTmWRTYourCountry();
				$days = daysBetweenTwoDates(trim($transDtTm), $currDtTm);
				if ($days > $massPayDays) {
					$processPayment = false;
				}
			}
			if ($processPayment) {
				if ($rsBuyerStatus['PAYERSTATUS'] == 'verified' || $rsBuyerStatus['PAYERSTATUS'] ==
					'unverified') {
					$info = 'USER=' . $apiUN . '&PWD=' . $apiPwd . '&SIGNATURE=' . $apiSign .
						'&VERSION=94' . '&METHOD=TransactionSearch' . '&STARTDATE=2012-01-01T05:38:48Z' .
						'&ENDDATE=2030-01-01T05:38:48Z' . '&TRANSACTIONID=' . $transID;
					$curl = curl_init('https://api-3t.paypal.com/nvp');
					curl_setopt($curl, CURLOPT_FAILONERROR, true);
					curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
					curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $info);
					curl_setopt($curl, CURLOPT_HEADER, 0);
					curl_setopt($curl, CURLOPT_POST, 1);
					$result = curl_exec($curl);

					$result1 = $result;

					parse_str($result1, $result1);

					$result = explode("&", $result);

					foreach ($result as $value) {
						$value = explode("=", $value);
						$temp[$value[0]] = $value[1];
					}
					$counter = intval(count($temp) / 11);

					for ($i = 0; $i < $counter; $i++) {
						//$timestamp		=    urldecode($result1["L_TIMESTAMP".$i]);
						//$timezone         =    urldecode($result1["L_TIMEZONE".$i]);
						//$type             =    urldecode($result1["L_TYPE".$i]);
						$payer_email = urldecode($result1["L_EMAIL" . $i]);
						//$name             =    urldecode($result1["L_NAME".$i]);
						$transactionId = urldecode($result1["L_TRANSACTIONID" . $i]);
						$ppPmntStatus = urldecode($result1["L_STATUS" . $i]);
						$invAmount = urldecode($result1["L_AMT" . $i]);
						$currency_code = urldecode($result1["L_CURRENCYCODE" . $i]);
						$fee_amount = urldecode($result1["L_FEEAMT" . $i]);
						$invCredits = urldecode($result1["L_NETAMT" . $i]);
					}

					if (($ppPmntStatus == 'Completed' || $ppPmntStatus == 'Cleared') && $currency_code ==
						$myCurrency && $transactionId != '') {
						$receiver_email = '';
						//$transactionId = $transID;//check_input($result['L_TRANSACTIONID0'], $objDBCD14->dbh);
						//$rs = $objDBCD14->queryUniqueObject("SELECT PaymentId FROM tbl_gf_payments WHERE TransactionId = '$transactionId' AND PaymentStatus = '2'");

						$rs = $CI->db->User_model->get_payment_id($transactionId);
						if (isset($rs->PaymentId) && $rs->PaymentId != '') {
							$message = "Transaction ID - $transactionId has already been added in the system against invoice # - " .
								$rs->PaymentId;
							return $message;
						} else {

							$rs = $CI->User_model->get_payment_detail($id);
							if (isset($rs->PaymentId) && $rs->PaymentId != '') {
								$INV_AMOUNT = decrypt($rs->Amount, $keys);
								$INV_CURRENCY = stripslashes($rs->Currency);
								$INV_DT = $rs->PaymentDtTm;

								$r = $CI->User_model->get_user_autofillcredits();
								$autoFillCredits = $r->AutoFillCredits;

								/* ============================== CHECK IF ANY FEE IS NOT SET ================================*/
								if (is_numeric($fee_amount) && $fee_amount < 0) {
									$amountToBeDeducted = $invAmount * 0.05;
									$invCredits = roundMe($invAmount - $amountToBeDeducted);
								}
								/* ============================== CHECK IF ANY FEE IS NOT SET ================================*/
								/* ============================== CHECK IF FEE TYPE IS FROM BACKEND ================================*/
								if ($feeType == '1' && is_numeric($pMFee) && $pMFee > 0) {
									$amountToBeDeducted = $invAmount * $pMFee;
									$invCredits = roundMe($invAmount - $amountToBeDeducted);
								}

								$encInvCrdts = encrypt($invCredits, $keys);
								$encInvAmnt = encrypt($invAmount, $keys);

								if ($autoFillCredits == '1') {
									$credits = 0;

									$rsCredits = $CI->User_model->get_user_credits();
									if (isset($rsCredits->Credits) && $rsCredits->Credits != '') {
										$credits = $rsCredits->Credits;
									}
									$decCredits = decrypt($credits, $keys);

									$decCredits += $invCredits;
									$encCredits = encrypt($decCredits, $keys);

									$CI->User_model->update_user_credits($encCredits);

									$dtTm = setDtTmWRTYourCountry();

									$hstDesc = "+ Add Funds (Invoice #$id)";


									$data = array(
										'UserId' => $this->session->userdata('GSM_FUS_UserId'),
										'Credits' => $invCredits,
										'FeePercentage' => $ppTrnsfrFee . '%',
										'Description' => $hstDesc,
										'IMEINo' => '',
										'HistoryDtTm' => $dtTm,
										'CreditsLeft' => $encCredits,
										'PaymentId' => $id);

									$CI->User_model->insert_credit_history($data);
									$CI->User_model->update_payments($receiver_email, $payer_email, $transactionId,
										$dtTm, $encInvCrdts, $encInvAmnt, $id);

									$pStatusId = 2;
									send_push_notification($invCredits . ' Credits have been added in your account!',
										$this->session->userdata('GSM_FUS_UserId'));
									if ($this->session->userdata('UserEmail') != '') {
										$arr = getEmailDetails();
										invoiceEmail($this->session->userdata('UserEmail'), $this->session->userdata('UserName'),
											$id, $invCredits, $INV_AMOUNT, $INV_CURRENCY, $INV_DT, 'Mass PayPal', 1);
										invoiceEmail($arr[4], 'Admin', $id, $invCredits, $INV_AMOUNT, $INV_CURRENCY, $INV_DT,
											'Mass PayPal', 1, $BACKEND_FOLDER, $_SESSION['UserName']);
									}
									$message = $this->lang->line('CUST_GEN_MSG');
									return $message;
								} else {

									$this->db->update_payment_2($receiver_email, $payer_email, $transactionId, $dtTm,
										$encInvCrdts, $encInvAmnt, $id);


									$arr = getEmailDetails();
									invoiceEmail($arr[4], 'Admin', $id, $invCredits, $INV_AMOUNT, $INV_CURRENCY, $INV_DT,
										'Mass PayPal', 5, $BACKEND_FOLDER, $_SESSION['UserName']);
									$message = 'Operation has been completed successfully. Please contact admin to add credits into your account!';
									return $message;
								}
							} else {
								$message = "Transaction ID - $transactionId has already been added in the system!";
								return $message;
							}
						}
					} else {
						$message = "Transaction ID - $transactionId has not been paid yet!";
						return $message;
					}


				} else {
					$message = "Your payment is not a verified one, please contact admin!";
					return $message;
				}
			} else {
				$message = "Paypal transaction has been expired, please contact Admin!";
				return $message;
			}

		} else {
			$message = 'Please enter Transaction ID';
			return $message;
		}

	}
}

function fetch_currency_data()
{

	$CI = &get_instance();

	$CI->db->select('CurrencyAbb , CurrencyId , CurrencySymbol , Currency');
	$CI->db->from('tbl_gf_currency');
	$CI->db->where('DefaultCurrency', 1);
	$CI->db->where('DisableCurrency', 0);

	$result = $CI->db->get();
	return $result->row();
}

function fetch_currency_by_id()
{

	$CI = &get_instance();

	$CI->db->select('CurrencyId, ConversionRate');
	$CI->db->from('tbl_gf_currency');
	$CI->db->where('DefaultCurrency', 0);
	$CI->db->where('DisableCurrency', 0);

	$result = $CI->db->get();
	return $result->result();
}

function fetch_currency_info()
{

	$CI = &get_instance();

	$CI->db->select('CurrencyId AS Id, Currency AS Value');
	$CI->db->from('tbl_gf_currency');
	$CI->db->order_by('Currency');

	$result = $CI->db->get();
	return $result->result();
}

function fetch_service_api_pricing($fs, $id, $onlyAPIId)
{

	$CI = &get_instance();

	$CI->db->select('*');
	$CI->db->from('tbl_gf_service_api_pricing');
	$CI->db->where('ServiceType', $fs);
	$CI->db->where('ServiceId', $id);
	$CI->db->where('ServiceAPIId', $onlyAPIId);

	$result = $CI->db->get();
	return $result->result();
}

function fetch_price_plans()
{

	$CI = &get_instance();

	$CI->db->select('PricePlanId, PricePlan');
	$CI->db->from('tbl_gf_price_plans');
	$CI->db->where('DisablePricePlan', 0);
	$CI->db->order_by('PricePlan');

	$result = $CI->db->get();
	return $result->result();

}

function fetch_price_plans_2()
{
	$CI = &get_instance();

	$CI->db->select('PricePlanId as Id, PricePlan as Value');
	$CI->db->from('tbl_gf_price_plans');
	$CI->db->where('DisablePricePlan', 0);
	$CI->db->order_by('PricePlan');

	$result = $CI->db->get();
	return $result->result();
}

function getPlansPricesForService($serviceId, $sc)
{
	$PACK_PRICES = array();
	$CI = &get_instance();

	$CI->db->select('PlanId, CurrencyId, Price');
	$CI->db->from('tbl_gf_plans_packages_prices');
	$CI->db->where('PackageId', $serviceId);
	$CI->db->where('ServiceType', $sc);

	$result = $CI->db->get();
	$rs = $result->result();


	foreach ($rs as $row) {
		$PACK_PRICES[$row->PlanId][$row->CurrencyId] = roundMe($row->Price);
	}
	return $PACK_PRICES;
}

function getpackprice($myNetworkId, $MY_CURRENCY_ID, $IMEI_TYPE)
{
	$CI = &get_instance();
	$rsPackPrice = $CI->User_model->get_package_prices($CI->session->userdata('GSM_FUS_UserId'),
		$myNetworkId);
	if (isset($rsPackPrice->Price) && $rsPackPrice->Price != '') {
		return $packagePrice = number_format($rsPackPrice->Price, 2, '.', '');
	} else {
		$rsPackPrice = $CI->User_model->get_package_prices_2($CI->session->userdata('GSM_FUS_UserId'),
			$myNetworkId, $MY_CURRENCY_ID, $IMEI_TYPE);

		if (isset($rsPackPrice->Price) && $rsPackPrice->Price != '') {
			return $packagePrice = number_format($rsPackPrice->Price, 2, '.', '');
		} else {
			$USER_CURRENCY_RATE = 1;
			$rwUsrRate = $CI->User_model->get_converstion_rate($CI->session->userdata('GSM_FUS_UserId'));
			if (isset($rwUsrRate->ConversionRate) && $rwUsrRate->ConversionRate != '') {
				$USER_CURRENCY_RATE = $rwUsrRate->ConversionRate;
			}
			$rsPlanPr_DEFAULT = $CI->User_model->get_package_prices_3($CI->session->
			userdata('GSM_FUS_UserId'), $myNetworkId, $IMEI_TYPE);

			if (isset($rsPlanPr_DEFAULT->Price) && $rsPlanPr_DEFAULT->Price != '') {
				return $packagePrice = number_format($rsPlanPr_DEFAULT->Price * $USER_CURRENCY_RATE,
					2, '.', '');
			} else {
				$rsPackPrice = $CI->User_model->get_package_price_4($MY_CURRENCY_ID, $myNetworkId);
				if (isset($rsPackPrice->PackageId) && $rsPackPrice->PackageId != '') {
					if ($rsPackPrice->Price == '') {
						return $packagePrice = number_format($rsPackPrice->PackagePrice * $USER_CURRENCY_RATE,
							2, '.', '');
					} else
						return $packagePrice = number_format($rsPackPrice->Price, 2, '.', '');
				}
			}
		}
	}
}

function getlogpackprice($USER_ID, $logPackageId, $MY_CURRENCY_ID, $myNetworkId, $CONVERSION_RATE)
{
	$CI = &get_instance();
	$rsPackPrice = $CI->User_model->get_users_log_packages($USER_ID, $logPackageId);
	if (isset($rsPackPrice->Price) && $rsPackPrice->Price != '') {
		return $packagePrice = $rsPackPrice->Price;
	} else {
		$USER_CURRENCY_RATE = 1;
		$rsPackPrice = $CI->User_model->get_users_plans_packages($USER_ID, $logPackageId,
			$MY_CURRENCY_ID);
		if (isset($rsPackPrice->Price) && $rsPackPrice->Price != '') {
			return $packagePrice = $rsPackPrice->Price;
		}
		if (isset($rwUsrRate->ConversionRate) && $rwUsrRate->ConversionRate != '') {
			$USER_CURRENCY_RATE = $rwUsrRate->ConversionRate;
		}
		$rsPlanPr_DEFAULT = $CI->User_model->get_user_packageprices_and_currency($USER_ID,
			$logPackageId);

		if (isset($rsPlanPr_DEFAULT->Price) && $rsPlanPr_DEFAULT->Price != '') {
			return $packagePrice = number_format($rsPlanPr_DEFAULT->Price * $USER_CURRENCY_RATE,
				2, '.', '');
		} else {
			$rsPackPrice = $CI->User_model->get_packages_and_curencies($MY_CURRENCY_ID, $myNetworkId);

			if (isset($rsPackPrice->PackageId) && $rsPackPrice->PackageId != '') {
				if ($rsPackPrice->Price == '') {

					return $packagePrice = number_format($rsPackPrice->PackagePrice * $CONVERSION_RATE,
						2, '.', '');

				} else
					return $packagePrice = number_format($rsPackPrice->Price, 2, '.', '');
			}
		}
	}
}

function newServerOrderEmail($email, $name, $service, $orderData, $ip, $creditsDeducted,
							 $finalCredits, $orderDt, $serviceId, $altEmail, $serial, $bccEmails = '', $notes =
							 '')
{
	$arr = getEmailDetails();
	list($subject, $contents, $sendCopy) = getEmailContents(6);

	$placeholders = array(
		"#CUSTOMER_NAME#",
		"#USER_NAME#",
		"#SERVICE_NAME#",
		"#ORDER_DATA#",
		"#CREDITS_DEDUCTED#",
		"#FINAL_CREDITS#",
		"#ORDER_DATE#",
		"#IP#",
		"#COMPANY_NAME#",
		"#COMPANY_EMAIL_ADDRESS#",
		"#TRACK_ORDER_PAGE#",
		"#SERIAL_NO#",
		"#NOTES#");
	$replacedData = array(
		$name,
		$name,
		$service,
		$orderData,
		$creditsDeducted,
		$finalCredits,
		$orderDt,
		$ip,
		stripslashes($arr[2]),
		'<a href="mailto:' . $arr[4] . '">' . $arr[4] . '</a>',
		'<a href="http://' . $arr[3] . '/trackorder.php" target="_blank">here</a>',
		$serial,
		$notes == '' ? '-' : $notes);
	$emailMsg = str_replace($placeholders, $replacedData, $contents);
	sendMail($arr[4], $arr[0], $arr[1], $subject, $emailMsg, '', $email, $bccEmails);
	if ($altEmail != '')
		sendMail($altEmail, $arr[0], $arr[1], $subject, $emailMsg);
}

function bulkQuantity_ServerService($packId, $quantity, $userId)
{
	$CI = &get_instance();
	$rw = $CI->User_model->get_user_priceplan($userId);
	$price = '0';
	if (isset($rw->PricePlanId) && $rw->PricePlanId > 0) {
		$rsPrice = $CI->User_model->get_bulk_prices($packId, $rw->PricePlanId, $quantity);

		if (isset($rsPrice->Price) && $rsPrice->Price != '') {
			$price = $rsPrice->Price;
		} else {
			$rsPrice = $CI->User_model->get_bulk_price_by_maxQty($packId, $rw->PricePlanId,
				$quantity);
			if (isset($rsPrice->Price) && $rsPrice->Price != '') {
				$price = $rsPrice->Price;
			}
		}
	} else {
		$rsPrice = $CI->User_model->get_bulk_price_by_max_min_Qty($packId, $quantity);
		if (isset($rsPrice->Price) && $rsPrice->Price != '') {
			//$price = number_format($rsPrice->Price, 2, '.', '');
			$price = $rsPrice->Price;
		} else {
			$rsPrice = $CI->User_model->get_bulk_price_without_group_id($packId, $quantity);
			if (isset($rsPrice->Price) && $rsPrice->Price != '') {
				//$price = number_format($rsPrice->Price, 2, '.', '');
				$price = $rsPrice->Price;
			}
		}
	}
	return $price;
}


function getDefaultPlansPrices($sc, $currencyId, $categoryId = 0)
{
	$CI = &get_instance();
	$PACK_PRICES = array();

	switch ($sc) {
		case '0': // IMEI services
			$tblPckName = 'tbl_gf_packages';
			$colId = 'PackageId';
			break;
		case '1': // File services
			$tblPckName = 'tbl_gf_packages';
			$colId = 'PackageId';
			break;
		case '2': // Server services
			$tblPckName = 'tbl_gf_log_packages';
			$colId = 'LogPackageId';
			break;
	}
	if ($categoryId > 0) {
		$CI->db->select("PlanId, A.PackageId, Price");
		$CI->db->from("tbl_gf_plans_packages_prices A, $tblPckName B");
		$CI->db->where("A.PackageId = B.$colId");
		$CI->db->where('A.ServiceType', $sc);
		$CI->db->where('CategoryId', $categoryId);
		$CI->db->where('CurrencyId', $currencyId);
	} else {
		$CI->db->select("PlanId, PackageId, Price");
		$CI->db->from("tbl_gf_plans_packages_prices");
		$CI->db->where('ServiceType', $sc);
		$CI->db->where('CurrencyId', $currencyId);

	}

	$result = $CI->db->get();
	$rs = $result->result();
	foreach ($rs as $row) {
		$PACK_PRICES[$row->PlanId][$row->PackageId] = roundMe($row->Price);
	}
	return $PACK_PRICES;
}


function fetch_log_package_category($tblName, $strWhereCat)
{
	$CI = &get_instance();

	$result = $CI->db->query("SELECT CategoryId AS Id, Category AS Value FROM $tblName WHERE DisableCategory = 0 
    AND ArchivedCategory = 0 $strWhereCat ORDER BY OrderBy, Category");

	return $result->result();

}


function fetch_log_package_category_by_arc_cat()
{
	$CI = &get_instance();
	$CI->db->select('CategoryId AS Id, Category AS Value');
	$CI->db->from('tbl_gf_log_package_category');
	$CI->db->where('DisableCategory', 0);
	$CI->db->where('ArchivedCategory', 0);
	$CI->db->order_by('OrderBy');
	$CI->db->order_by('Category');

	$result = $CI->db->get();
	return $result->result();
}

function custom_query_services_quickedit($colId, $colTitle, $tblPckName, $disableCol, $strWhereSrv, $categoryId)
{
	$CI = &get_instance();
	$result = $CI->db->query("SELECT $colId AS Id, $colTitle AS Value FROM $tblPckName WHERE $disableCol = 0 $strWhereSrv AND CategoryId = '$categoryId' AND ArchivedPack = 0 ORDER BY PackOrderBy, $colTitle");

	return $result->result();


}

function fetch_packageId_custom($colId, $tblPckName, $disableCol, $strWhereSrv, $strWhere, $colTitle)
{
	$CI = &get_instance();
	$result = $CI->db->query("SELECT $colId FROM $tblPckName WHERE $disableCol = 0 AND ArchivedPack = 0 
    $strWhereSrv $strWhere ORDER BY PackOrderBy, $colTitle");
	return $result->result();
}


function checkCountryRestriction($userId, $email, $userName, $oType)
{
	/*
    $IP = $_SERVER['REMOTE_ADDR'];
    $countryISO = '00';
    if (!empty($IP))
    {
    $countryISO = getLocationInfoByIp($IP);
    $rwUsrCntry = $objDBCD14->queryUniqueObject("SELECT COUNT(UserId) AS UserValidity FROM tbl_gf_user_login_countries WHERE ISO = '$countryISO' AND UserId = '$userId'");
    if ($rwUsrCntry->UserValidity == 0)
    {
    //send_push_notification('A recent login attempt failed due to the Country you are trying to login from is Not allowed.', $rowUser->UserId, $objDBCD14);
    $arr = getEmailDetails($objDBCD14);
    $emailMsg = 'A recent '.$oType.' order attempt failed due to the Country you are trying to place an order is Not allowed.<br />
    Details of the attempt are below<br />
    Login Attempted At: '.setDtTmWRTYourCountry($objDBCD14).'<br />
    IP Address: '.$IP.'<br />
    Please click <a href="http://'.$arr[3].'/ipwhitelisting.php?tkn1='.urlsafe_b64encode($userId).'&tkn2='.urlsafe_b64encode($countryISO).'" target="_blank">here</a> to add this IP Country to your whitelist.<br />';
    sendGeneralEmail($email, $arr[0], $arr[1], $userName, 'Unsuccessful '.$oType.' Order Placement Attempt '.$arr[2], '', $emailMsg, $objDBCD14);
    return false;
    }
    }*/
	return true;
}


function digits_validation($digits, $digits_label)
{
	//Value must be digits.
	$digits = trim($digits);
	if (strlen($digits) >= 1) {
		if (preg_match("/^[0-9]+$/", $digits) === 0) {
			$error = 'Please enter a valid ' . $digits_label .
				'. It must be Digits only!<br />';
		} else {
			$error = null;
		}
	} else {
		$error = 'Please enter ' . $digits_label . '!<br />';
	}

	return $error;
}

function characters_validation($val, $label)
{
	$error = null;
	if (ctype_alpha($val) === false) {
		$error = 'Please enter a valid ' . $label .
			'. It must be characters only!<br />';
	}
	return $error;
}

function username_validation($username, $username_label)
{
	//User must be digits and letters.
	$username = trim($username);
	if (strlen($username) >= 1) {
		if (preg_match("/^[0-9a-zA-Z_@.-]{3,}$/", $username) === 0)
			$error = $username_label . ' must be digits and letters and at least 3 characters!<br />';
		else $error = null;
	} else $error = 'Please enter ' . $username_label . '!<br />';

	return $error;
}

function validateAlphabetsAndDigits($val, $label)
{
	$error = null;
	if (preg_match('/[A-Za-z]/', $val) && preg_match('/[0-9]/', $val)) {
		return null;
	} else {
		return 'Please enter a valid ' . $label .
			'. It must contans Digits and Aplhabets!<br />';
	}

}

function newFileOrderEmail($email, $name, $service, $myIMEI, $ip, $creditsDeducted,
						   $finalCredits, $orderDt, $serviceId, $altEmail, $hash, $bccEmails = '', $notes =
						   '')
{
	$arr = getEmailDetails();
	list($subject, $contents, $sendCopy) = getEmailContents(5);

	$placeholders = array(
		"#USER_NAME#",
		"#SERVICE_NAME#",
		"#IMEI_NO#",
		"#CREDITS_DEDUCTED#",
		"#FINAL_CREDITS#",
		"#ORDER_DATE#",
		"#IP#",
		"#COMPANY_NAME#",
		"#COMPANY_EMAIL_ADDRESS#",
		"#TRACK_ORDER_PAGE#",
		"#HASH#",
		"#NOTES#");
	$replacedData = array(
		$name,
		$service,
		$myIMEI,
		$creditsDeducted,
		$finalCredits,
		$orderDt,
		$ip,
		stripslashes($arr[2]),
		'<a href="mailto:' . $arr[4] . '">' . $arr[4] . '</a>',
		'<a href="http://' . $arr[3] . '/trackorder.php" target="_blank">here</a>',
		$hash,
		$notes == '' ? '-' : $notes);
	$emailMsg = str_replace($placeholders, $replacedData, $contents);
	sendMail($arr[4], $arr[0], $arr[1], $subject, $emailMsg, '', $email, $bccEmails);
	if ($altEmail != '')
		sendMail($altEmail, $arr[0], $arr[1], $subject, $emailMsg);
}

function ifPackIdAssignedToSupplier($packageId, $serviceType)
{
	$CI = &get_instance();
	$packId = 0;
	$price = 0;
	$supplierId = 0;
	$row = $CI->User_model->get_supplier_package($packageId, $serviceType);
	if (isset($row->PackageId) && $row->PackageId != '')
		$packId = $row->PackageId;
	if (isset($row->PurchasePrice) && $row->PurchasePrice != '')
		$price = $row->PurchasePrice;
	if (isset($row->SupplierId) && $row->SupplierId != '')
		$supplierId = $row->SupplierId;
	return array(
		$packId,
		$price,
		$supplierId);
}

function newIMEIOrderEmail($email, $customerName, $serviceName, $myIMEI, $ip, $creditsDeducted,
						   $finalCredits, $orderDt, $serviceId, $altEmail, $bccEmails = '', $notes = '')
{
	$arr = getEmailDetails();
	list($subject, $contents, $sendCopy) = getEmailContents(4);

	$placeholders = array(
		"#CUSTOMER_NAME#",
		"#SERVICE_NAME#",
		"#IMEI_NO#",
		"#CREDITS_DEDUCTED#",
		"#FINAL_CREDITS#",
		"#ORDER_DATE#",
		"#IP#",
		"#COMPANY_NAME#",
		"#COMPANY_EMAIL_ADDRESS#",
		"#TRACK_ORDER_PAGE#",
		"#NOTES#");
	$replacedData = array(
		$customerName,
		$serviceName,
		$myIMEI,
		$creditsDeducted,
		$finalCredits,
		$orderDt,
		$ip,
		stripslashes($arr[2]),
		'<a href="mailto:' . $arr[4] . '">' . $arr[4] . '</a>',
		'<a href="http://' . $arr[3] . '/trackorder.php" target="_blank">here</a>',
		$notes == '' ? '-' : $notes);
	$emailMsg = str_replace($placeholders, $replacedData, $contents);
	sendMail($email, $arr[0], $arr[1], $subject, $emailMsg, '', $sendCopy == '1' ? $arr[4] :
		'', $bccEmails);
	if ($altEmail != '')
		sendMail($altEmail, $arr[0], $arr[1], $subject, $emailMsg);
}

function submit_imei_order($myNetworkId, $IMEI_TYPE, $MY_CURRENCY_ID, $CALLED_FROM_ORDER_PAGE,
						   $historyData, $altEmail, $vals_cfields, $myCurrency, $cols_cfields)
{
	$CI = &get_instance();
	$CALLED_FROM_ORDER_PAGE = 1;

	$data = array();
	if (isset($_POST['imeiFType']) && $CI->session->userdata('GSM_FUS_UserId') && $CI->
		session->userdata('ORDERED_FROM') == '1') {
		if ($CI->input->server('HTTP_HOST') != 'localhost') {
			if (!checkCountryRestriction($CI->session->userdata('GSM_FUS_UserId'), $CI->
			session->userdata('UserEmail'), $CI->session->userdata('UserName'), 'IMEI')) {
				redirect(base_url('page/error'));
			}
		}
		$data['apiId'] = 0;
		$apiType = 0;
		$apiKey = '';
		$serverURL = '';
		$accountId = '';
		$apiAction = '';
		$responseUrl = '';
		$toolForUB = '';
		$packageTitle = '';
		$packCostPrice = 0;
		$msgFromServer = '';
		$ifCodeSentToServer = 0;
		$orderIdFromServer = '';
		$toolId = '';
		$DELAY_TIME = '1';
		$CRON_DELAY_TIME = '1';
		$NEW_ORDER_EMAILS = '';
		$IMEI_F_TYPE = $CI->input->post('imeiFType') ?: '0';
		$PRE_ORDER = $CI->input->post('hdPreOrder') ?: '0';
		$mepId = '';
		$mepValue = '';
		$modelId = '';
		$modelValue = '';
		$serialNo = '';
		$modelValueToShow = '';
		$apiName = '';
		$IMEI_SERIES = '';
		$mobileId = 0;

		$rsPackTitle = $CI->User_model->get_package_data($myNetworkId);
		if (isset($rsPackTitle->PackageTitle) && $rsPackTitle->PackageTitle != '') {
			$packageTitle = stripslashes($rsPackTitle->PackageTitle);
			$packCostPrice = $rsPackTitle->CostPrice == '' ? 0 : $rsPackTitle->CostPrice;
			$SERVICE_TYPE = $rsPackTitle->ServiceType; // 0 for DATABASE, 1 for RECALCULATE
			$DELAY_TIME = $rsPackTitle->ResponseDelayTm == 0 ? '1' : $rsPackTitle->
			ResponseDelayTm;
			$CRON_DELAY_TIME = $rsPackTitle->CronDelayTm == 0 ? '1' : $rsPackTitle->
			CronDelayTm;
			$NEW_ORDER_EMAILS = $rsPackTitle->NewOrderEmailIDs;
			$IMEI_SERIES = trim($rsPackTitle->IMEISeries);
		}
		$rsAPI = $CI->User_model->get_api_data($myNetworkId);
		if (isset($rsAPI->APIId) && $rsAPI->APIId != '') {
			$data['apiId'] = $rsAPI->APIId;
			$apiType = $rsAPI->APIType;
			$apiKey = $rsAPI->APIKey;
			$serverURL = $rsAPI->ServerURL;
			$accountId = $rsAPI->AccountId;
			$serviceId = $rsAPI->ServiceId;
			$apiAction = $rsAPI->APIAction;
			$responseUrl = $rsAPI->ResponseURL;
			$apiName = $rsAPI->APITitle;
			$extNwkId = $rsAPI->ExternalNetworkId != '' ? $rsAPI->ExternalNetworkId : 0;
		}

		if (isset($_POST['brandId']) && $_POST['brandId'] != '0') {
			$mobileId = $CI->input->post('brandId') ?: '0';
		}
		if ($CI->input->post('modelId') && $CI->input->post('modelId') != '0') {
			$modelId = $CI->input->post('modelId') ?: '0';
			$modelValueToShow = $CI->input->post('mdlVal') ?: '0';
		}
		$arrIMEIS = array();
		$duplicateIMEIs = array();
		$invalidIMEIs = array();
		$DUP_IMEIS = array();
		$ODLIMEIs = array();
		$ODL_COUNTER = 0;
		$totalIMEIs = 0;
		if ($IMEI_F_TYPE == '0') {
			if ($CI->input->post("rdIMEIType") == '1') {
				if ($CI->input->post('chkSm') && $CI->input->post('chkSm') == '1')
					$arrIMEIS[0] = $CI->input->post("txtIMEI") . $CI->input->post("txtIMEILastDigit");
				else
					$arrIMEIS[0] = $CI->input->post("txtIMEI");
			} else {
				$arrIMEIS = explode("\n", $CI->input->post("imei"));
				$r = 0;
				foreach ($arrIMEIS as $v) {
					$arrIMEIS[$r] = trim($v);
					$r++;
				}
				$duplicateIMEIs = array_repeat($arrIMEIS);
				$arrIMEIS = array_values(array_filter(array_unique($arrIMEIS))); // remvoving duplication then empty values and then re-numbering the array index
			}
			$totalIMEIs = sizeof($arrIMEIS);
		} else
			if ($IMEI_F_TYPE == '1') {
				if ($CI->input->post('chkSm') && $CI->input->post('chkSm') == '1') {
					$arrIMEIS[0] = $CI->input->post("txtIMEI") . $CI->input->post("txtIMEILastDigit");
				} else {
					$arrIMEIS[0] = $CI->input->post("txtIMEI");
				}
				$totalIMEIs = sizeof($arrIMEIS);
			} else
				if ($IMEI_F_TYPE == '2') {
					$arrIMEIS = explode("\n", $CI->input->post("imei"));
					$r = 0;
					foreach ($arrIMEIS as $v) {
						$arrIMEIS[$r] = trim($v);
						$r++;
					}
					$duplicateIMEIs = array_repeat($arrIMEIS);
					$arrIMEIS = array_values(array_filter(array_unique($arrIMEIS))); // remvoving duplication then empty values and then re-numbering the array index
					$totalIMEIs = sizeof($arrIMEIS);
				} else
					if ($IMEI_F_TYPE == '3') {
						$mnLen = $CI->input->post('customFldMinLen') ?: 0;
						$mxLen = $CI->input->post('customFldMaxLen') ?: 0;
						$custFldRes = $CI->input->post('custFldRstrctn') ?: 0;
						$arrCustomData = explode("\n", $CI->input->post("txtCustomFld"));
						$r = 0;
						$rr = 0;
						$arrTEMP = array();
						foreach ($arrCustomData as $v) {
							$IS_VALID = true;
							$arrValue = trim($v);
							$strCustError = '';
							if ($mnLen != 0 && $mnLen != '') {
								if (strlen($arrValue) < $mnLen) {
									$invalidIMEIs[$rr] = $arrValue;
									$rr++;
									$IS_VALID = false;
								}
							}
							if ($mxLen != 0) {
								if (strlen($arrValue) > $mxLen) {
									$invalidIMEIs[$rr] = $arrValue;
									$rr++;
									$IS_VALID = false;
								}
							}
							// CHECK IF THERE IS ANY RESTRICTION AT CUSTOM FIELD.
							$strCustError = checkCustomFieldRestriction($custFldRes, $objForm, $arrValue);
							if ($strCustError != '') {
								$invalidIMEIs[$rr] = $arrValue;
								$rr++;
								$IS_VALID = false;
							}
							if ($IS_VALID)
								$arrIMEIS[$r] = $arrValue;
							$r++;
						}
						$duplicateIMEIs = array_repeat($arrIMEIS);
						$arrIMEIS = array_values(array_filter(array_unique($arrIMEIS))); // remvoving duplication then empty values and then re-numbering the array index

						$totalIMEIs = sizeof($arrIMEIS);
					}

		$personalRecord = $CI->input->post('txtPersonalRecord');
		$phoneLockedOn = $CI->input->post('txtOprName') ?: '';
		$countryId = $CI->input->post('countryId') ?: 0;
		$notes = $CI->input->post('txtNotes');
		$comments = $CI->input->post('txtComments');
		$allowDupIMEIs = $CI->input->post('hdDupIMEI') ?: 0;
		$packagePrice = 0;
		$USER_ID = $_SESSION['GSM_FUS_UserId'];

		$packagePrice = getpackprice($myNetworkId, $MY_CURRENCY_ID, $IMEI_TYPE);
		$dupMsg = '';
		$odlMsg = '';
		$strInsertOrders = '';
		$strCreditHistory = '';
		$strEmailContents = '';
		$strInsert = '0';
		$suppPurchasePrice = '0';
		$supplierId = '0';

		list($suppPackId, $suppPurchasePrice, $supplierId) = ifPackIdAssignedToSupplier($myNetworkId,
			0);
		if ($suppPackId != '0')
			$strInsert = '1';

		$STR_IMEIS = '0';
		$currDtTm = setDtTmWRTYourCountry();

		for ($j = 0; $j < $totalIMEIs; $j++) {
			if (trim($arrIMEIS[$j]) != '')
				$STR_IMEIS .= ", '" . trim($arrIMEIS[$j]) . "'";
		}
		if ($allowDupIMEIs == '0') {
			$rsIfExists = $CI->User_model->get_codes($myNetworkId, $STR_IMEIS);

			foreach ($rsIfExists as $row) {
				$DUP_IMEIS[$row->IMEINo] = '1';
			}
		}
		$rwUNC = $CI->User_model->fetch_negative_user_credits();
		$ALLOWNGTVCRDTS = $rwUNC->AllowNegativeCredits;
		$ODL = $rwUNC->OverdraftLimit;

		$PLACED_ORDERS_COUNT = 0;

		for ($j = 0; $j < $totalIMEIs; $j++) {
			$UNLOCK_CODE = '';
			$strUpdate = '';
			$myIMEI = trim($arrIMEIS[$j]);
			$allowIMEIEntry = true;

			if (isset($DUP_IMEIS[$myIMEI]) && $DUP_IMEIS[$myIMEI] == '1') {
				$allowIMEIEntry = false;
				if ($dupMsg == '')
					$dupMsg = $myIMEI;
				else
					$dupMsg .= 'LB ' . $myIMEI;
			}

			if ($allowIMEIEntry && trim($myIMEI) != '') {
				//$PLACED_ORDERS_COUNT++;
				include APPPATH . 'scripts/submitimeiorder_qrs.php';
			}
		}
		if ($strInsertOrders != '' && $strCreditHistory != '') {
			$CI->User_model->add_codes_data($strInsertOrders, $cols_cfields);
			$CI->User_model->update_user_credits($encryptedCredits);
			$CI->User_model->add_credit_history_iemi($strCreditHistory);
		}
		if ($strEmailContents != '' && $CI->data['settings']->SendNewIMEIOrderEmail ==
			'1') {
			newIMEIOrderEmail($CI->session->userdata('UserEmail'), $CI->session->userdata('UserName'),
				$packageTitle, $strEmailContents, '', $packagePrice, $finalCr, $currDtTm, $myNetworkId,
				$altEmail, $NEW_ORDER_EMAILS, $notes);
		}

		if ($PLACED_ORDERS_COUNT > 0)
			$message = "ThankSPyouSPforSPplacingSP($PLACED_ORDERS_COUNT)SPIMEISPorders.";
		if ($dupMsg != '') {
			$ORDER_COMPLETED = 0;
			$message .= "LBLBRepeatedSPIMEI(s)SPORSPInvalidSPData:SPLB$dupMsg";
		}
		if ($dupMsg == '' && (sizeof($duplicateIMEIs) > 0 || sizeof($invalidIMEIs) > 0)) {
			$ORDER_COMPLETED = 0;
			$message .= "LBLBRepeatedSPIMEI(s)SPORSPInvalidSPData: ";
		}
		if ($odlMsg != '' && sizeof($ODLIMEIs) > 0) {
			$ORDER_COMPLETED = 0;
			$message .= "LBLBYourSPOverdraftSPLimitSPhasSPbeenSPfinishedSPforSPIMEI(s): ";
		}

		foreach ($duplicateIMEIs as $dupVal) {
			if ($dupVal != '') {
				$ORDER_COMPLETED = 0;
				$message .= "LB$dupVal";
			}
		}
		foreach ($invalidIMEIs as $imeiVal) {
			if ($imeiVal != '') {
				$ORDER_COMPLETED = 0;
				$message .= "LB$imeiVal";
			}
		}
		foreach ($ODLIMEIs as $oldIMEI) {
			if ($oldIMEI != '') {
				$ORDER_COMPLETED = 0;
				$message .= "LB$oldIMEI";
			}
		}

		$data['message'] = $message;

	} else {
		echo "here";
		//redirect(base_url('dashboard'));
	}

	return $data;

}


function getServiceFeatures($packId, $serviceType)
{
	$CI = &get_instance();
	$strFeatures = '';
	$rs = $CI->User_model->getServiceFeatures($packId, $serviceType);
	foreach ($rs as $row) {
		$strFeatures .= '<i class="' . $row->Icon . '" title="' . stripslashes($row->
			Feature) . '"></i>&nbsp;&nbsp;&nbsp;';
	}
	return $strFeatures;
}

function switchlanguage()
{
	$CI = &get_instance();
	if ($CI->input->post_get('ln'))
		$CI->session->set_userdata('LANGUAGE', $CI->input->post_get('ln'));
	if (!$CI->session->userdata('LANGUAGE'))
		$CI->session->set_userdata('LANGUAGE', 'en');
	switch ($CI->session->userdata('LANGUAGE')) {
		case 'en':
			echo '<script language="javascript" src="' . base_url() .
				'assets/js/languages/english.js"></script>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
			$strLbl = 'English';
			$strFlag = 'gb';
			break;
		case 'sp':
			echo '<script language="javascript" src="' . base_url() .
				'assets/js/languages/spanish.js"></script>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
			$strLbl = 'Spanish';
			$strFlag = 'es';
			break;
		case 'ru':
			echo '<script language="javascript" src="' . base_url() .
				'assets/js/languages/russian.js"></script>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
			$strLbl = 'Russian';
			$strFlag = 'ru';
			break;
		case 'fr':
			echo '<script language="javascript" src="' . base_url() .
				'assets/js/languages/french.js"></script>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
			$strLbl = 'French';
			$strFlag = 'fr';
			break;
		case 'pl':
			echo '<script language="javascript" src="' . base_url() .
				'assets/js/languages/polish.js"></script>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
			$strLbl = 'Polish';
			$strFlag = 'pl';
			break;
		case 'dt':
			echo '<script language="javascript" src="' . base_url() .
				'assets/js/languages/dutch.js"></script>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
			break;
		case 'pt':
			echo '<script language="javascript" src="' . base_url() .
				'assets/js/languages/portuguese.js"></script>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
			$strLbl = 'Portuguese';
			$strFlag = 'pt';
			break;
		case 'sw':
			echo '<script language="javascript" src="' . base_url() .
				'assets/js/languages/swedish.js"></script>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />';
			break;
		case 'de':
			echo '<script language="javascript" src="' . base_url() .
				'assets/js/languages/german.js"></script>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
			$strLbl = 'German';
			$strFlag = 'de';
			break;
		case 'hb':
			echo '<script language="javascript" src="' . base_url() .
				'assets/js/languages/german.js"></script>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
			break;
		case 'alb':
			echo '<script language="javascript" src="' . base_url() .
				'assets/js/languages/albania.js"></script>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />';
			break;
		case 'prs':
			echo '<script language="javascript" src="' . base_url() .
				'assets/js/languages/persian.js"></script>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
			break;
		case 'th':
			echo '<script language="javascript" src="' . base_url() .
				'assets/js/languages/english.js"></script>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
			break;
		case 'hu':
			echo '<script language="javascript" src="' . base_url() .
				'assets/js/languages/english.js"></script>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
			$strLbl = 'Hungarian';
			$strFlag = 'hu';
			break;
		case 'cns':
			echo '<script language="javascript" src="' . base_url() .
				'assets/js/languages/chinese-s.js"></script>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
			$strLbl = 'Chinese (Simplified)';
			$strFlag = 'cn';
			break;
		case 'ro':
			echo '<script language="javascript" src="' . base_url() .
				'assets/js/languages/romanian.js"></script>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
			$strLbl = 'Romanian';
			$strFlag = 'ro';
			break;
		case 'vn':
			echo '<script language="javascript" src="' . base_url() .
				'assets/js/languages/vietnamese.js"></script>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
			$strLbl = 'Vietnamese';
			$strFlag = 'vn';
			break;
		case 'it':
			echo '<script language="javascript" src="' . base_url() .
				'assets/js/languages/italian.js"></script>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
			$strLbl = 'Italian';
			$strFlag = 'it';
			break;
	}
}

function orderVerificationEmail($orderType, $orderNo, $service, $code, $credits, $ip, $orderDt, $imei = '', $vData = '')
{
	$imeiRow = '';
	$vLnkRow = '';
	$arr = getEmailDetails();
	list($subject, $contents, $sendCopy) = getEmailContents('35');
	if ($imei != '') {
		$imeiRow = '<tr><td width="150" valign="top" align="left" style="font: bold 13px/20px Arial,Helvetica,sans-serif">
                    <p style="padding: 0; margin: 0 0 15px 0; color: #8c8c8c; display: inline-block; width: 85%">IMEI #:</p>
				</td>
				<td valign="top" align="left">
					<p style="padding: 0; margin: 0 0 15px 0; color: #1a1a1a; font: normal 13px Arial,Helvetica,sans-serif; line-height: 20px">' . $imei . '</p>
				</td>
			</tr>';
	}
	if ($vData != '') {
		$vLnkRow = '<tr>
				<td width="150" valign="top" align="left" style="font: bold 13px/20px Arial,Helvetica,sans-serif">
                    <p style="padding: 0; margin: 0 0 15px 0; color: #8c8c8c; display: inline-block; width: 85%">GSX Data/Video Link:</p>
				</td>
				<td valign="top" align="left">
					<p style="padding: 0; margin: 0 0 15px 0; color: #1a1a1a; font: normal 13px Arial,Helvetica,sans-serif; line-height: 20px">' . $vData . '</p>
				</td>
			</tr>';
	}
	if ($orderType == '0') {
		$lnk = '<a href="http://' . $arr[3] . '/page/' . 'verifyorders" style="color: #08a2f8" target="_blank">here</a>';
		$subject = 'IMEI Order for verification!';
		$alertMsg = 'New Imei Order came for Verification';
	} else if ($orderType == '1') {
		$lnk = '<a href="http://' . $arr[3] . '/page/' . 'verifyorders" style="color: #08a2f8" target="_blank">here</a>';
		$subject = 'File Order for verification!';
		$alertMsg = 'New File Order came for Verification';
	} else if ($orderType == '2') {
		$lnk = '<a href="http://' . $arr[3] . '/page/' . 'verifyorders" style="color: #08a2f8" target="_blank">here</a>';
		$subject = 'Server Order for verification!';
		$alertMsg = 'New Server Order came for Verification';
	}
	$placeholders = array("#ORDER_NO#", "#SERVICE#", "#IMEI#", "#CODE#", "#CREDITS#", "#IP#", "#ORDER_DATE#", "#VIDEO_LINK#", "#ORDER_ADMIN_LINK#", "#MESSAGE#");
	$replacedData = array($orderNo, $service, $imeiRow, $code, $credits, $ip, $orderDt, $vLnkRow, $lnk, $alertMsg);
	$subject = str_replace($placeholders, $replacedData, $subject);
	$emailMsg = str_replace($placeholders, $replacedData, $contents);
	sendMail($arr[4], $arr[0], $arr[1], $subject, $emailMsg);
}

function updateorderstatus($data, $codeStatusId, $codeId, $code)
{
	$CI = &get_instance();

	$userId = $data[1];
	$codeCr = $data[2];
	$myIMEI = $data[3];
	$pId = $data[5];
	$serviceTitle = $ARR_SRVC_NAME[$pId];
	$strDeduct = '';
	$currDtTm = setDtTmWRTYourCountry();
	if ($codeStatusId == '3') {
		send_push_notification('Your IMEI order has been cancelled!', $userId);
		if (imeiOrderRefunded($codeId) == '0') {
			$dec_points = refundIMEICredits($userId, $codeId, $myIMEI, $serviceTitle, $currDtTm, $pId, $codeCr, '1');
			$strDeduct = ", Refunded = '1', CheckDuplication = 0";
		}
		$message = 'Order(s) have been rejected successfully!';
	} else {
		send_push_notification('Your IMEI order has been completed!', $userId);
		if (imeiOrderRefunded($codeId) == '1') {
			rebateIMEICredits($userId, $codeId, $myIMEI, $serviceTitle, $currDtTm, $pId, $codeCr, '1');
		}
		$strDeduct = ", Refunded = '0'";
		$message = 'Order(s) have been completed successfully!';
	}

	$CI->db->query("UPDATE tbl_gf_codes SET LastUpdatedBy = " . $this->session->userdata('AdminUserName') . ", CodeStatusId = '$codeStatusId', Code = '$code', ReplyDtTm = '$currDtTm' $strDeduct WHERE CodeId = '$codeId'");
	return $message;
}


function updatefileorderstatus($data, $codeId, $codeStatusId, $code)
{

	$CI = &get_instance();
	$message = '';
	$userId = $data[1];
	$codeCr = $data[2];
	$myIMEI = $data[3];
	$pId = $data[5];
	$strDeduct = '';
	$currDtTm = setDtTmWRTYourCountry();
	if ($codeStatusId == '3') {
		if (fileOrderRefunded($codeId) == '0') {
			$dec_points = refundFileCredits($userId, $codeId, $myIMEI, $serviceTitle, $currDtTm, $pId, $codeCr, '1');
			$strDeduct = ", Refunded = '1', CheckDuplication = 0";
		}
	} else {
		if (fileOrderRefunded($codeId) == '1') {
			rebateFileCredits($userId, $codeId, $myIMEI, $serviceTitle, $currDtTm, $pId, $codeCr, '1');
		}
		$strDeduct = ", Refunded = '0'";
	}

	$CI->db->query("UPDATE tbl_gf_codes_slbf SET LastUpdatedBy = '" . $_SESSION['AdminUserName'] . "', CodeStatusId = '$codeStatusId', Code = '$code', ReplyDtTm = '$currDtTm' $strDeduct WHERE CodeId = '$codeId'");

	if ($codeStatusId == '2' || $codeStatusId == '3') {
		$row = $CI->db->query("SELECT UserName AS CustomerName, Code, UserEmail, AlternateEmail, CodeStatus, RequestedAt, A.UserId, 	
                    PackageTitle, A.Comments, B.SendSMS AS SMS_User, D.SendSMS AS SMS_Pack, B.Phone FROM tbl_gf_codes_slbf A, tbl_gf_users B, tbl_gf_code_status C,
                    tbl_gf_packages D WHERE A.UserId = B.UserId AND A.CodeStatusId = C.CodeStatusId AND A.PackageId = D.PackageId AND CodeId = '$codeId'")->row();
		if (isset($row->UserEmail) && $row->UserEmail != '') {
			if ($codeStatusId == '2') {
				send_push_notification('Your File order has been completed!', $userId);
				successfulFileOrderEmail($row->UserEmail, stripslashes($row->CustomerName), stripslashes($row->PackageTitle), $myIMEI, stripslashes($row->Code),
					$row->RequestedAt, $pId, $row->AlternateEmail, $codeCr, $row->UserId, $row->Comments, $codeId);
				if ($row->SMS_User == '1' && $row->SMS_Pack == '1' && $row->Phone != '' && $PHONE_ADMIN != '') {
					checkAndSendSMS($row->Phone, $PHONE_ADMIN, '8', stripslashes($row->CustomerName), stripslashes($row->PackageTitle), $myIMEI,
						stripslashes($row->Code), $row->RequestedAt, $row->Comments);
				}
				$message = 'Order(s) have been completed successfully!';
			} else if ($codeStatusId == '3') {
				send_push_notification('Your File order has been cancelled!', $userId);
				rejectedFileOrderEmail($row->UserEmail, stripslashes($row->CustomerName), stripslashes($row->PackageTitle), $myIMEI, stripslashes($row->Code),
					$row->RequestedAt, $pId, $row->AlternateEmail, $codeCr, $dec_points, $row->Comments, $codeId);
				if ($row->SMS_User == '1' && $row->SMS_Pack == '1' && $row->Phone != '' && $PHONE_ADMIN != '') {
					checkAndSendSMS($row->Phone, $PHONE_ADMIN, '14', stripslashes($row->CustomerName), stripslashes($row->PackageTitle), $myIMEI,
						stripslashes($row->Code), $row->RequestedAt, $row->Comments);
				}
				$message = 'Order(s) have been rejected successfully!';
			}
		}
	}


	return $message;


}

function updateserverorderstatus($data, $codeId, $codeStatusId, $code, $serviceTitle = '')
{

	$CI = &get_instance();

	$userId = $data[1];
	$codeCr = $data[2];
	$pId = $data[4];
	$strDeduct = '';
	$currDtTm = setDtTmWRTYourCountry();
	if ($codeStatusId == '3') {
		if (serverOrderRefunded($codeId) == '0') {
			$dec_points = refundServerCredits($userId, $codeId, $serviceTitle, $currDtTm, $codeCr, '1');
			$strDeduct = ", Refunded = '1'";
		}
	} else {
		if (serverOrderRefunded($codeId) == '1') {
			rebateServerCredits($userId, $codeId, $serviceTitle, $currDtTm, $codeCr, '1');
		}
		$strDeduct = ", Refunded = '0'";
	}

	$CI->db->query("UPDATE tbl_gf_log_requests SET LastUpdatedBy = '" . $_SESSION['AdminUserName'] . "', StatusId = '$codeStatusId', Code = '$code', ReplyDtTm = '$currDtTm' $strDeduct WHERE LogRequestId = '$codeId'");

	if ($codeStatusId == '2' || $codeStatusId == '3') {
		$row = $CI->db->query("SELECT UserName AS CustomerName, LogPackageTitle, AlternateEmail, UserEmail, CodeStatus, A.UserId, OrderData, 
                RequestedAt, A.Comments, B.SendSMS AS SMS_User, D.SendSMS AS SMS_Pack, B.Phone FROM tbl_gf_log_requests A, tbl_gf_users B, tbl_gf_code_status C,
                tbl_gf_log_packages D WHERE A.UserId = B.UserId AND A.StatusId = C.CodeStatusId AND A.LogPackageId = D.LogPackageId AND LogRequestId = '$codeId'")->row();
		if (isset($row->UserEmail) && $row->UserEmail != '') {
			if ($codeStatusId == '2') {
				send_push_notification('Your Server order has been completed!', $row->UserId);
				successfulServerOrderEmail($row->UserEmail, stripslashes($row->CustomerName), stripslashes($row->LogPackageTitle), $code, $row->RequestedAt, $pId,
					$row->AlternateEmail, $codeCr, $row->UserId, $row->OrderData, $row->Comments, $codeId);
				if ($row->SMS_User == '1' && $row->SMS_Pack == '1' && $row->Phone != '' && $PHONE_ADMIN != '') {
					checkAndSendSMS($row->Phone, $PHONE_ADMIN, '9', stripslashes($row->CustomerName), stripslashes($row->LogPackageTitle), '', $code,
						$row->RequestedAt, $row->Comments);
				}
				$message = 'Order(s) have been completed successfully!';
			} else if ($codeStatusId == '3') {
				send_push_notification('Your Server order has been cancelled!', $row->UserId);
				rejectedServerOrderEmail($row->UserEmail, stripslashes($row->CustomerName), stripslashes($row->LogPackageTitle), $code, $row->RequestedAt, $pId,
					$row->AlternateEmail, $codeCr, $dec_points, $row->OrderData, $row->Comments, $codeId);
				if ($row->SMS_User == '1' && $row->SMS_Pack == '1' && $row->Phone != '' && $PHONE_ADMIN != '') {
					checkAndSendSMS($row->Phone, $PHONE_ADMIN, '15', stripslashes($row->CustomerName), stripslashes($row->LogPackageTitle), '', $code,
						$row->RequestedAt, $row->Comments);
				}
				$message = 'Order(s) have been rejected successfully!';
			}
		}
	}


	return $message;

}

function checkAndSendSMS($userPhone, $adminPhone, $templateId, $name, $service, $imei, $code, $orderDt, $notes = '')
{

	$CI = &get_instance();

	$CI->db->select('SMSGatewayId, Username, Password');
	$CI->db->from('tbl_gf_sms_gateways');
	$CI->db->where('DisableSMSGateway', 0);

	$result = $CI->db->get();
	$row = $result->row();
	if (isset($row->SMSGatewayId) && $row->SMSGatewayId != '' && $row->Username != '') {
		$userPhone = str_replace(' ', '', $userPhone);
		$userPhone = str_replace('-', '', $userPhone);

		$adminPhone = str_replace(' ', '', $adminPhone);
		$adminPhone = str_replace('-', '', $adminPhone);

		$contents = getSMSContents($templateId);
		if ($contents != '') {
			$placeholders = array("#CUSTOMER_NAME#", "#SERVICE_NAME#", "#IMEI_NO#", "#DETAILS#", "#ORDER_DATE#", '#NOTES#');
			$replacedData = array($name, $service, $imei, $code, $orderDt, $notes);
			$smsMsg = str_replace($placeholders, $replacedData, $contents);
			sendSMS($row->SMSGatewayId, $row->Username, $row->Password, $smsMsg, $userPhone, $adminPhone);
		}
	}
}


function successfulServerOrderEmail($email, $name, $service, $code, $orderDt, $serviceId, $altEmail, $credits, $userId, $details, $notes = '', $orderNo = '')
{
	if (sendOrderEmail('SendSuccessSrvrOrderEmail')) {
		$arr = getEmailDetails();
		list($subject, $contents, $sendCopy) = getEmailContents(9);
		$creditsLeft = getUserCredits($userId);
		$placeholders = array("#CUSTOMER_NAME#", "#SERVICE_NAME#", "#DETAILS#", "#ORDER_DATE#", "#COMPANY_NAME#", "#COMPANY_EMAIL_ADDRESS#", '#NOTES#', '#ORDER_NO#', "#CREDITS#", "#CREDITS_LEFT#", "#ORDER_DETAILS#");
		$replacedData = array($name, $service, $code, $orderDt, stripslashes($arr[2]), '<a href="mailto:' . $arr[4] . '">' . $arr[4] . '</a>', $notes == '' ? '-' : $notes, $orderNo, $credits, $creditsLeft, $details);
		$emailMsg = str_replace($placeholders, $replacedData, $contents);
		sendMail($email, $arr[0], $arr[1], $subject, $emailMsg, '', $sendCopy == '1' ? $arr[4] : '');
		if ($altEmail != '')
			sendMail($altEmail, $arr[0], $arr[1], $subject, $emailMsg);
	}
}


function rebateServerCredits($userId, $orderId, $packTitle, $currDtTm, $amount, $byAdmin)
{
	$CI = &get_instance();
	$key = crypt_key($userId);
	if ($amount > 0) {
		$points = 0;
		$CI->db->select('Credits');
		$CI->db->from('tbl_gf_users');
		$CI->db->where('UserId', $userId);
		$result = $CI->db->get();
		$row = $result->row();
		if (isset($row->Credits) && $row->Credits != '')
			$points = $row->Credits;

		$dec_points = decrypt($points, $key);
		$dec_points -= $amount;
		$enc_points = encrypt($dec_points, $key);

		$CI->db->set('Credits', $enc_points);
		$CI->db->where('UserId', $userId);
		$CI->db->update('tbl_gf_users');


		$orderCustomData = getOrderCustomData($orderId, 2);

		$CI->db->set('ByAdmin', $byAdmin);
		$CI->db->set('UserId', $userId);
		$CI->db->set('Credits', $amountToRefund);
		$CI->db->set('IMEINo', $orderCustomData);
		$CI->db->set('Description', 'Credits Rebated, Server Order for' . $packTitle);
		$CI->db->set('HistoryDtTm', $currDtTm);
		$CI->db->set('CreditsLeft', $enc_points);
		$CI->db->set('LogRequestId', $orderId);
		$CI->db->set('OType', 2);
		$CI->db->insert('tbl_gf_credit_history');
	}
}


function showAcceptedServerOrders($ids, $download = 0)
{
	$CI = &get_instance();

	$strIMEIs = '';
	$strCustomCols = '';
	$colIndex = 1;
	$lineBreaker = $download == 0 ? "<br />" : "\r\n";
	$startBoldTag = $download == 0 ? "<b>" : "";
	$endBoldTag = $download == 0 ? "</b>" : "";
	$CUST_COL_LBL = array();

	$CI->db->select('FieldLabel, FieldColName');
	$CI->db->from('tbl_gf_custom_fields');
	$CI->db->where('ServiceType', 2);
	$CI->db->order_by('FieldLabel');

	$result = $CI->db->get();
	$rsFields = $result->result();
	$currDtTm = setDtTmWRTYourCountry();

	foreach ($rsFields as $row) {
		$strCustomCols .= ', ' . $row->FieldColName . ' AS Col' . $colIndex;
		$CUST_COL_LBL['Col' . $colIndex] = $row->FieldLabel;
		$colIndex++;
	}

	$CI->db->select("A.LogPackageId AS PackageId, LogPackageTitle $strCustomCols");
	$CI->db->from('tbl_gf_log_requests A, tbl_gf_log_packages B');
	$CI->db->where('A.LogPackageId = B.LogPackageId');
	$CI->db->where_in('LogRequestId', $ids);
	$CI->db->order_by('A.LogPackageId');

	$result = $CI->db->get();
	$rsIPData = $result->result();

	foreach ($rsIPData as $row) {
		$serviceName = stripslashes($row->LogPackageTitle);
		if ($prevPackId > 0) {
			if ($row->PackageId != $prevPackId) {
				if ($download == '0')
					$strIMEIs .= "$lineBreaker================================================================================================================$lineBreaker";
				$strIMEIs .= "			$startBoldTag Service: $serviceName - $currDtTm $endBoldTag $lineBreaker
								================================================================================================================$lineBreaker";
			}
		} else {
			if ($download == '0')
				$strIMEIs .= "================================================================================================================$lineBreaker";
			$strIMEIs .= "$startBoldTag Service: $serviceName - $currDtTm $endBoldTag $lineBreaker================================================================================================================$lineBreaker";
		}
		for ($i = 1; $i < $colIndex; $i++) {
			$colName = 'Col' . $i;
			if (isset($row->$colName) && trim($row->$colName) != '') {
				if (isset($CUST_COL_LBL[$colName]) && $CUST_COL_LBL[$colName] != '')
					$strIMEIs .= ' ' . $CUST_COL_LBL[$colName] . ':';
				$strIMEIs .= ' ' . $row->$colName;
			}
		}
		$strIMEIs .= "$lineBreaker";
		$prevPackId = $row->PackageId;
	}
	return $strIMEIs;
}

function successfulFileOrderEmail($email, $name, $service, $imei, $code, $orderDt, $serviceId, $altEmail, $credits, $userId, $notes = '', $orderNo = '')
{
	if (sendOrderEmail('SendSuccessFileOrderEmail')) {
		$arr = getEmailDetails();
		list($subject, $contents, $sendCopy) = getEmailContents(8);

		$creditsLeft = getUserCredits($userId);
		$placeholders = array("#CUSTOMER_NAME#", "#SERVICE_NAME#", "#IMEI_NO#", "#DETAILS#", "#ORDER_DATE#", "#COMPANY_NAME#", "#COMPANY_EMAIL_ADDRESS#", '#NOTES#', '#ORDER_NO#', "#CREDITS#", "#CREDITS_LEFT#");
		$replacedData = array($name, $service, $imei, $code, $orderDt, stripslashes($arr[2]), '<a href="mailto:' . $arr[4] . '">' . $arr[4] . '</a>', $notes == '' ? '-' : $notes, $orderNo, $credits, $creditsLeft);
		$emailMsg = str_replace($placeholders, $replacedData, $contents);
		sendMail($email, $arr[0], $arr[1], $subject, $emailMsg, '', $sendCopy == '1' ? $arr[4] : '');
		if ($altEmail != '')
			sendMail($altEmail, $arr[0], $arr[1], $subject, $emailMsg);
	}
}

function rebateIMEICredits($userId, $orderId, $imei, $packTitle, $currDtTm, $packageId, $amount, $byAdmin)
{
	$CI = &get_instance();
	$keys = crypt_key($userId);
	if ($amount > 0) {
		$points = 0;
		$row = $CI->db->query("SELECT Credits FROM tbl_gf_users WHERE UserId = '$userId'");
		if (isset($row->Credits) && $row->Credits != '')
			$points = $row->Credits;

		$dec_points = decrypt($points, $keys);
		$dec_points -= $amount;
		$enc_points = encrypt($dec_points, $keys);

		$CI->db->query("UPDATE tbl_gf_users SET Credits = '$enc_points' WHERE UserId = '$userId'");

		$orderCustomData = getOrderCustomData($orderId, 0);

		$CI->db->insert("INSERT INTO tbl_gf_credit_history SET ByAdmin = '$byAdmin', UserId = '$userId', Credits = '$amount', 
				Description = 'Credits Rebated - IMEI Order for $packTitle', IMEINo = '" . addslashes($orderCustomData) . "', HistoryDtTm = '$currDtTm', 
				CreditsLeft = '$enc_points', PackageId = '$packageId', LogRequestId = '$orderId', OType = '0'");
	}
}


function fileOrderRefunded($orderId)
{
	$CI = &get_instance();
	$CI->db->select('Refunded');
	$CI->db->from('tbl_gf_codes_slbf');
	$CI->db->where('CodeId', $orderId);

	$result = $CI->db->get();
	$row = $result->row();
	return $row->Refunded;
}

function serverOrderRefunded($orderId)
{
	$CI = &get_instance();
	$CI->db->select('Refunded');
	$CI->db->from('tbl_gf_log_requests');
	$CI->db->where('LogRequestId', $orderId);

	$result = $CI->db->get();
	$row = $result->row();
	return $row->Refunded;

}

function refundFileCredits($userId, $orderId, $imei, $packTitle, $currDtTm, $packageId, $amountToRefund, $byAdmin, $comments = '')
{
	$CI = &get_instance();
	$key = crypt_key($userId);
	if ($amountToRefund > 0) {
		$points = 0;
		$CI->db->select('Credits');
		$CI->db->from('tbl_gf_users');
		$CI->db->where('UserId', $userId);
		$result = $CI->db->get();
		$row = $result->row();

		if (isset($row->Credits) && $row->Credits != '')
			$points = $row->Credits;

		$dec_points = decrypt($points, $key);
		$dec_points += $amountToRefund;
		$enc_points = $encrypt($dec_points, $key);

		$CI->db->set('Credits', $enc_points);
		$CI->db->where('UserId', $userId);
		$CI->db->update('tbl_gf_users');

		$desc = 'Credits Refunded, File Order Rejected for ' . addslashes($packTitle);
		$CI->db->query("INSERT INTO tbl_gf_credit_history SET ByAdmin = '$byAdmin', UserId = '$userId', Credits = '$amountToRefund', 
				Description = '$desc', IMEINo = '$imei', HistoryDtTm = '$currDtTm', LogRequestId = '$orderId', OType = '1', 
				CreditsLeft = '$enc_points', PackageId = '$packageId', Comments = '$comments'");
	}
	return $dec_points;
}

function refundServerCredits($userId, $orderId, $packTitle, $currDtTm, $amountToRefund, $byAdmin, $comments = '')
{
	$CI = &get_instance();
	$key = crypt_key($userId);

	if ($amountToRefund > 0) {
		$points = 0;
		$CI->db->select('Credits');
		$CI->db->from('tbl_gf_users');
		$CI->db->where('UserId', $userId);
		$result = $CI->db->get();
		$row = $result->row();

		if (isset($row->Credits) && $row->Credits != '')
			$points = $row->Credits;

		$dec_points = decrypt($points, $key);
		$dec_points += $amountToRefund;
		$enc_points = encrypt($dec_points, $key);


		$CI->db->set('Credits', $enc_points);
		$CI->db->where('UserId', $userId);
		$CI->db->update('tbl_gf_users');


		$orderCustomData = getOrderCustomData($orderId, 2);
		$desc = 'Credits Refunded, Server Order Rejected for ' . addslashes($packTitle);
		$CI->db->query("INSERT INTO tbl_gf_credit_history SET ByAdmin = '$byAdmin', UserId = '$userId', Credits = '$amountToRefund', IMEINo = '$orderCustomData', 
				Description = '$desc', HistoryDtTm = '$currDtTm', CreditsLeft = '$enc_points', OType = '2', 
				LogRequestId = '$orderId', Comments = '$comments'");
	}
	return $dec_points;
}

function rejectedFileOrderEmail($email, $name, $service, $imei, $code, $orderDt, $serviceId, $altEmail, $creditsReturned, $creditsLeft, $notes = '', $orderNo = '')
{
	$CI = &get_instance();
	if (sendOrderEmail('SendFailureFileOrderEmail')) {
		$arr = getEmailDetails();
		$CI->db->select('CancelTplSubject, CancelTplBody, SendRejectCopyToAdmin');
		$CI->db->from('tbl_gf_packages');
		$CI->db->where('PackageId', $serviceId);
		$CI->db->where('CancelledTplType', 1);

		$result = $CI->db->get();
		$row = $result->row();

		if (isset($row->CancelTplSubject) && $row->CancelTplSubject != '') {
			$subject = stripslashes($row->CancelTplSubject);
			$contents = stripslashes($row->CancelTplBody);
			$sendCopy = $row->SendRejectCopyToAdmin;
		} else {
			list($subject, $contents, $sendCopy) = getEmailContents(14);
		}
		$placeholders = array("#CUSTOMER_NAME#", "#SERVICE_NAME#", "#IMEI_NO#", "#DETAILS#", "#ORDER_DATE#", "#COMPANY_NAME#", "#COMPANY_EMAIL_ADDRESS#", "#CREDITS_RETURNED#", "#CREDITS_LEFT#", '#NOTES#', '#ORDER_NO#');
		$replacedData = array($name, $service, $imei, $code, $orderDt, stripslashes($arr[2]), '<a href="mailto:' . $arr[4] . '">' . $arr[4] . '</a>', $creditsReturned, $creditsLeft, $notes == '' ? '-' : $notes, $orderNo);
		$emailMsg = str_replace($placeholders, $replacedData, $contents);
		sendMail($email, $arr[0], $arr[1], $subject, $emailMsg, '', $sendCopy == '1' ? $arr[4] : '');
		if ($altEmail != '')
			sendMail($altEmail, $arr[0], $arr[1], $subject, $emailMsg);
	}
}

function imeiOrderRefunded($orderId)
{
	$CI = &get_instance();
	$row = $CI->General_model->get_imeiOrderRefunded($orderId);
	return $row->Refunded;
}

function getOrderCustomData($orderId, $sc)
{
	switch ($sc) {
		case '0': // IMEI Orders
			$tblOrders = 'tbl_gf_codes';
			$orderCol = 'CodeId';
			break;
		case '2': // Server Orders
			$tblOrders = 'tbl_gf_log_requests';
			$orderCol = 'LogRequestId';
			break;
	}
	$rw = $this->General_model->get_orders_custom($orderId);
	return $rw->OrderData;
}

function refundIMEICredits($userId, $orderId, $imei, $packTitle, $currDtTm, $packageId,
						   $amountToRefund, $byAdmin, $comments = '')
{
	$CI = &get_instance();
	$keys = crypt_key($userId);
	if ($amountToRefund > 0) {
		$points = 0;
		$row = $CI->General_model->get_user_credits($userId);
		if (isset($row->Credits) && $row->Credits != '')
			$points = $row->Credits;

		$dec_points = decrypt($points, $keys);
		$dec_points += $amountToRefund;
		$enc_points = $crypt->encrypt($dec_points);
		$CI->General_model->update_user_credits($enc_points, $userId);
		$orderCustomData = getOrderCustomData($orderId, 0);
		$desc = 'Credits Refunded, IMEI Order Rejected for ' . addslashes($packTitle);
		$CI->General_model->insert_creditHistory($desc, $orderCustomData, $currDtTm, $enc_points,
			$packageId, $comments, $orderId);

	}
	return $dec_points;
}

function sendOrderEmail($colName)
{
	$CI = &get_instance();
	$row = $CI->General_model->get_failure_email($colName);
	return $row->$colName;
}

function rejectedIMEIOrderEmail($email, $name, $service, $imei, $code, $orderDt, $serviceId, $altEmail, $creditsReturned, $creditsLeft, $notes = '', $orderNo = '')
{
	if (sendOrderEmail('SendFailureIMEIOrderEmail')) {
		$arr = getEmailDetails();
		$row = get_cancel_subject($serviceId);
		if (isset($row->CancelTplSubject) && $row->CancelTplSubject != '') {
			$subject = stripslashes($row->CancelTplSubject);
			$contents = stripslashes($row->CancelTplBody);
			$sendCopy = $row->SendRejectCopyToAdmin;
		} else {
			list($subject, $contents, $sendCopy) = getEmailContents(13);
		}

		$placeholders = array(
			"#CUSTOMER_NAME#",
			"#SERVICE_NAME#",
			"#IMEI_NO#",
			"#DETAILS#",
			"#ORDER_DATE#",
			"#COMPANY_NAME#",
			"#COMPANY_EMAIL_ADDRESS#",
			"#CREDITS_RETURNED#",
			"#CREDITS_LEFT#",
			'#NOTES#',
			'#ORDER_NO#');
		$replacedData = array(
			$name,
			$service,
			$imei,
			$code,
			$orderDt,
			stripslashes($arr[2]),
			'<a href="mailto:' . $arr[4] . '">' . $arr[4] . '</a>',
			$creditsReturned,
			$creditsLeft,
			$notes == '' ? '-' : $notes,
			$orderNo);
		$emailMsg = str_replace($placeholders, $replacedData, $contents);
		sendMail($email, $arr[0], $arr[1], $subject, $emailMsg, '', $sendCopy == '1' ? $arr[4] :
			'');
		if ($altEmail != '')
			sendMail($altEmail, $arr[0], $arr[1], $subject, $emailMsg);
	}
}

function fixTextInTD($text, $stIndex, $mainIndex)
{
	$strTxt = '';
	$arrPckTitle = explode(' ', stripslashes($text));
	foreach ($arrPckTitle as $pKey => $pVal) {
		if ($pKey > $stIndex) {
			if ($pKey % $mainIndex == 0) {
				$strTxt .= '<br />';
			} else
				$strTxt .= ' ';
		} else
			if ($pKey != 0)
				$strTxt .= ' ';
		if ($strTxt == '')
			$strTxt = $pVal;
		else
			$strTxt = $strTxt . $pVal;
	}
	return $strTxt;
}

function checkLastPayerEmail($paymentId, $userId, $payerEmail, $arr, $uName)
{
	$rs = $this->User_model->get_payer_email($userId, $paymentId);
	if (isset($rs->PayerEmail) && $rs->PayerEmail != $payerEmail) {
		$lnk = '<p style="font: normal 14px Arial,Helvetica,sans-serif; color: #3c763d">Please click <a href="' .
			getBaseUrl() . 'page/payment/' . $paymentId .
			'" style="color: #08a2f8" target="_blank">here</a> to view the invoice.</p>';
		$emailMsg = "PayPal Payer Email has been changed for invoice # $paymentId from the last paid invoice.<br /><br />
				Username: $uName<br /><br />
				Current Payer Email: $payerEmail<br /><br />
				Last Payer Email: " . $rs->PayerEmail . "<br /><br />
				$lnk<br /><br />";
		sendGeneralEmail($arr[4], $arr[0], $arr[1], 'Admin',
			'PayPay Payer From Email Changed', $emailMsg, '');
	}
}


function get_payment_method()
{
	$CI = &get_instance();
	$data = $CI->User_model->get_payment_method();
	return $data;
}


function get_payment_status()
{
	$CI = &get_instance();
	$data = $CI->User_model->get_payment_status();
	return $data;
}

function get_payment_methods_and_detail()
{
	$CI = &get_instance();
	$data = $CI->User_model->get_payment_methods_and_detail();
	return $data;
}

function get_all_countries()
{
	$CI = &get_instance();
	$data = $CI->User_model->get_all_countries();
	return $data;
}

function get_rsPackages($strPackIds)
{
	$CI = &get_instance();

	$CI->db->select('A.CategoryId, Category, A.PackageId, PackageTitle');
	$CI->db->from('tbl_gf_package_category Cat, tbl_gf_packages A');
	$CI->db->where('Cat.CategoryId = A.CategoryId');
	$CI->db->where('DisablePackage', 0);
	$CI->db->where('DisableCategory', 0);
	$CI->db->where('sl3lbf', 1);
	$CI->db->where('ArchivedPack', 0);
	$CI->db->where('ArchivedCategory', 0);
	$CI->db->where_not_in('PackageId', $strPackIds);
	$CI->db->order_by('OrderBy');
	$CI->db->order_by('Category');
	$CI->db->order_by('PackOrderBy');
	$CI->db->order_by('PackageTitle');

	$result = $CI->db->get();
	return $result->result();


}

function get_log_rsPackages($strPackIds)
{
	$CI = &get_instance();

	$CI->db->select('A.CategoryId, Category,A.LogPackageId AS PackageId, LogPackageTitle, DeliveryTime');
	$CI->db->from('tbl_gf_log_package_category Cat, tbl_gf_log_packages A');
	$CI->db->where('Cat.CategoryId = A.CategoryId ');
	$CI->db->where('DisableLogPackage', 0);
	$CI->db->where('DisableCategory', 0);
	$CI->db->where('ArchivedPack', 0);
	$CI->db->where('ArchivedCategory', 0);
	$CI->db->where_not_in('LogPackageId', $strPackIds);
	$CI->db->order_by('OrderBy');
	$CI->db->order_by('Category');
	$CI->db->order_by('PackOrderBy');
	$CI->db->order_by('LogPackageTitle');

	$result = $CI->db->get();
	return $result->result();

}

function fetch_log_package_category_packages()
{

	$CI = &get_instance();

	$CI->db->select('A.CategoryId, Category, A.LogPackageId AS PackageId, LogPackageTitle');
	$CI->db->from('tbl_gf_log_package_category Cat, tbl_gf_log_packages A');
	$CI->db->where('Cat.CategoryId = A.CategoryId ');
	$CI->db->where('DisableLogPackage', 0);
	$CI->db->where('ArchivedPack', 0);
	$CI->db->order_by('OrderBy');
	$CI->db->order_by('Category');
	$CI->db->order_by('PackOrderBy');
	$CI->db->order_by('LogPackageTitle');

	$result = $CI->db->get();
	return $result->result();
}

function get_logpackages_with_status($strPackIds)
{

	$CI = &get_instance();

	$CI->db->select('A.CategoryId, Category, A.LogPackageId AS PackageId, LogPackageTitle');
	$CI->db->from('tbl_gf_log_package_category Cat, tbl_gf_log_packages A');
	$CI->db->where('Cat.CategoryId = A.CategoryId');
	$CI->db->where('DisableLogPackage', 0);
	$CI->db->where_not_in('LogPackageId', $strPackIds);
	$CI->db->order_by('OrderBy');
	$CI->db->order_by('LogPackageTitle');

	$result = $CI->db->get();
	return $result->result();


}

function fetch_log_packages()
{
	$CI = &get_instance();
	$CI->db->select('LogPackageId AS Id, LogPackageTitle AS Value');
	$CI->db->from('tbl_gf_log_packages');
	$CI->db->where('DisableLogPackage', 0);
	$CI->db->where('ArchivedPack', 0);
	$CI->db->order_by('PackOrderBy');
	$CI->db->order_by('PackOrderBy');
	$result = $CI->db->get();
	return $result->result();

}

function get_totalrecs_codes($strWhere)
{
	$CI = &get_instance();
	$row = $CI->db->query("SELECT COUNT(CodeId) AS TotalRecs FROM tbl_gf_codes_slbf A WHERE UserId = '" . $CI->session->userdata('GSM_FUS_UserId') . "' AND Archived = 0 $strWhere")->row();
	return $row;
}

function doPages_DropDown($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next, $frmName = '')
{
	$l = 1;
	$strOptions = '';
	$currPage = 0;
	for ($i = 0; $i < $totalRows; $i = $i + $limit) {
		$selected = '';
		if ($i == $eu) {
			$selected = 'selected';
			$currPage = $l;
		}
		$strOptions .= '<option ' . $selected . ' value="' . $i . '">' . $l . '</option>';
		$l = $l + 1;
	}
	?>
	<br/>
	<div class="form-group" align="right">
		<label class="control-label">Page Number</label>
		<select id="pagingId" name="pagingId"
				onchange="document.getElementById('start').value = document.getElementById('pagingId').value;
					document.getElementById('<? echo $frmName; ?>').submit();" class="form-control" style="width:75px;">
			<?php echo $strOptions; ?>
		</select>
		<span class="help-block">(Total <? echo $totalRows; ?> results)</span>
	</div>
	<?php
}

function doPages($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next, $formSubmit = 0, $frmName = '')
{
	echo '<br /><br /><div class="paging">';
	if ($back < 0) {
		echo '<span class="prn">&lt;&lt; </span>&nbsp;';
	} else {
		if ($formSubmit == '0')
			echo '<a href="' . $page_name . '?start=' . $back . $txtlqry . '" class="prn" rel="nofollow">&lt;&lt; </a>&nbsp;';
		else if ($formSubmit == '1') { ?>
			<a class="prn" rel="nofollow" href="JavaScript:void(0);"
			   onclick="document.getElementById('start').value = <? echo $back; ?>;
				   document.getElementById('<? echo $frmName; ?>').submit();">&lt; Previous</a>&nbsp;
			<?
		}
	}
	$l = 1;
	for ($i = 0; $i < $totalRows; $i = $i + $limit) {
		if ($i == $eu)
			echo '<span>' . $l . '</span>&nbsp;';
		else {
			if ($formSubmit == '0')
				echo '<a href="' . $page_name . '?start=' . $i . $txtlqry . '">' . $l . '</a>&nbsp;';
			else if ($formSubmit == '1') { ?>
				<a href="JavaScript:void(0);" onclick="document.getElementById('start').value = <? echo $i; ?>;
					document.getElementById('<? echo $frmName; ?>').submit();"><? echo $l; ?></a>&nbsp;
				<?
			}
		}
		if ($l != 0) {
			if ($l % 25 == 0)
				echo '<br /><br />';
		}
		$l = $l + 1;
		if ($i < $totalRows)
			$pLast = $i;
	}
	if ($thisp < $totalRows) {
		if ($formSubmit == '0')
			echo '<a href="' . $page_name . '?start=' . $next . $txtlqry . '" class="prn" rel="nofollow">&gt;&gt;</a>&nbsp;';
		else if ($formSubmit == '1') { ?>
			<a class="prn" rel="nofollow" href="JavaScript:void(0);"
			   onclick="document.getElementById('start').value = <? echo $next; ?>;
				   document.getElementById('<? echo $frmName; ?>').submit();">&gt;&gt;</a>&nbsp;
			<?
		}
	} else
		echo '<span class="prn">&gt;&gt;</span>&nbsp;';
	echo '<p id="total_count">(Total ' . $totalRows . ' results)</p></div>';
}

function addMinutesToDtTm($dt, $mins)
{
	$newtimestamp = strtotime("$dt + $mins minute");
	return date('Y-m-d H:i:s', $newtimestamp);
}

function daysHoursMinsDifferenceInDates($dt1, $dt2)
{
	$time1 = strtotime($dt1);
	$time2 = strtotime($dt2);
	$diff = abs($time2 - $time1);
	$years = floor($diff / (365 * 60 * 60 * 24));
	$months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
	$days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
	$hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
	$minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);

	$strData = '';
	if ($days > 0)
		$strData = $days . " Days ";
	if ($hours > 0)
		$strData .= $hours . " Hours ";
	if ($minutes > 0)
		$strData .= $minutes . " Minutes";
	return $strData;
}

function rejectedServerOrderEmail($email, $name, $service, $code, $orderDt, $serviceId, $altEmail, $creditsReturned, $creditsLeft, $orderData, $notes = '', $orderNo = '')
{
	$CI = &get_instance();
	if (sendOrderEmail('SendFailureSrvrOrderEmail')) {
		$arr = getEmailDetails();
		$CI->db->select('CancelTplSubject, CancelTplBody, SendRejectCopyToAdmin');
		$CI->db->from('tbl_gf_log_packages');
		$CI->db->where('LogPackageId', $serviceId);
		$CI->db->where('CancelledTplType', 1);
		$result = $CI->db->get();
		$row = $result->row();

		if (isset($row->CancelTplSubject) && $row->CancelTplSubject != '') {
			$subject = stripslashes($row->CancelTplSubject);
			$contents = stripslashes($row->CancelTplBody);
			$sendCopy = $row->SendRejectCopyToAdmin;
		} else {
			list($subject, $contents, $sendCopy) = getEmailContents(15);
		}
		$placeholders = array("#CUSTOMER_NAME#", "#SERVICE_NAME#", "#DETAILS#", "#ORDER_DATE#", "#COMPANY_NAME#", "#COMPANY_EMAIL_ADDRESS#", "#CREDITS_RETURNED#", "#CREDITS_LEFT#", '#NOTES#', '#ORDER_NO#', "#ORDER_DETAILS#");
		$replacedData = array($name, $service, $code, $orderDt, stripslashes($arr[2]), '<a href="mailto:' . $arr[4] . '">' . $arr[4] . '</a>', $creditsReturned, $creditsLeft, $notes == '' ? '-' : $notes, $orderNo, $orderData);
		$emailMsg = str_replace($placeholders, $replacedData, $contents);
		sendMail($email, $arr[0], $arr[1], $subject, $emailMsg, '', $sendCopy == '1' ? $arr[4] : '');
		if ($altEmail != '')
			sendMail($altEmail, $arr[0], $arr[1], $subject, $emailMsg);
	}
}

function name_validation($name, $field = 'Name', $min_length = 3, $max_length = 33)
{
	$error = '';
	// Full Name must contain letters, dashes and spaces only. We have to eliminate dash at the begining of the name.
	$name = trim($name);
	if (strlen($name) >= $min_length) {
		if (strlen($name) <= $max_length) {
			if (preg_match("/^[a-zA-Z][a-zA-Z -]+$/", $name) === 0)
				$error = $field . ' must contain letters, dashes and spaces only. We do not accept dash at the begining of the ' . $field . '!<br />';
			else $error = null;
		} else $error = $field . ' must contain less than ' . $max_length . ' letters!<br />';
	} else $error = $field . ' must contain at least ' . $min_length . ' letters!<br />';
	return $error;
	/*
    if we want to impose the Full Name to start with upper case letter we have to use that:
    if(preg_match("/^[A-Z][a-zA-Z -]+$/", $name) === 0)
    */
}

function get_country_data()
{

	$CI = &get_instance();

	$CI->db->select('CountryId AS Id, Country AS Value');
	$CI->db->from('tbl_gf_countries');
	$CI->db->where('DisableCountry', 0);
	$CI->db->order_by('Country');


	$result = $CI->db->get();
	return $result->result();

}

function fillListFromString($ids)
{

	$CI = &get_instance();

	$CI->db->select('PaymentMethodId AS Id, PaymentMethod AS Value');
	$CI->db->from('tbl_gf_payment_methods');
	$CI->db->where('ForWholeSale', 1);
	$CI->db->where('DisablePaymentMethod', 0);
	$CI->db->order_by('PaymentMethod');

	$result = $CI->db->get();
	$rs = $result->result();

	$arrIds = array();
	if ($ids != '' && $ids != '0') {
		$arrIds = explode(',', $ids);
	}
	foreach ($rs as $row) {
		if (in_array($row->Id, $arrIds))
			echo "<option value='$row->Id' selected>$row->Value</option>";
		else
			echo "<option value='$row->Id' >$row->Value</option>";
	}
}

function get_reg_field_values($FieldId)
{

	$CI = &get_instance();

	$CI->db->select('RegValue');
	$CI->db->from('tbl_gf_reg_field_values');
	$CI->db->where('DisableRegValue', 0);
	$CI->db->where('FieldId', $FieldId);
	$CI->db->order_by('RegValue');
	$result = $CI->db->get();
	return $result->result();
}

function serviceAPIHistory($packageId, $apiId, $extNetworkId, $currDtTm, $serviceType)
{
	$CI = &get_instance();

	$CI->db->query("INSERT INTO tbl_gf_api_srv_history (PackageId, APIId, APIServiceId, DtTm, ServiceType)
	VALUES ('$packageId', '$apiId', '$extNetworkId', '$currDtTm', '$serviceType')");

}



function get_users_by_id($USER_ID_BTNS)
{

	$CI = &get_instance();
	$CI->db->select('UserName, DisableUser');
	$CI->db->from('tbl_gf_users');
	$CI->db->where('UserId', $USER_ID_BTNS);

	$result = $CI->db->get();
	return $result->row();
}

function showAcceptedIMEIs($ids, $download = 0)
{
	$strIMEIs = '';
	$strCustomCols = '';
	$colIndex = 1;
	$lineBreaker = $download == 0 ? "<br />" : "\r\n";
	$startBoldTag = $download == 0 ? "<b>" : "";
	$endBoldTag = $download == 0 ? "</b>" : "";

	$CI = &get_instance();

	$CI->db->select('FieldColName');
	$CI->db->from('tbl_gf_custom_fields');
	$CI->db->where('ServiceType', 0);
	$CI->db->order_by('FieldLabel');

	$result = $CI->db->get();
	$rsFields = $result->result();
	foreach ($rsFields as $row) {
		$strCustomCols .= ', ' . $row->FieldColName . ' AS Col' . $colIndex;
		$colIndex++;
	}
	$currDtTm = setDtTmWRTYourCountry();

	$CI->db->select("A.PackageId, PackageTitle, IMEINo, ModelNo $strCustomCols");
	$CI->db->from('tbl_gf_codes A, tbl_gf_packages B');
	$CI->db->where('A.PackageId = B.PackageId');
	$CI->db->where_in('CodeId', $ids);
	$CI->db->order_by('A.PackageId');

	$result = $CI->db->get();
	$rsPData = $result->result();


	$prevPackId = 0;
	foreach ($rsPData as $row) {
		$serviceName = stripslashes($row->PackageTitle);
		if ($prevPackId > 0) {
			if ($row->PackageId != $prevPackId) {
				if ($download == '0')
					$strIMEIs .= "$lineBreaker================================================================================================================$lineBreaker";
				$strIMEIs .= "			$startBoldTag Service: $serviceName - $currDtTm $endBoldTag $lineBreaker
								================================================================================================================$lineBreaker";
			}
		} else {
			if ($download == '0')
				$strIMEIs .= "================================================================================================================$lineBreaker";
			$strIMEIs .= "$startBoldTag Service: $serviceName - $currDtTm $endBoldTag $lineBreaker================================================================================================================$lineBreaker";
		}
		if ($row->IMEINo != '')
			$strIMEIs .= $row->IMEINo;
		if ($row->ModelNo != '')
			$strIMEIs .= ' ' . $row->ModelNo;
		for ($i = 1; $i < $colIndex; $i++) {
			$colName = 'Col' . $i;
			if (isset($row->$colName) && $row->$colName != '')
				$strIMEIs .= ' ' . $row->$colName;
		}
		$strIMEIs .= "$lineBreaker";
		$prevPackId = $row->PackageId;
	}
	return $strIMEIs;
}

function get_api_data()
{
	$CI = &get_instance();

	$CI->db->select('APIId AS Id, APITitle AS Value');
	$CI->db->from('tbl_gf_api');
	$CI->db->where('DisableAPI', 0);
	$CI->db->order_by('APITitle');

	$result = $CI->db->get();
	return $result->result();

}


function fetch_api_data_by_concat()
{

	$CI = &get_instance();

	$CI->db->select('CONCAT(APIId, "~", SendExternalId, "~", APIType) AS Id, APITitle AS Value');
	$CI->db->from('tbl_gf_api');
	$CI->db->where('DisableAPI', 0);
	$CI->db->order_by('APITitle');

	$result = $CI->db->get();
	return $result->result();


}

function get_code_status()
{

	$CI = &get_instance();

	$CI->db->select('CodeStatusId AS Id, CodeStatus AS Value');
	$CI->db->from('tbl_gf_code_status');
	$CI->db->where('DisableCodeStatus', 0);
	$CI->db->order_by('CodeStatusId');

	$result = $CI->db->get();
	return $result->result();

}

function fetch_api_srv_history_by_id($packId, $sc)
{

	$CI = &get_instance();

	$CI->db->select('APIId, APIServiceId');
	$CI->db->from('tbl_gf_api_srv_history');
	$CI->db->where('PackageId', $packId);
	$CI->db->where('ServiceType', $sc);
	$CI->db->order_by('Id', 'DESC');

	$result = $CI->db->get();
	return $result->row();


}

function fetch_supplier_services($lastAPIId, $sc)
{

	$CI = &get_instance();

	$CI->db->distinct('ServiceId AS Id, ServiceName, ServicePrice, ServiceTime');
	$CI->db->from('tbl_gf_supplier_services');
	$CI->db->where('APIId', $lastAPIId);
	$CI->db->where('ServiceType', $sc);
	$CI->db->order_by('ServiceName');

	$result = $CI->db->get();
	return $result->result();

}

function fetch_api_srv_history_supplier_services($id)
{

	$CI = &get_instance();

	$CI->db->select('A.APIId, A.APIServiceId, APITitle, ServiceName, ServicePrice');
	$CI->db->from('tbl_gf_api_srv_history A, tbl_gf_supplier_services B, tbl_gf_api C ');
	$CI->db->where('A.APIId = B.APIId');
	$CI->db->where('B.APIId = C.APIId');
	$CI->db->where('A.APIServiceId = B.ServiceId');
	$CI->db->where('PackageId', $id);
	$CI->db->where('A.ServiceType', 2);
	$CI->db->where('B.ServiceType', 2);
	$CI->db->order_by('A.Id', 'DESC');

	$result = $CI->db->get();
	return $result->result();
}

function fetch_supplier_services_by_concat($onlyAPIId, $fs)
{
	$CI = &get_instance();
	$CI->db->select("ServiceId AS Id, CONCAT(ServiceName, '-', ServicePrice, ' Credits', '-', ServiceTime) AS Value");
	$CI->db->from('tbl_gf_supplier_services');
	$CI->db->where('APIId', $onlyAPIId);
	$CI->db->where('ServiceType', $fs);
	$CI->db->order_by('ServiceName');

	$result = $CI->db->get();
	return $result->result();
}


function fetch_supplier_services_by_api_id($onlyAPIId)
{

	$CI = &get_instance();
	$CI->db->select("ServiceId, ServiceName, ServicePrice, RequiredParams");
	$CI->db->from('tbl_gf_supplier_services');
	$CI->db->where('APIId', $onlyAPIId);
	$CI->db->where('ServiceType', 2);
	$CI->db->order_by('ServiceName');

	$result = $CI->db->get();
	return $result->result();
}


function fetch_service_service_types_by_apiid($onlyAPIId)
{
	$CI = &get_instance();
	$CI->db->select("ServiceTypeId AS Id, CONCAT(ServiceTypeName, '-', ServiceTypePrice, ' Credits') AS Value");
	$CI->db->from('tbl_gf_service_service_types');
	$CI->db->where('APIId', $onlyAPIId);
	$CI->db->where('ServiceType', 2);
	$CI->db->order_by('ServiceTypeName');

	$result = $CI->db->get();
	return $result->result();
}

function fetch_custom_field_values($FieldId)
{

	$CI = &get_instance();
	$CI->db->select("RegValue");
	$CI->db->from('tbl_gf_custom_field_values');
	$CI->db->where('DisableRegValue', 0);
	$CI->db->where('FieldId', $FieldId);
	$CI->db->order_by('RegValue');

	$result = $CI->db->get();
	return $result->result();

}

function fetch_packages($fs)
{
	$CI = &get_instance();

	$CI->db->select('PackageId AS Id, PackageTitle AS Value');
	$CI->db->from('tbl_gf_packages');
	$CI->db->where('DisablePackage', 0);
	$CI->db->where('ArchivedPack', 0);
	$CI->db->where('sl3lbf', $fs);
	$CI->db->order_by('PackOrderBy');
	$CI->db->order_by('PackageTitle');

	$result = $CI->db->get();
	return $result->result();
}

function fetch_package_category($fs)
{
	$CI = &get_instance();

	$CI->db->select('CategoryId AS Id, Category AS Value');
	$CI->db->from('tbl_gf_package_category');
	$CI->db->where('DisableCategory', 0);
	$CI->db->where('ArchivedCategory', 0);
	$CI->db->where('SL3BF', $fs);
	$CI->db->order_by('OrderBy');
	$CI->db->order_by('Category');

	$result = $CI->db->get();
	return $result->result();

}

function fetch_imei_restricted_series()
{

	$CI = &get_instance();

	$CI->db->select('SeriesId AS Id, SeriesName AS Value');
	$CI->db->from('tbl_gf_imei_restricted_series');
	$CI->db->where('DisableSeries', 0);
	$CI->db->order_by('SeriesName');


	$result = $CI->db->get();
	return $result->result();
}

function getOrderBGColor($oStatusId)
{
	$strBGColor = '';
	switch ($oStatusId) {
		case '1';
			$strBGColor = 'style="background-color:#F0AD4E; color:#FFFFFF;"';
			break;
		case '2';
			$strBGColor = 'style="background-color:#1D943B; color:#FFFFFF;"';
			break;
		case '3';
			$strBGColor = 'style="background-color:#BB2413; color:#FFFFFF;"';
			break;
		case '4';
			$strBGColor = 'style="background-color:#F0AD4E; color:#FFFFFF;"';
			break;
	}
	return $strBGColor;
}

function getTicketBGColor($oStatusId)
{
	$strBGColor = '';
	switch ($oStatusId) {
		case '1';
			$strBGColor = 'style="background-color:#FFFFC0; color:#000000;"';
			break;
		case '2';
			$strBGColor = 'style="background-color:#CCCCCC; color:#000000;"';
			break;
		case '3';
			$strBGColor = 'style="background-color:#FFCF7F; color:#000000;"';
			break;
		case '4';
			$strBGColor = 'style="background-color:#F35959; color:#FFFFFF;"';
			break;
	}
	return $strBGColor;
}

function get_codes_users_data($strWhere)
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT COUNT(CodeId) AS TotalRecs FROM tbl_gf_codes A, tbl_gf_users D WHERE A.UserId = D.UserId 
    AND AdminArchived = 0 $strWhere");

	return $query->row();
}


function deletePack($packId)
{
	$CI = &get_instance();
	$CI->db->set('ArchivedPack', 1);
	$CI->db->where('PackageId', $packId);
	$CI->db->update('tbl_gf_packages');
}


function fetch_users_codes_count($strWhere)
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT COUNT(CodeId) AS TotalRecs FROM tbl_gf_codes A, tbl_gf_users D WHERE A.UserId = D.UserId AND Verify = 1 $strWhere");
	return $query->row();
}

function fetch_users_codes_count_by_status($strWhere)
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT COUNT(CodeId) AS TotalRecs FROM tbl_gf_codes A, tbl_gf_users D WHERE A.UserId = D.UserId 
    AND OrderAPIId > 0 AND OrderIdFromServer <> '' AND CodeStatusId = '4' $strWhere");
	return $query->row();
}

function get_api_srv_history($packId, $sc)
{
	$CI = &get_instance();

	$CI->db->select('APIId, APIServiceId');
	$CI->db->from('tbl_gf_api_srv_history');
	$CI->db->where('PackageId', $packId);
	$CI->db->where('ServiceType', $sc);
	$CI->db->order_by('Id', 'DESC');

	$result = $CI->db->get();
	return $result->row();


}

function fetch_codes_slbf_users($strWhere)
{

	$CI = &get_instance();

	$query = $CI->db->query("SELECT COUNT(CodeId) AS TotalRecs FROM tbl_gf_codes_slbf A, tbl_gf_users D WHERE A.UserId = D.UserId 
    AND AdminArchived = 0 $strWhere");

	return $query->row();

}


function fetch_slbf_code_users_record($strWhere)
{
	$CI = &get_instance();

	$query = $CI->db->query("SELECT COUNT(CodeId) AS TotalRecs FROM tbl_gf_codes_slbf A, tbl_gf_users D WHERE A.UserId = D.UserId AND Verify = 1 $strWhere");

	return $query->row();

}

function fetch_count_log_requests($strWhere)
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT COUNT(LogRequestId) AS TotalRecs FROM tbl_gf_log_requests A, tbl_gf_users D WHERE A.UserId = D.UserId $strWhere");
	return $query->row();


}

function fetch_count_log_requests_by_verify($strWhere)
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT COUNT(LogRequestId) AS TotalRecs FROM tbl_gf_log_requests A, tbl_gf_users D WHERE A.UserId = D.UserId AND Verify = 1 $strWhere");
	return $query->row();
}

function downloadFile($path, $contentType = 'application/octet-stream')
{
	ignore_user_abort(true);
	header('Content-Transfer-Encoding: binary');
	header('Content-Disposition: attachment; filename="' .
		basename($path) . "\";");
	header("Content-Type: $contentType");

	$res = array(
		'status' => false,
		'errors' => array(),
		'readfileStatus' => null,
		'aborted' => false
	);

	$res['readfileStatus'] = readfile($path);
	if ($res['readfileStatus'] === false) {
		$res['errors'][] = 'readfile failed.';
		$res['status'] = false;
	}

	if (connection_aborted()) {
		$res['errors'][] = 'Connection aborted.';
		$res['aborted'] = true;
		$res['status'] = false;
	}
	return $res;
}


function fetch_price_plan_data()
{

	$CI = &get_instance();

	$CI->db->select('PricePlanId AS Id, PricePlan AS Value');
	$CI->db->from('tbl_gf_price_plans');
	$CI->db->where('DisablePricePlan', 0);
	$CI->db->order_by('PricePlan');

	$result = $CI->db->get();
	return $result->result();

}

function fetch_price_plan_data1()
{

	$CI = &get_instance();

	$CI->db->select('PricePlanId, PricePlan');
	$CI->db->from('tbl_gf_price_plans');
	$CI->db->where('DisablePricePlan', 0);
	$CI->db->order_by('PricePlan');

	$result = $CI->db->get();
	return $result->result();

}


function fetch_tblcat($tblCat, $strWhereCat)
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT CategoryId AS Id, Category AS Value FROM $tblCat WHERE DisableCategory = 0 AND ArchivedCategory = 0 $strWhereCat ORDER BY OrderBy, Category");
	return $query->result();
}

function fetch_service_category($tblName, $strWhereCat)
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT CategoryId AS Id, Category AS Value FROM $tblName WHERE DisableCategory = 0
    AND ArchivedCategory = 0 $strWhereCat ORDER BY OrderBy, Category");
	return $query->result();
}

function package_category($strWhereCat)
{
	$CI = &get_instance();

	$query = $CI->db->query("SELECT CategoryId AS Id, Category AS Value FROM tbl_gf_package_category WHERE DisableCategory = 0
    AND ArchivedCategory = 0 $strWhereCat ORDER BY OrderBy, Category");

	return $query->result();

}

function fetch_package_category_count($strWhere)
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT LogPackageId FROM tbl_gf_package_category D, tbl_gf_log_packages A WHERE 
    A.CategoryId = D.CategoryId $strWhere ORDER BY OrderBy, Category, PackOrderBy, LogPackageTitle");

	$result = $query->result();
	$result_count = count($result);
	return $result_count;
}

function fetch_service_cat_by_archived_cat($tblName, $strWhereCat)
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT CategoryId AS Id, Category AS Value FROM $tblName WHERE DisableCategory = 0
    AND ArchivedCategory = 0 $strWhereCat ORDER BY OrderBy, Category");

	return $query->result();
}

function fetch_category_packages_api($strWhere)
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT PackageId, PackageTitle, PackagePrice, If(A.APIId = '-1', '-', APITitle) AS API,DisablePackage, Category FROM tbl_gf_package_category D, tbl_gf_packages A LEFT JOIN tbl_gf_api B ON (A.APIId = B.APIId) WHERE 
        A.CategoryId = D.CategoryId AND A.ArchivedPack=0 $strWhere ORDER BY OrderBy, Category, PackOrderBy, PackageTitle");

	return $query->result();

}

function fetch_custom_log_package_category($tblCat, $strWhereCat)
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT CategoryId AS Id, Category AS Value FROM $tblCat WHERE DisableCategory = 0 AND ArchivedCategory = 0 $strWhereCat ORDER BY OrderBy, Category");
	return $query->result();
}


function fetch_custom_fields()
{
	$CI = &get_instance();
	$CI->db->select('FieldId AS Id, FieldLabel AS Value');
	$CI->db->from('tbl_gf_custom_fields');
	$CI->db->where('ServiceType', 0);
	$CI->db->where('DisableField', 0);
	$CI->db->order_by('FieldLabel');
	$result = $CI->db->get();

	return $result->result();
}

function getServiceFeaturesForEmail($packId, $serviceType)
{
	$CI = &get_instance();
	$strFeatures = '';
	$rs = $CI->db->query("SELECT Feature, IF(B.FeatureId IS NULL, 'No', 'Yes') AS Featured FROM tbl_gf_service_features A LEFT JOIN tbl_gf_pack_selected_features B ON (A.FeatureId = B.FeatureId AND ServiceId = '$packId' AND ServiceType = '$serviceType')");
	foreach ($rs as $row) {
		$strFeatures .= '<p>' . stripslashes($row->Feature) . ' - <b>' . $row->Featured . '</b></p>';
	}
	return $strFeatures;
}

function replicateService($serviceId, $newTitle)
{
	$CI = &get_instance();
	$CI->db->query("INSERT INTO tbl_gf_packages (PackageTitle, PackagePrice, ExternalNetworkId, APIId, CategoryId, TimeTaken, MustRead, sl3lbf, DisablePackage,
					ToolForUnlockBase, DuplicateIMEIsNotAllowed, SEOURLName, MetaKW, HTMLTitle, FileName, MetaTags, IMEIFieldType, NewTplType, SuccessTplType, CancelledTplType,
					NewTplSubject, NewTplBody, SuccessTplSubject, SuccessTplBody, CancelTplSubject, CancelTplBody, SendNewCopyToAdmin, SendSuccessCopyToAdmin,
					SendRejectCopyToAdmin, CostPrice, EditedAt, Supplier, CalculatePreCodes, SendSMS, CustomFieldId, ServiceType, ResponseDelayTm, CronDelayTm, CancelOrders,
					VerifyOrders, OrderVerifyMins, RedirectionURL, TOCs, CostPriceFromAPI, NewOrderEmailIDs
	)
	SELECT '$newTitle' AS PackageTitle, PackagePrice, ExternalNetworkId, APIId, CategoryId, TimeTaken, MustRead, sl3lbf, DisablePackage,
		ToolForUnlockBase, DuplicateIMEIsNotAllowed, SEOURLName, MetaKW, HTMLTitle, FileName, MetaTags, IMEIFieldType, NewTplType, SuccessTplType, CancelledTplType,
		NewTplSubject, NewTplBody, SuccessTplSubject, SuccessTplBody, CancelTplSubject, CancelTplBody, SendNewCopyToAdmin, SendSuccessCopyToAdmin,
		SendRejectCopyToAdmin, CostPrice, EditedAt, Supplier, CalculatePreCodes, SendSMS, CustomFieldId, ServiceType, ResponseDelayTm, CronDelayTm, CancelOrders,
		VerifyOrders, OrderVerifyMins, RedirectionURL, TOCs, CostPriceFromAPI, NewOrderEmailIDs FROM tbl_gf_packages WHERE PackageId = '$serviceId'");
	return $CI->db->insert_id();
}

function applyAPIOnPendingOrders($serviceType, $apiId, $apiServiceId, $packageId)
{
	$CI = &get_instance();
	$CI->db->select('APIId, APIKey, APITitle, APIType, ServerURL, AccountId');
	$CI->db->from('tbl_gf_api');
	$CI->db->where('APIId', $apiId);
	$CI->db->where('DisableAPI', 0);
	$result = $CI->db->get();
	$rsAPI = $result->row();
	if (isset($rsAPI->APIId) && $rsAPI->APIId != '') {
		$apiType = $rsAPI->APIType;
		$apiKey = $rsAPI->APIKey;
		$serverURL = $rsAPI->ServerURL;
		$apiUserName = $rsAPI->AccountId;
		$apiName = stripslashes($rsAPI->APITitle);

		switch ($serviceType) {
			case '0';
				$tblName = 'tbl_gf_codes';
				$statusCol = 'CodeStatusId';
				$serviceCol = 'PackageId';
				break;
			case '1';
				$tblName = 'tbl_gf_codes_slbf';
				$statusCol = 'CodeStatusId';
				$serviceCol = 'PackageId';
				break;
			case '2';
				$tblName = 'tbl_gf_log_requests';
				$statusCol = 'StatusId';
				$serviceCol = 'LogPackageId';
				break;
			case '3';
				$tblName = 'tbl_gf_retail_orders';
				$statusCol = 'OrderStatusId';
				$serviceCol = 'PackageId';
				break;
		}
		$CI->db->query("UPDATE $tblName SET OrderAPIId = '$apiId', OrderAPIURL = '$serverURL', OrderAPIUserName = '$apiUserName', OrderAPIType = '$apiType', 
		OrderAPIKey = '$apiKey', OrderAPIServiceId = '$apiServiceId', OrderAPIName = '$apiName' WHERE $serviceCol = '$packageId' AND $statusCol = '1' AND OrderAPIId = 0");
	}
}

function fetch_log_package_category_by_id()
{

	$CI = &get_instance();
	$CI->db->select('CategoryId AS Id, Category AS Value');
	$CI->db->from('tbl_gf_log_package_category');
	$CI->db->where('DisableCategory', 0);
	$CI->db->where('ArchivedCategory', 0);
	$CI->db->order_by('OrderBy');
	$CI->db->order_by('Category');

	$result = $CI->db->get();
	return $result->result();
}

function fetch_precode_count($id, $strPrCdWhr)
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT COUNT(PreCodeId) AS TotalRecs FROM tbl_gf_precodes WHERE ServiceId = '$id' AND ServiceType = '2' $strPrCdWhr");
	return $query->row();
}

function FillSelectedList($fs, $id, $Column)
{

	$CI = &get_instance();
	$CI->db->select('FieldId AS Id, FieldLabel AS Value');
	$CI->db->from('tbl_gf_custom_fields');
	$CI->db->where('ServiceType', $fs);
	$CI->db->where('DisableField', 0);
	$CI->db->order_by('FieldLabel');
	$result = $CI->db->get();
	$rs = $result->result();
	foreach ($rs as $row) {
		$CI->db->select('FieldId AS Id');
		$CI->db->from('tbl_gf_package_custom_fields');
		$CI->db->where('PackageId', $id);
		$CI->db->where('ServiceType', $fs);
		$CI->db->where($Column, $row->Id);
		$result = $CI->db->get();
		$rw = $result->row();
		if (isset($rw->Id) && $rw->Id != '')
			echo "<option value='$row->Id' selected>$row->Value</option>";
		else
			echo "<option value='$row->Id' >$row->Value</option>";
	}
}

function FillSelectedList2($id, $col)
{

	$CI = &get_instance();
	$CI->db->select('ISO AS Id, Country AS Value ');
	$CI->db->from('tbl_gf_countries');
	$CI->db->where('DisableCountry', 0);
	$CI->db->order_by('Country');
	$result = $CI->db->get();
	$rs = $result->result();
	foreach ($rs as $row) {
		$CI->db->select('ISO AS Id');
		$CI->db->from('tbl_gf_user_login_countries');
		$CI->db->where('UserId', $id);
		$CI->db->where($col, $row->Id);
		$result = $CI->db->get();
		$rw = $result->row();
		if (isset($rw->Id) && $rw->Id != '')
			echo "<option value='$row->Id' selected>$row->Value</option>";
		else
			echo "<option value='$row->Id' >$row->Value</option>";
	}
}

function FillSelectedList_payments($USER_ID_BTNS, $col)
{

	$CI = &get_instance();
	$CI->db->select('PaymentMethodId AS Id, PaymentMethod AS Value');
	$CI->db->from('tbl_gf_payment_methods');
	$CI->db->where('DisablePaymentMethod', 0);
	$CI->db->order_by('PaymentMethod');
	$result = $CI->db->get();
	$rs = $result->result();
	foreach ($rs as $row) {
		$CI->db->select('PaymentMethodId AS Id');
		$CI->db->from('tbl_gf_user_payment_methods');
		$CI->db->where('UserId', $USER_ID_BTNS);
		$CI->db->where($col, $row->Id);
		$result = $CI->db->get();
		$rw = $result->row();

		if (isset($rw->Id) && $rw->Id != '')
			echo "<option value='$row->Id' selected>$row->Value</option>";
		else
			echo "<option value='$row->Id' >$row->Value</option>";
	}
}


function updateUser($userIds)
{
	$CI = &get_instance();
	$IdsList = explode(",", $userIds);

	foreach ($IdsList as $id) {

		$CI->db->set('UserArchived', 1);
		$CI->db->where('UserId', $id);
		$CI->db->update('tbl_gf_users');
	}

}

function fetch_codes_sum($USER_ID_BTNS)
{

	$CI = &get_instance();

	$CI->db->select('SUM(Credits) AS LockedAmount');
	$CI->db->from('tbl_gf_codes');
	$CI->db->where_in('CodeStatusId', array(1, 4));
	$CI->db->where('UserId', $USER_ID_BTNS);

	$result = $CI->db->get();
	return $result->row();

}

function fetch_codes_slbf($USER_ID_BTNS)
{

	$CI = &get_instance();

	$CI->db->select('SUM(Credits) AS LockedAmount');
	$CI->db->from('tbl_gf_codes_slbf');
	$CI->db->where_in('CodeStatusId', array(1, 4));
	$CI->db->where('UserId', $USER_ID_BTNS);

	$result = $CI->db->get();
	return $result->row();
}

function fetch_log_requests($USER_ID_BTNS)
{

	$CI = &get_instance();

	$CI->db->select('SUM(Credits) AS LockedAmount');
	$CI->db->from('tbl_gf_log_requests');
	$CI->db->where_in('StatusId', array(1, 4));
	$CI->db->where('UserId', $USER_ID_BTNS);

	$result = $CI->db->get();
	return $result->row();
}

function fetch_codes_by_CodeStatusId($USER_ID_BTNS)
{

	$CI = &get_instance();

	$CI->db->select("SUM(Credits) AS UsedAmount");
	$CI->db->from('tbl_gf_codes');
	$CI->db->where('CodeStatusId', '2');
	$CI->db->where('UserId', $USER_ID_BTNS);

	$result = $CI->db->get();
	return $result->row();
}

function fetch_codes_slbf_by_CodeStatusId($USER_ID_BTNS)
{

	$CI = &get_instance();

	$CI->db->select("SUM(Credits) AS UsedAmount");
	$CI->db->from('tbl_gf_codes_slbf');
	$CI->db->where('CodeStatusId', '2');
	$CI->db->where('UserId', $USER_ID_BTNS);

	$result = $CI->db->get();
	return $result->row();

}

function fetch_log_requests_by_statusid($USER_ID_BTNS)
{

	$CI = &get_instance();

	$CI->db->select("SUM(Credits) AS UsedAmount");
	$CI->db->from('tbl_gf_log_requests');
	$CI->db->where('StatusId', '2');
	$CI->db->where('UserId', $USER_ID_BTNS);

	$result = $CI->db->get();
	return $result->row();

}

function fetch_payment_details_sum($USER_ID_BTNS)
{

	$CI = &get_instance();

	$CI->db->select("SUM(InvAmount) AS Amount");
	$CI->db->from('tbl_gf_payment_details A, tbl_gf_payments B');
	$CI->db->where('A.InvoiceId = B.PaymentId');
	$CI->db->where('PaymentStatus', 1);
	$CI->db->where('UserId', $USER_ID_BTNS);
	$CI->db->where('ByAdmin', 1);

	$result = $CI->db->get();
	return $result->row();
}

function fetch_credit_history_by_userid($userId, $strWhere)
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT COUNT(HistoryId) AS TotalRecs FROM tbl_gf_credit_history A WHERE UserId = $userId $strWhere");

	return $query->row();

}

function fetch_payment_status()
{

	$CI = &get_instance();
	$CI->db->select('PaymentStatusId AS Id, PaymentStatus As Value');
	$CI->db->from('tbl_gf_payment_status');
	$CI->db->where_in('PaymentStatusId', array(1, 2));
	$CI->db->where('DisablePaymentStatus', 0);
	$CI->db->order_by('PaymentStatus');

	$result = $CI->db->get();

	return $result->result();
}

function fetch_payment_status_by_id($strWherePmnts)
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT PaymentStatusId AS Id, PaymentStatus AS Value FROM tbl_gf_payment_status WHERE DisablePaymentStatus = 0 $strWherePmnts ORDER BY PaymentStatus");
	return $query->result();
}

function fetch_payment_methods()
{

	$CI = &get_instance();
	$CI->db->select('PaymentMethodId AS Id, PaymentMethod AS Value');
	$CI->db->from('tbl_gf_payment_methods');
	$CI->db->where('DisablePaymentMethod', 0);
	$CI->db->order_by('PaymentMethod');

	$result = $CI->db->get();
	return $result->result();

}

function userStatusEmail($userId)
{
	$CI = &get_instance();

	$CI->db->select("CONCAT(FirstName, '', LastName) AS UName, UserEmail, DisableUser");
	$CI->db->from('tbl_gf_users');
	$CI->db->where('UserId', $userId);

	$result = $CI->db->get();

	$rwUN = $result->row();
	if (isset($rwUN->DisableUser) && $rwUN->DisableUser != '') {
		$name = $rwUN->UName;
		$dUser = $rwUN->DisableUser;
		$email = $rwUN->UserEmail;
	}
	$arr = getEmailDetails();
	list($subject, $contents, $sendCopy) = getEmailContents($dUser == '1' ? '22' : '21');
	$placeholders = array("#CUSTOMER_NAME#", "#COMPANY_NAME#", "#COMPANY_EMAIL_ADDRESS#", "#LOGIN_LINK#");
	$replacedData = array($name, stripslashes($arr[2]), '<a href="mailto:' . $arr[4] . '">' . $arr[4] . '</a>',
		'<a href="http://' . $arr[3] . '/login">http://' . $arr[3] . '/login</a>');
	$subject = str_replace($placeholders, $replacedData, $subject);
	$emailMsg = str_replace($placeholders, $replacedData, $contents);
	sendMail($email, $arr[0], $arr[1], $subject, $emailMsg, '', $sendCopy == '1' ? $arr[4] : '');
}

function apiKeyGnrtnEmail($email, $name, $apiKey)
{
	$arr = getEmailDetails();
	list($subject, $contents, $sendCopy) = getEmailContents(16);
	$placeholders = array("#CUSTOMER_NAME#", "#API_KEY#", "#COMPANY_NAME#", "#COMPANY_EMAIL_ADDRESS#", "#API_DOWNLOAD_LINK#");
	$replacedData = array($name, $apiKey, stripslashes($arr[2]), '<a href="mailto:' . $arr[4] . '">' . $arr[4] . '</a>', '<a href="http://' . $arr[3] . '/api.rar">here</a>');
	$subject = str_replace($placeholders, $replacedData, $subject);
	$emailMsg = str_replace($placeholders, $replacedData, $contents);
	sendMail($email, $arr[0], $arr[1], $subject, $emailMsg, '', $sendCopy == '1' ? $arr[4] : '');
}

function rebateUserCredits($invId)
{
	$CI = &get_instance();
	$row = $CI->db->query("SELECT A.UserId, B.UserName, UserEmail, A.Credits AS InvCredits, B.Credits AS UserCredits, CreditsTransferred, A.Currency, 
					C.PaymentMethod FROM tbl_gf_payments A, tbl_gf_users B, tbl_gf_payment_methods C WHERE A.UserId = B.UserId AND A.PaymentMethod = C.PaymentMethodId 
					AND PaymentId = '$invId' AND CreditsTransferred = '1' ")->row();
	$key = crypt_key($row->UserId);
	$credits = 0;
	$updatedCredits = 0;
	if (isset($row->UserCredits) && $row->UserCredits != '')
		$credits = $row->UserCredits;

	$decCredits = decrypt($credits, $key);
	$newCredits = decrypt($row->InvCredits, $key);
	$decCredits -= $newCredits;
	$updatedCredits = $newCredits;
	if (is_numeric($decCredits)) {
		$encNewCredits = encrypt($newCredits, $key);
		$encCredits = encrypt($decCredits, $key);
		$CI->db->query("UPDATE tbl_gf_users SET Credits = '$encCredits' WHERE UserId = '" . $row->UserId . "'");
		$currDtTm = setDtTmWRTYourCountry();
		$arrCurrDt = explode(' ', $currDtTm);
		$currDt = $arrCurrDt[0];
		$CI->db->query("UPDATE tbl_gf_payments SET CreditsTransferred = 0 WHERE PaymentId = '$invId'");

		$CI->db->query("INSERT INTO tbl_gf_credit_history SET ByAdmin = 1, UserId = '" . $row->UserId . "', Credits = '-$updatedCredits', 
		Description = 'Rebated Funds for Invoice # $invId', IMEINo = '', HistoryDtTm = '$currDtTm', CreditsLeft = '$encCredits', Comments = '$comments'");

		$message = "<b>$updatedCredits</b> credits have been successfully rebated for " . $row->UserName . " <b>(Invoice #$invId)</b>, set as Cancelled";
		invoiceEmail($row->UserEmail, $row->UserName, $invId, $newCredits, $newCredits, $row->Currency, $currDt, $row->PaymentMethod, '6', '', '', $message);
	}
}

function fetch_users_payments($strWhere)
{

	$CI = &get_instance();

	$result = $CI->db->query("SELECT COUNT(PaymentId) AS TotalRecs FROM tbl_gf_payments A, tbl_gf_users B WHERE A.UserId = B.UserId " . $strWhere);
	return $result->row();

}
function fetch_user_retail_payments($strWhere){

	$CI = &get_instance();

	$result = $CI->db->query("SELECT COUNT(RetailPaymentId) AS TotalRecs FROM tbl_gf_retail_payments A ".$strWhere);

	return $result->row();

}

function fetch_distinct_users_countries()
{
	$CI = &get_instance();
	$result = $CI->db->query("SELECT DISTINCT A.CountryId AS Id, Country AS Value FROM tbl_gf_users A, tbl_gf_countries B WHERE A.CountryId = B.CountryId ORDER BY Country");
	return $result->result();

}

function fetch_users_count($strWhere)
{
	$CI = &get_instance();
	$result = $CI->db->query("SELECT COUNT(UserId) AS TotalRecs FROM tbl_gf_users A WHERE (1) $strWhere");
	return $result->row();
}

function fetch_currency_by_disablecurrency()
{
	$CI = &get_instance();
	$result = $CI->db->query("SELECT CurrencyId AS Id, Currency AS Value FROM tbl_gf_currency WHERE DisableCurrency = 0 ORDER BY Currency");
	return $result->result();
}

function convertPostToArray()
{
	static $post;
	if (!isset($post)) {
		$pairs = explode("&", file_get_contents("php://input"));
		$post = array();
		foreach ($pairs as $pair) {
			$x = explode("=", $pair);
			$post[rawurldecode($x[0])] = rawurldecode($x[1]);
		}
	}
	return $post;
}

function getCurrencyPricesForPlans($planId = 0, $sc = 0)
{
	$CI = &get_instance();
	$strWhere = " AND ServiceType = '$sc'";
	$strCurrIds = '';
	if ($planId > 0)
		$strWhere .= " AND PlanId = '$planId'";
	$PACK_PRICES = array();
	$query = $CI->db->query("SELECT CurrencyId FROM tbl_gf_currency WHERE DefaultCurrency <> 1 AND DisableCurrency = 0");
	$rsCurrencyIds = $query->result();
	foreach ($rsCurrencyIds as $row) {
		if ($strCurrIds == '')
			$strCurrIds = $row->CurrencyId;
		else
			$strCurrIds .= ', ' . $row->CurrencyId;
	}
	if ($strCurrIds != '') {
		$query = $CI->db->query("SELECT PackageId, CurrencyId, Price FROM tbl_gf_plans_packages_prices WHERE CurrencyId IN ($strCurrIds) $strWhere");
		$rs = $query->result();
		foreach ($rs as $row) {
			$PACK_PRICES[$row->PackageId][$row->CurrencyId] = roundMe($row->Price);
		}
		if (count($PACK_PRICES) == 0)// No price found in above criteria
		{
			if ($sc == '2')
				$tbl = 'tbl_gf_log_packages_currencies';
			else
				$tbl = 'tbl_gf_packages_currencies';
			$query = $CI->db->query("SELECT PackageId, Price, CurrencyId FROM $tbl WHERE CurrencyId IN ($strCurrIds)");
			$rs = $query->result();
			foreach ($rs as $row) {
				$PACK_PRICES[$row->PackageId][$row->CurrencyId] = roundMe($row->Price);
			}
		}
	}
	return $PACK_PRICES;
}

function fetch_cat_by_tblname($tblName, $strWhereCat)
{
	$CI = &get_instance();

	$query = $CI->db->query("SELECT CategoryId AS Id, Category AS Value FROM $tblName WHERE DisableCategory = 0 AND ArchivedCategory = 0 
    $strWhereCat ORDER BY OrderBy, Category");

	return $query->result();

}

function generatePassword($length = 6, $level = 2)
{
	list($usec, $sec) = explode(' ', microtime());
	srand((float)$sec + ((float)$usec * 100000));

	$validchars[1] = "0123456789abcdfghjkmnpqrstvwxyz";
	$validchars[2] = "0123456789abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$validchars[3] = "0123456789_!@#$%&*()-=+/abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_!@#$%&*()-=+/";

	$password = "";
	$counter = 0;

	while ($counter < $length) {
		$actChar = substr($validchars[$level], rand(0, strlen($validchars[$level]) - 1), 1);

		// All character must be different
		if (!strstr($password, $actChar)) {
			$password .= $actChar;
			$counter++;
		}
	}
	return $password;
}

function generatePin($length)
{
	$chars = "0123456789";
	$size = strlen($chars);
	$str='';
	for ($i = 0; $i < $length; $i++) {
		$str .= $chars[rand(0, $size - 1)];
	}
	return $str;
}

function fetch_form_type()
{
	$CI = &get_instance();
	$CI->db->select('FormTypeId AS Id, FormType AS Value');
	$CI->db->from('tbl_gf_form_type');
	$CI->db->order_by('FormTypeId');

	$result = $CI->db->get();
	return $result->result();

}

function fetch_pay_method_types()
{

	$CI = &get_instance();
	$CI->db->select('PayMethodTypeId AS Id, PayMethodType AS Value');
	$CI->db->from('tbl_gf_pay_method_types');
	$CI->db->order_by('PayMethodType');

	$result = $CI->db->get();
	return $result->result();
}

//ahmed
function lookupcases($service = '', $iFrm = '')
{
	$CI = &get_instance();
	if ($service != '') {
		$strLabel = $_REQUEST['service'];
		$idCol = 'Id';
		$textCol = 'Title';
		$disableCol = 'DisableValue';
		$tbl = $_REQUEST['servtbl'];
	} else {
		switch ($iFrm) {
			case '3':
				$strLabel = $CI->lang->line('BE_LS_3');
				$idCol = 'CodeStatusId';
				$textCol = 'CodeStatus';
				$disableCol = 'DisableCodeStatus';
				$tbl = 'tbl_gf_code_status';
				break;
			case '5':
				$strLabel = $CI->lang->line('BE_LS_5');
				$idCol = 'CategoryId';
				$textCol = 'Category';
				$disableCol = 'DisableCategory';
				$tbl = 'tbl_gf_package_category';
				break;
			case '6':
				$strLabel = $CI->lang->line('BE_LS_6');
				$idCol = 'CountryId';
				$textCol = 'Country';
				$disableCol = 'DisableCountry';
				$tbl = 'tbl_gf_countries';
				break;
			case '7':
				$strLabel = $CI->lang->line('BE_LS_7');
				$idCol = 'PaymentStatusId';
				$textCol = 'PaymentStatus';
				$disableCol = 'DisablePaymentStatus';
				$tbl = 'tbl_gf_payment_status';
				break;
			case '8':
				$strLabel = 'Task Category';
				$idCol = 'CategoryId';
				$textCol = 'Category';
				$disableCol = 'DisableCategory';
				$tbl = 'tbl_gf_task_category';
				break;
			case '9':
				$strLabel = $CI->lang->line('BE_LBL_189');
				$idCol = 'TcktPriorityId';
				$textCol = 'TcktPriority';
				$disableCol = 'DisableTcktPriority';
				$tbl = 'tbl_gf_tckt_priority';
				break;
			case '10':
				$strLabel = $CI->lang->line('BE_LBL_18');
				$idCol = 'PricePlanId';
				$textCol = 'PricePlan';
				$disableCol = 'DisablePricePlan';
				$tbl = 'tbl_gf_price_plans';
				break;
			case '11':
				$strLabel = $CI->lang->line('BE_LBL_35');
				$idCol = 'ListId';
				$textCol = 'List';
				$disableCol = 'DisableList';
				$tbl = 'tbl_gf_user_list';
				break;
			case '12':
				$strLabel = $CI->lang->line('BE_LBL_118');
				$idCol = 'PricePlanOfferId';
				$textCol = 'PricePlanOffer';
				$disableCol = 'DisablePricePlanOffer';
				$tbl = 'tbl_gf_price_plans_offers';
				break;
			case '13':
				$strLabel = $CI->lang->line('BE_LBL_841');
				$idCol = 'CategoryId';
				$textCol = 'Category';
				$disableCol = 'DisableCategory';
				$tbl = 'tbl_gf_manufacturer';
				break;
			case '14':
				$strLabel = $CI->lang->line('BE_LBL_723');
				$idCol = 'TcktStatusId';
				$textCol = 'TcktStatus';
				$disableCol = 'DisableTcktStatus';
				$tbl = 'tbl_gf_tckt_status';
				break;
			case '15':
				$strLabel = $CI->lang->line('BE_LBL_724');
				$idCol = 'TcktCategoryId';
				$textCol = 'TcktCategory';
				$disableCol = 'DisableTcktCategory';
				$tbl = 'tbl_gf_tckt_category';
				break;
			case '16':
				$strLabel = $CI->lang->line('BE_LBL_803');
				$idCol = 'CategoryId';
				$textCol = 'Category';
				$disableCol = 'DisableCategory';
				$tbl = 'tbl_gf_knowledgebase_cat';
				break;
			case '17':
				$strLabel = $CI->lang->line('BE_CAT_1');
				$idCol = 'CategoryId';
				$textCol = 'Category';
				$disableCol = 'DisableCategory';
				$tbl = 'tbl_gf_newsltrs_cat';
				break;
			case '18':
				$strLabel = $CI->lang->line('BE_CAT_1');
				$idCol = 'CategoryId';
				$textCol = 'Category';
				$disableCol = 'DisableCategory';
				$tbl = 'tbl_gf_news_cat';
				break;
			case '19':
				$strLabel = $CI->lang->line('BE_LS_6');
				$idCol = 'CountryId';
				$textCol = 'Country';
				$disableCol = 'DisableCountry';
				$tbl = 'tbl_gf_make_country';
				break;
			case '20':
				$strLabel = $CI->lang->line('BE_LS_7');
				$idCol = 'PaymentStatusId';
				$textCol = 'PaymentStatus';
				$disableCol = 'DisablePaymentStatus';
				$tbl = 'tbl_gf_ecommerce_pstatus';
				break;
			case '21':
				$strLabel = $CI->lang->line('BE_LS_3');
				$idCol = 'OrderStatusId';
				$textCol = 'OrderStatus';
				$disableCol = 'DisableOrderStatus';
				$tbl = 'tbl_gf_order_status';
				break;
		}
	}
	$lookupcases = array();
	$lookupcases['strLabel'] = $strLabel;
	$lookupcases['idCol'] = $idCol;
	$lookupcases['textCol'] = $textCol;
	$lookupcases['disableCol'] = $disableCol;
	$lookupcases['tbl'] = $tbl;
	return $lookupcases;
}

//ahmed
function getNewsCategories()
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT CategoryId AS Id, Category AS Value FROM tbl_gf_news_cat WHERE DisableCategory = 0 ORDER BY Category");
	return $query->result();
}

//ahmed
function getKnowledegeBaseCategories()
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT CategoryId AS Id, Category AS Value FROM tbl_gf_knowledgebase_cat WHERE DisableCategory = 0 ORDER BY Category");
	return $query->result();
}

function tbl_gf_newsltrs_cat()
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT CategoryId AS Id, Category AS Value FROM tbl_gf_newsltrs_cat WHERE DisableCategory = 0 ORDER BY Category");
	return $query->result();
}

/// Usman

function newsletter_dropdown()
{

	$CI = &get_instance();

	$query = $CI->db->query("SELECT A.CategoryId, Category, A.NewsLtrId, NewsLtrTitle FROM tbl_gf_newsltrs_cat Cat, tbl_gf_news_letter A WHERE Cat.CategoryId = A.CategoryId AND DisableNewsLtr = 0 ORDER BY OrderBy, Category, NewsLtrTitle");
	$rsNLs = $query->result();
	if ($rsNLs) {
		$prevCatId = 0;
		foreach ($rsNLs as $row) {
			if ($prevCatId > 0) {
				if ($row->CategoryId != $prevCatId)
					echo '</optgroup><optgroup label="' . $row->Category . '"> ';
			} else {
				echo '<option value="0" selected>Choose a Newsletter</option><optgroup label="' . $row->Category . '">';
			}
			?>
			<option value="<?php echo $row->NewsLtrId ?>"><?php echo $row->NewsLtrTitle; ?></option>
			<?php
			$prevCatId = $row->CategoryId;
		}
		echo '</optgroup>';

	}
// else{
// 	echo '<option class="clsOption" value="0" selected>Choose a Newsletter</option>';
// }

}


//Added by Shahid - 5 Mar 2020
function encryptAPIKey($key, $password, $iv_len)
{
	$key .= "\x13";
	$n = strlen($key);
	if ($n % 16) $key .= str_repeat("\0", 16 - ($n % 16));
	$i = 0;
	$enc_text = get_rnd_iv($iv_len);
	$iv = substr($password ^ $enc_text, 0, 512);
	while ($i < $n) {
		$block = substr($key, $i, 16) ^ pack('H*', md5($iv));
		$enc_text .= $block;
		$iv = substr($block . $iv, 0, 512) ^ $password;
		$i += 16;
	}
	return base64_encode($enc_text);
}

function check_input($value, $connection = null, $ckEditor = 0)
{
	$value = addslashes($value);
	if ($connection != null) {
		mysqli_real_escape_string($connection, $value);
	}
	if ($ckEditor == '0')
		$val = replaceRN($value);
	return $value;
}

function getProductPrice($productId, $prodPrice, $promoStartDate, $promoEndDate, $promoDiscount, $TODAY_DT_TM, $quantity)
{
	if ($this->session->userdata('EUserPriceGroupId') && $$this->session->userdata('EUserPriceGroupId') > 0 && $this->session->userdata('EUserPriceGroupId') != '') {
		$prodPrice = getProductGroupPrice($this->session->userdata('GSM_FUS_ECustomerId'), $this->session->userdata('CurrencyID'), $productId, $prodPrice, $quantity);
	} else {
		$prodPrice = ifBulkQuantity($productId, $prodPrice, $quantity);
		$prodPrice = convertPrice($prodPrice);
	}
	$prodPrice = ifProductInPromotion($productId, $prodPrice, $TODAY_DT_TM, $promoStartDate, $promoEndDate, $promoDiscount);
	return $prodPrice;
}

function ifProductInPromotion($productId, $prodPrice, $currDt, $stDt, $endDt, $discount, $objDBCD14)
{
	if (is_numeric($discount) && $stDt != '0000-00-00' && $endDt != '0000-00-00') {
		$arr = explode(' ', $currDt);
		if (isset($arr[0]) && $arr[0] != '') {
			$daysFromStDt = daysDiff($stDt, $arr[0]);
			$daysFromEndDt = daysDiff($endDt, $arr[0]);
			if ($daysFromStDt >= 0 && $daysFromEndDt <= 0) {
				$discountedPrice = convertPrice($discount);
				$prodPrice = $prodPrice - $discountedPrice;
				$prodPrice = number_format($prodPrice, 2, '.', '');
			}
		}
	}
	return $prodPrice;
}

function ifBulkQuantity($productId, $prodPrice, $quantity)
{
	$CI = &get_instance();
	$rsPrice = $CI->queryUniqueObject("SELECT Price FROM tbl_gf_product_bulk_prices WHERE ProductId = '$productId' AND MinQty <= '$quantity' AND MaxQty >= '$quantity'");
	if (isset($rsPrice->Price) && $rsPrice->Price != '') {
		$prodPrice = number_format($rsPrice->Price, 2, '.', '');
	}
	return $prodPrice;
}

function getProductGroupPrice($userId, $currencyId, $productId, $prodPrice, $quantity)
{
	$CI = &get_instance();
	$rsPackPrice = $CI->db->query("SELECT PackageId, Price FROM tbl_gf_customers A, tbl_gf_plans_packages_prices B WHERE A.PricePlanId = B.PlanId 
		AND B.CurrencyId = '$currencyId' AND PackageId = '$productId' AND ServiceType = '3' AND CustomerId = '$userId' AND PricePlanId = " . $this->session->userdata('EUserPriceGroupId') . " ");
	if (isset($rsPackPrice->Price) && $rsPackPrice->Price != '') {
		$prodPrice = number_format($rsPackPrice->Price, 2, '.', '');
	} else {
		$prodPrice = convertPrice($prodPrice);
	}
	$prodPrice = ifGroupBulkQuantity($productId, $prodPrice, $quantity);
	return $prodPrice;
}

function fetch_brand_logos()
{
	$CI = &get_instance();
	$result = $CI->db->query("SELECT BrandImage FROM tbl_gf_brand_logos ORDER BY Id DESC");
	return $result->result();
}

function fetch_reviews()
{
	$CI = &get_instance();
	$result = $CI->db->query("SELECT * FROM tbl_gf_reviews WHERE Enabled = 1 AND ReviewType = '0' ORDER BY ReviewId DESC LIMIT 0, 5");
	return $result->result();

}

function fetch_news()
{
	$CI = &get_instance();
	$result = $CI->db->query("SELECT NewsId, ShortDescription, SEOURLName, NewsTitle, DAY(AddedAt) AS NewsDate, MONTHNAME(AddedAt) AS NewsMon FROM tbl_gf_news 
    WHERE DisableNews = 0 AND NewsType = 0 AND AtWebsite = '1' ORDER BY NewsId DESC LIMIT 0, 3");
	return $result->result();
}

function convertPrice($price)
{
	if ($this->session->userdata('ConversionRate') && $this->session->userdata('ConversionRate') > 0)
		$price = $price * $this->session->userdata('ConversionRate');
	return roundMe($price);
}

function ifGroupBulkQuantity($productId, $prodPrice, $quantity, $objDBCD14)
{
	$CI = &get_instance();
	$rsPrice = $CI->db->query("SELECT Price FROM tbl_gf_product_bulk_group_prices WHERE ProductId = '$productId' AND GroupId = '" . $_SESSION['EUserPriceGroupId'] . "' 
	AND MinQty <= '$quantity' AND MaxQty >= '$quantity'");
	if (isset($rsPrice->Price) && $rsPrice->Price != '') {
		$prodPrice = number_format($rsPrice->Price, 2, '.', '');
	}
	return $prodPrice;
}

function sanitizeAndEscapingXXS($str)
{
	return strip_tags($str);
}

//ahmed
function decryptAPIKey($enc_text, $password, $iv_len)
{
	$enc_text = base64_decode($enc_text);
	$n = strlen($enc_text);
	$i = $iv_len;
	$key = '';
	$iv = substr($password ^ substr($enc_text, 0, $iv_len), 0, 512);
	while ($i < $n) {
		$block = substr($enc_text, $i, 16);
		$key .= $block ^ pack('H*', md5($iv));
		$iv = substr($block . $iv, 0, 512) ^ $password;
		$i += 16;
	}
	return preg_replace('/\\x13\\x00*$/', '', $key);
}

function isUserLoggedIn()
{
	$CI = &get_instance();
	if (!$CI->session->userdata('GSM_FUS_UserId')) {
		return false;
	} else {
		return true;
	}
}

function check_user_login()
{
	return isUserLoggedIn();
}

function check_login_user()
{
	checkAdminLoginState();
}

//Dynamically add Javascript files to header page
if (!function_exists('add_js')) {
	function add_js($file = '')
	{
		$str = '';
		$ci = &get_instance();
		$header_js = $ci->config->item('header_js');

		if (empty($file)) {
			return;
		}

		if (is_array($file)) {
			if (!is_array($file) && count($file) <= 0) {
				return;
			}
			foreach ($file AS $item) {
				$header_js[] = $item;
			}
			$ci->config->set_item('header_js', $header_js);
		} else {
			$str = $file;
			$header_js[] = $str;
			$ci->config->set_item('header_js', $header_js);
		}
	}
}

//Dynamically add CSS files to header page
if (!function_exists('add_css')) {
	function add_css($file = '')
	{
		$str = '';
		$ci = &get_instance();
		$header_css = $ci->config->item('header_css');

		if (empty($file)) {
			return;
		}

		if (is_array($file)) {
			if (!is_array($file) && count($file) <= 0) {
				return;
			}
			foreach ($file AS $item) {
				$header_css[] = $item;
			}
			$ci->config->set_item('header_css', $header_css);
		} else {
			$str = $file;
			$header_css[] = $str;
			$ci->config->set_item('header_css', $header_css);
		}
	}
}

if (!function_exists('put_headers')) {
	function put_headers()
	{
		$str = '';
		$ci = &get_instance();
		$header_css = $ci->config->item('header_css');
		if ($header_css)
			foreach ($header_css AS $item) {
				$str .= '<link rel="stylesheet" href="' . base_url() . $item . '" type="text/css" />' . "\n";
			}

		return $str;
	}
}

function get_cart($limit = 0)
{

	$ci = &get_instance();
	$user_id = $ci->session->userdata('GSM_FUS_UserId') ? $ci->session->userdata('GSM_FUS_UserId') : '0';
	return $ci->common_model->get_cart($user_id, '', 'DESC', $limit);
}

function getDefaultCurrency()
{
	$ci = &get_instance();
	$defaultCurrencyId = 0;
	$defaultCurrency = '';
	$row = $ci->Services_model->getRow('SELECT CurrencyId, CurrencyAbb FROM tbl_gf_currency WHERE DefaultCurrency = 1');
	if (isset($row->CurrencyId) && $row->CurrencyId != '') {
		$defaultCurrencyId = $row->CurrencyId;
		$defaultCurrency = $row->CurrencyAbb;
	}
	return $defaultCurrencyId . ',' . $defaultCurrency;
}

function getCurrencyData($currencyId)
{
	$ci = &get_instance();
	$data = '';
	$row = $ci->Services_model->getRow("SELECT CurrencyAbb, ConversionRate FROM tbl_gf_currency WHERE CurrencyId = '$currencyId'");
	if (isset($row->CurrencyAbb) && $row->CurrencyAbb != '')
	{
		$data = $row->CurrencyAbb.'|'.$row->ConversionRate;
	}
	return $data;
}
function getPackPricesInOtherCurrency($currencyId = 0, $serviceType = 0)
{
	$ci = &get_instance();
	$PACK_PRICES = array();
	switch($serviceType)
	{
		case '0': // IMEI services
			$tblName = 'tbl_gf_packages_currencies';
			break;
		case '1': // File services
			$tblName = 'tbl_gf_packages_currencies';
			break;
		case '2': // File services
			$tblName = 'tbl_gf_log_packages_currencies';
			break;
	}
	$rs = $ci->Services_model->selectData("SELECT PackageId, Price FROM $tblName WHERE CurrencyId = '$currencyId'");
	foreach($rs as $row)
	{
		$PACK_PRICES[$row->PackageId] = number_format($row->Price, 2, '.', '');
	}
	return $PACK_PRICES;
}


function createArrayFromXML($xml)
{ 
	
	$values = array(); 
	$index  = array(); 
	$array  = array(); 
	$parser = xml_parser_create(); 
	xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
	xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
	xml_parse_into_struct($parser, $xml, $values, $index);
	xml_parser_free($parser);

	$i = 0; 
	$name = $values[$i]['tag']; 
	$array[$name] = isset($values[$i]['attributes']) ? $values[$i]['attributes'] : ''; 
	$array[$name] = struct_to_array1($values, $i); 
	return $array; 
}

function struct_to_array1($values, &$i)
{
	$child = array(); 
	if (isset($values[$i]['value'])) array_push($child, $values[$i]['value']); 
	
	while ($i++ < count($values)) { 
		switch ($values[$i]['type']) { 
			case 'cdata': 
				array_push($child, $values[$i]['value']); 
			break; 
			
			case 'complete': 
				$name = $values[$i]['tag']; 
				if(!empty($name)){
					$child[$name]= ($values[$i]['value'])?($values[$i]['value']):''; 
					if(isset($values[$i]['attributes'])) {					
						$child[$name] = $values[$i]['attributes']; 
					} 
				}	
			break; 
			
			case 'open': 
				$name = $values[$i]['tag']; 
				$size = isset($child[$name]) ? sizeof($child[$name]) : 0;
				$child[$name][$size] = struct_to_array1($values, $i); 
			break;
			
			case 'close': 
				return $child; 
			break; 
		}
	}
	return $child; 
}



?>
