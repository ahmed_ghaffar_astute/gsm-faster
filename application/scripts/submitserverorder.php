<?php
if (isset($_POST['packageId']) && $this->session->userdata('GSM_FUS_UserId') && $this->session->userdata('ORDERED_FROM') == '1')
{
	$logPackageId = $this->input->post('packageId');
	$notes = $this->input->post('txtNotes');
	$comments = $this->input->post('txtComments');
	$PRE_ORDER =  $this->input->post('hdPreOrder') ?: '0';
	$packagePrice = 0;

	$apiId = 0;
	$apiType = 0;
	$apiKey = '';
	$serverURL = '';
	$accountId = '';
	$apiAction = '';
	$packCostPrice = 0;
	$DELAY_TIME = '1';
	$CRON_DELAY_TIME = '1';
	$apiName = '';
	$NEW_ORDER_EMAILS = '';
	$SERVICE_TYPE_ID = 0;
	$rwUNC = $this->User_model->fetch_negative_user_credits();
	$ALLOWNGTVCRDTS = $rwUNC->AllowNegativeCredits;
	$ODL = $rwUNC->OverdraftLimit;

	$rsAPI = $this->User_model->get_api_and_packages($logPackageId);
	if (isset($rsAPI->LogPackageTitle) && $rsAPI->LogPackageTitle != '')
	{
		$apiId = $rsAPI->APIId == '' || $rsAPI->APIId == '-1' ? 0 : $rsAPI->APIId;
		$apiType = $rsAPI->APIType == '' ? 0 : $rsAPI->APIType;
		$apiKey = $rsAPI->APIKey;
		$serverURL = $rsAPI->ServerURL;
		$accountId = $rsAPI->AccountId == '' ? 0 : $rsAPI->AccountId;
		$extNwkId = $rsAPI->ExternalNetworkId != '' ? $rsAPI->ExternalNetworkId : 0;
		$packageTitle = stripslashes($rsAPI->LogPackageTitle);
		$apiName = $rsAPI->APITitle;
		$SERVICE_TYPE_ID = $rsAPI->ServiceTypeId;
	}

	$rsPackage = $this->User_model->get_log_packages($logPackageId);
	if (isset($rsPackage->LogPackageTitle) && $rsPackage->LogPackageTitle != '')
	{
		$packageTitle = stripslashes($rsPackage->LogPackageTitle);
		$packCostPrice = $rsPackage->CostPrice == '' ? 0 : $rsPackage->CostPrice;
		$SERVICE_TYPE = $rsPackage->ServiceType; // 0 for DATABASE, 1 for RECALCULATE
		$DELAY_TIME = $rsPackage->ResponseDelayTm == 0 ? '1' : $rsPackage->ResponseDelayTm;
		$CRON_DELAY_TIME = $rsPackage->CronDelayTm == 0 ? '1' : $rsPackage->CronDelayTm;
		$NEW_ORDER_EMAILS = $rsPackage->NewOrderEmailIDs;
	}
	$USER_ID = $this->session->userdata('GSM_FUS_UserId');
	$ConversionRate=$this->data['userDetails']->ConversionRate;
	$packagePrice=getlogpackprice($USER_ID , $logPackageId , $this->data['userDetails']->CurrencyId , $myNetworkId , $ConversionRate);
	
	if($PACK_QUANTITY == 0)
		$this->data['message'] = 'Invalid value of Quantity, please enter a value greater than or equal to 1.';
	else
	{
		if($isQuantity && is_numeric($PACK_QUANTITY))
		{
			$bulkPrice = bulkQuantity_ServerService($logPackageId, $PACK_QUANTITY, $USER_ID);
			if($bulkPrice != '0')
				$packagePrice = $bulkPrice;
			$packagePrice = $packagePrice * $PACK_QUANTITY;
		}
		$finalCr = $this->data['myCredits'] - $packagePrice; 
		if($finalCr < 0)
		{
			if($ALLOWNGTVCRDTS == '0')
				$this->data['message'] = $this->lang->line('CUST_CODE_MSG2');
			else if($ALLOWNGTVCRDTS == '1')
			{
				$REMAINING_CREDITS = abs($finalCr);
				if($REMAINING_CREDITS <= $ODL)
				{
					// check limit
					include APPPATH.'scripts/serverordersubmission.php';
				}
				else
				{
					$this->data['message'] = 'Order failed. Your Overdraft Limit has been finished.';
				}
			}
		}
		else
		{
			include APPPATH.'scripts/serverordersubmission.php';
		}
	}
}
else
{
	echo "<script>window.location = 'dashboard.php'</script>";			
}
?>