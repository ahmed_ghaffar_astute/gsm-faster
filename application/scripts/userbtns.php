<?php
    $dUser = 0;
    $userName = '' ;
    $rwUN = get_users_by_id($USER_ID_BTNS);

	if (isset($rwUN->UserName) && $rwUN->UserName != '')
	{
		$userName = $rwUN->UserName;
		$dUser = $rwUN->DisableUser;
	}
?>
<style>
 .floating{
     float :left ;
 }
</style>
<div class="navbar navbar-light navbar-expand-xl rounded-top">
	<div class="d-xl-none">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-demo3-mobile">
			<i class="icon-grid3"></i>
		</button>
	</div>
	<div class="navbar-collapse collapse" id="navbar-demo3-mobile">
		<ul class="navbar-nav">
            <li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link dropdown-toggle <?php echo $this->data['CURRENT_PAGE'] == 'codes' || $this->data['CURRENT_PAGE'] == 'codesslbf' || $this->data['CURRENT_PAGE'] == 'logrequests' ? 'btn btn-primary' : 'btn btn-default'; ?>" data-toggle="dropdown" aria-expanded="false">
                    <?php echo $this->lang->line('BE_DB_ICON2'); ?>
                </a>
				<div class="dropdown-menu dropdown-menu-left">
                    <a class="dropdown-item" href="<?php echo base_url('admin/services/codes?uId='.$USER_ID_BTNS); ?>"><?php echo $this->lang->line('BE_LBL_246'); ?></a>
                    <a class="dropdown-item" href="<?php echo base_url('admin/services/codesslbf?uId='.$USER_ID_BTNS);?>"><?php echo $this->lang->line('BE_LBL_249'); ?></a>
                    <a class="dropdown-item" href="<?php echo base_url('admin/services/logrequests?uId='.$USER_ID_BTNS);?>"><?php echo $this->lang->line('BE_LBL_252'); ?></a>
				</div>
			</li>
            <?php if (array_key_exists('327', $this->data['ARR_ADMIN_FORMS'])) { ?>
                <li class="nav-item <?php echo $this->data['CURRENT_PAGE'] == 'overview' ? 'active' : ''; ?>">
                    <a href="<?php echo base_url('admin/services/overview?id='.$USER_ID_BTNS);?>" class="navbar-nav-link">
                        <?php echo $this->lang->line('BE_LBL_324'); ?>
                    </a>
                </li>
            <?php } ?>

            <li class="nav-item <?php echo $this->data['CURRENT_PAGE'] == 'user' ? 'active' : ''; ?>">
                <a href="<?php echo base_url();?>admin/clients/user?id=<?php echo $USER_ID_BTNS; ?>" class="navbar-nav-link">
                    <?php echo $this->lang->line('BE_LBL_310'); ?>
                </a>
            </li>

            <?php if (array_key_exists('330', $this->data['ARR_ADMIN_FORMS'])) { ?>
                <li class="nav-item <?php echo $this->data['CURRENT_PAGE'] == 'addcredits' ? 'active' : ''; ?>">
                    <a href="<?php echo base_url();?>admin/clients/addcredits?userId=<?php echo $USER_ID_BTNS; ?>" class="navbar-nav-link">
                        <?php echo $this->lang->line('BE_LBL_318'); ?>
                    </a>
                </li>
            <?php } ?>

            <li class="nav-item dropdown">
                <a class="navbar-nav-link dropdown-toggle <?php echo $this->data['CURRENT_PAGE'] == 'packagesprices' || $this->data['CURRENT_PAGE'] == 'resetpackprices' || $this->data['CURRENT_PAGE'] == 'set_packs_for_user' ? 'active' : ''; ?> " data-toggle="dropdown" aria-expanded="false">
                    <?php echo $this->lang->line('BE_LBL_534'); ?>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="<?php echo base_url('admin/services/userpackageprices?userId='.$USER_ID_BTNS);?>"><?php echo $this->lang->line('BE_USR_11'); ?></a>
                    <a class="dropdown-item" href="<?php echo base_url('admin/services/userpackageprices?sc=1&userId='.$USER_ID_BTNS);?>"><?php echo $this->lang->line('BE_USR_222'); ?></a>
                    <a class="dropdown-item" href="<?php echo base_url('admin/services/userpackageprices?sc=2&userId='.$USER_ID_BTNS);?>"><?php echo $this->lang->line('BE_USR_12'); ?></a>
                    <a class="dropdown-item" href="<?php echo base_url('admin/services/resetpackprices?userId='.$USER_ID_BTNS);?>"><?php echo $this->lang->line('BE_LBL_408'); ?></a>
                    <a class="dropdown-item" href="<?php echo base_url('admin/services/resetpackprices?sc=1&userId='.$USER_ID_BTNS);?>"><?php echo $this->lang->line('BE_LBL_409'); ?></a>
                    <a class="dropdown-item" href="<?php echo base_url('admin/services/resetpackprices?sc=2&userId='.$USER_ID_BTNS);?>"><?php echo $this->lang->line('BE_LBL_410'); ?></a>
                    <a class="dropdown-item" href="<?php echo base_url('admin/services/set_packs_for_user?userId='.$USER_ID_BTNS);?>"><?php echo $this->lang->line('BE_LBL_312'); ?></a>
                    <a class="dropdown-item" href="<?php echo base_url('admin/services/set_packs_for_user?sc=1&userId='.$USER_ID_BTNS);?>"><?php echo $this->lang->line('BE_LBL_313'); ?></a>
                    <a class="dropdown-item" href="<?php echo base_url('admin/services/set_packs_for_user?sc=2&userId='.$USER_ID_BTNS);?>"><?php echo $this->lang->line('BE_LBL_314'); ?></a>
                </div>
            </li>

            <?php if (array_key_exists('331', $this->data['ARR_ADMIN_FORMS'])) { ?>
                <li class="nav-item <?php echo $this->data['CURRENT_PAGE'] == 'userapi' ? 'active' : ''; ?>" >
                    <a href="<?php echo base_url();?>admin/clients/userapi?userId=<?php echo $USER_ID_BTNS;?>" class="navbar-nav-link"><?php echo $this->lang->line('BE_PCK_15'); ?></a>
                </li>
            <?php } ?>

            <li class="nav-item <?php echo $this->data['CURRENT_PAGE'] == 'clientiplogrpt' ? 'active' : ''; ?>">
                <a href="<?php echo base_url();?>admin/reports/clientiplogrpt?userId=<?php echo $USER_ID_BTNS;?>" class="navbar-nav-link">
                    <?php echo $this->lang->line('BE_LBL_319'); ?>
                </a>
            </li>

            <?php if (array_key_exists('329',$this->data['ARR_ADMIN_FORMS'])) { ?>
                <li  class="nav-item <?php echo $this->data['CURRENT_PAGE'] == 'credithistory' ? 'active' : ''; ?>">
                    <a class="navbar-nav-link" href="<?php echo base_url();?>admin/clients/credithistory?userId=<?php echo $USER_ID_BTNS;?>">
                        <?php echo $this->lang->line('BE_LBL_320'); ?>
                    </a>
                </li>
            <?php } ?>


            <li class="nav-item dropdown">
                <a href="" class="navbar-nav-link dropdown-toggle <?php echo $this->data['CURRENT_PAGE'] == 'payments' ? 'active' : ''; ?>" data-toggle="dropdown" aria-expanded="false"><?php echo $this->lang->line('BE_LBL_315'); ?></a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="<?php echo base_url('admin/clients/payments?byAdmin=1&userId='.$USER_ID_BTNS);?>"><?php echo $this->lang->line('BE_LBL_316');?></a>
                    <a class="dropdown-item" href="<?php echo base_url('admin/clients/payments?byAdmin=0&userId='.$USER_ID_BTNS);?>"><?php echo $this->lang->line('BE_LBL_317'); ?></a>
                </div>
            </li>
		</ul>
	</div>
</div>
    <div class="page-header-content header-elements-md-inline">
        <div style="padding-top: 10px !important;" class="page-title d-flex">
            <h4>
                <?php echo $this->lang->line('BE_USR_HD'); ?>:</b> <?php echo $userName; ?>
            </h4>
            <?php if($this->data['CURRENT_PAGE'] == 'user') { ?>
                &nbsp;&nbsp;&nbsp;
                <input type="hidden" name="hdUStatus" id="hdUStatus" value="<?php echo $dUser; ?>" />
                <div class="form-group row">
                    <div class="col-lg-12">
                        <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" id="btnEnDis">
                            <font id="fntStatus" name="fntStatus">
                                <?php echo $dUser == '1' ? $this->lang->line('BE_LBL_744') : $this->lang->line('BE_GNRL_1'); ?>                             
                                <?php echo ' '.$userName; ?>
                            </font>
                        </button>
                    </div>
                </div>
                
                <img style="display:none;" id="statusULdr" src="<?php echo base_url('assets/images/loading.gif');?>" border="0" alt="Please wait..." />
            <?php } ?>
        </div>
    </div>
