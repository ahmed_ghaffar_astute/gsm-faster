<?php
if (isset($_POST['packageId']) && $this->session->userdata('GSM_FUS_UserId') && $this->session->userdata('ORDERED_FROM') == '1')
{
	if($this->input->server('HTTP_HOST') != 'localhost')
    {
		if(!checkCountryRestriction($this->session->userdata('GSM_FUS_UserId'), $this->session->userdata('UserEmail'), $this->session->userdata('UserName'), 'File'))
		{
			redirect(base_url('page/404'));
		}
	}
	
	require(APPPATH.'libraries/FileManager.php');
	$filemanager = new FileManager();
	$FILES_ARR = array();
	$packagePrice = 0;
	$apiId = 0;
	$apiKey = '';
	$apiType = '';
	$serverURL = '';
	$accountId = '';
	$placeOrder = true;
	$packCostPrice = 0;
	$DELAY_TIME = '1';
	$CRON_DELAY_TIME = '1';
	$NEW_ORDER_EMAILS = '';
	$apiName = '';
	$rsPackTitle = $this->User_model->fetch_package_by_id($myNetworkId);
	if (isset($rsPackTitle->PackageTitle) && $rsPackTitle->PackageTitle != '')
	{
		$packageTitle = $rsPackTitle->PackageTitle;
		$packCostPrice = $rsPackTitle->CostPrice == '' ? 0 : $rsPackTitle->CostPrice;
		$SERVICE_TYPE = $rsPackTitle->ServiceType; // 0 for DATABASE, 1 for RECALCULATE
		$DELAY_TIME = $rsPackTitle->ResponseDelayTm == 0 ? '1' : $rsPackTitle->ResponseDelayTm;
		$CRON_DELAY_TIME = $rsPackTitle->CronDelayTm == 0 ? '1' : $rsPackTitle->CronDelayTm;
		$NEW_ORDER_EMAILS = $rsPackTitle->NewOrderEmailIDs;
	}

	$rsPackage = $this->User_model->get_packages_api_by_id($myNetworkId);
	
	if (isset($rsPackage->APIId) && $rsPackage->APIId != '')
	{
		$apiId = $rsPackage->APIId == '-1' ? '0' : $rsPackage->APIId;
		$apiKey = $rsPackage->APIKey;
		$apiType = $rsPackage->APIType;
		$serverURL = $rsPackage->ServerURL;
		$accountId = $rsPackage->AccountId;
		$extNwkId = $rsPackage->ExternalNetworkId;
		$apiName = $rsPackage->APITitle;
		$apiKey2 = $rsPackage->APIKey2;
		$apiPwd = $rsPackage->APIPassword;
	}
	$rdFileType = 0;
	//=========================================== ZIP FILE =========================================//
	if($rdFileType == '0')
	{
		if($filemanager->getFileName('txtFile') != '')
		{
			$fileExt = $filemanager->getFileExtension("txtFile");
			if($fileExt == '.log' || $fileExt == '.fnx' || $fileExt == '.bcl' || $fileExt == '.txt' || $fileExt == '.sha' || $fileExt == '.ask')
			{			
				$ORDER_FILENAME = 'slbf_files/'.$filemanager->getFileName('txtFile');
				
				if(InStr($ORDER_FILENAME, 'php'))
				{ 
					$this->data['message'] = $this->lang->line('CUST_LBL_182');
					$placeOrder = false;
				}
				else
				{
					$FILES_ARR[0] = $filemanager->getFileName('txtFile');
				}
			}
			else
			{
				$this->data['message'] = $this->lang->line('CUST_LBL_182');
				$placeOrder = false;
			}
		}
	}
	if($placeOrder)
	{
		$USER_ID = $this->session->userdata('GSM_FUS_UserId');
	
		$packagePrice = getpackprice($myNetworkId , $this->data['userDetails']->CurrencyId , $IMEI_TYPE);
		$totalFiles = sizeof($FILES_ARR);

		$rsPackage = $this->User_model->get_dupImeis($myNetworkId);
		$allowDupIMEIs = $rsPackage->DuplicateIMEIsNotAllowed;
		
		$strInsert = ", ShowToSupplier = '0'";
		
		list($suppPackId, $suppPurchasePrice, $supplierId) = ifPackIdAssignedToSupplier($myNetworkId, 1);
		
		if($suppPackId != '0')
			$strInsert = ", ShowToSupplier = '1'";
		if($suppPurchasePrice != '0')
			$strInsert .= ", Payout = '$suppPurchasePrice'";
		if($supplierId != '0')
			$strInsert .= ", SupplierId = '$supplierId'";
		
		$rwUNC = $this->User_model->get_negative_credits();
		
		$ALLOWNGTVCRDTS = $rwUNC->AllowNegativeCredits;
		$ODL = $rwUNC->OverdraftLimit;
		
		for($j = 0; $j < $totalFiles; $j++)
		{
			$finalCr = $this->data['myCredits'] - $packagePrice;
			if($finalCr < 0)
			{
				if($ALLOWNGTVCRDTS == '0'){
					$this->data['message'] = $this->lang->line('CUST_CODE_MSG2');
				}			
				else if($ALLOWNGTVCRDTS == '1')
				{
					$REMAINING_CREDITS = abs($finalCr);
					if($REMAINING_CREDITS <= $ODL)
					{
						include APPPATH.'scripts/fileordersubmission.php';
					}
					else
					{
						$this->data['message'] = 'Order failed. Your Overdraft Limit has been finished.';
					}
				}
			}
			else
			{
				include APPPATH.'scripts/fileordersubmission.php';
			}
		}
	}
}
else
{
	redirect(base_url('dashboard'));		
}
?>