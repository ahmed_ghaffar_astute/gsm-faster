<?php
	$apiId = $this->input->post('apiId') ?: 0;
	$supplierPackId = $this->input->post('supplierPackId') ?: 0;

	$selectedOrders = '0';
	for($i = 0; $i < $totalCodes; $i++)
	{
		$data = explode('|', $codeIds[$i]);
		$selectedOrders .= ','.$data[0];
	}
	$submit = true;
	if($selectedOrders == '0')
	{
		$errorMsg = $this->lang->line('BE_LBL_577');
		$submit = false;
	}
	if($apiId == '0')
	{
		$errorMsg .=  $errorMsg != '' ? '<br />' : '';
		$errorMsg .= 'Please select API';
		$submit = false;
	}
	if($supplierPackId == '0')
	{
		$errorMsg .=  $errorMsg != '' ? '<br />' : '';
		$errorMsg .= 'Please select supplier service';
		$submit = false;
	}
	if($submit)
	{
		switch($sc)
		{
			case '0';
				$tblName = 'tbl_gf_codes';
				$statusCol = 'CodeStatusId';
				$serviceCol = 'PackageId';
				$tblPack = 'tbl_gf_packages';
				$packCol = 'PackageTitle';
				$idCol = 'CodeId';
				$imeiCol = 'IMEINo';
				break;
			case '1';
				$tblName = 'tbl_gf_codes_slbf';
				$statusCol = 'CodeStatusId';
				$serviceCol = 'PackageId';
				$tblPack = 'tbl_gf_packages';
				$packCol = 'PackageTitle';
				$idCol = 'CodeId';
				$imeiCol = 'IMEINo';
				break;
			case '2';
				$tblName = 'tbl_gf_log_requests';
				$statusCol = 'StatusId';
				$serviceCol = 'LogPackageId';
				$tblPack = 'tbl_gf_log_packages';
				$packCol = 'LogPackageTitle';
				$idCol = 'LogrequestId';
				$imeiCol = 'serialno_U4L6';
				break;
			case '3';
				$tblName = 'tbl_gf_retail_orders';
				$statusCol = 'OrderStatusId';
				$serviceCol = 'PackageId';
				$tblPack = 'tbl_gf_retail_services';
				$packCol = 'PackageTitle';
				$idCol = 'RetailOrderId';
				$imeiCol = 'IMEINo';
				break;
			case '4';
				$tblName = 'tbl_gf_retail_orders';
				$statusCol = 'OrderStatusId';
				$serviceCol = 'PackageId';
				$tblPack = 'tbl_gf_retail_services';
				$packCol = 'PackageTitle';
				$idCol = 'RetailOrderId';
				$imeiCol = 'serialno_U4L6';
				break;
		}
		$rsAPI = $this->Services_model->fetch_api_data_by_id($apiId); 
		
		if (isset($rsAPI->APIId) && $rsAPI->APIId != '')
		{
			$apiType = $rsAPI->APIType;
			$apiKey = $rsAPI->APIKey;
			$serverURL = $rsAPI->ServerURL;
			$apiUserName = $rsAPI->AccountId;
			$apiName = stripslashes($rsAPI->APITitle);

			//======================================== LINKING ORDERS ==============================================//
			$LINKED_IMEIS = '';
			for($i = 0; $i < $totalCodes; $i++)
			{
				$data = explode('|', $codeIds[$i]);
				$ORDER_ID = $data[0];
				$IMEI = $data[3];
				$SERVICE_ID = $data[5];
				$ORDER_TYPE = 0;
				if($sc == '0' || $sc == '3')
					$ORDER_TYPE = $data[6];
				else if($sc == '1')
					$ORDER_TYPE = $data[7];

				if($ORDER_TYPE == '0')
				{
					$rwCode = $this->Services_model->select_general_query($idCol ,$tblName , $imeiCol , $IMEI, $serviceCol , $SERVICE_ID , $statusCol ,$idCol , $ORDER_ID); 
					
					if(isset($rwCode->$idCol) && $rwCode->$idCol != '')
					{
						if($ORDER_ID != $rwCode->$idCol)
						{
							$this->Services_model->update_general_query($tblName , $statusCol , $rwCode->$idCol ,$idCol ,$ORDER_ID);
							
							if($LINKED_IMEIS == '')
								$LINKED_IMEIS = $IMEI;
							else
								$LINKED_IMEIS .= ', '.$IMEI;
						}
					}
					else
					{
						$this->Services_model->update_query($tblName ,$apiId , $serverURL ,$apiUserName ,$apiType ,$apiKey,$supplierPackId , $apiName ,$idCol ,$ORDER_ID);
						
					}
				}
				else
				{
					$this->Services_model->update_query($tblName ,$apiId , $serverURL ,$apiUserName ,$apiType ,$apiKey,$supplierPackId , $apiName ,$idCol ,$ORDER_ID);

				}
			}
			//======================================== LINKING ORDERS ==============================================//
			//=================================== CREATING API HISTORY ======================================//
			if($apiId > 0)
			{
				$currDtTm = setDtTmWRTYourCountry();
				serviceAPIHistory($packId, $apiId, $supplierPackId, $currDtTm, $sc);
			}
			//=================================== CREATING API HISTORY ======================================//
			$message = 'API has been attached successfully!';
			if($LINKED_IMEIS != '')
				$errorMsg = 'The following IMEIs already exist and marked as linked '.$LINKED_IMEIS;
		}
	}
?>