<?
if (isset($_POST['imeiFType']) && $CI->session->userdata('GSM_FUS_UserId') && $CALLED_FROM_ORDER_PAGE == '1' && $CI->session->userdata('ORDERED_FROM') == '1')
{
	$PLACED_ORDERS_COUNT++;
	if($strInsertOrders != '')
	{
		$strInsertOrders .= ',';
	}
	if($strCreditHistory != '')
	{
		$strCreditHistory .= ',';
	}
	$keys=crypt_key($CI->session->userdata('GSM_FUS_UserId'));
	$encryptedCredits = encrypt($finalCr , $keys); 
	$profit = 0;
	$packPriceToDefaultCurr = 0;
	$orderType = $SERVICE_TYPE;		
	//========================================== Calculate Profit ===================================================//
	if($packCostPrice != '0')
	{
		$packPriceToDefaultCurr = roundMe($packagePrice/$CONVERSION_RATE); //Convert local Currency into default currency as per currency rate
		$profit = $packPriceToDefaultCurr - $packCostPrice;
	}
	//========================================== Calculate Profit ===================================================//
	
	//========================================== CHECK IMEI SERIES ===================================================//
	if($IMEI_SERIES != '')
	{
		if(ifIMEIRestricted($myIMEI, $IMEI_SERIES))
		{
			$orderType = '2';		
		}
	}
	//========================================== CHECK IMEI SERIES ===================================================//
	$strHistData = $myIMEI.$historyData;
	
	$strInsertOrders .= "('$personalRecord', '1', '".$CI->session->userdata('GSM_FUS_UserId')."', '$packagePrice', '$phoneLockedOn', '$myIMEI', '$myNetworkId', '$notes', '$comments', 
	'$countryId', '$currDtTm', '$altEmail', '".$CI->session->userdata['CLIENT_GF_IP']."', '".$data['apiId']."', '$apiName', '$serverURL', '$accountId', '$apiType', '$apiKey', '$extNwkId', '0', '0', 
	'$strInsert', '$suppPurchasePrice', '$PRE_ORDER', '$supplierId', '$orderType', '$DELAY_TIME', '$packCostPrice', '$profit', '$packPriceToDefaultCurr', 
	'$CRON_DELAY_TIME', '$mobileId', '$modelId', '$modelValueToShow', '$strHistData', '".$CI->session->userdata('ORDERED_FROM')."' $vals_cfields)";
	
	$strCreditHistory .= "('".$CI->session->userdata('GSM_FUS_UserId')."', '$packagePrice', 'New IMEI Order for $packageTitle', '$strHistData', 
	'$myNetworkId', '$currDtTm', '$encryptedCredits', '$myCurrency', '".$CI->session->userdata['CLIENT_GF_IP']."')";
	
	if($CI->data['settings']->SendNewIMEIOrderEmail == '1')
	{
		$strEmailContents .= $myIMEI.'<br />';
	}
	$myCredits = $finalCr;
}
else
{
	redirect(base_url('dashboard'));		
}
?>