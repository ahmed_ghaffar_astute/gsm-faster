<?
if ($this->input->post('packageId') && $this->session->userdata('GSM_FUS_UserId')  && $this->session->userdata('ORDERED_FROM') == '1')
{
	if($this->input->server('HTTP_HOST') != 'localhost')
	{
		if(!checkCountryRestriction($this->session->userdata('GSM_FUS_UserId'), $this->session->userdata('UserEmail'), $this->session->userdata('UserName'), 'Server'))
		{
			redirect(base_url('page/404'));
		}
	}
	$this->crypt_key($this->session->userdata('GSM_FUS_UserId'));
	$encryptedCredits = $this->encrypt($finalCr); 
	
	$currDtTm = setDtTmWRTYourCountry();
	
	$strInsert = ", ShowToSupplier = '0'";
	list($suppPackId, $suppPurchasePrice, $supplierId) = ifPackIdAssignedToSupplier($myNetworkId, 2);
	if($suppPackId != '0')
		$strInsert = ", ShowToSupplier = '1'";
	if($suppPurchasePrice != '0')
		$strInsert .= ", Payout = '$suppPurchasePrice'";
	if($supplierId != '0')
		$strInsert .= ", SupplierId = '$supplierId'";
	
	$profit = 0;
	$packPriceToDefaultCurr = 0;
	$serial = '' ;
	$boxUN = '';
	$uName = '';
	//========================================== Calculate Profit ===================================================//
	if($packCostPrice != '0')
	{
		$packPriceToDefaultCurr = roundMe($packagePrice/$this->data['userDetails']->ConversionRate); //Convert local Currency into default currency as per currency rate
		if($PACK_QUANTITY > 1)
		{
			$profit = $packPriceToDefaultCurr - $packCostPrice * $PACK_QUANTITY;
			$packCostPrice = $packCostPrice * $PACK_QUANTITY;
		}
		else 
			$profit = $packPriceToDefaultCurr - $packCostPrice;
	}
	//========================================== Calculate Profit ===================================================//
	$myID = $this->User_model->insert_log_data($serial , $boxUN , $uName , $packagePrice ,$logPackageId,$notes ,$comments,$currDtTm, $altEmail,$apiId,$serverURL, $apiName, $historyData , $accountId, $apiType ,$apiKey,$extNwkId,$PRE_ORDER,$packCostPrice,$profit,$SERVICE_TYPE,$DELAY_TIME,$packPriceToDefaultCurr,$CRON_DELAY_TIME,$SERVICE_TYPE_ID,$strInsert,$col_val);

	$insert_data = array(
		'UserId' => $this->session->userdata('GSM_FUS_UserId'),
		'Credits' => $packagePrice ,
		'LogRequestId' => $myID,
		'Description' => 'New Server Order for '.$packageTitle,
		'HistoryDtTm' => $currDtTm,
		'CreditsLeft' => $encryptedCredits,
		'IMEINo' => $historyData,
		'Currency' => $this->data['userDetails']->CurrencyAbb,
		'OType' => 2 ,
		'IP' => $this->session->userdata('CLIENT_GF_IP'),
	);

	$this->User_model->insert_credit_history($insert_data);
	
	$this->User_model->update_user_credits($encryptedCredits);

	
	$this->data['message'] = $this->lang->line('CUST_LBL_185');
	$SRVR_ORDER_EMAIL=$this->data['userDetails']->SendNewServerOrderEmail;
	if($SRVR_ORDER_EMAIL == '1')
	{
		newServerOrderEmail($this->session->userdata('UserEmail'), $this->session->userdata('UserName'), $packageTitle, $historyData, $this->session->userdata('CLIENT_GF_IP'), $packagePrice, $finalCr,
		$currDtTm, $myNetworkId, $altEmail, $serial, $NEW_ORDER_EMAILS, $notes);
	}
	$myCredits = $finalCr;
	redirect(base_url('page/server_services?success=1'));
			
}
else
{
	redirect(base_url('dashboard'));	
}
?>