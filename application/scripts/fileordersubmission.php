<?
if (isset($_POST['packageId']) && $this->session->userdata('GSM_FUS_UserId') && $this->session->userdata('ORDERED_FROM') == '1')
{
	$allowSubmission = true;
	$hash = '';
	if($allowSubmission)
	{
		$filemanager->uploadAs('txtFile', $ORDER_FILENAME);
		$arrFName = explode('.', $FILES_ARR[$j]);
		$arrSize = sizeof($arrFName);
		$fileExt = $arrFName[$arrSize-1];
		$fileName = str_replace(".$fileExt", '', $FILES_ARR[$j]);
		$file = FCPATH."slbf_files/$fileName.$fileExt";
		if($fileExt != '')
		{
			if($fileExt == 'ask')
			{
				$file_handle = fopen($file, "rb");
				$i = 0;
				$haveIMEI = true;
				while (!feof($file_handle))
				{
					$line_of_text = fgets($file_handle);
					if($i == 0)
					{
						if(!InStr($line_of_text, 'IMEI1'))
						{
							$haveIMEI = false;
						}
					}
					if($haveIMEI)
					{
						if($i == 0)
						{
							$parts = explode('=', $line_of_text);
							if($parts[0] == 'IMEI1')
								$imei = trim($parts[1]);
						}
						else if($i == 7)
						{
							$hash = str_replace('RAPx_HASH:', '', $line_of_text);
						}
					}
					else
					{
						if($i == 0)
						{
							$imei = substr($line_of_text, 0, 15);
							$hash = '';
						}
					}
					$i++;
				}
				fclose($file_handle);
			}
			else
			{
				if($fileExt != 'sha')
				{
					$file_handle = fopen($file, "rb");
					$i = 0;
					$FILE_TYPE = 'other';
					while (!feof($file_handle))
					{
						$line_of_text = fgets($file_handle);
						if($i == 0)
						{
							if(InStr(strtolower($line_of_text), '[log]'))
								$FILE_TYPE = 'other';
							else
								$FILE_TYPE = 'txt';
							$imei = trim($line_of_text);
						}
						if($FILE_TYPE == 'txt')
						{
							if($i == 0)
								$imei = trim($line_of_text);
							else if($i == 1)
								$hash = trim($line_of_text);
						}
						else
						{
							$parts = explode('=', $line_of_text);
							if(strtolower($parts[0]) == 'imei')
								$imei = trim($parts[1]);
							else if(strtolower($parts[0]) == 'hash')
								$hash = trim($parts[1]);
						}
						$i++;
					}
					fclose($file_handle);
				}
				else
				{
					$imei = $fileName;
				}
			}
		}
		$this->crypt_key($this->session->userdata('GSM_FUS_UserId'));
		$encryptedCredits = $this->encrypt($finalCr);
	
		$currDtTm = setDtTmWRTYourCountry();
		$fileContents = file_get_contents($file);
		$profit = 0;
		$packPriceToDefaultCurr = 0;
		//========================================== Calculate Profit ===================================================//
		if($packCostPrice != '0')
		{
			$packPriceToDefaultCurr = roundMe($packagePrice/$CONVERSION_RATE); //Convert local Currency into default currency as per currency rate
			$profit = $packPriceToDefaultCurr - $packCostPrice;
		}
		//========================================== Calculate Profit ===================================================//
		$myID = $this->User_model->insert_codes_slbf_data($hash,$packagePrice ,$imei, $file , $myNetworkId , $notes ,$comments , $currDtTm ,$altEmail , $apiId , $serverURL, $accountId, $apiName, $apiType ,$apiKey, $extNwkId , $fileContents ,$SERVICE_TYPE , $DELAY_TIME , $packCostPrice , $profit , $CRON_DELAY_TIME , $packPriceToDefaultCurr ,$apiKey2 ,$apiPwd,$strInsert);
		
	    $this->User_model->update_userdata($encryptedCredits);

		$insert_data = array(
			'UserId' => $this->session->userdata('GSM_FUS_UserId'),
			'Credits' => $packagePrice,
			'LogRequestId' => $myID,
			'OType' => 1,
			'Description' => 'New File Order for '.$packageTitle,
			'IMEINo' => $imei,
			'HistoryDtTm' => $currDtTm ,
			'CreditsLeft' => $encryptedCredits,
			'Currency' => $this->data['userDetails']->CurrencyAbb,
			'IP' => $this->session->userdata('CLIENT_GF_IP')
		);

		$this->User_model->insert_credit_history($insert_data);
		$FILE_ORDER_EMAIL = $this->data['settings']->SendNewFileOrderEmail;
		if($FILE_ORDER_EMAIL == '1')
		{
			newFileOrderEmail($this->session->userdata('UserEmail'), $this->session->userdata('UserName'), $packageTitle, $imei, $this->session->userdata('CLIENT_GF_IP'), $packagePrice, $finalCr, $currDtTm, $myNetworkId, $altEmail, $hash, $NEW_ORDER_EMAILS, $notes);
		}
		$myCredits = $finalCr;
		$comments = '';
		$notes = '';
		$timeTaken = '';
		$this->data['mustRead']  = '';
		redirect(base_url('page/file_services?success=1'));
	}
}
else
{
	redirect(base_url('dashboard'));		
}
?>