<?php
$row = $this->User_model->get_api_data_by_payment_id($PAYMENT_METHOD_ID);
if (isset($row->APIUsername) && $row->APIUsername != '')
{
	$API_USERNAME = stripslashes($row->APIUsername);
	$API_PASSWORD = stripslashes($row->APIPassword);
}

$bFName = $this->input->post('txtFName') ?: '';
$bLName =$this->input->post('txtLName') ?: '';
$bEmail = $this->input->post('txtEmail') ?: '';
$bPhone = $this->input->post('txtPhone') ?: '';
$addressCC = $this->input->post('txtAddress') ?: '';
$stateCC = $this->input->post('txtState') ?: '';
$cityCC = $this->input->post('txtCity') ?: '';
$zipCC = $this->input->post('txtZip') ?: '';
$countryISO = $this->input->post('country') ?: '';
// Set up your API credentials, PayPal end point, and API version.

$update_data = array(
	'BillingFName' => $bFName,
	'BillingLName'=> $bLName,
	'BillingEmail' => $bEmail,
	'BillingPhone' => $bPhone,
	'BillingAddress' => $addressCC,
	'BillingState' => $stateCC,
	'BillingCity' => $cityCC,
	'BillingZip' => $zipCC,
	'BillingCountry' => $countryISO,
);

$this->User_model->update_gf_payments($update_data,$invId);

include_once APPPATH.'libraries/NetworkonlieBitmapPaymentIntegration.php';
$networkOnlineArray = array('Network_Online_setting' => array(
											
											'merchantKey'    => $API_PASSWORD, 	  	   	    	// The Network Online Test
											'merchantId'     => $API_USERNAME,
											'collaboratorId' => 'NI',	           	 // Constant used by Network Online international
											//'collaboratorId' => 'ADCB',
											'iv' 			 => '0123456789abcdef', // Used for initializing CBC encryption mode
											'url'	         => false              // Set to false if you are using testing environment , set to true if you are using live environment
								),
								'Block_Existence_Indicator' => array(
											'transactionDataBlock' => true,
											'billingDataBlock' 	   => true,
											'shippingDataBlock'    => false,
											'paymentDataBlock'     => true,
											'merchantDataBlock'    => false,
											'otherDataBlock'       => true,
											'DCCDataBlock' 		   => false
								),
								'Field_Existence_Indicator_Transaction' => array(
											'merchantOrderNumber'  => $invId, 
											'amount'  			   => $INV_AMOUNT,
											'successUrl'           => $BASE_URL."bitmap_ipn.php?invId=".$invId,
											'failureUrl'           => $BASE_URL."bitmap_ipn.php?invId=".$invId,
											'transactionMode'      => 'INTERNET',
											'payModeType'          => '',
											'transactionType'      => '01',
											'currency'             => $INV_CURRENCY
											//'currency'             => 'USD'
								),
								'Field_Existence_Indicator_Billing' => array(
											'billToFirstName'       => $bFName, 
											'billToLastName'        => $bLName,
											'billToStreet1'         => $addressCC,
											'billToStreet2'         => $addressCC,
											'billToCity'       	    => $cityCC,
											'billToState'      	    => $stateCC,
											'billtoPostalCode'      => $zipCC,
											'billToCountry'         => $countryISO,
											'billToEmail'           => $bEmail,
											'billToMobileNumber'    => $bPhone,
											'billToPhoneNumber1'    => '',
											'billToPhoneNumber2'    => '',
											'billToPhoneNumber3'    => ''
								),
								'Field_Existence_Indicator_Shipping' => array(
											'shipToFirstName'    => 'FutueLink Trading', 
											'shipToLastName'     => 'FZE', 
											'shipToStreet1'      => 'Office # C1 -514 D', 
											'shipToStreet2'      => 'parkstreet', 
											'shipToCity'         => 'Ajman',
											'shipToState'        => 'Ajman ',
											'shipToPostalCode'   => '40395',
											'shipToCountry'      => 'AE',
											'shipToPhoneNumber1' => '971582927878',
											'shipToPhoneNumber2' => '',
											'shipToPhoneNumber3' => '',
											'shipToMobileNumber' => '971582927878'
								),
								'Field_Existence_Indicator_Payment' => array(
											'cardNumber'  	  => '4111111111111111', // 1. Card Number  
											'expMonth'  	  => '08', 				 // 2. Expiry Month 
											'expYear'  		  => '2020',             // 3. Expiry Year
											'CVV'  			  => '123',              // 4. CVV  
											'cardHolderName'  => 'Soloman',          // 5. Card Holder Name 
											'cardType'  	  => 'Visa',             // 6. Card Type
											'custMobileNumber'=> '9820998209',       // 7. Customer Mobile Number
											'paymentID' 	  => '123456',           // 8. Payment ID 
											'OTP'  			  => '123456',           // 9. OTP field 
											'gatewayID'  	  => '1026',             // 10.Gateway ID 
											'cardToken'   	  => '1202'              // 11.Card Token 
								),
								'Field_Existence_Indicator_Merchant'  => array(
													'UDF1'   => '115.121.181.112', // This is a ‘user-defined field’ that can be used to send additional information about the transaction.
													'UDF2'   => 'abc', 			   // This is a ‘user-defined field’ that can be used to send additional information about the transaction.
													'UDF3'   => 'abc',             // This is a ‘user-defined field’ that can be used to send additional information about the transaction.
													'UDF4'   => 'abc',             // This is a ‘user-defined field’ that can be used to send additional information about the transaction.
													'UDF5'   => 'abc',             // This is a ‘user-defined field’ that can be used to send additional information about the transaction.
													'UDF6'   => 'abc',             // This is a ‘user-defined field’ that can be used to send additional information about the transaction.
													'UDF7'   => 'abc',             // This is a ‘user-defined field’ that can be used to send additional information about the transaction.
													'UDF8'   => 'abc',             // This is a ‘user-defined field’ that can be used to send additional information about the transaction.
													'UDF9'   => 'abc',             // This is a ‘user-defined field’ that can be used to send additional information about the transaction.
													'UDF10'  => 'abc'              // This is a ‘user-defined field’ that can be used to send additional information about the transaction.								
								),
								'Field_Existence_Indicator_OtherData'  => array(
										'custID'			     => $_SESSION['GSM_FUS_UserId'],  
										//'custID'			     => '12345',  
										'transactionSource'      => 'IVR',
										'productInfo'            => 'Credits',  						
										'isUserLoggedIn'         => 'Y', 	 						
										'itemTotal'              => $INV_AMOUNT, 
										'itemCategory'           => 'Virtual Credits', 						
										'ignoreValidationResult' => 'FALSE'
								),
								'Field_Existence_Indicator_DCC'   => array(
										'DCCReferenceNumber' => '09898787', // DCC Reference Number
										'foreignAmount'	     => $foreignAm, // Foreign Amount
										'ForeignCurrency'    => $foreignCurr  // Foreign Currency
								)
							);

$networkOnlineObject = new NetworkonlieBitmapPaymentIntegration($networkOnlineArray);
$requestParameter = $networkOnlineObject->NeoPostData;

//$requestUrl = 'https://www.timesofmoney.com/direcpay/secure/PaymentTxnServlet';
/*
if($networkOnlineObject->url)
	$requestUrl = 'https://NeO.network.ae/direcpay/secure/PaymentTxnServlet';
else
	$requestUrl = 'https://uat-NeO.network.ae/direcpay/secure/PaymentTxnServlet';
*/
/*
TEST URL: https://uat.timesofmoney.com/direcpay/secure/PaymentTxnServlet
LIVE URL: https://www.timesofmoney.com/direcpay/secure/PaymentTxnServlet
*/
$requestUrl = 'https://NeO.network.ae/direcpay/secure/PaymentTxnServlet';
?>

<form action="<?php echo $requestUrl; ?>" method="post" name="network_online_payment" id="network_online_payment">
	<?php echo '<input type="hidden" name="requestParameter" value='.$requestParameter.'>'; ?>
</form>
<script language='javascript' type='text/javascript'>document.getElementById('network_online_payment').submit();</script>