<?php
$HEADER = $fs == '0' ? 'IMEI' : 'File';
?>
				<a href="<?php echo base_url('admin/services/services?fs=' . $fs . '&frmId=' . $this->input->post_get('frmId') . '&fTypeId=' . $this->input->post_get('fTypeId')); ?>">
					<i class="fa icon-list-alt"></i> List All <?php echo $HEADER; ?> Services
				</a>
				<a style="padding-left:10px;"
				   href="<?php echo base_url('admin/services/services_quickedit?fs=' . $fs . '&frmId=' . $this->input->post_get('frmId') . '&fTypeId=' . $this->input->post_get('fTypeId')); ?>">
					<i class="fa fa-pencil"></i> Quick Edit <?php echo $HEADER; ?> Services
				</a>
				&nbsp;&nbsp;&nbsp;
				<a style="padding-left:10px;"
				   href="<?php echo base_url('admin/services/package?fs=' . $fs . '&frmId=' . $this->input->post_get('frmId') . '&fTypeId=' . $this->input->post_get('fTypeId')); ?>">
					<i class="fa fa-plus"></i> <?php echo $this->lang->line('BE_GNRL_14') . ' ' . $HEADER . ' ' . $this->lang->line('BE_PCK_HD_4'); ?>
				</a>
				&nbsp;&nbsp;&nbsp;
				<a style="padding-left:10px;"
				   href="<?php echo base_url('admin/services/categories?fs=' . $fs . '&frmId=' . $this->input->post_get('frmId') . '&fTypeId=' . $this->input->post_get('fTypeId')); ?>">
					<i class="fa icon-list-alt"></i> List <?php echo $HEADER; ?> Service Groups
				</a>
				&nbsp;&nbsp;&nbsp;
				<a style="padding-left:10px;"
				   href="<?php echo base_url('admin/services/category?fs=' . $fs . '&frmId=' . $this->input->post_get('frmId') . '&fTypeId=' . $this->input->post_get('fTypeId')); ?>">
					<i class="fa fa-plus"></i> Add <?php echo $HEADER; ?> Service Group
				</a>
