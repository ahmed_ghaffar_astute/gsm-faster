<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-reorder"></i><?php echo $this->lang->line('BE_LBL_626'); ?>
        </div>
    </div>
    <div class="portlet-body form">
        <form class="form-horizontal" name="frm1" id="frm1" method="post">
            <div class="form-body">
                <div class="portlet-body form">
                    <input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
                    <input type="hidden" id="invAmnt" name="invAmnt" value="<?php echo($amountPayable); ?>">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_PM_11'); ?>:</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" placeholder="Enter Amount" name="txtAmount1" id="txtAmount1" 
                                maxlength="50" value="<?php echo($amount1);?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_PM_5'); ?>:*</label>
                            <div class="col-md-4">
                                <?php  $data = get_payment_method();?>
                                <select id="pMethodId1" name="pMethodId1" class="form-control select2me" data-placeholder="Select...">
                                    <?php FillCombo($pMethodId, $data); ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_PM_12'); ?>:</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" placeholder="Enter Transaction #" name="txtTransactionId1" id="txtTransactionId1" 
                                maxlength="50" value="<?php echo($transactionId1);?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_GNRL'); ?>:</label>
                            <div class="col-md-4">
                                <textarea class="form-control" rows="3" id="txtComments1" name="txtComments1"><?php echo $comments1; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-3"></div><div class="col-lg-9">
                    <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> id="btnAddPmnt" name="btnAddPmnt" class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
                    <div style="display:none;" id="dvLoader1"><img src="<?php echo base_url();?>/assets/img/loading.gif" border="0" alt="Please wait..." /></div>
                </div>
            </div>
		</form>
    </div>
</div>
