<?php
$PCK_TITLE = 'PackageTitle';
$CATEGORY = 'Category';
?>
<div class="page-header border-bottom-0">
    <div class="page-header-content header-elements-md-inline">
        <div style="padding-top: 10px !important;" class="page-title d-flex">
            <h4>
                <i class="icon-arrow-left52 mr-2"></i>
                <?php echo $this->lang->line('BE_GNRL_BTN_1').' '.$this->lang->line('BE_DB_ICON2'); ?>
            </h4>
        </div>
    </div>
</div>


    <div class="form-group row">
        <div class="col-lg-6">
            <label class="control-label"><?php echo $this->lang->line('BE_PCK_HD'); ?></label>
            <select name="packageId" id="packageId" class="form-control select2me" data-placeholder="Select...">
                <?php include 'packagesdropdown.php'; ?>
            </select>
        </div>
        <div class="col-lg-6">
            <label class="control-label"><?php echo $this->lang->line('BE_CODE_3'); ?></label>
            <?php $status_data = get_code_status(); ?>
            <select name="codeStatusId" class="form-control select2me" data-placeholder="Select...">
                <option value="0"><?php echo $this->lang->line('BE_LBL_275'); ?></option>
                <?php FillCombo($codeStatusId, $status_data); ?>
            </select> 
        </div>
    </div>
    <div class="form-group row">
        <div class="col-lg-6">
            <label class="control-label"><?php echo $this->lang->line('BE_PM_7'); ?></label><br />
            <input class="form-control form-control-inline input-largest date-picker" type="text" name="txtFromDt" value="<?php echo($dtFrom);?>" />
        </div>
        <div class="col-lg-6">
            <label class="control-label"><?php echo $this->lang->line('BE_PM_8'); ?></label><br />
            <input class="form-control form-control-inline input-largest date-picker" type="text" name="txtToDt" value="<?php echo($dtTo);?>" />
        </div>
    </div>
    <div class="form-group row">
        <div class="col-lg-6">
            <label class="control-label">Order #</label>
            <input type="text" placeholder="Enter Order #" name="txtOrderNo" value="<?php echo($orderNo);?>" class="form-control">
           
        </div>
        <div class="col-lg-6">
            <label class="control-label"><?php echo $this->lang->line('BE_USR_1'); ?></label>
            <input type="text" name="txtUName" placeholder="Enter Username" value="<?php echo($uName);?>" class="form-control">
        </div>                
    </div>
    <div class="form-group row">
        <div class="col-lg-6">
            <label class="control-label"><?php echo $this->lang->line('BE_CODE_1'); ?>:</label>
            <textarea class="form-control" rows="3" id="txtBulkIMEIs" name="txtBulkIMEIs"><?php echo @$this->input->post('txtBulkIMEIs'); ?></textarea>
            <span class="help-block">
                <?php echo $this->lang->line('BE_LBL_87'); ?>
            </span>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-lg-12">
            <?php if($pageType != '2') { ?>
                <table width="50%">
                <tr>
                    <td width="15%"><label class="control-label">Linked Orders:</label></td>
                    <td><input type="checkbox" class="chkSelect" name="chkLnkdOrders" <?php if($this->input->post_get('chkLnkdOrders')) echo 'checked'; ?> /></td>
                </tr></table>
            <?php } ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-9">
            <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
        </div>
    </div>
    