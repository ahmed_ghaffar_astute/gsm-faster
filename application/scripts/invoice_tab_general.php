<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-reorder"></i><?php echo $this->lang->line('BE_DB_ICON4'); ?>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form class="form-horizontal" name="frm" id="frm">
                    <input type="hidden" id="id" name="id" value="<?php echo($id); ?>" />
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_INDEX_USRNAME'); ?>:</label>
                            <div class="col-md-4">
	                            <p class="form-control-static"><strong><a href="overview.php?id=<?php echo $userId; ?>" style="text-decoration:underline;"><?php echo $userName; ?></a></strong></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_PM_5'); ?>:<span class="required_field">*</span></label>
                            <div class="col-md-4">
                                <?php  $data = get_payment_method();?>
                                <select id="pMethodId" name="pMethodId" class="form-control select2me" data-placeholder="Select...">
                                    <?php FillCombo($pMethodId, $data); ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_LS_7'); ?>:<span class="required_field">*</span></label>
                            <?php $data = get_payment_status(); ?>
                            <div class="col-md-4">
                                <select name="pStatusId" id="pStatusId" class="form-control select2me" data-placeholder="Select...">
                                    <?php FillCombo($pStatusId, $data); ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_PM_12'); ?>:<span class="required_field">*</span></label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" placeholder="Enter Transaction #" name="txtTransactionId" id="txtTransactionId" maxlength="50" value="<?php echo($transactionId);?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_LBL_170'); ?>:<span class="required_field">*</span></label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" placeholder="Enter <?php echo $this->lang->line('BE_LBL_170');?>" id="txtPEmail" name="txtPEmail" maxlength="100" value="<?php echo($pEmail);?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_PM_10'); ?>:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->lang->line('myCredits'); ?></p>
                            </div>
                        </div>
                        <?php if($amountPayable != '') { ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><b><?php echo $this->lang->line('BE_LBL_628'); ?>:</b></label>
                                <div class="col-md-4">
                                    <p class="form-control-static"><b><?php echo $this->lang->line('amountPayable'); ?></b></p>
                                </div>
                            </div>
						<?php } ?>
                        <?php if($paidDtTm != '') { ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><b><?php echo $this->lang->line('BE_LBL_629'); ?>:</b></label>
                                <div class="col-md-4">
                                    <p class="form-control-static"><b><?php echo $this->lang->line('paidDtTm'); ?></b></p>
                                </div>
                            </div>
						<?php } ?>
                        <?php if($creditsTransferred == 0) { ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_PM_17'); ?>:</label>
                                <div class="col-md-4">
                                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                                        <input type="checkbox" name="chkTransferCredits" id="chkTransferCredits" class="toggle"/><span><label for="chkTransferCredits"></label></span>
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_LBL_145'); ?>:</label>
                                <div class="col-md-4">
                                    <p class="form-control-static"><?php echo $this->lang->line('BE_LBL_524'); ?></p>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_LBL_525'); ?>:</label>
                            <div class="col-md-4">
                                    <input type="checkbox" name="chkNotify" id="chkNotify" />
                            </div>
                        </div>
                        <div class="form-group" style="display:none;" id="dvNType">
                            <label class="col-md-3 control-label">&nbsp;</label>
                            <div class="col-md-9 radio-list">
                                <label class="radio-inline">
                                <input type="radio" name="chkNotificationType" id="chkNotificationType1" checked><?php echo $this->lang->line('BE_LBL_526'); ?></label>
                                <label class="radio-inline">
                                <input type="radio" name="chkNotificationType" id="chkNotificationType2"><?php echo $this->lang->line('BE_LBL_527'); ?></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_GNRL'); ?>:</label>
                            <div class="col-md-4">
                                <textarea class="form-control" rows="3" id="txtComments" name="txtComments"><?php echo $comments; ?></textarea>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-3"></div><div class="col-lg-9">
                <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> id="btnSave" name="btnSave" class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
                <img style="display:none;" id="statusLoader" src="<?php echo base_url();?>/assets/img/loading.gif" border="0" alt="Please wait..." />
            </div>
        </div>
    </div>
</div>
