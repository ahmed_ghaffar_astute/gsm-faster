<?
$rsPayments = get_payment_methods_and_detail();
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-reorder"></i><?php echo $this->lang->line('BE_LBL_227'); ?>
        </div>
    </div>
	<div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-advance table-hover">
            <thead>
            <tr>
                <th><?php echo $this->lang->line('BE_PM_11'); ?></th>
                <th><?php echo $this->lang->line('$BE_PM_5'); ?></th>
                <th><?php echo $this->lang->line('$BE_PM_12'); ?></th>
                <th><?php echo $this->lang->line('BE_CR_3'); ?></th>
                <th><?php echo $this->lang->line('BE_GNRL'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if($rsPayments)
            {
                foreach($rsPayments as $row)
                {
                ?>
                    <tr>
                        <td class="highlight"><?php echo $currency.' '.$row->InvAmount;?></a></td>
                        <td><?php echo stripslashes($row->PaymentMethod);?></td>
                        <td><?php echo stripslashes($row->InvTransactionId);?></td>
                        <td><?php echo stripslashes($row->InvDtTm);?></td>
                        <td><?php echo stripslashes($row->InvComments);?></td>
                    </tr>
            <?php }?>
            <?php }else{ ?>
                <tr><td colspan="5"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td></tr>
            <?php } ?>			    			
            </tbody>
            </table>
        </div>
    </div>                    
</div>