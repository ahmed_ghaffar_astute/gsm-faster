<?php
if(@$verifyPage == '1')
{
    $DD_WIDTH = '360px';
    $rsPackages = $this->Services_model->fetch_distinct_log_package_category_request();
}
else
{
    $rsPackages = $this->Services_model->fetch_log_package_category_request();

}
?>
<div class="page-header border-bottom-0">
    <div class="page-header-content header-elements-md-inline">
        <div style="padding-top: 10px !important;" class="page-title d-flex">
            <h4>
                <i class="icon-arrow-left52 mr-2"></i>
                <?php echo $this->lang->line('BE_GNRL_BTN_1').' '.$this->lang->line('BE_DB_ICON2'); ?>
            </h4>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-6">
        <label class="control-label"><?php echo $this->lang->line('BE_CODE_12'); ?></label>
        <select name="logPackageId" id="logPackageId" class="form-control select2me" data-placeholder="Select...">
            <?php
            $prevCatId = 0;
            foreach($rsPackages as $row)
            {
                if($prevCatId > 0)
                {
                    if($row->CategoryId != $prevCatId)
                    echo '</optgroup><optgroup label="'.$row->Category.'"> ';
                }
                else
                    echo '<option value="0" selected>'. $this->lang->line('BE_LBL_276') .'</option><optgroup label="'.$row->Category.'">';
                $srvcStatus = $row->DisableLogPackage == '0' ? '' : ' - In Active';
            ?>
                <option value="<?php echo $row->PackageId?>" <?php if($myNetworkId == $row->PackageId) echo 'selected';?> >
                    <?php echo $row->LogPackageTitle.$srvcStatus;?>
                </option>
            <?php
                $prevCatId = $row->CategoryId;
            }
            echo '</optgroup>';
            ?>
        </select>
    </div>
    <div class="col-lg-6">
        <label class="control-label"><?php echo $this->lang->line('BE_USR_1'); ?></label>
        <input type="text" name="txtUName" placeholder="Enter Username" value="<?php echo($uName);?>" class="form-control">
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-6">
        <label class="control-label">Order #</label>
        <input type="text" placeholder="Enter Order #" name="txtOrderNo" value="<?php echo($orderNo);?>" class="form-control">
    </div>
    <div class="col-lg-6">
        <?php $code_status = get_code_status() ?>
        <label class="control-label"><?php echo $this->lang->line('BE_CODE_3'); ?></label>
        <select name="codeStatusId" class="form-control select2me" data-placeholder="Select...">
            <option value="0">Please Choose Status...</option>
            <?php FillCombo($codeStatusId, $code_status); ?>
        </select>
    </div>
</div>
<div class="from-group row">
    <div class="col-lg-6">
        <label class="control-label"><?php echo $this->lang->line('BE_PM_7'); ?></label><br />
        <input class="form-control form-control-inline input-largest date-picker" type="text" name="txtFromDt" value="<?php echo($dtFrom);?>" />
    </div>
    <div class="col-lg-6">
        <label class="control-label"><?php echo $this->lang->line('BE_PM_8'); ?></label><br />
        <input class="form-control form-control-inline input-largest date-picker" type="text" name="txtToDt" value="<? echo($dtTo);?>" />
    </div>            
</div>
       
<div class="form-group row">
    <div class="col-lg-6">
        <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
    </div>
</div>
    
