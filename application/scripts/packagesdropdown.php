<?php
if(@$verifyPage == '1')
{
	$rsPackages = $this->General_model->get_package_cat($CATEGORY ,$PCK_TITLE ,$strPckWhere );
}
else
{
	$tblOrders = 'tbl_gf_codes';
	if(@$serviceType == '1')
		$tblOrders = 'tbl_gf_codes_slbf';
	$rsPackages = $this->General_model->get_package_cat_2($CATEGORY ,$PCK_TITLE ,$strPckWhere , $strPackIds);
}
$totalPackages = count($rsPackages);
if($totalPackages > 0)
{
	$prevCatId = 0;
	foreach($rsPackages as $row)
	{
		if($prevCatId > 0)
		{
			if($row->CategoryId != $prevCatId)
				echo '</optgroup><optgroup label="'.$row->Category.'"> ';
		}
		else
			$choose = $this->lang->line('BE_LBL_274');
			echo '<option value="0" selected>'.$choose.'</option><optgroup label="'.$row->$CATEGORY.'">';
		$srvcStatus = $row->DisablePackage == '0' ? '' : ' - In Active';
	?>
		<option value="<?php echo $row->PackageId?>" <?php if($packageId == $row->PackageId) echo 'selected';?> >
			<?php echo $row->$PCK_TITLE.$srvcStatus;?>
		</option>
	<?php
		$prevCatId = $row->CategoryId;
	}
	echo '</optgroup>';
}
else
	echo '<option class="clsOption" value="0" selected>'. $this->lang->line('BE_LBL_276') .'</option>';
?>