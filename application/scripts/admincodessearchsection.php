<?php
$PCK_TITLE = 'PackageTitle';
$CATEGORY = 'Category';
?>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                    <?php echo $this->lang->line('BE_GNRL_BTN_1').' '.$this->lang->line('BE_DB_ICON2'); ?>
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <div id="card-collapse" class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label class="control-label"><?php echo $this->lang->line('BE_PCK_HD_4'); ?></label>
                            <select name="packageId" class="form-control select2" >
                            <?php
                                include APPPATH.'scripts/packagesdropdown.php';
                            ?>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <label class="control-label"><?php echo $this->lang->line('BE_CODE_3'); ?></label>
                            <?php $codestatus = get_code_status(); ?>
                            <select name="codeStatusId" class="form-control select2me" data-placeholder="Select...">
                                <option value="0"><?php echo $this->lang->line('BE_LBL_275'); ?></option>
                                <?php FillCombo($codeStatusId, $codestatus); ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label class="control-label">Order #</label>
                            <input type="text" placeholder="Enter Order #" name="txtOrderNo" value="<?php echo isset($orderNo) ? $orderNo : '';?>" class="form-control">
                        </div>
                        <div class="col-lg-6">
                            <label class="control-label"><?php echo $this->lang->line('BE_USR_1'); ?></label>
                            <input type="text" name="txtUName" placeholder="Enter Username" value="<?php echo isset($uName) ? $uName : '' ;?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label class="control-label"><?php echo $this->lang->line('BE_PM_7'); ?></label><br />
                            <input class="form-control form-control-inline input-largest date-picker" type="text" name="txtFromDt" value="<?php echo isset($dtFrom)? $dtFrom : '' ;?>" />
                        </div>
                        <div class="col-lg-6">
                            <label class="control-label"><?php echo $this->lang->line('BE_PM_8'); ?></label><br />
                            <input class="form-control form-control-inline input-largest date-picker" type="text" name="txtToDt" value="<?php echo isset($dtTo)? $dtTo :'';?>" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label class="control-label"><?php echo $this->lang->line('BE_CODE_1'); ?>:</label>
                            <textarea class="form-control" rows="3" id="txtBulkIMEIs" name="txtBulkIMEIs"><?php echo @$this->input->post_get('txtBulkIMEIs'); ?></textarea>
                            <span class="help-block">
                                <?php echo $this->lang->line('BE_LBL_87'); ?>
                            </span>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>