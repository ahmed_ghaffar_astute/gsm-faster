<?php
$rsNLs = $this->Services_model->fetch_newsltrs_cat_news_letter();

if($rsNLs)
{
	$prevCatId = 0;
	foreach($rsNLs as $row)
	{
		if($prevCatId > 0)
		{
			if($row->CategoryId != $prevCatId)
				echo '</optgroup><optgroup label="'.$row->Category.'"> ';
		}
		else
			echo '<option value="0" selected>Choose a Newsletter</option><optgroup label="'.$row->Category.'">';
	?>
		<option value="<?php echo $row->NewsLtrId?>"><?php echo $row->NewsLtrTitle; ?></option>
	<?php
		$prevCatId = $row->CategoryId;
	}
	echo '</optgroup>';
}
else
	echo '<option class="clsOption" value="0" selected>Choose a Newsletter</option>';
?>