			<a href="<?php echo base_url('admin/services/serverservices?frmId='.$this->input->post_get('frmId').'&fTypeId='.$this->input->post_get('fTypeId'));?>">
				<i class="fa icon-list-alt"></i> List All Server Services
			</a>
			<a style="padding-left:10px;" href="<?php echo base_url('admin/services/services_quickedit?fs=2&frmId='.$this->input->post_get('frmId').'&fTypeId='.$this->input->post_get('fTypeId'));?>">
				<i class="fa fa-pencil"></i> Quick Edit Server Services
			</a>
			&nbsp;&nbsp;&nbsp;
			<a style="padding-left:10px;" href="<?php echo base_url('admin/services/serverservice?frmId='.$this->input->post_get('frmId').'&fTypeId='.$this->input->post_get('fTypeId'));?>">
				<i class="fa fa-plus"></i> <?php echo $this->lang->line('BE_GNRL_14').' Server '.$this->lang->line('BE_PCK_HD_4'); ?>
			</a>
			&nbsp;&nbsp;&nbsp;
			<a style="padding-left:10px;" href="<?php echo base_url('admin/services/categories?iFrm=1&frmId='.$this->input->post_get('frmId').'&fTypeId='.$this->input->post_get('fTypeId'));?>">
				<i class="fa icon-list-alt"></i> List Server Service Groups
			</a>
			&nbsp;&nbsp;&nbsp;
			<a style="padding-left:10px;" href="<?php echo base_url('admin/services/category?iFrm=1&frmId='.$this->input->post_get('frmId').'&fTypeId='.$this->input->post_get('fTypeId'));?>"><i class="fa fa-plus"></i> Add Server Service Group</a>
