<?php
$row = $this->User_model->get_api_data_by_payment_id($PAYMENT_METHOD_ID);
if (isset($row->APIUsername) && $row->APIUsername != '')
{
	$API_USERNAME = stripslashes($row->APIUsername);
	$API_PASSWORD = stripslashes($row->APIPassword);
	$API_SIGNATURE = stripslashes($row->APISignature);
}

$carholder_name = $this->input->post('carholder_name') ?: '0';
$creditCardType = $this->input->post('card_type') ?: '0';
$creditCardNumber = $this->input->post('card_number') ?: '';
$expDateMonth = $this->input->post('expiry_month') ?: '0';
$expDateYear = $this->input->post('expiry_year') ?: '';
$CVV = $this->input->post('cvv') ?: '';

$addressCC = $this->input->post('txtAddress') ?: '';
$stateCC = $this->input->post('txtState') ?: '';
$cityCC = $this->input->post('txtCity') ?: '';
$zipCC = $this->input->post('txtZip') ?: '';
$countryISO = $this->input->post('country') ?: '';
// Set up your API credentials, PayPal end point, and API version.

//define("ENVIROMENT","sandbox");
define("ENVIROMENT","live");

//PAYMENT DATA
$name = explode(" ", $carholder_name);
$lastname = '';
if(isset($name[1])){$lastname = $name[1];}
$paymentType 			= 'Sale';
$firstName 				= urlencode($name[0]);
$lastName 				= urlencode($lastname);
$creditCardType 		= urlencode($creditCardType);
$creditCardNumber	    = urlencode($creditCardNumber);
$expDateMonth 			= urlencode($expDateMonth);
$padDateMonth 			= urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));
$expDateYear 			= urlencode($expDateYear);
$cvv2Number 			= urlencode($CVV);
$address1 				= $addressCC;
$city 					= $cityCC;
$state 					= $stateCC;
$zip 					= $zipCC;
$country 				= $countryISO;
$amount 				= $INV_AMOUNT;
$currencyID 			= $INV_CURRENCY;

$nvpStr 				="&PAYMENTACTION=$paymentType&Amt=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=$padDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$country&CURRENCYCODE=$currencyID&DESC=Pay&L_BILLINGTYPEn=1";


/*
&PAYMENTACTION=Sale&Amt=31.50&CREDITCARDTYPE=VISA&ACCT=4242424242424242&EXPDATE=12&CVV2=123&FIRSTNAME=Hammad&LASTNAME=Akbar&STREET=none&CITY=Largo&STATE=FL&ZIP=92870&COUNTRYCODE=USA&CURRENCYCODE=USD&DESC=Pay&L_BILLINGTYPEn=1
*/

$methodName_ = 'DoDirectPayment';
$environment = ENVIROMENT;

$API_UserName 	= urlencode($API_USERNAME);
$API_Password 	= urlencode($API_PASSWORD);
$API_Signature 	= urlencode($API_SIGNATURE);

$API_Endpoint = "https://api-3t.paypal.com/nvp";
if("sandbox" === $environment || "beta-sandbox" === $environment)
{
	$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
}
		
$version = urlencode('54.0');
// Set the API operation, version, and API signature in the request.
$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr"; 

// Set the curl parameters.
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
// Turn off the server and peer verification (TrustManager Concept).
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
// Set the request as a POST FIELD for curl.
curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
// Get response from the server.
$httpResponse = curl_exec($ch);
$httpResponseAr = explode("&", $httpResponse);
//echo '==================='.$httpResponse."<br><br>";
$httpParsedResponseAr = array();
/*
echo '<pre>';
print_r($httpResponseAr);
echo '</pre>';*/
foreach ($httpResponseAr as $i => $value)
{
	$tmpAr = explode("=", $value);
	if(sizeof($tmpAr) > 1)
	{
		$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
	}
}
/*echo '<br><br><br>';
echo '************************************************';
echo '<pre>';
print_r($httpParsedResponseAr);
echo '</pre>';*/
if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
{
	
	//send mail to payment user
	$amt = urldecode($httpParsedResponseAr["AMT"]);
	$r = $this->User_model->get_autofillcredits();
	$autoFillCredits = $r->AutoFillCredits;
	$USER_CREDITS = $r->Credits;
	if($autoFillCredits == '1')
	{
		$this->crypt_key($this->session->userdata('GSM_FUS_UserId'));
		$decCredits = $this->decrypt($USER_CREDITS);
		$decCredits += $ÌNV_CREDITS;
		$encCredits = $this->encrypt($decCredits);

		$this->User_model->update_user_credits();
		$dtTm = setDtTmWRTYourCountry();
	
		$insert_data = array(
			'UserId' => $this->session->userdata('GSM_FUS_UserId'),
			'Credits' =>  $ÌNV_CREDITS,
			'Description' => '+ Add Funds (Invoice #'.$invId.')' ,
			'IMEINo' => '' ,
			'HistoryDtTm' => $dtTm,
			'CreditsLeft' => $encCredits,
			'PaymentId' => $invId,
			'IP'=> $this->input->server('REMOTE_ADDR'),
		);
		$this->User_model->insert_credit_history($insert_data);
		
		$update_data= array(
			'TransactionId' => $httpParsedResponseAr["TRANSACTIONID"],
			'PaymentStatus' => 2,
			'CreditsTransferred' => 1,
			'UpdatedAt' => $dtTm,
		);
		$this->User_model->update_gf_payments($update_data , $invId);

		send_push_notification($ÌNV_CREDITS.' Credits have been added in your account!', $this->session->userdata('GSM_FUS_UserId'));

		if($this->session->userdata('UserEmail') != '')
		{
			$arr = getEmailDetails();
			invoiceEmail($this->session->userdata('UserEmail'), $this->session->userdata('UserName'), $invId, $ÌNV_CREDITS, $INV_AMOUNT, $INV_CURRENCY, $INV_DT, 'Debit/Credit Card', 1);
			invoiceEmail($arr[4], 'Admin', $invId, $ÌNV_CREDITS, $INV_AMOUNT, $INV_CURRENCY, $INV_DT, 'Debit/Credit Card', 1, $this->session->userdata('UserName'));									
		}
		redirect(base_url('page/paypalpro?success=1'));
		// echo "<script>window.location.href='paypalpro.php?success=1';</script>";

	}
	else
	{
		$update_data= array(
			'TransactionId' => $httpParsedResponseAr["TRANSACTIONID"],
			'PaymentStatus' => 5,
			'UpdatedAt' => $dtTm,
		);

		$this->User_model->update_gf_payments($update_data , $invId);

		$arr = getEmailDetails();
		invoiceEmail($arr[4], 'Admin', $invId, $ÌNV_CREDITS, $INV_AMOUNT, $INV_CURRENCY, $INV_DT, 'Debit/Credit Card', 5, $this->session->userdata('UserName'));
		
		redirect(base_url('page/paypalpro?success=2'));
		//$message = 'Your card has been charged successfully. Please contact admin to add credits into your account!';
	}
}
else
{

	$arr = explode('=', $httpResponseAr['6']);
	$err = @urldecode($arr[1]);
	redirect(base_url('page/paypalpro?failure='.$err));
	
	// echo "<script>window.location.href='paypalpro.php?failure=$err';</script>";
}

if(!$httpResponse)
{
	exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
}
?>