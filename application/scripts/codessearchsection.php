<?php
$PCK_TITLE = 'PackageTitle';
$CATEGORY = 'Category';
$CD_STATUS = 'CodeStatus';

?>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                    Search Orders
                </a>
            </h4>
        </div>

        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <div id="card-collapse" class="card-body">
                    <div class="row">
                        <div class="col-md-8 form-group">
                            <select name="packageId" class="form-control select2" >
                            <?php
                                $strPackIds = getUserPacksIds($this->session->userdata('GSM_FUS_UserId') , 0);
                                include APPPATH.'scripts/packagesdropdown.php';
                            ?>
                            </select>
                        </div>
                        <div class="col-md-2 form-group">
                            <select class="form-control" name="codeStatusId" style="width:100%">
                                <option value="0">Choose Status</option>
                                <?php 
                                    if($STATUSES){
                                        foreach ($STATUSES as $key => $value)
                                        { ?>
                                            <option <?php if($codeStatusId == $key) echo 'selected';?> value="<?php echo $key;?>"><?php echo $value;?></option>
                                        <?php
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-2 form-group">
                            <input type="text" class="form-control" autocomplete="off" placeholder="Order No." name="oId" value="<?php echo($oId);?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <textarea placeholder="IMEIs" rows="3" class="form-control" style="height:95px;" id="txtIMEI" name="txtIMEI"><?php echo @$this->input->post('txtIMEI'); ?></textarea>
                        </div>
                        <div class="col-md-2 form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" autocomplete="off" placeholder="From Date" name="txtFromDt" value="<?php echo $dtFrom;?>" id="datepicker-autoclose">
								<span class="input-group-text input-group-addon icon_click"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" autocomplete="off" placeholder="To Date" name="txtToDt" value="<?php echo($dtTo);?>" id="datepicker1-autoclose">
								<span class="input-group-text input-group-addon icon_click1"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <select class="form-control" name="records" style="width:100%">
                                <option value="0">No Of Records</option>
                                <?php
                                    $NO_OF_RECORDS = array("10"=>10, "50"=>50, "100"=>100, "200"=>200, "300"=>300, "400"=>400, "500"=>500, "1000"=>1000); 
                                    foreach ($NO_OF_RECORDS as $key => $value)
                                    {
                                        $selected = '';
                                        if($limit == $key)
                                            $selected = 'selected';
                                        echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-info waves-effect waves-light"><?php echo $this->lang->line('CUST_LBL_317'); ?></button>          
                            <input type="hidden" name="start" id="start" value="0" />
                            <input type="hidden" name="sortBy" id="sortBy" value='<?php echo $sortBy; ?>' />
                            <input type="hidden" name="orderByType" id="orderByType" value='<?php echo $orderByType; ?>' />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
