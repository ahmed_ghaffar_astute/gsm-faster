<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function view($ticketId) {

    	if($this->session->flashdata('message')) {
			$this->data['message'] = $this->session->flashdata('message');
		}

		$this->data['ticket'] = $this->User_model->getUserTicket($this->user_id, $ticketId);
        $this->data['ticketDepartment'] = $this->User_model->getTicketDepartmentDetails($this->user_id, $ticketId);
        $this->data['ticketReplies'] = $this->User_model->getTicketReplies($this->user_id, $this->data['ticket']->TicketNo);
        //last_query();
        $this->data['view'] = 'ticket';
        $this->load->view('layouts/default', $this->data);
    }
    public function update($ticketId)
    {
    	if($this->input->post_get('close')==1)
		{
			$data['StatusId'] = 3;
			$where['TicketNo'] = $ticketId;
			$this->Tickets_model->update_tickets($data, $where);

			$this->session->set_flashdata('message', "Ticket closed successfully");
			redirect(base_url("ticket/view/" . $ticketId));

		}
    	else {

			$ticket = $this->User_model->getUserTicket($this->user_id, $ticketId);

			$data['DepartmentId'] = $ticket->DepartmentId;
			$data['CategoryId'] = $ticket->CategoryId;
			$data['PriorityId'] = $ticket->PriorityId;
			$data['StatusId'] = $ticket->StatusId;
			$data['UserId'] = $this->user_id;
			$data['Name'] = "";
			$data['Email'] = "";
			$data['Subject'] = "";
			$data['Message'] = $this->input->post("txtReply");
			$data['DtTm'] = "";
			$data['ReadByAdmin'] = "";
			$data['ByAdmin'] = "";
			$data['Notes'] = "";
			$data['DtTm'] = date('Y-m-d H:i:s');
			$data['TicketNo'] = $ticket->TicketNo;
			$this->User_model->add_user_ticket($data);
			$this->session->set_flashdata('message', "Ticket updated successfully");
			redirect(base_url("ticket/view/" . $ticketId));
		}


    }



}
