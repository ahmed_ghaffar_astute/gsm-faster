<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class System extends MY_Controller{
     
	public function __construct() {
        parent::__construct();
    }
    

    public function subadmin(){
        $id = $this->input->post_get('id') ?: 0;
        $this->data['email'] = $this->data['firstName'] = $this->data['lastName'] = $this->data['phone'] = $this->data['disable'] = $this->data['secret'] = '';
        $this->data['roleId'] = $this->data['gAuth'] = 0;
        //require_once 'include/php/googleLib/GoogleAuthenticator.php';
        //$ga = new GoogleAuthenticator();
        if($id > 0)
        {
            $row = $this->System_model->fetch_all_admin_data($id);
            
            if (isset($row->AdminUserName) && $row->AdminUserName != '')
            {
                $this->data['email'] = $row->AdminUserName;
                $this->data['firstName'] = stripslashes($row->FirstName);
                $this->data['lastName'] = stripslashes($row->LastName);
                $this->data['phone'] = stripslashes($row->Phone);
                $this->data['disable'] = $row->DisableAdmin;
                $this->data['roleId'] = $row->RoleId;
                $this->data['gAuth'] = $row->GoogleAuth;
                $this->data['secret'] = theString_Decrypt($row->GAuthSecret, '800g73@uTH', 10);
            }
        }
        $google_obj = new CI_GoogleAuthenticator();
        $secret = '' ;
        if(trim($this->data['secret']) == '')
        {
            $secret =  $google_obj->createSecret(); // CREATING GOOGLE SECRET KEY
            
        }
        $website = $this->input->server('HTTP_HOST');
        $this->data['qrCodeUrl'] = '' ;
        $this->data['qrCodeUrl'] = $google_obj->getQRCodeGoogleUrl($website, $secret, 'Backend');

        $this->data['id'] = $id;
        $this->data['roles'] = $this->System_model->fetch_roles();
       
        $this->data['view'] = 'admin/sub_admin' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function ajxsubadmin(){
        $purpose = $this->input->post_get('purpose');

        if($purpose == 'save')
        {
            $msg = $pwd = '';
            $id = $this->input->post_get('id');
            $phone = $this->input->post_get('phone');
            $email = $this->input->post_get('email');
            $fName = $this->input->post_get('fName');
            $lName = $this->input->post_get('lName');
            $roleId = $this->input->post_get('roleId');
            $chkGAuth = $this->input->post_get('chkGAuth');
            $gAuthSecret = $this->input->post_get('gAuthSecret');
            if($this->input->post_get('pass') != '')
                $pwd = bFEncrypt($this->input->post_get('pass'));
            $disableVal = $this->input->post_get('chkBxVal');
           
            $row = $this->System_model->fetch_admin_count($email , $id);
            if ($row->TotalRecs > 0)
            {
                $msg = "AdminUserName '<b>".$email."</b>'". $this->lang->line('BE_GNRL_10')."~0"; 
            }
            else
            {
                if($gAuthSecret != '')
                    $gAuthSecret = encryptThe_String($gAuthSecret, '800g73@uTH', 10);
                if($id == 0)
                {
                    $insert_array = array(
                        'FirstName' => $fName,
                        'LastName' => $lName ,
                        'AdminUserName' => $email ,
                        'AdminUserPassword' => $pwd ,
                        'RoleId' => $roleId,
                        'Phone' => $phone ,
                        'DisableAdmin' => $disableVal ,
                        'GoogleAuth' => $chkGAuth,
                        'GAuthSecret' => $gAuthSecret,
                    );
                    $this->System_model->insert_admin_data($insert_array);
                    
                    $msg =  "Operation has been completed successfully!~1";
                }
                else
                {
                    $qry = '';
                    $update_data = array();
                    if($pwd != ''){
                        $update_data['AdminUserPassword'] = $pwd ;
                    }
                    $update_data['FirstName'] = $fName;
                    $update_data['LastName'] = $lName;
                    $update_data['Phone'] = $phone;
                    $update_data['AdminUserName'] =  $email;
                    $update_data['DisableAdmin'] = $disableVal;
                    $update_data['RoleId'] = $roleId ;
                    $update_data['GoogleAuth'] = $chkGAuth;
                    $update_data['GAuthSecret'] = $gAuthSecret;

                    $where = array(
                        'AdminId' => $id ,
                    );

                    $this->update_admins_data($update_data , $where);
                    
                    $msg =  "Operation has been completed successfully!~0";
                }
            }
        }	
        echo $msg;
    }

    public function subadmins(){
        $strWhere = '';
        $fName = '';
        $lName = '';
        $uName = '';
        $message = '';
        $roleId = 0;
        $del = $this->input->post_get('del') ? 1 : 0;
        if($del == '1')
        {
            $id = $this->input->post_get('id') ?: 0;
            if ($id > 0 && $id != '1')
            {
                $this->System_model->delete_admins($id);
                
                $message =  $this->lang->line('BE_GNRL_12');
            }
        }
        if($this->input->post_get('btnSubmit') )
        {
            $adminIds = $this->input->post('chkAdmins') ?: 0;
            $selectedAdmins = '0';
            if(is_array($adminIds))
                $selectedAdmins = implode(",", $adminIds);
            $allAdminIds = $this->input->post('strAdminIds') ?: 0;
            if($allAdminIds != '0')
            {
                $update_data = array(
                    'DisableAdmin' => 0
                );
                $where_in = array(
                    'AdminId' => $allAdminIds
                );
                $this->System_model->update_admins_data($update_data , '' , $where_in);
                
                $update_data = array(
                    'DisableAdmin' => 1
                );
                $where_in = array(
                    'AdminId' => $selectedAdmins
                );
                $this->System_model->update_admins_data($update_data , '' , $where_in);
               
               
            }
            $message = $this->lang->line('BE_GNRL_11');	
        }
        if($this->input->post_get('txtFName') && $this->input->post_get('txtFName') != '')
        {
            $fName = trim($this->input->post_get('txtFName'));
            if($fName != '')
            {
                $strWhere .= " WHERE FirstName LIKE '%".$fName."%'";
            }
        }
        if($this->input->post_get('txtLName') && $this->input->post_get('txtLName') != '')
        {
            $lName = trim($this->input->post_get('txtLName'));
            if($lName != '')
            {
                $strWhere .= " AND LastName LIKE '%".$lName."%'";
            }
        }
        if($this->input->post_get('txtUName') && $this->input->post_get('txtUName') != '')
        {
            $uName = trim($this->input->post_get('txtUName'));
            if($uName != '')
            {
                $strWhere .= " AND AdminUserName LIKE '%".$uName."%'";
            }
        }
        if($this->input->post_get('roleId') && $this->input->post_get('roleId') != '0')
        {
            $roleId = $this->input->post_get('roleId');
            $strWhere .= " AND A.RoleId = '$roleId'";
        }
        $this->data['rsAdmins'] = $this->System_model->fetch_admins_roles_data($strWhere);
       
        $this->data['roles'] = $this->System_model->fetch_roles();
        $this->data['message'] = $message;
        $this->data['fName'] = $fName;
        $this->data['lName'] = $lName;
        $this->data['uName'] = $uName;
        $this->data['roleId'] = $roleId;


        $this->data['view'] = 'admin/sub_admins' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function adminrole(){
        $id = $this->input->post_get('id') ?: 0;
        $aRole = '';
        $disable = 0;
        $message = '';
        if ($this->input->post('txtARole'))
        {
            $aRole = $this->input->post('txtARole');
           
            $row = $this->System_model->fetch_specific_role_by_id($aRole , $id);
            if (isset($row->RoleId) && $row->RoleId != '')
            {
                $message = "'$aRole'".$this->lang->line('BE_GNRL_10');
            }
            else
            {
                $disable = $this->input->post('chkDisable') ? 1 : 0;
                if($id == 0)
                {
                    $insert_data = array(
                        'Role' => $aRole,
                        'DisableRole' => $disable
                    );
                    $id = $this->System_model->insert_roles($insert_data);
                }			
                else
                {
                    $update_data = array(
                        'Role' => $aRole,
                        'DisableRole' => $disable
                    );

                    $where = array(
                        'RoleId' => $id
                    );

                    $this->System_model->update_roles($update_data , $where);
                }
                $message = $this->lang->line('BE_GNRL_11');
            }
        }
        if ($this->input->post('totalRTypes') && $id > 0)
        {
            $totalRlTypes = $this->input->post('totalRTypes') ?: 0;
            $strData = '';
            for($m = 1; $m <= $totalRlTypes; $m++)
            {
                $selectedIds = sizeof($this->input->post('roles'.$m));
                for($k = 0; $k < $selectedIds; $k++)
                {
                    if($strData != '')
                        $strData .= ',';
                    $strData .= "(".($id).",".$this->input->post('roles'.$m)[$k].")";
                }
            }
            if($strData != '')
            {
              
                $this->System_model->del_roles_forms($id);
                $this->System_model->insert_roles_forms($strData);
               
            }
            $message = $this->lang->line('BE_GNRL_11');
        }
        if($id > 0)
        {
            $row = $this->System_model->fetch_role_by_id($id,  $aRole );
         
            if (isset($row->Role) && $row->Role != '')
            {
                $aRole = stripslashes($row->Role);
                $disable = $row->DisableRole;
            }
        }
        if($this->input->post_get('btnSave'))
        {
            $message = $this->lang->line('BE_GNRL_11');
        }

        $count = 0;
        $strTypeBsdFrms = '0';

        $formTypeId = $this->input->post_get('formTypeId') ?: 0;
        $ARR_ASGND_FORMS = array();
        if($this->input->post_get('btnSave'))
        {

            $formIds = $this->input->post('chkForms');
            
            $ids = $this->input->post('typeBsdFrms');
            if($formIds){
                $totalForms = count($formIds);
            }
            $str = '';
            for ($j = 0; $j < $totalForms; $j++)
            {
                if($j > 0)
                    $str .= ',';
                $str .= "('$id','".$formIds[$j]."')";
            }
            $this->System_model->del_roles_forms_by_formids($id , $ids);
         
            if($str != '')
                $this->System_model->insert_roles_forms($str);
                
            $message = $this->lang->line('BE_GNRL_11');
        }
        if($id > 0)
        {
            $rsAssignedForms = $this->System_model->fetch_roles_forms_by_role_id($id);
           
            foreach($rsAssignedForms as $row)
            {
                $ARR_ASGND_FORMS[$row->FormId] = $row->FormId;
            }
        }
        if($formTypeId != '0')
        {
            $this->data['rsForms'] = $this->System_model->fetch_forms_type_data($formTypeId);
         
            $count = count($this->data['rsForms']);
        }
        $this->data['count'] = $count;
        $this->data['strTypeBsdFrms'] = $strTypeBsdFrms;
        $this->data['formTypeId'] = $formTypeId;
        $this->data['ARR_ASGND_FORMS'] = $ARR_ASGND_FORMS;
        $this->data['message'] = $message ;
        $this->data['id'] = $id;
        $this->data['disable'] = $disable;
        $this->data['aRole'] = $aRole;
        
        $this->data['arole_tab_general'] = $this->load->view('admin/arole_tab_general' , $this->data , TRUE);
        $this->data['arole_tab_roles'] = $this->load->view('admin/arole_tab_roles' , $this->data , TRUE);

        $this->data['view'] = 'admin/admin_role' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }


    public function adminroles(){
        $this->data['rsARoles'] = $this->System_model->fetch_roles_data();
       
        $this->data['view'] = 'admin/admin_roles' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

}

