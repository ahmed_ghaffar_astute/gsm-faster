<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends MY_Controller{
     
	public function __construct() {
        parent::__construct();
    }

    public function blockpmethods(){
        $id = $this->input->post_get('countryId') ?: 0;
        $cldFrm = $this->input->post('cldFrm') ?: 0;
        $del = $this->input->post_get('del') ? 1 : 0;
        $message = '';
        $strIds = '';
        $strData = '' ;
        if($del == '1')
        {
            
            $update_data = array(
                'BlockedPMethodIds' => ''
            );
            $where = array(
                'CountryId' => $id
            );

            $this->Sales_model->update_countries($update_data , $where);
            $message = $this->lang->line('BE_GNRL_12');
        }

        if($cldFrm == '1' && $id > 0)
        {
            if($this->input->post('payMethods'))
            {
               
                if(is_array($this->input->post('payMethods')))
                {
                    $selectedIds = sizeof($this->input->post('payMethods'));
                    $strData = '';
                    for($k = 0; $k < $selectedIds; $k++)
                    {
                        if($k > 0)
                            $strData .= ',';
                        $strData .= $this->input->post('payMethods')[$k];
                    }
                    if($strData != '')
                    {
                        $update_data = array(
                            'BlockedPMethodIds' => $strData
                        );
                        $where = array(
                            'CountryId' => $id
                        );
                        $this->Sales_model->update_countries($update_data , $where);
                    }
                }
                $message = $this->lang->line('BE_GNRL_11');
            }
        }
        
        if($id > 0)
        {
            $row = $this->Sales_model->fetch_country_data_by_id($id);
          
            if (isset($row->BlockedPMethodIds) && $row->BlockedPMethodIds != '')
            {
                $strIds = stripslashes($row->BlockedPMethodIds);
            }
        }
        $this->data['rsData'] = $this->Sales_model->fetch_countries_data();
        $this->data['count'] = count($this->data['rsData']);
        $this->data['message'] = $message;
        $this->data['id'] = $id ;
        $this->data['strIds'] = $strIds;
        $this->data['rsPMs'] = $this->Sales_model->fetch_payment_methods();
       

        $this->data['view'] = 'admin/block_pmethods' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function paymentmethods(){
        $message = '';
        $del = $this->input->post_get('del') ? 1 : 0;
        if($del == '1')
        {
            $id = $this->input->post_get('id') ?: 0;
            
            $this->Sales_model->del_pp_receivers($id);
            $message = $this->lang->line('BE_GNRL_12');
        }
        $this->data['rsPMs'] = $this->Sales_model->fetch_payment_method_types();
        $this->data['rsPPR'] = $this->Sales_model->fetch_pp_receivers();
        
        $this->data['message'] = $message;

        $this->data['view'] = 'admin/payment_methods' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function pprecvrdet(){
        $id =  $this->input->post_get('id') ?: 0;
        $userName = '';
        $disable = 0;
        $message = '';
        $fee = '';
        $desc =  $massPayDays = $apiSign = $apiUN = $apiPwd = $feeType = '';
        if ($this->input->post('txtUName'))
        {
            if($this->input->post('chkDisable'))
                $disable = 1;
            $userName = $this->input->post('txtUName');
            $apiUN = $this->input->post('txtAPIUN');
            $apiPwd = $this->input->post('txtAPIPwd');
            $apiSign = $this->input->post('txtAPISign');
            $massPayDays = $this->input->post('massPayDays') ?: '0';
            $fee = $this->input->post('txtFee');
            $feeType = $this->input->post('rdFeeType');

            $message = '';
          
            $row = $this->Sales_model->fetch_pp_receivers_by_username($userName , $disable ,$id);
           
            if (isset($row->ReceiverId) && $row->ReceiverId != '')
                $message = "'$userName'". $this->lang->line('BE_GNRL_10'); 
            else
            {
                if($id == 0)
                {
                    $insert_data = array(
                        'Username' => $userName ,
                        'DisablePP' => $disable,
                        'APIUsername' => $apiUN,
                        'APIPassword' => $apiPwd,
                        'APISignature' => $apiSign,
                        'MassPaymentTime' => $massPayDays,
                        'Fee' => $fee,
                        'FeeType' => $feeType
                    );

                    $this->Sales_model->insert_pp_receivers($insert_data);
                  
                }
                else
                {
                    $update_data = array(
                        'Username' => $userName ,
                        'DisablePP' => $disable,
                        'APIUsername' => $apiUN,
                        'APIPassword' => $apiPwd,
                        'APISignature' => $apiSign,
                        'MassPaymentTime' => $massPayDays,
                        'Fee' => $fee,
                        'FeeType' => $feeType
                    );

                    $where = array(
                        'ReceiverId' => $id
                    );

                    $this->Sales_model->update_pp_receivers($update_data ,  $where);

                  
                }
                
                $message = $this->lang->line('BE_GNRL_11');
            }

        }

        if($id > 0)
            {
                $row = $this->Sales_model->fetch_all_receivers_data_by_id($id);
              
                if (isset($row->ReceiverId) && $row->ReceiverId > 0)
                {
                    $userName = $row->Username;
                    $disable = $row->DisablePP;
                    $apiUN = stripslashes($row->APIUsername);
                    $apiPwd = stripslashes($row->APIPassword);
                    $apiSign = stripslashes($row->APISignature);
                    $massPayDays = $row->MassPaymentTime;
                    $fee = $row->Fee;
                    $feeType = $row->FeeType;
                }
            }

        $this->data['message'] = $message;
        $this->data['feeType'] = $feeType;
        $this->data['fee'] = $fee;
        $this->data['massPayDays'] = $massPayDays;
        $this->data['apiUN'] = $apiUN;
        $this->data['apiPwd'] = $apiPwd;
        $this->data['apiSign'] = $apiSign;
        $this->data['disable'] = $disable;
        $this->data['id'] = $id;
        $this->data['userName'] = $userName;
        // $this->data[''] = ;
        // $this->data[''] = ;
        $this->data['view'] = 'admin/pprecvrdet' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function paymentmethod(){
        $id = $this->input->post_get('id') ?: 0;
        $file =$errmessage=  $massPayDays = $rstCurrId = $pMethod = $userName = $message = $fee = $vat = $desc = $existingImage = $rFee = $sFee = $apiUN = $apiPwd = $apiSign = '';
        $disable = $feeType = $typeId = $wholeSale = $retail = $shop =  0;
        $THEME = 1;
        if ($this->input->post('txtPM'))
        {
			require(APPPATH.'libraries/FileManager.php');
			$filemanager = new FileManager();
			$fileExt = $filemanager->getFileExtension("txtImage");
			if($fileExt == '.JPG' || $fileExt == '.jpg' || $fileExt == '.jpeg' || $fileExt == '.png' || $fileExt == '.gif' || $fileExt == '')
			{
				$pMethod = $this->input->post('txtPM');
				if($this->input->post('chkDisable'))
					$disable = 1;
				$userName = $this->input->post('txtUName');
				$fee = $this->input->post('txtFee');
				$rFee = $this->input->post('txtRFee');
				$sFee = $this->input->post('txtSFee');
				$desc = $this->input->post('txtDesc');
				$apiUN = $this->input->post('txtAPIUN');
				$apiPwd = $this->input->post('txtAPIPwd');
				$apiSign = $this->input->post('txtAPISign');
				$typeId = $this->input->post('typeId');
				$wholeSale = $this->input->post('chkWS') ? 1 : 0;
				$retail = $this->input->post('chkRetail') ? 1 : 0;
				$shop = $this->input->post('chkShop') ? 1 : 0;
				$rstCurrId = $this->input->post('rstCurrId');
				$feeType = $this->input->post('rdFeeType');
				$vat = $this->input->post('txtVat');
				$existingImage = $this->input->post('existingImage');
				if(!is_numeric($vat))
					$vat  = 0;
				$massPayDays = $this->input->post('massPayDays') ?: '0';

				$message = '';

				$row = $this->Sales_model->fetch_payment_method_by_id($id ,  $pMethod , $disable , $typeId);
				if (isset($row->PaymentMethodId) && $row->PaymentMethodId != '')
					$message = "'$pMethod'".$this->lang->line('BE_GNRL_10');
				else
				{
					require(APPPATH.'libraries/FileManager.php');
					$filemanager = new FileManager();

					if($id == 0)
					{
						$insert_data = array(
							'Fee' => $fee ,
							'Vat' => $vat ,
							'RetailFee' => $rFee ,
							'ShopFee' => $sFee ,
							'PaymentMethod' => $pMethod,
							'Username' => $userName,
							'PayMethodTypeId' => $typeId ,
							'DisablePaymentMethod' => $disable,
							'Description' => $desc ,
							'APIUsername' => $apiUN,
							'APIPassword' => $apiPwd,
							'APISignature'=> $apiSign,
							'ForWholeSale' => $wholeSale,
							'ForRetail' => $retail,
							'ForShop' => $shop,
							'MassPaymentTime' => $massPayDays,
							'RestrictedCurrency' => $rstCurrId,
							'FeeType' => $feeType
						);
						$id = $this->Sales_model->insert_payment_methods($insert_data);

						if ($filemanager->getFileName('txtImage') != '')
							$file =	'payments/'.$id.$filemanager->getFileExtension("txtImage");
					}
					else
					{

						$update_data = array(
							'Fee' => $fee ,
							'Vat' => $vat ,
							'RetailFee' => $rFee ,
							'ShopFee' => $sFee ,
							'PaymentMethod' => $pMethod,
							'Username' => $userName,
							'PayMethodTypeId' => $typeId ,
							'DisablePaymentMethod' => $disable,
							'Description' => $desc ,
							'APIUsername' => $apiUN,
							'APIPassword' => $apiPwd,
							'APISignature'=> $apiSign,
							'ForWholeSale' => $wholeSale,
							'ForRetail' => $retail,
							'ForShop' => $shop,
							'MassPaymentTime' => $massPayDays,
							'RestrictedCurrency' => $rstCurrId,
							'FeeType' => $feeType
						);

						$where = array(
							'PaymentMethodId' => $id
						);

						$this->Sales_model->update_payment_methods($update_data ,  $where);

						if ($filemanager->getFileName('txtImage') != '')
							$file =	'payments/'.$id.$filemanager->getFileExtension("txtImage");
					}
					if($file != '')
					{
						$update_data = array(
							'PayImage' => $file
						);

						$where = array(
							'PaymentMethodId' => $id
						);

						$this->Sales_model->update_payment_methods($update_data ,  $where);

					}
					if ($filemanager->getFileName('txtImage') != '')
					{
						if($existingImage != '')
							@unlink(FCPATH."/uplds$THEME/$existingImage");
						$filemanager->uploadAs('txtImage', FCPATH."uplds$THEME/$file");
					}

					$message = $this->lang->line('BE_GNRL_11');
				}
			}else{
				//$this->session->set_flashdata('error_message', "The username or password is incorrect.");
				$errmessage ='Please upload allowed files';
			}

        }
        if($id > 0)
        {
            $row = $this->Sales_model->fetch_all_payment_data_by_id($id);
           
            if (isset($row->PaymentMethodId) && $row->PaymentMethodId > 0)
            {
                $pMethod = stripslashes($row->PaymentMethod);
                $userName = $row->Username;
                $disable = $row->DisablePaymentMethod;
                $fee = $row->Fee;
                $rFee = $row->RetailFee;
                $sFee = $row->ShopFee;
                $desc = stripslashes($row->Description);
                $vat = $row->Vat;
                $apiUN = stripslashes($row->APIUsername);
                $apiPwd = stripslashes($row->APIPassword);
                $apiSign = stripslashes($row->APISignature);
                $typeId = $row->PayMethodTypeId;
                $wholeSale = $row->ForWholeSale;
                $retail = $row->ForRetail;
                $shop = $row->ForShop;
                $massPayDays = $row->MassPaymentTime;
                $rstCurrId = $row->RestrictedCurrency;
                $feeType = $row->FeeType;
                if($row->PayImage != '')
                    $existingImage = "uplds$THEME/".$row->PayImage;
            }
        }

        $this->data['message'] =  $message ;
        $this->data['errmessage'] =  $errmessage ;
        $this->data['existingImage'] =   $existingImage;
        $this->data['id'] = $id  ;
        $this->data['pMethod'] = $pMethod   ;
        $this->data['feeType'] =  $feeType ;
        $this->data['fee'] =   $fee;
        $this->data['sFee'] =   $sFee;
        $this->data['apiUN'] =  $apiUN ;
        $this->data['apiPwd'] =  $apiPwd ;
        $this->data['apiSign'] =  $apiSign ;
        $this->data['desc'] =  $desc  ;
        $this->data['wholeSale'] =  $wholeSale ;
        $this->data['retail'] =   $retail;
        $this->data['shop'] =   $shop;
        $this->data['disable'] =  $disable;
        $this->data['typeId'] =  $typeId ;
        $this->data['rstCurrId'] =  $rstCurrId;
        $this->data['userName'] =   $userName;
        $this->data['vat'] =  $vat ;
        $this->data['rFee'] =  $rFee;
        $this->data['massPayDays'] = $massPayDays  ;
        //$this->data[''] =   ;
        //$this->data[''] =   ;
        $this->data['view']  = 'admin/payment_method' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }
    public function rinvoices(){
		$txtlqry = '';
		$name = '';
		$email = '';
		$uName = '';
		$strWhere1 = '';
		$strWhere = '';
		$message = '';
		$paymentId = '';
		$page_name = $_SERVER['PHP_SELF'];

		if(!isset($start))
			$start = 0;

		if($this->input->post_get('start')){
			$start = $this->input->post_get('start');
		}

		$eu = ($start - 0);
		$limit = $this->input->post('records') ? check_input($this->input->post('records'), $this->db->conn_id) : 50;
		$thisp = $eu + $limit;
		$back = $eu - $limit;
		$next = $eu + $limit;
		$dtFrom = ($this->input->post_get('txtFromDt')) ? check_input($this->input->post_get('txtFromDt'), $this->db->conn_id) : '';
		$dtTo = ($this->input->post_get('txtToDt')) ? check_input($this->input->post_get('txtToDt'), $this->db->conn_id) : '';
		$cldFrm = ($this->input->post_get('cldFrm')) ? check_input($this->input->post_get('cldFrm'), $this->db->conn_id) : 0;
		$pStatusId = ($this->input->post_get('pStatusId')) ? check_input($this->input->post_get('pStatusId'), $this->db->conn_id) : 0;
		if($pStatusId != '0')
		{
			$strWhere .= " AND A.PaymentStatus = '$pStatusId'";
			$txtlqry .= "&pStatusId=$pStatusId";
		}
		if(($this->input->post_get('pMethodId')))
		{
			if($cldFrm == '1')
			{
				$arrIds = ($this->input->post_get('chkPayments')) ? $this->input->post_get('chkPayments') : 0;
				$ids = implode(',', $arrIds);
				$this->db->query("DELETE FROM tbl_gf_retail_payments WHERE RetailPaymentId IN ($ids)");
				$message = 'Payment(s) have been deleted succesfully!';
			}

			$name = ($this->input->post_get('txtName')) ? trim(check_input($this->input->post_get('txtName'), $this->db->conn_id)) : '';
			$email = ($this->input->post_get('txtEmail')) ? trim(check_input($this->input->post_get('txtEmail'), $this->db->conn_id)) : '';
			$uName = ($this->input->post_get('txtUName')) ? trim(check_input($this->input->post_get('txtUName'), $this->db->conn_id)) : '';
			$pUName = ($this->input->post_get('txtPUName')) ? trim(check_input($this->input->post_get('txtPUName'), $this->db->conn_id)) : '';
			$paymentId = ($this->input->post_get('txtPaymentId')) ? trim(check_input($this->input->post_get('txtPaymentId'), $this->db->conn_id)) : '';
			$pMethodId = ($this->input->post_get('pMethodId')) ? check_input($this->input->post_get('pMethodId'), $this->db->conn_id) : 0;
			$invoicesBy = ($this->input->post_get('invoicesBy')) ? check_input($this->input->post_get('invoicesBy'), $this->db->conn_id) : 0;


			if($dtFrom != '')
				$txtlqry .= "&txtFromDt=$dtFrom";
			if($dtTo != '')
				$txtlqry .= "&txtToDt=$dtTo";
			if($dtFrom != '' && $dtTo != '')
				$strWhere .= " And DATE(PaymentDtTm) >= '$dtFrom' AND DATE(PaymentDtTm) <= '$dtTo'";
			else
			{
				if($dtFrom != '')
					$strWhere .= " AND DATE(PaymentDtTm) >= '$dtFrom'";
				if($dtTo!= '')
					$strWhere .= " AND DATE(PaymentDtTm) <= '$dtTo'";
			}
			if($name != '')
			{
				$strWhere .= " AND Name LIKE '%$name%'";
				$txtlqry .= "&txtName=$name";
			}
			if($email != '')
			{
				$strWhere .= " AND Email LIKE '%$email%'";
				$txtlqry .= "&txtEmail=$email";
			}
			if($paymentId != '')
			{
				$strWhere .= " AND RetailPaymentId LIKE '%$paymentId%'";
				$txtlqry .= "&txtPaymentId=$paymentId";
			}
			if($pMethodId != '0')
			{
				$strWhere .= " AND A.PaymentMethod = '$pMethodId'";
				$txtlqry .= "&pMethodId=$pMethodId";
			}
		}
		$qry = "	SELECT A.RetailPaymentId, Amount, A.Email, OrderPrice, A.Name, D.PaymentStatus, C.PaymentMethod, PaymentDtTm, A.Currency, RetailOrderId, D.PaymentStatusId, 
			IF(TransactionId IS NULL OR TransactionId = '', '-', TransactionId) AS TransactionId, IF(PayerEmail IS NULL OR PayerEmail = '', '-', PayerEmail) AS PayerEmail, 
			RetailOrderType	FROM tbl_gf_payment_status D, tbl_gf_retail_payments A, tbl_gf_payment_methods C, tbl_gf_retail_orders B WHERE A.PaymentMethod = C.PaymentMethodId 
			AND A.RetailPaymentId = B.PaymentId AND A.PaymentStatus = D.PaymentStatusId $strWhere ORDER BY RetailPaymentId DESC";
		$this->session->set_userdata('qryPayments',$qry);
		$rsPayments = $this->db->query($qry." LIMIT $eu, $limit");
		$count = $rsPayments->num_rows($rsPayments);


		$this->data['strWherePmnts'] =  '';
		$this->data['limit'] =  $limit;
		$this->data['dtTo'] =  $dtTo;
		$this->data['dtFrom'] =  $dtFrom;
		$this->data['paymentId'] =  $paymentId;
		$this->data['email'] =  $email;
		$this->data['name'] =  $name ;
		$this->data['message'] =  $message ;
		$this->data['count'] =  $count ;
		$this->data['IS_DEMO'] =  FALSE ;
		$this->data['view']  = 'admin/rinvoices' ;
		$this->load->view('admin/layouts/default1' , $this->data);
	}
	public function einvoices(){
		$txtlqry = '';
		$name = '';
		$email = '';
		$uName = '';
		$strWhere1 = '';
		$strWhere = '';
		$message = '';
		$paymentId = '';
		$page_name = $_SERVER['PHP_SELF'];

		if(!isset($start))
			$start = 0;

		if($this->input->post_get('start')){
			$start = $this->input->post_get('start');
		}

		$eu = ($start - 0);
		$limit = $this->input->post('records') ? check_input($this->input->post('records'), $this->db->conn_id) : 50;
		$thisp = $eu + $limit;
		$back = $eu - $limit;
		$next = $eu + $limit;
		$dtFrom = ($this->input->post_get('txtFromDt')) ? check_input($this->input->post_get('txtFromDt'), $this->db->conn_id) : '';
		$dtTo = ($this->input->post_get('txtToDt')) ? check_input($this->input->post_get('txtToDt'), $this->db->conn_id) : '';
		$cldFrm = ($this->input->post_get('cldFrm')) ? check_input($this->input->post_get('cldFrm'), $this->db->conn_id) : 0;
		$pStatusId = ($this->input->post_get('pStatusId')) ? check_input($this->input->post_get('pStatusId'), $this->db->conn_id) : 0;
		if($pStatusId != '0')
		{
			$strWhere .= " AND A.PaymentStatus = '$pStatusId'";
			$txtlqry .= "&pStatusId=$pStatusId";
		}
		if(($this->input->post_get('pMethodId')))
		{
			if($cldFrm == '1')
			{
				$arrIds = ($this->input->post_get('chkPayments')) ? $this->input->post_get('chkPayments') : 0;
				$ids = implode(',', $arrIds);
				$this->db->query("DELETE FROM tbl_gf_retail_payments WHERE RetailPaymentId IN ($ids)");
				$message = 'Payment(s) have been deleted succesfully!';
			}

			$name = ($this->input->post_get('txtName')) ? trim(check_input($this->input->post_get('txtName'), $this->db->conn_id)) : '';
			$email = ($this->input->post_get('txtEmail')) ? trim(check_input($this->input->post_get('txtEmail'), $this->db->conn_id)) : '';
			$uName = ($this->input->post_get('txtUName')) ? trim(check_input($this->input->post_get('txtUName'), $this->db->conn_id)) : '';
			$pUName = ($this->input->post_get('txtPUName')) ? trim(check_input($this->input->post_get('txtPUName'), $this->db->conn_id)) : '';
			$paymentId = ($this->input->post_get('txtPaymentId')) ? trim(check_input($this->input->post_get('txtPaymentId'), $this->db->conn_id)) : '';
			$pMethodId = ($this->input->post_get('pMethodId')) ? check_input($this->input->post_get('pMethodId'), $this->db->conn_id) : 0;
			$invoicesBy = ($this->input->post_get('invoicesBy')) ? check_input($this->input->post_get('invoicesBy'), $this->db->conn_id) : 0;


			if($dtFrom != '')
				$txtlqry .= "&txtFromDt=$dtFrom";
			if($dtTo != '')
				$txtlqry .= "&txtToDt=$dtTo";
			if($dtFrom != '' && $dtTo != '')
				$strWhere .= " And DATE(PaymentDtTm) >= '$dtFrom' AND DATE(PaymentDtTm) <= '$dtTo'";
			else
			{
				if($dtFrom != '')
					$strWhere .= " AND DATE(PaymentDtTm) >= '$dtFrom'";
				if($dtTo!= '')
					$strWhere .= " AND DATE(PaymentDtTm) <= '$dtTo'";
			}
			if($name != '')
			{
				$strWhere .= " AND Name LIKE '%$name%'";
				$txtlqry .= "&txtName=$name";
			}
			if($email != '')
			{
				$strWhere .= " AND Email LIKE '%$email%'";
				$txtlqry .= "&txtEmail=$email";
			}
			if($paymentId != '')
			{
				$strWhere .= " AND RetailPaymentId LIKE '%$paymentId%'";
				$txtlqry .= "&txtPaymentId=$paymentId";
			}
			if($pMethodId != '0')
			{
				$strWhere .= " AND A.PaymentMethod = '$pMethodId'";
				$txtlqry .= "&pMethodId=$pMethodId";
			}
		}
		$qry = "	SELECT A.RetailPaymentId, Amount, A.Email, OrderPrice, A.Name, D.PaymentStatus, C.PaymentMethod, PaymentDtTm, A.Currency, RetailOrderId, D.PaymentStatusId, 
			IF(TransactionId IS NULL OR TransactionId = '', '-', TransactionId) AS TransactionId, IF(PayerEmail IS NULL OR PayerEmail = '', '-', PayerEmail) AS PayerEmail, 
			RetailOrderType	FROM tbl_gf_payment_status D, tbl_gf_retail_payments A, tbl_gf_payment_methods C, tbl_gf_retail_orders B WHERE A.PaymentMethod = C.PaymentMethodId 
			AND A.RetailPaymentId = B.PaymentId AND A.PaymentStatus = D.PaymentStatusId $strWhere ORDER BY RetailPaymentId DESC";
		$this->session->set_userdata('qryPayments',$qry);
		$rsPayments = $this->db->query($qry." LIMIT $eu, $limit");
		$count = $rsPayments->num_rows($rsPayments);


		$this->data['strWherePmnts'] =  '';
		$this->data['limit'] =  $limit;
		$this->data['dtTo'] =  $dtTo;
		$this->data['dtFrom'] =  $dtFrom;
		$this->data['paymentId'] =  $paymentId;
		$this->data['email'] =  $email;
		$this->data['name'] =  $name ;
		$this->data['message'] =  $message ;
		$this->data['count'] =  $count ;
		$this->data['IS_DEMO'] =  FALSE ;
		$this->data['view']  = 'admin/rinvoices' ;
		$this->load->view('admin/layouts/default1' , $this->data);
	}

}

