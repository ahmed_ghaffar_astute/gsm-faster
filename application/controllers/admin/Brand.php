<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Brand extends MY_Controller {

    public function __construct(){
        parent::__construct();

        $this->load->helper('image'); 
        $this->load->model('Category_model');
        $this->load->model('common_model');
        $this->load->model('Brand_model');
    }

    function index(){

        $this->data['page_title'] = 'Brands';
        $this->data['current_page'] = 'Brands';
        
        $row=$this->Brand_model->get_list();

        $config = array();
        $config["base_url"] = base_url() . 'admin/brand';
        $config["total_rows"] = count($row);
        $config["per_page"] = 18;

        $config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;

        $config['enable_query_strings'] = TRUE;
        $config['page_query_string'] = FALSE;

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
         
        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
         
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
         
        $config['next_link'] = '';
        $config['next_tag_open'] = '<span class="nextlink">';
        $config['next_tag_close'] = '</span>';

        $config['prev_link'] = '';
        $config['prev_tag_open'] = '<span class="prevlink">';
        $config['prev_tag_close'] = '</span>';

        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        $page=($page-1) * $config["per_page"];

        if($this->input->post('search_value')!=''){
            $keyword=addslashes(trim($this->input->post('search_value')));

            $this->data['brand_list'] = $this->Brand_model->get_list('id','DESC', '', '', $keyword);
        }
        else{
            $this->data["links"] = $this->pagination->create_links();
            $this->data['brand_list'] = $this->Brand_model->get_list('id','DESC', $config['per_page'], $page);
        }
		$this->data['view'] = 'admin/page/brand';
        $this->load->view('admin/layouts/default1', $this->data); // :blush:
    }    

    function addForm()
    {
        $this->form_validation->set_rules('title', 'Enter Brand Title', 'trim|required');

        if($this->form_validation->run() == FALSE)
        {
            $messge = array('message' => 'Enter all required fields','class' => 'alert alert-danger');
                $this->session->set_flashdata('response_msg', $messge);
            redirect(base_url() . 'admin/brand/add', 'refresh');
        }
        else
        {

            if($_FILES['file_name']['error']!=4){
                $config['upload_path'] =  'assets/images/brand/';
                $config['allowed_types'] = 'jpg|png|jpeg|gif';

                $image = date('dmYhis').'_'.rand(0,99999).".".pathinfo($_FILES['file_name']['name'], PATHINFO_EXTENSION);

                $config['file_name'] = $image;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('file_name')) {
                    $messge = array('message' => $this->upload->display_errors(),'class' => 'alert alert-danger');
                    $this->session->set_flashdata('response_msg', $messge);

                    redirect(base_url() . 'admin/brand/add', 'refresh');
                } 
                else
                {  
                    $upload_data = $this->upload->data();
                }
            }
            else{
                $image='';
            }


            $this->load->helper("date");

            $data = array(
                'category_id' => implode(',', $this->input->post('category_id')),
                'brand_name' => $this->input->post('title'),
                'brand_image' => $image,
                'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
            );

            $data = $this->security->xss_clean($data);

            if($this->common_model->insert($data, 'tbl_brands')){
                $messge = array('message' => 'New brand added...','class' => 'alert alert-success');
                $this->session->set_flashdata('response_msg', $messge);

            }
            else{
                $messge = array('message' => 'Error in adding brand !','class' => 'alert alert-danger');
                $this->session->set_flashdata('response_msg', $messge);
            }

            redirect(base_url() . 'admin/brand/add', 'refresh');
        }
    }


    public function get_category()
    {
        $this->data['page_title'] = 'Brands';
        $this->data['current_page'] = 'Brands';
        $this->data['category_list'] = $this->Category_model->category_list();
		$this->data['view'] = 'admin/page/category';
        $this->load->view('admin/layouts/default1', $this->data); // :blush:
    }

    public function brand_form()
    {

        $id =  $this->uri->segment(4);

        $this->data['category_list'] = $this->Category_model->category_list();

        $this->data['page_title'] = 'Brands';
        if($id==''){
            $this->data['current_page'] = 'Add Brand';
        }
        else{
            $this->data['brand'] = $this->Brand_model->single_brand($id);

            $this->data['current_page'] = 'Edit Brand';
        }
		$this->data['view'] = 'admin/page/brand_form';
        $this->load->view('admin/layouts/default1' , $this->data); // :blush:
    }



    //-- update brand info
    public function editForm($id)
    {
        $data = $this->Brand_model->single_brand($id);

        if($_FILES['file_name']['error']!=4){

            if(file_exists('assets/images/brand/'.$data[0]->brand_image)){
                unlink('assets/images/brand/'.$data[0]->brand_image);
            }

            $config['upload_path'] =  'assets/images/brand/';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';

            $image = date('dmYhis').'_'.rand(0,99999).".".pathinfo($_FILES['file_name']['name'], PATHINFO_EXTENSION);

            $config['file_name'] = $image;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file_name')) {
                $messge = array('message' => $this->upload->display_errors(),'class' => 'alert alert-danger');
                $this->session->set_flashdata('response_msg', $messge);

                redirect(base_url() . 'admin/brand/edit/'.$id, 'refresh');
            }

        }
        else{
            $image=$data[0]->brand_image;
        }

        $this->load->helper("date");

        $data = array(
            'category_id' => implode(',', $this->input->post('category_id')),
            'brand_name' => $this->input->post('title'),
            'brand_image' => $image
        );

        $data = $this->security->xss_clean($data);

        if($this->common_model->update($data, $id,'tbl_brands')){
            $messge = array('message' => 'Brand updated...','class' => 'alert alert-success');
            $this->session->set_flashdata('response_msg', $messge);

        }
        else{
            $messge = array('message' => 'Error in updating brand !','class' => 'alert alert-danger');
            $this->session->set_flashdata('response_msg', $messge);
        }

        redirect(base_url() . 'admin/brand/edit/'.$id, 'refresh');
        
    }

    
    //-- active user
    public function active($id) 
    {
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'tbl_brands');
        echo $this->lang->line('enable_msg');
    }

    //-- deactive user
    public function deactive($id) 
    {
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'tbl_brands');
        echo $this->lang->line('disable_msg');
    }

    //-- delete category
    public function delete($id)
    {
        echo $this->Brand_model->delete($id);
    }


}
