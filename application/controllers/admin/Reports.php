<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reports extends MY_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('reports_model');

	}
	public function statistics(){

		$strWhere = '';
		$dtFrom = '';
		$dtTo = '';
		$today = date ("Y-m-d");
		$dtFrom = $this->input->post_get('txtFromDt') ? check_input($this->input->post_get('txtFromDt'), $this->db->conn_id) : '';
		$dtTo = $this->input->post_get('txtToDt') ? check_input($this->input->post_get('txtToDt'), $this->db->conn_id) : '';
		$check = $this->input->post_get('txtFromDt')?:'No';
		if($check != 'No')
		{
			$rsIMEISrchd = $this->reports_model->getIMEISrchd($dtTo,$dtFrom);
			$pending_Srchd = $rsIMEISrchd->P != '' ? $rsIMEISrchd->P : 0;
			$inProcess_Srchd = $rsIMEISrchd->IP != '' ? $rsIMEISrchd->IP : 0;
			$available_Srchd = $rsIMEISrchd->CA != '' ? $rsIMEISrchd->CA : 0;
			$notAvailable_Srchd = $rsIMEISrchd->NA != '' ? $rsIMEISrchd->NA : 0;

			$this->data['pending_Srchd']=$pending_Srchd;
			$this->data['inProcess_Srchd']=$inProcess_Srchd;
			$this->data['available_Srchd']=$available_Srchd;
			$this->data['notAvailable_Srchd']=$notAvailable_Srchd;

			$rsFileSrchd = $this->reports_model->getFileSrchd($dtTo,$dtFrom);
			$pending_Srchd_File = $rsFileSrchd->P != '' ? $rsFileSrchd->P : 0;
			$inProcess_Srchd_File = $rsFileSrchd->IP != '' ? $rsFileSrchd->IP : 0;
			$available_Srchd_File = $rsFileSrchd->CA != '' ? $rsFileSrchd->CA : 0;
			$notAvailable_Srchd_File = $rsFileSrchd->NA != '' ? $rsFileSrchd->NA : 0;

			$this->data['pending_Srchd_File']=$pending_Srchd_File;
			$this->data['inProcess_Srchd_File']=$inProcess_Srchd_File;
			$this->data['available_Srchd_File']=$available_Srchd_File;
			$this->data['notAvailable_Srchd_File']=$notAvailable_Srchd_File;

			$rsServerSrchd = $this->reports_model->getServerSrchd($dtTo,$dtFrom);
			$pending_Srchd_Server = $rsServerSrchd->P != '' ? $rsServerSrchd->P : 0;
			$inProcess_Srchd_Server = $rsServerSrchd->IP != '' ? $rsServerSrchd->IP : 0;
			$available_Srchd_Server = $rsServerSrchd->CA != '' ? $rsServerSrchd->CA : 0;
			$notAvailable_Srchd_Server = $rsServerSrchd->NA != '' ? $rsServerSrchd->NA : 0;

			$this->data['pending_Srchd_Server']=$pending_Srchd_Server;
			$this->data['inProcess_Srchd_Server']=$inProcess_Srchd_Server;
			$this->data['available_Srchd_Server']=$available_Srchd_Server;
			$this->data['notAvailable_Srchd_Server']=$notAvailable_Srchd_Server;

			$rsIMEIOrdersSrchd = $this->reports_model->getIMEIOrdersSrchd($dtTo,$dtFrom);
			$rsFileOrdersSrchd = $this->reports_model->getFileOrdersSrchd($dtTo,$dtFrom);
			$rsServerOrdersSrchd = $this->reports_model->getServerOrdersSrchd($dtTo,$dtFrom);

			$this->data['rsIMEIOrdersSrchd']=$rsIMEIOrdersSrchd;
			$this->data['rsFileOrdersSrchd']=$rsFileOrdersSrchd;
			$this->data['rsServerOrdersSrchd']=$rsServerOrdersSrchd;

			/*include 'stats_searchedordersummary.php';
			echo '<br />';
			include 'stats_searchedorders.php';
			echo '<br />';*/
		}

		//stats_todaysummary.php starts
		//getTodayReport()
		$rsIMEIToday = $this->reports_model->getTodayReport($today);
		$pending_Today = $rsIMEIToday->P != '' ? $rsIMEIToday->P : 0;
		$inProcess_Today = $rsIMEIToday->IP != '' ? $rsIMEIToday->IP : 0;
		$available_Today = $rsIMEIToday->CA != '' ? $rsIMEIToday->CA : 0;
		$notAvailable_Today = $rsIMEIToday->NA != '' ? $rsIMEIToday->NA : 0;

		//getTodayReport()
		$rsFileToday = $this->reports_model->getTodayFilesReport($today);
		$pending_Today_File = $rsFileToday->P != '' ? $rsFileToday->P : 0;
		$inProcess_Today_File = $rsFileToday->IP != '' ? $rsFileToday->IP : 0;
		$available_Today_File = $rsFileToday->CA != '' ? $rsFileToday->CA : 0;
		$notAvailable_Today_File = $rsFileToday->NA != '' ? $rsFileToday->NA : 0;

		//getTodayReport()
		$rsServerToday = $this->reports_model->getTodayserverReport($today);
		$pending_Today_Server = $rsServerToday->P != '' ? $rsServerToday->P : 0;
		$inProcess_Today_Server = $rsServerToday->IP != '' ? $rsServerToday->IP : 0;
		$available_Today_Server = $rsServerToday->CA != '' ? $rsServerToday->CA : 0;
		$notAvailable_Today_Server = $rsServerToday->NA != '' ? $rsServerToday->NA : 0;
		//stats_todaysummary.php ends

		//stats_todayorders queries start
		$rsIMEIOrdersToday = $this->reports_model->getTodayIMEIOrdersReport($today);
		$rsFileOrdersToday = $this->reports_model->getTodayFileOrdersReport($today);
		$rsServerOrdersToday = $this->reports_model->getTodayServerOrdersReport($today);
		//stats_todayorders queries end

		//stats_allordersummary queries start
		$rsIMEIAll = $this->reports_model->getTodayIMEIAllOrdersReport($today);
		$pending_All = $rsIMEIAll->P != '' ? $rsIMEIAll->P : 0;
		$inProcess_All = $rsIMEIAll->IP != '' ? $rsIMEIAll->IP : 0;
		$available_All = $rsIMEIAll->CA != '' ? $rsIMEIAll->CA : 0;
		$notAvailable_All = $rsIMEIAll->NA != '' ? $rsIMEIAll->NA : 0;

		$rsFileAll = $this->reports_model->getTodayFileAllOrdersReport($today);
		$pending_All_File = $rsFileAll->P != '' ? $rsFileAll->P : 0;
		$inProcess_All_File = $rsFileAll->IP != '' ? $rsFileAll->IP : 0;
		$available_All_File = $rsFileAll->CA != '' ? $rsFileAll->CA : 0;
		$notAvailable_All_File = $rsFileAll->NA != '' ? $rsFileAll->NA : 0;

		$rsServerAll = $this->reports_model->getTodayServerAllOrdersReport($today);
		$pending_All_Server = $rsServerAll->P != '' ? $rsServerAll->P : 0;
		$inProcess_All_Server = $rsServerAll->IP != '' ? $rsServerAll->IP : 0;
		$available_All_Server = $rsServerAll->CA != '' ? $rsServerAll->CA : 0;
		$notAvailable_All_Server = $rsServerAll->NA != '' ? $rsServerAll->NA : 0;
		//stats_allordersummary queries ENDS

		//stats_allorders queries start
		$rsIMEIOrdersAll = $this->reports_model->getIMEIOrdersAllReport($today);
		$rsFileOrdersAll = $this->reports_model->getFileOrdersAllReport($today);
		$rsServerOrdersAll = $this->reports_model->getServerOrdersAllReport($today);
		//stats_allorders queries Ends

		//stats_todaysummary.php starts
		$this->data['rsIMEIAll']=$rsIMEIAll;
		$this->data['rsFileAll']=$rsFileAll;
		$this->data['rsServerAll']=$rsServerAll;

		$this->data['rsIMEIOrdersAll']=$rsIMEIOrdersAll;
		$this->data['rsFileOrdersAll']=$rsFileOrdersAll;
		$this->data['rsServerOrdersAll']=$rsServerOrdersAll;

		$this->data['today']=$today;
		$this->data['strWhere']=$strWhere;
		$this->data['dtFrom']=$dtFrom;
		$this->data['dtTo']=$dtTo;

		$this->data['pending_Today']=$pending_Today;
		$this->data['inProcess_Today']=$inProcess_Today;
		$this->data['available_Today']=$available_Today;
		$this->data['notAvailable_Today']=$notAvailable_Today;

		$this->data['pending_Today_File']=$pending_Today_File;
		$this->data['inProcess_Today_File']=$inProcess_Today_File;
		$this->data['available_Today_File']=$available_Today_File;
		$this->data['notAvailable_Today_File']=$notAvailable_Today_File;

		$this->data['pending_Today_Server']=$pending_Today_Server;
		$this->data['inProcess_Today_Server']=$inProcess_Today_Server;
		$this->data['available_Today_Server']=$available_Today_Server;
		$this->data['notAvailable_Today_Server']=$notAvailable_Today_Server;
		//stats_todaysummary.php ends

		//stats_todayorders data Start
		$this->data['rsIMEIOrdersToday'] = $rsIMEIOrdersToday;
		$this->data['rsFileOrdersToday'] = $rsFileOrdersToday;
		$this->data['rsServerOrdersToday'] = $rsServerOrdersToday;
		//stats_todayorders data End

		//stats_allordersummary data start
		$this->data['pending_All'] = $pending_All;
		$this->data['inProcess_All'] = $inProcess_All;
		$this->data['available_All'] = $available_All;
		$this->data['notAvailable_All'] = $notAvailable_All;

		$this->data['pending_All_File'] = $pending_All_File;
		$this->data['inProcess_All_File'] = $inProcess_All_File;
		$this->data['available_All_File'] = $available_All_File;
		$this->data['notAvailable_All_File'] = $notAvailable_All_File;

		$this->data['pending_All_Server'] = $pending_All_Server;
		$this->data['inProcess_All_Server'] = $inProcess_All_Server;
		$this->data['available_All_Server'] = $available_All_Server;
		$this->data['notAvailable_All_Server'] = $notAvailable_All_Server;
		//stats_allordersummary data ends

		$this->data['IS_DEMO'] = false;
		$this->data['message'] = '';
		$this->data['check'] = $check;
		$this->data['view'] = 'admin/manage_statistics';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function eproductvisitors(){

		$strWhere = '';
		$dtFrom = '';
		$dtTo = '';
		$ip = '';
		$txtlqry = '';
		$sqlwhere = '';
		$page_name = $_SERVER['PHP_SELF'];

		if(!isset($start))
			$start = 0;

		if(isset($_REQUEST["start"]))
			$start = $_REQUEST["start"];

		$eu = ($start - 0);
		$limit = 150;
		$thisp = $eu + $limit;
		$back = $eu - $limit;
		$next = $eu + $limit;
		$message = '';
		$dtFrom = ($this->input->post_get('txtFromDt')) ? check_input($this->input->post_get('txtFromDt'), $this->db->conn_id) : '';
		$dtTo = ($this->input->post_get('txtToDt')) ? check_input($this->input->post_get('txtToDt'), $this->db->conn_id) : '';
		if($this->input->post_get('txtIP'))
		{
			$ip = trim($this->input->post_get('txtIP'));
			if($ip != '')
			{
				$strWhere .= " AND A.IP LIKE '%$ip%'";
				$txtlqry .= "&txtIP=$ip";
			}
			if($dtFrom != '' && $dtTo != '')
			{
				$strWhere .= " And DATE(VisitDtTm) >= '$dtFrom' AND DATE(VisitDtTm) <= '$dtTo'";
				$txtlqry .= "&txtFromDt=$dtFrom&txtToDt=$dtTo";
			}
			else
			{
				if($dtFrom != '')
				{
					$strWhere .= " AND DATE(VisitDtTm) >= '$dtFrom'";
					$txtlqry .= "&txtFromDt=$dtFrom";
				}
				if($dtTo!= '')
				{
					$strWhere .= " AND DATE(VisitDtTm) <= '$dtTo'";
					$txtlqry .= "&txtToDt=$dtTo";
				}
			}
		}

		$rsIpRpt = $this->reports_model->getData("SELECT COUNT(Id) AS TotalVisits, A.ProductId, ProductName FROM tbl_gf_product_visitors A, tbl_gf_products B WHERE A.ProductId = B.ProductId $strWhere
				GROUP BY A.ProductId ORDER BY TotalVisits DESC LIMIT $eu, $limit");
		$count = $this->reports_model->getData("SELECT COUNT(Id) AS TotalVisits, A.ProductId, ProductName FROM tbl_gf_product_visitors A, tbl_gf_products B WHERE A.ProductId = B.ProductId $strWhere
				GROUP BY A.ProductId ORDER BY TotalVisits DESC LIMIT $eu, $limit");
		$row = $this->reports_model->getRowData("SELECT COUNT(Id) AS TotalRecs FROM tbl_gf_product_visitors A WHERE (1) $strWhere");
		$totalRows = $row;

		$this->data['IS_DEMO'] = false;
		$this->data['message'] = '';
		$this->data['ip'] = $ip;
		$this->data['dtFrom'] = $dtFrom;
		$this->data['dtTo'] = $dtTo;
		$this->data['txtlqry'] = $txtlqry;
		$this->data['limit'] = $limit;
		$this->data['count'] = $count;
		$this->data['totalRows'] = $totalRows;
		$this->data['rsIpRpt'] = $rsIpRpt;
		$this->data['view'] = 'admin/manage_eproduct_visitors';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function eprodvisitdtls(){

		$productId =  ($this->input->post_get('pId')) ? check_input($this->input->post_get('pId'), $this->db->conn_id) : 0;
		$prodName =  ($this->input->post_get('pn')) ? check_input($this->input->post_get('pn'), $this->db->conn_id) : '';
		$message = '';
		$del =  ($this->input->post_get('del')) ? 1 : 0;
		if($del == '1')
		{
			$id =  ($this->input->post_get('id')) ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
			$this->reports_model->delteData("DELETE FROM tbl_gf_product_visitors WHERE Id = '$id'");
			$message =  $this->lang->line('BE_GNRL_12');
		}
		$rsIpRpt = $this->reports_model->getData("SELECT * FROM tbl_gf_product_visitors WHERE ProductId = '$productId' ORDER BY Id DESC");
		$count =$this->reports_model->getNumData("SELECT * FROM tbl_gf_product_visitors WHERE ProductId = '$productId' ORDER BY Id DESC");

		$this->data['count'] = $count;
		$this->data['message'] = $message;
		$this->data['productId'] = $productId;
		$this->data['prodName'] = $prodName;
		$this->data['rsIpRpt'] = $rsIpRpt;
		$this->data['view'] = 'admin/eproduct_visit_details';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public  function monthlyordersrpt(){

		$message = '';
		$sc = ($this->input->post_get('sc')) ? check_input($this->input->post_get('sc'), $this->db->conn_id) : 0;
		switch($sc)
		{
			case '0': // IMEI Orders
				$tblOrders = 'tbl_gf_codes';
				$statusCol = 'CodeStatusId';
				$heading = $this->lang->line('BE_LBL_246');
				break;
			case '1': // File Orders
				$tblOrders = 'tbl_gf_codes_slbf';
				$statusCol = 'CodeStatusId';
				$heading = $this->lang->line('BE_LBL_249');
				break;
			case '2': // File Orders
				$tblOrders = 'tbl_gf_log_requests';
				$statusCol = 'StatusId';
				$heading = $this->lang->line('BE_LBL_252');
				break;
			case '3': // Retail Orders
				$tblOrders = 'tbl_gf_retail_orders';
				$statusCol = 'OrderStatusId';
				$heading = $this->lang->line('BE_LBL_590');
				break;
		}
		$rsRpt = $this->reports_model->getData("SELECT MONTHNAME(RequestedAt) AS OrdersMonth, YEAR(RequestedAt) AS OrdersYear,
					SUM( IF( $statusCol = '1', 1, 0 ) ) AS `P`, SUM( IF( $statusCol = '2', 1, 0 ) ) AS `CA`,
					SUM( IF( $statusCol = '3', 1, 0 ) ) AS `NA`, SUM( IF( $statusCol = '4', 1, 0 ) ) AS `IP` FROM $tblOrders
					GROUP BY MONTH(RequestedAt) ORDER BY RequestedAt");

		$this->data['sc'] = $sc;
		$this->data['message'] = $message;
		$this->data['tblOrders'] = $tblOrders;
		$this->data['statusCol'] = $statusCol;
		$this->data['heading'] = $heading;
		$this->data['rsRpt'] = $rsRpt;
		$this->data['view'] = 'admin/monthly_orders_report';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function apisrpt(){

		$ARR_SRVCS = array();
		$ARR_F_SRVCS = array();
		$ARR_S_SRVCS = array();
		$SERVICE_PRICES = array();
		$FILE_SERVICE_PRICES = array();
		$SRVR_SRVC_PRICES = array();
		$apiData = 0;
		$rsAPIs = $this->reports_model->getData("SELECT APIId, APITitle, AccountId, ServerURL, APIKey, APIType, APIKey2, APIPassword FROM tbl_gf_api WHERE DisableAPI = '0' AND APIInUse = '1' ORDER BY APITitle");

		$rsAPIServcs = $this->reports_model->getData("SELECT APIId, sl3lbf, COUNT(PackageId) AS TotalPacks FROM tbl_gf_packages WHERE APIId > 0 AND DisablePackage = 0 AND ArchivedPack = 0 GROUP BY APIId, sl3lbf");
		foreach( $rsAPIServcs as $row)
		{
			if($row->sl3lbf == '0')
				$ARR_SRVCS[$row->APIId] = $row->TotalPacks;
			else if($row->sl3lbf == '1')
				$ARR_F_SRVCS[$row->APIId] = $row->TotalPacks;
		}

		$rsAPISServcs = $this->reports_model->getData("SELECT APIId, COUNT(LogPackageId) AS TotalPacks FROM tbl_gf_log_packages WHERE APIId > 0 AND DisableLogPackage = 0 AND ArchivedPack = 0 GROUP BY APIId");

		foreach($rsAPISServcs as $row)
		{
			$ARR_S_SRVCS[$row->APIId] = $row->TotalPacks;
		}
//===================================== GET ALL PRICES AGAINST SERVICES TO CHECK WITH COST PRICES ==============================//
		$this->data['ARR_S_SRVCS'] = $ARR_S_SRVCS;
		$this->data['ARR_F_SRVCS'] = $ARR_F_SRVCS;
		$this->data['ARR_SRVCS'] = $ARR_SRVCS;
		$this->data['FILE_SERVICE_PRICES'] = $FILE_SERVICE_PRICES;
		$this->data['SERVICE_PRICES'] = $SERVICE_PRICES;
		$this->data['SRVR_SRVC_PRICES'] = $SRVR_SRVC_PRICES;
		$this->data['apiData'] = $apiData;
		$this->data['rsAPIs'] = $rsAPIs;
		$this->data['view'] = 'admin/api_stats';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function profitlossrpt(){

		$strWhere = '';
		$dtFrom = '';
		$dtTo = '';
		$DEFAULT_CURRENCY = '';
		$qtyCol = '';

		$sc = ($this->input->post_get('sc')) ?: '0';
		$token = ($this->input->post_get('csrf_test_name')) ? check_input($this->input->post_get('csrf_test_name'), $this->db->conn_id) : 0;
		$categoryId = ($this->input->post_get('categoryId')) ? check_input($this->input->post_get('categoryId'), $this->db->conn_id) : 0;
		$serviceId = ($this->input->post_get('serviceId')) ? check_input($this->input->post_get('serviceId'), $this->db->conn_id) : '0';
		$dtFrom = ($this->input->post_get('txtFromDt')) ? check_input($this->input->post_get('txtFromDt'), $this->db->conn_id) : '';
		$dtTo = ($this->input->post_get('txtToDt')) ? check_input($this->input->post_get('txtToDt'), $this->db->conn_id) : '';
		$rwCrncy = $this->reports_model->getSingleRowData("SELECT CurrencyAbb FROM tbl_gf_currency WHERE DefaultCurrency = 1 AND DisableCurrency = 0");
		if ( $rwCrncy->CurrencyAbb && $rwCrncy->CurrencyAbb != '' )
		{
			$DEFAULT_CURRENCY = $rwCrncy->CurrencyAbb;
		}
		switch($sc)
		{
			case '0': // IMEI services
				$strWhereSrv = " AND sl3lbf = 0";
				$strWhereCat = " AND SL3BF = 0";
				$tblName = 'tbl_gf_package_category';
				$tblPckName = 'tbl_gf_packages';
				$colId = 'PackageId';
				$colTitle = 'PackageTitle';
				$disableCol = 'DisablePackage';
				$tblOrders = 'tbl_gf_codes';
				$orderCol = 'PackageTitle';
				$statusCol = 'CodeStatusId';
				$heading = $this->lang->line('BE_MENU_PCKGS');
				break;
			case '1': // File services
				$strWhereSrv = " AND sl3lbf = 1";
				$strWhereCat = " AND SL3BF = 1";
				$tblName = 'tbl_gf_package_category';
				$tblPckName = 'tbl_gf_packages';
				$colId = 'PackageId';
				$colTitle = 'PackageTitle';
				$disableCol = 'DisablePackage';
				$tblOrders = 'tbl_gf_codes_slbf';
				$orderCol = 'PackageTitle';
				$statusCol = 'CodeStatusId';
				$heading = $this->lang->line('BE_PCK_25');
				break;
			case '2': // Server services
				$strWhereSrv = '';
				$strWhereCat = '';
				$tblName = 'tbl_gf_log_package_category';
				$tblPckName = 'tbl_gf_log_packages';
				$colId = 'LogPackageId';
				$colTitle = 'LogPackageTitle';
				$disableCol = 'DisableLogPackage';
				$tblOrders = 'tbl_gf_log_requests';
				$orderCol = 'LogPackageTitle';
				$statusCol = 'StatusId';
				$heading = $this->lang->line('BE_LBL_299');
				break;
		}

		if($this->input->post_get('categoryId'))
		{
			if($categoryId != '0')
				$strWhere .= " AND CategoryId = '$categoryId'";
			if($serviceId != '0')
				$strWhere .= " AND A.$colId = '$serviceId'";
			if($dtFrom != '' && $dtTo != '')
				$strWhere .= " And DATE(RequestedAt) >= '" . $dtFrom . "' AND DATE(RequestedAt) <= '" . $dtTo . "'";
			else
			{
				if($dtFrom != '')
					$strWhere .= " AND DATE(RequestedAt) >= '" . $dtFrom . "'";
				if($dtTo!= '')
					$strWhere .= " AND DATE(RequestedAt) <= '" . $dtTo . "'";
			}
		}
		$qryRpt = "SELECT A.$colId, $colTitle, ";
		/*if($sc == '2')
		{
			$qryRpt .= "SUM( IF( quantity_G7V7 > 1, A.CostPrice * quantity_G7V7, A.CostPrice) ) AS PackCostPrice";
		}
		else*/
		$qryRpt .= "SUM(A.CostPrice) AS PackCostPrice ";
		$qryRpt .= ", SUM(OrderCostPrice) AS OrderCostPrice, SUM(Profit) AS ProfitLoss $qtyCol FROM $tblOrders A, $tblPckName B WHERE A.$colId = B.$colId AND A.CostPrice > 0
			AND OrderCostPrice > 0 AND $statusCol = '2' $strWhere GROUP BY A.$colId ORDER BY $orderCol";
		$rsRpt = $this->reports_model->getData($qryRpt);

		$fileComboResult2 = $this->reports_model->getData("SELECT $colId AS Id, $colTitle AS Value FROM $tblPckName WHERE $disableCol = 0 $strWhereSrv AND CategoryId = '$categoryId' AND ArchivedPack = 0 ORDER BY PackOrderBy, $colTitle");

		$fileComboResult = $this->reports_model->getData("SELECT CategoryId AS Id, Category AS Value FROM $tblName WHERE DisableCategory = 0 AND ArchivedCategory = 0 $strWhereCat ORDER BY OrderBy, Category");

		$this->data['DEFAULT_CURRENCY'] = $DEFAULT_CURRENCY;
		$this->data['fileComboResult'] = $fileComboResult;
		$this->data['fileComboResult2'] = $fileComboResult2;
		$this->data['strWhereSrv'] = $strWhereSrv;
		$this->data['disableCol'] = $disableCol;
		$this->data['tblPckName'] = $tblPckName;
		$this->data['colTitle'] = $colTitle;
		$this->data['colId'] = $colId;
		$this->data['serviceId'] = $serviceId;
		$this->data['tblName'] = $tblName;
		$this->data['strWhereCat'] = $strWhereCat;
		$this->data['categoryId'] = $categoryId;
		$this->data['dtFrom'] = $dtFrom;
		$this->data['dtTo'] = $dtTo;
		$this->data['sc'] = $sc;
		$this->data['tblOrders'] = $tblOrders;
		$this->data['statusCol'] = $statusCol;
		$this->data['heading'] = $heading;
		$this->data['rsRpt'] = $rsRpt;
		$this->data['view'] = 'admin/profitlossrpt';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function ajxgeneral(){
		$purpose = $this->input->post_get('purpose');
		$strWhere='';
		if ($purpose == 'getsrvcs')
		{
			$categoryId = ($this->input->post_get('categoryId')) ? check_input($this->input->post_get('categoryId'), $this->db->conn_id) : 0;
			$sc = ($this->input->post_get('sc')) ? check_input($this->input->post_get('sc'), $this->db->conn_id) : 0;
			switch($sc)
			{
				case '0': // IMEI services
					$strWhere .= " AND sl3lbf = 0";
					$tblPckName = 'tbl_gf_packages';
					$colId = 'PackageId';
					$colTitle = 'PackageTitle';
					$disableCol = 'DisablePackage';
					$sortCol = 'PackOrderBy';
					break;
				case '1': // File services
					$strWhere .= " AND sl3lbf = 1";
					$tblPckName = 'tbl_gf_packages';
					$colId = 'PackageId';
					$colTitle = 'PackageTitle';
					$disableCol = 'DisablePackage';
					$sortCol = 'PackOrderBy';
					break;
				case '2': // File services
					$tblPckName = 'tbl_gf_log_packages';
					$colId = 'LogPackageId';
					$colTitle = 'LogPackageTitle';
					$disableCol = 'DisableLogPackage';
					$sortCol = 'PackOrderBy';
					break;
				case '3': // Retail services
					$tblPckName = 'tbl_gf_retail_services';
					$colId = 'PackageId';
					$colTitle = 'PackageTitle';
					$disableCol = 'DisablePackage';
					$sortCol = 'OrderBy';
					break;
			}
			$rsSrvcs = $this->reports_model->getData("SELECT $colId AS Id, $colTitle AS Value FROM $tblPckName WHERE $disableCol = 0 $strWhere AND CategoryId = '$categoryId' ORDER BY $sortCol, $colTitle");
			$msg = "<option value='0' selected='selected'>Please Choose Service...</option>";
			foreach($rsSrvcs as $row)
			{
				$msg .= "<option value='".$row->Id."'>".stripslashes($row->Value)."</option>";
			}
		}
		if ($purpose == 'rmvprf')
		{
			$orderNo = ($this->input->post_get('orderNo')) ? check_input($this->input->post_get('orderNo'), $this->db->conn_id) : 0;
			$sc = ($this->input->post_get('sc')) ? check_input($this->input->post_get('sc'), $this->db->conn_id) : 0;
			switch($sc)
			{
				case '0':
					$tbl = 'tbl_gf_codes';
					$colId = 'CodeId';
					break;
				case '1':
					$tbl = 'tbl_gf_codes_slbf';
					$colId = 'CodeId';
					break;
				case '2':
					$tbl = 'tbl_gf_log_requests';
					$colId = 'LogRequestId';
					break;
			}
			$this->reports_model->updateData("UPDATE $tbl SET Profit = 0 WHERE $colId = '$orderNo'");
			$msg = "Profit has been removed for Order No. $orderNo";
		}

		echo $msg;
		exit;
	}
	public function creditsrpt(){

		$strWhere = '';
		$userName = '';
		$email = '';
		$maxCr = $minCr = $strIPCredits = $strCreditsUsed = $strAvCredits = $txtlqry = '';
		$userId = ($this->input->post_get('userId')) ? check_input($this->input->post_get('userId'), $this->db->conn_id) : 0;
		if($this->input->post_get('txtEmail'))
		{
			$userName = check_input($this->input->post_get('txtUName'), $this->db->conn_id);
			$email = trim(check_input($this->input->post_get('txtEmail'), $this->db->conn_id));
			$minCr = ($this->input->post_get('txtMinCr')) ? check_input($this->input->post_get('txtMinCr'), $this->db->conn_id) : '';
			$maxCr = ($this->input->post_get('txtMaxCr')) ? check_input($this->input->post_get('txtMaxCr'), $this->db->conn_id) : '';
			if($email != '')
			{
				$strWhere .= " AND UserEmail LIKE '%$email%'";
				$txtlqry .= "&txtEmail=$email";
			}
			if($userName != '')
			{
				$strWhere .= " AND UserName LIKE '%$userName%'";
				$txtlqry .= "&txtUName=$userName";
			}
		}
		if($userId > 0)
		{
			$strWhere .= " AND A.UserId = '$userId'";
			$txtlqry .= "&userId=$userId";
		}
		if($this->input->post_get('txtEmail') || $userId > 0)
		{
			$this->data['rsUsers'] = $rsUsers = $this->reports_model->getData("SELECT UserId, UserName, UserEmail, Credits, CONCAT(A.FirstName, ' ', A.LastName) AS Name, DisableUser, CurrencyAbb
					FROM tbl_gf_users A, tbl_gf_currency B WHERE A.CurrencyId = B.CurrencyId $strWhere ORDER BY UserName");
		}

		$CREDITS_USED = 0;
		$AVAIL_CREDITS = 0;
		$IP_CREDITS = 0;
		$INPROCESS_IMEI_ARR = array();
		$INPROCESS_FILE_ARR = array();
		$INPROCESS_SRVR_ARR = array();

		$COMPLETED_IMEI_ARR = array();
		$COMPLETED_FILE_ARR = array();
		$COMPLETED_SRVR_ARR = array();
		$rsProcess_IMEI = $this->reports_model->getData("SELECT UserId, SUM(Credits) AS LockedAmount FROM tbl_gf_codes WHERE CodeStatusId IN (1, 4) GROUP BY UserId");
		foreach($rsProcess_IMEI as $row)
		{
				$INPROCESS_IMEI_ARR[$row->UserId] = $row->LockedAmount;
		}
		$rsProcess_FILE = $this->reports_model->getData("SELECT UserId, SUM(Credits) AS LockedAmount FROM tbl_gf_codes_slbf WHERE CodeStatusId IN (1, 4) GROUP BY UserId");
		foreach($rsProcess_FILE as $row)
		{

				$INPROCESS_FILE_ARR[$row->UserId] = $row->LockedAmount;
		}
		$rsProcess_SERVER = $this->reports_model->getData("SELECT UserId, SUM(Credits) AS LockedAmount FROM tbl_gf_log_requests WHERE StatusId IN (1, 4) GROUP BY UserId");
		foreach($rsProcess_SERVER as $row)
		{
				$INPROCESS_SRVR_ARR[$row->UserId] = $row->LockedAmount;
		}
		$rsCreditsUsed_IMEI = $this->reports_model->getData("SELECT UserId, SUM(Credits) AS LockedAmount FROM tbl_gf_codes WHERE CodeStatusId = 2 GROUP BY UserId");
		foreach($rsCreditsUsed_IMEI as $row)
		{
				$COMPLETED_IMEI_ARR[$row->UserId] = $row->LockedAmount;
		}
		$rsCreditsUsed_FILE = $this->reports_model->getData("SELECT UserId, SUM(Credits) AS LockedAmount FROM tbl_gf_codes_slbf WHERE CodeStatusId = 2 GROUP BY UserId");
		foreach($rsCreditsUsed_FILE as $row)
		{
				$COMPLETED_FILE_ARR[$row->UserId] = $row->LockedAmount;
		}
		$rsCreditsUsed_SERVER = $this->reports_model->getData("SELECT UserId, SUM(Credits) AS LockedAmount FROM tbl_gf_log_requests WHERE StatusId = 2 GROUP BY UserId");
		foreach($rsCreditsUsed_SERVER as $row)
		{
				$COMPLETED_SRVR_ARR[$row->UserId] = $row->LockedAmount;
		}
		/*$crypt = new crypt;*/
		$rsTotalUsers = $this->reports_model->getData("SELECT A.CurrencyId, UserId, Credits, DisableUser, CurrencyAbb FROM tbl_gf_users A, tbl_gf_currency B WHERE A.CurrencyId = B.CurrencyId
							$strWhere ORDER BY A.CurrencyId");
		$prevCurrId = 0;
		foreach($rsTotalUsers as $row)
		{
			//var_dump(in_array($row->UserId, $INPROCESS_IMEI_ARR),array_search($row->UserId, key($INPROCESS_IMEI_ARR)),$row->UserId.' => ',$INPROCESS_IMEI_ARR);

			crypt_key($row->UserId);
			$amount = 0;
			$myCredits = decrypt($row->Credits);
			if($myCredits == '')
				$myCredits = '0';
			else
				$myCredits = roundMe($myCredits);

			if($prevCurrId > 0)
			{
				if($row->CurrencyId != $prevCurrId)
				{
					if($AVAIL_CREDITS != '' && $AVAIL_CREDITS != 0 )
						$strAvCredits .= '<font style="color:#0044CC; font-size:22px;">'.roundMe($AVAIL_CREDITS).' '.$prevCurr.'</font><br />';
					if($IP_CREDITS != '' && $IP_CREDITS != 0 )
						$strIPCredits .= '<font style="color:#0044CC; font-size:22px;">'.roundMe($IP_CREDITS).' '.$prevCurr.'</font><br />';
					if($CREDITS_USED != '' && $CREDITS_USED != 0 )
						$strCreditsUsed .= '<font style="color:#0044CC; font-size:22px;">'.roundMe($CREDITS_USED).' '.$prevCurr.'</font><br />';
					$CREDITS_USED = 0;
					$AVAIL_CREDITS = 0;
					$IP_CREDITS = 0;
				}
			}
			else
			{
				if($AVAIL_CREDITS != '' && $AVAIL_CREDITS != 0 )
					$strAvCredits .= '<font style="color:#0044CC; font-size:22px;">'.$AVAIL_CREDITS.' '.$prevCurr.'</font><br />';
				if($IP_CREDITS != '' && $IP_CREDITS != 0 )
					$strIPCredits .= '<font style="color:#0044CC; font-size:22px;">'.$IP_CREDITS.' '.$prevCurr.'</font><br />';
				if($CREDITS_USED != '' && $CREDITS_USED != 0 )
					$strCreditsUsed .= '<font style="color:#0044CC; font-size:22px;">'.$CREDITS_USED.' '.$prevCurr.'</font><br />';
				$CREDITS_USED = 0;
				$AVAIL_CREDITS = 0;
				$IP_CREDITS = 0;
			}

			if ( $row->UserId == key($INPROCESS_IMEI_ARR) && $row->UserId == key($INPROCESS_IMEI_ARR) && $row->UserId== key($INPROCESS_SRVR_ARR)) {
			$inProcessCredits = $INPROCESS_IMEI_ARR[$row->UserId] +  $INPROCESS_FILE_ARR[$row->UserId] +  $INPROCESS_SRVR_ARR[$row->UserId];
				$IP_CREDITS += roundMe($inProcessCredits);
		}

			if ( $row->UserId== key($COMPLETED_IMEI_ARR) && $row->UserId== key($COMPLETED_FILE_ARR) && $row->UserId== key($COMPLETED_SRVR_ARR)) {
				$usedCredits =  $COMPLETED_IMEI_ARR[$row->UserId] +  $COMPLETED_FILE_ARR[$row->UserId] +  $COMPLETED_SRVR_ARR[$row->UserId];
				$CREDITS_USED += roundMe($usedCredits);
			}
			if($minCr != '' && $maxCr != '')
			{
				if(is_numeric($minCr) && is_numeric($maxCr))
				{
					if($myCredits >= $minCr && $myCredits <= $maxCr)
						$AVAIL_CREDITS += $myCredits;
				}
			}
			else if($minCr != '' || $maxCr != '')
			{
				if($minCr != '' && is_numeric($minCr))
				{
					if($myCredits >= $minCr)
						$AVAIL_CREDITS += $myCredits;
				}
				if($maxCr != '' && is_numeric($maxCr))
				{
					if($myCredits <= $maxCr)
						$AVAIL_CREDITS += $myCredits;
				}
			}
			else if($minCr == '' && $maxCr == '')
				$AVAIL_CREDITS += $myCredits;

			$prevCurrId = $row->CurrencyId;
			$prevCurr = $row->CurrencyAbb;
		}
		if($AVAIL_CREDITS != '' && $AVAIL_CREDITS != 0 )
			$strAvCredits .= '<font style="color:#0044CC; font-size:22px;">'.roundMe($AVAIL_CREDITS).' '.$prevCurr.'</font><br />';
		if($IP_CREDITS != '' && $IP_CREDITS != 0 )
			$strIPCredits .= '<font style="color:#0044CC; font-size:22px;">'.roundMe($IP_CREDITS).' '.$prevCurr.'</font><br />';
		if($CREDITS_USED != '' && $CREDITS_USED != 0 )
			$strCreditsUsed .= '<font style="color:#0044CC; font-size:22px;">'.roundMe($CREDITS_USED).' '.$prevCurr.'</font><br />';

		//userbtns file data
		$dUser = 0;
		$USER_ID_BTNS = $userId;
		$rwUN = $this->reports_model->getSingleRowData("SELECT UserName, DisableUser FROM tbl_gf_users WHERE UserId = '$USER_ID_BTNS'");
		/*$rwUN = $objDBCD14->queryUniqueObject();*/
		if ($rwUN != false)
		{
			$userName = $rwUN->UserName;
			$dUser = $rwUN->DisableUser;
		}
		//userbtns file data END
		$this->data['email'] = $email;
		$this->data['userName'] = $userName;
		$this->data['dUser'] = $dUser;
		$this->data['rwUN'] = $rwUN;
		$this->data['minCr'] = $minCr;
		$this->data['maxCr'] = $maxCr;
		$this->data['COMPLETED_SRVR_ARR'] = $COMPLETED_SRVR_ARR;
		$this->data['COMPLETED_FILE_ARR'] = $COMPLETED_FILE_ARR;
		$this->data['INPROCESS_SRVR_ARR'] = $INPROCESS_SRVR_ARR;
		$this->data['INPROCESS_FILE_ARR'] = $INPROCESS_FILE_ARR;
		$this->data['COMPLETED_IMEI_ARR'] = $COMPLETED_IMEI_ARR;
		$this->data['INPROCESS_IMEI_ARR'] = $INPROCESS_IMEI_ARR;
		$this->data['strAvCredits'] = $strAvCredits;
		$this->data['strIPCredits'] = $strIPCredits;
		$this->data['strCreditsUsed'] = $strCreditsUsed;
		$this->data['userId'] = $userId;
		// $this->data['userbtns'] = $this->load->view ('scripts/userbtns', $this->data, TRUE);
		$this->data['view'] = 'admin/creditsrpt';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function clientsbyincomerpt(){
		$message = '';
		$strWhere = '';
		if($this->input->post_get('txtEmail'))
		{
			$userName = check_input( $this->input->post_get('txtUName'), $this->db->conn_id);
			$email = trim(check_input( $this->input->post_get('txtEmail'), $this->db->conn_id));
			if($email != '')
				$strWhere = " AND UserEmail LIKE '%$email%'";
			if($userName != '')
				$strWhere .= " AND UserName LIKE '%$userName%'";
		}
		$USER_AMOUNTS = array();
		$rsInvoices = $this->reports_model->getData("SELECT A.UserId, A.Amount, UserName FROM tbl_gf_payments A, tbl_gf_users B WHERE A.UserId = B.UserId AND PaymentStatus = '2' $strWhere ORDER BY UserName");

		$prevUserId = $uName = $email= '';
		$totalAmount = 0;
		foreach($rsInvoices as $row)
		{
			crypt_key($row->UserId);
			$invoiceAmount = decrypt($row->Amount);
			if($prevUserId != '')
			{
				if($row->UserName != $prevUserId)
				{
					$USER_AMOUNTS[$prevUserId] = $totalAmount;
					$totalAmount = 0;
				}
			}
			else
			{
				$USER_AMOUNTS[$row->UserName] = $invoiceAmount;
			}
			$totalAmount += $invoiceAmount;
			$prevUserId = $row->UserName;
			$USER_AMOUNTS[$row->UserName] = $totalAmount;
		}
		$rsUsers = $this->reports_model->getData("SELECT A.UserId, UserName, CurrencySymbol, UserEmail, CONCAT(A.FirstName, ' ', A.LastName) AS Name FROM tbl_gf_users A, tbl_gf_currency B WHERE A.CurrencyId = B.CurrencyId $strWhere");
		$USERS_CURRENCY = array();

		$this->data['uName'] = $uName;
		$this->data['email'] = $email;
		$this->data['userId'] = '';
		$this->data['message'] = $message;
		$this->data['rsInvoices'] = $rsInvoices;
		$this->data['USER_AMOUNTS'] = $USER_AMOUNTS;
		$this->data['USERS_CURRENCY'] = $USERS_CURRENCY;
		$this->data['rsUsers'] = $rsUsers;
		$this->data['view'] = 'admin/clientsbyincomerpt';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function precodeshistory(){
		$strWhere = '';
		$dtFrom = '';
		$dtTo = '';
		$userName = '';
		$service = '';
		$txtlqry = '';
		$sqlwhere = '';
		$page_name = $_SERVER['PHP_SELF'];

		if(!isset($start))
			$start = 0;

		if(isset($_REQUEST["start"]))
			$start = $_REQUEST["start"];

		$eu = ($start - 0);
		$limit = 150;
		$thisp = $eu + $limit;
		$back = $eu - $limit;
		$next = $eu + $limit;
		$message = '';
		$userId = ($this->input->post_get('userId')) ? check_input($this->input->post_get('userId'), $this->db->conn_id) : 0;
		$dtFrom = ($this->input->post_get('txtFromDt')) ? check_input($this->input->post_get('txtFromDt'), $this->db->conn_id) : '';
		$dtTo = ($this->input->post_get('txtToDt')) ? check_input($this->input->post_get('txtToDt'), $this->db->conn_id) : '';
		$userName = ($this->input->post_get('txtUName')) ? check_input($this->input->post_get('txtUName'), $this->db->conn_id) : '';
		$service = ($this->input->post_get('txtService')) ? check_input($this->input->post_get('txtService'), $this->db->conn_id) : '';

		if(($this->input->post_get('txtService')))
		{
			if($service != '')
			{
				$strWhere .= " AND A.Service LIKE '%".$service."%'";
				$txtlqry .= "&txtService=$service";
			}
			if($userName != '')
			{
				$strWhere .= " AND UserName LIKE '%".$userName."%'";
				$txtlqry .= "&txtUName=$userName";
			}
			if($dtFrom != '' && $dtTo != '')
			{
				$strWhere .= " And DATE(HistoryDtTm) >= '" . $dtFrom . "' AND DATE(HistoryDtTm) <= '" . $dtTo . "'";
				$txtlqry .= "&txtFromDt=$dtFrom&txtToDt=$dtTo";
			}
			else
			{
				if($dtFrom != '')
				{
					$strWhere .= " AND DATE(HistoryDtTm) >= '" . $dtFrom . "'";
					$txtlqry .= "&txtFromDt=$dtFrom";
				}
				if($dtTo!= '')
				{
					$strWhere .= " AND DATE(HistoryDtTm) <= '" . $dtTo . "'";
					$txtlqry .= "&txtToDt=$dtTo";
				}
			}
		}
		if($userId > 0)
		{
			$strWhere .= " AND A.UserId = '$userId'";
			$txtlqry .= "&userId=$userId";
		}
		$rsRpt = $this->reports_model->getData("SELECT UserName, Service, Code, HistoryDtTm FROM tbl_gf_precodes_history A, tbl_gf_users B WHERE A.UserId = B.UserId $strWhere ORDER BY HistoryId DESC LIMIT $eu, $limit");
		$row = $this->reports_model->getData("SELECT COUNT(HistoryId) AS TotalRecs FROM tbl_gf_precodes_history A, tbl_gf_users B WHERE A.UserId = B.UserId $strWhere");
/*
		$this->data['disableCol'] = $disableCol;
		$this->data['strWhereSrv'] = $strWhereSrv;
*/
		$this->data['dtTo'] = $dtTo;
		$this->data['row'] = $row;
		$this->data['service'] = $service;
		$this->data['userId'] = $userId;
		$this->data['userName'] = $userName;
		$this->data['message'] = $message;
		$this->data['txtlqry'] = $txtlqry;
		$this->data['dtFrom'] = $dtFrom;
		$this->data['rsRpt'] = $rsRpt;
		$this->data['view'] = 'admin/precodeshistory';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function creditseatenrpt(){
		$strWhere = '';
		$rsCreditsEaten = '';
		$message = '';
		$strWhereSrv = '';
		$dtFrom = '';
		$row = '';
		$dtTo = '';
		$uName = '';
		$txtlqry = '';
		$page_name = $_SERVER['PHP_SELF'];

		$sc = ($this->input->post_get('sc')) ? check_input($this->input->post_get('sc'), $this->db->conn_id) : 0;
		$categoryId = ($this->input->post_get('categoryId')) ? check_input($this->input->post_get('categoryId'), $this->db->conn_id) : 0;
		$serviceId = ($this->input->post_get('serviceId')) ? check_input($this->input->post_get('serviceId'), $this->db->conn_id) : '0';
		$dtFrom = ($this->input->post_get('txtFromDt')) ? check_input($this->input->post_get('txtFromDt'), $this->db->conn_id) : '';
		$dtTo = ($this->input->post_get('txtToDt')) ? check_input($this->input->post_get('txtToDt'), $this->db->conn_id) : '';
		switch($sc)
		{
			case '0': // IMEI services
				$strWhereSrv .= " AND sl3lbf = 0";
				$strWhereCat = " AND SL3BF = 0";
				$tblName = 'tbl_gf_package_category';
				$tblPckName = 'tbl_gf_packages';
				$colId = 'PackageId';
				$colTitle = 'PackageTitle';
				$disableCol = 'DisablePackage';
				$tblOrders = 'tbl_gf_codes';
				$orderColId = 'CodeId';
				$displayCol = 'IMEINo';
				$heading = $this->lang->line('BE_LBL_394');
				break;
			case '1': // File services
				$strWhereSrv .= " AND sl3lbf = 1";
				$strWhereCat = " AND SL3BF = 1";
				$tblName = 'tbl_gf_package_category';
				$tblPckName = 'tbl_gf_packages';
				$colId = 'PackageId';
				$colTitle = 'PackageTitle';
				$disableCol = 'DisablePackage';
				$tblOrders = 'tbl_gf_codes_slbf';
				$orderColId = 'CodeId';
				$displayCol = 'IMEINo';
				$heading = $this->lang->line('BE_LBL_395');
				break;
			case '2': // File services
				$tblName = 'tbl_gf_log_package_category';
				$tblPckName = 'tbl_gf_log_packages';
				$strWhereCat = "";
				$colId = 'LogPackageId';
				$colTitle = 'LogPackageTitle';
				$disableCol = 'DisableLogPackage';
				$tblOrders = 'tbl_gf_log_requests';
				$orderColId = 'LogRequestId';
				$displayCol = 'SerialNo';
				$heading = $this->lang->line('BE_LBL_396');
				break;
		}

		if(!isset($start))
			$start = 0;

		if(($this->input->post_get("start")))
			$start = $this->input->post_get("start");

		$eu = ($start - 0);
		$limit = 150;
		$thisp = $eu + $limit;
		$back = $eu - $limit;
		$next = $eu + $limit;

		if(($this->input->post_get('categoryId')))
		{
			if($categoryId != '0')
				$strWhere .= " AND CategoryId = '$categoryId'";
			if($serviceId != '0')
				$strWhere .= " AND A.$colId = '$serviceId'";
			if($dtFrom != '' && $dtTo != '')
				$strWhere .= " And DATE(RequestedAt) >= '" . $dtFrom . "' AND DATE(RequestedAt) <= '" . $dtTo . "'";
			else
			{
				if($dtFrom != '')
					$strWhere .= " AND DATE(RequestedAt) >= '" . $dtFrom . "'";
				if($dtTo!= '')
					$strWhere .= " AND DATE(RequestedAt) <= '" . $dtTo . "'";
			}
			$rsCreditsEaten = $this->reports_model->getData("SELECT UserName, CONCAT(FirstName, ' ', LastName) AS Name, $colTitle, $displayCol, A.Credits, RequestedAt FROM $tblOrders A, tbl_gf_users B, $tblPckName C WHERE A.UserId = B.UserId AND A.$colId = C.$colId $strWhere ORDER BY $orderColId DESC LIMIT $eu, $limit");
			$row = $this->reports_model->getData("SELECT COUNT($orderColId) AS TotalRecs FROM $tblOrders A, tbl_gf_users B, $tblPckName C WHERE A.UserId = B.UserId AND A.$colId = C.$colId $strWhere");

		}
		$FillCombo1 = $this->reports_model->getData("SELECT $colId AS Id, $colTitle AS Value FROM $tblPckName WHERE $disableCol = 0 $strWhereSrv AND CategoryId = '$categoryId' AND ArchivedPack = 0 ORDER BY PackOrderBy, $colTitle");
		$FillCombo2 = $this->reports_model->getData("SELECT CategoryId AS Id, Category AS Value FROM $tblName WHERE DisableCategory = 0 AND ArchivedCategory = 0 $strWhereCat ORDER BY OrderBy, Category");

		$this->data['categoryId'] = $categoryId;
		$this->data['serviceId'] = $serviceId;
		$this->data['FillCombo2'] = $FillCombo2;
		$this->data['FillCombo1'] = $FillCombo1;
		$this->data['dtTo'] = $dtTo;
		$this->data['dtFrom'] = $dtFrom;
		$this->data['sc'] = $sc;
		$this->data['$row'] = $row;
		$this->data['message'] = $message;
		$this->data['heading'] = $heading;
		$this->data['rsCreditsEaten'] = $rsCreditsEaten;
		$this->data['view'] = 'admin/creditseatenrpt';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function admincrlog(){

		$message = '';
		$strWhere = '';
		$dtFrom = '';
		$dtTo = '';
		$uName = '';
		$txtlqry = '';
		$sqlwhere = '';
		$page_name = $_SERVER['PHP_SELF'];

		if(!isset($start))
			$start = 0;

		if($this->input->post_get("start"))
			$start = $this->input->post_get("start");

		$eu = ($start - 0);
		$limit = 150;
		$thisp = $eu + $limit;
		$back = $eu - $limit;
		$next = $eu + $limit;
		$dtFrom = ($this->input->post_get('txtFromDt')) ? check_input($this->input->post_get('txtFromDt'), $this->db->conn_id) : '';
		$dtTo = ($this->input->post_get('txtToDt')) ? check_input($this->input->post_get('txtToDt'), $this->db->conn_id) : '';
		$uName = ($this->input->post_get('txtUName')) ? check_input($this->input->post_get('txtUName'), $this->db->conn_id) : '';
		$fName = ($this->input->post_get('txtFName')) ? check_input($this->input->post_get('txtFName'), $this->db->conn_id) : '';

		if(isset($_POST['txtUName']))
		{
			if($uName != '')
				$strWhere .= " AND UserName LIKE '%".$uName."%'";
			if($fName != '')
				$strWhere .= " AND FirstName LIKE '%".$fName."%'";
			if($dtFrom != '' && $dtTo != '')
				$strWhere .= " And DATE(HistoryDtTm) >= '" . $dtFrom . "' AND DATE(HistoryDtTm) <= '" . $dtTo . "'";
			else
			{
				if($dtFrom != '')
					$strWhere .= " AND DATE(HistoryDtTm) >= '" . $dtFrom . "'";
				if($dtTo!= '')
					$strWhere .= " AND DATE(HistoryDtTm) <= '" . $dtTo . "'";
			}
		}
		$rsCats = $this->reports_model->getData("SELECT A.UserId, UserName, A.Credits, CONCAT(FirstName, ' ', LastName) AS Name, Description, HistoryDtTm, CreditsLeft, A.Comments
						FROM tbl_gf_credit_history A, tbl_gf_users B WHERE A.UserId = B.UserId AND ByAdmin = 1 $strWhere ORDER BY HistoryDtTm DESC LIMIT $eu, $limit");

		//$row = $this->reports_model->getSingleRowData("SELECT COUNT(A.UserId) AS TotalRecs FROM tbl_gf_credit_history A, tbl_gf_users B WHERE A.UserId = B.UserId ".$strWhere);


		$this->data['row'] = '';
		$this->data['limit'] = $limit;
		$this->data['byAdmin'] = '';
		$this->data['IS_DEMO'] = false;
		$this->data['userId'] = '';
		$this->data['fName'] = $fName;
		$this->data['uName'] = $uName;
		$this->data['message'] = $message;
		$this->data['rsCats'] = $rsCats;
		$this->data['strWhere'] = $strWhere;
		$this->data['dtFrom'] = $dtFrom;
		$this->data['dtTo'] = $dtTo;
		$this->data['view'] = 'admin/admincrlog';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function clientiplogrpt(){
		$strWhere = '';
		$dtFrom = '';
		$dtTo = '';
		$userName = '';
		$ip = '';
		$txtlqry = '';
		$sqlwhere = '';
		$page_name = $this->input->server('PHP_SELF');

		if(!isset($start))
			$start = 0;

		if(($this->input->post_get("start")))
			$start = $this->input->post_get("start");

		$eu = ($start - 0);
		$limit = 150;
		$thisp = $eu + $limit;
		$back = $eu - $limit;
		$next = $eu + $limit;
		$message = '';
		$userId = ($this->input->post_get('userId')) ? check_input($this->input->post_get('userId'), $this->db->conn_id) : 0;
		$dtFrom = ($this->input->post_get('txtFromDt')) ? check_input($this->input->post_get('txtFromDt'), $this->db->conn_id) : '';
		$dtTo = ($this->input->post_get('txtToDt')) ? check_input($this->input->post_get('txtToDt'), $this->db->conn_id) : '';
		if(($this->input->post_get('txtIP')))
		{
			$userName = check_input($this->input->post_get('txtUName'), $this->db->conn_id);
			$ip = trim($this->input->post_get('txtIP'));
			if($ip != '')
			{
				$strWhere .= " AND A.IP LIKE '%".$ip."%'";
				$txtlqry .= "&txtIP=$ip";
			}
			if($userName != '')
			{
				$strWhere .= " AND UserName LIKE '%".$userName."%'";
				$txtlqry .= "&txtUName=$userName";
			}
			if($dtFrom != '' && $dtTo != '')
			{
				$strWhere .= " And DATE(LoginTime) >= '" . $dtFrom . "' AND DATE(LoginTime) <= '" . $dtTo . "'";
				$txtlqry .= "&txtFromDt=$dtFrom&txtToDt=$dtTo";
			}
			else
			{
				if($dtFrom != '')
				{
					$strWhere .= " AND DATE(LoginTime) >= '" . $dtFrom . "'";
					$txtlqry .= "&txtFromDt=$dtFrom";
				}
				if($dtTo!= '')
				{
					$strWhere .= " AND DATE(LoginTime) <= '" . $dtTo . "'";
					$txtlqry .= "&txtToDt=$dtTo";
				}
			}
		}
		if($userId > 0)
		{
			$strWhere .= " AND A.UserId = '$userId'";
			$txtlqry .= "&userId=$userId";
		}
		$rsIpRpt = $this->reports_model->getData("SELECT Id, A.IP, LoginTime, LogoutTime, UserName, A.UserId FROM tbl_gf_user_login_log A, tbl_gf_users B WHERE A.UserId = B.UserId $strWhere ORDER BY Id DESC LIMIT $eu, $limit");
		$row = $this->reports_model->getData("SELECT COUNT(Id) AS TotalRecs FROM tbl_gf_user_login_log A, tbl_gf_users B WHERE A.UserId = B.UserId " .$strWhere);
		$message=$uName=$fName=$byAdmin=$userId='';
		$this->data['row'] = $row;
		$this->data['ip'] = $ip;
		$this->data['userId'] = $userId;
		$this->data['byAdmin'] = $byAdmin;
		$this->data['message'] = $message;
		$this->data['uName'] = $uName;
		$this->data['fName'] = $fName;
		$this->data['txtlqry'] = $txtlqry;
		$this->data['rsIpRpt'] = $rsIpRpt;
		$this->data['strWhere'] = $strWhere;
		$this->data['dtFrom'] = $dtFrom;
		$this->data['dtTo'] = $dtTo;
		$this->data['limit'] = $limit ;
		$this->data['view'] = 'admin/clientiplogrpt';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function iplogrpt(){

		$strWhere = '';
		$dtFrom = '';
		$dtTo = '';
		$ip = '';
		$txtlqry = '';
		$sqlwhere = '';
		$page_name = $_SERVER['PHP_SELF'];

		if(!isset($start))
			$start = 0;

		if(isset($_REQUEST["start"]))
			$start = $_REQUEST["start"];

		$eu = ($start - 0);
		$limit = 50;
		$thisp = $eu + $limit;
		$back = $eu - $limit;
		$next = $eu + $limit;
		$message = '';
		$dtFrom = isset($_REQUEST['txtFromDt']) ? check_input($_REQUEST['txtFromDt'], $this->db->conn_id) : '';
		$dtTo = isset($_REQUEST['txtToDt']) ? check_input($_REQUEST['txtToDt'], $this->db->conn_id) : '';
		if(isset($_REQUEST['txtIP']))
		{
			$dtFrom = check_input($_REQUEST['txtFromDt'], $this->db->conn_id);
			$dtTo = check_input($_REQUEST['txtToDt'], $this->db->conn_id);
			$ip = trim($_REQUEST['txtIP']);
			if($ip != '')
			{
				$strWhere .= " AND IP LIKE '%".$ip."%'";
				$txtlqry .= "&txtIP=$ip";
			}
			if($dtFrom != '' && $dtTo != '')
			{
				$strWhere .= " And DATE(LoginTime) >= '" . $dtFrom . "' AND DATE(LoginTime) <= '" . $dtTo . "'";
				$txtlqry .= "&txtFromDt=$dtFrom&txtToDt=$dtTo";
			}
			else
			{
				if($dtFrom != '')
				{
					$strWhere .= " AND DATE(LoginTime) >= '" . $dtFrom . "'";
					$txtlqry .= "&txtFromDt=$dtFrom";
				}
				if($dtTo!= '')
				{
					$strWhere .= " AND DATE(LoginTime) <= '" . $dtTo . "'";
					$txtlqry .= "&txtToDt=$dtTo";
				}
			}
		}
		$rsCats = $this->reports_model->getData("SELECT Id, IP, LoginTime, LogoutTime FROM tbl_gf_admin_login_log WHERE (1) ".$strWhere." ORDER BY Id DESC LIMIT $eu, $limit");
		//$row = $this->reports_model->getSingleRowData("SELECT COUNT(Id) AS TotalRecs FROM tbl_gf_admin_login_log WHERE (1) ".$strWhere);
		$row = $limit = '';
		$this->data['limit'] = $limit;
		$this->data['row'] = $row;
		$this->data['ip'] = $ip;
		$this->data['message'] = $message;
		$this->data['txtlqry'] = $txtlqry;
		$this->data['rsCats'] = $rsCats;
		$this->data['count'] = $rsCats;
		$this->data['strWhere'] = $strWhere;
		$this->data['dtFrom'] = $dtFrom;
		$this->data['dtTo'] = $dtTo;
		$this->data['view'] = 'admin/iplogrpt';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function blockedipsrpt(){
		$strWhere = '';
		$dtFrom = '';
		$dtTo = '';
		$ip = '';
		$txtlqry = '';
		$sqlwhere = '';
		$page_name = $_SERVER['PHP_SELF'];
		$comments = '';
		$ip = '';

		if(!isset($start))
			$start = 0;

		if(isset($_REQUEST["start"]))
			$start = $_REQUEST["start"];

		$eu = ($start - 0);
		$limit = 50;
		$thisp = $eu + $limit;
		$back = $eu - $limit;
		$next = $eu + $limit;
		$message = '';
		$del = ($this->input->post_get('del')) ? 1 : 0;
		if($del == '1')
		{
			$id = ($this->input->post_get('id')) ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
			$uId = $this->input->post_get('uId') ? check_input($this->input->post_get('uId'), $this->db->conn_id) : 0;
			$this->reports_model->updateData("UPDATE tbl_gf_users SET DisableUser = 0, LoginAttempts = 0, Comments = '' WHERE UserId = '$uId'");
			$this->reports_model->delteData("DELETE FROM tbl_gf_blocked_ips WHERE Id = '$id'");

			$message = $this->lang->line('BE_GNRL_12');
		}
		$dtFrom = ($this->input->post_get('txtFromDt')) ? check_input($this->input->post_get('txtFromDt'), $this->db->conn_id) : '';
		$dtTo = ($this->input->post_get('txtToDt')) ? check_input($this->input->post_get('txtToDt'), $this->db->conn_id) : '';
		if(($this->input->post_get('txtIP')))
		{
			$dtFrom = check_input($this->input->post_get('txtFromDt'), $this->db->conn_id);
			$dtTo = check_input($this->input->post_get('txtToDt'), $this->db->conn_id);
			$ip = trim($this->input->post_get('txtIP'));
			$uName = trim($this->input->post_get('txtUName'));
			if($ip != '')
			{
				$strWhere .= " AND A.IP LIKE '%$ip%'";
				$txtlqry .= "&txtIP=$ip";
			}
			if($uName != '')
			{
				$strWhere .= " AND UserName LIKE '%$uName%'";
				$txtlqry .= "&txtUName=$uName";
			}
			if($dtFrom != '' && $dtTo != '')
			{
				$strWhere .= " And DATE(DtTm) >= '$dtFrom' AND DATE(DtTm) <= '$dtTo'";
				$txtlqry .= "&txtFromDt=$dtFrom&txtToDt=$dtTo";
			}
			else
			{
				if($dtFrom != '')
				{
					$strWhere .= " AND DATE(DtTm) >= '$dtFrom'";
					$txtlqry .= "&txtFromDt=$dtFrom";
				}
				if($dtTo!= '')
				{
					$strWhere .= " AND DATE(DtTm) <= '$dtTo'";
					$txtlqry .= "&txtToDt=$dtTo";
				}
			}
		}
		if(isset($_POST['txtIPs']) && $_POST['txtIPs'] != '')
		{
			if($_POST['txtIPs'] != '')
			{
				$ip = check_input($_REQUEST['txtIPs'], $this->db->conn_id);
				$comments = check_input($_REQUEST['txtComments'], $this->db->conn_id);
				$dtTm = setDtTmWRTYourCountry();
				$arrIPs = explode(',', $ip);
				$strData = '';
				foreach ($arrIPs as $value)
				{
					if($value != '')
					{
						if($strData != '')
							$strData .= ',';
						$strData .= "('".$value."','".$dtTm."','".$comments."')";
					}
				}
				$this->reports_model->insertData("INSERT INTO tbl_gf_blocked_ips (IP, DtTm, Comments) VALUES $strData");
				$comments = '';
				$ip = '';
				$message = $this->lang->line('BE_GNRL_11');
			}
			else if($_POST['txtIPs'] == '')
				$message = $this->lang->line('BE_LBL_392');
		}
		$rsCats = $this->reports_model->getData("SELECT Id, A.IP, DtTm, A.Comments, UserEmail, UserName, CONCAT(FirstName, ' ', LastName) AS Name, A.UserId FROM tbl_gf_blocked_ips A
				LEFT JOIN tbl_gf_users B ON (A.UserId = B.UserId) WHERE (1) $strWhere ORDER BY Id DESC LIMIT $eu, $limit");
		$row = $this->reports_model->getSingleRowData("SELECT COUNT(Id) AS TotalRecs FROM tbl_gf_blocked_ips A LEFT JOIN tbl_gf_users B ON (A.UserId = B.UserId)
										WHERE (1) ".$strWhere);

		$id='';

		$this->data['limit'] = '';
		$this->data['userId'] = '';
		$this->data['byAdmin'] = '';
		$this->data['uName'] = '';
		$this->data['row'] = $row;
		$this->data['id'] = $id;
		$this->data['ip'] = $ip;
		$this->data['message'] = $message;
		$this->data['txtlqry'] = $txtlqry;
		$this->data['rsCats'] = $rsCats;
		$this->data['count'] = $rsCats;
		$this->data['strWhere'] = $strWhere;
		$this->data['dtFrom'] = $dtFrom;
		$this->data['dtTo'] = $dtTo;
		$this->data['view'] = 'admin/blockedipsrpt';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function blockip(){
		$id = isset($_REQUEST['id']) ? check_input($_REQUEST['id'], $this->db->conn_id) : 0;
		$comments = '';
		$ip = '';
		$message = '';

		if(($this->input->post_get('txtIPs')) && $this->input->post_get('txtIPs') != '')
		{
			$ip = check_input($this->input->post_get('txtIPs'), $this->db->conn_id);
			$comments = check_input($this->input->post_get('txtComments'), $this->db->conn_id);
			$dtTm = setDtTmWRTYourCountry();
			$arrIPs = explode(',', $ip);
			$strData = '';
			foreach ($arrIPs as $value)
			{
				if($value != '')
				{
					if($strData != '')
						$strData .= ',';
					$strData .= "('".$value."','".$dtTm."','".$comments."')";
				}
			}
			$this->reports_model->insertData("INSERT INTO tbl_gf_blocked_ips (IP, DtTm, Comments) VALUES $strData");
			$comments = '';
			$ip = '';
			$message = $this->lang->line('BE_GNRL_11');
		}
		else if(($this->input->post_get('txtIPs')) && $this->input->post_get('txtIPs') == '')
			$message = $this->lang->line('BE_LBL_392');
		$this->data['id'] = $id;
		$this->data['message'] = $message;
		$this->data['comments'] = $comments;
		$this->data['ip'] = $ip;
		$this->data['view'] = 'admin/blockip';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	
}
?>
