<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tickets extends MY_Controller{
     
	public function __construct() {
        parent::__construct();
    }


    public function tickets(){
        $txtlqry = '';
        $strWhere1 = '';
        $message = '' ;
        $strWhere  = '';
        $this->data['page_name'] = $this->input->server('PHP_SELF');

        if(!isset($start))
            $start = 0;

        if($this->input->post_get('start'))
            $start = $this->input->post_get('start');

        $eu = ($start - 0);
        $limit = $this->input->post('records') ?: 50;
        $thisp = $eu + $limit;
        $back = $eu - $limit;
        $next = $eu + $limit;
        $dtFrom = $this->input->post_get('txtFromDt') ?: '';
        $dtTo = $this->input->post_get('txtToDt') ?: '';
        $cldFrm = $this->input->post_get('cldFrm') ?: 0;
        $dId = $this->input->post_get('dId') ?: 0;
        $cId = $this->input->post_get('cId') ?: 0;
        $pId = $this->input->post_get('pId') ?: 0;
        $uName = $this->input->post_get('txtUName') ?: '';
        $tcktNo = $this->input->post_get('txtTcktNo') ?: '';
        $sId = $this->input->post_get('sId') ?: 0;
        if($this->input->post('chkTckts'))
        {
            $ids = '0';
            $arrIds = array();
            if($this->input->post('chkTckts'))
            {
                $arrIds = $this->input->post('chkTckts') ?: 0;
                $ids = implode(',', $arrIds);
            }
            if($cldFrm == '1')
            {
                $this->Tickets_model->del_tickets($ids);
               
                $message = 'Ticket(s) have been deleted succesfully!';
            }
            else if($cldFrm == '3')
            {
                $update_data = array(
                    'StatusId' => 2
                );

                $where = array(
                    'TicketNo' => $ids
                );
                $this->Tickets_model->update_tickets($update_data  ,  $where);
               
                $update_data = array(
                    'StatusId' => 2,
                );
                $where_in = array(
                    'TicketNo' => $ids
                );

                $this->Tickets_model->update_tickets($update_data  ,  '' ,$where_in);

                $rsUserIds = $this->Tickets_model->fetch_tickets_by_ids($ids);
               
                foreach($rsUserIds as $rw)
                {
                    send_push_notification('Your ticket has been closed!', $rw->UserId);
                }
                
                $message = 'Ticket(s) have been closed succesfully!';
            }
            
        }

        if($dtFrom != '')
            $txtlqry .= "&txtFromDt=$dtFrom";
        if($dtTo != '')
            $txtlqry .= "&txtToDt=$dtTo";
        if($dtFrom != '' && $dtTo != '')
            $strWhere .= " And DATE(PaymentDtTm) >= '" . $dtFrom . "' AND DATE(PaymentDtTm) <= '" . $dtTo . "'";
        else
        {
            if($dtFrom != '')
                $strWhere .= " AND DATE(PaymentDtTm) >= '" . $dtFrom . "'";
            if($dtTo!= '')
                $strWhere .= " AND DATE(PaymentDtTm) <= '" . $dtTo . "'";
        }
        if($uName != '')
        {
            $strWhere .= " AND F.UserName LIKE '%".$uName."%'";
            $txtlqry .= "&txtUName=$uName";
        }
        if($tcktNo != '')
        {
            $strWhere .= " AND TicketNo = '$tcktNo'";
            $txtlqry .= "&txtTcktNo=$tcktNo";
        }
        if($dId > 0)
        {
            $strWhere .= " AND A.DepartmentId = '$dId'";
            $txtlqry .= "&dId=$dId";
        }
        if($pId > '0')
        {
            $strWhere .= " AND A.PriorityId = '$pId'";
            $txtlqry .= "&pId=$pId";
        }
        if($cId > 0)
        {
            $strWhere .= " AND A.CategoryId = '$cId'";
            $txtlqry .= "&cId=$cId";
        }
        if($sId > 0)
        {
            $strWhere .= " AND A.StatusId = '$sId'";
            $txtlqry .= "&sId=$sId";
        }
        $this->data['rsTckts'] = $this->Tickets_model->fetch_tickets_data_and_users($strWhere , $eu , $limit); 
        
        $this->data['count'] = count($this->data['rsTckts']);

        $this->data['message'] = $message ;
        $this->data['uName'] = $uName;
        $this->data['tcktNo'] = $tcktNo;
        $this->data['dept_data'] = $this->Tickets_model->fetch_tckt_dept();
        $this->data['dept_cat'] = $this->Tickets_model->fetch_tckt_cat();
        $this->data['tckt_priority'] =$this->Tickets_model->fetch_tckt_priority();
        $this->data['dtFrom'] = $dtFrom;
        $this->data['dtTo'] = $dtTo;
        $this->data['start'] = $start;

        $this->data['totalRows'] = $this->Tickets_model->fetch_tickets_data_and_users_count($strWhere , $eu , $limit);
        $this->data['back'] = $back;
        $this->data['txtlqry'] =  $txtlqry;
        $this->data['limit'] =  $limit;
        $this->data['eu'] = $eu ;
        $this->data['pLast'] =  '';
        $this->data['thisp'] =  $thisp;
        $this->data['next'] =  $next;
        $this->data['view'] = 'admin/tickets' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }
    
    public function ticket(){
        $id = $this->input->post_get('id') ?: 0;
        $subject =  '' ;
        $message = '' ;
        $update_data = array(
            'ReadByAdmin' => 1
        );
        $where = array(
            'TicketId' => $id
        );
        $this->Tickets_model->update_tickets($update_data  ,  $where);
        $row = $this->Tickets_model->fetch_ticket_data($id);
        if (isset($row->TicketNo) && $row->TicketNo != '')
        {
            $dId = $row->DepartmentId;
            $cId = $row->CategoryId;
            $pId = $row->PriorityId;
            $tcktNo = $row->TicketNo;
            $subject = stripslashes($row->Subject);
            $name = stripslashes($row->Name);
            $email = $row->Email;
            $userId = $row->UserId;
            $stId = $row->StatusId;
            $notes = stripslashes($row->Notes);
        }
        if($this->input->post('btnReply'))
        {
            $message = $this->input->post('btnReply') ?: '';
            $message = convertNextLineToBRTag($message);
            $dtTm = setDtTmWRTYourCountry();
            $ip = $this->input->server('REMOTE_ADDR');
            
            $insert_data = array(
                'TicketNo' => $tcktN ,
                'UserId' => $userId,
                'DepartmentId' => $dId ,
                'CategoryId' => $cId,
                'PriorityId' => $pId ,
                'Name' =>  $name ,
                'Email' => $email,
                'Subject' => $subject,
                'Message' => $message ,
                'DtTm' => $dtTm,
                'IP' => $ip,
                'StatusId' => '3' ,
                'AId' => 1 ,
                'ByAdmin' => 1
            );

            $tcktId = $this->Tickets_model->insert_ticket_data($insert_data);

            $update_data = array(
                'StatusId' => 3
            );

            $where = array(
                'TicketNo' => $tcktNo
            );
            $this->Tickets_model->update_tickets($update_data  ,  $where);
           
            send_push_notification('Your ticket has been answered!', $userId);
            ticketDetailsEmail($tcktId, '1');
            
            $message = $this->lang->line('BE_GNRL_11');
        }
        $this->data['rsTckts'] = $this->Tickets_model->fetch_tickets_dept_users($tcktNo); 
        $this->data['message'] = $message ;
        $this->data['tcktNo'] = $tcktNo;
        $this->data['subject'] = $subject;
        $this->data['id']= $id ;
        $this->data['tcktNo'] = $tcktNo;
        $this->data['dId'] = $dId;
        $this->data['dept_data'] = $this->Tickets_model->fetch_tckt_dept();
        $this->data['dept_cat'] = $this->Tickets_model->fetch_tckt_cat();
        $this->data['tckt_status'] = $this->Tickets_model->fetch_tckt_status();
        $this->data['tckt_priority'] = $this->Tickets_model->fetch_tckt_priority();
        $this->data['pId'] = $pId;
        $this->data['stId'] = $stId;
        $this->data['cId'] = $cId;
        $this->data['notes'] = $notes;
       
        $this->data['tckt_tab_reply'] = $this->load->view('admin/tckt_tab_reply' , $this->data , TRUE);
        $this->data['tckt_tab_settings'] = $this->load->view('admin/tckt_tab_settings' , $this->data , TRUE);
        
        $this->data['view'] = 'admin/ticket' ;
        $this->load->view('admin/layouts/default1' , $this->data);

    }

    public function ajxticket(){
        $purpose = $this->input->post('purpose');

        if ($purpose == 'update')
        {
            $subject = $this->input->post_get('subject');
            $dId = $this->input->post_get('dId');
            $cId = $this->input->post_get('cId');
            $pId = $this->input->post_get('pId');
            $stId = $this->input->post_get('stId');
            $tcktNo = $this->input->post_get('tcktNo');
            $notes = $this->input->post_get('notes');
            $update_data = array(
                'Subject' => $subject,
                'DepartmentId' => $dId,
                'CategoryId' => $cId,
                'PriorityId' => $pId ,
                'StatusId' => $stId ,
                'Notes' => $notes
            );
            $where = array(
                'TicketNo' => $tcktNo
            );
            $this->Tickets_model->update_tickets($update_data  ,  $where);
            $msg = $this->lang->line('BE_GNRL_11');
            echo $msg;
        }
    }

    public function departments(){
        $this->data['rsDepts'] = $this->Tickets_model->fetch_tckt_department(); 
       
        $this->data['count'] = count($this->data['rsDepts']);

        $this->data['view'] = 'admin/departments' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function department(){
        $id = $this->input->post_get('id') ?: 0;
        $dept = $email = $desc = '';
        $disable = 0;
        if($id > 0)
        {
            $row = $this->Tickets_model->fetch_department_data($id);
           
            if (isset($row->DeptName) && $row->DeptName != '')
            {
                $dept = stripslashes($row->DeptName);
                $email = stripslashes($row->DeptEmail);
                $desc = stripslashes($row->DeptDescription);
                $disable = $row->DisableDept;
            }
        }
        $this->data['id'] = $id ;
        $this->data['dept'] =  $dept;
        $this->data['email'] = $email ;
        $this->data['desc'] = $desc;
        $this->data['disable'] = $disable;

        $this->data['view'] = 'admin/department' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function ajxdepartment()
    {
        $purpose = $this->input->post_get('purpose');
        if ($purpose == 'save')
        {
            $id = $this->input->post_get('id');
            $dept = $this->input->post_get('dept');
            $email = $this->input->post_get('email');
            $desc = $this->input->post_get('desc');
            $disableVal = $this->input->post_get('disableVal');
            $msg = '';
           
            $row = $this->Tickets_model->fetch_tckt_dept_by_id($dept ,$disableVal , $id);
            if (isset($row->DeptId) && $row->DeptId != '')
                $msg = "'$dept'".$this->lang->line('BE_GNRL_10')."~0"; 
            else
            {
                if($id == 0)
                {
                    $insert_data = array(
                        'DeptName' => $dept,
                        'DeptEmail' => $email ,
                        'DeptDescription' => $desc ,
                        'DisableDept' => $disableVal
                    );
                    $this->Tickets_model->insert_tckt_dept($insert_data);
                    
                    $msg =  $this->lang->line('BE_GNRL_11')."~1";
                }			
                else
                {
                    $update_data = array(
                        'DeptName' => $dept,
                        'DeptEmail' => $email ,
                        'DeptDescription' => $desc ,
                        'DisableDept' => $disableVal
                    );
                    $where = array(
                        'DeptId' => $id 
                    );
                    $this->Tickets_model->update_tckt_dept($update_data , $where);
                    
                    $msg = $this->lang->line('BE_GNRL_11')."~0";
                }
            }
            echo $msg;
        }
    }
    

}