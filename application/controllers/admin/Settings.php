<?php
/** @noinspection DuplicatedCode */
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->data['message'] = "";
		//$this->load->library('foneshop_lib');
	}

	private $apiTitle = "";
	private $apiKey = "";
	private $url = "";
	private $apiType = "";
	private $accountId = "";
	private $serviceId = "";
	private $apiKey2 = "";
	private $password = "";

	public function apis()
	{
		$del = $this->input->post_get('del') ? 1 : 0;
		$this->data['message'] = '';
		if($del == '1')
		{
			$id = $this->input->post_get('id') ?: 0;
			$this->Settings_model->deleteAPI($id);
			$this->data['message'] = $this->lang->line('BE_GNRL_12');
		}
		$this->data['rsAPIs'] = $this->Settings_model->getApis();
		$this->data['view'] = "admin/apis";
		$this->load->view('admin/layouts/default1', $this->data);
	}

	public function viewlookuplist()
	{


		$this->data['message'] = '';
		$iFrm = $this->input->post_get('iFrm') ?: 0;
		$strLabel = '';
		$message = '';
		$service = $this->input->post_get('service')?: '';
		$del = $this->input->post_get('del') ? 1 : 0;
		$this->data['iFrm'] = $iFrm;
		$this->data['service'] = $service;
		$this->lookupcases($service, $iFrm, 0);
		/*echo "second time<br><br>";
		print_r($this->data);
		exit();*/
		$this->data['view'] = "admin/view_lookup_list";
		$this->load->view('admin/layouts/default1', $this->data);


	}

	public function lookuplist()
	{

		$txtVal = '';
		$disable = 0;
		$id = $this->input->post_get('id') ?: 0;
		$iFrm = $this->input->post_get('iFrm') ?: 0;
		$service = $this->input->post_get('service') ?: '';
		$result = $this->lookupcases($service, $iFrm, $id);
		$this->data['id'] = $id;
		$this->data['view'] = "admin/lookup_list";
		$this->load->view('admin/layouts/default1', $this->data);

	}

	private function lookupcases($service, $iFrm, $id)
	{
		$strLabel = '';
		$idCol = '';
		$textCol = '';
		$disableCol = '';
		$tbl = '';

		if ($service != '') {
			$strLabel = $this->input->post_get('service');
			$idCol = 'Id';
			$textCol = 'Title';
			$disableCol = 'DisableValue';
			$tbl = $this->input->post_get('servtbl');
		} else {
			switch ($iFrm) {
				case '3':
					$strLabel = $this->lang->line('BE_LS_3');
					$idCol = 'CodeStatusId';
					$textCol = 'CodeStatus';
					$disableCol = 'DisableCodeStatus';
					$tbl = 'tbl_gf_code_status';
					break;
				case '5':
					$strLabel = $this->lang->line('BE_LS_5');
					$idCol = 'CategoryId';
					$textCol = 'Category';
					$disableCol = 'DisableCategory';
					$tbl = 'tbl_gf_package_category';
					break;
				case '6':
					$strLabel = $this->lang->line('BE_LS_6');
					$idCol = 'CountryId';
					$textCol = 'Country';
					$disableCol = 'DisableCountry';
					$tbl = 'tbl_gf_countries';
					break;
				case '7':
					$strLabel = $this->lang->line('BE_LS_7');
					$idCol = 'PaymentStatusId';
					$textCol = 'PaymentStatus';
					$disableCol = 'DisablePaymentStatus';
					$tbl = 'tbl_gf_payment_status';
					break;
				case '8':
					$strLabel = 'Task Category';
					$idCol = 'CategoryId';
					$textCol = 'Category';
					$disableCol = 'DisableCategory';
					$tbl = 'tbl_gf_task_category';
					break;
				case '9':
					$strLabel = $this->lang->line('BE_LBL_189');
					$idCol = 'TcktPriorityId';
					$textCol = 'TcktPriority';
					$disableCol = 'DisableTcktPriority';
					$tbl = 'tbl_gf_tckt_priority';
					break;
				case '10':
					$strLabel = $this->lang->line('BE_LBL_18');
					$idCol = 'PricePlanId';
					$textCol = 'PricePlan';
					$disableCol = 'DisablePricePlan';
					$tbl = 'tbl_gf_price_plans';
					break;
				case '11':
					$strLabel = $this->lang->line('BE_LBL_35');
					$idCol = 'ListId';
					$textCol = 'List';
					$disableCol = 'DisableList';
					$tbl = 'tbl_gf_user_list';
					break;
				case '12':
					$strLabel = $this->lang->line('BE_LBL_118');
					$idCol = 'PricePlanOfferId';
					$textCol = 'PricePlanOffer';
					$disableCol = 'DisablePricePlanOffer';
					$tbl = 'tbl_gf_price_plans_offers';
					break;
				case '13':
					$strLabel = $this->lang->line('BE_LBL_841');
					$idCol = 'CategoryId';
					$textCol = 'Category';
					$disableCol = 'DisableCategory';
					$tbl = 'tbl_gf_manufacturer';
					break;
				case '14':
					$strLabel = $this->lang->line('BE_LBL_723');
					$idCol = 'TcktStatusId';
					$textCol = 'TcktStatus';
					$disableCol = 'DisableTcktStatus';
					$tbl = 'tbl_gf_tckt_status';
					break;
				case '15':
					$strLabel = $this->lang->line('BE_LBL_724');
					$idCol = 'TcktCategoryId';
					$textCol = 'TcktCategory';
					$disableCol = 'DisableTcktCategory';
					$tbl = 'tbl_gf_tckt_category';
					break;
				case '16':
					$strLabel = $this->lang->line('BE_LBL_803');
					$idCol = 'CategoryId';
					$textCol = 'Category';
					$disableCol = 'DisableCategory';
					$tbl = 'tbl_gf_knowledgebase_cat';
					break;
				case '17':
					$strLabel = $this->lang->line('BE_CAT_1');
					$idCol = 'CategoryId';
					$textCol = 'Category';
					$disableCol = 'DisableCategory';
					$tbl = 'tbl_gf_newsltrs_cat';
					break;
				case '18':
					$strLabel = $this->lang->line('BE_CAT_1');
					$idCol = 'CategoryId';
					$textCol = 'Category';
					$disableCol = 'DisableCategory';
					$tbl = 'tbl_gf_news_cat';
					break;
				case '19':
					$strLabel = $this->lang->line('BE_LS_6');
					$idCol = 'CountryId';
					$textCol = 'Country';
					$disableCol = 'DisableCountry';
					$tbl = 'tbl_gf_make_country';
					break;
				case '20':
					$strLabel = $this->lang->line('BE_LS_7');
					$idCol = 'PaymentStatusId';
					$textCol = 'PaymentStatus';
					$disableCol = 'DisablePaymentStatus';
					$tbl = 'tbl_gf_ecommerce_pstatus';
					break;
				case '21':
					$strLabel = $this->lang->line('BE_LS_3');
					$idCol = 'OrderStatusId';
					$textCol = 'OrderStatus';
					$disableCol = 'DisableOrderStatus';
					$tbl = 'tbl_gf_order_status';
					break;
			}
		}
		$this->data['strLabel'] = $strLabel;
		//$this->data['service'] = $strLabel ;
		$this->data['idCol'] =$idCol ;
		$this->data['tbl'] = $tbl;
		$this->data['disableCol'] = $disableCol;
		$this->data['textCol'] =$textCol;

		if($id > 0)
		{
			$rsLists = $this->Settings_model->getRecordsagainstId($idCol, $textCol, $disableCol, $tbl, $id);

			if ($rsLists)
			{
				/*
				echo $this->db->last_query();
				echo "<br>$textCol<br>$disableCol";
				exit();
				*/
				foreach ($rsLists as $row)
				$txtVal = $row->$textCol;
				$disable = $row->$disableCol;
				$this->data['txtVal'] = $txtVal;
				$this->data['disable'] = $disable;
			}
		}
		else
		{
			$this->data['rsLists'] = $this->Settings_model->getRecords($idCol, $textCol, $disableCol, $tbl);
		}
		/*print_r($this->data);*/

	}

	public function manageapi()
	{
		$id = $this->input->post_get('id') ?: 0;

		if($this->data['rsStngs']->CanAddNewAPI == '0' && $id == '0')
		{
			redirect(base_url('admin/settings/apis'));
		}
		$extId = 1;
		$apiType = $systemAPI = $disableAPI = $arrServiceType = $IMEI_API = $FILE_API = $SERVER_API = 0;
		$apiTitle = $serviceId = $apiKey = $url = $accountId = $action = $message = $responseURL = $apiKey2 = $password = $threshold = $emails =  '';
		$this->data['message'] = "";
		/*echo "outside IF";
		exit();*/
		if ($this->input->post_get('txtTitle') && $this->session->get_userdata('GSM_FSN_AdmId') )
		{

			$apiTitle = $this->input->post_get('txtTitle');
			$row = $this->Settings_model->getApi($apiTitle, $id);
			if (isset($row->APIId) && $row->APIId != '')
			{
				$message = "'$apiTitle' ".$this->lang->line('BE_GNRL_10');
			}
			else
			{
				$col1 = '';
				$colVal1 = '';
				$colVal2 = '';
				$apiKey = $this->input->post_get('txtAPIKey');
				if(trim($apiKey) != '')
				{
					$apiKey = encryptAPIKey($apiKey, $this->data['rsStngs']->EncKeyPwd, $this->data['rsStngs']->EncKeyLen);
					$col1 = ', APIKey';
					$colVal1 = ", '$apiKey'";
					$colVal2 = ", APIKey = '$apiKey'";
				}
				$apiType = $this->input->post('apiType') ?: $this->input->post_get('hdAPIType');
				$url = $this->input->post_get('txtURL');
				$responseURL = $this->input->post_get('txtResponseURL');
				$accountId = $this->input->post_get('txtAccountId');
				$serviceId = $this->input->post_get('txtServiceId');
				$action = $this->input->post_get('txtAction');
				$apiKey2 = $this->input->post_get('txtAPIKey2');
				$password = $this->input->post_get('txtAPIPass');
				$threshold = $this->input->post('txtThresholdLimit') != '' ?: 0;
				$emails = $this->input->post_get('txtNEmails');
				$disableAPI = $this->input->post('chkDisable') ? 1 : 0;
				//$extId = $this->input->post_get('chkExtId') ? 1 : 0;
				$dtTm = setDtTmWRTYourCountry();
				if($id == 0)
				{
					$row = $this->Settings_model->getNextApiID();
					$apiId = $row->CurrAPIId + 1;

					$this->Setting_model->addApi("INSERT INTO tbl_gf_api (APIId, APITitle, ServerURL, APIAction, AccountId, ResponseURL, ServiceId, DisableAPI, SendExternalId, APIType, 
			APIKey2, APIPassword, ThresholdAmount, ThresholdEmails $col1) VALUES ('$apiId', '$apiTitle', '$url', '$action', '$accountId', '$responseURL', '$serviceId', 
			'$disableAPI', '$extId', '$apiType', '$apiKey2', '$password', '$threshold', '$emails' $colVal1)");

					$id = $apiId;
					$this->data['message'] = $this->lang->line('BE_GNRL_11');
				}
				else
				{
					$this->Settings_model->updateApi("UPDATE tbl_gf_api SET APITitle = '$apiTitle', ServerURL = '$url', AccountId = '$accountId', ResponseURL = '$responseURL', 
			APIAction = '$action', ServiceId = '$serviceId', APIType = '$apiType', DisableAPI = '$disableAPI', APIKey2 = '$apiKey2', SendExternalId = '$extId', 
			APIPassword = '$password', ThresholdAmount = '$threshold', ThresholdEmails = '$emails' $colVal2 WHERE APIId = '$id'");
					$apiId = $id;
					$this->data['message'] = $this->lang->line('BE_GNRL_11');
				}
				/*
				$strIds = 0;
				$selectedIds = sizeof($this->input->post_get('packageIds']);
				for($k = 0; $k < $selectedIds; $k++)
				{
					$strIds .= ", ".$this->input->post_get('packageIds'][$k);
				}
				$this->db->query("UPDATE tbl_gf_packages SET APIId = 0 WHERE APIId = ".$apiId);
				$this->db->query("UPDATE tbl_gf_packages SET APIId = $apiId WHERE PackageId IN (".$strIds.")"); 
				*/
			}
		}
		if($id > 0)
		{
			$row = $this->Settings_model->getApiForID($id);
			$this->data['id'] = $id;
			if (isset($row->APIId) && $row->APIId != '')
			{
				$this->data['apiTitle'] = stripslashes($row->APITitle);
				$this->data['apiKey'] = $row->APIKey;
				$this->data['apiKey2'] = $row->APIKey2;
				$this->data['apiType'] = $row->APIType;
				$this->data['url'] = $row->ServerURL;
				$this->data['responseURL'] = $row->ResponseURL;
				$this->data['accountId'] = $row->AccountId;
				$this->data['serviceId'] = $row->ServiceId;
				$this->data['action'] = $row->APIAction;
				$this->data['disableAPI'] = $row->DisableAPI;
				$this->data['systemAPI'] = $row->SystemAPI;
				$this->data['password'] = $row->APIPassword;
				$this->data['threshold'] = $row->ThresholdAmount;
				$this->data['emails'] = $row->ThresholdEmails;
				$this->data['arrServiceType'] = explode(',', $row->APIServiceType);
				$this->data['IMEI_API'] = $this->data['arrServiceType'][0] != '' ? $this->data['arrServiceType'][0] : 0;
				$this->data['FILE_API'] = $this->data['arrServiceType'][1] != '' ? $this->data['arrServiceType'][1] : 0;
				$this->data['SERVER_API'] = $this->data['arrServiceType'][2] != '' ? $this->data['arrServiceType'][2] : 0;

			}
		}
		$this->data['api_tab_general'] = $this->load->view ('admin/api_tab_general', $this->data, TRUE);
		$this->data['api_tab_sync'] = $this->load->view ('admin/api_tab_sync', $this->data, TRUE);
		$this->data['api_file_sync'] = $this->load->view ('admin/api_file_sync', $this->data, TRUE);
		$this->data['api_server_sync'] = $this->load->view ('admin/api_server_sync', $this->data, TRUE);
		$this->data['api_tab_crons'] = $this->load->view ('admin/api_tab_crons', $this->data, TRUE);

		$this->data['view'] = "admin/manage_api";
		$this->load->view('admin/layouts/default1', $this->data);

	}

	public function services_sync()
	{

		$purpose = $this->input->post_get('purpose');
		//@set_time_limit(0);


		if ($purpose == 'vc') {
			$id = $this->input->post_get('id') ?: 0;
			$apiType = $this->input->post_get('apiType') ?: 0;
			$msg = '';
//			$row = $this->db->queryUniqueObject("SELECT * FROM tbl_gf_api WHERE APIId = '$id'");
			$row = $this->Settings_model->getApiForID($id);


			$apiTitle = "";
			$apiKey = "";
			$url = "";
			$apiType = "";
			$accountId = "";
			$serviceId = "";
			$apiKey2 = "";
			$password = "";

			if ($row->APITitle != '') {
				$this->apiTitle = $apiTitle = $this->data['apiTitle'] = stripslashes($row->APITitle);
				$this->apiKey = $apiKey = $this->data['apiKey'] = decryptAPIKey($row->APIKey, $this->data['rsStngs']->EncKeyPwd, $this->data['rsStngs']->EncKeyLen);
				$this->url = $url = $this->data['url'] = $row->ServerURL;
				$this->apiType = $apiType = $this->data['apiType'] = $row->APIType;
				$this->accountId = $accountId = $this->data['accountId'] = $row->AccountId;
				$this->serviceId = $serviceId = $this->data['serviceId'] = $row->ServiceId;
				$this->apiKey2 = $apiKey2 = $this->data['apiKey2'] = $row->APIKey2;
				$this->password = $password = $this->data['password'] = $row->APIPassword;
			}
			if ($apiType == '0') {
				if ($id == '201') // The Fone Shop
				{
					//require('../../../apis/thefoneshop/thefoneshop.api.class.php');
					//$api = new TheFoneShop();
					$this->foneshop_lib->action('GET_SERVICE_LIST', $this->data['apiKey'], $this->data['accountId'], $this->data['url']);
					$this->foneshop_lib->XmlToArray(base64_decode($this->foneshop_lib->getResult()));
					$arrayData = $this->foneshop_lib->createArray();
					if (strtolower(trim($arrayData['message']['status'])) == "err") {
						$msg = 'Connection Error: ' . $arrayData['message']['statusmessage'];
					} else if (strtolower(trim($arrayData['message']['status'])) == "ok") {
						$msg = 'Connected Successfully!';
					} else
						$msg = 'Error while connecting to the server!';
				} else if ($id == '202') // Blue Unlock
				{
					//require('../../../apis/blueunlock/api.php');
					//$api = new GFM_API();
					$this->CI_GFM_API->sendCommand('GET_CREDITS', $this->data['apiKey'], $this->data['url'], array());
					$resultArray = $this->CI_GFM_API->parse2Array($this->CI_GFM_API->getResult());
					$strError = $this->CI_GFM_API->checkError($resultArray['RESULT']);
					if ($strError) {
						$msg = 'Connection Error: ' . $strError;
					} else {
						$result = $resultArray['RESULT'];
						$msg = 'Connected Successfully! You have ' . $result['CREDITS'] . ' credits';
					}
				} else if ($id == '203') // DLGSM.com
				{
					//require('../../../apis/dlgsm.com/api.php');
					//$api = new DLGSM_API();
					$this->CI_GFM_API->doAction('imeiservicelist', $this->data['accountId'], $this->data['apiKey'], $this->data['url'], array());
					$arrResponse = $this->CI_GFM_API->getResult();
					if (is_array($arrResponse)) {
						if (sizeof($arrResponse['SUCCESS']['LIST']) > 0)
							$msg = 'Connected Successfully!';
						else if (isset($arrResponse['ERROR']))
							$msg = 'Connection Error: ' . htmlspecialchars($arrResponse['ERROR']['DESCRIPTION']);
					} else {
						$msg = 'Connection Error: Could not parse the XML stream!';
					}
				} else if ($id == '204') //Unlock Base
				{
					//require('../../../apis/unlockbase/API.php');
					$XML = $this->CI_UnlockBase->CallAPI($this->data['apiKey'], $this->data['url'], 'AccountInfo');
					if (is_string($XML)) {
						$Data = $this->CI_UnlockBase->ParseXML($XML);
						if (is_array($Data)) {
							if (isset($Data['Error'])) {
								$msg = 'Connection Error: ' . htmlspecialchars($Data['Error']);
							} else {
								$msg = 'Connected Successfully!';
							}
						} else {
							$msg = 'Connection Error: Could not parse the XML stream!';
						}
					} else {
						$msg = 'Connection Error: Could not communicate with the api!';
					}
				} else if ($id == '206') //bruteforce.mobi
				{
					//require('../../../apis/bruteforce.mobi/bruteforce.mobi_api.php');
					$result = $this->CI_bruteforce_mobi_api->GetQueueSlowFast($url, $accountId, $password, $apiKey, $apiKey2);
					if ($result->status == 'ok') {
						$msg = 'Connected Successfully!';
					} else {
						$msg = $result->status;
					}
				} else if ($id == '207') //mybruteforce.com
				{
					//require('../../../apis/mybruteforce/mybruteforce_api.php');
					//$mbfAPI = new Api();
					$arrResult = $this->CI_mybruteforce_api->doAction('info', $url, $accountId, $apiKey, array());
					if (isset($arrResult['credits'])) {
						$msg = 'Connected Successfully! You have ' . $arrResult['credits'] . ' credits';
					} else {
						$msg = 'Connection Error! ' . $arrResult['errors'];
					}
				} else if ($id == '208') //simunlocks.com
				{
					//require('../../../apis/simunlocks_api/class.mgr_simunlocks.php');
					//$objAPI = new mgr_simunlocks($url, $apiKey);
					$params = array('url' => $this->url, 'apiKey' => $this->apiKey);

					$this->load->library('CI_mgr_simunlocks', $params);

					$response = $this->CI_mgr_simunlocks->credits_balance('');
					if (is_numeric($response))
						$msg = 'Connected Successfully! You have ' . $response . ' credits';
					else
						$msg = 'Connection Error!';
				} else if ($id == '209') //sunnysofts.biz
				{
					//require('../../../apis/sunnysofts.biz/api.php');
					//$api = new API();
					$this->CI_sunnysofts->doAction($apiKey, $url, '012754009031137');
					$response = $this->CI_sunnysofts->getResult();
					if ($response == 'Wrong API Key' || $response == 'Bad Request') {
						$msg = 'Connection Error: ' . $response;
					} else {
						$msg = 'Connected Successfully!';
					}
				} else if ($id == '210') //gsmunlockusa.com
				{
					//require('../../../apis/gsmunlockusa.com/API.php');
					//require('../../../apis/gsmunlockusa.com/ArrayToXML.php');

					$Data = $this->CI_gsmunlockusa_api->GetAccountInfo($apiKey, $url);
					if (!empty($Data) && $Data != null) {    /* Everything works fine */
						// Output: Array
						if (isset($Data['GetAccountInfoResult']['API']['Result']['Credits']))
							$msg = 'Connected Successfully. Your credits are ' . $Data['GetAccountInfoResult']['API']['Result']['Credits'];
					} else $msg = 'Connection Error!';
				} else if ($id == '211') //unlock.uk
				{
					//require('../../../apis/unlock.hk/API.php');
					$XML = $this->CI_UnlockHK->CallAPI($apiKey, $accountId, $url, 'AccountInfo');
					if (is_string($XML)) {
						$Data = $this->CI_UnlockHK->ParseXML($XML);
						if (is_array($Data)) {
							if (isset($Data['Error'])) {
								$msg = 'Connection Error: ' . htmlspecialchars($Data['Error']);
							} else {
								$msg = 'Connected Successfully!<br />Account Email: ' . htmlspecialchars($Data['Email']) . '<br />Credits available: ' . htmlspecialchars($Data['Credits']);
							}
						} else {
							$msg = 'Could not parse the XML stream';
						}
					} else {
						$msg = 'Could not communicate with the api';
					}
				} else if ($id == '212') //unlocktele.com
				{
					//require('../../../apis/unlocktele.com/API.php');
					$XML = $this->CI_unlocktele_unlockhk->CallAPI($apiKey, $accountId, $url, 'AccountInfo');

					if (is_string($XML)) {
						$Data = $this->CI_unlocktele_unlockhk->ParseXML($XML);
						if (is_array($Data)) {
							if (isset($Data['Error'])) {
								$msg = 'Connection Error: ' . htmlspecialchars($Data['Error']);
							} else {
								$msg = 'Connected Successfully!<br />Account Email: ' . htmlspecialchars($Data['Email']) . '<br />Credits available: ' . htmlspecialchars($Data['Credits']);
							}
						} else {
							$msg = 'Could not parse the XML stream';
						}
					} else {
						$msg = 'Could not communicate with the api';
					}
				} else if ($id == '218' || $id == '223') //sl3team.com
				{
					if ($id == '218') //sl3team.com
					{
						$params = array('apiKey' => $apiKey);
						$this->load->library('CI_sl3team', $params);
						$response = $this->CI_sl3team->accountInfo();
						if (isset($response['msg_code']) && $response['msg_code'] == 0)
							$msg = 'Connected Successfully!<br />Available Credits: ' . $response['credit'] . '<br />Credits Consumed: ' . $response['credit_consume'];
						else
							$msg = 'Connection Error: ' . $this->CI_sl3team->getMessage($response['msg_code']);
					}
						//require('../../../apis/sl3team.com/sl3.inc.php');
					else if ($id == '223') //sl3team.com
					{
						//require('../../../apis/sl3bf.net/sl3.inc.php');
						//$api = new SL3_API($apiKey);

						$params = array('apiKey' => $apiKey);
						$this->load->library('CI_sl3bf', $params);
						$response = $this->CI_sl3bf->accountInfo();
						if (isset($response['msg_code']) && $response['msg_code'] == 0)
							$msg = 'Connected Successfully!<br />Available Credits: ' . $response['credit'] . '<br />Credits Consumed: ' . $response['credit_consume'];
						else
							$msg = 'Connection Error: ' . $this->CI_sl3bf->getMessage($response['msg_code']);
					}
				} else if ($id == '219') {
					//require('../../../apis/appvisi0n.biz/api.php');
					//$api = new API();
					$this->CI_appvisi0n->doAction($url, '012754009031137');
					$response = $this->CI_appvisi0n->getResult();
					if ($response == 'Wrong API Key' || $response == 'Bad Request') {
						$msg = 'Connection Error: ' . $response;
					} else {
						$msg = 'Connected Successfully!';
					}
				} else if ($id == '222') {
					//require('../../../apis/chimeratoolapi/chimeratoolapi.php');
					//$api = new ChimeraApi();
					$data = $this->CI_chimeratoolapi->getInfo($apiKey, $accountId, $url);
					if (is_array($data)) {
						if (isset($data['success']) && $data['success'] == '1') {
							$msg = 'Connected Successfully! Your credits are ' . $data['creditBalance'];
						} else if (isset($data['message']) && $data['success'] == '') {
							$msg = 'Connection Error: ' . $data['message'];
						} else {
							$msg = 'Error while connecting to Chimera tool API!';
						}
					} else {
						$msg = 'Error while connecting to Chimera tool API!';
					}
				} else if ($id == '224') {
					//require('../../../apis/unlockcodesource.com/ucs.php');
					//$ucs = new UCS();
					$this->CI_unlockcodesource->setApikey($apiKey);
					$data = $this->CI_unlockcodesource->getAccount();
					if (is_array($data)) {
						if (isset($data['name']) && $data['name'] != '') {
							$msg = 'Connected Successfully!<br /> Your credits are <b>' . $data['credits'] . '</b>';
						} else if (isset($data['message']) && $data['message'] != '') {
							$msg = 'Connection Error:<br />' . $data['message'];
						} else {
							$msg = 'Error while connecting to Unlock Code Source API!';
						}
					} else {
						$msg = 'Error while connecting to Unlock Code Source API!';
					}
				} else if ($id == '225') {
					//require('../../../apis/gsmkody.pl/api.php');
					//$objAPI = new gsmkody();
					$this->CI_gsmkody->doAction('account.json', $apiKey, $url, 'GET');
					$arrData = $this->CI_gsmkody->getResult();
					$strErr = $this->CI_gsmkody->checkError($arrData);
					if (trim($strErr) != '') {
						$msg = $strErr;
					} else {
						if (isset($arrData['status']) && $arrData['status'] == 'ok') {
							if (isset($arrData['account']) && isset($arrData['account']['id']))
								$msg = 'Connected Successfully!<br />Your credits are ' . $arrData['account']['credits'];
						}
					}
				} else if ($id == '227') {
					//require('../../../apis/lockpop/lockpop_api.php');
					//$objAPI = new LPAPI();
					$this->CI_lockpop_api->doAction('Options', $apiKey, $url, array());
					$this->CI_lockpop_api->getResult();
					$this->CI_lockpop_api->XmlToArray($this->CI_lockpop_api->getResult());
					$arrayData = $this->CI_lockpop_api->createArray();
					if (is_array($arrayData['OptionsResponse'])) {
						if (isset($arrayData['OptionsResponse']['Error']))
							$msg = $arrayData['OptionsResponse']['Error'];
						else if (isset($arrayData['OptionsResponse']['Options']) && sizeof($arrayData['OptionsResponse']['Options']) > 0) {
							$msg = 'Connected Successfully!';
						}
					} else {
						$msg = 'Error while connecting with LockPop API!';
					}
				} else if ($id == '229' || $id == '260' || $id == '513') {
					//require('../../../apis/z3x-team.com/z3xteamapi.class.php');
					//$api = new Z3XTeam();
					$data = $this->CI_z3x_team->action('credits_left', $apiKey, $url);
					if (is_object($data)) {
						if ($data->ErrorCode == 0) {
							$msg = 'Connected Successfully! Your credits are ' . $data->Data->sams_upd;
						} else if ($data->ErrorCode > 0) {
							$msg = 'Connection Error: ' . $data->ErrorTxt;
						}
					} else {
						$msg = 'Error while connecting to API!';
					}
				} else if ($id == '236') {
					//require('../../../apis/ucocustomapi/ucocustomapi.class.php');
					//$api = new ucocustomapi();
					$data = $this->CI_ucocustomapi->action('Check', $apiKey, $url);
					if (isset($data['error'])) {
						$msg = 'Connection Error: ' . $data['error'];
					} else if (isset($data['result'][0]['created'])) {
						$msg = "Connected Successfully!<br />Total Requests: " . $data['result'][0]['total_requests'] . "<br />Remaining Credits: " . $data['result'][0]['remaining_credits'];
					} else {
						$msg = 'Error while connecting to API!';
					}
				} else if ($id == '241') {
					//require('../../../apis/unlockingsmart.co.uk/usmartapi.php');
					//$objAPI = new USMARTAPI();
					$this->CI_unlockingsmart->doAction('accountinfo', $apiKey, $url, $accountId, array());
					$this->CI_unlockingsmart->getResult();
					$this->CI_unlockingsmart->XmlToArray($this->CI_unlockingsmart->getResult());
					$arrayData = $this->CI_unlockingsmart->createArray();
					if (isset($arrayData['error']) && sizeof($arrayData['error']) > 0) {
						$msg = '<b>' . $arrayData['error'][0] . '</b>';
					} else if (isset($arrayData['Client']['ClientCredits'])) {
						$msg = 'Connected Successfully! Your credits are ' . $arrayData['Client']['ClientCredits'];
					} else {
						$msg = 'Error while connecting to unlockingsmart.co.uk';
					}
				} else if ($id == '243') {
					//require('../../../apis/zzunlockcustomapi/zzunlockcustomapi.class.php');
					//$api = new zzunlockcustomapi();
					$data = $this->CI_zzunlockcustomapi->action($apiKey, $url);
					if (isset($data['code']) && $data['code'] != '0') {
						$msg = 'Connection Error: ' . $data['message'];
					} else if (isset($data['code']) && $data['code'] == '0') {
						$msg = "Connected Successfully!";
					} else {
						$msg = 'Unknown Error while connecting to API!';
					}
				} else if ($id == '244') {
					//require('../../../apis/zzucustomapi_sl/zzucustomapi_sl.class.php');
					//$api = new zzucustomapi_sl();
					$data = $this->CI_zzucustomapi_sl->action($apiKey, $url);
					if (isset($data['code']) && $data['code'] != '0') {
						$msg = 'Connection Error: ' . $data['message'];
					} else if (isset($data['code']) && $data['code'] == '0') {
						$msg = "Connected Successfully!";
					} else {
						$msg = 'Unknown Error while connecting to API!';
					}
				} else if ($id == '255') {
					//require('../../../apis/furiousgold.com/furiousgold_api.php');
					list($result, $info, $error, $errorno) = $this->CI_furiousgold->call_api('CHECK_MY_CREDITS', $accountId, $apiKey, $url);

					if (isset($result['ERROR'])) {
						$errors['response'] = $result['ERROR'] . ' => error number ' . $result['ERROR_CODE'];
						$msg = 'Connection Error: ' . $errors['response'];
					} else if (isset($result['REQUESTED_METHOD']) && $result['REQUESTED_METHOD'] == 'CHECK_MY_CREDITS') // the response is valid
					{
						$response = $result['RESULT']['AMOUNT'] . ' ' . $result['RESULT']['CURRENCY'];
						$msg = "Connected Successfully! Your credits are $response";
					} else
						$msg = 'Unknown API Connection Error';
				} else if ($id == '256') {
					//require('../../../apis/dc-unlocker.com/DcUnlockerAPI.class.php');
					//$API = new DCUnlockerAPI($accountId, $apiKey);
					$params = array('accountId' => $this->accountId, 'apiKey' => $this->apiKey);

					$this->load->library('CI_dc_unlocker', $params);

					$action = $this->CI_dc_unlocker->information();
					$action->creditsLeft();
					$message = $action->submit();
					$results = $message->get_response();
					if (isset($results['errors'])) {
						if ($results['errors'] == 0)
							$msg = 'Connected Successfully! ' . $results['response']['message'];
						else
							$msg = 'Connection Error: ' . $results['response']['message'];
					} else
						$msg = 'Connection Error! - Unknown Error';
				} else if ($id == '500') {
					//require('../../../apis/samkey.org/api_icell_ae.php');
					//$api = new API();
					$arrResponse = $this->CI_samkey->action('accountInfo', $apiKey, $accountId, $url);
					if (isset($arrResponse['ERROR'])) {
						if (isset($arrResponse['ERROR'][0]['MESSAGE'])) {
							$msg = 'Connection Error: ' . check_input($arrResponse["ERROR"][0]["MESSAGE"]);
						}
					}
					if (isset($arrResponse['SUCCESS']) && is_array($arrResponse['SUCCESS'][0])) {
						if (isset($arrResponse['SUCCESS'][0]['CREDITS'])) {
							$msg = 'Connected Successfully! Your Credits are ' . $arrResponse['SUCCESS'][0]['CREDITS'];
						}
					}
				} else if ($id == '501' || $id == '508') {
					//require('../../../apis/miracleserver.com/api_miracle.php');
					//$api = new API();
					$arrResponse = $this->CI_miracleserver->action('Login', $apiKey, $accountId, $url);
					if (isset($arrResponse['result']['statusid'])) {
						if ($arrResponse['result']['statusid'] == '0') {
							$msg = 'Connection Error: ' . $arrResponse['result']['info'];
						} else if ($arrResponse['result']['statusid'] == '1') {
							$msg = 'Connected Successfully! Your Credits are ' . $arrResponse['result']['info']['AvailableCredit'];
						}
					} else
						$msg = 'Unknown Error!';
				} else if ($id == '509') {
					//require('../../../apis/samkey.us/gsmhub.class.php');
					//$api = new SAMKEYFusion();
					$arrResponse = $this->CI_samkey_us->action('accountinfo', $apiKey, $accountId, $url);
					if (is_array($arrResponse)) {
						if (isset($arrResponse['SUCCESS'][0]['AccoutInfo']['credit']))
							$msg = 'Connected Successfully! Your credits are ' . $arrResponse['SUCCESS'][0]['AccoutInfo']['credit'] . ' ' . $arrResponse['SUCCESS'][0]['AccoutInfo']['currency'];
						else if (isset($arrResponse['ERROR'][0]))
							$msg = 'Connection Error: ' . htmlspecialchars($arrResponse['ERROR'][0]['MESSAGE']);
					} else {
						$msg = 'Connection Error: Something wrong in the URL!';
					}
				} else if ($id == '263') {
					//require('../../../apis/unlocksworld.com/unlocksworldapi.class.php');
					//$api = new unlocksWorld();
					$arr = array("OrderId" => "", "Skip" => "0", "Take" => "20");
					$request = $this->CI_unlocksworld->action('services', $apiKey, $url, $arr);
					$arrResponse = json_decode($request, true);
					if (isset($arrResponse['Status']) && $arrResponse['Status'] == 'Success')
						$msg = 'Connected Successfully!';
					else if (isset($arrResponse['Status']) && $arrResponse['Status'] == 'Error')
						$msg = 'Connection Error: ' . $arrResponse['Message'];
				}
			} else if ($apiType == '1') {
				//require('../../../../apis/fusionclientapi/dhrufusionapi.class.php');
				//$api = new DhruFusion();
				$arrResponse = $this->CI_fusionclientapi->action('imeiservicelist', $apiKey, $accountId, $url);
				if (is_array($arrResponse)) {
					if (isset($arrResponse['SUCCESS'][0]))
						$msg = 'Connected Successfully!';
					else if (isset($arrResponse['ERROR'][0]))
						$msg = 'Connection Error: ' . htmlspecialchars($arrResponse['ERROR'][0]['MESSAGE']);
				} else {
					$msg = 'Connection Error: Could not parse the XML stream! Try changing the URL.';
				}
			} else if ($apiType == '2' || $apiType == '7') {
				if ($apiType == '2') {
					//require('../../../../apis/fusionclientapi/dhrufusionapi.class.php');

					//$api = new DhruFusion();
					$arrResponse = $this->CI_fusionclientapi->action('accountinfo', $apiKey, $accountId, $url);
					if (is_array($arrResponse)) {
						if (isset($arrResponse['SUCCESS'][0]))
							$msg = 'Connected Successfully!';
						else if (isset($arrResponse['ERROR'][0]))
							$msg = 'Connection Error: ' . htmlspecialchars($arrResponse['ERROR'][0]['MESSAGE']);
					} else {
						$msg = 'Connection Error: Could not parse the XML stream!';
					}
				}
				else if ($apiType == '7') {
					//require('../../../../apis/fusionclientapi/gsmhubapi.class.php');
					//$api = new DhruFusion();
					$arrResponse = $this->CI_fusionclientapi2->action('accountinfo', $apiKey, $accountId, $url);
					if (is_array($arrResponse)) {
						if (isset($arrResponse['SUCCESS'][0]))
							$msg = 'Connected Successfully!';
						else if (isset($arrResponse['ERROR'][0]))
							$msg = 'Connection Error: ' . htmlspecialchars($arrResponse['ERROR'][0]['MESSAGE']);
					} else {
						$msg = 'Connection Error: Could not parse the XML stream!';
					}
				}
			} else if ($apiType == '3') {
				//require('../../../apis/codeskpro/codeskapi.class.php');
				//$api = new CodeskApi();
				$arrResponse = $this->CI_codeskpro->action('ACCOUNTINFO', $apiKey, $accountId, $url);
				if (is_array($arrResponse)) {
					if (isset($arrResponse['RET']) && $arrResponse['RET'] == '0')
						$msg = 'Connected Successfully!';
					else
						$msg = 'Connection Error: Can not connect to the supplier!';
				} else {
					$msg = 'Connection Error: Can not parse the Data stream!';
				}
			} else if ($apiType == '4' || $apiType == '6') {
				//require('../../../../apis/gsmfusionapi/api/gsmfusion_api.php');
				//$objGSMFUSIONAPI = new GSMFUSIONAPI();
				$this->CI_gsmfusionapi->doAction('imeiservices', $apiKey, $url, $accountId, array());
				if ($this->CI_gsmfusionapi->apiError() != '') {
					$msg = $this->CI_gsmfusionapi->apiError();
				} else {
					//$this->CI_gsmfusionapi->getResult();
					$this->CI_gsmfusionapi->XmlToArray($this->CI_gsmfusionapi->getResult());
					$arrayData = $this->CI_gsmfusionapi->createArray();
					if (isset($arrayData['error']) && sizeof($arrayData['error']) > 0) {
						$msg = '<b>' . $arrayData['error'][0] . '</b>';
					} else {
						$msg = 'Connected Successfully!';
					}
				}
			} else if ($apiType == '5') {
				//require('../../../apis/nakshsoft/naksh_api.php');
				//$objAPI = new NAKSHAPI();
				$this->CI_nakshsoft->doAction('imeiservices', $apiKey, $url, $accountId, array());
				//$objAPI->getResult();
				$this->CI_nakshsoft->XmlToArray($this->CI_nakshsoft->getResult());
				$arrayData = $this->CI_nakshsoft->createArray();
				if (isset($arrayData['error']) && sizeof($arrayData['error']) > 0) {
					$msg = '<b>' . $arrayData['error'][0] . '</b>';
				} else {
					$msg = 'Connected Successfully!';
				}
			}
		} else if ($purpose == 'syncIMEI') {
			$id = $this->input->post_get('id') ? $this->input->post_get('id') : 0;
			$syncIMEIS = $this->input->post_get('syncIMEIS') ?: 0;
			$attachSrv = $this->input->post_get('attachSrv') ?: 0;
			$type = $this->input->post_get('priceType') ?: 0;
			$pricing = $this->input->post_get('pricing') ?: 0;
			$otherCurr = $this->input->post_get('otherCurrencies') ?: 0;
			$price = $this->input->post_get('price') ? $this->input->post_get('price') : 0;
			$syncModels = $this->input->post_get('syncModels') ? $this->input->post_get('syncModels') : 0;
			//$this->db->queryUniqueObject("SELECT APITitle, APIKey, ServerURL, AccountId, ServiceId, APIType FROM tbl_gf_api WHERE APIId = '$id'");
			if (isset($row->APITitle) && $row->APITitle != '') {
				$apiTitle = stripslashes($row->APITitle);
				$apiKey = decryptAPIKey($row->APIKey, $this->data['rsStngs']->EncKeyPwd, $this->data['rsStngs']->EncKeyLen);//decryptAPIKey($row->APIKey, $API_KEY_ENCRYPT_PWD, $API_KEY_ENCRYPT_LEN);
				$url = $row->ServerURL;
				$accountId = $row->AccountId;
				$serviceId = $row->ServiceId;
				$apiType = $row->APIType;
				//$this->db->query("UPDATE tbl_gf_api SET APIInUse = '1' WHERE APIId = '$id'");
				$this->Settings_model->SetApiInUse($id);

			}
			if ($apiType == '0') {
				switch ($id) {
					case '201':
						//include 'sync_thefoneshop.php';
						$this->sync_thefoneshop($id);
						break;
					case '202':
						$this->sync_blueunlock($id);
						break;
					case '203':
						$this->sync_dlgsm($id);
						break;
					case '204':
						$this->sync_unlockbase($syncIMEIS, $syncModels, $id);
						break;
					case '207':
						$this->sync_mybrutforce($id);
						break;
					case '208':
						$this->sync_simunlock($id);
						break;
					case '210':
						$this->sync_gsmunlockusa($syncIMEIS, $id);
						break;
					case '211':
						$this->sync_unlock($id, $syncIMEIS);
						break;
					case '212':
						$this->sync_unlocktele($id, $syncIMEIS);
						break;
					case '218':
						$this->sync_sl3team($id);
						break;
					case '219':
						$this->sync_appvisi0n($id);
						break;
					case '223':
						$this->sync_sl3team($id);
						break;
					case '224':
						$this->sync_ucs($id, $syncIMEIS);
						break;
					case '225':
						$this->sync_gsmkody($id);
						break;
					case '227':
						$this->sync_lockpop($id);
						break;
					case '236':
						$this->sync_ucocustomapi($id);
						break;
					case '241':
						$this->sync_unlockingsmart($id);
						break;
					case '243':
						$this->sync_zzcustomapi($id);
						break;
					case '244':
						$this->sync_zzcustomapi_sl($id);
						break;
					case '263':
						$this->sync_unlocksworld($id);
						break;
				}
			}
			//Commented by Shahid - to be looked at later on

			if ($apiType == '1' || $apiType == '2' || $apiType == '7') {
				/*require('../../../../apis/fusionclientapi/dhrufusionapi.class.php');*/
				$this->sync_dhru_imeiservices($syncModels,$id,$syncIMEIS,$apiKey, $accountId, $url);
			} else if ($apiType == '3') {
				/*require 'sync_codesk_services.php';*/
				$this->sync_codesk_services($syncModels,$id,$syncIMEIS,$apiKey, $accountId, $url);
			} else if ($apiType == '4' || $apiType == '6') {
				/*require('../../../../apis/gsmfusionapi/api/gsmfusion_api.php');

				include 'sync_gsmf_imeiservices.php';*/
				$this->sync_gsmf_imeiservices($syncIMEIS,$id,$apiKey, $accountId, $url);
				//include 'sync_gsmf_srvrservices.php';
			} else if ($apiType == '5') {
				/*require('../../../apis/nakshsoft/naksh_api.php');*/

				/*include 'sync_nksh_imeiservices.php';*/
				$this->sync_nksh_imeiservices($syncIMEIS,$id,$apiKey, $accountId, $url);
			}
			if ($attachSrv == '1' && $syncIMEIS == '1') {
				$this->Settings_model->executeQuery("UPDATE tbl_gf_packages A, tbl_gf_supplier_services B SET ExternalNetworkId=B.ServiceId, A.APIId='$id' WHERE A.PackageTitle = B.ServiceName AND B.APIId='$id'");
				if ($type != '2') {
					$srvc = 0;
					/*include '../../../srvpricing.php';*/
					$this->srvpricing($srvc,$price,$type,$pricing);
				}
			}

		}
		//Commented by Shahid - to be looked at later on

		 else if ($purpose == 'syncFS') {
			$id = $this->input->post_get('id') ? $this->input->post_get('id') : 0;
			$row = $this->db->queryUniqueObject("SELECT APITitle, APIKey, ServerURL, AccountId, ServiceId, APIType FROM tbl_gf_api WHERE APIId = '$id'");
			if (isset($row->APITitle) && $row->APITitle != '') {
				$apiTitle = stripslashes($row->APITitle);
				$apiKey = decryptAPIKey($row->APIKey, $this->data['rsStngs']->EncKeyPwd, $this->data['rsStngs']->EncKeyLen);//decryptAPIKey($row->APIKey, $API_KEY_ENCRYPT_PWD, $API_KEY_ENCRYPT_LEN);
				$url = $row->ServerURL;
				$accountId = $row->AccountId;
				$serviceId = $row->ServiceId;
				$apiType = $row->APIType;
				$this->db->query("UPDATE tbl_gf_api SET APIInUse = '1' WHERE APIId = '$id'");
			}
			if ($apiType == '0') {
				switch ($id) {
					case '206':
						/*include 'sync_bruteforce.mobi.php';*/
						$this->sync_bruteforce_mobi($id);
						break;
					case '255':
						/*include 'sync_furiousgold_com.php';*/
						$this->sync_furiousgold_com($id);
						break;
					case '256':
						/*include 'sync_dc-unlocker_com.php';*/
						$this->sync_dc_unlocker_com($id);
						break;
				}
			}
			if ($apiType == '1' || $apiType == '2' || $apiType == '7') {
				/*require('../../../../apis/fusionclientapi/dhrufusionapi.class.php');*/
				/*include 'sync_dhru_fileservices.php';*/
				$this->sync_dhru_fileservices($id,$apiType,$apiKey, $accountId, $url);
			} else if ($apiType == '4' || $apiType == '6') {
				/*require('../../../../apis/gsmfusionapi/api/gsmfusion_api.php');*/
				/*include 'sync_gsmf_fileservices.php';*/
				$this->sync_gsmf_fileservices($id,$apiType,$apiKey, $accountId, $url);
			} else if ($apiType == '5') {
				/*require('../../../apis/nakshsoft/naksh_api.php');*/
				/*include 'sync_nksh_fileservices.php';*/
				$this->sync_nksh_fileservices($id,$apiType,$apiKey, $accountId, $url);
			}
		}

		else if ($purpose == 'syncSS') {
			$id = $this->input->post_get('id') ? $this->input->post_get('id') : 0;
			$syncSS = $this->input->post_get('syncSRVCS') ? $this->input->post_get('syncSRVCS') : 0;
			$attachSrv = $this->input->post_get('attachSrv') ? $this->input->post_get('attachSrv') : 0;
			$type = $this->input->post_get('priceType') ? $this->input->post_get('priceType') : 0;
			$pricing = $this->input->post_get('pricing') ? $this->input->post_get('pricing') : 0;
			$otherCurr = $this->input->post_get('otherCurrencies') ? $this->input->post_get('otherCurrencies') : 0;
			$price = $this->input->post_get('price') ? $this->input->post_get('price') : 0;
			$row = $this->db->queryUniqueObject("SELECT APITitle, APIKey, ServerURL, AccountId, ServiceId, APIType FROM tbl_gf_api WHERE APIId = '$id'");
			if (isset($row->APITitle) && $row->APITitle != '') {
				$apiTitle = stripslashes($row->APITitle);
				$apiKey = decryptAPIKey($row->APIKey, $this->data['rsStngs']->EncKeyPwd, $this->data['rsStngs']->EncKeyLen);//decryptAPIKey($row->APIKey, $API_KEY_ENCRYPT_PWD, $API_KEY_ENCRYPT_LEN);
				$url = $row->ServerURL;
				$accountId = $row->AccountId;
				$serviceId = $row->ServiceId;
				$apiType = $row->APIType;
				$this->db->query("UPDATE tbl_gf_api SET APIInUse = '1' WHERE APIId = '$id'");
			}
			if ($apiType == '0') {
				switch ($id) {
					case '222':
						/*include 'sync_chimeratool.php';*/
						$this->sync_chimeratool($id,$apiKey, $accountId, $url);
						break;
					case '229':
						/*include 'sync_z3xteam.php';*/
						$this->sync_z3xteam($id);
						break;
					case '260':
						/*include 'sync_z3xteam.php';*/
						$this->sync_z3xteam($id);
						break;
					case '513':
						/*include 'sync_z3xteam.php';*/
						$this->sync_z3xteam($id);
						break;
					case '255':
						/*include 'sync_furiousgold_com.php';*/
						$this->sync_furiousgold_com($id);
						break;
					case '256':
						/*include 'sync_dc-unlocker_com.php';*/
						$this->sync_dc_unlocker_com($id);
						break;
					case '500':
						/*include 'sync_samkey_org.php';*/
						$this->sync_samkey_org($id);
						break;
					case '501':
						/*include 'sync_miracleserver.php';*/
						$this->sync_miracleserver($id);
						break;
					case '502':
						/*include 'sync_octopus.php';*/
						$this->sync_octopus($id);
						break;
					case '508':
						/*include 'sync_miracleserver.php';*/
						$this->sync_miracleserver($id);
						break;
					case '509':
						/*include 'sync_samkey_us.php';*///file was not found so skipping it
						$this->sync_samkey_us();
						break;
					case '511':
						/*include 'sync_easy_firmware.php';*/
						$this->sync_easy_firmware();
						break;
				}
			}
			if ($apiType == '1' || $apiType == '2' || $apiType == '7') {
				if ($apiType == '1' || $apiType == '2')
//					/*require('../../../../apis/fusionclientapi/dhrufusionapi.class.php');*/
					$this->sync_dhru_fileservices($id,$apiType,$apiKey, $accountId, $url);
				else if ($apiType == '7')
					require('../../../../apis/fusionclientapi/gsmhubapi.class.php');
				/*include 'sync_dhru_serverservices.php';*/
				$this->sync_dhru_serverservices($id,$apiType,$apiKey, $accountId, $url);
			} else if ($apiType == '4' || $apiType == '6') {
				/*require('../../../../apis/gsmfusionapi/api/gsmfusion_api.php');*/
				/*include 'sync_gsmf_srvrservices.php';*/
				$this->sync_gsmf_srvrservices($id,$apiType,$apiKey, $accountId, $url);
				if ($attachSrv == '1' && $syncSS == '1') {
					$this->db->query("UPDATE tbl_gf_log_packages A, tbl_gf_supplier_services B SET ExternalNetworkId=B.ServiceId, A.APIId='$id' WHERE A.LogPackageTitle = B.ServiceName AND B.APIId='$id'");
					if ($type != '2') {
						$srvc = 2;
						/*include '../../../srvpricing.php';*/
						$this->srvpricing($srvc,$price,$type,$pricing,$otherCurr);
					}
				}
			}
		} else if ($purpose == 'fetchCredits') {
			$apiId = $this->input->post('apiId') ?: '0';
			$apiUN = $this->input->post('apiUN') ?: '';
			$apiKey = $this->input->post('apiKey') ?: '';
			$apiType = $this->input->post('apiType') ?: '0';
			$apiKey2 = $this->input->post('apiKey2') ?: '';
			$apiPwd = $this->input->post('apiPwd') ?: '';
			$apiURL = $this->input->post('apiURL') ?: '';

			$apiCredits = 'N/A';
			/*require_once 'ajxfetchapicredits.php';*/
			if ($_SERVER['HTTP_HOST'] != 'localhost') {
				$apiCredits = fetchCredits($apiId, $apiUN, $apiKey, $apiType, $apiURL, $apiKey2, $apiPwd);
				if (trim($apiCredits) == '')
					$apiCredits = 'N/A';
			}
			$msg = $apiId . '|' . $apiCredits;
		}
		//$objDBCD14->close();

		echo $msg;

	}

	private function sync_thefoneshop($id)
	{

		//require('../../../apis/thefoneshop/thefoneshop.api.class.php');
		//$api = new TheFoneShop();
		$this->foneshop_lib->action('GET_SERVICE_LIST', $this->apiKey, $this->accountId, $this->url);
		$this->foneshop_lib->XmlToArray(base64_decode($this->foneshop_lib->getResult()));
		$arrayData = $this->foneshop_lib->createArray();
		if (strtolower(trim($arrayData['message']['status'])) == "err") {
			$msg = 'Sync Error: ' . $arrayData['message']['statusmessage'];
		} else if (strtolower(trim($arrayData['message']['status'])) == "ok") {
			$RESPONSE_ARR = array();
			if (isset($arrayData['message']['reaply_txt'][0]['Group']) && sizeof($arrayData['message']['reaply_txt'][0]['Group']) > 0)
				$RESPONSE_ARR = array_unique($arrayData['message']['reaply_txt'][0]['Group']);
			$str = '';
			$strServices = '';
			if ($syncIMEIS == '1') {    //$this->db->query("UPDATE tbl_gf_package_category SET ArchivedCategory = '1' WHERE SL3BF = '0'");
				$this->Settings_model->archiveServicesPackage1();
			}
			foreach ($RESPONSE_ARR as $key => $value) {
				if (is_array($value['Service'])) {
					if ($syncIMEIS == '1') {
						//$this->db->query("INSERT INTO tbl_gf_package_category (Category, DisableCategory, SL3BF) VALUES ('" . check_input($value['Name'], $this->db->conn_id) . "', 0, 0)");
						$this->Settings_model->addServicesPackage($value['Name']);
					}
					foreach ($value['Service'] as $k => $v) {
						if ($str != '')
							$str .= ',';
						$str .= "('$id', '" . check_input($v['ID'], $this->db->conn_id) . "', '" . check_input($v['Name'], $this->db->conn_id) . "', '" . check_input($v['Credits'], $this->db->conn_id) . "', '" . check_input($v['Deliverytime'], $this->db->conn_id) . "', 0)";
						if ($syncIMEIS == '1') {
							if ($strServices != '')
								$strServices .= ',';
							$strServices .= "('" . check_input($value['ID'], $this->db->conn_id) . "', '" . check_input($v['Name'], $this->db->conn_id) . "', '" . check_input($v['Credits'], $this->db->conn_id) . "', '" . check_input($v['Deliverytime'], $this->db->conn_id) . "', '" . check_input($v['info'], $this->db->conn_id) . "', 0, 0)";
						}
					}
				}
			}
			if ($str != '') {
				//$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
				$this->Settings_model->deleteSupplierServicesZero($id);
				//$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES " . $str);
				$this->Settings_model->addSupplierServices($str);
				$msg = 'Services have been synchronized successfully!';
			}
			if ($syncIMEIS == '1' && $strServices != '') {
				//$this->db->query("DELETE FROM tbl_gf_plans_packages_prices WHERE ServiceType = '0'");
				$this->Settings_model->deletePlanPackagesPrices();
				//$this->db->query("DELETE FROM tbl_gf_users_packages_prices");
				$this->Settings_model->deleteUsersPackagesPrices();
				//$this->db->query("DELETE FROM tbl_gf_user_packages WHERE ServiceType = '0'");
				$this->Settings_model->deleteUsersPackages();

				//$this->db->query("UPDATE tbl_gf_packages SET ArchivedPack = '1' WHERE sl3lbf = '0'");
				$this->Settings_model->archivePackages();
				//$this->db->query("INSERT INTO tbl_gf_packages (CategoryId, PackageTitle, PackagePrice, TimeTaken, MustRead, DisablePackage, sl3lbf) VALUES " . $strServices);
				$this->Settings_model->addPackages($strServices);
				$msg = 'Services have been synchronized successfully!';
			}
		}

	}
	private function sync_blueunlock($id)
	{


		$this->CI_GFM_API->sendCommand('GET_TOOLS', $this->apiKey, $this->url, array('id' => '0'));
		$arrTools = $this->CI_GFM_API->parse2Array($this->CI_GFM_API->getResult());
		$strError = $this->CI_GFM_API->checkError($arrTools['RESULT']);
		if ($strError) {
			$msg = 'Connection Error: ' . $strError;
		} else {
			$totalTools = count($arrTools['RESULT']);
			$str = '';
			for ($count = 1; $count <= $totalTools; $count++) {
				$operator = $arrTools['RESULT']['TOOL' . $count];
				if ($str != '')
					$str .= ',';
				$str .= "('$id', '" . check_input($operator['ID'], $this->db->conn_id) . "', '" . check_input($operator['TOOL_NAME'], $this->db->conn_id) . "', 
		'" . check_input($operator['CREDITS'], $this->db->conn_id) . "', '" . check_input($operator['DELIVERY_TIME'], $this->db->conn_id) . "', 0)";
			}
			if ($str != '') {
				//$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
				$this->Settings_model->deleteSupplierServicesZero($id);
				$this->Settings_model->addSupplierServices($str);
				//$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES " . $str);
				$msg = 'Services have been synchronized successfully!';
			} else
				$msg = 'Error while synchronizing the services!';
		}

	}
	private function sync_dlgsm($id)
	{

		//require('../../../apis/dlgsm.com/api.php');
		//$api = new DLGSM_API();
		$this->CI_GFM_API->doAction('imeiservicelist', $this->accountId, $this->apiKey, $this->url, array());
		$arrResponse = $this->CI_GFM_API->getResult();
		if (is_array($arrResponse)) {
			if (sizeof($arrResponse['SUCCESS']['LIST']) > 0) {
				$RESPONSE_ARR = $arrResponse['SUCCESS']['LIST'];
				$str = '';
				foreach ($RESPONSE_ARR as $key => $value) {
					if ($str != '')
						$str .= ',';
					$str .= "('$id', '" . check_input($value['SERVICEID'], $this->db->conn_id) . "', '" . check_input($value['SERVICENAME'], $this->db->conn_id) . "', 
			'" . check_input($value['CREDIT'], $this->db->conn_id) . "', '" . check_input($value['TIME'], $this->db->conn_id) . "', 0)";
					if ($str != '') {
						//$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
						$this->Settings_model->deleteSupplierServicesZero($id);
						//$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES " . $str);
						$this->Settings_model->addSupplierServices($str);
						$msg = 'Services have been synchronized successfully!';
					} else
						$msg = 'Error while synchronizing the services!';
				}
			} else if (isset($arrResponse['ERROR']))
				$msg = 'Connection Error: ' . htmlspecialchars($arrResponse['ERROR']['DESCRIPTION']);
		} else {
			$msg = 'Error while synchronizing!';
		}

	}

	private function sync_unlockbase($syncIMEIS, $syncModels, $id)
	{
		return; //not needed
		//require('../../../apis/unlockbase/API.php');
		$XML =$this->CI_UnlockBase->CallAPI($this->apiKey, $this->url, 'GetTools');
		if (is_string($XML)) {
			$Data = $this->CI_UnlockBase->ParseXML($XML);
			if (is_array($Data)) {
				if (isset($Data['Error'])) {
					$msg = 'API error : ' . htmlspecialchars($Data['Error']);
				} else {
					$str = '';
					$strServices = '';
					if ($syncIMEIS == '1')
						$this->db->query("UPDATE tbl_gf_package_category SET ArchivedCategory = '1' WHERE SL3BF = '0'");
					foreach ($Data['Group'] as $Group) {
						if ($syncIMEIS == '1') {
							$this->db->query("INSERT INTO tbl_gf_package_category (Category, DisableCategory, SL3BF) VALUES ('" . check_input($Group['Name'], $this->db->conn_id) . "', 0, 0)");
							$UBCatId = $objDBCD14->lastInsertedId();
						}
						foreach ($Group['Tool'] as $Tool) {
							if ($str != '')
								$str .= ',';
							$serviceName = htmlspecialchars($Group['Name']) . ' - ' . htmlspecialchars($Tool['Name']);
							$delTime = htmlspecialchars($Tool['Delivery.Min']) . ' - ' . htmlspecialchars($Tool['Delivery.Max']) . ' - ' . htmlspecialchars($Tool['Delivery.Unit']);
							$str .= "('$id', '" . check_input(htmlspecialchars($Tool['ID']), $this->db->conn_id) . "', '" . check_input($serviceName, $this->db->conn_id) . "', 
					'" . check_input(htmlspecialchars($Tool['Credits']), $this->db->conn_id) . "', '" . check_input($delTime, $this->db->conn_id) . "', 0)";
							if ($syncIMEIS == '1') {
								if ($strServices != '')
									$strServices .= ',';
								$strServices .= "('" . check_input($UBCatId, $this->db->conn_id) . "', '" . check_input(htmlspecialchars($Tool['Name']), $this->db->conn_id) . "', 
						'" . check_input(htmlspecialchars($Tool['Credits']), $this->db->conn_id) . "', '" . check_input($delTime, $this->db->conn_id) . "', 
						'" . check_input($Tool['Message'], $this->db->conn_id) . "', 0, 0)";
							}
						}
					}
					if ($str != '') {
						$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
						$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES " . $str);
						$msg = 'Services have been synchronized successfully!';
					}
					if ($syncIMEIS == '1' && $strServices != '') {
						$this->db->query("DELETE FROM tbl_gf_plans_packages_prices WHERE ServiceType = '0'");
						$this->db->query("DELETE FROM tbl_gf_users_packages_prices");
						$this->db->query("DELETE FROM tbl_gf_user_packages WHERE ServiceType = '0'");

						$this->db->query("UPDATE tbl_gf_packages SET ArchivedPack = '1' WHERE sl3lbf = '0'");
						$this->db->query("INSERT INTO tbl_gf_packages (CategoryId, PackageTitle, PackagePrice, TimeTaken, MustRead, DisablePackage, sl3lbf) VALUES " . $strServices);
						$msg = 'Services have been synchronized successfully!';
					}
				}
			} else {
				$msg = 'Could not parse the XML stream';
			}
		} else {
			$msg = 'Connection Error: Could not communicate with the api!';
		}


//=============================================== SYNCING BRANDS AND MODELS ================================================//
		if ($syncModels == '1') {
			$XML = $this->CI_UnlockBase->CallAPI($this->apiKey, $this->url, 'GetMobiles');

			if (is_string($XML)) {
				/* Parse the XML stream */
				$Data = $this->CI_UnlockBase->ParseXML($XML);

				if (is_array($Data)) {
					if (isset($Data['Error'])) {
						$msg = 'API error : ' . htmlspecialchars($Data['Error']);
					} else {
						$strBrands = '';
						$strModels = '';
						foreach ($Data['Brand'] as $Brand) {
							$apiBrand = check_input(htmlspecialchars($Brand['Name']), $this->db->conn_id);
							$apiBrandId = check_input(htmlspecialchars($Brand['ID']), $this->db->conn_id);
							if ($apiBrand != '') {
								if ($strBrands != '')
									$strBrands .= ',';
								$strBrands .= "('$id', '$apiBrandId', '$apiBrand')";
							}
							foreach ($Brand['Mobile'] as $Mobile) {
								if ($strModels != '')
									$strModels .= ',';
								$strModels .= "('$id', '$apiBrandId', '" . check_input(htmlspecialchars($Mobile['ID']), $this->db->conn_id) . "', 
						'" . check_input(htmlspecialchars($Mobile['Name']), $this->db->conn_id) . "')";
							}
						}
						if ($syncModels == '1' && $strBrands != '') {
							//$this->db->query("DELETE FROM tbl_gf_api_brands WHERE APIId = '$id'");
							$this->Settings_model->deleteApiBrands($id);
							//$this->db->query("INSERT INTO tbl_gf_api_brands (APIId, BrandId, Brand) VALUES " . $strBrands);
							$this->Settings_model->addApiBrands($strBrands);
							$msg = 'Services and Models have been synchronized successfully!';
						}
						if ($syncModels == '1' && $strModels != '') {
							//$this->db->query("DELETE FROM tbl_gf_api_models WHERE APIId = '$id'");
							$this->Settings_model->deleteApiModels($id);
							//$this->db->query("INSERT INTO tbl_gf_api_models (APIId, BrandId, ModelId, Model) VALUES " . $strModels);
							$this->Settings_model->addApiModels($strModels);
							$msg = 'Services and Models have been synchronized successfully!';
						}
					}
				} else {
					$msg = 'Could not parse the XML stream';
				}
			} else {
				$msg = 'Could not communicate with the api';
			}
		}
//=============================================== SYNCING BRANDS AND MODELS ================================================//

	}
	private function sync_mybrutforce($id)
	{

		//require('../../../apis/mybruteforce/mybruteforce_api.php');
		//$mbfAPI = new Api();
		$arrResult = $this->CI_mybruteforce_api->doAction('info', $this->url, $this->accountId, $this->apiKey);
		echo '<pre>';
		print_r($arrResult);
		echo '</pre>';
		exit;

		$arrResult = $this->CI_mybruteforce_api->doAction('service', $url, $accountId, $apiKey, array('no' => '1'));
		echo '<pre>';
		print_r($arrResult);
		echo '</pre>';

		$arrResult = $this->CI_mybruteforce_api->doAction('service', $url, $accountId, $apiKey, array('no' => '2'));
		echo '<pre>';
		print_r($arrResult);
		echo '</pre>';

		$arrResult = $this->CI_mybruteforce_api->doAction('service', $url, $accountId, $apiKey, array('no' => '3'));
		echo '<pre>';
		print_r($arrResult);
		echo '</pre>';

		$arrResult = $this->CI_mybruteforce_api->doAction('service', $url, $accountId, $apiKey, array('no' => '4'));
		echo '<pre>';
		print_r($arrResult);
		echo '</pre>';

		$arrResult = $this->CI_mybruteforce_api->doAction('service', $url, $accountId, $apiKey, array('no' => '5'));
		echo '<pre>';
		print_r($arrResult);
		echo '</pre>';

		$arrResult = $this->CI_mybruteforce_api->doAction('service', $url, $accountId, $apiKey, array('no' => '6'));
		echo '<pre>';
		print_r($arrResult);
		echo '</pre>';

		$arrResult = $this->CI_mybruteforce_api->doAction('service', $url, $accountId, $apiKey, array('no' => '7'));
		echo '<pre>';
		print_r($arrResult);
		echo '</pre>';

		$arrResult = $this->CI_mybruteforce_api->doAction('service', $url, $accountId, $apiKey, array('no' => '8'));
		echo '<pre>';
		print_r($arrResult);
		echo '</pre>';

		$arrResult = $this->CI_mybruteforce_api->doAction('service', $url, $accountId, $apiKey, array('no' => '9'));
		echo '<pre>';
		print_r($arrResult);
		echo '</pre>';

		$arrResult = $this->CI_mybruteforce_api->doAction('service', $url, $accountId, $apiKey, array('no' => '10'));
		echo '<pre>';
		print_r($arrResult);
		echo '</pre>';

		$arrResult = $this->CI_mybruteforce_api->doAction('service', $url, $accountId, $apiKey, array('no' => '11'));
		echo '<pre>';
		print_r($arrResult);
		echo '</pre>';

		//exit; //Confirm with Hammad
		if (isset($arrResult['error'])) {
			$msg = 'Connection Error: ' . $arrResult['error'];
		} else if (isset($arrResult['errors'])) {
			$msg = 'Connection Error: ' . $arrResult['errors'][0];
		} else {
			$totalTools = count($arrTools['RESULT']);
			$str = '';
			for ($count = 1; $count <= $totalTools; $count++) {
				$operator = $arrTools['RESULT']['TOOL' . $count];
				if ($str != '')
					$str .= ',';
				$str .= "('$id', '" . check_input($operator['ID'], $this->db->conn_id) . "', '" . check_input($operator['TOOL_NAME'], $this->db->conn_id) . "', 
		'" . check_input($operator['CREDITS'], $this->db->conn_id) . "', '" . check_input($operator['DELIVERY_TIME'], $this->db->conn_id) . "', 0)";
			}
			if ($str != '') {
				//$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
				$this->Settings_model->deleteSupplierServicesZero($id);

				//$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES " . $str);
				$this->Settings_model->addSupplierServices($str);

				$msg = 'Services have been synchronized successfully!';
			} else
				$msg = 'Error while synchronizing the services!';
		}
	}
	public function sync_simunlock($id)
	{

		//require('../../../apis/simunlocks_api/class.mgr_simunlocks.php');
		$params = array('url' => $this->url, 'apiKey' => $this->apiKey);

		$this->load->library('CI_mgr_simunlocks', $params);
		//$objAPI = new mgr_simunlocks($this->url, $this->apiKey);
		$response = $this->CI_mgr_simunlocks->get_services();
		if (count($response) == '1') {
			$msg = 'Error: ' . $response;
		} else {
			$str = '';
			foreach ($response as $s) {
				$serviceName = check_input($s->group_name . ' - ' . $s->name, $this->db->conn_id);
				if ($str != '')
					$str .= ',';
				$str .= "('$id', '" . check_input($s->id, $this->db->conn_id) . "', '$serviceName', 
		'" . check_input($s->credits, $this->db->conn_id) . "', '" . check_input($s->delivery, $this->db->conn_id) . "', 0)";
			}
			if ($str != '') {
				//$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
				$this->Settings_model->deleteSupplierServicesZero($id);
				//$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES " . $str);
				$this->Settings_model->addSupplierServices($str);
				$msg = 'Services have been synchronized successfully!';
			} else
				$msg = 'Error while synchronizing the services!';
		}

	}
	public function sync_gsmunlockusa($syncIMEIS, $id)
	{

		//require('../../../apis/gsmunlockusa.com/API.php');
		//require('../../../apis/gsmunlockusa.com/ArrayToXML.php');
		$Data = $this->CI_gsmunlockusa_api->GetService($this->apiKey, $this->url);
		if (!empty($Data) && $Data != null) {    /* Everything works fine */
			// Output: Array
			$RESPONSE_ARR = array();
			$RESPONSE_ARR = $Data['GetServiceResult']['API'];
			$str = '';
			$strCat = '';
			$strServices = '';
			if ($syncIMEIS == '1')
				$this->Settings_model->archiveServicesPackage1();
			$prevCatId = 0;
			foreach ($RESPONSE_ARR['Service'] as $k => $v) {
				if (is_array($v)) {
					if ($syncIMEIS == '1') {
						if ($prevCatId > 0) {
							if ($v['PriceGroupID'] != $prevCatId) {
								if ($strCat != '')
									$strCat .= ',';
								$strCat .= "('" . check_input($v['PriceGroupID'], $this->db->conn_id) . "', '" . check_input($v['PriceGroupName'], $this->db->conn_id) . "','0', '0')";
							}
						} else {
							$strCat = "('" . check_input($v['PriceGroupID'], $this->db->conn_id) . "', '" . check_input($v['PriceGroupName'], $this->db->conn_id) . "','0', '0')";
						}
					}
					if ($str != '')
						$str .= ',';
					$str .= "('$id', '" . check_input($v['ID'], $this->db->conn_id) . "', '" . check_input($v['Name'], $this->db->conn_id) . "', '" . check_input($v['Credit'], $this->db->conn_id) . "', '" . check_input($v['ProcessTime'], $this->db->conn_id) . "', 0)";
					if ($syncIMEIS == '1') {
						if ($strServices != '')
							$strServices .= ',';
						$strServices .= "('" . check_input($v['PriceGroupID'], $this->db->conn_id) . "', '" . check_input($v['Name'], $this->db->conn_id) . "', '" . check_input($v['Credit'], $this->db->conn_id) . "', '" . check_input($v['ProcessTime'], $this->db->conn_id) . "', '', 0, 0)";
					}
					$prevCatId = $v['PriceGroupID'];
				}
			}
			if ($str != '') {
//				$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
//				$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES " . $str);
				$this->Settings_model->deleteSupplierServicesZero($id);
				$this->Settings_model->addSupplierServices($str);
				$msg = 'Services have been synchronized successfully!';
			}
			if ($syncIMEIS == '1' && $strServices != '') {
/*				$this->db->query("DELETE FROM tbl_gf_plans_packages_prices WHERE ServiceType = '0'");
				$this->db->query("DELETE FROM tbl_gf_users_packages_prices");
				$this->db->query("DELETE FROM tbl_gf_user_packages WHERE ServiceType = '0'");
*/
				$this->Settings_model->deletePlanPackagesPrices();
				$this->Settings_model->deleteUsersPackagesPrices();
				$this->Settings_model->deleteUsersPackages();
				$this->Settings_model->archivePackages();

				$this->db->query("UPDATE tbl_gf_packages SET ArchivedPack = '1' WHERE sl3lbf = '0'");
				if ($strCat != '') {
					//$this->db->query("INSERT INTO tbl_gf_package_category (CategoryId, Category, DisableCategory, SL3BF) VALUES $strCat");
					$this->Settings_model->addServicesPackage2($strCat);
					}

				//$this->db->query("INSERT INTO tbl_gf_packages (CategoryId, PackageTitle, PackagePrice, TimeTaken, MustRead, DisablePackage, sl3lbf) VALUES $strServices");
				$this->Settings_model->addPackages($strServices);
				$msg = 'Services have been synchronized successfully!';
			}
		} else
			$msg = 'Error while synchronizing the services';
	}
	public function sync_unlock($id, $syncIMEIS)
	{

		//require('../../../apis/unlock.hk/API.php');
		$XML = $this->CI_UnlockHK->CallAPI($this->apiKey, $this->accountId, $this->url, 'GetModels');
		if (is_string($XML)) {
			/* Parse the XML stream */
			$Data = $this->CI_UnlockHK->ParseXML($XML);

			if (is_array($Data)) {
				if (isset($Data['Error'])) {
					$msg = 'API error : ' . htmlspecialchars($Data['Error']);
				} else {
					$str = '';
					$strServices = '';
					if ($syncIMEIS == '1')
						//$this->db->query("UPDATE tbl_gf_package_category SET ArchivedCategory = '1' WHERE SL3BF = '0'");
						$this->Settings_model->archiveServicesPackage1();

					foreach ($Data['PhoneBrand'] as $PhoneBrand) {
						if ($syncIMEIS == '1') {
							//$this->db->query("INSERT INTO tbl_gf_package_category (CategoryId, Category, DisableCategory, SL3BF) VALUES
							$value = "('" . check_input($PhoneBrand['ID'], $this->db->conn_id) . "', '" . check_input($PhoneBrand['Name'], $this->db->conn_id) . "', 0, 0)";
							$this->Settings_model->addServicesPackage2($value);
						}
//				print('<b><font color="'.$PhoneBrand['Color'].'">['.$PhoneBrand['ID'].'] '.htmlspecialchars($PhoneBrand['Name']).'</font></b><br />');
						foreach ($PhoneBrand['PhoneModel'] as $PhoneModel) {
							if ($str != '')
								$str .= ',';
							$serviceName = htmlspecialchars($PhoneBrand['Name']) . ' - ' . htmlspecialchars($PhoneModel['Name']);
							$str .= "('$id', '" . check_input(htmlspecialchars($PhoneModel['ID']), $this->db->conn_id) . "', '" . check_input($serviceName, $this->db->conn_id) . "', 
					'" . check_input(htmlspecialchars($PhoneModel['Credits']), $this->db->conn_id) . "', 0)";
							if ($syncIMEIS == '1') {
								if ($strServices != '')
									$strServices .= ',';
								$strServices .= "('" . check_input($PhoneBrand['ID'], $this->db->conn_id) . "', 
										'" . check_input(htmlspecialchars($PhoneModel['Name']), $this->db->conn_id) . "', 
										'" . check_input(htmlspecialchars($PhoneModel['Credits']), $this->db->conn_id) . "', 
										'" . check_input($PhoneModel['Remark'], $this->db->conn_id) . "', 0, 0)";
							}

							//print('&nbsp;&nbsp;['.$PhoneModel['ID'].'] '.htmlspecialchars($PhoneModel['Name']).'('.$PhoneModel['Credits'].') - '.$PhoneModel['Remark'].'<br />');
						}
					}
					if ($str != '') {
						//$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
						$this->Settings_model->deleteSupplierServicesZero($id);

						//$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceType) VALUES " . $str);
						$this->Settings_model->addSupplierServices2($str);
						$msg = 'Services have been synchronized successfully!';
					}
					if ($syncIMEIS == '1' && $strServices != '') {
/*
 						$this->db->query("DELETE FROM tbl_gf_plans_packages_prices WHERE ServiceType = '0'");
						$this->db->query("DELETE FROM tbl_gf_users_packages_prices");
						$this->db->query("DELETE FROM tbl_gf_user_packages WHERE ServiceType = '0'");
*/
						$this->Settings_model->deletePlanPackagesPrices();
						$this->Settings_model->deleteUsersPackagesPrices();
						$this->Settings_model->deleteUsersPackages();

						//$this->db->query("UPDATE tbl_gf_packages SET ArchivedPack = '1' WHERE sl3lbf = '0'");
						$this->Settings_model->archivePackages();
						//$this->db->query("INSERT INTO tbl_gf_packages (CategoryId, PackageTitle, PackagePrice, MustRead, DisablePackage, sl3lbf) VALUES " . $strServices);
						$this->Settings_model->addPackages2($strServices);
						$msg = 'Services have been synchronized successfully!';
					}
				}
			} else {
				/* Parsing error */
				$msg = 'Could not parse the XML stream';
			}
		} else {
			$msg = 'Could not communicate with the api';
		}

	}
	public function sync_chimeratool($id, $apiKey, $accountId, $url){
		//require('../../../apis/chimeratoolapi/chimeratoolapi.php');
		$str = "('$id', '1', 'Chimera Tool', '', '', 2)";
		// $api = new ChimeraApi();
		$data = $this->CI_chimeratoolapi->availableLicences($apiKey, $accountId, $url);
		if (is_array($data))
		{
			if (isset($data['success']) && $data['success'] == '1')
			{
				if (isset($data['licences']) && $data['licences'] != '')
				{
					foreach($data['licences'] as $key => $value)
					{
						$name = '';
						$friendlyNm = '';
						$price = '';
						foreach($value as $key1 => $value1)
						{
							if($key1 == 'name')
								$name = $value1;
							else if($key1 == 'friendlyName')
								$friendlyNm = $value1;
							else if($key1 == 'price')
								$price = $value1;
						}
						if($name != '' && $friendlyNm != '')
						{
							$str .= ", ('$id', '".check_input($name, $this->db->conn_id)."', '".check_input('Chimera Licence - '.$friendlyNm, $this->db->conn_id)."', '".check_input($price, $this->db->conn_id)."', '', '2')";
						}
					}

				}
			}
		}
		$this->Settings_model->execute("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id'");
		$this->Settings_model->execute("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES $str");

		$msg = 'Services have been synchronized successfully!';
	}
	public function sync_z3xteam($id){
		$this->Settings_model->execute("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id'");
		$this->Settings_model->execute("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES
		('$id', '1', 'LG Activation', '', '', 2), ('$id', '2', 'Samsung Activation', '', '', 2), ('$id', '3', 'Samsung Update', '', '', 2),
		('$id', '4', 'Create 30 Credits Pack', '', '', 2), ('$id', '5', 'Create 50 Credits Pack', '', '', 2), ('$id', '6', 'Refill Credit Pack', '', '', 2)");
		$msg = 'Services have been synchronized successfully!';
	}
	public function sync_dhru_fileservices($id,$apiType,$apiKey, $accountId, $url){
		if($apiType == '2')
		{
			$api = new DhruFusion();
			$arrResponse = $api->action('fileservicelist', $apiKey, $accountId, $url);
			if (is_array($arrResponse))
			{
				if (isset($arrResponse['SUCCESS'][0]))
				{
					$RESPONSE_ARR = array();
					if(isset($arrResponse['SUCCESS'][0]['LIST']) && is_array($arrResponse['SUCCESS'][0]['LIST']))
						$RESPONSE_ARR = $arrResponse['SUCCESS'][0]['LIST'];
					$str = '';
					$strFileServices = '';
					if($syncFS == '1')
						$this->db->query("UPDATE tbl_gf_package_category SET ArchivedCategory = '1' WHERE SL3BF = '1'");
					foreach( $RESPONSE_ARR as $key => $value)
					{
						if(is_array($value['SERVICES']))
						{
							$dhruCatId = 0;
							if($syncFS == '1')
							{
								$dhruCatId = $this->Settings_model->fusionclientapiInsertData("INSERT INTO tbl_gf_package_category (Category, DisableCategory, SL3BF) VALUES ('".addslashes($value['GROUPNAME'])."', 0, 1)");
							}
							foreach($value['SERVICES'] as $k => $v)
							{
								if($str != '')
									$str .= ',';
								$str .= "('$id', '".addslashes($v['SERVICEID'])."', '".addslashes($v['SERVICENAME'])."', '".addslashes($v['CREDIT'])."', '".addslashes($v['TIME'])."', 1)";

								if($syncFS == '1')
								{
									if($strFileServices != '')
										$strFileServices .= ',';
									$strFileServices .= "('$dhruCatId', '".addslashes($v['SERVICENAME'])."', '".addslashes($v['CREDIT'])."', '".addslashes($v['TIME'])."', '".addslashes($v['INFO'])."', 0, 1)";
								}
							}
						}
					}
					if($str != '')
					{
						$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '1'");
						$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES ".$str);
						$msg = 'Services have been synchronized successfully!';
					}
					if($syncFS == '1' && $strFileServices != '')
					{
						$this->db->query("DELETE FROM tbl_gf_plans_packages_prices WHERE ServiceType = '1'");
						$this->db->query("DELETE FROM tbl_gf_users_packages_prices");
						$this->db->query("DELETE FROM tbl_gf_user_packages WHERE ServiceType = '1'");
						$this->db->query("UPDATE tbl_gf_packages SET ArchivedPack = '1' WHERE sl3lbf = '1'");
						$this->db->query("INSERT INTO tbl_gf_packages (CategoryId, PackageTitle, PackagePrice, TimeTaken, MustRead, DisablePackage, sl3lbf) VALUES ".$strFileServices);
						$msg = 'Services have been synchronized successfully!';
					}
				}
			}
			else
			{
				$msg = 'Connection Error: Could not parse the XML stream!';
			}
		}
	}
	public function sync_gsmf_fileservices($id,$apiType,$apiKey, $accountId, $url){
		$objGSMFUSIONAPI = new GSMFUSIONAPI();
		$objGSMFUSIONAPI->doAction('fileservices', $apiKey, $url, $accountId, array());
		$objGSMFUSIONAPI->XmlToArray($objGSMFUSIONAPI->getResult());
		$arrayData = $objGSMFUSIONAPI->createArray();
		if(isset($arrayData['error']) && sizeof($arrayData['error']) > 0)
		{
			$msg = '<b>'.$arrayData['error'][0].'</b>';
		}
		else
		{
			$RESPONSE_ARR = array();
			if(isset($arrayData['Packages']['Package']) && sizeof($arrayData['Packages']['Package']) > 0)
				$RESPONSE_ARR = $arrayData['Packages']['Package'];
			$total = count($RESPONSE_ARR);
			$str = '';
			$strFileServices = '';
			if($syncFS == '1')
				$this->db->query("UPDATE tbl_gf_package_category SET ArchivedCategory = '1' WHERE SL3BF = '1'");
			$prevCatId = 0;
			for($count = 0; $count < $total; $count++)
			{
				if($syncFS == '1')
				{
					if($prevCatId > 0)
					{
						if($RESPONSE_ARR[$count]['CategoryId'] != $prevCatId)
						{
							$this->db->query("INSERT INTO tbl_gf_package_category (CategoryId, Category, DisableCategory, SL3BF) VALUES
					('".check_input($RESPONSE_ARR[$count]['CategoryId'], $this->db->conn_id)."', '".check_input($RESPONSE_ARR[$count]['Category'], $this->db->conn_id)."', 0, 1)");
							$objDBCD14->execute();
						}
					}
					else
					{
						$this->db->query("INSERT INTO tbl_gf_package_category (CategoryId, Category, DisableCategory, SL3BF) VALUES
				('".check_input($RESPONSE_ARR[$count]['CategoryId'], $this->db->conn_id)."', '".check_input($RESPONSE_ARR[$count]['Category'], $this->db->conn_id)."', 0, 1)");
					}
				}
				if($str != '')
					$str .= ',';
				$str .= "('$id', '".check_input($RESPONSE_ARR[$count]['PackageId'], $this->db->conn_id)."', '".check_input($RESPONSE_ARR[$count]['PackageTitle'], $this->db->conn_id)."',
				'".check_input($RESPONSE_ARR[$count]['PackagePrice'], $this->db->conn_id)."', '".check_input($RESPONSE_ARR[$count]['TimeTaken'], $this->db->conn_id)."', 1)";
				if($syncFS == '1')
				{
					if($strFileServices != '')
						$strFileServices .= ',';
					$strFileServices .= "('".check_input($RESPONSE_ARR[$count]['CategoryId'], $this->db->conn_id)."', '".check_input($RESPONSE_ARR[$count]['PackageTitle'], $this->db->conn_id)."',
				'".check_input($RESPONSE_ARR[$count]['PackagePrice'], $this->db->conn_id)."', '".check_input($RESPONSE_ARR[$count]['TimeTaken'], $this->db->conn_id)."',
				'".check_input($RESPONSE_ARR[$count]['MustRead'], $this->db->conn_id)."', 0, 1)";
				}
				$prevCatId = $RESPONSE_ARR[$count]['CategoryId'];
			}
			if($str != '')
			{
				$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '1'");
				$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES ".$str);
				$msg = 'Services have been synchronized successfully!';
			}
			if($syncFS == '1' && $strFileServices != '')
			{
				$this->db->query("DELETE FROM tbl_gf_plans_packages_prices WHERE ServiceType = '1'");
				$this->db->query("DELETE FROM tbl_gf_users_packages_prices");
				$this->db->query("DELETE FROM tbl_gf_user_packages WHERE ServiceType = '1'");
				$this->db->query("UPDATE tbl_gf_packages SET ArchivedPack = '1' WHERE sl3lbf = '1'");
				$this->db->query("INSERT INTO tbl_gf_packages (CategoryId, PackageTitle, PackagePrice, TimeTaken, MustRead, DisablePackage, sl3lbf) VALUES ".$strFileServices);

				$msg = 'Services have been synchronized successfully!';
			}
		}

	}
	public function sync_nksh_fileservices($id,$apiType,$apiKey, $accountId, $url){
		$objAPI = new NAKSHAPI();
		$objAPI->doAction('fileservices', $apiKey, $url, $accountId, array());
		$objAPI->XmlToArray($objAPI->getResult());
		$arrayData = $objAPI->createArray();
		if(isset($arrayData['error']) && sizeof($arrayData['error']) > 0)
		{
			$msg = '<b>'.$arrayData['error'][0].'</b>';
		}
		else
		{
			$RESPONSE_ARR = array();
			if(isset($arrayData['Packages']['Package']) && sizeof($arrayData['Packages']['Package']) > 0)
				$RESPONSE_ARR = $arrayData['Packages']['Package'];
			$total = count($RESPONSE_ARR);
			$str = '';
			$strFileServices = '';
			if($syncFS == '1')
				$this->db->query("UPDATE tbl_gf_package_category SET ArchivedCategory = '1' WHERE SL3BF = '1'");
			$prevCatId = 0;
			for($count = 0; $count < $total; $count++)
			{
				if($syncFS == '1')
				{
					if($prevCatId > 0)
					{
						if($RESPONSE_ARR[$count]['CategoryId'] != $prevCatId)
						{
							$this->db->query("INSERT INTO tbl_gf_package_category (CategoryId, Category, DisableCategory, SL3BF) VALUES ('".addslashes($RESPONSE_ARR[$count]['CategoryId'])."', '".addslashes($RESPONSE_ARR[$count]['Category'])."', 0, 1)");
						}
					}
					else
					{
						$this->db->query("INSERT INTO tbl_gf_package_category (CategoryId, Category, DisableCategory, SL3BF) VALUES ('".addslashes($RESPONSE_ARR[$count]['CategoryId'])."', '".addslashes($RESPONSE_ARR[$count]['Category'])."', 0, 1)");

					}
				}
				if($str != '')
					$str .= ',';
				$str .= "('$id', '".$RESPONSE_ARR[$count]['PackageId']."', '".addslashes($RESPONSE_ARR[$count]['PackageTitle'])."', '".addslashes($RESPONSE_ARR[$count]['PackagePrice'])."',
				 '".addslashes($RESPONSE_ARR[$count]['TimeTaken'])."', 1)";
				if($syncFS == '1')
				{
					if($strFileServices != '')
						$strFileServices .= ',';
					$strFileServices .= "('".addslashes($RESPONSE_ARR[$count]['CategoryId'])."', '".addslashes($RESPONSE_ARR[$count]['PackageTitle'])."', '".addslashes($RESPONSE_ARR[$count]['PackagePrice'])."',
				'".addslashes($RESPONSE_ARR[$count]['TimeTaken'])."', '".addslashes($RESPONSE_ARR[$count]['MustRead'])."', 0, 1)";
				}
				$prevCatId = $RESPONSE_ARR[$count]['CategoryId'];
			}
			if($str != '')
			{
				$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '1'");
				$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES ".$str);
				$msg = 'Services have been synchronized successfully!';
			}
			if($syncFS == '1' && $strFileServices != '')
			{
				$this->db->query("DELETE FROM tbl_gf_plans_packages_prices WHERE ServiceType = '1'");
				$this->db->query("DELETE FROM tbl_gf_users_packages_prices");
				$this->db->query("DELETE FROM tbl_gf_user_packages WHERE ServiceType = '1'");
				$this->db->query("UPDATE tbl_gf_packages SET ArchivedPack = '1' WHERE sl3lbf = '1'");
				$this->db->query("INSERT INTO tbl_gf_packages (CategoryId, PackageTitle, PackagePrice, TimeTaken, MustRead, DisablePackage, sl3lbf) VALUES ".$strFileServices);

				$msg = 'Services have been synchronized successfully!';
			}
		}
	}
	public function sync_dhru_serverservices($id,$apiType,$apiKey, $accountId, $url){
		$api = new DhruFusion();
		if($apiType == '1' || $apiType == '2')
		{
			$arrResponse = $api->action('imeiservicelist', $apiKey, $accountId, $url);
		}
		else if($apiType == '7')
		{
			$arrResponse = $api->action('serverservicelist', $apiKey, $accountId, $url);
			$arrResponse_ST = $api->action('serverservicetypelist', $apiKey, $accountId, $url);

			/********************************** GETTING SERVICE TYPES ******************************************/
			$RESPONSE_ARR_ST = array();
			if(isset($arrResponse_ST['SUCCESS'][0]['LIST']) && is_array($arrResponse_ST['SUCCESS'][0]['LIST']))
				$RESPONSE_ARR_ST = $arrResponse_ST['SUCCESS'][0]['LIST'];

			$strServiceTypes = '';
			foreach( $RESPONSE_ARR_ST as $key => $value_ST)
			{
				if($strServiceTypes != '')
					$strServiceTypes .= ',';
				$strServiceTypes .= "('".check_input($value_ST['tool_id'], $this->db->conn_id)."', '".check_input($value_ST['id'], $this->db->conn_id)."', '".check_input($value_ST['name'], $this->db->conn_id)."', '".check_input($value_ST['price'], $this->db->conn_id)."', 2, '$id')";
			}
			if($strServiceTypes != '')
			{
				$this->db->query("DELETE FROM tbl_gf_service_service_types WHERE APIId = '$id' AND ServiceType = '2'");
				$this->db->query("INSERT INTO tbl_gf_service_service_types (ServiceId, ServiceTypeId, ServiceTypeName, ServiceTypePrice, ServiceType, APIId) VALUES $strServiceTypes");
			}
			/********************************** GETTING SERVICE TYPES ******************************************/
		}
		if (is_array($arrResponse))
		{
			if (isset($arrResponse['SUCCESS'][0]))
			{
				$RESPONSE_ARR = array();
				if(isset($arrResponse['SUCCESS'][0]['LIST']) && is_array($arrResponse['SUCCESS'][0]['LIST']))
					$RESPONSE_ARR = $arrResponse['SUCCESS'][0]['LIST'];
				$str = '';
				$strServices = '';
				if($syncSS == '1')
					$this->db->query("UPDATE tbl_gf_log_package_category SET ArchivedCategory = '1'");
				foreach( $RESPONSE_ARR as $key => $value)
				{
					if(is_array($value['SERVICES']))
					{
						$dhruCatId = 0;
						if($syncSS == '1')
						{
							$dhruCatId = $this->Setting_model->fusionclientapiInsertData("INSERT INTO tbl_gf_log_package_category (Category, DisableCategory) VALUES ('".addslashes($value['GROUPNAME'])."', 0)");
						}
						foreach($value['SERVICES'] as $k => $v)
						{
							if($str != '')
								$str .= ',';
							$str .= "('$id', '".addslashes($v['SERVICEID'])."', '".addslashes($v['SERVICENAME'])."', '".addslashes($v['CREDIT'])."', '".addslashes($v['TIME'])."', '".addslashes($v['REQUIRED'])."', 2)";

							if($syncSS == '1')
							{
								if($strServices != '')
									$strServices .= ',';
								$strServices .= "('$dhruCatId', '".addslashes($v['SERVICENAME'])."', '".addslashes($v['CREDIT'])."', '".addslashes($v['TIME'])."', '".addslashes($v['INFO'])."', 0)";
							}
						}
					}
				}
				if($str != '')
				{
					$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '2'");
					$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, RequiredParams	, ServiceType) VALUES $str");
					$msg = 'Services have been synchronized successfully!';
				}
				if($syncSS == '1' && $strServices != '')
				{
					$this->db->query("DELETE FROM tbl_gf_plans_packages_prices WHERE ServiceType = '2'");
					$this->db->query("DELETE FROM tbl_gf_users_log_packages_prices");
					$this->db->query("DELETE FROM tbl_gf_user_packages WHERE ServiceType = '2'");
					$this->db->query("UPDATE tbl_gf_log_packages SET ArchivedPack = '1'");

					$this->db->query("INSERT INTO tbl_gf_log_packages (CategoryId, LogPackageTitle, LogPackagePrice, DeliveryTime, LogPackageDetail, DisableLogPackage) VALUES $strServices");
					$msg = 'Server Services have been synchronized successfully!';
				}
			}
			else if (isset($arrResponse['ERROR'][0]))
				$msg = 'Connection Error: ' . htmlspecialchars($arrResponse['ERROR'][0]['MESSAGE']);
		}
		else
		{
			$msg = 'Connection Error: Could not parse the XML stream!';
		}
	}
	public function sync_gsmf_srvrservices($id,$apiType,$apiKey, $accountId, $url){
		$objGSMFUSIONAPI = new GSMFUSIONAPI();
		//==================================== SERVER SERVICES ==========================================//
		$objGSMFUSIONAPI->doAction('serverservices', $apiKey, $url, $accountId, array());
		$objGSMFUSIONAPI->XmlToArray($objGSMFUSIONAPI->getResult());
		$arrayData = $objGSMFUSIONAPI->createArray();
		if(isset($arrayData['error']) && sizeof($arrayData['error']) > 0)
		{
			$msg = '<b>'.$arrayData['error'][0].'</b>';
		}
		else
		{
			$RESPONSE_ARR = array();
			if(isset($arrayData['Packages']['Package']) && sizeof($arrayData['Packages']['Package']) > 0)
				$RESPONSE_ARR = $arrayData['Packages']['Package'];
			$total = count($RESPONSE_ARR);
			$str = '';
			$strServerSrvcs = '';
			if($syncSS == '1')
				$this->db->query("UPDATE tbl_gf_log_package_category SET ArchivedCategory = '1'");
			$prevCatId = 0;
			$CATEGORY_ID = 0;
			for($count = 0; $count < $total; $count++)
			{
				if($syncSS == '1')
				{
					if($prevCatId > 0)
					{
						if($RESPONSE_ARR[$count]['CategoryId'] != $prevCatId)
						{
							$CATEGORY_ID = $this->Settings_model->fusionclientapiInsertData("INSERT INTO tbl_gf_log_package_category (Category, DisableCategory) VALUES
					('".check_input($RESPONSE_ARR[$count]['Category'], $this->db->conn_id)."', 0)");
						}
					}
					else
					{
						$CATEGORY_ID = $this->Settings_model->fusionclientapiInsertData("INSERT INTO tbl_gf_log_package_category (Category, DisableCategory) VALUES
				('".check_input($RESPONSE_ARR[$count]['Category'], $this->db->conn_id)."', 0)");

					}
				}
				if($str != '')
					$str .= ',';
				$str .= "('$id', '".check_input($RESPONSE_ARR[$count]['PackageId'], $this->db->conn_id)."', '".check_input($RESPONSE_ARR[$count]['PackageTitle'], $this->db->conn_id)."',
				'".check_input($RESPONSE_ARR[$count]['PackagePrice'], $this->db->conn_id)."', '".check_input($RESPONSE_ARR[$count]['TimeTaken'], $this->db->conn_id)."', 2)";
				if($syncSS == '1')
				{
					if($strServerSrvcs != '')
						$strServerSrvcs .= ',';
					$strServerSrvcs .= "('".check_input($CATEGORY_ID, $this->db->conn_id)."', '".check_input($RESPONSE_ARR[$count]['PackageTitle'], $this->db->conn_id)."',
				'".check_input($RESPONSE_ARR[$count]['PackagePrice'], $this->db->conn_id)."', '".check_input($RESPONSE_ARR[$count]['TimeTaken'], $this->db->conn_id)."',
				'".check_input($RESPONSE_ARR[$count]['MustRead'], $this->db->conn_id)."', 0)";
				}
				$prevCatId = $RESPONSE_ARR[$count]['CategoryId'];
			}
			if($str != '')
			{
				$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '2'");
				$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES $str");
				$msg = 'Services have been synchronized successfully!';
			}
			if($syncSS == '1' && $strServerSrvcs != '')
			{
				$this->db->query("DELETE FROM tbl_gf_plans_packages_prices WHERE ServiceType = '2'");
				$this->db->query("DELETE FROM tbl_gf_users_log_packages_prices");
				$this->db->query("DELETE FROM tbl_gf_user_packages WHERE ServiceType = '2'");
				$this->db->query("UPDATE tbl_gf_log_packages SET ArchivedPack = '1'");
				$this->db->query("INSERT INTO tbl_gf_log_packages (CategoryId, LogPackageTitle, LogPackagePrice, DeliveryTime, LogPackageDetail, DisableLogPackage) VALUES $strServerSrvcs");
				$msg = 'Services have been synchronized successfully!';
			}
		}
	}
	public function fetchCredits($id, $accountId, $apiKey, $apiType, $url, $apiKey2, $apiPwd)
	{
		if($apiType == '0')
		{
			if($id == '202') // Blue Unlock
			{
				/*require_once('../../../apis/blueunlock/api.php');*/
				$api=new GFM_API();
				$api->sendCommand('GET_CREDITS', $apiKey, $url, array());
				$resultArray = $api->parse2Array($api->getResult());
				$strError = $api->checkError($resultArray['RESULT']);
				if($strError)
				{
					$msg = 'Connection Error: ' . $strError;
				}
				else
				{
					$result = $resultArray['RESULT'];
					$msg = $result['CREDITS'];
				}
			}
			else if($id == '204') //Unlock Base
			{
				/*require_once('../../../apis/unlockbase/API.php');*/
				$XML = UnlockBase::CallAPI($apiKey, $url, 'AccountInfo');
				if (is_string($XML))
				{
					$Data = UnlockBase::ParseXML($XML);
					if (is_array($Data))
					{
						if (isset($Data['Error']))
						{
							$msg = 'Connection Error: ' . htmlspecialchars($Data['Error']);
						}
						else
						{
							$msg = htmlspecialchars($Data['Credits']);
						}
					}
					else
					{
						$msg = 'Connection Error: Could not parse the XML stream!';
					}
				}
				else
				{
					$msg = 'Connection Error: Could not communicate with the api!';
				}
			}
			else if($id == '206') //bruteforce.mobi
			{
				/*require('../../../apis/bruteforce.mobi/bruteforce.mobi_api.php');*/
				$arrayData = $this->CI_bruteforce_mobi_api->GetBalance($url, $accountId, $apiPwd, $apiKey, $apiKey2);
				if ($arrayData->status == 'ok')
				{
					$msg = trim($arrayData->balance);
				}
			}
			else if($id == '207') //mybruteforce.com
			{
				/*require_once('../../../apis/mybruteforce/mybruteforce_api.php');*/
				$mbfAPI = new Api();
				$arrResult = $mbfAPI->doAction('info', $url, $accountId, $apiKey, array());
				if (isset($arrResult['credits']))
				{
					$msg = $arrResult['credits'];
				}
				else
				{
					$msg = 'Connection Error! '.$arrResult['errors'];
				}
			}
			else if($id == '208') //simunlocks.com
			{
				/*require_once('../../../apis/simunlocks_api/class.mgr_simunlocks.php');*/
				$objAPI = new mgr_simunlocks($url, $apiKey);
				$response = $objAPI->credits_balance('');
				if(is_numeric($response))
					$msg = $response;
				else
					$msg = 'Connection Error!';
			}
			else if($id == '210') //gsmunlockusa.com
			{
				$this->lib->load('CI_gsmunlockusa_api');
				/*require_once('../../../apis/gsmunlockusa.com/ArrayToXML.php');*/

				$Data = $this->CI_gsmunlockusa_api->GetAccountInfo($apiKey, $url);
				if (!empty($Data) && $Data != null)
				{
					if(isset($Data['GetAccountInfoResult']['API']['Result']['Credits']))
						$msg = $Data['GetAccountInfoResult']['API']['Result']['Credits'];
				}
				else $msg = 'Connection Error!';
			}
			else if($id == '211') //unlock.uk
			{
				/*require_once('../../../apis/unlock.hk/API.php');*/
				$XML = UnlockHK::CallAPI($apiKey, $accountId, $url, 'AccountInfo');
				if (is_string($XML))
				{
					$Data = UnlockHK::ParseXML($XML);
					if (is_array($Data))
					{
						if (isset($Data['Error']))
						{
							$msg = 'Connection Error: ' . htmlspecialchars($Data['Error']);
						}
						else
						{
							$msg = htmlspecialchars($Data['Credits']);
						}
					}
					else
					{
						$msg = 'Could not communicate with the api';
					}
				}
				else
				{
					$msg = 'Could not communicate with the api';
				}
			}
			else if($id == '212') //unlocktele.com
			{
				/*require_once('../../../apis/unlocktele.com/API.php');*/
				$XML = UnlockHK::CallAPI($apiKey, $accountId, $url, 'AccountInfo');

				if (is_string($XML))
				{
					$Data = UnlockHK::ParseXML($XML);
					if (is_array($Data))
					{
						if (isset($Data['Error']))
						{
							$msg = 'Connection Error: ' . htmlspecialchars($Data['Error']);
						}
						else
						{
							$msg = htmlspecialchars($Data['Credits']);
						}
					}
					else
					{
						$msg = 'Could not communicate with the api';
					}
				}
				else
				{
					$msg = 'Could not communicate with the api';
				}
			}
			else if($id == '218' || $id == '223') //sl3team.com
			{
				if($id == '218') //sl3team.com
					$this->lib->load('CI_sl3bf');
					/*require_once('../../../apis/sl3team.com/sl3.inc.php');*/
				else if($id == '223') //sl3team.com
					$this->lib->load('CI_sl3team');
					/*require_once('../../../apis/sl3bf.net/sl3.inc.php');*/
				$api = new SL3_API($apiKey);
				$response = $api->accountInfo();
				if (isset($response['msg_code']) && $response['msg_code'] == 0)
					$msg = $response['credit'];
				else
					$msg = 'Connection Error: ' . $api->getMessage($response['msg_code']);
			}
			else if($id == '222')
			{
				/*require_once('../../../apis/chimeratoolapi/chimeratoolapi.php');*/
				$api = new ChimeraApi();
				$data = $api->getInfo($apiKey, $accountId, $url);
				if (is_array($data))
				{
					if (isset($data['success']) && $data['success'] == '1')
					{
						$msg = $data['creditBalance'];
					}
					else if (isset($data['message']) && $data['success'] == '')
					{
						$msg = 'Connection Error: ' . $data['message'];
					}
					else
					{
						$msg = 'Error while connecting to Chimera tool API!';
					}
				}
				else
				{
					$msg = 'Error while connecting to Chimera tool API!';
				}
			}
			else if($id == '224')
			{
				/*require_once('../../../apis/unlockcodesource.com/ucs.php');*/
				$ucs = new UCS();
				$ucs->setApikey($apiKey);
				$data = $ucs->getAccount();
				if (is_array($data))
				{
					if (isset($data['name']) && $data['name'] != '')
					{
						$msg = $data['credits'];
					}
					else if (isset($data['message']) && $data['message'] != '')
					{
						$msg = 'Connection Error:<br />' . $data['message'];
					}
					else
					{
						$msg = 'Error while connecting to Unlock Code Source API!';
					}
				}
				else
				{
					$msg = 'Error while connecting to Unlock Code Source API!';
				}
			}
			else if($id == '225')
			{
				/*require_once('../../../apis/gsmkody.pl/api.php');*/
				$objAPI = new gsmkody();
				$objAPI->doAction('account.json', $apiKey, $url, 'GET');
				$arrData = $objAPI->getResult();
				$strErr = $objAPI->checkError($arrData);
				if(trim($strErr) != '')
				{
					$msg = $strErr;
				}
				else
				{
					if(isset($arrData['status']) && $arrData['status'] == 'ok')
					{
						if(isset($arrData['account']) && isset($arrData['account']['id']))
							$msg = $arrData['account']['credits'];
					}
				}
			}
			else if($id == '229' || $id == '260')
			{
				/*require_once('../../../apis/z3x-team.com/z3xteamapi.class.php');*/
				$api = new Z3XTeam();
				$data = $api->action('credits_left', $apiKey, $url);
				if (is_object($data))
				{
					if ($data->ErrorCode == 0)
					{
						$msg = $data->Data->sams_upd;
					}
					else if ($data->ErrorCode > 0)
					{
						$msg = 'Connection Error: ' . $data->ErrorTxt;
					}
				}
				else
				{
					$msg = 'Error while connecting to API!';
				}
			}
			else if($id == '255')
			{
				require('../../../apis/furiousgold.com/furiousgold_api.php');
				list($result, $info, $error, $errorno) = call_api('CHECK_MY_CREDITS', $accountId, $apiKey, $url);

				if (isset($result['ERROR']))
				{
					$errors['response'] = $result['ERROR'] . ' => error number ' . $result['ERROR_CODE'];
					$msg = 'Connection Error: ' . $errors['response'];
				}
				else if (isset($result['REQUESTED_METHOD']) && $result['REQUESTED_METHOD'] == 'CHECK_MY_CREDITS') // the response is valid
				{
					$response = $result['RESULT']['AMOUNT'] . ' ' . $result['RESULT']['CURRENCY'];
					$msg = $response;
				}
				else
					$msg = 'Unknown API Connection Error';
			}
			else if($id == '256')
			{
				/*require('../../../apis/dc-unlocker.com/DcUnlockerAPI.class.php');*/
				$API = new DCUnlockerAPI($accountId, $apiKey);
				$action = $API->information();
				$action->creditsLeft();
				$message = $action->submit();
				$results = $message->get_response();
				if (isset($results['errors']))
				{
					if($results['errors'] == 0)
						$msg = extractFloatFromString($results['response']['message']);
					else
						$msg = 'Connection Error: ' . $results['response']['message'];
				}
				else
					$msg = 'Connection Error! - Unknown Error';
			}
		}
		else if($apiType == '1' || $apiType == '7')
		{
			/*require_once('../../../apis/fusionclientapi/dhrufusionapi.class.php');*/
			$api = new DhruFusion();
			$arrResponse = $api->action('imeiservicelist', $apiKey, $accountId, $url);
			if (is_array($arrResponse))
			{
				if (isset($arrResponse['SUCCESS'][0]))
					$msg = 'Connected Successfully!';
				else if (isset($arrResponse['ERROR'][0]))
					$msg = 'Connection Error: ' . htmlspecialchars($arrResponse['ERROR'][0]['MESSAGE']);
			}
			else
			{
				$msg = 'Connection Error: Could not parse the XML stream! Try changing the URL.';
			}
		}
		else if($apiType == '2')
		{
			/*require_once('../../../../apis/fusionclientapi/dhrufusionapi.class.php');*/
			$api = new DhruFusion();
			$arrResponse = $api->action('accountinfo', $apiKey, $accountId, $url);
			if (is_array($arrResponse))
			{
				if (isset($arrResponse['SUCCESS'][0]['AccoutInfo']['credit']))
					$msg = $arrResponse['SUCCESS'][0]['AccoutInfo']['credit'];
				else if (isset($arrResponse['ERROR'][0]))
					$msg = 'Connection Error: ' . htmlspecialchars($arrResponse['ERROR'][0]['MESSAGE']);
			}
			else
			{
				$msg = 'Connection Error: Could not parse the XML stream!';
			}
		}
		else if($apiType == '4' || $apiType == '6')
		{
			/*require_once('../../../../apis/gsmfusionapi/api/gsmfusion_api.php');*/
			$objGSMFUSIONAPI = new GSMFUSIONAPI();
			$objGSMFUSIONAPI->doAction('accountinfo', $apiKey, $url, $accountId, array());
//$objGSMFUSIONAPI->getResult();
			$objGSMFUSIONAPI->XmlToArray($objGSMFUSIONAPI->getResult());
			$arrayData = $objGSMFUSIONAPI->createArray();
			if(isset($arrayData['error']) && sizeof($arrayData['error']) > 0)
			{
				$msg = '<b>'.$arrayData['error'][0].'</b>';
			}
			else if(isset($arrayData['Client']['ClientCredits']))
			{
				$msg = $arrayData['Client']['ClientCredits'];
			}
		}
		return trim($msg);
	}
	//work by danish
	public function sync_bruteforce_mobi($id){

		$this->Settings_model-> executeQuery("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '1'");

		$this->Settings_model->executeQuery("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES
		('$id', 'slow', 'BruteForce.mobi - Slow', 'N/A', '', 1), ('$id', 'norm', 'BruteForce.mobi - Normal', 'N/A', '', 1), ('$id', 'fast', 'BruteForce.mobi - Fast', 'N/A', '', 1)");

		$msg = 'Services have been synchronized successfully!';
	}
	public function sync_furiousgold_com($id){
		//require('../../../apis/furiousgold.com/furiousgold_api.php');
		list($result, $info, $error, $errorno) = $this->CI_furiousgold->call_api('LIST_PRODUCTS', $accountId, $apiKey, $url);
		if (is_array($result) && isset($result['REQUESTED_METHOD']) && $result['REQUESTED_METHOD'] == 'LIST_PRODUCTS') // the response is valid
		{
			if (isset($result['ERROR']))
			{
				$errors['response'] = $result['ERROR'] . ' => error number ' . $result['ERROR_CODE'];
				$msg = 'Connection Error: ' . $errors['response'];
			}
			else
			{
				$this->Settings_model->executeQuery("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id'");
				$this->Settings_model->executeQuery("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES
				('$id', '1', 'Credits Transferring', 'N/A', '', '2')");

				$msg = 'Services have been synchronized successfully!';

			}
		}
	}
	public function sync_dc_unlocker_com($id){
		// require('../../../apis/dc-unlocker.com/DcUnlockerAPI.class.php');
		// $API = new DCUnlockerAPI($accountId, $apiKey);

		$str = "('$id', 'TC-1', 'Transfer Credits For New User', '-', '', '2'), ('$id', 'TC-2', 'Transfer Credits For New Reseller', '-', '', '2'), ('$id', 'TC-3', 'Transfer Credits For Existing User/Reseller', '-', '', '2'), ('$id', 'TC-4', 'Transfer Credits To Dongle', '-', '', '2')";

		$action = $this->CI_dc_unlocker->information();
		$action->getDongleActivationList(); // request dongle activations
		$message = $action->submit();
		$results = $message->get_response();
		if (isset($results['errors']))
		{
			if($results['errors'] == 0)
			{
				$arrData = $results['response']['activations'];
				foreach ($arrData as $result)
				{
					if(isset($result['id']) && $result['id'] != '')
					{
						$str .= ", ('$id', 'ACT-".check_input($result['id'], $this->db->conn_id)."', '".check_input($result['name'], $objDBCD14->dbh)."',
						'".check_input($result['price'], $this->db->conn_id)."', '', '2')";
					}
				}
			}
			else
				$msg = 'Connection Error: ' . $results['response']['message'].'<br />';
		}
		else
			$msg = 'Connection Error! - Unknown Error<br />';

		$action = $this->CI_dc_unlocker->information();
		$action->getDongleSupportList(); // request dongle activations
		$message = $action->submit();
		$results = $message->get_response();
		if (isset($results['errors']))
		{
			if($results['errors'] == 0)
			{
				$arrData = $results['response']['supports'];
				foreach ($arrData as $result)
				{
					if(isset($result['id']) && $result['id'] != '')
					{
						$str .= ", ('$id', 'SUP-".check_input($result['id'], $this->db->conn_id)."', '".check_input($result['name'], $this->db->conn_id)."',
						'".check_input($result['price'], $this->db->conn_id)."', '', '2')";
					}
				}
			}
			else
				$msg .= 'Connection Error: ' . $results['response']['message'].'<br />';
		}
		else
			$msg .= 'Connection Error! - Unknown Error<br />';

		$action = $this->CI_dc_unlocker->information();
		$action->getTimedLicenseList(); // request dongle activations
		$message = $action->submit();
		$results = $message->get_response();
		if (isset($results['errors']))
		{
			if($results['errors'] == 0)
			{
				$arrData = $results['response']['licenses'];
				foreach ($arrData as $result)
				{
					if(isset($result['id']) && $result['id'] != '')
					{
						$str .= ", ('$id', 'LIC-".check_input($result['id'], $this->db->conn_id)."', '".check_input($result['name'], $this->db->conn_id)."',
						'".check_input($result['price'], $this->db->conn_id)."', '', '2')";
					}
				}
			}
			else
				$msg .= 'Connection Error: ' . $results['response']['message'].'<br />';
		}
		else
			$msg .= 'Connection Error! - Unknown Error<br />';

		$action = $API->information();
		$action->getFeaturesList(); // request dongle activations
		$message = $action->submit();
		$results = $message->get_response();
		if (isset($results['errors']))
		{
			if($results['errors'] == 0)
			{
				$arrData = $results['response']['features'];
				foreach ($arrData as $result)
				{
					if(isset($result['id']) && $result['id'] != '')
					{
						$str .= ", ('$id', 'FTR-".check_input($result['id'], $this->db->conn_id)."', '".check_input($result['name'], $this->db->conn_id)."',
						'".check_input($result['price'], $this->db->conn_id)."', '', '2')";
					}
				}
			}
			else
				$msg .= 'Connection Error: ' . $results['response']['message'].'<br />';
		}
		else
			$msg .= 'Connection Error! - Unknown Error<br />';

		if($str != '')
		{
			$this->Settings_model->execute("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '2'");

			$this->Settings_model->execute("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES $str");

			$msg = 'Services have been synchronized successfully!';
		}
		else
			$msg .= 'Error while synchronizing the server services!';
	}
	public function sync_samkey_org($id){
		$this->Settings_model->execute("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '2'");
		$this->Settings_model->execute("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES
		('$id', '1', 'New User', '', '', 2), ('$id', '2', 'Existing User', '', '', 2)");

		$msg = 'Server Services have been synchronized successfully!';
	}
	public function sync_miracleserver($id){
		$this->Settings_model->execute("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '2'");

		if($id == '501')
		{
			$this->Settings_model->execute("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES
			('$id', '3', 'Renew SerialNumber License', '', '', 2)");
		}
		else if($id == '508')
		{
			$this->Settings_model->execute("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES
				('$id', '10', 'Add/Renew Pack To SerialNumber - FRP Tool', '', '', 2),
				('$id', '11', 'Add/Renew Pack To SerialNumber - Huawei Pack 1', '', '', 2),
				('$id', '12', 'Add/Renew Pack To SerialNumber - Huawei Pack 2', '', '', 2)");
		}
		$msg = 'Server Services have been synchronized successfully!';
	}
	public function sync_octopus($id){
		$this->Settings_model->execute("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '2'");

		$this->Settings_model->execute("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES
		('$id', '1', 'Add Credits For New Users', '', '', 2), ('$id', '2', 'Update Credits For Existing Users', '', '', 2)");

		$msg = 'Server Services have been synchronized successfully!';
	}
	public function sync_samkey_us(){

	}
	public function sync_easy_firmware(){
		$this->Settings_model->execute("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '2'");
		$this->Settings_model->execute("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES
		('$id', '1', 'Transfer Credits', '1', '', 2)");
		$msg = 'Server Services have been synchronized successfully!';
	}

	public function sync_unlocktele($id, $syncIMEIS)
	{

		require('../../../apis/unlocktele.com/API.php');
		$XML = $this->CI_unlocktele_unlockhk->CallAPI($this->apiKey, $this->accountId, $this->url, 'GetModels');
		if (is_string($XML)) {
			/* Parse the XML stream */
			$Data = $this->CI_unlocktele_unlockhk->ParseXML($XML);

			if (is_array($Data)) {
				if (isset($Data['Error'])) {
					$msg = 'API error : ' . htmlspecialchars($Data['Error']);
				} else {
					$str = '';
					$strServices = '';
					if ($syncIMEIS == '1') {
						//$this->db->query("UPDATE tbl_gf_package_category SET ArchivedCategory = '1' WHERE SL3BF = '0'");
						$this->Settings_model->archiveServicesPackage1();
					}
					foreach ($Data['PhoneBrand'] as $PhoneBrand) {
						if ($syncIMEIS == '1') {
							//$this->db->query("INSERT INTO tbl_gf_package_category (CategoryId, Category, DisableCategory, SL3BF) VALUES 
							$value = "('" . check_input($PhoneBrand['ID'], $this->db->conn_id) . "', '" . check_input($PhoneBrand['Name'], $this->db->conn_id) . "', 0, 0)";
							$this->Settings_model->addServicesPackage2($value);
						}
						foreach ($PhoneBrand['PhoneModel'] as $PhoneModel) {
							if ($str != '')
								$str .= ',';
							$serviceName = htmlspecialchars($PhoneBrand['Name']) . ' - ' . htmlspecialchars($PhoneModel['Name']);
							$str .= "('$id', '" . check_input(htmlspecialchars($PhoneModel['ID']), $this->db->conn_id) . "', '" . check_input($serviceName, $this->db->conn_id) . "', 
					'" . check_input(htmlspecialchars($PhoneModel['Credits']), $this->db->conn_id) . "', 0)";
							if ($syncIMEIS == '1') {
								if ($strServices != '')
									$strServices .= ',';
								$strServices .= "('" . check_input($PhoneBrand['ID'], $this->db->conn_id) . "', 
										'" . check_input(htmlspecialchars($PhoneModel['Name']), $this->db->conn_id) . "', 
										'" . check_input(htmlspecialchars($PhoneModel['Credits']), $this->db->conn_id) . "', 
										'" . check_input($PhoneModel['Remark'], $this->db->conn_id) . "', 0, 0)";
							}
						}
					}
					if ($str != '') {
						//$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
						$this->Settings_model->deleteSupplierServicesZero(id);
						//$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceType) VALUES " . $str);
						$this->Settings_model->addSupplierServices2($str);
						$msg = 'Services have been synchronized successfully!';
					}
					if ($syncIMEIS == '1' && $strServices != '') {
						//$this->db->query("DELETE FROM tbl_gf_plans_packages_prices WHERE ServiceType = '0'");
						//$this->db->query("DELETE FROM tbl_gf_users_packages_prices");
						//$this->db->query("DELETE FROM tbl_gf_user_packages WHERE ServiceType = '0'");
						$this->Settings_model->deletePlanPackagesPrices();
						$this->Settings_model->deleteUsersPackagesPrices();
						$this->Settings_model->deleteUsersPackages();						

						//$this->db->query("INSERT INTO tbl_gf_packages (CategoryId, PackageTitle, PackagePrice, MustRead, DisablePackage, sl3lbf) VALUES " . $strServices);
						$this->Settings_model->addPackages2($strServices);
						$msg = 'Services have been synchronized successfully!';
					}
				}
			} else {
				/* Parsing error */
				$msg = 'Could not parse the XML stream';
			}
		} else {
			$msg = 'Could not communicate with the api';
		}
	}
	public function sync_sl3team($id)
	{
		//$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '1'");
		$this->Settings_model->deleteSupplierServicesOne($id);
		//$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES
		$value = "('$id', '1', 'SL3 Service Sl3 Local Brute Force (SLOW)', '', '', 1)";
		$this->Settings_model->addSupplierServices($value);
		//$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES
		$value = "('$id', '2', 'SL3 Service Sl3 Local Brute Force (FAST)', '', '', 1)";
		$this->Settings_model->addSupplierServices($value);
		$msg = 'Services have been synchronized successfully!';
	}

	public function sync_ucs($id, $syncIMEIS)
	{

		require('../../../apis/unlockcodesource.com/ucs.php');
		$ucs = new UCS();
		$this->CI_unlockcodesource->setApikey($this->apiKey);
		$arrTools = $this->CI_unlockcodesource->getTools();

		if (isset($arrTools['message']) && $arrTools['code'] != '') {
			$msg = 'Connection Error:<br />' . $arrTools['message'];
		} else if (isset($arrTools[0]['toolId']) && $arrTools[0]['toolId'] != '') {
			$totalTools = count($arrTools);
			/*	echo '<pre>';
				print_r($arrTools);
				echo '</pre>';
				exit;*/
			$str = '';
			$strServices = '';
			if ($syncIMEIS == '1') {
				//$this->db->query("UPDATE tbl_gf_package_category SET ArchivedCategory = '1' WHERE SL3BF = '0'");
				$this->Settings_model->archiveServicesPackage1();
			}
			$prevCatId = 0;
			$CATEGORY_ID = 0;
			if ($syncIMEIS == '1') {
				//$this->db->query("INSERT INTO tbl_gf_package_category (Category, DisableCategory, SL3BF) VALUES ('Carrier Unlock Tools', 0, 0)");
				addServicesPackage("Carrier Unlock Tools");
				$CATEGORY_ID = $this->db->insert_id();
			}
			for ($count = 1; $count <= $totalTools; $count++) {
				if (trim($arrTools[$count]['toolName']) != '') {
					if ($str != '')
						$str .= ',';
					$str .= "('$id', '" . check_input($arrTools[$count]['toolId'], $this->db->conn_id) . "', '" . check_input($arrTools[$count]['toolName'], $this->db->conn_id) . "', 
			'" . check_input($arrTools[$count]['price'], $this->db->conn_id) . "', '" . check_input($arrTools[$count]['processTime'], $this->db->conn_id) . "', 0)";

					if ($syncIMEIS == '1') {
						if ($strServices != '')
							$strServices .= ',';
						$strServices .= "('" . check_input($CATEGORY_ID, $this->db->conn_id) . "', '" . check_input($arrTools[$count]['toolName'], $this->db->conn_id) . "', 
				'" . check_input($arrTools[$count]['price'], $this->db->conn_id) . "', '" . check_input($arrTools[$count]['processTime'], $this->db->conn_id) . "', 
				'" . check_input($arrTools[$count]['description'], $this->db->conn_id) . "', 0, 0)";
					}
					$prevCatId = $RESPONSE_ARR[$count]['CategoryId'];
				}
			}
			if ($str != '') {
				//$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
				$this->Settings_model->deleteSupplierServicesZero($id);
				//$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES $str");
				$this->Settings_model->addSupplierServices($str);
				$msg = 'Services have been synchronized successfully!';
			}
			if ($syncIMEIS == '1' && $strServices != '') {
/*				$this->db->query("DELETE FROM tbl_gf_plans_packages_prices WHERE ServiceType = '0'");
				$this->db->query("DELETE FROM tbl_gf_users_packages_prices");
				$this->db->query("DELETE FROM tbl_gf_user_packages WHERE ServiceType = '0'");
*/
				$this->Settings_model->deletePlanPackagesPrices();
				$this->Settings_model->deleteUsersPackagesPrices();
				$this->Settings_model->deleteUsersPackages();

				//$this->db->query("UPDATE tbl_gf_packages SET ArchivedPack = '1' WHERE sl3lbf = '0'");
				$this->Settings_model->archivePackages();
				//$this->db->query("INSERT INTO tbl_gf_packages (CategoryId, PackageTitle, PackagePrice, TimeTaken, MustRead, DisablePackage, sl3lbf) VALUES $strServices");
				$this->Settings_model->addPackages($strServices);
				$msg = 'Services have been synchronized successfully!';
			}
		} else {
			$msg = 'Error while connecting to Unlock Code Source API!';
		}

	}
	public function sync_appvisi0n($id)
	{
		//$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id'");
		$this->Settings_model->deleteSupplierServicesWithoutServiceType($id);
		//$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES
		$str = "('$id', '1', 'Find My iPhone', '', '', 0)";
		$this->Settings_model->addSupplierServices($str);
		$msg = 'Services have been synchronized successfully!';
	}

	public function sync_gsmkody($id)
	{

		//require('../../../apis/gsmkody.pl/api.php');
		//$objAPI = new gsmkody();
		$this->CI_gsmkody->doAction('products.json', $this->apiKey, $this->url, 'GET', 'imei');
		$arrTools = $this->CI_gsmkody->getResult();
		$strErr = $this->CI_gsmkody->checkError($arrTools);
		if (trim($strErr) != '') {
			$msg = $strErr;
		} else {
			if (isset($arrTools['status']) && $arrTools['status'] == 'ok' && $arrTools['count'] > 0) {
				$totalService = $arrTools['count'] - 1;
				$str = '';
				for ($count = 0; $count <= $totalService; $count++) {
					if (trim($arrTools['products'][$count]['name']) != '') {
						if ($str != '')
							$str .= ',';
						$str .= "('$id', '" . check_input($arrTools['products'][$count]['id'], $this->db->conn_id) . "', 
				'" . check_input($arrTools['products'][$count]['name'], $this->db->conn_id) . "', 
				'" . check_input($arrTools['products'][$count]['discount_price'], $this->db->conn_id) . "', 
				'" . check_input($arrTools['products'][$count]['delivery_time'], $this->db->conn_id) . "', 0)";
					}
				}
				if ($str != '') {
					//$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
					$this->Settings_model->deleteSupplierServicesZero($id);
					//$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES " . $str);
					$this->Settings_model->addSupplierServices($str);
					$msg = 'Services have been synchronized successfully!';
				} else
					$msg = 'Error while synchronizing the services!';
			}
		}

	}
	
	public function sync_lockpop($id)
	{

		//require('../../../apis/lockpop/lockpop_api.php');
		//$objAPI = new LPAPI();
		$this->CI_lockpop_api->doAction('Options', $this->apiKey, $this->url, array());
		//$this->CI_lockpop_api->getResult();
		$this->CI_lockpop_api->XmlToArray($this->CI_lockpop_api->getResult());
		$arrayData = $this->CI_lockpop_api->createArray();
		if (is_array($arrayData['OptionsResponse'])) {
			if (isset($arrayData['OptionsResponse']['Error']))
				$msg = $arrayData['OptionsResponse']['Error'];
			else if (isset($arrayData['OptionsResponse']['Options'][0]['OptionDescription']) && sizeof($arrayData['OptionsResponse']['Options'][0]['OptionDescription']) > 0) {
				$RESPONSE_ARR = array();
				$RESPONSE_ARR = $arrayData['OptionsResponse']['Options'][0]['OptionDescription'];
				$total = count($RESPONSE_ARR);
				$str = '';
				for ($count = 0; $count < $total; $count++) {
					if ($str != '')
						$str .= ',';
					$str .= "('$id', '" . check_input($RESPONSE_ARR[$count]['OptionId'], $this->db->conn_id) . "', '" . check_input($RESPONSE_ARR[$count]['Name'], $this->db->conn_id) . "', 
					'" . check_input($RESPONSE_ARR[$count]['Price'], $this->db->conn_id) . "', '', 0)";
				}
				if ($str != '') {
					//$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
					//$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES " . $str);
					$this->Settings_model->deleteSupplierServicesZero($id);
					$this->Settings_model->addSupplierServices($str);
					$msg = 'Services have been synchronized successfully!';
				}
			}
		} else {
			$msg = 'Error while connecting with LockPop API!';
		}

	}
	public function sync_ucocustomapi($id)
	{
		//$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id'");
		$this->Settings_model->deleteSupplierServicesWithoutServiceType($id);
		//$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES
		$str = "('$id', '1', 'Unlock Codes Online Custom', '1', '', 0)";
		$this->Settings_model->addSupplierServices($str);
		$msg = 'Services have been synchronized successfully!';
	}

	public function sync_unlockingsmart($id)
	{

		//require('../../../apis/unlockingsmart.co.uk/usmartapi.php');
		//$objAPI = new USMARTAPI();
		$this->CI_unlockingsmart->doAction('imeiservices', $this->apiKey, $this->url, $this->accountId, array());
		$this->CI_unlockingsmart->XmlToArray($this->CI_unlockingsmart->getResult());
		$arrayData = $this->CI_unlockingsmart->createArray();
		if (isset($arrayData['error']) && sizeof($arrayData['error']) > 0) {
			$msg = '<b>' . $arrayData['error'][0] . '</b>';
		} else if (isset($arrayData['Packages']['Package']) && sizeof($arrayData['Packages']['Package']) > 0) {
			$RESPONSE_ARR = array();
			if (isset($arrayData['Packages']['Package']) && sizeof($arrayData['Packages']['Package']) > 0)
				$RESPONSE_ARR = $arrayData['Packages']['Package'];
			$total = count($RESPONSE_ARR);
			$str = '';
			for ($count = 0; $count < $total; $count++) {
				if ($str != '')
					$str .= ',';
				$str .= "('$id', '" . check_input($RESPONSE_ARR[$count]['PackageId'], $this->db->conn_id) . "', '" . check_input($RESPONSE_ARR[$count]['PackageTitle'], $this->db->conn_id) . "', 
				'" . check_input($RESPONSE_ARR[$count]['PackagePrice'], $this->db->conn_id) . "', '" . check_input($RESPONSE_ARR[$count]['TimeTaken'], $this->db->conn_id) . "', 0)";
			}
			if ($str != '') {
				//$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
				$this->Settings_model->deleteSupplierServicesZero($id);
				//$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES " . $str);
				$this->Settings_model->addSupplierServices($str);
				$msg = 'Services have been synchronized successfully!';
			}
		} else {
			$msg = 'Error while connecting to unlockingsmart.co.uk';
		}

	}

	public function sync_zzcustomapi($id)
	{
		//$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id'");
		$this->Settings_model->deleteSupplierServicesWithoutServiceType($id);
		//$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES
		$str = 	"('$id', '1', 'S/N Unlocking Codes', '1', '', 0)";
		$this->Settings_model->addSupplierServices($str);
		$msg = 'Services have been synchronized successfully!';
	}

	public function sync_zzcustomapi_sl($id)
	{

		//$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id'");
		$this->Settings_model->deleteSupplierServicesWithoutServiceType($id);
		//$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES
		$str = "('$id', '1', 'S/N SimLock Data', '1', '', 0)";
		$this->Settings_model->addSupplierServices($str);
		$msg = 'Services have been synchronized successfully!';

	}
	
	public function sync_unlocksworld($id)
	{

		//require('../../../apis/unlocksworld.com/unlocksworldapi.class.php');
		//$api = new unlocksWorld();
		$arr = array("OrderId" => "", "Skip" => "0", "Take" => "20");
		$request = $this->CI_unlocksworld->action('services', $this->apiKey, $this->url, $arr);
		$arrResponse = json_decode($request, true);
		if (isset($arrResponse['Status']) && $arrResponse['Status'] == 'Success') {
			$str = '';
			foreach ($arrResponse['Data'] as $key => $value) {
				if ($str != '')
					$str .= ',';
				$str .= "('$id', '" . check_input($value['Id'], $this->db->conn_id) . "', '" . check_input($value['Name'], $this->db->conn_id) . "', 
				'" . check_input($value['Price'], $this->db->conn_id) . "', '', 0)";
			}
			if ($str != '') {
				//$this->db->query("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
				$this->Settings_model->deleteSupplierServicesZero($id);
				//$this->db->query("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES $str");
				$this->Settings_model->addSupplierServices($str);
				$msg = 'Services have been synchronized successfully!';
			} else
				$msg = 'Error while synchronizing the services!';
		} else if (isset($arrResponse['Status']) && $arrResponse['Status'] == 'Error')
			$msg = 'Connection Error: ' . $arrResponse['Message'];

	}

	public function sync_dhru_imeiservices($syncModels,$id,$syncIMEIS,$apiKey, $accountId, $url){

		$this->load->library('CI_fusionclientapi');
		$arrResponse = $this->CI_fusionclientapi->action('imeiservicelist', $apiKey, $accountId, $url);
		if (is_array($arrResponse))
		{
			if ($arrResponse['SUCCESS'][0])
			{
				$RESPONSE_ARR = array();
				if(isset($arrResponse['SUCCESS'][0]['LIST']) && is_array($arrResponse['SUCCESS'][0]['LIST']))
					$RESPONSE_ARR = $arrResponse['SUCCESS'][0]['LIST'];
				$str = '';
				$strServices = '';
				$strBrands = '';
				$strModels = '';
				if($syncIMEIS == '1')
					$this->setting_model->executeQuery("UPDATE tbl_gf_package_category SET ArchivedCategory = '1' WHERE SL3BF = '0'");
				foreach( $RESPONSE_ARR as $key => $value)
				{
					if(is_array($value['SERVICES']))
					{
						$dhruCatId = 0;
						if($syncIMEIS == '1')
						{

							$dhruCatId = $this->settings_model->fusionclientapiInsertData("INSERT INTO tbl_gf_package_category (Category, DisableCategory, SL3BF) VALUES ('".addslashes($value['GROUPNAME'])."', 0, 0)");

						}
						foreach($value['SERVICES'] as $k => $v)
						{
							if($str != '')
								$str .= ',';
							$str .= "('$id', '".addslashes($v['SERVICEID'])."', '".addslashes($v['SERVICENAME'])."', '".addslashes($v['CREDIT'])."', '".addslashes($v['TIME'])."', 0)";
							if($syncIMEIS == '1')
							{
								if($strServices != '')
									$strServices .= ',';
								$strServices .= "('$dhruCatId', '".addslashes($v['SERVICENAME'])."', '".addslashes($v['CREDIT'])."', '".addslashes($v['TIME'])."', '".addslashes($v['INFO'])."', 0, 0)";
							}
							//=============================================== SYNCING MODELS ================================================//
							if($syncModels == '1' && $v['Requires.Mobile'] == 'Required')
							{
								//if($v['SERVICEID'] != '1076')
								//{
								$paraBrands['ID'] = $v['SERVICEID']; // got from 'imeiservicelist' [SERVICEID]
								$api = new DhruFusion();
								$arrBrands = $api->action('modellist', $apiKey, $accountId, $url, $paraBrands);
								if (is_array($arrBrands))
								{
									if (isset($arrBrands['SUCCESS'][0]))
									{
										$ARR_BRANDS = array();
										if(isset($arrBrands['SUCCESS'][0]['LIST']) && is_array($arrBrands['SUCCESS'][0]['LIST']))
											$ARR_BRANDS = $arrBrands['SUCCESS'][0]['LIST'];
										foreach($ARR_BRANDS as $key => $brand)
										{
											if($brand['NAME'] != '')
											{
												if($strBrands != '')
													$strBrands .= ',';
												$strBrands .= "('$id', '".check_input($brand['ID'], $this->db->conn_id)."',
											'".check_input($brand['NAME'], $this->db->conn_id)."')";

												if(is_array($brand['MODELS']))
												{
													foreach($brand['MODELS'] as $k => $mdl)
													{
														if($strModels != '')
															$strModels .= ',';
														//											echo '<br>...................................'.$mdl['ID'];
														//										echo '<br>*************************************'.$mdl['NAME'];
														$strModels .= "('$id', '".check_input($brand['ID'], $this->db->conn_id)."',
													'".check_input($mdl['ID'], $this->db->conn_id)."', '".check_input($mdl['NAME'], $this->db->conn_id)."')";
														/*												$strModels .= "('".$id."', '".check_input($v['SERVICEID'], $objDBCD14->dbh)."',
                                                                                                        '".check_input($brand['ID'], $objDBCD14->dbh)."', '".$id."', '".$id."')";	*/
														/*
                                                                                                        $strModels .= "('".check_input($id, $objDBCD14->dbh)."', '".check_input($v['SERVICEID'], $objDBCD14->dbh)."',
                                                                                                                    '".check_input($brand['ID'], $objDBCD14->dbh)."', '".check_input($model['ID'], $objDBCD14->dbh)."',
                                                                                                                    '".check_input($model['NAME'], $objDBCD14->dbh)."')";*/
													}
												}
											}
										}
									}
								}
								//}
							}
							//=============================================== SYNCING MODELS ================================================//
						}
					}
				}
				if($str != '')
				{
					$this->settings_model->executeQuery("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
					$this->settings_model->executeQuery("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES ".$str);
					$msg = 'Services have been synchronized successfully!';
				}
				if($syncIMEIS == '1' && $strServices != '')
				{
					$this->settings_model->executeQuery("DELETE FROM tbl_gf_plans_packages_prices WHERE ServiceType = '0'");
					$this->settings_model->executeQuery("DELETE FROM tbl_gf_users_packages_prices");
					$this->settings_model->executeQuery("DELETE FROM tbl_gf_user_packages WHERE ServiceType = '0'");

					$this->settings_model->executeQuery("UPDATE tbl_gf_packages SET ArchivedPack = '1' WHERE sl3lbf = '0'");
					$this->settings_model->executeQuery("INSERT INTO tbl_gf_packages (CategoryId, PackageTitle, PackagePrice, TimeTaken, MustRead, DisablePackage, sl3lbf) VALUES ".$strServices);
					$msg = 'Services have been synchronized successfully!';
				}
				//=============================================== SYNCING BRAND AND MODELS ================================================//
				if($syncModels == '1' && $strBrands != '')
				{
					$this->settings_model->executeQuery("DELETE FROM tbl_gf_api_brands WHERE APIId = '$id'");
					$this->settings_model->executeQuery("INSERT INTO tbl_gf_api_brands (APIId, BrandId, Brand) VALUES ".$strBrands);
					$msg = 'Services and Models have been synchronized successfully!';
				}
				if($syncModels == '1' && $strModels != '')
				{
					$this->settings_model->executeQuery("DELETE FROM tbl_gf_api_models WHERE APIId = '$id'");
					$this->settings_model->executeQuery("INSERT INTO tbl_gf_api_models (APIId, BrandId, ModelId, Model) VALUES ".$strModels);
					$msg = 'Services and Models have been synchronized successfully!';
				}
				//=============================================== SYNCING BRAND AND MODELS ================================================//
			}
			else if (isset($arrResponse['ERROR'][0]))
				$msg = 'Connection Error: ' . htmlspecialchars($arrResponse['ERROR'][0]['MESSAGE']);
		}
		else
		{
			$msg = 'Connection Error: Could not parse the XML stream!';
		}
	}
	public function sync_codesk_services($id,$apiKey,$accountId, $url){
		$this->load->library('CI_codeskpro');
		$arrResponse = $this->CI_codeskpro->action('SERVICELIST', $apiKey, $accountId, $url);

		if(is_array($arrResponse))
		{
			$str = '';
			foreach($arrResponse as $key => $value)
			{
				foreach($value as $k => $v)
				{
					$time = $v['TIMEMIN']."-".$v['TIMEMAX']." ".$v['TIMEMAXTYPE'];
					if($str != '')
						$str .= ',';
					$str .= "('$id', '".check_input($v['IDPROD'], $this->db->conn_id)."', '".check_input($v['PRODNAME'], $this->db->conn_id)."',
			'".check_input($v['PRODPRICE'], $this->db->conn_id)."', '".check_input($time, $this->db->conn_id)."', 0)";
				}
			}
			if($str != '')
			{
				$this->settings_model->executeQuery("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
				$this->settings_model->executeQuery("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES ".$str);
				$msg = 'Services have been synchronized successfully!';
			}
			else
				$msg = 'Error while synchronizing the services!';
		}
	}
	public function sync_gsmf_imeiservices($syncIMEIS,$id,$apiKey,$accountId, $url){
		$objGSMFUSIONAPI = new GSMFUSIONAPI();
		$objGSMFUSIONAPI->doAction('imeiservices', $apiKey, $url, $accountId, array());
		if($objGSMFUSIONAPI->apiError() != '')
		{
			$msg = $objGSMFUSIONAPI->apiError();
		}
		else
		{
			$objGSMFUSIONAPI->XmlToArray($objGSMFUSIONAPI->getResult());
			$arrayData = $objGSMFUSIONAPI->createArray();
			if(isset($arrayData['error']) && sizeof($arrayData['error']) > 0)
			{
				$msg = '<b>'.$arrayData['error'][0].'</b>';
			}
			else
			{
				$RESPONSE_ARR = array();
				if(isset($arrayData['Packages']['Package']) && sizeof($arrayData['Packages']['Package']) > 0)
					$RESPONSE_ARR = $arrayData['Packages']['Package'];
				$total = count($RESPONSE_ARR);
				$str = '';
				$strServices = '';
				if($syncIMEIS == '1')
					$this->setiings_model->executeQuery("UPDATE tbl_gf_package_category SET ArchivedCategory = '1' WHERE SL3BF = '0'");
				$prevCatId = 0;
				$CATEGORY_ID = 0;
				for($count = 0; $count < $total; $count++)
				{
					if($syncIMEIS == '1')
					{
						if($prevCatId > 0)
						{
							if($RESPONSE_ARR[$count]['CategoryId'] != $prevCatId)
							{
								$CATEGORY_ID =$this->setiings_model->fusionclientapiInsertData("INSERT INTO tbl_gf_package_category (Category, DisableCategory, SL3BF) VALUES ('".check_input($RESPONSE_ARR[$count]['Category'], $this->db->conn_id)."', 0, 0)");
							}
						}
						else
						{
							$CATEGORY_ID =$this->setiings_model->fusionclientapiInsertData("INSERT INTO tbl_gf_package_category (Category, DisableCategory, SL3BF) VALUES
					('".check_input($RESPONSE_ARR[$count]['Category'], $this->db->conn_id)."', 0, 0)");
						}
					}
					if($str != '')
						$str .= ',';
					$str .= "('$id', '".check_input($RESPONSE_ARR[$count]['PackageId'], $this->db->conn_id)."', '".check_input($RESPONSE_ARR[$count]['PackageTitle'], $this->db->conn_id)."',
					'".check_input($RESPONSE_ARR[$count]['PackagePrice'], $this->db->conn_id)."', '".check_input($RESPONSE_ARR[$count]['TimeTaken'], $this->db->conn_id)."', 0)";
					if($syncIMEIS == '1')
					{
						if($strServices != '')
							$strServices .= ',';
						$strServices .= "('".check_input($CATEGORY_ID, $this->db->conn_id)."', '".check_input($RESPONSE_ARR[$count]['PackageTitle'], $this->db->conn_id)."',
				'".check_input($RESPONSE_ARR[$count]['PackagePrice'], $this->db->conn_id)."',
				'".check_input($RESPONSE_ARR[$count]['TimeTaken'], $this->db->conn_id)."', '".check_input($RESPONSE_ARR[$count]['MustRead'], $this->db->conn_id)."', 0, 0)";
					}
					$prevCatId = $RESPONSE_ARR[$count]['CategoryId'];
				}
				if($str != '')
				{
					$this->setiings_model->executeQuery("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
					$this->setiings_model->executeQuery("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES $str");
					$msg = 'Services have been synchronized successfully!';
				}
				if($syncIMEIS == '1' && $strServices != '')
				{
					$this->setiings_model->executeQuery("DELETE FROM tbl_gf_plans_packages_prices WHERE ServiceType = '0'");
					$this->setiings_model->executeQuery("DELETE FROM tbl_gf_users_packages_prices");
					$this->setiings_model->executeQuery("DELETE FROM tbl_gf_user_packages WHERE ServiceType = '0'");

					$this->setiings_model->executeQuery("UPDATE tbl_gf_packages SET ArchivedPack = '1' WHERE sl3lbf = '0'");
					$this->setiings_model->executeQuery("INSERT INTO tbl_gf_packages (CategoryId, PackageTitle, PackagePrice, TimeTaken, MustRead, DisablePackage, sl3lbf) VALUES ".$strServices);

					$msg = 'Services have been synchronized successfully!';
				}
			}
		}

	}
	public function sync_nksh_imeiservices($syncIMEIS,$id,$apiKey,$accountId, $url){
		$objAPI = new NAKSHAPI();
		$objAPI->doAction('imeiservices', $apiKey, $url, $accountId, array());
		$objAPI->XmlToArray($objAPI->getResult());
		$arrayData = $objAPI->createArray();
		if(isset($arrayData['error']) && sizeof($arrayData['error']) > 0)
		{
			$msg = '<b>'.$arrayData['error'][0].'</b>';
		}
		else
		{
			$RESPONSE_ARR = array();
			if(isset($arrayData['Packages']['Package']) && sizeof($arrayData['Packages']['Package']) > 0)
				$RESPONSE_ARR = $arrayData['Packages']['Package'];
			$total = count($RESPONSE_ARR);
			$str = '';
			$strServices = '';
			if($syncIMEIS == '1')
				$this->settings_model->executeQuery("UPDATE tbl_gf_package_category SET ArchivedCategory = '1' WHERE SL3BF = '0'");
			$prevCatId = 0;
			for($count = 0; $count < $total; $count++)
			{
				if($syncIMEIS == '1')
				{
					if($prevCatId > 0)
					{
						if($RESPONSE_ARR[$count]['CategoryId'] != $prevCatId)
						{
							$this->settings_model->executeQuery("INSERT INTO tbl_gf_package_category (CategoryId, Category, DisableCategory, SL3BF) VALUES ('".addslashes($RESPONSE_ARR[$count]['CategoryId'])."', '".addslashes($RESPONSE_ARR[$count]['Category'])."', 0, 0)");
						}
					}
					else
					{
						$this->settings_model->executeQuery("INSERT INTO tbl_gf_package_category (CategoryId, Category, DisableCategory, SL3BF) VALUES ('".addslashes($RESPONSE_ARR[$count]['CategoryId'])."', '".addslashes($RESPONSE_ARR[$count]['Category'])."', 0, 0)");
					}
				}
				if($str != '')
					$str .= ',';
				$str .= "('$id', '".$RESPONSE_ARR[$count]['PackageId']."', '".addslashes($RESPONSE_ARR[$count]['PackageTitle'])."', '".addslashes($RESPONSE_ARR[$count]['PackagePrice'])."',
				 '".addslashes($RESPONSE_ARR[$count]['TimeTaken'])."', 0)";
				if($syncIMEIS == '1')
				{
					if($strServices != '')
						$strServices .= ',';
					$strServices .= "('".addslashes($RESPONSE_ARR[$count]['CategoryId'])."', '".addslashes($RESPONSE_ARR[$count]['PackageTitle'])."',
				'".addslashes($RESPONSE_ARR[$count]['PackagePrice'])."', '".addslashes($RESPONSE_ARR[$count]['TimeTaken'])."',
				'".addslashes($RESPONSE_ARR[$count]['MustRead'])."', 0, 0)";
				}
				$prevCatId = $RESPONSE_ARR[$count]['CategoryId'];
			}
			if($str != '')
			{
				$this->settings_model->executeQuery("DELETE FROM tbl_gf_supplier_services WHERE APIId = '$id' AND ServiceType = '0'");
				$this->settings_model->executeQuery("INSERT INTO tbl_gf_supplier_services (APIId, ServiceId, ServiceName, ServicePrice, ServiceTime, ServiceType) VALUES ".$str);
				$msg = 'Services have been synchronized successfully!';
			}
			if($syncIMEIS == '1' && $strServices != '')
			{
				$this->settings_model->executeQuery("DELETE FROM tbl_gf_plans_packages_prices WHERE ServiceType = '0'");
				$this->settings_model->executeQuery("DELETE FROM tbl_gf_users_packages_prices");
				$this->settings_model->executeQuery("DELETE FROM tbl_gf_user_packages WHERE ServiceType = '0'");

				$this->settings_model->executeQuery("UPDATE tbl_gf_packages SET ArchivedPack = '1' WHERE sl3lbf = '0'");
				$this->settings_model->executeQuery("INSERT INTO tbl_gf_packages (CategoryId, PackageTitle, PackagePrice, TimeTaken, MustRead, DisablePackage, sl3lbf) VALUES ".$strServices);
				$msg = 'Services have been synchronized successfully!';
			}
		}
	}
	public function srvpricing($srvc,$price,$type,$pricing,$otherCurr){

		switch($srvc)
		{
			case '0';
				$tbl = 'tbl_gf_packages';
				$col = 'PackagePrice';
				$colId = 'PackageId';
				$where = ' WHERE sl3lbf = 0';
				break;
			case '1';
				$tbl = 'tbl_gf_packages';
				$col = 'PackagePrice';
				$colId = 'PackageId';
				$where = ' WHERE sl3lbf = 1';
				break;
			case '2';
				$tbl = 'tbl_gf_log_packages';
				$col = 'LogPackagePrice';
				$colId = 'LogPackageId';
				$where = ' WHERE (1) ';
				break;
		}
		if(is_numeric($price) && $price > 0)
		{
			$operator = $type == 0 ? '+' : '-';
			$setClause = '';
			if($pricing == '0')
			{
				$setClause = " $col $operator $price ";
			}
			else if($pricing == '1')
			{
				$setClause = " $col $operator ($col * $price/100) ";
			}
			$this->Setting_model->executeQuery("UPDATE $tbl SET $col = $setClause $where");
		}
		if($otherCurr == '1')
		{
			$rsCurrencies = $this->Setting_model->executeQuery("SELECT CurrencyId FROM tbl_gf_currency WHERE DisableCurrency = 0 AND DefaultCurrency = 0");

			$strCurrencies = '0';
			foreach($rsCurrencies as $row)
			{
				$strCurrencies .= ', '.$row->CurrencyId;
			}
			if($strCurrencies != '0')
			{
				if($srvc =='0' || $srvc == '1')
					$this->Setting_model->executeQuery("DELETE FROM tbl_gf_packages_currencies WHERE CurrencyId IN ($strCurrencies) AND PackageId IN (SELECT PackageId FROM tbl_gf_packages
				WHERE sl3lbf = '$srvc')");
				else
				{
					$this->Setting_model->executeQuery("DELETE FROM tbl_gf_log_packages_currencies WHERE CurrencyId IN ($strCurrencies)");
				}

				//========================================== FOR PACKS PRICES =================================================//
				$this->Setting_model->executeQuery("DELETE FROM tbl_gf_plans_packages_prices WHERE CurrencyId IN ($strCurrencies) AND ServiceType = '$srvc'");
				//========================================== CONVERT PACKS PRICES =================================================//

				//========================================== FOR USERS PRICES =================================================//
				$strUserIds = '0';
				$rsUsers = $this->Setting_model->executeQuery("SELECT UserId FROM tbl_gf_users WHERE CurrencyId IN ($strCurrencies)");
				foreach($rsUsers as $row)
				{
					$strUserIds .= ", ".$row->UserId;
				}
				if($strUserIds != '0')
				{
					if($srvc =='0' || $srvc == '1')
						$this->Setting_model->executeQuery("DELETE FROM tbl_gf_users_packages_prices WHERE UserId IN ($strUserIds)");
					else
						$this->Setting_model->executeQuery("DELETE FROM tbl_gf_users_log_packages_prices WHERE UserId IN ($strUserIds)");

				}
				//========================================== CONVERT USERS PRICES =================================================//
			}
		}

	}


	//maintenance
	public function maintenance()
	{


		if ($this->input->post_get('btnSave') ) 
		{
			echo "here";
			$text = check_input($this->input->post_get('txtText'), $this->db->conn_id, 1);
			$disable = $this->input->post_get('disable') ? 1 : 0;
			$this->Settings_model->updateMaintenanceTable($text, $disable);
			$this->data['message'] = $this->lang->line('BE_GNRL_11');
		}
		
		$rsMaintenance = $this->Settings_model->getMaintenanceTable();
		foreach ($rsMaintenance as $row)
		{
			$this->data['text'] = stripslashes($row->MaintenanceText);
			$this->data['disable'] = $row->DisableMaintenance;
		}

		$this->data['view'] = "admin/maintenance";
		$this->load->view('admin/layouts/default1', $this->data);
	}

	public function convertprices()
	{
		$this->data['rsResults'] = $this->Settings_model->getCurrencies("SELECT CurrencyId AS Id, Currency AS Value FROM tbl_gf_currency WHERE DisableCurrency = 0 ORDER BY Currency");
		$this->data['view'] = "admin/convertprices";
		$this->load->view('admin/layouts/default1', $this->data);
	}

	public function cnvrtprices()
	{

		$purpose = $this->input->post_get('purpose');

		if ($purpose == 'save') {
			$currencyId = $this->input->post_get('currencyId') ?: 0;
			$msg = '';
			$CONVERSION_RATE = 1;
			$rwCrncy = $this->Settings_model->getConversionRate("SELECT ConversionRate FROM tbl_gf_currency WHERE CurrencyId = '$currencyId'");
			if ($rwCrncy->ConversionRate != '') {
				$CONVERSION_RATE = $rwCrncy->ConversionRate;
			}

			// Prices for IMEI and File Services
			$this->Settings_model->updateImeiAndFilePrices("UPDATE tbl_gf_packages_currencies A, tbl_gf_packages B SET Price = PackagePrice * $CONVERSION_RATE WHERE A.PackageId = B.PackageId AND CurrencyId = '$currencyId'");
			// Prices for Server Services
			$this->Settings_model->updateServerPrices("UPDATE tbl_gf_log_packages_currencies A, tbl_gf_log_packages B SET Price = LogPackagePrice * $CONVERSION_RATE WHERE A.PackageId = B.LogPackageId AND CurrencyId = '$currencyId'");

			//User prices for IMEI and File Services
			$this->Settings_model->updateUserImeiAndFilePrices("UPDATE tbl_gf_users_packages_prices A, tbl_gf_packages B, tbl_gf_users C SET Price = PackagePrice * $CONVERSION_RATE WHERE A.PackageId = B.PackageId AND 
					A.UserId = C.UserId AND CurrencyId = '$currencyId'");

			//User prices for Server Services
			$this->Settings_model->updateUserServerPrices("UPDATE tbl_gf_users_log_packages_prices A, tbl_gf_log_packages B, tbl_gf_users C SET Price = LogPackagePrice * $CONVERSION_RATE WHERE A.LogPackageId = B.LogPackageId 
					AND A.UserId = C.UserId AND CurrencyId = '$currencyId'");

			// Price Plans For IMEI and File services
			$this->Settings_model->updatePlansForImeiAndFilePrices("UPDATE tbl_gf_plans_packages_prices A, tbl_gf_packages B SET Price = PackagePrice * $CONVERSION_RATE WHERE A.PackageId = B.PackageId AND 
					A.ServiceType IN (0, 1) AND CurrencyId = '$currencyId'");
			// Price Plans For Server services
			$this->Settings_model->updatePlansForServerPrices("UPDATE tbl_gf_plans_packages_prices A, tbl_gf_log_packages B SET Price = LogPackagePrice * $CONVERSION_RATE WHERE A.PackageId = B.LogPackageId AND 
					A.ServiceType = '2' AND CurrencyId = '$currencyId'");
			$msg = $this->lang->line('BE_GNRL_11');
		}

		echo $msg;

	}

	public function currencies()
	{
		$rsCurrencies = $this->Settings_model->getCurrencies('SELECT * FROM tbl_gf_currency ORDER BY Currency');
		$this->data['rsCurrencies'] = $rsCurrencies;

		$this->data['view'] = "admin/currencies";
		$this->load->view('admin/layouts/default1', $this->data);

	}

	public function currency()
	{
		$id = $this->input->post_get('id') ?: 0;
		$currency = '';
		$abbr = '';
		$symbol = '';
		$defaultCur = 0;
		$retailCurr = 0;
		$cRate = '';
		$disable = 0;
		$message = '';
		$row = $rsCurrencies = $this->Settings_model->getCurrency("SELECT * FROM tbl_gf_currency WHERE CurrencyId = '$id'");
		if ($row)
		{
			$this->data['currency'] = stripslashes($row->Currency);
			$this->data['abbr'] = stripslashes($row->CurrencyAbb);
			$this->data['symbol'] = $row->CurrencySymbol;
			$this->data['defaultCur'] = $row->DefaultCurrency;
			$this->data['cRate'] = $row->ConversionRate;
			$this->data['disable'] = $row->DisableCurrency;
			$this->data['retailCurr'] = $row->RetailCurrency;
		}
		else
		{
			$this->data['currency'] = "";
			$this->data['abbr'] = "";
			$this->data['symbol'] = "";
			$this->data['defaultCur'] = "";
			$this->data['cRate'] = "";
			$this->data['disable'] = "";
			$this->data['retailCurr'] = "";
		}

		$this->data['id'] = $id;
		$this->data['view'] = "admin/currency";
		$this->load->view('admin/layouts/default1', $this->data);

	}

	public function ajxcurrency()
	{
		$purpose = $this->input->post_get('purpose');

		if ($purpose == 'save') {
			$id = check_input($this->input->post('id'));
			$currency = check_input($this->input->post('currency'));
			$abb = check_input($this->input->post('abb'));
			$symbol = check_input($this->input->post('symbol'));
			$disableVal = check_input($this->input->post('disable'));
			$defaultCur = check_input($this->input->post('defaultCur'));
			$retailCurr = check_input($this->input->post('retailCurr'));
			$rate = check_input($this->input->post('rate'));
			$msg = '';
			$query = "SELECT CurrencyId FROM tbl_gf_currency WHERE Currency = '$currency' AND DisableCurrency = '$disableVal'";
			if ($id > 0)
				$query .= " AND CurrencyId <> " . $id;
			$row = $this->Settings_model->getRow($query);
			if (isset($row->CurrencyId) && $row->CurrencyId != '')
				$msg = "'" . $currency . "'" . $this->lang->line('BE_GNRL_10') . "~0";
			else {
				if ($rate == '')
					$rate = 0;
				if ($defaultCur == '1')
					$this->Settings_model->executeQuery("UPDATE tbl_gf_currency SET DefaultCurrency = 0");
				if ($id == 0) {
					$query = "INSERT INTO tbl_gf_currency (Currency, ConversionRate, CurrencyAbb, CurrencySymbol, DefaultCurrency, DisableCurrency, RetailCurrency) 
					VALUES ('$currency', '$rate', '$abb', '$symbol', '$defaultCur', '$disableVal', '$retailCurr')";
					$msg = $this->lang->line('BE_GNRL_11') . "~1";
				} else {
					$query = "UPDATE tbl_gf_currency SET Currency = '$currency', ConversionRate = '$rate', DisableCurrency = '$disableVal', DefaultCurrency = '$defaultCur',
					CurrencyAbb = '$abb', CurrencySymbol = '$symbol', RetailCurrency = '$retailCurr' WHERE CurrencyId = $id";
					$msg = $this->lang->line('BE_GNRL_11') . "~0";
				}
				$this->Settings_model->executeQuery($query);
			}
		} else if ($purpose == 'refreshprices') {
			$id = check_input($this->input->post('id'));
			//========================================== FOR SERVICES PRICES =================================================//
			$this->Settings_model->executeQuery("DELETE FROM tbl_gf_packages_currencies WHERE CurrencyId = '$id'");
			$this->Settings_model->executeQuery("DELETE FROM tbl_gf_log_packages_currencies WHERE CurrencyId = '$id'");
			//========================================== CONVERT SERVICES PRICES =================================================//

			//========================================== FOR PACKS PRICES =================================================//
			$this->Settings_model->executeQuery("DELETE FROM tbl_gf_plans_packages_prices WHERE CurrencyId = '$id'");
			//========================================== CONVERT PACKS PRICES =================================================//

			//========================================== FOR USERS PRICES =================================================//
			$strUserIds = '0';
			$rsUsers = $this->Settings_model->selectData("SELECT UserId FROM tbl_gf_users WHERE CurrencyId = '$id'");
			foreach ($rsUsers as $row) {
				$strUserIds .= ", " . $row->UserId;
			}
			if ($strUserIds != '0') {
				$this->Settings_model->executeQuery("DELETE FROM tbl_gf_users_packages_prices WHERE UserId IN ($strUserIds)");
				$this->Settings_model->executeQuery("DELETE FROM tbl_gf_users_log_packages_prices WHERE UserId IN ($strUserIds)");
			}
			$msg = $this->lang->line('BE_GNRL_11');
			//========================================== CONVERT USERS PRICES =================================================//

			/*
				$row = $this->db->queryUniqueObject("SELECT ConversionRate FROM tbl_gf_currency WHERE CurrencyId = '$id' AND DisableCurrency = 0 AND DefaultCurrency = 0");
				if (isset($row->ConversionRate) && $row->ConversionRate > 0)
				{
					$cRate = $row->ConversionRate;

					//========================================== CONVERT SERVICES PRICES =================================================//
					$rsPacks = $this->db->query("SELECT PackageId, PackagePrice FROM tbl_gf_retail_services WHERE ArchivedPack = '0' AND DisablePackage = 0");
					$count = $objDBCD14->numRows($rsPacks);

					while($row = $objDBCD14->fetchNextObject($rsPacks))
					{
						$PACK_PRICES[$row->PackageId] = $row->PackagePrice;
					}
					//========================================== CONVERT SERVICES PRICES =================================================//
				}*/
		}

		echo $msg;

	}

	public function allowips()
	{

		$message = '';
		$del = $this->input->post_get('del') ? 1 : 0;
		if ($del == '1' && !$this->data['IS_DEMO']) {
			$id = $this->input->post_get('id') ?: 0;
			$this->Settings_model->executeQuery("DELETE FROM tbl_gf_allowed_ips WHERE Id = '$id'");
			$message = $this->lang->line('BE_GNRL_12');
		}
		if ($this->input->post('txtIP')) {
			$ip = check_input($this->input->post('txtIP'));
			$ip = encryptThe_String($ip, 'WLIP', 10);
			$this->Settings_model->executeQuery("INSERT INTO tbl_gf_allowed_ips (IP) VALUES ('$ip')");
			$message = $this->lang->line('BE_GNRL_11');
		}
		$rsIPs = $this->Settings_model->selectData("SELECT Id, IP FROM tbl_gf_allowed_ips WHERE IPType = '0' ORDER BY Id DESC");
		$this->data['message'] = $message;
		$this->data['rsIPs'] = $rsIPs;

		$this->data['view'] = "admin/allowips";
		$this->load->view('admin/layouts/default1', $this->data);



	}
	public function regfields()
	{

		$this->data['rsFields'] = $this->Settings_model->selectData('SELECT FieldId, FieldLabel, FieldType, DisableField, Mandatory FROM tbl_gf_registration_fields ORDER BY FieldLabel');
		$this->data['view'] = "admin/regfields";
		$this->load->view('admin/layouts/default1', $this->data);
	}

	public function regfield()
	{
		$lbl = '';
		$fType = $disable = $mandatory = 0;
		$id = $this->input->post_get('id') ?: 0;
		if ($id > 0) {
			$row = $this->Settings_model->getRow("SELECT * FROM tbl_gf_registration_fields WHERE FieldId = '$id'");
			if ($row->FieldLabel != '') {
				$lbl = stripslashes($row->FieldLabel);
				$fType = $row->FieldType;
				$disable = $row->DisableField;
				$mandatory = $row->Mandatory;
			}
		}
		$this->data['id'] = $id;
		$this->data['lbl'] = $lbl;
		$this->data['fType'] = $fType;
		$this->data['disable'] = $disable;
		$this->data['mandatory'] = $mandatory;
		$this->data['view'] = "admin/regfield";
		$this->load->view('admin/layouts/default1', $this->data);

	}
	
	public function ajxregfield()
	{

		$purpose = $this->input->post('purpose');

		if ($purpose == 'save') {
			$fieldLbl = check_input($this->input->post('fieldLbl'));
			$disable = check_input($this->input->post('disable'));
			$fieldType = check_input($this->input->post('fieldType'));
			$mandatory = check_input($this->input->post('mandatory'));
			$id = check_input($this->input->post('id'));

			$msg = '';
			$query = "SELECT Count(FieldId) AS TotalRecs FROM tbl_gf_registration_fields WHERE FieldLabel = '$fieldLbl'";
			if ($id > 0)
				$query .= " AND FieldId <> '$id'";
			$row = $this->Settings_model->getRow($query);
			if ($row->TotalRecs > 0) {
				$msg = "Field Label <b>" . $fieldLbl . "</b>" . $this->lang->line('BE_GNRL_10') . "~0";
			} else {
				if ($id == 0) {
					$colName = '';
					for ($i = 0; $i < 9; $i++) {
						$colName .= chr(rand(65, 90)) . chr(rand(48, 57));
					}

					$this->Settings_model->executeQuery("INSERT INTO tbl_gf_registration_fields (FieldLabel, FieldType, FieldColName, Mandatory, DisableField) 
			VALUES ('$fieldLbl', '$fieldType', '$colName', '$mandatory', '$disable')");

					if ($fieldType == 'Text Box')
						$this->Settings_model->executeQuery("ALTER TABLE tbl_gf_users ADD $colName VARCHAR( 100 ) NULL");
					else if ($fieldType == 'Text Area')
						$this->Settings_model->executeQuery("ALTER TABLE tbl_gf_users ADD $colName TEXT NULL");
					else if ($fieldType == 'Drop Down' || $fieldType == 'Radio Button')
						$this->Settings_model->executeQuery("ALTER TABLE tbl_gf_users ADD $colName VARCHAR( 100 ) NULL");

					$msg = $this->lang->line('BE_GNRL_11') . "~1";
				} else {
					$this->Settings_model->executeQuery("UPDATE tbl_gf_registration_fields SET FieldLabel = '$fieldLbl', Mandatory = '$mandatory', DisableField = '$disable' WHERE FieldId = '$id'");
					$msg = $this->lang->line('BE_GNRL_11') . "~0";
				}
			}
		}
		echo $msg;

	}

	public function ajxregfieldval()
	{

		$purpose = $this->input->post('purpose');

		if ($purpose == 'save') {
			$val = check_input($this->input->post('val'));
			$disable = check_input($this->input->post('disable'));
			$fId = check_input($this->input->post('fId'));
			$id = check_input($this->input->post('id'));

			$msg = '';
			$query = "SELECT Count(RegValueId) AS TotalRecs FROM tbl_gf_reg_field_values WHERE RegValue = '$val' AND FieldId = '$fId'";
			if ($id > 0)
				$query .= " AND RegValueId <> '$id'";
			$row = $this->Settings_model->getRow($query);
			//echo $this->db->last_query();
			if ($row->TotalRecs > 0) {
				$msg = "Field Label <b>" . $val . "</b>" . $this->lang->line('BE_GNRL_10') . "~0";
			} else {
				if ($id == 0) {
					$this->Settings_model->executeQuery("INSERT INTO tbl_gf_reg_field_values (RegValue, FieldId, DisableRegValue) VALUES ('$val', '$fId', '$disable')");
					$msg = $this->lang->line('BE_GNRL_11') . "~1";
				} else {
					$this->Settings_model->executeQuery("UPDATE tbl_gf_reg_field_values SET RegValue = '$val', FieldId = '$fId', DisableRegValue = '$disable' WHERE RegValueId = '$id'");
					$msg = $this->lang->line('BE_GNRL_11') . "~0";
				}
			}
		}

		echo $msg;

	}
	
	public function regfieldvalues()
	{

		$id = $this->input->post_get('id') ?: 0;
		$this->data['rsValues'] = $this->Settings_model->selectData("SELECT RegValueId, FieldId, RegValue, DisableRegValue FROM tbl_gf_reg_field_values WHERE FieldId = '$id' ORDER BY RegValue");
		$this->data['id'] = $id;

		$this->data['view'] = "admin/regfieldvalues";
		$this->load->view('admin/layouts/default1', $this->data);


	}
	
	public function regfieldvalue()
	{
		$id = $this->input->post_get('id') ?: 0;
		$fId = $this->input->post_get('fId') ?: 0;
		$val = "";
		$disable = 0;
		if ($id > 0) {
			$row = $this->Settings_model->getRow("SELECT * FROM tbl_gf_reg_field_values WHERE RegValueId = '$id'");
			if ($row->RegValue != '') {
				$val = stripslashes($row->RegValue);
				$disable = $row->DisableRegValue;
				$fId = $row->FieldId;
			}
		}
		$this->data['id'] = $id;
		$this->data['val'] = $val;
		$this->data['disable'] = $disable;
		$this->data['fId'] = $fId;

		$this->data['view'] = "admin/regfieldvalue";
		$this->load->view('admin/layouts/default1', $this->data);


	}

	public function settings()
	{

		$this->data['ADD_CREDITS_DEFAULT_VAL'] = 0;
		$this->data['AUTO_CREDITS'] = 0;
		$row = $this->Settings_model->getRow('SELECT * FROM tbl_gf_email_settings WHERE Id = 1');
		if (isset($row->Company) && $row->Company != '') {
			$this->data['fromName'] = stripslashes($row->FromName);
			$this->data['fromEmail'] = $row->FromEmail;
			$this->data['address'] = $row->Address;
			$this->data['phone'] = $row->Phone;
			$this->data['retail'] = $row->Retail;
			$this->data['company'] = stripslashes($row->Company);
			$this->data['url'] = $row->SiteURL;
			$this->data['chkSm'] = $row->CheckSumIMEI;
			$this->data['payTo'] = $row->PayTo;
			$this->data['customDesign'] = $row->CustomDesign;
			$this->data['ffPwdChDays'] = $row->ForcefullPwdChangeDays;
			$this->data['theme'] = $row->Theme;
			$this->data['faqs'] = $row->FAQs;
			$this->data['lang'] = $row->LocalLanguage;
			$this->data['sendSMS'] = $row->SendSMS;
			$this->data['imeiFType'] = $row->IMEIFieldType;
			$this->data['newClients'] = $row->AllowNewUserToLogIn;
			//$this->data['ipCheckAtLogin'] = $row->CountryIPCheckAtLogin;
			$this->data['loginAttempts'] = $row->LoginAttempts;
			$this->data['imeiDD'] = $row->IMEIOrderDropDownType;
			$this->data['fileDD'] = $row->FileOrderDropDownType;
			$this->data['serverDD'] = $row->ServerOrderDropDownType;
			$this->data['autoCredits'] = $row->AutoCredits;
			$this->data['ipRestriction'] = $row->IPRestriction;
			$this->data['minCredits'] = $row->MinCredits;
			$this->data['maxCredits'] = $row->MaxCredits;
			$this->data['SHOW_IMEI_SERVICES'] = $row->IMEIServices;
			$this->data['SHOW_FILE_SERVICES'] = $row->FileServices;
			$this->data['SHOW_SERVER_SERVICES'] = $row->ServerServices;
			$this->data['pinCode'] = $row->PinCode;
//	$this->data['canBuyCredits'] = $row->CanBuyCredits;
			$this->data['sendNL'] = $row->SendNLViaCron;
			$this->data['SHOW_PRICES'] = $row->ShowPricesAtWeb;
			$this->data['SHOW_EPRICES'] = $row->ShowEPricesAtWeb;
			$this->data['IMEI_ORDER_EMAIL'] = $row->SendNewIMEIOrderEmail;
			$this->data['FILE_ORDER_EMAIL'] = $row->SendNewFileOrderEmail;
			$this->data['SRVR_ORDER_EMAIL'] = $row->SendNewServerOrderEmail;
			$this->data['IMEI_SUCCESS_ORDER_EMAIL'] = $row->SendSuccessIMEIOrderEmail;
			$this->data['FILE_SUCCESS_ORDER_EMAIL'] = $row->SendSuccessFileOrderEmail;
			$this->data['SRVR_SUCCESS_ORDER_EMAIL'] = $row->SendSuccessSrvrOrderEmail;
			$this->data['IMEI_FAILURE_ORDER_EMAIL'] = $row->SendFailureIMEIOrderEmail;
			$this->data['FILE_FAILURE_ORDER_EMAIL'] = $row->SendFailureFileOrderEmail;
			$this->data['SRVR_FAILURE_ORDER_EMAIL'] = $row->SendFailureSrvrOrderEmail;
			$this->data['knowledgeBase'] = $row->KnowledgeBase;
			$this->data['ticketSys'] = $row->TicketSystem;
			$this->data['videos'] = $row->Videos;
			$this->data['themeStyle'] = $row->ThemeStyle;
			$this->data['geniePay'] = $row->GeniePay;
			$this->data['ppTrnsfrFee'] = $row->PPTransferFee;
			$this->data['crdtsTrnsfrFee'] = $row->CreditsTransferFee;
			$this->data['smtp'] = $row->SMTP;
			$this->data['smtpAuth'] = $row->SMTPAuth;
			$this->data['smtpHost'] = stripslashes($row->SMTPHost);
			$this->data['smtpPort'] = stripslashes($row->SMTPPort);
			$this->data['smtpUN'] = stripslashes($row->SMTPUN);
			$this->data['smtpPwd'] = stripslashes($row->SMTPPwd);
			$this->data['priceEmails'] = $row->ServicePriceEmails;
			$this->data['homePage'] = $row->HomePage;
			$this->data['toEmail'] = stripslashes($row->ToEmail);
			$this->data['signature'] = stripslashes($row->EmailSignature);
			$this->data['gCaptchaSiteKey'] = $row->GoogleCaptchaSiteKey;
			$this->data['gCaptchaSecretKey'] = $row->GoogleCaptchaSecretKey;
			$this->data['adminCaptcha'] = $row->AdminLoginCaptcha;
			$this->data['userCaptcha'] = $row->ClientLoginCaptcha;
			$this->data['rgstrCaptcha'] = $row->RegisterCaptcha;
			$this->data['cntctCaptcha'] = $row->ContactCaptcha;
			$this->data['gTransU'] = $row->GTranslatorU;
			$this->data['gTransS'] = $row->GTranslatorS;
			$this->data['rushPmnt'] = $row->RushPaymentNotification;
			$this->data['invsNotify'] = $row->AdminInvoiceNotification;
			$this->data['apiIPRstrct'] = $row->RestrictAPIIP;
			$this->data['aTitle'] = stripslashes($row->AdminTitle);
			$this->data['sTitle'] = stripslashes($row->SiteTitle);
			$this->data['shopSTitle'] = stripslashes($row->ShopSiteTitle);
			$this->data['copyRights'] = stripslashes($row->Copyrights);
			$this->data['contactEmail'] = stripslashes($row->ContactUsEmail);
			$arr = explode('|', $row->TimeDifference);
			if (is_array($arr)) {
				if (isset($arr[0]))
					$this->data['dtTmDiffType'] = $arr[0];
				if (isset($arr[1]))
					$this->data['dtTmDiffHours'] = $arr[1];
				if (isset($arr[2]))
					$this->data['dtTmDiffMins'] = $arr[2];
			}
		}
		$rsUsrs = $this->Settings_model->selectData("DESCRIBE tbl_gf_users");
		foreach($rsUsrs as $rw) {
			if ($rw->Field == 'AutoFillCredits') {
				$this->data['AUTO_CREDITS'] = $rw->Default;
			}
			if ($rw->Field == 'CanAddCredits') {
				$this->data['ADD_CREDITS_DEFAULT_VAL'] = $rw->Default;
			}
		}

		$this->data['settings_tab_general'] = $this->load->view ('admin/settings_tab_general', $this->data, TRUE);
		$this->data['settings_tab_services'] = $this->load->view ('admin/settings_tab_services', $this->data, TRUE);
		$this->data['settings_tab_user'] = $this->load->view ('admin/settings_tab_user', $this->data, TRUE);
		$this->data['settings_tab_layout'] = $this->load->view ('admin/settings_tab_layout', $this->data, TRUE);
		$this->data['settings_tab_nl'] = $this->load->view ('admin/settings_tab_nl', $this->data, TRUE);
		$this->data['settings_tab_srvc_emails'] = $this->load->view ('admin/settings_tab_srvc_emails', $this->data, TRUE);
		$this->data['settings_tab_ip'] = $this->load->view ('admin/settings_tab_ip', $this->data, TRUE);
		$this->data['settings_tab_sms'] = $this->load->view ('admin/settings_tab_sms', $this->data, TRUE);
		$this->data['settings_tab_smtp'] = $this->load->view ('admin/settings_tab_smtp', $this->data, TRUE);
		$this->data['settings_tab_geniepay'] = $this->load->view ('admin/settings_tab_geniepay', $this->data, TRUE);

		$this->data['view'] = "admin/settings";
		$this->load->view('admin/layouts/default1', $this->data);

	}
	public function ajxemailsettings()
	{

		
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
			$purpose = $this->input->post('purpose');

			if ($purpose == 'save') {
				$fName = check_input($this->input->post('fName'));
				$fEmail = check_input($this->input->post('fEmail'));
				$tEmail = check_input($this->input->post('tEmail'));
				$cuEmail = check_input($this->input->post('cuEmail'));
				$company = check_input($this->input->post('company'));
				$url = check_input($this->input->post('url'));
				$dtTmDiffType = check_input($this->input->post('dtTmDiffType'));
				$dtTmDiffHours = check_input($this->input->post('dtTmDiffHours'));
				$dtTmDiffMins = check_input($this->input->post('dtTmDiffMins'));
				$payTo = check_input($this->input->post('payTo'));
				$chkSm = check_input($this->input->post('chkSm'));
				$customDesign = check_input($this->input->post('customDsgn'));
				$theme = check_input($this->input->post('theme'));
				$design = 0;
				$txtDir = 0;//check_input($this->input->post('txtDir'));
				$design = 0;//check_input($this->input->post('design'));
				$imeiFType = check_input($this->input->post('imeiFType'));
				$loginAttempts = check_input($this->input->post('loginAttempts'));
				$imeiDDType = check_input($this->input->post('imeiDDType'));
				$fileDDType = check_input($this->input->post('fileDDType'));
				$serverDDType = check_input($this->input->post('serverDDType'));
				$imeiServices = check_input($this->input->post('imeiServices'));
				$fileServices = check_input($this->input->post('fileServices'));
				$serverServices = check_input($this->input->post('serverServices'));
				$minCredits = check_input($this->input->post('minCredits'));
				$maxCredits = check_input($this->input->post('maxCredits'));
				$mPlanId = 0;//check_input($this->input->post('mPlanId'));
				$frmDt = '';//check_input($this->input->post('dtFrm'));
				$toDt = '';//check_input($this->input->post('dtTo'));
				$lang = check_input($this->input->post('lang'));
				$phone = check_input($this->input->post('phone'));
				$gCSiteKey = check_input($this->input->post('gCSiteKey'));
				$gCSecKey = check_input($this->input->post('gCSecKey'));
				$address = check_input($this->input->post('address'));
				$newClients = check_input($this->input->post('newClients'));
				$adminCaptcha = check_input($this->input->post('adminCaptcha'));
				$userCaptcha = check_input($this->input->post('userCaptcha'));
				$pinCode = check_input($this->input->post('pinCode'));
				$showPrices = check_input($this->input->post('showPrices'));
				$showEPrices = check_input($this->input->post('showEPrices'));
				$rushPmnt = check_input($this->input->post('rushPmnt'));
				$ffPwdChDays = check_input($this->input->post('ffPwdChDays'));
				$invsNotify = check_input($this->input->post('invsNotify'));
				$imeiOrdrEmail = check_input($this->input->post('imeiOrdrEmail'));
				$fileOrdrEmail = check_input($this->input->post('fileOrdrEmail'));
				$srvrOrdrEmail = check_input($this->input->post('srvrOrdrEmail'));
				$sendNL = check_input($this->input->post('sendNL'));
				$imeiScsOrdrEmail = check_input($this->input->post('imeiScsOrdrEmail'));
				$fileScsOrdrEmail = check_input($this->input->post('fileScsOrdrEmail'));
				$srvrScsOrdrEmail = check_input($this->input->post('srvrScsOrdrEmail'));
				$imeiRejOrdrEmail = check_input($this->input->post('imeiRejOrdrEmail'));
				$fileRejOrdrEmail = check_input($this->input->post('fileRejOrdrEmail'));
				$srvrRejOrdrEmail = check_input($this->input->post('srvrRejOrdrEmail'));
				$knowledgeBase = check_input($this->input->post('knowledgeBase'));
				$ticketSystem = check_input($this->input->post('ticketSystem'));
				$faqs = check_input($this->input->post('faqs'));
				$retail = check_input($this->input->post('retail'));
				$videos = check_input($this->input->post('videos'));
				$homePage = check_input($this->input->post('homePage'));
				$signature = check_input($this->input->post('signature'));
				$crdtsTrnsfrFee = $this->input->post('crdtsTrnsfrFee') ?: '0';
				$sendEmails = $this->input->post('sendEmails') ?: '1';
				$autoCredits = check_input($this->input->post('autoCredits'));
				$canBuyCr = check_input($this->input->post('canBuyCr'));
				$gTransU = check_input($this->input->post('gTransU'));
				$gTransS = check_input($this->input->post('gTransS'));
				$rgstrCaptcha = check_input($this->input->post('rgstrCaptcha'));
				$cntctCaptcha = check_input($this->input->post('cntctCaptcha'));
				$aTitle = check_input($this->input->post('aTitle'));
				$sTitle = check_input($this->input->post('sTitle'));
				$shTitle = check_input($this->input->post('shTitle'));
				$copyRights = check_input($this->input->post('copyRights'));

				$strTheme = ", Theme = '$theme'";
				if ($customDesign == '1')
					$strTheme = '';

				$timeDiff = $dtTmDiffType . '|' . $dtTmDiffHours . '|' . $dtTmDiffMins;
				$this->Settings_model->executeQuery("UPDATE tbl_gf_email_settings SET Company = '$company', FromName = '$fName', FromEmail = '$fEmail', ToEmail = '$tEmail', SiteURL = '$url', 
				LocalLanguage = '$lang', TimeDifference = '$timeDiff', CheckSumIMEI = '$chkSm', ShowEPricesAtWeb = '$showEPrices', AutoAdminArchived = '0', 
				Design = '$design', TextDirection = '$txtDir', IMEIFieldType = '$imeiFType', Phone = '$phone', Retail = '$retail', GoogleCaptchaSiteKey = '$gCSiteKey', 
				Address = '$address', PinCode = '$pinCode', IMEIOrderDropDownType = '$imeiDDType', FAQs = '$faqs', GoogleCaptchaSecretKey = '$gCSecKey', 
				FileOrderDropDownType = '$fileDDType', ServerOrderDropDownType = '$serverDDType', AllowNewUserToLogIn = '$newClients', CreditsTransferFee = '$crdtsTrnsfrFee', 
				LoginAttempts = '$loginAttempts', IMEIServices = '$imeiServices', FileServices = '$fileServices', ServicePriceEmails = '$sendEmails', 
				ServerServices = '$serverServices', MinCredits = '$minCredits', MaxCredits = '$maxCredits', AdminLoginCaptcha = '$adminCaptcha', 
				ShowPricesAtWeb = '$showPrices', ForcefullPwdChangeDays = '$ffPwdChDays', SendNewIMEIOrderEmail = '$imeiOrdrEmail', SendNLViaCron = '$sendNL', 
				SendNewFileOrderEmail = '$fileOrdrEmail', SendNewServerOrderEmail = '$srvrOrdrEmail', SendSuccessIMEIOrderEmail = '$imeiScsOrdrEmail', GTranslatorU = '$gTransU',
				SendSuccessFileOrderEmail = '$fileScsOrdrEmail', SendSuccessSrvrOrderEmail = '$srvrScsOrdrEmail', SendFailureIMEIOrderEmail = '$imeiRejOrdrEmail',
				SendFailureFileOrderEmail = '$fileRejOrdrEmail', SendFailureSrvrOrderEmail = '$srvrRejOrdrEmail', Videos = '$videos', PayTo = '$payTo', GTranslatorS = '$gTransS', 
				KnowledgeBase = '$knowledgeBase', TicketSystem = '$ticketSystem', HomePage = '$homePage', EmailSignature = '$signature', ClientLoginCaptcha = '$userCaptcha', 
				RushPaymentNotification = '$rushPmnt', AdminInvoiceNotification = '$invsNotify', RegisterCaptcha = '$rgstrCaptcha', ContactCaptcha = '$cntctCaptcha', 
				AdminTitle = '$aTitle', SiteTitle = '$sTitle', ShopSiteTitle = '$shTitle', Copyrights = '$copyRights', ContactUsEmail = '$cuEmail' $strTheme WHERE Id = '1'");

				$this->Settings_model->executeQuery("ALTER TABLE `tbl_gf_users` CHANGE `AutoFillCredits` `AutoFillCredits` TINYINT(1) NULL DEFAULT '$autoCredits'");
				$this->Settings_model->executeQuery("ALTER TABLE `tbl_gf_users` CHANGE `CanAddCredits` `CanAddCredits` TINYINT(1) NULL DEFAULT '$canBuyCr'");

				$_SESSION['LANGUAGE_BE'] = $lang;
				$msg = $this->lang->line('BE_GNRL_11') . "!~0";
			} else if ($purpose == 'saveIPRes') {
				$ipRestriction = $this->input->post('ipRestriction') ?: '0';
				$apiIPRstrct = $this->input->post('apiIPRstrct') ?: '0';
				$this->Settings_model->executeQuery("UPDATE tbl_gf_email_settings SET IPRestriction = '$ipRestriction', RestrictAPIIP = '$apiIPRstrct' WHERE Id = '1'");
				$this->Settings_model->executeQuery("UPDATE tbl_gf_users SET CanLoginFrmOtherCntry = '$ipRestriction'");
				$msg = $this->lang->line('BE_GNRL_11'). "!~0";
			} else if ($purpose == 'saveSMS') {
				$sendSMS = $this->input->post('sendSMS') ?: '0';
				$this->Settings_model->executeQuery("UPDATE tbl_gf_email_settings SET SendSMS = '$sendSMS' WHERE Id = '1'");
				$this->Settings_model->executeQuery("UPDATE tbl_gf_users SET SendSMS = '$sendSMS'");
				$this->Settings_model->executeQuery("UPDATE tbl_gf_retail_services SET SendSMS = '$sendSMS'");
				$msg = $this->lang->line('BE_GNRL_11') . "!~0";
			} else if ($purpose == 'saveGP') {
				$ppTrnsfrFee = $this->input->post('ppTrnsfrFee') ?: '0';
				$this->Settings_model->executeQuery("UPDATE tbl_gf_email_settings SET PPTransferFee = '$ppTrnsfrFee' WHERE Id = '1'");
				$msg = $this->lang->line('BE_GNRL_11') . "!~0";
			} else if ($purpose == 'saveSMTP') {
				$smtp = $this->input->post('smtp') ?: '0';
				$smtpAuth = $this->input->post('smtpAuth') ?: '0';
				$smtpHost = $this->input->post('smtpHost') ?: '0';
				$smtpPort = $this->input->post('smtpPort') ?: '0';
				$smtpUN = $this->input->post('smtpUN') ?: '0';
				$smtpPwd = $this->input->post('smtpPwd') ?: '0';

				$this->Settings_model->executeQuery("UPDATE tbl_gf_email_settings SET SMTP = '$smtp', SMTPAuth = '$smtpAuth', SMTPHost = '$smtpHost', SMTPPort = '$smtpPort', SMTPUN = '$smtpUN', 
					SMTPPwd = '$smtpPwd' WHERE Id = '1'");
				$msg = $this->lang->line('BE_GNRL_11') . "!~0";
			}

			echo $msg;
		} else {
			echo 'Direct access not allowed.';
		}

	}

	public function cpaneltheme()
	{

		$pr = $this->input->post_get('pr') ?: 0;
		$headerData = '';
		$contentData = '';
		$footerData = '';
		$allData = '';
		$message = '';
		if ($this->input->post('txtHdr7') && isset($_SESSION['GSM_FSN_AdmId'])) {
			for ($i = 1; $i <= 10; $i++) {
				$headerData .= check_input($this->input->post('txtHdr' . $i));
				if ($i != '10')
					$headerData .= '|';
			}
			for ($i = 1; $i <= 16; $i++) {
				$contentData .= check_input($this->input->post('txtCnt' . $i));
				if ($i != '16')
					$contentData .= '|';
			}
			for ($i = 1; $i <= 4; $i++) {
				$footerData .= check_input($this->input->post('txtFtr' . $i));
				if ($i != '4')
					$footerData .= '|';
			}
			$allData = $headerData . "\r\n" . $contentData . "\r\n" . $footerData;
			if ($allData != '') {
				$fh = fopen(APPPATH . "../assets/css_pr/style.txt", 'w') or die("can't open file");
				fwrite($fh, $allData);
				fclose($fh);
			}
			$message = $this->lang->line('BE_GNRL_11');
		}
		if ($pr == '1') {

			$defaultCSSData = file_get_contents(APPPATH . "../assets/css_pr/defaultstyle.txt", "r");
			$fh = fopen(APPPATH . "../assets/css_pr/style.txt", 'w') or die("can't open file");
			fwrite($fh, $defaultCSSData);
			fclose($fh);
			$message = $this->lang->line('BE_GNRL_11');
		}
		$file = fopen(APPPATH . "../assets/css_pr/style.txt", "r");
		$cntr = 0;
		$arrColors = array();
		$clrsHdr = array();
		$clrsCnts = array();
		$clrsFtr = array();
		while (!feof($file)) {
			$arrColors[$cntr] = fgets($file);
			$cntr++;
		}
		fclose($file);
		if (isset($arrColors[0]) && $arrColors[0] != '')
			$clrsHdr = explode('|', $arrColors[0]);
		if (isset($arrColors[1]) && $arrColors[1] != '')
			$clrsCnts = explode('|', $arrColors[1]);
		if (isset($arrColors[2]) && $arrColors[2] != '')
			$clrsFtr = explode('|', $arrColors[2]);

		$this->data['clrsHdr'] = $clrsHdr;
		$this->data['clrsCnts'] = $clrsCnts;
		$this->data['clrsFtr'] = $clrsFtr;
		$this->data['pr'] = $pr;
		$this->data['message'] = $message;

		$this->data['view'] = "admin/cpaneltheme";
		$this->load->view('admin/layouts/default1', $this->data);

	}
}
