<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends MY_Controller{
     
	public function __construct() {
		parent::__construct();
    }

    public function newimeiorders(){
        $message = '' ;
        if($this->input->post('btnReprocessing'))
        {
            $arrIds = $this->input->post_get('chkOrders') ?: 0;
            if(is_array($arrIds))
            {
                $orderIds = implode(',', $arrIds);
                $update_data = array(
                    'HiddenStatus' => 0 ,
                    'MessageFromServer' => '' ,
                );
               
                $where_data = array(
                    'HiddenStatus' => 1
                );
                $where_in = array(
                    'codeId' => $orderIds
                );
                $this->Services_model->update_gf_codes($update_data , $where_data , $where_in);
                
                $message = 'IMEI Orders have been marked to be resubmitted successfully!';
            }
        }
        if($this->input->post_get('rp') && $this->input->post_get('rp') == '1')
        {
            $pkId = $this->input->post_get('pkId') ?: 0;
            $update_data = array(
                'HiddenStatus' => 0,
                'MessageFromServer' => '' ,     
            );
            $where_data = array(
                'PackageId' => $pkId ,
                'CodeStatusId' => 1 ,
                'OrderAPIId >' => 0,
                'CodeSentToOtherServer' => 0 ,
                'HiddenStatus' => 1
            );
            $this->Services_model->update_gf_codes($update_data , $where_data);
          
            $message = 'IMEI Service orders have been marked to be resubmitted successfully!';
        }

        $strWhere = '';
        $dt = $this->input->post_get('dt') ?: '';
        if($dt != '')
        {
            $strWhere = " AND DATE(RequestedAt) >= '$dt'";
        }
        $this->data['rsNewOrders'] =   $this->Services_model->fetch_packages_codes_by_date($strWhere);
    

        $ARR_API_ERRORS = array();
        $rsAPIErrorOrders =  $this->Services_model->fetch_codes_by_date($strWhere);
      
        foreach($rsAPIErrorOrders as  $row)
        {
            if($row->MessageFromServer != '')
                $ARR_API_ERRORS[$row->PackageId] = stripslashes($row->MessageFromServer);

        }

        $this->data['dt'] = $dt;
        $this->data['ARR_API_ERRORS'] = $ARR_API_ERRORS;
        $this->data['message'] = $message ;
        $this->data['view'] = 'admin/new_imei_orders';
        $this->load->view('admin/layouts/default1' , $this->data);
    }


    public function codes(){
        $message = '';
        $errorMsg = '';
        $strWhere = '';
        $imei = '';
        $packageId = 0;
        $codeStatusId = 0;
        $txtlqry = '';
        $sqlwhere = '';
        $uName = '';
        $this->data['sc'] = 0;
        $cldFrmBulk = '0';
        $blkReplyType = 0;
        $IMEIS_ARR = array();
        $page_name = $this->input->server('PHP_SELF');
        $SELECTED_IDS = '0';
        $STATUS_ID = 0;
        $orderNo = '';
        $arcIds = '0';
        $ORDER_IDS = '0';
        $LINKED_IMEIS = '';
        
        if(!isset($start))
            $start = 0;
        if($this->input->post_get("start"))
            $start = $this->input->post_get("start");
        $eu = ($start - 0);
        $this->data['bulk'] = '1';
        $searchType = $this->input->post_get('searchType') ?: 0;
        $uId = $this->input->post_get('uId') ?: 0;
        $packId = $this->input->post_get('packId') ?: 0;
        $this->data['applyAPI'] = $this->input->post_get('applyAPI') ?: 0;
        $dtFrom = $this->input->post_get('txtFromDt') ?: '';
        $dtTo = $this->input->post_get('txtToDt') ?: '';
        $planId = $this->input->post_get('planId') ?: 0;
        $orderBy = $this->input->post_get('oB') ?: 'DESC';
        $this->data['pageType'] = $this->input->post_get('pT') ?: '0';

        if($planId != '0')
        {
            $strWhere .= " AND PricePlanId = '$planId'";
            $txtlqry .= "&planId=$planId";
        }
        if($uId != '0')
        {
            $strWhere .= " AND A.UserId = '$uId'";
            $txtlqry .= "&uId=$uId";
        }
        if($searchType != '0')
        {
            $txtlqry .= "&searchType=$searchType";
        }
        if($this->input->post('txtMultiIMEIs') && $this->input->post('txtMultiIMEIs') != '')
        {
            $rearrange_val = $this->input->post('txtMultiIMEIs');
            $replySep = $this->input->post('replySep');
            $this->data['blkReplyType'] = $this->input->post('replyType');
            $new_val = explode("\n", $rearrange_val);
            $strIMEIs = '0';
            foreach ($new_val as $key => $val)
            {
                if(trim($val) != '')
                {
                    $val = trim(strip_tags($val));
                    $arrVal = explode($replySep, $val);
                    if(sizeof($arrVal) == '2')
                    {
                        $IMEI_INDEX = trim($arrVal[0]);
                        $IMEIS_ARR[$IMEI_INDEX] = $arrVal[1];
                    }
                    else if(sizeof($arrVal) > 2)
                    {
                        $strBulkVal = '';
                        $IMEI_INDEX = trim($arrVal[0]);
                        foreach ($arrVal as $arrKey => $arrVal)
                        {
                            if($arrKey != 0)
                            {
                                if($strBulkVal == '')
                                    $strBulkVal = $arrVal;
                                else
                                    $strBulkVal .= $replySep.$arrVal;
                            }
                        }
                        $IMEIS_ARR[$IMEI_INDEX] = $strBulkVal;
                    }
                    else if(sizeof($arrVal) == 1)
                    {
                        $IMEI_INDEX = trim($arrVal[0]);
                        $IMEIS_ARR[$IMEI_INDEX] = $replySep;
                    }
                    if($strIMEIs == '0')
                        $strIMEIs = "'".$IMEI_INDEX."'";
                    else
                        $strIMEIs .= ",'".$IMEI_INDEX."'";
                }
            }

            $strWhere .= " AND IMEINo IN (".$strIMEIs.") AND A.CodeStatusId = '4'";
            $cldFrmBulk = '1';
            $limit = sizeof($new_val);

        }
        else{
            $limit = $this->input->post_get('records') && is_numeric($this->input->post_get('records')) ? $this->input->post_get('records') : 100;
        }
        
        $txtlqry .= "&records=$limit";
        $thisp = $eu + $limit;
        $back = $eu - $limit;
        $next = $eu + $limit;
        $cldFrm = $this->input->post_get('cldFrm') ? $this->input->post_get('cldFrm') : 0;
        if($this->input->post_get('hdsbmt') && $this->session->userdata('GSM_FSN_AdmId'))
        {
            $codeIds = $this->input->post_get('chkCodes') ?: 0;
            $totalCodes = 0;
            if(is_array($codeIds))
                $totalCodes = count($codeIds);
            if($searchType == '3')
            {
                $codeIds_reply = $this->input->post_get('chkReply') ?: 0;
                $totalCodes_reply = 0;
                if(is_array($codeIds_reply))
                    $totalCodes_reply = count($codeIds_reply);
            }
            if($cldFrm == 1 || $cldFrm == 4)
            {
                $arcIds = '0';
                for($i = 0; $i < $totalCodes; $i++)
                {
                    $data = explode('|', $codeIds[$i]);
                    $arcIds .= ','.$data[0];
                }
                if($cldFrm == 1){
                    $update_data = array(
                        'LastUpdatedBy' => $this->session->userdata('AdminUserName'),
                        'AdminArchived' => 1,
                        'CheckDuplication' => 0 ,
                    );

                    $where_in_data= array(
                        'CodeId' => $arcIds
                    );

                    $this->Services_model->update_gf_codes($update_data , '' , $where_in_data);
                }
                else if($cldFrm == 4)
                {
                    // LINK IMEIs
                    for($i = 0; $i < $totalCodes; $i++)
                    {
                        $data = explode('|', $codeIds[$i]);
                        $ORDER_ID = $data[0];
                        $IMEI = $data[3];
                        $SERVICE_ID = $data[5];
                        $ORDER_TYPE = $data[6];
                        if($ORDER_TYPE == '0')
                        {
                            $rwCode =  $this->Services_model->fetch_codes_by_statusId_packageId($SERVICE_ID , $IMEI);
                           
                            if(isset($rwCode->CodeId) && $rwCode->CodeId != '')
                            {
                                $update_data = array(
                                    'LastUpdatedBy' => $this->session->userdata('AdminUserName'),
                                    'CodeStatusId' => 4 ,
                                    'LinkedOrderId' => $rwCode->CodeId,
                                );
                                $where_data = array(
                                    'CodeId' => $ORDER_ID
                                );
                               
                                $this->Services_model->update_gf_codes($update_data , $where_data);

                                if($LINKED_IMEIS == '')
                                    $LINKED_IMEIS = $IMEI;
                                else
                                    $LINKED_IMEIS .= ','.$IMEI;
                            }
                            else
                            {
                                $update_data = array(
                                    'LastUpdatedBy' => $this->session->userdata('AdminUserName'),
                                    'CodeStatusId' => 4 ,
                                );
                                $where_data = array(
                                    'CodeId' => $ORDER_ID
                                );
                                $this->Services_model->update_gf_codes($update_data , $where_data);
                                $ORDER_IDS .= ','.$ORDER_ID;
                            }
                        }
                        else
                        {
                            $update_data = array(
                                'CodeStatusId' => 4 ,
                            );
                            $where_data = array(
                                'CodeId' => $ORDER_ID
                            ); 
                            
                            $this->Services_model->update_gf_codes($update_data , $where_data);
                            $ORDER_IDS .= ','.$ORDER_ID;
                        }
                    }
                }
                $message = $this->lang->line('BE_GNRL_11');
            }
            else if($cldFrm == 2 || $cldFrm == 3 || $cldFrm == 5 || $cldFrm == 6)
            {
              
                //========================================== GET SERVICES NAMES ================================================//
                $packIds = '0';
                $ARR_SRVC_NAME = array();
                for($i = 0; $i < $totalCodes; $i++)
                {
                    $data = explode('|', $codeIds[$i]);
                    $packIds .= ','.$data[5];
                    $rsSrcvs = $this->Services_model->fetch_packages_by_ids($packIds);
                   
                    foreach($rsSrcvs as $row)
                    {
                        $ARR_SRVC_NAME[$row->PackageId] = $row->PackageTitle;
                    }
                }
                //========================================== GET SERVICES NAMES ================================================//	
                for($i=0; $i < $totalCodes; $i++)
                {
                    $updateStatus = true;
                    $data = explode('|', $codeIds[$i]);
                    $codeId = $data[0];
                    if($this->input->post('txtBulkCodeVal') && $_POST['txtBulkCodeVal'] != '')
                        $code = $this->input->post('txtBulkCodeVal');
                    else
                        $code = $this->input->post('txtCode'.$data[4]);
                    
                    if($cldFrm == '2')
                        $codeStatusId = $this->input->post_get('codeStatusId'.$data[4]);
                    else if($cldFrm == '3')
                        $codeStatusId = $this->input->post_get('blkCodeStatusId');
                    else if($cldFrm == '5')
                        $codeStatusId = '3';
                    else if($cldFrm == '6')
                    {
                        if($codeIds_reply != '0')
                        {
                            if(in_array($codeIds[$i], $codeIds_reply))
                                $codeStatusId = '3';
                            else
                                $updateStatus = false;
                        }
                        else
                            $updateStatus = false;
                    }
                    if($updateStatus)
                    {
                        // REJECTION
                        $message = updateorderstatus($data ,$ARR_SRVC_NAME , $codeId , $code);
                        $SELECTED_IDS .= ', '.$codeId;
                    }
                }
                if($searchType == '3')
                {
                    //========================================== GET SERVICES NAMES ================================================//
                    $packIds = '0';
                    $ARR_SRVC_NAME = array();
                    for($i = 0; $i < $totalCodes_reply; $i++)
                    {
                        $data = explode('|', $codeIds_reply[$i]);
                        $packIds .= ','.$data[5];
                        $rsSrcvs = $this->Services_model->fetch_packages_by_ids($packIds);
                        foreach($rsSrcvs as $row)
                        {
                            $ARR_SRVC_NAME[$row->PackageId] = $row->PackageTitle;
                        }
                    }
                    //========================================== GET SERVICES NAMES ================================================//	
                    for($i=0; $i < $totalCodes_reply; $i++)
                    {
                        $data = explode('|', $codeIds_reply[$i]);
                        $codeId = $data[0];
                        if($this->input->post('txtBulkCodeVal') && $this->input->post('txtBulkCodeVal') != '')
                            $code = $this->input->post('txtBulkCodeVal');
                        else
                            $code =  $this->input->post('txtCode'.$data[4]);
                        
                            $codeStatusId = '2';
                        $updateStatus = true;
                        if($codeIds != '0')
                        {
                            if(in_array($codeIds_reply[$i], $codeIds))
                                $updateStatus = false;
                        }
                        if($updateStatus)
                        {
                            // SUCCESS
                            $message = updateorderstatus($data ,$ARR_SRVC_NAME , $codeId , $code);
                            $SELECTED_IDS .= ', '.$codeId;
                        }
                    }
                }
                if($SELECTED_IDS != '0' && $STATUS_ID == '2' || $STATUS_ID == '3')
                {
                    $this->Services_model->delete_order_emails($SELECTED_IDS , $STATUS_ID);
                    $this->Services_model->insert_order_emails_data($SELECTED_IDS ,$STATUS_ID);
                    
                }
            }
            else if($cldFrm == '7')
            {
                include APPPATH.'scripts/applyapi.php';	
            }
        }
        if($this->input->post_get('txtOrderNo'))
        {
            $orderNo = trim($this->input->post('txtOrderNo'));
            if($orderNo != '')
            {
                $strWhere .= " AND CodeId = '$orderNo'";
                $txtlqry .= "&txtOrderNo=$orderNo";
            }
        }
        if($this->input->post_get('chkLnkdOrders'))
        {
            $strWhere .= " AND LinkedOrderId > 0";
            $txtlqry .= "&chkLnkdOrders=1";
        }
        
        if($packId != '0' && $packId != '')
        {
            $strWhere .= " AND A.PackageId = '$packId'";
            $txtlqry .= "&packId=$packId";
        }
        if($this->input->post('hdnOrdrIds') && $this->input->post('hdnOrdrIds') != '')
        {
            $orderIds = $this->input->post('hdnOrdrIds'); 
            $strWhere .= " AND A.CodeId IN ($orderIds)";
            $txtlqry .= "&hdnOrdrIds=$orderIds";
        }
        
        if($this->input->post_get('txtIMEI'))
        {
            $imei = $this->input->post_get('txtIMEI') ? trim($this->input->post_get('txtIMEI')) : '';
            if($imei != '')
            {
                $strWhere .= " AND IMEINo LIKE '%$imei%'";
                $txtlqry .= "&txtIMEI=$imei";
            }
        }
        if($this->input->post_get('txtUName') && $this->input->post_get('txtUName') != '')
        {
            $uName = $this->input->post_get('txtUName') ? trim($this->input->post_get('txtUName')) : '';
            if($uName != '')
            {
                $strWhere .= " AND UserName LIKE '%".$uName."%'";
                $txtlqry .= "&txtUName=$uName";
            }
        }
        if($this->input->post_get('txtFromDt'))
        {
            if($dtFrom != '' && $dtTo != '')
                $strWhere .= " And DATE(RequestedAt) >= '" . $dtFrom . "' AND DATE(RequestedAt) <= '" . $dtTo . "'";
            else
            {
                if($dtFrom != '')
                    $strWhere .= " AND DATE(RequestedAt) >= '" . $dtFrom . "'";
                if($dtTo!= '')
                    $strWhere .= " AND DATE(RequestedAt) <= '" . $dtTo . "'";
            }
        }
        if($this->input->post_get('packageId'))
        {
            $packageId = $this->input->post_get('packageId') ?: 0;
            if($packageId != '0')
            {
                $strWhere .= " AND A.PackageId = '$packageId'";
                $txtlqry .= "&packageId=$packageId";
            }
        }
        if($this->input->post_get('codeStatusId'))
        {
            $codeStatusId = $this->input->post_get('codeStatusId') ?: 0;
            if($codeStatusId != '0')
            {
                $strWhere .= " AND A.CodeStatusId = '".$codeStatusId."'";
                $txtlqry .= "&codeStatusId=$codeStatusId";
            }
        }
        
        if($this->input->post_get('txtBulkIMEIs') && $this->input->post_get('txtBulkIMEIs') != '')
        {
            $arrIMEIS = explode("\n", $this->input->post("txtBulkIMEIs"));
            $imeis = '';
            for($i=0; $i<sizeof($arrIMEIS); $i++)
            {
                if($imeis == '')
                    $imeis .= "'".substr(trim($arrIMEIS[$i]), 0, 15)."'";
                else
                {
                    if(trim($arrIMEIS[$i]) != '')
                        $imeis .= ",'".substr(trim($arrIMEIS[$i]), 0, 15)."'";
                }
            }
            if($imeis != '0')
                $strWhere .= " AND IMEINo IN (".$imeis.")";
            $txtlqry .= "&blkimeis=$imeis";
        }
        if($this->input->post_get('blkimeis') && $this->input->post_get('blkimeis') != '')
        {
            $strWhere .= " AND IMEINo IN (".$imeis.")";
        }
        
        $this->data['rsCodes'] = $this->Services_model->fetch_codes_packages_users_currency_data($strWhere ,$orderBy , $eu, $limit); 
        $this->data['count'] = count($this->data['rsCodes']);
        $this->data['strPckWhere'] = ' AND Cat.SL3BF = 0 AND A.sl3lbf = 0';
        $this->data['strWhere'] = $strWhere;
        $this->data['cldFrmBulk'] = $cldFrmBulk;
        $this->data['strPackIds'] = '';
        $this->data['cldFrm'] = $cldFrm;
        $this->data['arcIds'] = $arcIds;
        $this->data['start'] = $start ;
        $this->data['packageId'] = $packageId;
        $this->data['codeStatusId'] = $codeStatusId;
        $this->data['uName'] = $uName;
        $this->data['orderNo'] = $orderNo;
        $this->data['dtTo'] = $dtTo;
        $this->data['dtFrom'] = $dtFrom;
        $this->data['packId'] = $packId;
        $this->data['orderBy'] = $orderBy;
        $this->data['searchType'] = $searchType;
        $this->data['planId'] = $planId;
        $this->data['limit'] = $limit;
        $this->data['ORDER_IDS'] = $ORDER_IDS;
        $this->data['LINKED_IMEIS'] = $LINKED_IMEIS;
        $this->data['message'] = $message;
        $this->data['errorMsg'] = $errorMsg;
        $this->data['uId'] = $uId;
        $this->data['view'] = 'admin/codes' ; 
        $this->load->view('admin/layouts/default1' , $this->data);

    }


    public function multilinebulkreply(){
    
        $message = '';
        $strAndPack = '';
        $replySep = '';
        $packageId = '';
        $file = '' ;
        $upldType = 0;
        $cldFrm = '';
        $this->data['strPckWhere'] = ' AND Cat.SL3BF = 0 AND A.sl3lbf = 0';
        if($this->input->post('packageId') && $this->input->post('GSM_FSN_AdmId'))
        {
            $packageId = $this->input->post('packageId') ?: 0;
            if($packageId > 0)
                $strAndPack = "	AND PackageId = '$packageId'";
            $bulkData = $this->input->post('txtData') ?: '';
            $replyType = $this->input->post('rdReplyType') ?: '0';
            $replySep = $this->input->post('txtReplySep') ?: '';
            if(Trim($replySep) == '')
                $replySep = ' ';
            $cldFrm = $this->input->post_get('cldFrm') ? $this->input->post_get('cldFrm') : 0;
            if($cldFrm == '0')
            {
               
                require(APPPATH.'libraries/FileManager.php');
                $filemanager = new FileManager();
                
                $fileExt = $filemanager->getFileExtension("txtFile");
                if ($filemanager->getFileName('txtFile') != '')
                    $file =	FCPATH.'samplefiles/bulkdata'.$filemanager->getFileExtension("txtFile");
                if ($filemanager->getFileName('txtFile') != '')
                {
                    $filemanager->uploadAs('txtFile', $file);
                    $content = file_get_contents($file);
                    $rearrange_val = nl2br($content);
                }
                else if ($bulkData != '')
                {
                    $upldType = 1;
                    $content = $bulkData;
                    $rearrange_val = $content;
                }
                $message = $this->lang->line('BE_GNRL_11');
            }

            if ($rearrange_val != '')
            {
                echo "	<html><body>'".form_open(base_url('admin/services/codes') , 'name="frmOrder" id="frmOrder"')."'
                        <input type='hidden' name='txtMultiIMEIs' value='".$rearrange_val."'>
                        <input type='hidden' name='searchType' value='3'>
                        <input type='hidden' name='bulk' value='1'>
                        <input type='hidden' name='packId' value='".$packId."'>
                        <input type='hidden' name='replyType' value='".$replyType."'>
                        <input type='hidden' name='replySep' value='".$replySep."'>";
                echo form_close()."</body></html><script language='javascript' type='text/javascript'>document.getElementById('frmOrder').submit();</script>";
                exit;
            }
            
        }
        $this->data['replySep'] = $replySep;
        $this->data['strPackIds'] ='';
        $this->data['packageId'] = $packageId;
        $this->data['file'] = $file;
        $this->data['upldType'] = $upldType;
        $this->data['strRejData'] = '' ;
        $this->data['strData'] = '';
        $this->data['cldFrm'] = $cldFrm;
        $this->data['message'] = $message ;
        $this->data['view'] = 'admin/multilinebulkreply';
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function verifyorders(){
        $strWhere = '';
        $imei = '';
        $packageId = 0;
        $codeStatusId = 0;
        $searchType = 0;
        $txtlqry = '';
        $sqlwhere = '';
        $verifyPage = '1';
        $tblOrders = 'tbl_gf_codes';
        $this->data['page_name'] = $this->input->server('PHP_SELF');
        $searchType = $this->input->post_get('searchType') ?: '0';
        $uId = $this->input->post_get('uId') ?: 0;
        if($uId != '0')
        {
            $strWhere .= " AND A.UserId = '$uId'";
            $txtlqry .= "&uId=$uId";
        }
        if(!isset($start))
            $start = 0;

        if($this->input->post_get("start"))
            $start = $this->input->post_get("start");

        $eu = ($start - 0);
        $limit = $this->input->post_get('records') ?: 50;
        $this->data['thisp'] = $eu + $limit;
        $this->data['back'] = $eu - $limit;
        $this->data['next'] = $eu + $limit;
        $message = '';
        if($this->input->post_get('hdsbmt') && $this->session->userdata('GSM_FSN_AdmId'))
        {
            $codeIds = $this->input->post_get('chkCodes') ?: 0;
            $totalCodes = 0;
            if(is_array($codeIds))
                $totalCodes = count($codeIds);

            $codeIds_reply = $this->input->post_get('chkReply') ?: 0;
            $totalCodes_reply = 0;
            if(is_array($codeIds_reply))
                $totalCodes_reply = count($codeIds_reply);
            if($totalCodes_reply == 0)
                $message = $this->lang->line('BE_LBL_577');
            else
            {
                for($i=0; $i < $totalCodes; $i++)
                {
                    $updateStatus = true;
                    $data = explode('|', $codeIds[$i]);
                    $codeId = $data[0];
                    $code = $this->input->post_get('txtCode'.$data[4]);
                    if($codeIds_reply != '0')
                    {
                        if(in_array($codeIds[$i], $codeIds_reply))
                            $codeStatusId = '3';
                        else
                            $updateStatus = false;
                    }
                    else
                        $updateStatus = false;
                    if($updateStatus)
                    {
                        $message = updateorderstatus($data, $codeStatusId , $codeId , $code);
                        $update_data = array(
                            'LastUpdatedBy' => $this->session->userdata('AdminUserName'),
                            'Verify' => 2,
                        );

                        $where_data = array(
                            'CodeId' => $codeId,
                        );

                        $this->Services_model->update_gf_codes($update_data , $where_data);  
                    }
                }
                for($i=0; $i < $totalCodes_reply; $i++)
                {
                    $data = explode('|', $codeIds_reply[$i]);
                    $codeId = $data[0];
                    $code = $this->input->post_get('txtCode'.$data[4]);
                    $codeStatusId = '2';
                    $updateStatus = true;
                    if($codeIds != '0')
                    {
                        if(in_array($codeIds_reply[$i], $codeIds))
                            $updateStatus = false;
                    }
                    if($updateStatus)
                    {
                        $message = updateorderstatus($data, $codeStatusId , $codeId , $code);
                        $update_data = array(
                            'LastUpdatedBy' => $this->session->userdata('AdminUserName'),
                            'Verify' => 2,
                        );

                        $where_data = array(
                            'CodeId' => $codeId,
                        );

                        $this->Services_model->update_gf_codes($update_data , $where_data);
                    }
                }
                $message = $this->lang->line('BE_GNRL_11');
            }
        }
        if($this->input->post_get('txtIMEI'))
        {
            $imei = trim($this->input->post_get('txtIMEI'));
            $packageId = $this->input->post_get('packageId');
            $codeStatusId = $this->input->post_get('codeStatusId');
            $uName = $this->input->post_get('txtUName') ? trim($this->input->post_get('txtUName')) : '';
            if($uName != '')
            {
                $strWhere = " AND UserName LIKE '%".$uName."%'";
                $txtlqry .= "&txtUName=$uName";
            }
            if($imei != '')
            {
                $strWhere = " AND IMEINo LIKE '%".$imei."%'";
                $txtlqry .= "&txtIMEI=$imei";
            }
            if($packageId != '0')
            {
                $strWhere .= " AND A.PackageId = '$packageId'";
                $txtlqry .= "&packageId=$packageId";
            }
            if($codeStatusId != '0')
            {
                $strWhere .= " AND A.CodeStatusId = '$codeStatusId'";
                $txtlqry .= "&codeStatusId=$codeStatusId";
            }
        }

        $this->data['rsCodes']=$this->Services_model->fetch_codes_packages_users_data($strWhere ,$eu, $limit);
        $this->data['count'] = count($this->data['rsCodes']);
        $this->data['packageId'] = $packageId;
        $this->data['codeStatusId'] = $codeStatusId;
        $this->data['orderNo'] = '' ;
        $this->data['strWhere'] = $strWhere;
        $this->data['strPckWhere'] = '' ;
        $this->data['uId'] = $uId;
        $this->data['txtlqry'] = $txtlqry;
        $this->data['limit'] = $limit;
        $this->data['eu'] = $eu;
        $this->data['message'] = $message ;
        $this->data['view'] = 'admin/verifyorders';
        $this->load->view('admin/layouts/default1' , $this->data);
    }


    public function codesslbf(){
        $strWhere = '';
        $imei = '';
        $packageId = 0;
        $serviceType = '1';
        $codeStatusId = 0;
        $txtlqry = '';
        $sqlwhere = '';
        $uName = '' ;
        $arcIds = '' ;
        $orderNo = '' ;
        $lastAPIId = '' ;
        $ORDER_IDS = '' ;
        $LINKED_IMEIS = '' ;
        $sc = 1;
        $page_name = $this->input->server('PHP_SELF');
        $strPckWhere = ' AND Cat.SL3BF = 1 AND A.sl3lbf = 1';
        $searchType = $this->input->post_get('searchType') ?: 0;
        $uId = $this->input->post_get('uId') ?: 0;
        $planId = $this->input->post_get('planId') ?: 0;
        $dtFrom = $this->input->post_get('txtFromDt') ?: '';
        $dtTo = $this->input->post_get('txtToDt') ?: '';
        $applyAPI = $this->input->post_get('applyAPI') ?: 0;
        $packId = $this->input->post_get('packId') ?: 0;
        if($planId != '0')
        {
            $strWhere .= " AND PricePlanId = '$planId'";
            $txtlqry .= "&planId=$planId";
        }
        if($uId != '0')
        {
            $strWhere .= " AND A.UserId = '$uId'";
            $txtlqry .= "&uId=$uId";
        }
        if($searchType != '0')
        {
            $txtlqry .= "&searchType=$searchType";
        }

        if(!isset($start))
            $start = 0;
        if($this->input->post_get("start"))
            $start = $this->input->post_get("start");

        $eu = ($start - 0);
        $limit = $this->input->post_get('records') ?: 100;
        $thisp = $eu + $limit;
        $back = $eu - $limit;
        $next = $eu + $limit;
        $cldFrm = $this->input->post_get('cldFrm') ?: 0;
        $message = '';
        if($this->input->post_get('hdsbmt') && $this->session->userdata('GSM_FSN_AdmId'))
        {
            $codeIds = $this->input->post_get('chkCodes') ?: 0;
            $totalCodes = 0;
            if(is_array($codeIds))
                $totalCodes = count($codeIds);
            if($searchType == '3')
            {
                $codeIds_reply = $this->input->post_get('chkReply') ?: 0;
                $totalCodes_reply = 0;
                if(is_array($codeIds_reply))
                    $totalCodes_reply = count($codeIds_reply);
            }
            if($cldFrm == 1 || $cldFrm == 4)
            {
                $arcIds = '0';
                for($i=0; $i<$totalCodes; $i++)
                {
                    $data = explode('|', $codeIds[$i]);
                    $arcIds .= ','.$data[0];
                }
                if($cldFrm == 1){
                    $update_data = array(
                        'LastUpdatedBy' => $this->session->userdata('AdminUserName'),
                        'AdminArchived' => 1 ,
                        'CheckDuplication' =>  0 

                    );

                    $where_in = array(
                        'CodeId' => $arcIds
                    );
                    $this->Services_model->update_slbf_codes($update_data , ''  , $where_in);

                }
                else if($cldFrm == 4)
                {
                    // LINK IMEIs
                    $ORDER_IDS = '0';
                    $LINKED_IMEIS = '';
                    for($i = 0; $i < $totalCodes; $i++)
                    {
                        $data = explode('|', $codeIds[$i]);
                        $ORDER_ID =$data[0];
                        $IMEI = $data[3];
                        $SERVICE_ID = $data[5];
                        $ORDER_TYPE = $data[7];
                        if($ORDER_TYPE == '0')
                        {
                            $rwCode =  $this->Services_model->fetch_codes_slbf_by_statusId_packageId($IMEI , $SERVICE_ID); 
                            
                            if(isset($rwCode->CodeId) && $rwCode->CodeId != '')
                            {

                                $update_data = array(
                                    'LastUpdatedBy' => $this->session->userdata('AdminUserName'),
                                    'CodeStatusId' => 4,
                                    'LinkedOrderId' =>  $rwCode->CodeId
                                );
            
                                $where_data = array(
                                    'CodeId' => $ORDER_ID
                                );
                                $this->Services_model->update_slbf_codes($update_data , $where_data);

                                if($LINKED_IMEIS == '')
                                    $LINKED_IMEIS = $IMEI;
                                else
                                    $LINKED_IMEIS .= ','.$IMEI;
                            }
                            else
                            {
                                $update_data = array(
                                    'LastUpdatedBy' => $this->session->userdata('AdminUserName'),
                                    'CodeStatusId' => 4,
                                );
            
                                $where_data = array(
                                    'CodeId' => $ORDER_ID
                                );
                                $this->Services_model->update_slbf_codes($update_data , $where_data);
                                $ORDER_IDS .= ','.$ORDER_ID;
                            }
                        }
                        else
                        {
                            $update_data = array(
                                'LastUpdatedBy' => $this->session->userdata('AdminUserName'),
                                'CodeStatusId' => 4,
                            );
        
                            $where_data = array(
                                'CodeId' => $ORDER_ID
                            );
                            $this->Services_model->update_slbf_codes($update_data , $where_data);
                         
                            $ORDER_IDS .= ','.$ORDER_ID;
                        }
                    }
                }
                $message = $this->lang->line('BE_GNRL_11');
            }
            else if($cldFrm == 2 || $cldFrm == 3 || $cldFrm == 5 || $cldFrm == 6)
            {
                for($i=0; $i<$totalCodes; $i++)
                {
                    $updateStatus = true;
                    $data = explode('|', $codeIds[$i]);
                    $codeId = $data[0];
                    if($this->input->post('txtBulkCodeVal') && $this->input->post('txtBulkCodeVal') != '')
                        $code = $this->input->post('txtBulkCodeVal');
                    $serviceTitle = $data[6];
                    if($cldFrm == '2')
                        $codeStatusId = $this->input->post_get('codeStatusId'.$data[4]);
                    else if($cldFrm == '3')
                        $codeStatusId = $this->input->post_get('blkCodeStatusId');
                    else if($cldFrm == '5')
                        $codeStatusId = '3';
                    else if($cldFrm == '6')
                    {
                        if($codeIds_reply != '0')
                        {
                            if(in_array($codeIds[$i], $codeIds_reply))
                                $codeStatusId = '3';
                            else
                                $updateStatus = false;
                        }
                        else
                            $updateStatus = false;
                    }
                    if($updateStatus)
                    {
                        $message = updatefileorderstatus($data , $codeId , $codeStatusId, $code);
                    }
                }
                if($searchType == '3')
                {
                    for($i=0; $i < $totalCodes_reply; $i++)
                    {
                        $data = explode('|', $codeIds_reply[$i]);
                        $codeId = $data[0];
                        if($this->input->post('txtBulkCodeVal') && $this->input->post('txtBulkCodeVal') != '')
                            $code = $this->input->post('txtBulkCodeVal');
                        $serviceTitle = $data[6];
                        $codeStatusId = '2';
                        $updateStatus = true;
                        if($codeIds != '0')
                        {
                            if(in_array($codeIds_reply[$i], $codeIds))
                                $updateStatus = false;
                        }
                        if($updateStatus)
                        {
                            $message = updatefileorderstatus($data , $codeId , $codeStatusId, $code);
                        }
                    }
                }
            }
            else if($cldFrm == '7')
            {
                include 'scripts/applyapi.php';	
            }
        }
        if($packId != '0' && $packId != '')
        {
            $strWhere .= " AND A.PackageId = '$packId'";
            $txtlqry .= "&packId=$packId";
        }
        if($this->input->post_get('txtOrderNo'))
        {
            $orderNo = trim($this->input->post_get('txtOrderNo'));
            if($orderNo != '')
            {
                $strWhere .= " AND CodeId = '$orderNo'";
                $txtlqry .= "&txtOrderNo=$orderNo";
            }
        }
        if($this->input->post_get('txtIMEI'))
        {
            $imei = trim($this->input->post_get('txtIMEI'));
            if($imei != '')
            {
                $strWhere .= " AND IMEINo LIKE '%".$imei."%'";
                $txtlqry .= "&txtIMEI=$imei";
            }
        }
        if($this->input->post_get('txtBulkIMEIs') && $this->input->post_get('txtBulkIMEIs') <> '')
        {
            $arrIMEIS = explode("\n", $this->input->post("txtBulkIMEIs"));
            $imeis = '';
            for($i=0; $i<sizeof($arrIMEIS); $i++)
            {
                if($imeis == '')
                    $imeis .= "'".substr(trim($arrIMEIS[$i]), 0, 15)."'";
                else
                {
                    if(trim($arrIMEIS[$i]) != '')
                        $imeis .= ",'".substr(trim($arrIMEIS[$i]), 0, 15)."'";
                }
            }
            if($imeis != '0')
                $strWhere .= " AND IMEINo IN (".$imeis.")";
            $txtlqry .= "&blkimeis=$imeis";
        }
        if($this->input->post_get('blkimeis') && $this->input->post_get('blkimeis') != '')
        {
            $strWhere .= " AND IMEINo IN (".$imeis.")";
        }
        if($this->input->post_get('hdnOrdrIds') && $this->input->post_get('hdnOrdrIds') != '')
        {
            $orderIds = $this->input->post_get('hdnOrdrIds'); 
            $strWhere .= " AND A.CodeId IN ($orderIds)";
            $txtlqry .= "&hdnOrdrIds=$orderIds";
        }
        if($this->input->post_get('packageId'))
        {
            $packageId = $this->input->post_get('packageId');
            if($packageId != '0')
            {
                $strWhere .= " AND A.PackageId = '$packageId'";
                $txtlqry .= "&packageId=$packageId";
            }
        }
        if($this->input->post_get('txtUName'))
        {
            $uName = $this->input->post_get('txtUName') ? trim($this->input->post_get('txtUName')) : '';
            if($uName != '')
            {
                $strWhere .= " AND UserName LIKE '%".$uName."%'";
                $txtlqry .= "&txtUName=$uName";
            }
        }
        if($this->input->post_get('codeStatusId'))
        {
            $codeStatusId = $this->input->post_get('codeStatusId');
            if($codeStatusId != '0')
            {
                $strWhere .= " AND A.CodeStatusId = '$codeStatusId'";
                $txtlqry .= "&codeStatusId=$codeStatusId";
            }
        }
        if($this->input->post_get('txtFromDt'))
        {
            if($dtFrom != '' && $dtTo != '')
                $strWhere .= " And DATE(RequestedAt) >= '" . $dtFrom . "' AND DATE(RequestedAt) <= '" . $dtTo . "'";
            else
            {
                if($dtFrom != '')
                    $strWhere .= " AND DATE(RequestedAt) >= '" . $dtFrom . "'";
                if($dtTo!= '')
                    $strWhere .= " AND DATE(RequestedAt) <= '" . $dtTo . "'";
            }
        }
        $this->data['rsCodes'] =  $this->Services_model->fetch_codes_slbf_packages_user($strWhere ,$eu, $limit);
        $this->data['count'] = count($this->data['rsCodes']);

        $this->data['message'] = $message ;
        $this->data['uName'] = $uName;
        $this->data['arcIds'] = $arcIds;
        $this->data['searchType'] = $searchType;
        $this->data['limit'] = $limit;
        $this->data['planId'] = $planId;
        $this->data['applyAPI'] = $applyAPI;
        $this->data['packId'] = $packId;
        $this->data['sc'] = $sc;
        $this->data['dtFrom'] = $dtFrom;
        $this->data['dtTo'] = $dtTo;
        $this->data['orderNo'] = $orderNo;
        $this->data['strWhere'] = $strWhere;
        $this->data['txtlqry'] = $txtlqry;
        $this->data['pLast'] = '';
        $this->data['thisp'] = $thisp;
        $this->data['back'] = $back;
        $this->data['start'] = $start;
        $this->data['packageId'] =$packageId;
        $this->data['codeStatusId'] = $codeStatusId;
        $this->data['lastAPIId'] = $lastAPIId;
        $this->data['ORDER_IDS'] = $ORDER_IDS;
        $this->data['LINKED_IMEIS'] = $LINKED_IMEIS;
        $this->data['uId'] = $uId;
        $this->data['view'] = 'admin/codesslbf' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function codeslbf(){
		$id = ($this->input->post_get('id')) ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
		$imeiNo = '';
		$pckTitle = '';
		$userId = '';
		$userName = '';
		$file = '';
		$hash = '';
		$hash = '';
		$codeStatusId = 0;
		$credits = '';
		$notes = '';
		$PHONE_ADMIN = '';
		$orderIdFrmServer = '';
		$SMS_Pack = '';
		$plan = '';
		$packageId = 0;
		$currDtTm = 0;
		$message = '';
		$comments = '';
		if ($this->input->post('txtCode'))
		{
			$code = check_input($this->input->post('txtCode'), $this->db->conn_id);
			$notes = check_input($this->input->post('txtNotes'), $this->db->conn_id);
			$comments = check_input($this->input->post('txtComments'), $this->db->conn_id);
			$codeStatusId = check_input($this->input->post('codeStatusId'), $this->db->conn_id);
			$userId = check_input($this->input->post('userId'), $this->db->conn_id);
			$codeCr = check_input($this->input->post('codeCr'), $this->db->conn_id);
			$packageId = check_input($this->input->post('packageId'), $this->db->conn_id);
			$hash = check_input($this->input->post('txtHash'), $this->db->conn_id);
			$imei = check_input($this->input->post('txtIMEI'), $this->db->conn_id);
			$myIMEI = ($this->input->post('imei')) ? check_input($this->input->post('imei'), $this->db->conn_id) : 0;
			$pckTitle = check_input($this->input->post('pckTitle'), $this->db->conn_id);
			$strDeduct = '';
			if($codeStatusId == '3')
			{
				if(fileOrderRefunded($id) == '0')
				{
					$dec_points = refundFileCredits($userId, $id, $myIMEI, $pckTitle, $currDtTm, $packageId, $codeCr, '1', $objDBCD14);
					$strDeduct = ", Refunded = '1', CheckDuplication = 0";
				}
			}
			else
			{
				if(fileOrderRefunded($id) == '1')
				{
					rebateFileCredits($userId, $id, $myIMEI, $pckTitle, $currDtTm, $packageId, $codeCr, '1');
				}
				$strDeduct = ", Refunded = '0'";
			}

			$file = '';
			require(APPPATH.'libraries/FileManager.php');
			$filemanager = new FileManager();
			$strAND = '';
			if ($filemanager->getFileName('txtFile') != '')
			{
				$fileExt = $filemanager->getFileExtension("txtFile");
				$existingFile = check_input($_POST['existingFile'], $this->db->conn_id);
				if($existingFile != '')
					@unlink('../'.$existingFile);
				$file =	'sl3_reply/'.$filemanager->getFileName("txtFile");
				$filemanager->uploadAs('txtFile', '../'.$file);
				$strAND = ", ReplyFile = '$file' ";
			}
			$this->db->query("UPDATE tbl_gf_codes_slbf SET HashValue = '$hash', IMEINo = '$imei', CodeStatusId = '$codeStatusId', Code = '$code', 
	ReplyDtTm = '$currDtTm', Comments = '$notes', Comments1 = '$comments', LastUpdatedBy = '".$_SESSION['AdminUserName']."' $strAND $strDeduct WHERE CodeId = '$id'");
			$query = "SELECT CodeFile, UserName AS CustomerName, AlternateEmail, Code, UserEmail, CodeStatus, A.UserId, A.Comments, RequestedAt, B.SendSMS AS SMS_User, B.Phone FROM tbl_gf_codes_slbf A, tbl_gf_users B, tbl_gf_code_status C WHERE A.UserId = B.UserId AND A.CodeStatusId = C.CodeStatusId AND CodeId='$id'";
			$row = $this->Services_model->fetch_codes_by_id($query,$id);

			if (isset($row->UserEmail) && $row->UserEmail != '')
			{
				send_push_notification('Your File order has been completed!', $row->UserId);
				if($codeStatusId == '2')
				{
					successfulFileOrderEmail($row->UserEmail, stripslashes($row->CustomerName), $_POST["pckTitle"], $_POST['imei'], stripslashes($row->Code),$row->RequestedAt, $packageId, $row->AlternateEmail, $codeCr, $row->UserId, $row->Comments, $id);
					if($row->SMS_User == '1' && $_POST["SMS_Pack"] == '1' && $row->Phone!= '' && $PHONE_ADMIN != '')
					{
						checkAndSendSMS($row->Phone, $PHONE_ADMIN, '8', stripslashes($row->CustomerName), $_POST["pckTitle"], $_POST['imei'], stripslashes($row->Code),$row->RequestedAt, $row->Comments);
					}
				}
				else if($codeStatusId == '3')
				{
					send_push_notification('Your File order has been cancelled!', $row->UserId);
					rejectedFileOrderEmail($row->UserEmail, stripslashes($row->CustomerName), $_POST["pckTitle"], $_POST['imei'], stripslashes($row->Code), $row->RequestedAt, $packageId, $row->AlternateEmail, $codeCr, $dec_points, $row->Comments, $id);
					if($row->SMS_User == '1' && $_POST["SMS_Pack"] == '1' && $row->Phone!= '' && $PHONE_ADMIN != '')
					{
						checkAndSendSMS($row->Phone, $PHONE_ADMIN, '14', stripslashes($row->CustomerName), $_POST["pckTitle"], $_POST['imei'],
							stripslashes($row->Code), $row->RequestedAt, $row->Comments);
					}
				}
			}
			$message = $this->lang->line('BE_GNRL_11');
		}
		if($id > 0)
		{
			$query =  "SELECT CodeFile, HashValue, IMEINo, A.UserId, Code, CodeStatusId, A.Credits, A.Comments, A.Comments1, A.PackageId, PackageTitle, 
				ReplyDtTm, RequestedAt, B.SendSMS, OrderIdFromServer, MessageFromServer, CodeSentToOtherServer, OrderAPIName, LastUpdatedBy, ReplyFile, UserName 
				FROM tbl_gf_codes_slbf A, tbl_gf_packages B, tbl_gf_users D WHERE A.PackageId = B.PackageId AND A.UserId = D.UserId AND CodeId = '$id'";
			$row = $this->Services_model->fetch_codes_by_id($query,$id);
			//var_dump($row);die;
			if (isset($row->PackageTitle) && $row->PackageTitle != '')
			{
				$file = $row->CodeFile;
				$replyFile = $row->ReplyFile;
				$hash = stripslashes($row->HashValue);
				$imeiNo = $row->IMEINo;
				$userId = $row->UserId;
				$code = stripslashes($row->Code);
				$pckTitle = stripslashes($row->PackageTitle);
				$codeStatusId = $row->CodeStatusId;
				$credits = $row->Credits;
				$notes = stripslashes($row->Comments);
				$comments = stripslashes($row->Comments1);
				$packageId = $row->PackageId;
				$SMS_Pack = $row->SendSMS;
				$orderDt = $row->RequestedAt;
				$replyDtTm = $row->ReplyDtTm;
				$orderIdFrmServer = $row->OrderIdFromServer;
				$msg4mSrvr = $row->MessageFromServer;
				$sent2OtherSrvr = $row->CodeSentToOtherServer;
				$apiName = stripslashes($row->OrderAPIName);
				$updatedBy = stripslashes($row->LastUpdatedBy);
				$userName = $row->UserName;
			}
		}
		$this->data['plan'] = $plan ;
		$this->data['comments'] = $comments ;
		$this->data['notes'] = $notes ;
		$this->data['code'] = $code ;
		$this->data['updatedBy'] = $updatedBy ;
		$this->data['msg4mSrvr'] = $msg4mSrvr ;
		$this->data['sent2OtherSrvr'] = $sent2OtherSrvr ;
		$this->data['orderIdFrmServer'] = $orderIdFrmServer ;
		$this->data['apiName'] = $apiName ;
		$this->data['replyDtTm'] = $replyDtTm ;
		$this->data['orderDt'] = $orderDt ;
		$this->data['replyFile'] = $replyFile ;
		$this->data['hash'] = $hash ;
		$this->data['userName'] = $userName ;
		$this->data['SMS_Pack'] = $SMS_Pack ;
		$this->data['pckTitle'] = $pckTitle ;
		$this->data['imeiNo'] = $imeiNo ;
		$this->data['packageId'] = $packageId ;
		$this->data['credits'] = $credits ;
		$this->data['userId'] = $userId ;
		$this->data['file'] = $file ;
		$this->data['id'] = $id ;
		$this->data['IS_DEMO'] = FALSE ;
		$this->data['message'] = $message ;
		$this->data['view'] = 'admin/codeslbf';
		$this->load->view('admin/layouts/default1' , $this->data);
	}

	public function ajxoverview(){
		$purpose = $this->input->post_get('purpose');

		if($purpose == 'chPwd')
		{
			$id = ($this->input->post('id')) ? check_input($this->input->post('id'), $this->db->conn_id) : 0;
			if($id != '0')
			{
				$pwd = generatePassword(10, 3);
				$encPwd = bFEncrypt($pwd);
				$this->db->query("UPDATE tbl_gf_users SET UserPassword = '$encPwd', DisableUser = 0, LoginAttempts = 0 WHERE UserId = '$id'");
				$this->db->query("DELETE FROM tbl_gf_blocked_ips WHERE UserId = '$id'");
				$arr = getEmailDetails();
				$result = $objDBCD14->queryUniqueObject("SELECT FirstName, LastName, UserEmail FROM tbl_gf_users WHERE UserId = '$id'");
				if (isset($result->UserEmail) && $result->UserEmail != '')
				{
					$emailMsg = 'Your '.$arr[2].' account password is <b>'.$pwd.'</b>';
					sendGeneralEmail($result->UserEmail, $arr[0], $arr[1], stripslashes($result->FirstName).' '.stripslashes($result->LastName), 'Your '.$arr[2].' Password Information',
						$emailMsg, '');
				}
				$msg = $this->lang->line('BE_GNRL_11');
			}
			else
				$msg =  "Invalid User!";
		}
		else if($purpose == 'changePwd')
		{
			$id = ($this->input->post('id')) ? check_input($this->input->post('id'), $this->db->conn_id) : 0;
			if($id != '0')
			{

				$pwd = ($this->input->post('pwd')) ? check_input($this->input->post('pwd'), $this->db->conn_id) : '';
				$encPwd = bFEncrypt($pwd);
				$this->db->query("UPDATE tbl_gf_users SET UserPassword = '$encPwd' WHERE UserId = '$id'");
				$arr = getEmailDetails();
				$result = $this->Services_model->fetch_codes_by_id("SELECT FirstName, LastName, UserEmail FROM tbl_gf_users WHERE UserId = '$id'",$id);
				if (isset($result->UserEmail) && $result->UserEmail != '')
				{
					$emailMsg = '
			<p>Your '.$arr[2].' account password has been reset to <b>'.$pwd.'</b></p>';
					sendGeneralEmail($result->UserEmail, $arr[0], $arr[1], stripslashes($result->FirstName).' '.stripslashes($result->LastName), 'Your '.$arr[2].' Password Information',
						$emailMsg, '');
				}
				$msg =  $this->lang->line('BE_GNRL_11');
			}
			else
				$msg =  "Invalid User!";
		}
		else if($purpose == 'sendEml')
		{
			$id = ($this->input->post('userId')) ? check_input($this->input->post('userId'), $this->db->conn_id) : '0';
			$ltrId = ($this->input->post('ltrId')) ? check_input($this->input->post('ltrId'), $this->db->conn_id) : 0;
			if($id != '0' && $ltrId > 0)
			{
				$row =$this->Services_model->fetch_codes_by_id("SELECT NewsLtrTitle, NewsLtr FROM tbl_gf_news_letter WHERE NewsLtrId = '$ltrId'",$ltrId);
				if (isset($row->NewsLtrTitle) && $row->NewsLtrTitle != '')
				{
					$newsLtrTitle = stripslashes($row->NewsLtrTitle);
					$newsLtr = decodeHTML(stripslashes($row->NewsLtr));
				}
				$arr = getEmailDetails();
				$rwUsr = $this->Services_model->fetch_codes_by_id("SELECT UserEmail FROM tbl_gf_users WHERE UserId = '$id'",$id);

				sendGeneralEmail($rwUsr->UserEmail, $arr[0], $arr[1], 'Customer', $newsLtrTitle, '', $newsLtr);
				$msg =  $this->lang->line('BE_GNRL_11');
			}
			else
				$msg =  "Invalid User or Newsletter!";
		}
		else if($purpose == 'chPin')
		{
			$id = ($this->input->post('id')) ? check_input($this->input->post('id'), $this->db->conn_id) : 0;
			if($id != '0')
			{
				$pin = generatePin(4);
				$encPin = encryptThe_String($pin);
				$this->db->query("UPDATE tbl_gf_users SET PinCode = '$encPin' WHERE UserId = '$id'");
				$arr = getEmailDetails();
				$result = $this->Services_model->fetch_codes_by_id("SELECT FirstName, LastName, UserEmail FROM tbl_gf_users WHERE UserId = '$id'",$id);
				if (isset($result->UserEmail) && $result->UserEmail != '')
				{
					$emailMsg = '<p>Your '.$arr[2].' account pin code is <b>'.$pin.'</b></p>';
					sendGeneralEmail($result->UserEmail, $arr[0], $arr[1], stripslashes($result->FirstName).' '.stripslashes($result->LastName), 'Your '.$arr[2].' Pin Code Information',
						$emailMsg, '');
				}
				$msg =  $this->lang->line('BE_GNRL_11');
			}
			else
				$msg =  "Invalid User!";
		}
		else if($purpose == 'save')
		{
			$id = ($this->input->post('id')) ? check_input($this->input->post('id'), $this->db->conn_id) : 0;
			$autoFllCrdts = ($this->input->post('autoFllCrdts')) ? check_input($this->input->post('autoFllCrdts'), $this->db->conn_id) : 0;
			$allowCrdsTrnsfr = ($this->input->post('allowCrdsTrnsfr')) ? check_input($this->input->post('allowCrdsTrnsfr'), $this->db->conn_id) : 0;
			$rmvAllSrvcs = ($this->input->post('rmvAllSrvcs')) ? check_input($this->input->post('rmvAllSrvcs'), $this->db->conn_id) : 0;
			$allowAPI = ($this->input->post('allowAPI')) ? check_input($this->input->post('allowAPI'), $this->db->conn_id) : 0;
			$disableUser = ($this->input->post('disableUser')) ? check_input($this->input->post('disableUser'), $this->db->conn_id) : 0;
			$planId = ($this->input->post('planId')) ? check_input($this->input->post('planId'), $this->db->conn_id) : 0;
			$reason = ($this->input->post('reason')) ? check_input($this->input->post('reason'), $this->db->conn_id) : '';
			$buyCr = ($this->input->post('buyCr')) ? check_input($this->input->post('buyCr'), $this->db->conn_id) : 0;

			if($id != '0')
			{
				$this->db->query("UPDATE tbl_gf_users SET AutoFillCredits = '$autoFllCrdts', TransferCredits = '$allowCrdsTrnsfr', RemoveAllServices = '$rmvAllSrvcs', DisableUser = '$disableUser', PricePlanId = '$planId', AllowAPI = '$allowAPI', Comments = '$reason', CanAddCredits = '$buyCr' WHERE UserId = '$id'");
				if($rmvAllSrvcs == '1')
				{
					$str = '';
					$rsPacks =  $this->Services_model->fetch_packages_by_Archived(0);
					foreach($rsPacks as $row)
					{
						if($str != '')
							$str .= ',';
						$str .= "(".$id.", '".check_input($row->PackageId, $this->db->conn_id)."', '".check_input($row->sl3lbf, $this->db->conn_id)."')";
					}
					$this->db->query("DELETE FROM tbl_gf_user_packages WHERE UserId = '$id' AND ServiceType IN (0, 1, 2)");
					if($str != '')
						$this->db->query("INSERT INTO tbl_gf_user_packages (UserId, PackageId, ServiceType) VALUES $str");

					$str = '';
					$rsSrvrPacks = $this->Services_model->fetch_log_packages_by_Archived(0);
					foreach($rsSrvrPacks as $row )
					{
						if($str != '')
							$str .= ',';
						$str .= "(".$id.", '".check_input($row->LogPackageId, $this->db->conn_id)."', '2')";
					}
					if($str != '')
						$this->db->query("INSERT INTO tbl_gf_user_packages (UserId, PackageId, ServiceType) VALUES $str");
				}
				else
				{
					$this->db->query("DELETE FROM tbl_gf_user_packages WHERE UserId = '$id' AND ServiceType IN (0, 1, 2)");
				}
				$msg =  $this->lang->line('BE_GNRL_11');
			}
			else
				$msg =  "Invalid User!";

		}
		else if($purpose == 'sendCEml')
		{
			$subject = ($this->input->post('subject')) ? check_input($this->input->post('subject'), $this->db->conn_id) : '';
			$msg = ($this->input->post('msg')) ? check_input($this->input->post('msg'), $this->db->conn_id) : '';
			$id = ($this->input->post('userId')) ? check_input($this->input->post('userId'), $this->db->conn_id) : '0';
			if($subject != '' && $msg != '' && $id > 0)
			{
				$arr = getEmailDetails();
				$rwUsr = $this->Services_model->fetch_codes_by_id("SELECT FirstName, UserEmail FROM tbl_gf_users WHERE UserId = '$id'",$id);
				sendGeneralEmail($rwUsr->UserEmail, $arr[0], $arr[1], $rwUsr->FirstName, $subject, $msg, '', $objDBCD14, '', $subject);
				$msg =  $this->lang->line('BE_GNRL_11');
			}
			else
				$msg =  "Invalid Data!";
		}
		else if($purpose == 'sendEml_rtl')
		{
			$email = ($this->input->post('email')) ? check_input($this->input->post('email'), $this->db->conn_id) : '';
			$ltrId = ($this->input->post('ltrId')) ? check_input($this->input->post('ltrId'), $this->db->conn_id) : 0;
			if($email != '' && $ltrId > 0)
			{
				$row = $this->Services_model->fetch_codes_by_id("SELECT NewsLtrTitle, NewsLtr FROM tbl_gf_news_letter WHERE NewsLtrId = '$ltrId'",$ltrId);
				if (isset($row->NewsLtrTitle) && $row->NewsLtrTitle != '')
				{
					$newsLtrTitle = stripslashes($row->NewsLtrTitle);
					$newsLtr = decodeHTML(stripslashes($row->NewsLtr));
				}
				$arr = getEmailDetails();
				sendGeneralEmail($email, $arr[0], $arr[1], 'Customer', $newsLtrTitle, '', $newsLtr);
				$msg =  $this->lang->line('BE_GNRL_11');
			}
			else
				$msg =  "Invalid User or Newsletter!";
		}
		else if($purpose == 'sendCEml_rtl')
		{
			$subject = ($this->input->post('subject')) ? check_input($this->input->post('subject'), $this->db->conn_id) : '';
			$msg = ($this->input->post('msg')) ? check_input($this->input->post('msg'), $this->db->conn_id) : '';
			$email = ($this->input->post('email')) ? check_input($this->input->post('email'), $this->db->conn_id) : '';
			if($subject != '' && $msg != '' && $email != '')
			{
				$arr = getEmailDetails();
				sendGeneralEmail($email, $arr[0], $arr[1], 'Customer', $subject, $msg, '', '', $subject);
				$msg =  $this->lang->line('BE_GNRL_11');
			}
			else
				$msg =  "Invalid Data!".$email;
		}
		echo $msg;
		exit;
	}

    public function newfileorders(){
        if($this->input->post('btnReprocessing'))
        {
            $arrIds = $this->input->post_get('chkOrders') ?: 0;
            if(is_array($arrIds))
            {
                $orderIds = implode(',', $arrIds);
                
                $update_data = array(
                    'HiddenStatus' => 0 ,
                    'MessageFromServer' => ''
                );
                
                $where_data = array(
                    'HiddenStatus' => 1
                );

                $where_in = array(
                    'CodeId' => $orderIds
                );

                $this->Services_model->update_slbf_codes($update_data , $where_data , $where_in);
                $message = 'IMEI Orders have been marked to be resubmitted successfully!';
            }

        }

        if($this->input->post_get('rp') && $this->input->post_get('rp') == '1')
        {
            $pkId = $this->input->post_get('pkId') ?: 0;

            $update_data = array(
                'HiddenStatus' => 0 ,
                'MessageFromServer' => ''
            );
            
            $where_data = array(
                'PackageId' => $pkId,
                'CodeStatusId' => 1,
                'OrderAPIId >' => 0,
                'CodeSentToOtherServer' => 0,
                'HiddenStatus' => 1
            );

            $this->Services_model->update_slbf_codes($update_data , $where_data);
            $message = 'IMEI Service orders have been marked to be resubmitted successfully!';
        }
        $strWhere = '';
        $dt = $this->input->post_get('dt') ?: '';
        if($dt != '')
        {
            $strWhere = " AND DATE(RequestedAt) >= '$dt'";
        } 
        $this->data['dt'] = $dt;
        
        $this->data['rsNewOrders'] = $this->Services_model->fetch_codes_slbf_packages($strWhere);
        
       
        $this->data['count'] = count($this->data['rsNewOrders']);
        
        $ARR_API_ERRORS = array();
        $rsAPIErrorOrders =  $this->Services_model->fetch_codes_slbf($strWhere);
     
        foreach($rsAPIErrorOrders as $row)
        {
            if($row->MessageFromServer != '')
                $ARR_API_ERRORS[$row->PackageId] = stripslashes($row->MessageFromServer);
        }


        $this->data['ARR_API_ERRORS'] = $ARR_API_ERRORS;
        $this->data['view'] = 'admin/new_file_orders' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function verifyfileorders(){
        $strWhere = '';
        $imei = '';
        $packageId = 0;
        $codeStatusId = 0;
        $searchType = 0;
        $txtlqry = '';
        $sqlwhere = '';
        $verifyPage = '1';
        $pLast = '' ;
        $this->data['page_name'] = $this->input->server('PHP_SELF');
        $strPckWhere = ' AND Cat.SL3BF = 1 AND A.sl3lbf = 1';
        $tblOrders = 'tbl_gf_codes_slbf';
        $searchType = $this->input->post_get('searchType') ?: '3';
        $uId = $this->input->post_get('uId') ?: 0;
        if($uId != '0')
        {
            $strWhere .= " AND A.UserId = '$uId'";
            $txtlqry .= "&uId=$uId";
        }
        if(!isset($start))
            $start = 0;

        if($this->input->post_get("start"))
            $start = $this->input->post_get("start");

        $eu = ($start - 0);
        $limit = $this->input->post_get('records') ?: 50;
        $thisp = $eu + $limit;
        $back = $eu - $limit;
        $next = $eu + $limit;
        $message = '';
        if($this->input->post_get('hdsbmt') && $this->session->userdata('GSM_FSN_AdmId'))
        {
            $codeIds = $this->input->post_get('chkCodes') ?: 0;
            $totalCodes = 0;
            if(is_array($codeIds))
                $totalCodes = count($codeIds);

            $codeIds_reply = $this->input->post_get('chkReply') ?: 0;
            $totalCodes_reply = 0;
            if(is_array($codeIds_reply))
                $totalCodes_reply = count($codeIds_reply);

            if($totalCodes_reply == 0)
                $message = $this->lang->line('BE_LBL_577');
            else
            {
                for($i=0; $i < $totalCodes; $i++)
                {
                    $updateStatus = true;
                    $data = explode('|', $codeIds[$i]);
                    $codeId = $data[0];
                    $code = $this->input->post_get('txtCode'.$data[4]);
                    if($codeIds_reply != '0')
                    {
                        if(in_array($codeIds[$i], $codeIds_reply))
                            $codeStatusId = '3';
                        else
                            $updateStatus = false;
                    }
                    else
                        $updateStatus = false;
                    if($updateStatus)
                    {
                        $message  = updatefileorderstatus($data , $codeId , $codeStatusId, $code);
                        
                        $update_data = array(
                            'LastUpdatedBy' => $this->session->userdata('AdminUserName'),
                            'Verify' => 2
                        );

                        $where_data = array(
                            'CodeId' => $codeId
                        );

                        $this->Services_model->update_slbf_codes($update_data , $where_data);                 
                    }
                }
                for($i=0; $i < $totalCodes_reply; $i++)
                {
                    $data = explode('|', $codeIds_reply[$i]);
                    $codeId = $data[0];
                    $code = $this->input->post_get('txtCode'.$data[4]);
                    $codeStatusId = '2';
                    $updateStatus = true;
                    if($codeIds != '0')
                    {
                        if(in_array($codeIds_reply[$i], $codeIds))
                            $updateStatus = false;
                    }
                    if($updateStatus)
                    {
                        $message  = updatefileorderstatus($data , $codeId , $codeStatusId, $code);
                        $update_data = array(
                            'LastUpdatedBy' => $this->session->userdata('AdminUserName'),
                            'Verify' => 2
                        );

                        $where_data = array(
                            'CodeId' => $codeId
                        );

                        $this->Services_model->update_slbf_codes($update_data , $where_data);   
                        
                    }
                }
                $message = $this->lang->line('BE_GNRL_11');
            }
        }
        if($this->input->post_get('txtIMEI'))
        {
            $imei = trim($this->input->post_get('txtIMEI'));
            $packageId = $this->input->post_get('packageId');
            $codeStatusId =  $this->input->post_get('codeStatusId');
            $uName =  $this->input->post_get('txtUName') ? trim( $this->input->post_get('txtUName')) : '';
            if($uName != '')
            {
                $strWhere = " AND UserName LIKE '%".$uName."%'";
                $txtlqry .= "&txtUName=$uName";
            }
            if($imei != '')
            {
                $strWhere = " AND IMEINo LIKE '%".$imei."%'";
                $txtlqry .= "&txtIMEI=$imei";
            }
            if($packageId != '0')
            {
                $strWhere .= " AND A.PackageId = '$packageId'";
                $txtlqry .= "&packageId=$packageId";
            }
            if($codeStatusId != '0')
            {
                $strWhere .= " AND A.CodeStatusId = '$codeStatusId'";
                $txtlqry .= "&codeStatusId=$codeStatusId";
            }
        }
        $this->data['rsCodes'] = $this->Services_model->fetch_codes_slbf_packages_users_by_strwhere($strWhere ,$eu, $limit);
        
        $this->data['count'] = count($this->data['rsCodes']);

        $this->data['strWhere'] =$strWhere ;
        $this->data['strPckWhere'] = $strPckWhere;
        $this->data['strPackIds'] = '';
        $this->data['packageId'] = $packageId;
        $this->data['codeStatusId']  = $codeStatusId;
        $this->data['back'] = $back;
        $this->data['start'] = $start;
        $this->data['txtlqry'] =$txtlqry;
        $this->data['limit'] = $limit;
        $this->data['eu'] = $eu;
        $this->data['pLast'] =$pLast;
        $this->data['thisp'] = $thisp;
        $this->data['next'] = $next;
        $this->data['uId'] = $uId;
        $this->data['message'] = $message  ;

        $this->data['view'] = 'admin/verify_file_orders' ;

        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function overview(){
        $USER_ID_BTNS = $this->input->post_get('id') ?: 0;
        $message = '';
        $this->data['phone'] = '';
        $totalReceipts = 0;
        if($this->input->post_get('txtIP') && $this->input->post_get('txtIP') != '')
        {
            $ip = $this->input->post_get('txtIP');
            $comments = $this->input->post_get('txtComments');
            $dtTm = setDtTmWRTYourCountry();
            $arrIPs = explode(',', $ip);
            $strData = '';
            foreach ($arrIPs as $value)
            {
                if($value != '')
                {
                    if($strData != '')
                        $strData .= ',';
                    $strData .= "('".$value."','".$dtTm."','".$comments."','".$USER_ID_BTNS."')";
                }
            } 
            $this->Services_model->insert_blocked_ips($strData);
         
            $comments = '';
            $ip = '';
            $message = $this->lang->line('BE_GNRL_11');
        }
        else if($this->input->post_get('txtIP') && $this->input->post_get('txtIP') == '')
        {
            $message = $this->lang->line('BE_LBL_392');
        }
        if($this->input->post('hdRmvPMs'))
        {
            //========================================== REMOVING PAYMENT METHODS ==========================================//
            $selectedIds = sizeof($this->input->post('pMethodIds'));
            $strData = '';
            for($k = 0; $k < $selectedIds; $k++)
            {
                if($k > 0)
                    $strData .= ',';
                $strData .= "('$USER_ID_BTNS',".check_input($_POST['pMethodIds'][$k]).")";
            }
            $this->Services_model->del_user_payment_methods($USER_ID_BTNS);
           
            if($strData != '')
            {
                $this->Services_model->insert_user_payment_methods($strData);
              
            }
            //========================================== REMOVING PAYMENT METHODS ==========================================//
            $message = $this->lang->line('BE_GNRL_11');
        }
        $this->crypt_key($USER_ID_BTNS);
        $paidInvoices_user = 0;
        $paidInvoices_admin = 0;
        $unpaidInvoices_user = 0;
        $unpaidInvoices_admin = 0;
        $cancelledInvoices_user = 0;
        $rwUser = $this->Services_model->fetch_countries_users_currency($USER_ID_BTNS);
        if (isset($rwUser->UserName) && $rwUser->UserName != '')
        {
            $userName = $rwUser->UserName;
            $userEmail = $rwUser->UserEmail;
            $name = $rwUser->Name;
            $dt = $rwUser->AddedAt;
            $Phone = $rwUser->Phone;
            $country = $rwUser->Country;
            $status = $rwUser->DisableUser;
            $reason = stripslashes($rwUser->Comments);
            $autoFillCredits = $rwUser->AutoFillCredits;
            $transferCredits = $rwUser->TransferCredits;
            $currency = $rwUser->CurrencySymbol;
            $currencyAbb = $rwUser->CurrencyAbb;
            $API_ALLOWED = $rwUser->AllowAPI;
            $planId = $rwUser->PricePlanId;
            $rmvAllSrvcs = $rwUser->RemoveAllServices;
            $buyCr = $rwUser->CanAddCredits;
            $customerCredits = $this->decrypt($rwUser->Credits);
            if($customerCredits != '')
                $customerCredits = number_format($customerCredits, 2, '.', '');
            if($customerCredits == '')
                $customerCredits = '0.00';
        }
        if ($currency == '')
        {
            $rwCurr = fetch_currency_data();
            $currency = $rwCurr->CurrencySymbol;
        }

        $this->data['rsIMEIOrders'] = $this->Services_model->fetch_codes_packages_count($USER_ID_BTNS);
       
        $this->data['rsFileOrders'] = $this->Services_model->fetch_codes_slbf_packages_count($USER_ID_BTNS);
      
        $rsServerOrders = $this->Services_model->fetch_log_requests_packages_count($USER_ID_BTNS);
       
        $rsUserInvoices = $this->Services_model->fetch_payments_users($USER_ID_BTNS);
    
        $rsAdminInvoices = $this->Services_model->fetch_payments_admin($USER_ID_BTNS);
       
        foreach($rsUserInvoices as $row)
        {
            $invoiceAmount = $this->decrypt($row->Amount);
            if($row->PaymentStatus == '2')
                $paidInvoices_user = $paidInvoices_user + $invoiceAmount;
            else if($row->PaymentStatus == '1')
                $unpaidInvoices_user = $unpaidInvoices_user + $invoiceAmount;
        }
        foreach($rsAdminInvoices as $row)
        {
            $invoiceAmount = $this->decrypt($row->Amount);
            if($row->PaymentStatus == '2')
                $paidInvoices_admin = $paidInvoices_admin + $invoiceAmount;
            else if($row->PaymentStatus == '1')
                $unpaidInvoices_admin = $unpaidInvoices_admin + $invoiceAmount;
        }
        $row = $this->Services_model->fetch_codes_packages_count_sl3lbf($USER_ID_BTNS);
        $IMEI_SERVICES_USER = $row->TotalServices;
        
        $row = $this->Services_model->fetch_codes_slbf_packages_sl3lbf($USER_ID_BTNS);
        $FILE_SERVICES_USER = $row->TotalServices;
        
        $row = $this->Services_model->fetch_count_log_requests($USER_ID_BTNS);
        $SERVER_SERVICES_USER = $row->TotalServices;

        $row = $this->Services_model->fetch_packages_count();
        $IMEI_SERVICES = $row->TotalServices;
        
        $row = $this->Services_model->fetch_packages_count_by_sl3lbf();
        $FILE_SERVICES = $row->TotalServices;
        
        $row = $this->Services_model->fetch_log_packages_count();
        $SERVER_SERVICES = $row->TotalServices;

        $rwSettings = $this->Services_model->fetch_email_settings_by_id();
        $minCredits = $rwSettings->MinCredits;

        $rsTotalReceipts = $this->Services_model->fetch_payments_by_id($USER_ID_BTNS);
        foreach($rsTotalReceipts as $row)
        {
            $amount = $this->decrypt($row->Amount);
			if(is_numeric($amount))
            	$totalReceipts = $totalReceipts + $amount;
        }

        $del = $this->input->post_get('del') ? 1 : 0;
        if($del == '1')
        {
            $rid = $this->input->post_get('rid') ?: 0;
            $this->Services_model->update_users_data($USER_ID_BTNS);
          
            $this->Services_model->del_blocked_ips($rid);

            $message = $this->lang->line('BE_GNRL_12');
        }
        $this->data['rsBlockedIPs'] = $this->Services_model->fetch_blocked_ips_by_userid($USER_ID_BTNS);
       

        $this->data['message'] = $message ;
        $this->data['USER_ID_BTNS'] = $USER_ID_BTNS;
        $this->data['name'] = $name ;
        $this->data['userName'] = $userName;
        $this->data['status'] = $status;
        $this->data['userEmail'] = $userEmail;
        $this->data['country'] = $country;
        $this->data['dt'] = $dt ;
        $this->data['currencyAbb'] = $currencyAbb;
        $this->data['reason'] = $reason;
        $this->data['API_ALLOWED'] = $API_ALLOWED;
        $this->data['currency'] = $currency;
        $this->data['customerCredits'] = $customerCredits;
        $this->data['planId'] = $planId;
        $this->data['paidInvoices_user'] = $paidInvoices_user;
        $this->data['unpaidInvoices_user'] = $unpaidInvoices_user;
        $this->data['paidInvoices_admin'] = $paidInvoices_admin;
        $this->data['transferCredits'] = $transferCredits;
        $this->data['buyCr'] = $buyCr;
        $this->data['autoFillCredits'] = $autoFillCredits;
        $this->data['rmvAllSrvcs'] = $rmvAllSrvcs;
        $this->data['totalReceipts'] = $totalReceipts;
        $this->data['unpaidInvoices_admin'] = $unpaidInvoices_admin;
        $this->data['minCredits'] = $minCredits;
        $this->data['rsServerOrders'] = $rsServerOrders;


        $this->data['overview_topservices'] = $this->load->view('admin/overview_topservices' , $this->data , TRUE);
        $this->data['overview_quickactions'] = $this->load->view('admin/overview_quickactions' , $this->data , TRUE);
        $this->data['overview_invoices'] = $this->load->view('admin/overview_invoices' , $this->data , TRUE);
        $this->data['overview_blockedips'] = $this->load->view('admin/overview_blockedips' , $this->data , TRUE);
 
        $this->data['view'] = 'admin/overview' ;
        $this->load->view('admin/layouts/default1' , $this->data);

    }

    public function downloadlbffiles(){
        $id = $this->input->post_get('id') ?: 0;
        $file = '';
    
        $row = $this->Services_model->fetch_codefile_from_codes_slbf($id); 
        if (isset($row->CodeFile) && $row->CodeFile != '')
            $file = $row->CodeFile;
        if($file != '')
            downloadFile($file);
        
    }


    private function downloadFile($path, $contentType = 'application/octet-stream')
    {
        ignore_user_abort(true);	
        header('Content-Transfer-Encoding: binary');
        header('Content-Disposition: attachment; filename="' .basename($path) . "\";");
        header("Content-Type: $contentType");
        
        $res = array(
        'status' => false,
        'errors' => array(),
        'readfileStatus' => null,
        'aborted' => false
        );
        
        $res['readfileStatus'] = readfile($path);
        if ($res['readfileStatus'] === false)
        {
            $res['errors'][] = 'readfile failed.';
            $res['status'] = false;
        }
        
        if (connection_aborted())
        {
            $res['errors'][] = 'Connection aborted.';
            $res['aborted'] = true;
            $res['status'] = false;
        }	
        return $res;
    }

    public function downloadfileorders(){
        $ids = $this->input->post('ids') ?: 0;
        $strIMEIs = '';
        $strIMEIs = showAcceptedFileOrders($ids, 1);
        if($strIMEIs != '')
        {
            $fileName = rand(100000000, 999999999);
            $myFile = FCPATH."imeifiles/$fileName.txt";
            $fh = fopen($myFile, 'w') or die("can't open file");
            $strIMEIs = str_replace('<br />', "\r\n", $strIMEIs);
            $strIMEIs = str_replace('<br>', "\r\n", $strIMEIs);
            fwrite($fh, $strIMEIs);
            fclose($fh);

            $file = FCPATH.'imeifiles/'.$fileName.'.txt';
            $strData = '';
            $fh = fopen($file, 'r');
            $strData = fread($fh, filesize($file));
            fclose($fh);
            header("Pragma: public");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header('Content-Disposition: attachment; filename="'.$fileName.'.txt"');
            echo $strData;
            exit;
        }
        exit;
    }

    public function reprocessorders(){
        $MY_SERVICE_ID = $this->input->post_get('id') ?: 0;
        $sc = $this->input->post_get('sc') ?: 0;
        $pn = $this->input->post_get('pn') ? str_replace('S_P_', ' ', $this->input->post_get('pn')) : '';
        $type = $this->input->post_get('type') ?: 0;
        $totalOrders = 0;
        if($MY_SERVICE_ID > 0)
        {
            switch($sc)
            {
                case '0': // IMEI Orders
                    $colId = 'CodeId';
                    $packColId = 'PackageId';
                    $stColId = 'CodeStatusId';
                    $tblOrders = 'tbl_gf_codes';
                    $frmAction = 'admin/services/newimeiorders';
                    break;
                case '1': // File Orders
                    $colId = 'CodeId';
                    $packColId = 'PackageId';
                    $stColId = 'CodeStatusId';
                    $tblOrders = 'tbl_gf_codes_slbf';
                    $frmAction = 'admin/services/newfileorders';
                    break;
                case '2': // Server Orders
                    $colId = 'LogRequestId';
                    $packColId = 'LogPackageId';
                    $stColId = 'StatusId';
                    $tblOrders = 'tbl_gf_log_requests';
                    $frmAction = 'admin/services/newserverorders';
                    break;
                case '3': // Retail Orders
                    $colId = 'RetailOrderId';
                    $packColId = 'PackageId';
                    $stColId = 'OrderStatusId';
                    $tblOrders = 'tbl_gf_retail_orders';
                    $frmAction = 'admin/services/newretailorders?type='.$type;
                    break;
            }
            if($sc == '3') // Retail
            {
                $this->data['rsOrders'] = $this->Services_model->fetch_retail_payment_and_orders($colId ,$tblOrders ,$stColId ,$packColId);
                
            }
            else
            {
                $this->data['rsOrders'] = $this->Services_model->generel_reprocess_orders_query($colId ,$tblOrders ,$stColId , $packColId , $MY_SERVICE_ID);
                
            }
            $this->data['totalOrders'] = count($rsOrders);
        }

        $this->data['view'] = 'admin/reprocessorders' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function logrequests(){
        $strWhere = '';
        $orderNo = '';
        $logPackageId = 0;
        $codeStatusId = 0;
        $sc = 2;
        $txtlqry = '';
        $sqlwhere = '';
        $this->data['page_name'] = $this->input->server('PHP_SELF');
        $searchType = $this->input->post_get('searchType') ?: 0;
        $uId = $this->input->post_get('uId') ?: 0;
        $planId = $this->input->post_get('planId') ?: 0;
        $dtFrom = $this->input->post_get('txtFromDt') ?: '';
        $dtTo = $this->input->post_get('txtToDt') ?: '';
        $packId = $this->input->post_get('packId') ?: 0;
        $applyAPI = $this->input->post_get('applyAPI') ?: 0;  
        $arcIds = '0'; 
        $uName = '' ;    
        
        if($planId != '0')
        {
            $strWhere .= " AND PricePlanId = '$planId'";
            $txtlqry .= "&planId=$planId";
        }
        if($uId != '0')
        {
            $strWhere .= " AND A.UserId = '$uId'";
            $txtlqry .= "&uId=$uId";
        }
        if($searchType != '0')
        {
            $txtlqry .= "&searchType=$searchType";
        }
        if($packId != '0' && $packId != '')
        {
            $strWhere .= " AND A.LogPackageId = '$packId'";
            $txtlqry .= "&packId=$packId";
        }
        if($this->input->post_get('txtFromDt'))
        {
            if($dtFrom != '' && $dtTo != '')
                $strWhere .= " And DATE(RequestedAt) >= '" . $dtFrom . "' AND DATE(RequestedAt) <= '" . $dtTo . "'";
            else
            {
                if($dtFrom != '')
                    $strWhere .= " AND DATE(RequestedAt) >= '" . $dtFrom . "'";
                if($dtTo!= '')
                    $strWhere .= " AND DATE(RequestedAt) <= '" . $dtTo . "'";
            }
        }
        $TD1_WIDTH = '50%';
        $TD2_WIDTH = '50%';
        if($searchType == '2' || $searchType == '3')
        {
            $DD_WIDTH = '550px';
            $TD1_WIDTH = '40%';
            $TD2_WIDTH = '60%';
        }

        if(!isset($start))
            $start = 0;

        if($this->input->post_get("start"))
            $start = $this->input->post_get("start");

        $eu = ($start - 0);
        $limit = $this->input->post_get('records') ?: 100;
        $thisp = $eu + $limit;
        $back = $eu - $limit;
        $next = $eu + $limit;
        $message = '';
        $cldFrm = $this->input->post_get('cldFrm') ?: 0;

        if($this->input->post('hdsbmt') && $this->session->userdata('GSM_FSN_AdmId'))
        {
            $codeIds = $this->input->post('chkCodes') ?: 0;
            $totalCodes = 0;
            if(is_array($codeIds))
                $totalCodes = count($codeIds);

            if($searchType == '3')
            {
                $codeIds_reply = $this->input->post('chkReply') ?: 0;
                $totalCodes_reply = 0;
                if(is_array($codeIds_reply))
                    $totalCodes_reply = count($codeIds_reply);
            }
            if($cldFrm == 4)
            {
                
                for($i = 0; $i < $totalCodes; $i++)
                {
                    $data = explode('|', $codeIds[$i]);
                    $arcIds .= ','.$data[0];
                }

                $update_data = array(
                    'LastUpdatedBy' => $this->session->userdata('AdminUserName'),
                    'StatusId' => 4
                );

                $where_in = array(
                    'LogRequestId' => $arcIds,
                );
                $this->Services_model->update_log_requests($update_data, '' , $where_in);
                
                $message = $this->lang->line('BE_GNRL_11');
            }
            else if($cldFrm == 2 || $cldFrm == 3 || $cldFrm == 5 || $cldFrm == 6)
            {
                for($i=0; $i<$totalCodes; $i++)
                {
                    $updateStatus = true;
                    $data = explode('|', $codeIds[$i]);
                    $codeId = $data[0];
                    if($this->input->post('txtBulkCodeVal') && $this->input->post('txtBulkCodeVal') != '')
                        $code = $this->input->post('txtBulkCodeVal');
                    else
                        $code =$this->input->post('txtCode'.$data[3]);
                    $serviceTitle = $data[5];
                    if($cldFrm == '2')
                        $codeStatusId = $this->input->post('codeStatusId'.$data[3]);
                    else if($cldFrm == '3')
                        $codeStatusId = $this->input->post('blkCodeStatusId');
                    else if($cldFrm == '5')
                        $codeStatusId = '3';
                    else if($cldFrm == '6')
                    {
                        if($codeIds_reply != '0')
                        {
                            if(in_array($codeIds[$i], $codeIds_reply))
                                $codeStatusId = '3';
                            else
                                $updateStatus = false;
                        }
                        else
                            $updateStatus = false;
                    }
                    if($updateStatus)
                    {
                        $message=updateserverorderstatus($data , $codeId , $codeStatusId, $code , $serviceTitle);
                    }
                }
                if($searchType == '3')
                {
                    for($i=0; $i < $totalCodes_reply; $i++)
                    {
                        $data = explode('|', $codeIds_reply[$i]);
                        $codeId = $data[0];
                        if($this->input->post('txtBulkCodeVal'))
                            $code = $this->input->post('txtBulkCodeVal');
                        else
                            $code = $this->input->post('txtCode'.$data[3]);
                        $serviceTitle = $data[5];
                        $codeStatusId = '2';
                        $updateStatus = true;
                        if($codeIds != '0')
                        {
                            if(in_array($codeIds_reply[$i], $codeIds))
                                $updateStatus = false;
                        }
                        if($updateStatus)
                        {
                            $message=updateserverorderstatus($data , $codeId , $codeStatusId, $code , $serviceTitle);
                        }
                    }
                }
            }
            else if($cldFrm == '7')
            {
                include APPPATH.'scripts/applyapi.php';	
            }
        }

        if($this->input->post_get('txtOrderNo'))
        {
            $orderNo = trim($this->input->post_get('txtOrderNo'));
            if($orderNo != '')
            {
                $strWhere .= " AND LogRequestId = '$orderNo'";
                $txtlqry .= "&txtOrderNo=$orderNo";
            }
        }
        if($this->input->post('hdnOrdrIds'))
        {
            $orderIds = $this->input->post('hdnOrdrIds'); 
            $strWhere .= " AND A.LogRequestId IN ($orderIds)";
            $txtlqry .= "&hdnOrdrIds=$orderIds";
        }
        if($this->input->post('txtUName'))
        {
            $uName = $this->input->post('txtUName') ? trim($this->input->post('txtUName')) : '';
            if($uName != '')
            {
                $strWhere .= " AND UserName LIKE '%".$uName."%'";
                $txtlqry .= "&txtUName=$uName";
            }
        }
        if($this->input->post_get('logPackageId'))
        {
            $logPackageId = $this->input->post_get('logPackageId');
            if($logPackageId != '0')
                $strWhere .= " AND A.LogPackageId LIKE '$logPackageId'";
        }
        if($this->input->post_get('codeStatusId'))
        {
            $codeStatusId = $this->input->post_get('codeStatusId');
            if($codeStatusId != '0')
                $strWhere .= " AND A.StatusId = '$codeStatusId'";
        }
        $CUST_COL_LBL = array();
        $rsFields = $this->Services_model->fetch_custom_fields(); 
       
        $colIndex = 1 ;
        $strCustomCols = '';
        foreach($rsFields as $row)
        {
            $strCustomCols .= ', '.$row->FieldColName.' AS Col'.$colIndex;
            $CUST_COL_LBL['Col'.$colIndex] = $row->FieldLabel;
            $colIndex++;
        }
        $this->data['rsCodes'] = $this->Services_model->fetch_log_request_packages_users_status($strWhere , $strCustomCols ,$eu, $limit);
        $this->data['count'] = count($this->data['rsCodes']);

        $this->data['uId'] = $uId;
        $this->data['errorMsg'] = '';
        $this->data['message'] = $message;
        $this->data['arcIds'] = $arcIds;
        $this->data['applyAPI'] = $applyAPI;
        $this->data['limit'] =  $limit;
        $this->data['searchType'] = $searchType;
        $this->data['planId'] = $planId;
        $this->data['packId'] = $packId;
        $this->data['uName'] = $uName;
        $this->data['orderNo'] = $orderNo;
        $this->data['dtFrom'] = $dtFrom;
        $this->data['dtTo'] =  $dtTo;
        $this->data['sc'] = $sc;
        $this->data['CUST_COL_LBL'] =  $CUST_COL_LBL;
        $this->data['orderBy'] = '' ;
        $this->data['codeStatusId'] =  $codeStatusId;
        $this->data['start'] =  $start;
        $this->data['strWhere'] = $strWhere;
        $this->data['txtlqry'] = $txtlqry;
        $this->data['back'] = $back ;
        $this->data['eu'] = $eu; 
        $this->data['pLast'] = ''; 
        $this->data['thisp'] = $thisp;
        $this->data['next'] = $next;
        $this->data['colIndex'] = $colIndex;
        $this->data['logPackageId'] = $logPackageId;
        $this->data['myNetworkId']= '';
        $this->data['view'] = 'admin/log_requests';
        $this->load->view('admin/layouts/default1' , $this->data);

    }

    public function newserverorders(){
        if($this->input->post('btnReprocessing'))
        {
            $arrIds = $this->input->post_get('chkOrders') ?: 0;
            if(is_array($arrIds))
            {
                $orderIds = implode(',', $arrIds);
                $update_data = array(
                    'HiddenStatus' => 0 ,
                    'MessageFromServer' => '' ,
                );
                $where_data = array(
                    'HiddenStatus' => 1 
                );

                $where_in = array(
                    'LogRequestId' => $orderIds
                );

                update_log_requests($update_data, $where_data , $where_in);
               
                $message = 'IMEI Orders have been marked to be resubmitted successfully!';
            }
        }
        if($this->input->post_get('rp') && $this->input->post_get('rp') == '1')
        {
            $pkId = $this->input->post_get('pkId') ?: 0;

            $update_data = array(
                'HiddenStatus' => 0 ,
                'MessageFromServer' => '' ,
            );
            $where_data = array(
                'LogPackageId' => $pkId ,
                'StatusId' => 1,
                'OrderAPIId >' => 0 ,
                'CodeSentToOtherServer' => 0,
                'HiddenStatus' => 1
            );

            update_log_requests($update_data, $where_data);

            $message = 'IMEI Service orders have been marked to be resubmitted successfully!';
        }

        $strWhere = '';
        $dt = $this->input->post_get('dt') ?: '';
        if($dt != '')
        {
            $strWhere = " AND DATE(RequestedAt) >= '$dt'";
        }
        $this->data['rsNewOrders'] = $this->Services_model->fetch_log_requests_packages_api($strWhere);
       
        $this->data['count'] = count($this->data['rsNewOrders']);
        
        $ARR_API_ERRORS = array();
        
        $rsAPIErrorOrders = $this->Services_model->fetch_log_requests_by_strwhere($strWhere);
        foreach($rsAPIErrorOrders as $row)
        {
                $ARR_API_ERRORS[$row->LogPackageId] = stripslashes($row->MessageFromServer);
        }
        
        $this->data['ARR_API_ERRORS'] = $ARR_API_ERRORS;
        $this->data['dt'] = $dt;
        $this->data['view'] = 'admin/new_server_orders' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function verifyserverorders(){
        $strWhere = '';
        $imei = '';
        $packageId = 0;
        $codeStatusId = 0;
        $searchType = 0;
        $txtlqry = '';
        $sqlwhere = '';
        $verifyPage = '1';
        $page_name = $this->input->server('PHP_SELF');
        $tblOrders = 'tbl_gf_log_requests';
        $searchType = $this->input->post_get('searchType') ? $this->input->post('searchType') : '3';
        $uId =  $this->input->post_get('uId') ?: 0;
        $uName  = '' ;
        if($uId != '0')
        {
            $strWhere .= " AND A.UserId = '$uId'";
            $txtlqry .= "&uId=$uId";
        }

        if(!isset($start))
            $start = 0;

        if($this->input->post("start"))
            $start = $this->input->post("start");

        $eu = ($start - 0);
        $limit = $this->input->post('records') ?: 50;
        $thisp = $eu + $limit;
        $back = $eu - $limit;
        $next = $eu + $limit;
        $message = '';
        if($this->input->post_get('hdsbmt') && $this->session->userdata('GSM_FSN_AdmId'))
        {
            $codeIds = $this->input->post_get('chkCodes') ?: 0;
            $totalCodes = 0;
            if(is_array($codeIds))
                $totalCodes = count($codeIds);

            $codeIds_reply = $this->input->post_get('chkReply') ?: 0;
            $totalCodes_reply = 0;
            if(is_array($codeIds_reply))
                $totalCodes_reply = count($codeIds_reply);
            if($totalCodes_reply == 0)
                $message = $this->lang->line('BE_LBL_577');
            else
            {
                for($i=0; $i < $totalCodes; $i++)
                {
                    $updateStatus = true;
                    $data = explode('|', $codeIds[$i]);
                    $codeId = $data[0];
                    $code = $this->input->post_get('txtCode'.$data[4]);
                    if($codeIds_reply != '0')
                    {
                        if(in_array($codeIds[$i], $codeIds_reply))
                            $codeStatusId = '3';
                        else
                            $updateStatus = false;
                    }
                    else
                        $updateStatus = false;
                    if($updateStatus)
                    {
                        updateserverorderstatus($data , $codeId , $codeStatusId, $code);

                        $update_data = array(
                            'LastUpdatedBy' => $this->session->userdata('AdminUserName'),
                            'Verify' => 2
                        );

                        $where_data = array(
                            LogRequestId => $codeId
                        );
                       $this->Services_model->update_log_requests($update_data, $where_data);
                    }
                }
                for($i=0; $i < $totalCodes_reply; $i++)
                {
                    $data = explode('|', $codeIds_reply[$i]);
                    $codeId = $data[0];
                    $code = $this->input->post_get('txtCode'.$data[4]);
                    $codeStatusId = '2';
                    $updateStatus = true;
                    if($codeIds != '0')
                    {
                        if(in_array($codeIds_reply[$i], $codeIds))
                            $updateStatus = false;
                    }
                    if($updateStatus)
                    {
                        updateserverorderstatus($data , $codeId , $codeStatusId, $code);

                        $update_data = array(
                            'LastUpdatedBy' => $this->session->userdata('AdminUserName'),
                            'Verify' => 2
                        );

                        $where_data = array(
                            LogRequestId => $codeId
                        );
                       $this->Services_model->update_log_requests($update_data, $where_data);
                    }
                }
                $message = $this->lang->line('BE_GNRL_11');
            }
        }

        if($this->input->post_get('txtIMEI'))
        {
            $imei = trim($this->input->post_get('txtIMEI'));
            $packageId =$this->input->post_get('packageId');
            $codeStatusId = $this->input->post_get('codeStatusId');
            $uName = $this->input->post_get('txtUName') ? trim($this->input->post_get('txtUName')) : '';
            if($uName != '')
            {
                $strWhere = " AND UserName LIKE '%".$uName."%'";
                $txtlqry .= "&txtUName=$uName";
            }
            if($imei != '')
            {
                $strWhere = " AND IMEINo LIKE '%".$imei."%'";
                $txtlqry .= "&txtIMEI=$imei";
            }
            if($packageId != '0')
            {
                $strWhere .= " AND A.LogPackageId = '$packageId'";
                $txtlqry .= "&packageId=$packageId";
            }
            if($codeStatusId != '0')
            {
                $strWhere .= " AND A.StatusId = '$codeStatusId'";
                $txtlqry .= "&codeStatusId=$codeStatusId";
            }
        }
        
        $this->data['rsCodes'] = $this->Services_model->fetch_log_requests_packages_users($strWhere , $eu, $limit);
        $this->data['count'] = count($this->data['rsCodes']);

        $this->data['txtlqry'] = $txtlqry;
        $this->data['strWhere'] = $strWhere;
        $this->data['packageId'] = $packageId;
        $this->data['codeStatusId'] = $codeStatusId;
        $this->data['limit'] =  $limit;
        $this->data['thisp'] = $thisp;
        $this->data['back'] = $back;
        $this->data['next'] = $next;
        $this->data['message'] = $message;
        $this->data['uId'] = $uId;
        $this->data['uName'] = $uName;
        $this->data['orderNo'] = '';
        $this->data['dtTo'] = '';
        $this->data['dtFrom'] = '';
        $this->data['myNetworkId'] = '';
        $this->data['view'] = 'admin/verify_server_orders' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function inprocessimeiorders(){
        $strWhere = '';
        $dt = $this->input->post_get('dt') ?: '';
        if($dt != '')
        {
            $strWhere = " AND DATE(RequestedAt) >= '$dt'";
        }
        $this->data['rsNewOrders'] = $this->Services_model->fetch_codes_packages_api($strWhere);
       
        $this->data['count'] = count($this->data['rsNewOrders']);
        $this->data['dt'] = $dt ;
        $this->data['view'] = 'admin/in_process_imeiorders' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }


    public function apiorders(){
        $strWhere = '';
        $imei = '';
        $packageId = 0;
        $codeStatusId = 0;
        $searchType = 0;
        $txtlqry = '';
        $sqlwhere = '';
        $pLast  = '' ;
        $tblOrders = 'tbl_gf_codes';
        $this->data['page_name'] = $this->input->server('PHP_SELF');
        if(!isset($start))
            $start = 0;

        if($this->input->post_get("start"))
            $start = $this->input->post_get("start");

        $eu = ($start - 0);
        $limit = $this->input->post_get('records') ?: 50;
        $thisp = $eu + $limit;
        $back = $eu - $limit;
        $next = $eu + $limit;
        $message = '';

        if($this->input->post_get('hdsbmt') && $this->session->userdata('GSM_FSN_AdmId'))
        {
            $orderIds = $this->input->post('chkOrders') ?: 0;
            $selectedOrders = '0';
            if(is_array($orderIds))
                $selectedOrders = implode(",", $orderIds);
            if($selectedOrders == '0')
            {
                $message = $this->lang->line('BE_LBL_577');
            }
            else
            {
                $update_data = array(
                    'LastUpdatedBy'  => $this->session->userdata('AdminUserName'),
                    'OrderAPIId' => 0 ,
                    'OrderIdFromServer' => '' ,
                    'OrderAPIURL' =>  '' ,
                    'OrderAPIUserName' => '' ,
                    'OrderAPIType' => 0 ,
                    'OrderAPIKey' => '' ,
                    'OrderAPIServiceId' => 0 ,
                    'OrderAPIName' => '' ,
                );

                $where_in = array(
                    'CodeId' => $selectedOrders,
                );
                update_gf_codes($update_data , '' , $where_in);
                $message = $this->lang->line('BE_GNRL_11');
            }
        }
        if($this->input->post_get('txtUName'))
        {
            $packageId = $this->input->post_get('packageId');
            $codeStatusId = $this->input->post_get('codeStatusId');
            $uName = $this->input->post_get('txtUName') ? trim($this->input->post_get('txtUName')) : '';
            if($uName != '')
            {
                $strWhere = " AND UserName LIKE '%$uName%'";
                $txtlqry .= "&txtUName=$uName";
            }
        
            if($packageId != '0')
            {
                $strWhere .= " AND A.PackageId = '$packageId'";
                $txtlqry .= "&packageId=$packageId";
            }
            if($codeStatusId != '0')
            {
                $strWhere .= " AND A.CodeStatusId = '$codeStatusId'";
                $txtlqry .= "&codeStatusId=$codeStatusId";
            }
            if($this->input->post('txtBulkIMEIs'))
            {
                $arrIMEIS = explode("\n", $this->input->post('txtBulkIMEIs'));
                $imeis = '';
                for($i=0; $i<sizeof($arrIMEIS); $i++)
                {
                    if($imeis == '')
                        $imeis .= "'".substr(trim($arrIMEIS[$i]), 0, 15)."'";
                    else
                    {
                        if(trim($arrIMEIS[$i]) != '')
                            $imeis .= ",'".substr(trim($arrIMEIS[$i]), 0, 15)."'";
                    }
                }
                if($imeis != '0')
                    $strWhere .= " AND IMEINo IN (".$imeis.")";
                $txtlqry .= "&blkimeis=$imeis";
            }
        }

        if($this->input->post_get('blkimeis'))
        {
            $strWhere .= " AND IMEINo IN (".$imeis.")";
        }
        
        $this->data['rsCodes'] = $this->Services_model->fetch_codes_packages_users($strWhere ,$eu, $limit);
        $this->data['count'] = count($this->data['rsCodes']);

        $this->data['message'] = $message ;
        $this->data['strWhere'] = $strWhere;
        $this->data['back'] =  $back;
        $this->data['start'] = $start;
        $this->data['txtlqry'] = $txtlqry;
        $this->data['limit'] = $limit;
        $this->data['eu'] = $eu;
        $this->data['pLast'] = $pLast;
        $this->data['thisp'] = $thisp;
        $this->data['next'] = $next;
        $this->data['packageId'] = $packageId;
        $this->data['strPackIds'] = '';
        $this->data['strPckWhere'] = '';
        $this->data['codeStatusId'] = $codeStatusId;
 
        $this->data['view'] = 'admin/apiorders' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }


    public function code(){
        $id = $this->input->post_get('id') ?: 0;
        $modelNo = $imeiNo = $pckTitle = $credits = $notes = $comments = $orderIdFrmServer = $message = '';
        $packageId = $codeStatusId = 0;
        if ($this->input->post('txtCode'))
        {
            $code = $this->input->post('txtCode');
            $notes = $this->input->post('txtNotes');
            $comments = $this->input->post('txtComments');
            $codeStatusId = $this->input->post('codeStatusId');
            $userId = $this->input->post('userId');
            $codeCr = $this->input->post('codeCr');
            $packageId = $this->input->post('packageId');
            $strDeduct = '';
            $currDtTm = setDtTmWRTYourCountry();
            $myIMEI = $this->input->post('imei') ? $this->input->post('imei') : 0;
            $pckTitle = $this->input->post('pckTitle');

            if($codeStatusId == '3')
            {
                if(imeiOrderRefunded($id) == '0')
                {
                    $dec_points = refundIMEICredits($userId, $id, $myIMEI, $pckTitle, $currDtTm, $packageId, $codeCr, '1');
                    $strDeduct = ", Refunded = '1', CheckDuplication = 0";			
                }
            }
            else
            {
                $RESUBMIT = 0;
                if($codeStatusId == '5')
                {
                    $codeStatusId = '4';
                    $RESUBMIT = 1;
                }
                if(imeiOrderRefunded($id) == '1')
                {
                    rebateIMEICredits($userId, $id, $myIMEI, $pckTitle, $currDtTm, $packageId, $codeCr, '1');
                }
                $strDeduct = ", Refunded = '0'";
            }
            $col_val = $historyData = '';
            $ttlFields = $this->input->post("totalCustomFields") ?: '0';
            for($x = 1; $x <= $ttlFields; $x++)
            {
                $col =  $this->input->post('colNm'.$x) ? trim($this->input->post('colNm'.$x)) : '';
                if($col != '')
                {
                    $val = $this->input->post('fld'.$x) ? trim($this->input->post('fld'.$x)) : '';
                    $col_val .= ", $col = '$val'";
                    if($historyData != '')
                        $historyData .= '<br />';
                    $historyData .= $this->input->post['lbl'.$x].": ".$val;
                }
            }

            $this->Services_model->update_code_custom($codeStatusId , $code , $notes , $comments , $historyData , $currDtTm , $strDeduct , $col_val , $id);

            if($codeStatusId == '2' || $codeStatusId == '3' || $RESUBMIT == '1')
            {
                $row = $this->Services_model->fetch_codes_users_status($id);
               
                if (isset($row->UserEmail) && $row->UserEmail != '')
                {
                    if($codeStatusId == '2')
                    {
                        send_push_notification('Your IMEI order has been completed!', $row->UserId);
                        successfulIMEIOrderEmail($row->UserEmail, stripslashes($row->CustomerName), $this->input->post("pckTitle"), $this->input->post('imei'), $code, $row->RequestedAt, 
                                                $packageId, $row->AlternateEmail, $objDBCD14, $codeCr, $row->UserId, $row->Comments, $id);
                        if($row->SMS_User == '1' && $this->input->post("SMS_Pack") == '1' && $row->Phone!= '' && $PHONE_ADMIN != '')
                        {
                            checkAndSendSMS($row->Phone, $PHONE_ADMIN, '7', stripslashes($row->CustomerName), $this->input->post("pckTitle"), $this->input->post('imei'), $code, 
                                            $row->RequestedAt, $row->Comments);
                        }
                    }
                    else if($codeStatusId == '3')
                    {
                        send_push_notification('Your IMEI order has been cancelled!', $row->UserId);
                        rejectedIMEIOrderEmail($row->UserEmail, stripslashes($row->CustomerName), $this->input->post("pckTitle"), $this->input->post('imei'), $code, $row->RequestedAt, $packageId, $row->AlternateEmail, $codeCr, $dec_points, $row->Comments, $id);
                        if($row->SMS_User == '1' && $this->input->post("SMS_Pack") == '1' && $row->Phone!= '' && $PHONE_ADMIN != '')
                        {
                            checkAndSendSMS($row->Phone, $PHONE_ADMIN, '13', stripslashes($row->CustomerName), $this->input->post("pckTitle"), $this->input->post('imei'), $code, 
                                            $row->RequestedAt, $row->Comments);
                        }
                    }
                    if($RESUBMIT == '1')
                    {
                        resubmittedIMEIOrderEmail($row->UserEmail, stripslashes($row->CustomerName), $this->input->post("pckTitle"), $this->input->post('imei'), $row->RequestedAt, $row->AlternateEmail, $row->Comments);
                        if($row->SMS_User == '1' && $_POST["SMS_Pack"] == '1' && $row->Phone!= '' && $PHONE_ADMIN != '')
                        {
                            checkAndSendSMS($row->Phone, $PHONE_ADMIN, '13', stripslashes($row->CustomerName), $this->input->post("pckTitle"), $this->input->post('imei'), $code, 
                                            $row->RequestedAt, $row->Comments);
                        }
                    }
                }
            }
            $message = $this->lang->line('BE_GNRL_11');
        }

        if($id > 0)
        {
            $ROW_IMEI_ORDER = $this->Services_model->users_codes_packages_api($id); 
            
            if (isset($ROW_IMEI_ORDER->UserId) && $ROW_IMEI_ORDER->UserId != '')
            {
                $this->data['modelNo'] = $ROW_IMEI_ORDER->ModelNo == '' ? $ROW_IMEI_ORDER->Model : $ROW_IMEI_ORDER->ModelNo;
                $this->data['imeiNo'] = $ROW_IMEI_ORDER->IMEINo;
                $this->data['userId'] = $ROW_IMEI_ORDER->UserId;
                $this->data['code'] = stripslashes($ROW_IMEI_ORDER->Code);
                $this->data['pckTitle'] = stripslashes($ROW_IMEI_ORDER->PackageTitle);
                $this->data['codeStatusId'] = $ROW_IMEI_ORDER->CodeStatusId;
                $this->data['credits'] = $ROW_IMEI_ORDER->Credits;
                $this->data['notes'] = stripslashes($ROW_IMEI_ORDER->Comments);
                $this->data['comments'] = stripslashes($ROW_IMEI_ORDER->Comments1);
                $this->data['packageId'] = $ROW_IMEI_ORDER->PackageId;
                $this->data['serialNo'] = $ROW_IMEI_ORDER->SerialNo;
                $this->data['other'] = $ROW_IMEI_ORDER->OtherValue;
                $this->data['userName'] = $ROW_IMEI_ORDER->UserName;
                $this->data['prd'] = $ROW_IMEI_ORDER->PRD;
                $this->data['SMS_Pack'] = $ROW_IMEI_ORDER->SendSMS;
                $this->data['orderDt'] = $ROW_IMEI_ORDER->RequestedAt;
                $this->data['replyDtTm'] = $ROW_IMEI_ORDER->ReplyDtTm;
                $this->data['orderIdFrmServer'] = $ROW_IMEI_ORDER->OrderIdFromServer;
                $this->data['msg4mSrvr'] = $ROW_IMEI_ORDER->MessageFromServer;
                $this->data['sent2OtherSrvr'] = $ROW_IMEI_ORDER->CodeSentToOtherServer;
                $this->data['apiName'] = stripslashes($ROW_IMEI_ORDER->OrderAPIName);
                $this->data['updatedBy'] = stripslashes($ROW_IMEI_ORDER->LastUpdatedBy);
            }
            $this->data['rsFields'] = $this->Services_model->fetch_custom_fields();
            
        }

        $this->data['id'] = $id ; 
        $this->data['message'] =  $message;

        $this->data['view'] = 'admin/code' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function pendingorders(){
        $strWhere = '';
        $imei = '';
        $packageId = 0;
        $codeStatusId = 0;
        $searchType = 0;
        $txtlqry = '';
        $sqlwhere = '';
        $strCustomCols = '' ;
        $page_name = $this->input->server('PHP_SELF');
        if(!isset($start))
            $start = 0;

        if($this->input->post_get("start"))
            $start = $this->input->post_get("start");

        $eu = ($start - 0);
        $limit = $this->input->post_get('records') ?: 100;
        $sc = $this->input->post_get('sc') ?: 0;
        $packId = $this->input->post_get('packId') ?: 0;

        $thisp = $eu + $limit;
        $back = $eu - $limit;
        $next = $eu + $limit;
        $message = '';
        $lastAPIId = 0;
        $lastAPIServiceId = 0;
        $colIndex = '' ;
        $IMEINo = '' ; 


        switch($sc)
        {
            case '0';
                $tblName = 'tbl_gf_codes';
                $statusCol = 'CodeStatusId';
                $serviceCol = 'PackageId';
                $tblPack = 'tbl_gf_packages';
                $packCol = 'PackageTitle';
                $idCol = 'CodeId';
                break;
            case '1';
                $tblName = 'tbl_gf_codes_slbf';
                $statusCol = 'CodeStatusId';
                $serviceCol = 'PackageId';
                $tblPack = 'tbl_gf_packages';
                $packCol = 'PackageTitle';
                $idCol = 'CodeId';
                break;
            case '2';
                $tblName = 'tbl_gf_log_requests';
                $statusCol = 'StatusId';
                $serviceCol = 'LogPackageId';
                $tblPack = 'tbl_gf_log_packages';
                $packCol = 'LogPackageTitle';
                $idCol = 'LogrequestId';
                break;
        }

        if($packId != '0' && $packId != '')
        {
            $strWhere .= " AND A.$serviceCol = '$packId'";
            $txtlqry .= "&packId=$packId";
        }


        if($this->input->post_get('hdsbmt') && $this->session->userdata('GSM_FSN_AdmId'))
        {
            $orderIds = $this->input->post_get('chkOrders') ?: 0;
            $apiId = $this->input->post('apiId') ?: 0;
            $supplierPackId = $this->input->post('supplierPackId') ?: 0;
            $selectedOrders = '0';
            if(is_array($orderIds))
                $selectedOrders = implode(",", $orderIds);
            $submit = true;
            if($selectedOrders == '0')
            {
                $message = $this->lang->line('BE_LBL_577');
                $submit = false;
            }
            if($apiId == '0')
            {
                $message .= '<br />Please select API';
                $submit = false;
            }
            if($supplierPackId == '0')
            {
                $message .= '<br />Please select supplier service';
                $submit = false;
            }
            if($submit)
            {
                $rsAPI = $this->Services_model->fetch_api_data($apiId);
               
                if (isset($rsAPI->APIId) && $rsAPI->APIId != '')
                {
                    $apiType = $rsAPI->APIType;
                    $apiKey = $rsAPI->APIKey;
                    $serverURL = $rsAPI->ServerURL;
                    $apiUserName = $rsAPI->AccountId;
                    $apiName = stripslashes($rsAPI->APITitle);

                    $this->Services_model->pending_order_update_general_query($tblName , $apiId , $serverURL ,$apiUserName , $apiType , $apiKey , $supplierPackId , $apiName , $idCol , $selectedOrders);
                  
                    
                    $message = $this->lang->line('BE_GNRL_11');

                    //=================================== CREATING API HISTORY ======================================//
                    if($apiId > 0)
                    {
                        $currDtTm = setDtTmWRTYourCountry();
                        serviceAPIHistory($packId, $apiId, $supplierPackId, $currDtTm, $sc);
                    }
                    //=================================== CREATING API HISTORY ======================================//
                }
            }
        }

        $CUST_COL_LBL = array();
        $rsFields = $this->Services_model->fetch_custom_fields_by_type($sc);
        
        foreach($rsFields as $row)
        {
            $strCustomCols .= ', '.$row->FieldColName.' AS Col'.$colIndex;
            $CUST_COL_LBL['Col'.$colIndex] = $row->FieldLabel;
            $colIndex++;
        }
        if($sc == '0')
            $strCustomCols  .= ', IMEINo';
        $this->data['rsCodes'] = $this->Services_model->fetch_pending_orders_custom_data($serviceCol ,$idCol ,  $packCol ,$strCustomCols ,$tblName ,$tblPack, $statusCol , $strWhere , $eu , $limit);
    
        $this->data['count'] = count($this->data['rsCodes']);

        $row = $this->Services_model->fetch_custom_pending_tbl_users($idCol , $tblName , $statusCol , $strWhere) ;
        
        $this->data['totalRows'] = $row->TotalRecs;
        $this->data['message'] = $message;
        $this->data['back'] = $back;
        $this->data['start'] = $start;
        $this->data['txtlqry'] = $txtlqry;
        $this->data['limit'] = $limit;
        $this->data['eu'] = $eu;
        $this->data['pLast'] = '';
        $this->data['thisp'] = $thisp;
        $this->data['next'] = $next;
        $this->data['packId'] = $packId;
        $this->data['sc'] = $sc;
        $this->data['CUST_COL_LBL'] = $CUST_COL_LBL;
        $this->data['colIndex'] = $colIndex;
        $this->data['IMEINo'] = $IMEINo;
        $this->data['lastAPIId'] = $lastAPIId;
      
        $this->data['view'] = 'admin/pending_orders';
        $this->load->view('admin/layouts/default1' , $this->data);
    }


    public function ipserverorders(){
        $strWhere = '';
        $dt = $this->input->post_get('dt') ?: '';
        if($dt != '')
        {
            $strWhere = " AND DATE(RequestedAt) >= '$dt'";
        }
        $this->data['rsNewOrders'] = $this->Services_model->fetch_log_requests_packages_api_by_statusId($strWhere);
      
        $this->data['count'] = count($this->data['rsNewOrders']);
        $this->data['dt'] = $dt;
        $this->data['view'] = 'admin/ipserverorders' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }


    //Left bar started 

    public function services(){
        $eu = 0;
        $txtlqry = '';
        $strPackIds = '';
        if(!isset($start))
            $start = 0;

        if($this->input->post_get("start"))
            $start = $this->input->post_get("start");

        $eu = ($start - 0);
        $limit = $this->input->post_get('records') ?: 100;
        $txtlqry .= "&records=$limit";
        $thisp = $eu + $limit;
        $back = $eu - $limit;
        $next = $eu + $limit;

        $message = '';
        $strWhere = " AND sl3lbf = 0";
        $strWhereCat = " AND SL3BF = 0";
        $del = $this->input->post_get('del') ? 1 : 0;
        $fs = $this->input->post_get('fs') ?: 0;
        $deleteKey = '322';
        if($fs == '1')
            $deleteKey = '323';
        $dis = $this->input->post_get('dis') ?: 0;
        $cldFrm = $this->input->post_get('cldFrm') ?: 0;
        $categoryId = $this->input->post_get('categoryId') ?: 0;
        $packageId = $this->input->post_get('packageId') ?: 0;
        $apiId = $this->input->post_get('apiId') ?: 0;
        $srvStatus = $this->input->post_get('srvStatus') ?: '0';
        $txtlqry .= "&dis=$dis";
        $strPckWhere = ' AND Cat.SL3BF = 0 AND A.sl3lbf = 0';

        if($fs == '1')
        {
            $strWhere = " AND sl3lbf = 1";
            $strWhereCat = " AND SL3BF = 1";
            $txtlqry .= "&fs=$fs";
            $strPckWhere = ' AND Cat.SL3BF = 1 AND A.sl3lbf = 1';
        }
        
        if($categoryId != '0')
        {
            $strWhere .= " AND A.CategoryId = '$categoryId'";
            $txtlqry .= "&categoryId=$categoryId";
        }
        if($packageId != '0')
        {
            $strWhere .= " AND A.PackageId = '$packageId'";
            $txtlqry .= "&packageId=$packageId";
        }
        if($apiId != '0')
        {
            $strWhere .= " AND A.APIId = '$apiId'";
            $txtlqry .= "&apiId=$apiId";
        }
        if($srvStatus != '2')
        {
            $strWhere .= " AND A.DisablePackage = '$srvStatus'";
            $txtlqry .= "&srvStatus=$srvStatus";
        }
        if($this->input->post_get('txtService'))
        {
            $service = trim($this->input->post_get('txtService'));
            if($service != '')
            {
                $strWhere .= " AND PackageTitle LIKE '%$service%'";
                $txtlqry .= "&txtService=$service";
            }
        }
        if($del == '1')
        {
            $packId = $this->input->post_get('packId') ?: 0;
            deletePack($packId);
            $message = 'Service has been archived successfully!';
        }

        if($this->input->post('chkPacks') && $this->session->userdata('GSM_FSN_AdmId'))
        {
            $selectedPackages = '0';
            $packageIds = $this->input->post('chkPacks') ?: 0;
            if(is_array($packageIds))
                $selectedPackages = implode(",", $packageIds);
            if($cldFrm == '1')
            {
                $currpackIds = $this->input->post_get('currPcks') ?: 0;
                $currDtTm = setDtTmWRTYourCountry();

                $update_data = array(
                    'DisablePackage' =>  0 ,
                    'EditedAt' => $currDtTm,

                );

                $where_in = array(
                    'PackageId' => $currpackIds
                );

                $this->Services_model->update_packages($update_data, '' , $where_in);
                
                $update_data = array(
                    'DisablePackage' =>  1 ,
                    'EditedAt' => $currDtTm,
                    
                );
                
                $where_in = array(
                    'PackageId' => $selectedPackages
                );
                
                $this->Services_model->update_packages($update_data, '' , $where_in);
               
                
                foreach($this->input->post('packagePrice') as $key=>$val)
                {
                    if(is_float($val[0]) || is_numeric($val[0]))
                    {
                        $packagePrice = $val[0];

                        $update_data = array(
                            'PackagePrice' => $packagePrice  ,
                            'EditedAt' => $currDtTm,
                            
                        );
                        
                        $where_data = array(
                            'PackageId' => $key
                        );

                        $this->Services_model->update_packages($update_data, $where_data);
                    }
                }
                $message = $this->lang->line('BE_GNRL_11');
            }
            else if($cldFrm == '2'  && is_array($packageIds))
            {
                $this->Services_model->delete_data_user_packages($selectedPackages);
           
                $message = 'Selected services have been allowed for all users.';
            }
            else if($cldFrm == '3'  && is_array($packageIds))
            {
                $strQry = '';
                $strPacks = '';
                foreach($packageIds as $key => $val)
                {
                    if($strPacks != '')
                        $strPacks .= ',';
                    $strPacks .= "('USER-ID', '".$val."', '0')";
                }
                if($strPacks != '')
                {
                    $rsUsers = $this->Services_model->fetch_users();
                  
                    foreach($rsUsers as $rwU)
                    {
                        if($strQry != '')
                            $strQry .= ',';
                        $strQry .= str_replace('USER-ID', $rwU->UserId, $strPacks);
                    }
                    if($strQry != '')
                    {

                        $this->Services_model->delete_data_user_packages($selectedPackages);
                        $this->Services_model->insert_user_packages($strQry);
                      
                    }
                }
                $message = 'Selected services have been blocked for all users.';
            }
            
            else if($cldFrm == '4' && is_array($packageIds))
            {
                //HIDE FOR GROUP
                $groupId = $this->input->post_get('groupId') ?: 0;
                if($groupId > 0)
                {
                    $strQry = '';
                    $strPacks = '';
                    foreach($packageIds as $key => $val)
                    {
                        if($strPacks != '')
                            $strPacks .= ',';
                        $strPacks .= "('USER-ID', '".$val."', '0')";
                    }
                    if($strPacks != '')
                    {
                        $strUserIds = '';
                        $rsUsers = $this->Services_model->fetch_users_by_archived($groupId); 
                        
                        foreach($rsUsers as $rwU)
                        {
                            if($strQry != '')
                                $strQry .= ',';
                            if($strUserIds == '')
                                $strUserIds = $rwU->UserId;
                            else
                                $strUserIds = $strUserIds.','.$rwU->UserId;
                            $strQry .= str_replace('USER-ID', $rwU->UserId, $strPacks);
                        }
                        if($strQry != '')
                        {
                            $this->Services_model->delete_user_packages_by_userids($selectedPackages , $strUserIds);
                           
                            $this->Services_model->insert_user_packages($strQry);
                        }
                    }
                    $message = 'Selected services have been blocked for the selected group.';
                }
                else
                    $message = 'Please select Group';
            }
            else if($cldFrm == '5' && is_array($packageIds))
            {
                //SHOW FOR GROUP
                $groupId = $this->input->post('groupId') ?: 0;
                if($groupId > 0)
                {
                    $strUserIds = '';
                    $rsUsers =  $this->Serices_model->fetch_users_by_price_plan_id($groupId);
                   
                    foreach($rsUsers as $rwU)
                    {
                        if($strUserIds == '')
                            $strUserIds = $rwU->UserId;
                        else
                            $strUserIds = $strUserIds.','.$rwU->UserId;
                    }
                    if($strUserIds != '')
                    {
                        $this->Services_model->delete_user_packages_by_userids($selectedPackages , $strUserIds);
                    }
                    $message = 'Selected services have been allowed for the selected group.';
                }
                else
                    $message = 'Please select Group';
            }
        }
        $this->data['rsPacks'] = $this->Services_model->fetch_package_category_api($strWhere ,$eu, $limit);
       
        $this->data['count'] = count($this->data['rsPacks']);


        $this->data['strWhereCat'] = $strWhereCat;
        $this->data['deleteKey'] = $deleteKey;
        $this->data['dis'] = $dis;
        $this->data['fs'] = $fs ;
        $this->data['page_name'] = '';
        $this->data['back'] = $back;
        $this->data['start'] = $start;
        $this->data['txtlqry'] = $txtlqry;
        $this->data['limit'] = $limit;
        $this->data['eu'] = $eu;
        $this->data['pLast'] = '';
        $this->data['thisp'] = $thisp;
        $this->data['next'] = $next;
        $this->data['categoryId'] = $categoryId;
        $this->data['apiId'] =$apiId;
        $this->data['strPckWhere'] = $strPckWhere;
        $this->data['strPackIds'] = $strPackIds;
        $this->data['packageId'] = $packageId;
        $this->data['srvStatus'] = $srvStatus;
        $this->data['strWhere'] = $strWhere;
        $this->data['message'] = $message ;
        $this->data['view'] = 'admin/services';
        $this->load->view('admin/layouts/default1' , $this->data);
    }


    public function package(){
        $id = $this->input->post_get('id') ?: 0;
        $fs = $this->input->post_get('fs') ?: 0;
        $applyAPI = $this->input->post_get('applyAPI') ?: 0;
        $categoryId = $this->input->post('categoryId') ?: 0;
        $price = $this->input->post('txtPrice') ?: '0';
        $apiType =  $seriesId = $sendSMS = $calPreCodes =  $fName =  $metaTags = $seoName = $metaKW =$htmlTitle = $updatedAt = $preCodes = $packageTitle = $extNetworkId = $existingImage = $message = $toolForUB = $mustRead = $timeTaken = $redirect = $costPrice = $supplier = $emailIDs = $toc = '';
        $onlyAPIId = $disablePackage = $apiId = $extNtwkId = $newTplType = $successTplType = $cancelledTplType = $apiIdForBrand = $customFldId = $srvType = $imeiFType = $verifyOrders = $costPrFrmAPI = $dupIMEI = $cancelOrders = 0;
        $verifyMins = $cancelMins = '60';
        $responseDelayTm = $cronDelayTm = '1';
        $del = $this->input->post_get('del') ? 1 : 0;

        if($del == '1' && $id > 0)
        {
            $PMAPIId = $this->input->post_get('PMAPIId') ?: 0;
            $this->Services_model->del_service_api_pricing($id ,$PMAPIId , $fs);	
           
            $message = $this->lang->line('BE_GNRL_12');


        }

        if (isset($_POST['existingImage']))
        {
            $packageTitle = $this->input->post('txtTitle');
          
            $row = $this->Services_model->fetch_pacakges($packageTitle , $fs , $id);
            
            if (isset($row->PackageId) && $row->PackageId != '')
            {
                $message = "'$packageTitle'".' '.$this->lang->line('BE_GNRL_10');
            }
            else
            {
                if($this->input->post('dupSrvcId') && $this->input->post('dupSrvcId') > 0)
                {
                    $id = replicateService($this->input->post('dupSrvcId'), $packageTitle);
                    $message = 'Service has been replicated successfully';
                }
                else
                {


                    require(APPPATH.'libraries/FileManager.php');
                    $filemanager = new FileManager();
                    $fileExt = $filemanager->getFileExtension("txtImage");
                    if($fileExt == '.JPG' || $fileExt == '.jpg' || $fileExt == '.jpeg' || $fileExt == '.png' || $fileExt == '.PNG'  || $fileExt == '.gif' || $fileExt == '')
                    {
                        $timeTaken = $this->input->post('txtTimeTaken');
                        
                        $mustRead = stripslashes($this->input->post('txtMustRead'));
                        
                        $extNetworkId = $this->input->post('supplierPackId') != '' ?: 0;
                        $costPrice = $this->input->post('txtCostPrice') ?: '0';
                        if($costPrice == '')
                            $costPrice = '0';
                        $supplier = $this->input->post('txtSupplier') ?: '';
                        $emailIDs = $this->input->post('txtEmails') ?: '';
                        
                        $dupIMEI = $this->input->post('dupIMEI') ? 1 : 0;
                        $toc = $this->input->post('chkTOC') ? 1 : 0;
                        $costPrFrmAPI = $this->input->post('chkFetchCPr') ? 1 : 0;
                        
                        $arrAPIId = explode('~', $this->input->post('apiId'));
                        $apiId = $arrAPIId[0] == '' ? 0 :$arrAPIId[0];
                        $exAPIId = $this->input->post('exAPIId');
                        $exAPIServiceId = $this->input->post('exAPIServiceId');
                        $existingImage = $this->input->post('existingImage');
                        $disablePackage = $this->input->post('chkDisablePackage') ? 1 : 0;
                        $metaTags = $this->input->post('txtMetaTags');
                        $seoName = $this->input->post('txtSEOName');
                        $htmlTitle = $this->input->post('txtHTMLTitle');
                        $metaKW = $this->input->post('txtMetaKW');
                        $fName = $this->input->post('txtFileName');
                        $imeiFType = $this->input->post('rdIMEIFType');
                        if($imeiFType == '3')
                            $customFldId = $this->input->post('customFldId');
                        $newTplType = $this->input->post('rdNewTplType') ?: 0;
                        $sucTplType = $this->input->post('rdSuccessTplType') ?: 0;
                        $canTplType = $this->input->post('rdCanTplType') ?: 0;

                        $newTplSbj = $this->input->post('txtNewTplSbj') ?: '';
                        $newTplBody = $this->input->post('txtNewTplBody') ?: '';
                        $sucTplSbj = $this->input->post('txtSucTplSbj') ?: '';
                        $sucTplBody = $this->input->post('txtSucTplBody') ?: '';
                        $canTplSbj = $this->input->post('txtCanTplSbj') ?: '';
                        $canTplBody = $this->input->post('txtCanTplBody') ?: '';
                        $newSendCopy = $this->input->post('chkNewSendCopy') ? 1 : 0;
                        $sucSendCopy = $this->input->post('chkSucSendCopy') ? 1 : 0;
                        $canSendCopy = $this->input->post('chkCanSendCopy') ? 1 : 0;
                        $calPreCodes = $this->input->post('chkPreCodes') ? 1 : 0;
                        $sendSMS = $this->input->post('chkSMS') ? 1 : 0;
                        $srvType = $this->input->post('rdServiceType');
                        if($srvType == '0')
                            $responseDelayTm = $this->input->post('txtResDelayTm');
                        $cronDelayTm = $this->input->post('rdSrvcAPIType');
                        $cancelOrders = $this->input->post('cancelOrders') ? 1 : 0;
                        $verifyOrders = $this->input->post('verifyOrders') ? 1 : 0;
                        $cancelMins = $this->input->post('txtCancelTime') && $this->input->post('txtCancelTime') != '' ? $this->input->post('txtCancelTime') : '60';
                        $verifyMins = $this->input->post('txtVerifyTime') && $this->input->post('txtVerifyTime') != '' ? $this->input->post('txtVerifyTime') : '60';
                        $redirect = $this->input->post('txtRedirection') ? $this->input->post('txtRedirection') : '';
                        $seriesId = $this->input->post('seriesId');

                        $file = '';
                        $currDtTm = setDtTmWRTYourCountry();
                        if($id == 0)
                        {
                            
                            if($newTplType == '0')
                            {
                                $newTplSbj = '';
                                $newTplBody = '';
                                $newSendCopy = 0;
                            }
                            if($sucTplType == '0')
                            {
                                $sucTplSbj = '';
                                $sucTplBody = '';
                                $sucSendCopy = 0;
                            }
                            if($canTplType == '0')
                            {
                                $canTplSbj = '';
                                $canTplBody = '';
                                $canSendCopy = 0;
                            }
                            $insert_data = array(
                                'PackageTitle' => $packageTitle,
                                'PackagePrice' => $price,
                                'ExternalNetworkId' =>  $extNetworkId,
                                'APIId' => $apiId,
                                'CategoryId' => $categoryId,
                                'TimeTaken' => $timeTaken,
                                'MustRead' => $mustRead,
                                'sl3lbf' => $fs ,
                                'DisablePackage' => $disablePackage ,
                                'ToolForUnlockBase' =>  $toolForUB,
                                'DuplicateIMEIsNotAllowed' => $dupIMEI,
                                'SEOURLName' => $seoName,
                                'MetaKW' => $metaKW ,
                                'HTMLTitle' => $htmlTitle,
                                'FileName' => $fName,
                                'MetaTags' => $metaTags,
                                'IMEIFieldType' => $imeiFType ,
                                'NewTplType' => $newTplType ,
                                'SuccessTplType' => $sucTplType ,
                                'CancelledTplType' => $canTplType,
                                'NewTplSubject' => $newTplSbj,
                                'NewTplBody' => $newTplBody,
                                'SuccessTplSubject' => $sucTplSbj,
                                'SuccessTplBody' => $sucTplBody,
                                'CancelTplBody' => $canTplSbj,
                                'SendNewCopyToAdmin' => $newSendCopy ,
                                'SendSuccessCopyToAdmin' => $sucSendCopy,
                                'SendRejectCopyToAdmin' => $canSendCopy,
                                'CostPrice' => $costPrice,
                                'EditedAt' =>  $currDtTm,
                                'Supplier' => $supplier,
                                'CalculatePreCodes' => $calPreCodes,
                                'SendSMS' => $sendSMS,
                                'CustomFieldId' => $customFldId,
                                'ServiceType' => $srvType,
                                'ResponseDelayTm' => $responseDelayTm,
                                'CronDelayTm' => $cronDelayTm,
                                'CancelOrders' => $cancelOrders,
                                'VerifyOrders' => $verifyOrders,
                                'OrderCancelMins' => $cancelMins,
                                'OrderVerifyMins' => $verifyMins,
                                'RedirectionURL' => $redirect,
                                'TOCs' => $toc,
                                'CostPriceFromAPI' => $costPrFrmAPI,
                                'NewOrderEmailIDs' => $emailIDs,
                                'SeriesId' => $seriesId
                            );

                           

                            $id = $this->Services_model->insert_packages_data($insert_data);
                            
                          
                           
                            //=================================== CREATING API HISTORY ======================================//
                            if($apiId > 0 && ($exAPIId != $apiId || $exAPIServiceId != $extNetworkId))
                            {
                                serviceAPIHistory($id, $apiId, $extNetworkId, $currDtTm, $fs);
                            }
                            //=================================== CREATING API HISTORY ======================================//
                            
                            if ($filemanager->getFileName('txtImage') != '')
                                $file =	'packages/'.$id.$filemanager->getFileExtension("txtImage");
                            $message = $this->lang->line('BE_GNRL_11');
                        }			
                        else
                        {
                            
                            $strTpls = '';
                            if ($filemanager->getFileName('txtImage') != '')
                                $file =	'packages/'.$id.$filemanager->getFileExtension("txtImage");
                            if($newTplType == '1')
                            {
                                $strTpls .= ", NewTplSubject = '$newTplSbj', NewTplBody = '$newTplBody', SendNewCopyToAdmin = '$newSendCopy'";
                            }
                            if($sucTplType == '1')
                            {
                                $strTpls .= ", SuccessTplSubject = '$sucTplSbj', SuccessTplBody = '$sucTplBody', SendSuccessCopyToAdmin = '$sucSendCopy'";
                            }
                            if($canTplType == '1')
                            {
                                $strTpls .= ", CancelTplSubject = '$canTplSbj', CancelTplBody = '$canTplBody', SendRejectCopyToAdmin = '$canSendCopy'";
                            }

                            $this->Services_model->update_packages_general_query($packageTitle ,$price ,$extNetworkId ,$apiId ,$categoryId ,$fs ,$timeTaken ,$mustRead ,$metaKW ,$disablePackage , $dupIMEI ,$toolForUB ,$metaTags ,$seoName ,$htmlTitle ,$imeiFType ,$fName ,$redirect,$srvType ,$newTplType , $sucTplType ,$seriesId ,$canTplType ,$costPrice ,$customFldId ,$currDtTm ,$supplier , $calPreCodes , $sendSMS ,$responseDelayTm ,$cronDelayTm , $cancelOrders ,$verifyOrders ,$emailIDs ,$cancelMins , $verifyMins , $toc ,$costPrFrmAPI , $strTpls ,$id);
                           

                            //============================ APPLY API ON PENDING ORDERS ==================================//
                            if($apiId > 0)
                            {
                                applyAPIOnPendingOrders($fs, $apiId, $extNetworkId, $id);
                            }
                            //============================ APPLY API ON PENDING ORDERS ==================================//
                            //=================================== CREATING API HISTORY ======================================//
                            if($apiId > 0 && ($exAPIId != $apiId || $exAPIServiceId != $extNetworkId))
                            {
                                serviceAPIHistory($id, $apiId, $extNetworkId, $currDtTm, $fs);
                            }
                            //=================================== CREATING API HISTORY ======================================//

                            if($this->input->post('chkDelImg'))
                            {
                                if($existingImage != '')
                                {
                                    $update_data = array(
                                        'PackageImage' => '' 
                                    );

                                    $where_data = array(
                                        'PackageId' => $id
                                    );
                                    $this->Services_model->update_packages($update_data, $where_data);
                                    @unlink("../$existingImage");
                                }
                            }
                        }
                        if($file != '')
                        {
                            $update_data = array(
                                'PackageImage' => $file 
                            );

                            $where_data = array(
                                'PackageId' => $id
                            );
                            $this->Services_model->update_packages($update_data, $where_data);
                           
                        }
                        if ($filemanager->getFileName('txtImage') != '')
                        {
                            if($existingImage != '')
                                @unlink("../$existingImage");
                            $filemanager->uploadAs('txtImage', APPPATH."uplds/$THEME/$file");
                        }
                        $totalCurrencies = $this->input->post('totalCurrencies');
                        $strCurrencies = '';
                        $strDefPriceNotice = '';
                        $defaultCurrencyId = $this->input->post('defaultCurrId');
                        $strDefPriceNotice = "('$defaultCurrencyId', '$id', '$packageTitle', '$price', '$fs', 'USER_ID')";
                        for($j = 0; $j < $totalCurrencies; $j++)
                        {
                            if($j > 0)
                                $strCurrencies .= ',';
                            $cPrice = trim($this->input->post('txtCurrPrice'.$j));
                            if(!is_numeric($this->input->post('txtPrice'.$j)))
                            {
                                if($cPrice == ''){
                                    $cPrice = 0;
                                }
                                $strCurrencies .= "('$id', '".$this->input->post('currencyId'.$j)."', '$cPrice')";

                                $strDefPriceNotice .= ", ('".$this->input->post('currencyId'.$j)."', '$id', '$packageTitle', '$cPrice', '$fs', 'USER_ID')";
                            }
                        }
                        if($strCurrencies != '')
                        {
                            $this->Services_model->del_packages_currencies($id);
                            $this->Services_model->insert_packages_currencies($strCurrencies);
                           
                        }

                        if($strDefPriceNotice != '')
                        {
                           
                            $rsPlanClients =  $this->Services_model->fetch_users_packages_prices($id);
                            $strDefPriceNoticeNew = '';
                            foreach($rsPlanClients as $row)
                            {
                                if($strDefPriceNoticeNew != '')
                                    $strDefPriceNoticeNew .= ',';
                                $strDefPriceNoticeNew .= str_replace('USER_ID', $row->UserId, $strDefPriceNotice);
                            }
                            if($strDefPriceNoticeNew != '')
                            {
                                $this->Services_model->del_default_prices_notice($fs , $id);
                                $this->Services_model->insert_prices_notice($strDefPriceNoticeNew);
                                
                                
                            }
                        }

                        //========================================== ADDING CUSTOM FIELDS ==========================================//
                       
                        $this->Services_model->del_package_custom_fields($id , $fs);
                        if($this->input->post('customFlds'))
                        {
                            if(is_array($this->input->post('customFlds')))
                            {
                                $selectedIds = sizeof($this->input->post('customFlds'));
                                $strData = '';
                                for($k = 0; $k < $selectedIds; $k++)
                                {
                                    if($k > 0)
                                        $strData .= ',';
                                    $strData .= "(".$id.",".$this->input->post('customFlds')[$k].", '$fs')";
                                }
                                if($strData != '')
                                {
                                    $this->Services_model->insert_package_custom_fields($strData);
                                   
                                }
                            }
                        }
                        //========================================== ADDING CUSTOM FIELDS ==========================================//
                        //========================================== SAVING GROUP PRICES ==========================================//
                        if($this->input->post('hdSavePrices') && $this->input->post('hdSavePrices') == '1')
                        {
                            if($this->input->post('totalGroups') && $this->input->post('totalGroups') > 0)
                            {
                                $str = '';
                                $tGroups = $this->input->post('totalGroups');
                                $currencyCount = $this->input->post('currencyCount');
                                $strPrices = '';
                                $strPriceNotice = '';
                                for($j = 0; $j < $tGroups; $j++)
                                {
                                    $dfPrice = trim($this->input->post('txtPrice'.$j.'_0'));
                                    if(is_numeric($price) && $defaultCurrencyId != '' && $price > 0)
                                    {
                                        if($str != '')
                                            $str .= ',';
                                        if($strPrices != '')
                                            $strPrices .= ',';
                                        if($strPriceNotice != '')
                                            $strPriceNotice .= ',';
                                        $str .= "('$id', '$defaultCurrencyId', '".$this->input->post('planId'.$j)."','$dfPrice', '$fs')";
                                        $strPrices .= "('".$this->input->post('planId'.$j)."', '$defaultCurrencyId', '$id', '$packageTitle', '$dfPrice', '$fs')";
                                        $strPriceNotice .= "('".$this->input->post('planId'.$j)."', '$defaultCurrencyId', '$id', '$packageTitle', '$dfPrice', '$fs', 'USER_ID')";
                                    }
                                    for($z = 1; $z <= $currencyCount; $z++)
                                    {
                                        $price = trim($this->input->post('txtOtherPrice'.$j.'_'.$z));
                                        if(is_numeric($price) && $price > 0)
                                        {
                                            if($str != '')
                                                $str .= ',';
                                            if($strPrices != '')
                                                $strPrices .= ',';
                                            if($strPriceNotice != '')
                                                $strPriceNotice .= ',';
                                            $str .= "('$id', '".$this->input->post('otherCurrencyId'.$j.'_'.$z)."', 
                                                '".$this->input->post('planId'.$j)."', '$price', '$fs')";
                                            $strPrices .= "('".$this->input->post('planId'.$j)."', '".$this->input->post('otherCurrencyId'.$j.'_'.$z)."', 
                                                        '$id', '$packageTitle', '$price', '$fs')";
                                            $strPriceNotice .= "('".$this->input->post('planId'.$j)."', '".$this->input->post('otherCurrencyId'.$j.'_'.$z)."', '$id', '$packageTitle', '$price', '$fs', 'USER_ID')";
                                        }
                                    }
                                }
                                if($str != '')
                                {
                                  
                                    $this->Services_model->del_plans_packages_prices($id , $fs);

                                    $this->Services_model->insert_plans_packages_prices($str);
                                    
                                }
                                if($strPrices != '' && $SEND_PRICE_EMAILS == '1')
                                {
                                    $this->Services_model->del_plans_prices_email($planId ,$fs ,$id);
                                    $this->Services_model->insert_plans_prices_email($strPrices);
                                    
                                }
                                if($strPriceNotice != '')
                                {
                                   
                                    $rsPlanClients =  $this->Services_model->fetch_userIds();
                                    $strPriceNoticeNew = '';
                                    foreach($rsPlanClients as $row)
                                    {
                                        if($strPriceNoticeNew != '')
                                            $strPriceNoticeNew .= ',';
                                        $strPriceNoticeNew .= str_replace('USER_ID', $row->UserId, $strPriceNotice);
                                    }
                                    if($strPriceNoticeNew != '')
                                    {
                                        $this->Services_model->del_group_prices_notice($fs ,$id);
                                        $this->Services_model->insert_group_prices_notice($strPriceNoticeNew);
                                     
                                       
                                    }
                                }
                            }
                        }
                        //========================================== SAVING GROUP PRICES ==========================================//
                        //=================================== RESET SERVICE DISCOUNTS ======================================//
                        if($this->input->post('chkResetDiscounts'))
                        {
                            $this->Services_model->del_users_packages_prices($id);
                          
                        }
                        //=================================== RESET SERVICE DISCOUNTS ======================================//

                        //=================================== PRE CODES ======================================//
                        $arrPreCodes = explode("\n", $this->input->post("txtPreCodes"));
                        $strPreCodes = '';
                        foreach($arrPreCodes as $key => $value)
                        {
                            if($value != '')
                            {
                                if($strPreCodes != '')
                                    $strPreCodes .= ',';
                                $strPreCodes .= "('$id', '".$value."', '$fs')";
                            }
                        }

                        $this->Services_model->del_precodes($id ,$fs);
                       
                        if($strPreCodes != '')
                        {
                            $this->Services_model->insert_precodes($strPreCodes);
                            
                        }
                        //=================================== PRE CODES ======================================//
                        //=================================== ASSIGN BRANDS AND MODELS ======================================//
                        $apiIdForBrand = $this->input->post('apiIdForBrand') ?: 0;
                        $arrBrandIds = $this->input->post('chkBrands') ?: 0;
                        $arrModelIds = $this->input->post('chkModels') ?: 0;
                        $strBrandsQry = '';

                        $this->Services_model->del_packs_models($id ,$apiIdForBrand ,$fs);
                    
                        if(is_array($arrModelIds))
                        {
                            foreach($arrModelIds as $key => $value)
                            {
                                if($value != '' && $apiIdForBrand > 0)
                                {
                                    $arrVal = explode(',', $value);
                                    if($strBrandsQry != '')
                                        $strBrandsQry .= ',';
                                    $strBrandsQry .= "('$id', '".$arrVal[0]."', '".$arrVal[1]."', '$apiIdForBrand', '0')";
                                }
                            }
                            if($strBrandsQry != '')
                            {
                                $this->Services_model->insert_packs_models($strBrandsQry);

                                
                            }
                        }
                        //=================================== ASSIGN BRANDS AND MODELS ======================================//			
                        $message = $this->lang->line('BE_GNRL_11');
                    }
                    else
                        $message = $this->lang->line('BE_LBL_572');
                }
            }


        }

        if($applyAPI == '1')
        {
            $crAPIId = $this->input->post_get('crAPIId') ?: 0;
            $crAPISrvId = $this->input->post_get('crAPISrvId') ?: 0;
            if($crAPIId > 0 && $crAPISrvId > 0)
            {
                $update_data = array(
                    'ExternalNetworkId' => $crAPISrvId,
                    'APIId' => $crAPIId 
                );

                $where_data = array(
                    'PackageId' => $id
                );
                $this->Service_model->update_packages($update_data, $where_data);
              
                $message = $this->lang->line('BE_GNRL_11');
            }
        }

        if($id > 0)
        {
            $row = $this->Services_model->fetch_packages_api($id);
            if (isset($row->PackageTitle) && $row->PackageTitle != '')
            {
                $packageTitle = stripslashes($row->PackageTitle);
                $timeTaken = stripslashes($row->TimeTaken);
                $mustRead = stripslashes($row->MustRead);
                $price = $row->PackagePrice;
                $costPrice = $row->CostPrice;
                $supplier = stripslashes($row->Supplier);
                $extNetworkId = $row->ExternalNetworkId;
                $existingImage = $row->PackageImage != '' ? base_url()."uplds/$THEME/".$row->PackageImage : '';
                $apiId = $row->APIId.'~'.$row->SendExternalId.'~'.$row->APIType;
                $dupIMEI = $row->DuplicateIMEIsNotAllowed;
                $toc = $row->TOCs;
                $costPrFrmAPI = $row->CostPriceFromAPI;
                $onlyAPIId = $row->APIId;
                $apiType = $row->APIType;
                $extNtwkId = $row->SendExternalId;
                $categoryId = $row->CategoryId;
                $fs = $row->sl3lbf;
                $newTplType = $row->NewTplType;
                $successTplType = $row->SuccessTplType;
                $cancelledTplType = $row->CancelledTplType;
                $newTplSbj = stripslashes($row->NewTplSubject);
                $newTplBody = stripslashes($row->NewTplBody);
                $sucTplSbj = stripslashes($row->SuccessTplSubject);
                $sucTplBody = stripslashes($row->SuccessTplBody);
                $canTplSbj = stripslashes($row->CancelTplSubject);
                $canTplBody = stripslashes($row->CancelTplBody);
                $newSendCopy = stripslashes($row->SendNewCopyToAdmin);
                $sucSendCopy = stripslashes($row->SendSuccessCopyToAdmin);
                $canSendCopy = stripslashes($row->SendRejectCopyToAdmin);
                $disablePackage = $row->DisablePackage;
                $htmlTitle = stripslashes($row->HTMLTitle);
                $metaKW = stripslashes($row->MetaKW);
                $seoName = $row->SEOURLName;
                $metaTags = stripslashes($row->MetaTags);
                $fName = $row->FileName;
                $imeiFType = $row->IMEIFieldType;
                $updatedAt = $row->EditedAt;
                $calPreCodes = $row->CalculatePreCodes;
                $sendSMS = $row->SendSMS;
                $srvType = $row->ServiceType;
                $responseDelayTm = $row->ResponseDelayTm;
                $cronDelayTm = $row->CronDelayTm == '0' ? '1' : $row->CronDelayTm;
                $cancelOrders = $row->CancelOrders;
                $verifyOrders = $row->VerifyOrders;
                $cancelMins = $row->OrderCancelMins;
                $verifyMins = $row->OrderVerifyMins;
                $redirect = $row->RedirectionURL;
                $customFldId = $row->CustomFieldId;
                $emailIDs = $row->NewOrderEmailIDs;
                $seriesId = $row->SeriesId;
	        }   
            $rsPreCodes = $this->Services_model->fetch_precodes($id ,$fs); 
       
            foreach($rsPreCodes as $row)
            {
                $preCodes .= $row->PreCode."\n";	
            }
            
            $rwBrandAPI = $this->Services_model->fetch_packs_models($id ,$fs);
            if(isset($rwBrandAPI->APIId) && $rwBrandAPI->APIId != '')
            {
                $apiIdForBrand = $rwBrandAPI->APIId;
            }
        }
        $srvcType = $fs;
        if($this->input->post('packFeatures'))
        {
            $selectedIds = sizeof($this->input->post('packFeatures'));
            $strData = '';
            for($k = 0; $k < $selectedIds; $k++)
            {
                if($k > 0)
                    $strData .= ',';
                $strData .= "(".$id.",".$this->input->post('packFeatures')[$k].", '$srvcType')";
            }
            $this->Services_model->del_pack_selected_features($id ,$srvcType);
          
            if($strData != '')
            {
                $this->Services_model->insert_pack_selected_features($strData);
                
            }
        }


        if($onlyAPIId && $fs == 0)
        {
            $del = $this->input->post_get('del') ? 1 : 0;
            $altId = $this->input->post_get('altId') ?: 0;
            if($del == '1' && $altId > 0)
            {
                $this->Services_model->del_alternate_apis($altId , $id , $fs);
                $message = 'Alternate API has been deleted successfully!';
            }
            
             
            $this->data['rsAltAPIH'] = $this->Services_model->fetch_alternate_apis_upplier_services($id ,$fs);
            
        }

        if($id > 0)
        {
            $this->data['rsAPIHistory'] = $this->Services_model->fetch_srv_history_supplier_services($id ,$fs); 
        }
    


        //========================================== ADDING FEATURES ==========================================//
        $this->data['rsFtrs'] = $this->Services_model->fetch_service_features($id ,$srvcType);
       
        $this->data['rsCurrencies'] = $this->Services_model->fetch_packages_currencies($id);
        $this->data['apiIdForBrand'] = $apiIdForBrand; 
        $this->data['id'] = $id;
        $this->data['message'] = $message ;
        $this->data['count'] = count($this->data['rsCurrencies']);
        $this->data['preCodes'] = $preCodes;
        $this->data['fs'] = $fs ;



        $this->data['packageTitle'] = $packageTitle ; 
        $this->data['timeTaken'] = $timeTaken;
        $this->data['mustRead'] = $mustRead;
        $this->data['price'] = $price ;
        $this->data['costPrice'] =  $costPrice;
        $this->data['supplier'] = $supplier;
        $this->data['extNetworkId'] = $extNetworkId;
        $this->data['existingImage'] = $existingImage;
        $this->data['apiId'] = $apiId;
        $this->data['dupIMEI'] = $dupIMEI ;
        $this->data['toc'] = $toc;
        $this->data['costPrFrmAPI'] = $costPrFrmAPI;
        $this->data['onlyAPIId'] = $onlyAPIId ;
        $this->data['apiType'] =  $apiType;
        $this->data['extNtwkId'] = $extNtwkId ;
        $this->data['categoryId'] = $categoryId;
        $this->data['fs'] = $fs;
        $this->data['newTplType'] = $newTplType;
        $this->data['successTplType'] = $successTplType;
        $this->data['cancelledTplType'] = $cancelledTplType;
        $this->data['disablePackage'] = $disablePackage ;
        $this->data['htmlTitle'] =  $htmlTitle;
        $this->data['metaKW'] = $metaKW;
        $this->data['seoName'] = $seoName;
        $this->data['metaTags'] = $metaTags;
        $this->data['fName'] = $fName;
        $this->data['imeiFType'] = $imeiFType;
        $this->data['updatedAt'] = $updatedAt;
        $this->data['calPreCodes'] = $calPreCodes;
        $this->data['sendSMS'] = $sendSMS;
        $this->data['srvType'] = $srvType;
        $this->data['responseDelayTm'] = $responseDelayTm ;
        $this->data['cronDelayTm'] = $cronDelayTm;
        $this->data['cancelOrders'] = $cancelOrders;
        $this->data['verifyOrders'] = $verifyOrders;
        $this->data['cancelMins'] = $cancelMins;
        $this->data['verifyMins'] = $verifyMins;
        $this->data['redirect'] = $redirect;
        $this->data['customFldId'] = $customFldId ;
        $this->data['emailIDs'] = $emailIDs;
        $this->data['seriesId'] = $seriesId;
        // $this->data['newTplSbj'] = $newTplSbj ;
        // $this->data['newTplBody'] = $newTplBody;
        // $this->data['sucTplSbj'] = $sucTplSbj ;
        // $this->data['sucTplBody'] = $sucTplBody;
        
        $this->data['view'] = 'admin/package';
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    
    public function ajxpackage(){
        $purpose = $this->input->post('purpose');
        $msg = '' ;
        if ($purpose == 'pdp')
        {
            $packageId = $this->input->post('packageId') ?: 0;
            $msg = '';
            $price = '';
            $row = $this->Serices_model->fetch_packages_by_id($packageId);
            
            if (isset($row->PackagePrice) && $row->PackagePrice != '')
                $price = $row->PackagePrice;
            if($price != '')
            {
                $this->Services_model->update_users_packages_prices($price ,$packageId);
                $this->Services_model->update_plans_packages_prices($price ,$packageId);
                
                
            }
            $msg = $this->lang->line('BE_GNRL_11');
        }
        else if ($purpose == 'pcp')
        {
            $packageId = $this->input->post('packageId') ?: 0;
            $price = $this->input->post('price') ?: 0;
            $msg = '';
            $this->Services_model->update_users_packages_prices($price ,$packageId);
            $this->Services_model->update_plans_packages_prices($price ,$packageId);
            $msg = $this->lang->line('BE_GNRL_11');
        }
        else if ($purpose == 'getsuppliersrvcs')
        {
            $apiId = $this->input->post('apiId') ?: 0;
            $serviceType = $this->input->post('serviceType') ?: 0;
            $rsSrvcs =  fetch_supplier_services($apiId ,$serviceType);
            
            $msg = "<option value='0'>Please Select</option>";
            foreach($rsSrvcs as $row)
            {
                $optionVal = stripslashes($row->ServiceName);
                if($row->ServicePrice != '')
                    $optionVal .= ' - '.stripslashes($row->ServicePrice).' Credits';
                if($row->ServiceTime != '')
                    $optionVal .= ' - '.stripslashes($row->ServiceTime);
                $msg .= "<option value='".$row->Id."'>".$optionVal."</option>";
            }
        }
        else if ($purpose == 'getapibrands')
        {
            $id = $this->input->post('id') ?: 0;
            $apiId = $this->input->post('apiId') ?: 0;
            $serviceType = $this->input->post('serviceType') ?: 0;
            $hdAPIIdForBrand = $this->input->post('hdAPIIdForBrand') ?: 0;
            $i = 0;
            if($id > 0 && $hdAPIIdForBrand > 0)
            {
                $arrBrands = array();		
                $rsBrands = $this->Services_model->fetch_packs_models_by_api_id($id ,$hdAPIIdForBrand , $serviceType);
                
                foreach($rsBrands as $row )
                {
                    $arrBrands[$row->BrandId] = $row->BrandId;
                }
            }
            $rsBrnds = $this->Services_model->fetch_api_brands($apiId);
            
            foreach($rsBrnds as $row)
            {
                $checked = '';
                if(isset($arrBrands[$row->Id]))
                    $checked = 'checked';
                $msg .= '<label><input type="checkbox" '.$checked.' name="chkBrands[]" id="chkBrand'.$i.'" value="'.$row->Id.'"> '.stripslashes($row->Value).' </label><br />';
                $i++;
            }
            if($msg == '')
                $msg = '~~~0~~~<p>No Brand exists!</p>';
            else
            {
                $totalBrands = $i+1;
                $msg = "~~~".$totalBrands."~~~<label><input type='checkbox' id='chkSelect' name='chkSelect' onClick='selectAllBrands($i);' > <b>Select All Brands</b> </label><br />".$msg;
            }
        }
        else if ($purpose == 'getbrandmodels')
        {
            $id = $this->input->post('id') ?: 0;
            $serviceType = $this->input->post('serviceType') ?: 0;
            $brandIds = $this->input->post('brandIds') ?: 0;
            $apiId = $this->input->post('apiId') ?: 0;
            $i = 0;
            $arrModels = array();		
            $rsModels = $this->Services_model->fetch_packs_models_by_concat($id ,$apiId , $brandIds ,$serviceType);
            
            foreach($rsModels as $row)
            {
                $arrModels[$row->ModelId] = $row->ModelId;
            }
            $rsModels = $this->Services_model->fetch_api_models_brands($apiId ,$brandIds);
           
            foreach($rsModels as $row )
            {
                $checked = '';
                if(isset($arrModels[$row->ModelId]))
                    $checked = 'checked';
                $msg .= '<label><input type="checkbox" '.$checked.' name="chkModels[]" id="chkModel'.$i.'" value="'.$row->ModelId.'"> '.stripslashes($row->Model).' </label><br />';
                $i++;
            }
            if($msg == '')
                $msg = '<p>No Model exists!</p>';
            else
            {
                $totalBrands = $i+1;
                $msg = "<label><input type='checkbox' id='chkSelect1' name='chkSelect1' onClick='selectAllModels($i);' > <b>Select All Models</b> </label><br />".$msg;
            }
        }
        else if ($purpose == 'gettoolmobiles')
        {
            $apiId = $this->input->post('apiId') ?: 0;
            $serviceId = $this->input->post('serviceId') ?: 0;
            $rsMobiles = $this->Services_model->fetch_tool_mobiles($apiId , $serviceId);
            
            $msg = "<option value='0'>Please Select</option>";
            foreach($rsMobiles as $row)
            {
                $msg .= "<option value='".$row->Id."'>".stripslashes($row->Value)."</option>";
            }
        }
        else if ($purpose == 'dc')
        {
            $packageId = $this->input->post('packageId') ?: '0';
            $PACK_PRICE = $this->input->post('packPrice') ?: '-';
            $rsUsers = $this->Services_model->fetch_currency_users_price_plans();
            
            $users = count($rsUsers);
            $msg = "
                <table class='table table-striped table-bordered table-advance table-hover'>
                <thead>
                <tr class='bg-primary'>
                    <th>".$this->lang->line('BE_USR_HD')."</th>
                    <th>".$this->lang->line('BE_USR_9')."</th>
                    <th>".$this->lang->line('BE_LBL_18')."</th>
                    <th>".$this->lang->line('BE_LBL_201')."</th>
                </tr>
                </thead>
                <tbody>";
            if($users > 0)
            {
                $rsPlanPrices = $this->Services_model->fetch_plans_packages_prices($packageId);
                
                foreach($rsPlanPrices as $row)
                {
                    $PACK_PRICES_PLAN[$row->PlanId][$row->CurrencyId] = $row->Price;
                }
                $rsSrvcsPrices = $this->Services_model->fetch_users_packages_prices_by_package_id($packageId);
                
                foreach($rsSrvcsPrices as $row )
                {
                    $PACK_PRICES_USER[$row->UserId] = $row->Price;
                }
                
                
                foreach($rsUsers as $row)
                {
                    $group = $row->PricePlan == '' ? '-' : stripslashes($row->PricePlan);
                    $price = '';
                    if($row->PricePlanId > 0 && isset($PACK_PRICES_PLAN[$row->PricePlanId][$row->CurrencyId]))
                    {
                        $price = $PACK_PRICES_PLAN[$row->PricePlanId][$row->CurrencyId];
                    }
                    if(isset($PACK_PRICES_USER[$row->UserId]))
                    {
                        $price = $PACK_PRICES_USER[$row->UserId];
                    }
                    /*else if(isset($PACK_PRICES_CURR[$row->CurrencyId]))
                    {
                        $price = $PACK_PRICES_CURR[$row->CurrencyId];
                    }*/
                    if($price == '')
                        $price = $PACK_PRICE;
                    if($PACK_PRICE != $price)
                    {
                        $msg .= '<tr>
                            <td class="highlight"><div class="success"></div><a target="_blank" href="userpackageprices.php?userId='.$row->UserId.'">'.stripslashes($row->UserName).'</a></td>
                            <td>'.stripslashes($row->Name).'</td>
                            <td>'.$group.'</td>
                            <td><font style="color:red;"><del>'.$row->CurrencySymbol.' '.roundMe($PACK_PRICE).'</del></font>  '.$row->CurrencySymbol.' '.roundMe($price).'</td>
                        </tr>';
                    }
                }
            }
            else
                $msg .= "<tr><td colspan='4'>".$this->lang->line('BE_GNRL_9')."</td></tr>";
            $msg .= "</tbody></table>";
        }
        else if ($purpose == 'saoe')
        {
            $eml = $this->input->post('eml') ?: '';
            $sc = $this->input->post('sc') ?: '0';
            $ids = $this->input->post('ids') ?: 0;
            if($eml != '' && $ids != '0')
            {
                if($sc == '0')
                    $emailMsg = showAcceptedRtlIMEIs($ids);
                else if($sc == '2')
                    $emailMsg = showAcceptedRtlServerOrders($ids);
                $arr = getEmailDetails($objDBCD14);
                sendGeneralEmail($eml, $arr[0], $arr[1], 'Customer', 'Order Details', '', $emailMsg);
                $msg =  $this->lang->line('BE_GNRL_11');
            }
            else
                $msg =  'Invalid Email or Orders';
        }
        else if ($purpose == 'sendsrvcemail')
        {
            $id = $this->input->post('id') ?: 0;
            $sc = $this->input->post('sc') ?: 0;
            $notes = $this->input->post('notes') ?: '';
            $groupId = $this->input->post('groupId') ?: 0;
            $strAND = '';
            
            if($sc == '2')
            {
                $serviceId = $id;
                $col1 = 'LogPackageTitle';
                $col2 = 'DeliveryTime';
                $col3 = 'LogPackageDetail';
                $col4 = 'LogPackageId';
                $col5 = 'LogPackagePrice';
                $tbl = 'tbl_gf_log_packages';
            }
            else		
            {
                $IMEI_TYPE = $sc;
                $serviceId = $id;
                $col1 = 'PackageTitle';
                $col2 = 'TimeTaken';
                $col3 = 'MustRead';
                $col4 = 'PackageId';
                $col5 = 'PackagePrice';
                $tbl = 'tbl_gf_packages';
            }
            $features = '';
            if($groupId > 0)
                $strAND = " AND PricePlanId = '$groupId'";
            $rw = $this->Services_model->custom_query_packages($col1, $col2, $col3, $col5 ,$tbl , $col4 ,$id);
            
            if (isset($rw->$col1) && $rw->$col1 != '')
            {
                $packageTitle = stripslashes($rw->$col1);
                $timeTaken = stripslashes($rw->$col2);
                $mustRead = stripslashes($rw->$col3);
                $defaultPrice = $rw->$col5;
                $features = getServiceFeaturesForEmail($id, $sc);
            }
            $PACK_PRICES_PLAN = array();
            $PACK_PRICES_USER = array();
            if($sc == '2')
            {
                $idCol = 'LogPackageId';
                $tbl = 'tbl_gf_users_log_packages_prices';
            }
            else		
            {
                $idCol = 'PackageId';
                $tbl = 'tbl_gf_users_packages_prices';
            }
            //============================================================ CHECK IF PRICE SET AGAINST USER ==========================================================//
            $rsUserPrices = $this->Services_model->custom_users_packages_prices($tbl , $idCol ,$serviceId);
            
            foreach($rsUserPrices as $row )
            {
                $PACK_PRICES_USER[$row->UserId] = roundMe($row->Price);
            }
            //============================================================ CHECK IF PRICE SET AGAINST USER ==========================================================//
            
            //========================================================= CHECK IF USER PRICE SET IN PRICE PLAN =======================================================//
            $rsPlanPr = $this->Services_model->fetch_plans_packages_prices_by_package_id($sc , $serviceId);
            
            foreach($rsPlanPr as $row)
            {
                $PACK_PRICES_PLAN[$row->PlanId][$row->CurrencyId] = roundMe($row->Price);
            }
            //========================================================= CHECK IF USER PRICE SET IN PRICE PLAN =======================================================//
            
            $rsUsers = $this->Services_model->fetch_users_currency($strAND);
            
            $strPrices = '';
            foreach($rsUsers as $row )
            {
                $packagePrice = 0;
                if(isset($PACK_PRICES_USER[$row->UserId]) && $PACK_PRICES_USER[$row->UserId] != '') // IF CUSTOM
                    $packagePrice = roundMe($PACK_PRICES_USER[$row->UserId]);
                else if(isset($PACK_PRICES_PLAN[$row->PricePlanId][$row->CurrencyId]) && $PACK_PRICES_PLAN[$row->PricePlanId][$row->CurrencyId] != '' && $row->PricePlanId > 0) // GROUP
                    $packagePrice = roundMe($PACK_PRICES_PLAN[$row->PricePlanId][$row->CurrencyId]);
                else
                {
                    $packagePrice = roundMe($defaultPrice * $row->ConversionRate);
                }
                if($packagePrice > 0)
                {
                    if($strPrices != '')
                        $strPrices .= ',';
                    $strPrices .= "('$id', '".$row->ClientName."', '".$row->UserEmail."', 
                        '".$packagePrice.' '.$row->CurrencyAbb."', '$sc', '$notes', '".$packageTitle."', 
                        '".$timeTaken."', '".$mustRead."', '".$feature."')";
                }
            }
            if($strPrices != '')
            {
                $this->Services_model->del_service_price_email($id ,$sc);
                $this->Services_model->insert_service_price_email($strPrices);
                
            }
            $msg = $this->lang->line('BE_GNRL_11');
        }
        else if ($purpose == 'getservicetypes')
        {
            $apiId = $this->input->post('apiId') ?: 0;
            $serviceId = $this->input->post('serviceId') ?: 0;
            $serviceType = $this->input->post('serviceType') ?: 2;
            $rsSrvcTyps = $this->Services_model->fetch_service_service_types($apiId ,$serviceId , $serviceType);
            
            $msg = '0';
            foreach($rsSrvcTyps as $row )
            {
                $optionVal = stripslashes($row->ServiceTypeName);
                if($row->ServiceTypePrice != '')
                    $optionVal .= ' - '.stripslashes($row->ServiceTypePrice).' Credits';
                $msg .= "<option value='".$row->Id."'>".$optionVal."</option>";
            }
        }
        echo $msg;
    }


    public function services_quickedit(){
        $eu=0;
        if(!isset($start))
            $start = 0;

        if($this->input->post_get("start"))
            $start =$this->input->post_get("start");

        $eu = ($start - 0);
        $limit = $this->input->post_get('records') ?: 100;
        $txtlqry = "&records=$limit";
        $thisp = $eu + $limit;
        $back = $eu - $limit;
        $next = $eu + $limit;
        $message = $strWhere = $strWhereSrv =  $strWhereCat = '';
        $PLANS  = array();
        $sc = $this->input->post_get('fs') ?: 0;
        $txtlqry .= "&fs=$sc";
        $fs = $sc;
        $cldFrm = $this->input->post_get('cldFrm') ?: 0;
        $categoryId = $this->input->post_get('categoryId') ?: 0;
        $serviceId = $this->input->post_get('serviceId') ?: 0;
        $costPrice = '' ;
        switch($sc)
        {
            case '0': // IMEI services
                $strWhereSrv .= " AND sl3lbf = 0";
                $strWhereCat = " AND SL3BF = 0";
                $tblName = 'tbl_gf_package_category';
                $tblPckName = 'tbl_gf_packages';
                $colId = 'PackageId';
                $colTitle = 'PackageTitle';
                $disableCol = 'DisablePackage';
                $colPrice = 'PackagePrice';
                $colTime = 'TimeTaken';
                $heading = $this->lang->line('BE_MENU_PCKGS');
                break;
            case '1': // File services
                $strWhereSrv .= " AND sl3lbf = 1";
                $strWhereCat = " AND SL3BF = 1";
                $tblName = 'tbl_gf_package_category';
                $tblPckName = 'tbl_gf_packages';
                $colId = 'PackageId';
                $colTitle = 'PackageTitle';
                $disableCol = 'DisablePackage';
                $colPrice = 'PackagePrice';
                $colTime = 'TimeTaken';
                $heading = $this->lang->line('BE_PCK_25');
                break;
            case '2': // Server services
                $tblName = 'tbl_gf_log_package_category';
                $tblPckName = 'tbl_gf_log_packages';
                $colId = 'LogPackageId';
                $colTitle = 'LogPackageTitle';
                $disableCol = 'DisableLogPackage';
                $colPrice = 'LogPackagePrice';
                $heading = $this->lang->line('BE_LBL_299');
                $colTime = 'DeliveryTime';
                break;
        }
        $rw = fetch_currency_data();
        $defaultCurrencyId = $rw->CurrencyId;
        if($categoryId != '0')
        {
            $strWhere .= " AND CategoryId = '$categoryId'";
            $txtlqry .= "&categoryId=$categoryId";
        }
        if($serviceId != '0')
        {
            $strWhere .= " AND $colId = '$serviceId'";
            $txtlqry .= "&serviceId=$serviceId";
        }
        if($this->input->post_get('txtService') && $this->input->post_get('txtService') != '')
        {
            $service = trim($this->input->post_get('txtService'));
            if($service != '')
            {
                $strWhere .= " AND $colTitle LIKE '%$service%'";
                $txtlqry .= "&txtService=$service";
            }
        }
        if($this->input->post_get('btnSubmit') && $this->session->userdata('GSM_FSN_AdmId') && $cldFrm == '1')
        {
            $currDtTm = setDtTmWRTYourCountry();
            $arrPckIds = $this->input->post_get('chkPacks') ?: 0;
            $totalPckIds = 0;
            $strPackIds = '0';
            if(is_array($arrPckIds))
            {
                $totalPckIds = count($arrPckIds);
                $str = '';
                for($i = 0; $i < $totalPckIds; $i++)
                {
                    $col3 = '';
                    $data = explode('|', $arrPckIds[$i]);
                    $packId = $data[0];
                    $strPackIds .= ", $packId";
                    $pack = $this->input->post('txtPack'.$data[1]);
                    if($fs == '0')
                    {
                        $apiType = $this->input->post('rdSrvcAPIType'.$data[1]);
                        $col3 = ", CronDelayTm = '$apiType'";
                    }
                    $costPrice = $this->input->post('txtCostPrice'.$data[1]);
                    $price = $this->input->post('txtPrice'.$data[1]);
                    $time =  $this->input->post('txtTime'.$data[1]);
                    $col1 = '';
                    $col2 = '';
                    if(is_float($costPrice) || is_numeric($costPrice))
                    {
                        $col1 = ", CostPrice = '$costPrice'";
                    }
                    if(is_float($price) || is_numeric($price))
                    {
                        $col2 = ", $colPrice = '$price'";
                    }
                    $this->Services_model->update_custom_services_quickedit($tblPckName , $colTitle ,$pack ,$colTime , $time ,$currDtTm ,$col1 ,$col2 ,$col3 , $colId , $packId);
                   
                    // MAKE STRING FOR PRICE PLAN VALUES //
                    $totalPlans = $this->input->post('totalPlans');
                    for($j = 0; $j < $totalPlans; $j++)
                    {
                        $planPrice = $this->input->post('txtPlanPrice'.$j.'_'.$data[1]);
                        $planId = $this->input->post('hdPlanId'.$j.'_'.$data[1]);
                        if($str != '')
                            $str .= ',';
                        if(is_numeric($planPrice))
                        {
                            $str .= "('$planId', '".$defaultCurrencyId."', '$packId', '$planPrice', '$sc')";
                        }
                    }
                    // MAKE STRING FOR PRICE PLAN VALUES //
                }
                $message = $this->lang->line('BE_GNRL_11');	
                if($str != '')
                {
                    $this->Services_model->del_plans_packages_prices_by_package_ids($sc , $strPackIds);
                  
                    $this->Services_model->insert_plans_packages_prices($str);
                }
            }
            else{
                $message = 'Please select a record!';		
            }
        }
        $this->data['rsPacks'] = $this->Services_model->fetch_custom_data($colId, $colTitle, $colPrice, $disableCol, $colTime, $tblPckName , $strWhereSrv, $strWhere , $eu, $limit) ;
        			
        $this->data['count'] = count($this->data['rsPacks']);
        $this->data['totalGroups'] = 0;
        $DEFAULT_PLAN_PRICES = array();
        if($this->data['count'] > 0)
        {
            $this->data['rsGroups'] =  fetch_price_plans();
            $this->data['totalGroups'] = count( $this->data['rsGroups']);
            $DEFAULT_PLAN_PRICES = getDefaultPlansPrices($sc, $defaultCurrencyId, $categoryId);
            
        }

        $this->data['sc'] = $sc;
        $this->data['fs'] = $fs ;
        $this->data['heading'] = $heading;
        $this->data['message'] = $message ;
        $this->data['tblName'] = $tblName;
        $this->data['categoryId'] = $categoryId;
        $this->data['strWhereCat'] = $strWhereCat;
        $this->data['colId'] = $colId;
        $this->data['colTitle'] = $colTitle;
        $this->data['colTime'] = $colTime;
        $this->data['tblPckName'] = $tblPckName;
        $this->data['disableCol'] = $disableCol;
        $this->data['colPrice'] = $colPrice;
        $this->data['strWhereSrv'] = $strWhereSrv;
        $this->data['page_name'] = '' ;
        $this->data['back'] = $back ; 
        $this->data['start'] = $start;
        $this->data['txtlqry'] = $txtlqry;
        $this->data['limit'] = $limit ;
        $this->data['eu'] = $eu ;
        $this->data['pLast'] = '';
        $this->data['thisp'] = $thisp;
        $this->data['next'] = $next;
        $this->data['strWhere'] = $strWhere;
        $this->data['serviceId'] = $serviceId;
    
        $this->data['view'] = 'admin/services_quickedit';
        $this->load->view('admin/layouts/default1' , $this->data);
    }


    public function categories(){
        $iFrm = $this->input->post_get('iFrm') ?: 0;
        $dc = $this->input->post_get('dc') ?: 0;
        $fs = $this->input->post_get('fs') ?: 0;
        $tblName = 'tbl_gf_package_category';
        $strWhere = '';
        if($iFrm == '1')
        {
            $tblName = 'tbl_gf_log_package_category';
        }
        else if($iFrm == '2')
            $tblName = 'tbl_gf_category';
        else if($iFrm == '3')
            $tblName = 'tbl_gf_home_ecategory';
        else
        {
            $strWhere = " AND SL3BF = '$fs'";
        }
        if($dc == '0')
            $strWhere .= ' AND DisableCategory = 0';
        $this->data['rsCats'] = $this->Services_model->fetch_category_data($tblName , $strWhere);
       
        $this->data['count'] = count($this->data['rsCats']);
        $qryString = explode ('&dc',  $this->data['QUERY_STRING']);
        if(isset($qryString[0]) && $qryString[0] != '')
            $this->data['QUERY_STRING'] = $qryString[0];

        $this->data['iFrm'] = $iFrm;
        $this->data['fs'] = $fs;

        $this->data['view'] = 'admin/categories';
        $this->load->view('admin/layouts/default1' , $this->data);
    }


    public function processsortable(){
        $i = $this->input->post_get('i') ?: '0';
        if($i == 0)
        {
            $tblType = $this->input->post_get('hw') ?: '0';
            $fs = $this->input->post_get('fs') ?: '0';
            $strWhere = '';
            if($tblType == '0')
            {
                $tblName = 'tbl_gf_package_category';
                $strWhere = " AND SL3BF = '$fs'";
            }
            else if($tblType == '1')
                $tblName = 'tbl_gf_log_package_category';
            else if($tblType == '2')
                $tblName = 'tbl_gf_category';
            else if($tblType == '3')
                $tblName = 'tbl_gf_knowledgebase_cat';
            else if($tblType == '4')
                $tblName = 'tbl_gf_manufacturer';
            else if($tblType == '5')
                $tblName = 'tbl_gf_news_cat';
            else if($tblType == '6')
                $tblName = 'tbl_gf_newsltrs_cat';
            foreach ($this->input->get('listItem') as $position => $item) :
                $this->Services_model->update_sortable_table($tblName , $position , $item , $strWhere);
            endforeach;
            echo 'Data has been sorted successfully!';
        }
        else if($i == 1)
        {
            foreach ($this->input->get('listItem') as $position => $item) :
                $this->Services_model->update_payment_methods($position , $item);   
            endforeach;
            echo 'Payment Methods have been sorted successfully!';
        }
        else if($i == 2)
        {
            foreach ($this->input->get('listItem') as $position => $item) :
                $this->Services_model->update_models($position ,$item);
               
            endforeach;
            echo 'Models have been sorted successfully!';
        }
        else if($i == 3) //Done
        {
            foreach ($this->input->get('listItem') as $position => $item) :
                $this->Services_model->update_make_country($position , $item);
            endforeach;
            echo 'Countries have been sorted successfully!';
        }
        else if($i == 4)
        {
            foreach ($this->input->get('listItem') as $position => $item) :
                $this->Services_model->update_retail_services($position , $item);
            endforeach;
            echo 'Services have been sorted successfully!';
        }
        else if($i == 5)
        {
            foreach ($this->input->get('listItem') as $position => $item) :
                $this->Services_model->update_category($position , $item);
            endforeach;
            echo 'Product Categories have been sorted successfully!';
        }
        else if($i == 6)
        {
            foreach ($this->input->get('listItem') as $position => $item) :
                $this->Services_model->update_carriers($position , $item);  
            endforeach;
            echo 'Carriers have been sorted successfully!';
        }
        else if($i == 7)
        {
            $cId = $this->input->post_get('cId') ?: '0';
            foreach ($this->input->get['listItem'] as $position => $item) :
                $this->Services_model->update_products($position , $item);
            endforeach;
            echo 'Products have been sorted successfully!';
        }
        else if($i == 8)
        {
            foreach ($this->input->get('listItem') as $position => $item) :
                $this->Services_model->update_eheader_cats($position , $item);    
            endforeach;
            echo 'Header Categories have been sorted successfully!';
        }
        else if($i == 9)
        {
            $sc = $this->input->post_get('sc') ?: '0';
            foreach ($this->input->get('listItem') as $position => $item) :
                $this->Services_model->update_custom_fields($position , $item , $sc);
               
            endforeach;
            echo 'Custom Fields have been sorted successfully!';
        }
        else if($i == 10)
        {
            $sc = $this->input->post_get('sc') ?: '0';
            switch($sc)
            {
                case '0': // IMEI services
                    $tblPckName = 'tbl_gf_packages';
                    $colId = 'PackageId';
                    $sortCol = 'PackOrderBy';
                    break;
                case '1': // File services
                    $tblPckName = 'tbl_gf_packages';
                    $colId = 'PackageId';
                    $sortCol = 'PackOrderBy';
                    break;
                case '2': // Server services
                    $tblPckName = 'tbl_gf_log_packages';
                    $colId = 'LogPackageId';
                    $sortCol = 'PackOrderBy';
                    break;
                case '3': // Retail services
                    $tblPckName = 'tbl_gf_retail_services';
                    $colId = 'PackageId';
                    $sortCol = 'OrderBy';
                    break;
            }
            foreach ($this->input->get('listItem') as $position => $item) :
                $this->Services_model->update_retail_services_packages($tblPckName , $sortCol , $position , $colId , $item);
            endforeach;
            echo 'Services have been sorted successfully!';
        }
        else if($i == '11')
        {
            $sc = $this->input->post_get('sc') ?: '0';
            $id = $this->input->post_get('id') ?: '0';
            foreach ($this->input->get('listItem') as $position => $item) :
                $this->Services_model->update_alternate_apis($position , $item , $id , $sc);
               
            endforeach;
            echo 'Alternate APIs have been sorted successfully!';
        }
        else if($i == '12')
        {
            $type = $this->input->post_get('type') ?: '0';
            foreach ($this->input->get('listItem') as $position => $item) :
                $this->Services_model->update_banner($position , $item , $type);
                
            endforeach;
            echo 'Banners have been sorted successfully!';
        }
    }

    public function category(){
        $id = $this->input->post_get('id') ?: 0;
        $fs = $this->input->post_get('fs') ?: 0;
        $iFrm = $this->input->post_get('iFrm') ?: 0;
        $this->data['category'] = '';
        $this->data['disable'] = 0;
        $tblName = 'tbl_gf_package_category';
        $message = '';
        $this->data['htmlTitle'] = '';
        $this->data['seoName'] = '';
        $this->data['metaDesc'] = '';
        $this->data['metaKW'] = '';
        $this->data['desc'] = '';

        if($iFrm == '1'){
	        $tblName = 'tbl_gf_log_package_category';
        }
        else if($iFrm == '2'){
            $tblName = 'tbl_gf_category';
        }
        else if($iFrm == '3'){
            $tblName = 'tbl_gf_home_ecategory';
        }

        if ($this->input->post('txtCategory'))
        {
            $category = $this->input->post('txtCategory');

            $row = $this->Services_model->fetch_category_home_ecategory($tblName , $category ,$id);  

            if (isset($row->CategoryId) && $row->CategoryId != '')
                $message = $category.$this->lang->line('BE_GNRL_10'); 
            else
            {
                $disable = $this->input->post('disable') ? 1 : 0;
                $seoName = $this->input->post_get('txtSEOName');
                $metaDesc = $this->input->post_get('txtMetaTags');
                $metaKW = $this->input->post_get('txtMetaKW');
                $htmlTitle = $this->input->post_get('txtHTMLTitle');
                $desc = $this->input->post('txtDesc');
                if($id == 0)
                {
                    $col1 = '';
                    $col2 = '';
                    if($iFrm == '0')
                    {
                        $col1 = 'SL3BF';
                        $col2 = "'$fs'";
                    }
                    $insert_array = array(
                        'Category' => $category ,
                        'DisableCategory' => $disable,
                        'SEOURLName' => $seoName ,
                        'MetaTags' => $metaDesc ,
                        'MetaKW' => $metaKW ,
                        'Description' => $desc ,
                        'HTMLTitle' => $htmlTitle ,
                        "$col1" =>  $col2
                    );
                    $this->Services_model->insert_category_home_ecategory($insert_array , $tblName);
                  
                    $message =  'Category is added successfully';
                    $category = '';
                    $metaDesc = '';
                    $metaKW = '';
                    $htmlTitle = '';
                    $desc = '';
                    $seoName = '';
                }
                else
                {
                    $update_array = array();
                    $col = '';
                    if($iFrm == '0'){
                        $update_array['SL3BF'] = $fs ;
                    }
                    $update_array['Category'] = $category;
                    $update_array['MetaKW'] = $metaKW ;
                    $update_array['SEOURLName'] = $seoName ;
                    $update_array['MetaTags'] = $metaDesc ;
                    $update_array['HTMLTitle'] = $htmlTitle;
                    $update_array['Description'] = $desc ;
                    $update_array['DisableCategory'] = $disable;

                    $this->Services_model->update_category_home_ecategory($tblName , $id , $update_array);
                 
                    $message = 'Category is updated successfully';
                }
            }
            
        }

        if($id > 0)
        {
            $row = $this->Services_model->fetch_category_home_ecategory_by_id($tblName ,$id);
            
            if (isset($row->Category))
            {
                $this->data['category'] = stripslashes($row->Category);
                $this->data['disable'] = $row->DisableCategory;
                $this->data['htmlTitle'] = stripslashes($row->HTMLTitle);
                $this->data['metaDesc'] = stripslashes($row->MetaTags);
                $this->data['metaKW'] = stripslashes($row->MetaKW);
                $this->data['desc'] = stripslashes($row->Description);
                $this->data['seoName'] = $row->SEOURLName;
                $fs = $row->SL3BF;
            }
        }

        $this->data['id'] = $id;
        $this->data['fs'] = $fs ;
        $this->data['iFrm'] = $iFrm;
        $this->data['message'] = $message ;
        $this->data['view'] = 'admin/category' ;
        $this->load->view('admin/layouts/default1' , $this->data);

    }

    public function quickpricing(){
        $srvc = $this->input->post_get('srvc') ?: 0;
        switch($srvc)
        {
            case '0';
                $heading = $this->lang->line('BE_USR_11');
                $tblCat = 'tbl_gf_package_category';
                $strWhereCat = " AND SL3BF = 0";
                break;
            case '1';
                $heading = $this->lang->line('BE_LBL_220');
                $tblCat = 'tbl_gf_package_category';
                $strWhereCat = " AND SL3BF = 1";
                break;
            case '2';
                $heading = $this->lang->line('BE_LBL_221');
                $tblCat = 'tbl_gf_log_package_category';
                $strWhereCat = '';
                break;
        }
        $this->data['srvc'] = $srvc;
        $this->data['heading'] = $heading;
        $this->data['tblCat'] = $tblCat;
        $this->data['strWhereCat'] = $strWhereCat;
        $this->data['view'] = 'admin/quick_pricing_bkp';
        $this->load->view('admin/layouts/default1' , $this->data);

    }


    public function ajxpricing(){
        $purpose = $this->input->post_get('purpose');

        if ($purpose == 'update')
        {
            $price = $this->input->post_get('price') ?: 0;
            $type = $this->input->post('type') ?: 0;
            $pricing = $this->input->post('pricing') ?: 0;
            $otherCurr = $this->input->post('otherCurr') ?: 0;
            $srvc = $this->input->post('srvc') ?: 0;
            $categoryId = $this->input->post('categoryId') ?: 0;
            switch($srvc)
            {
                case '0';
                    $tbl = 'tbl_gf_packages';
                    $col = 'PackagePrice';
                    $colId = 'PackageId';
                    $where = ' WHERE sl3lbf = 0';
                    break;
                case '1';
                    $tbl = 'tbl_gf_packages';
                    $col = 'PackagePrice';
                    $colId = 'PackageId';
                    $where = ' WHERE sl3lbf = 1';
                    break;
                case '2';
                    $tbl = 'tbl_gf_log_packages';
                    $col = 'LogPackagePrice';
                    $colId = 'LogPackageId';
                    $where = ' WHERE (1) ';
                    break;
            }
            $strCategory = '';
            if($categoryId > 0)
                $strCategory = " AND CategoryId = '$categoryId'";
            if(is_numeric($price) && $price > 0)
            {
                $operator = $type == 0 ? '+' : '-';
                $setClause = '';
                if($pricing == '0')
                {
                    $setClause = " $col $operator $price ";
                }
                else if($pricing == '1')
                {
                    $setClause = " $col $operator ($col * $price/100) ";
                }
                $this->Services_model->update_pricing($tbl , $col , $setClause , $where , $strCategory);
                
            }
            if($otherCurr == '1')
            {
                $rsCurrencies = fetch_currency_by_id();
                $strCurrencies = '0';
                foreach($rsCurrencies as $row)
                {
                    $strCurrencies .= ', '.$row->CurrencyId;
                }
                if($strCurrencies != '0')
                {
                    if($srvc =='0' || $srvc == '1'){
                       
                        $this->Services_model->del_packages_currencies_and_packages($strCurrencies , $srvc , $strCategory);
                    }
                    else
                    {
                        $strLogCategory = '';
                        if($categoryId > 0){
                            $strLogCategory = " AND PackageId IN (SELECT LogPackageId FROM tbl_gf_log_packages WHERE CategoryId = '$categoryId')";
                        }
                        $this->Services_model->del_log_packages_currencies($strCurrencies , $strLogCategory);
                       
                    }
                    
                    //========================================== FOR PACKS PRICES =================================================//
                    $strPlanCategory = '';
                    if($categoryId > 0){
                        $strPlanCategory = " AND PackageId IN (SELECT $colId FROM $tbl WHERE CategoryId = '$categoryId')";
                    }
                    $this->Services_model->del_plans_packages_prices_and_packages($strCurrencies , $srvc , $strPlanCategory);
                    
                    //========================================== CONVERT PACKS PRICES =================================================//
                
                    //========================================== FOR USERS PRICES =================================================//
                    $strUserCategory = '';
                    if($categoryId > 0)
                        $strUserCategory = " AND $colId IN (SELECT $colId FROM $tbl WHERE CategoryId = '$categoryId')";

                    $strUserIds = '0';
                    $rsUsers = $this->Services_model->fetch_users_by_currencyId($strCurrencies); 
                   
                    foreach($rsUsers as $row)
                    {
                        $strUserIds .= ", ".$row->UserId;
                    }
                    if($strUserIds != '0')
                    {
                        if($srvc =='0' || $srvc == '1'){
                            $this->Services_model->del_users_packages_prices_by_userid($strUserIds , $strUserCategory);
                           
                        }
                        else{
                            $this->Services_model->del_users_log_packages_prices_by_userid($strUserIds , $strUserCategory);
                           
                        }
                    }
                    //========================================== CONVERT USERS PRICES =================================================//
                }
            }
            $msg = $this->lang->line('BE_GNRL_11');
        }
        if ($purpose == 'update_g')
        {
            $price = $this->input->post('price') ?: 0;
            $type = $this->input->post('type') ?: 0;
            $pricing = $this->input->post('pricing') ?: 0;
            $srvc = $this->input->post('srvc') ?: 0;
            $groupId = $this->input->post('groupId') ?: 0;
            if(is_numeric($price) && $price > 0)
            {
                $rwCurr = fetch_currency_data();
                if(isset($rwCurr->CurrencyId) && $rwCurr->CurrencyId > 0)
                {
                    switch($srvc)
                    {
                        case '0';
                            $tbl = 'tbl_gf_packages';
                            $col = 'PackagePrice';
                            $colId = 'PackageId';
                            $colD = 'DisablePackage';
                            $where = ' WHERE sl3lbf = 0';
                            break;
                        case '1';
                            $tbl = 'tbl_gf_packages';
                            $col = 'PackagePrice';
                            $colId = 'PackageId';
                            $colD = 'DisablePackage';
                            $where = ' WHERE sl3lbf = 1';
                            break;
                        case '2';
                            $tbl = 'tbl_gf_log_packages';
                            $col = 'LogPackagePrice';
                            $colId = 'LogPackageId';
                            $colD = 'DisableLogPackage';
                            $where = ' WHERE (1) ';
                            break;
                    }

                    $rsPrices = $this->Services_model->fetch_log_packages_by_slbf($colId , $col , $tbl , $where , $colD );
                   
                    $str = '';
                    echo $pricing.'===='.$type;
                    foreach($rsPrices as $row )
                    {
                        $newPrice = 0;
                        if($pricing == '0')
                        {
                            if($type == '0')
                                $newPrice = $row->PackPrice + $price;
                            else if($type == '1')
                                $newPrice = $row->PackPrice - $price;
                            echo $newPrice.'&'.$row->PackPrice.'<br>';
                        }
                        else if($pricing == '1')
                        {
                            if($type == '0')
                                $newPrice = $row->PackPrice + ($row->PackPrice * $price/100);
                            else if($type == '1')
                                $newPrice = $row->PackPrice - ($row->PackPrice * $price/100);
                        }
                        if(is_numeric($newPrice) && $newPrice > 0)
                        {
                            if($str != '')
                                $str .= ',';
                            $str .= "('$groupId', '".$rwCurr->CurrencyId."', '".$row->PackId."', '$newPrice', '$srvc')";
                        }
                    }
                    if($str != '')
                    {
                        $this->Services_model->del_plans_packages_prices_by_planId($groupId , $srvc);
                        $this->Services_model->insert_plans_packages_prices2($str);
                        
                        // echo "INSERT INTO tbl_gf_plans_packages_prices (PlanId, CurrencyId, PackageId, Price, ServiceType) VALUES $str";
                    }
                }
            }
            $msg = $this->lang->line('BE_GNRL_11');
        }

        echo $msg;
    }


    public function customfields(){
        $serviceType = $this->input->post('sc') ?: 0;
        switch($serviceType)
        {
            case '0':
                $hdr = $this->lang->line('BE_MENU_PCKGS');
                break;
            case '1':
                $hdr = $this->lang->line('BE_PCK_25');
                break;
            case '2':
                $hdr = $this->lang->line('BE_LBL_299');
                break;
        }
        $this->data['rsFields'] = $this->Services_model->fetch_custom_fields_by_servicetype($serviceType);
        $this->data['serviceType'] = $serviceType; 
        $this->data['hdr'] = $hdr ;
        $this->data['view'] = 'admin/custom_fields';
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function customfield(){
        $this->data['lbl'] = $this->data['fType'] = $this->data['restriction'] = $this->data['minLen'] = $this->data['maxLen'] = $this->data['instructions'] = '';
        $this->data['disable'] = $this->data['mandatory'] = $this->data['systemFld'] = $this->data['useAsQty'] = 0;

        $id = $this->input->post_get('id') ?: 0;
        $serviceType = $this->input->post_get('sc') ?: 0;
        $systemFld = 0;
        if($id > 0)
        {
            $row = $this->Services_model->fetch_custom_fields_by_fieldid($id);
           
            if (isset($row->FieldLabel) && $row->FieldLabel != '')
            {
                $this->data['lbl'] = stripslashes($row->FieldLabel);
                $this->data['fType'] = $row->FieldType;
                $this->data['disable'] = $row->DisableField;
                $this->data['mandatory'] = $row->Mandatory;
                $this->data['serviceType'] = $row->ServiceType;
                $this->data['restriction'] = $row->Restriction;
                $this->data['systemFld'] = $row->SystemField;
                $this->data['minLen'] = $row->MinLength == '0' ? '' : $row->MinLength;
                $this->data['maxLen'] = $row->MaxLength == '0' ? '' : $row->MaxLength;
                $this->data['useAsQty'] = $row->UseAsQuantity;
                $this->data['instructions'] = stripslashes($row->FInstructions);
            }
        }

        switch($serviceType)
        {
            case '0':
                $hdr = $this->lang->line('BE_MENU_PCKGS');
                break;
            case '1':
                $hdr = $this->lang->line('BE_PCK_25');
                break;
            case '2':
                $hdr = $this->lang->line('BE_LBL_299');
                break;
        }

        $this->data['hdr'] = $hdr ;
        $this->data['id'] = $id ;
        $this->data['serviceType'] = $serviceType;
        $this->data['view'] = 'admin/custom_field' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }


    public function ajxcustomfield(){
        $purpose = $this->input->post('purpose');

        if($purpose == 'save')
        {
            $fieldLbl = $this->input->post('fieldLbl');
            $disable = $this->input->post('disable');
            $fieldType = $this->input->post('fieldType');
            $srvType = $this->input->post('srvType');
            $mandatory = $this->input->post('mandatory');
            $minLen = $this->input->post('minLen');
            $maxLen = $this->input->post('maxLen');
            $inst = $this->input->post('inst');
            $useAsQty = $this->input->post('useAsQty');
            $restriction = $this->input->post('restriction');
            if(trim($minLen) == '')
                $minLen = 0;
            if(trim($maxLen) == '')
                $maxLen = 0;
            $id = $this->input->post('id');
            switch($srvType)
            {
                case '0':
                    $tbl = 'tbl_gf_codes';
                    break;
                case '1':
                    $tbl = 'tbl_gf_codes_slbf';
                    break;
                case '2':
                    $tbl = 'tbl_gf_log_requests';
                    break;
            }

            $msg = '';	
            $row = $this->Services_model->fetch_custom_fields_by_fieldlabel($fieldLbl , $id , $srvType);
          
            if ($row->TotalRecs > 0)
            {
                $msg = "Field Label '<b>".$fieldLbl."</b>'".$this->lang->line('BE_GNRL_10').'~0'; 
            }
            else
            {
                if($id == 0)
                {
                    $colName = strtoupper(str_replace(' ', '_', $fieldLbl));
                    $insert_data = array(
                        'FieldLabel' => $fieldLbl ,
                        'FieldType' => $fieldType ,
                        'FieldColName' => $colName ,
                        'Mandatory' => $mandatory ,
                        'DisableField' => $disable,
                        'ServiceType' => $srvType,
                        'MinLength' => $minLen,
                        'MaxLength' => $maxLen,
                        'FInstructions' => $inst,
                        'Restriction' => $restriction
                    );
                    $this->Services_model->insert_custom_fields($insert_data);
                   
                    if($fieldType == 'Text Box')
                    {
                        
                        $this->Services_model->alter_table_add_col($tbl , $colName);
                        if($srvType == '0' || $srvType == '2')
                            $tbl = 'tbl_gf_retail_orders';
                            $this->Services_model->alter_table_add_col($tbl , $colName);
                    }
                    else if($fieldType == 'Text Area')
                    {
                        
                        $this->Services_model->alter_table_text_col($tbl , $colName);
                        if($srvType == '0' || $srvType == '2')
                            $tbl = 'tbl_gf_retail_orders';
                            $this->Services_model->alter_table_text_col($tbl , $colName);
                    }
                    else if($fieldType == 'Drop Down' || $fieldType == 'Radio Button')
                    {
                        $this->Services_model->alter_table_add_col($tbl , $colName);
                        if($srvType == '0' || $srvType == '2')
                            $tbl = 'tbl_gf_retail_orders'; 
                            $this->Services_model->alter_table_add_col($tbl , $colName);
                           
                    }
                    $msg =  $this->lang->line('BE_GNRL_11')."~1";
                }
                else
                {
                    $update_data = array(
                        'FieldLabel' => $fieldLbl,
                        'Mandatory' => $mandatory ,
                        'DisableField' => $disable ,
                        'ServiceType' => $srvType ,
                        'MaxLength' => $maxLen ,
                        'MinLength' => $minLen ,
                        'FInstructions' => $inst ,
                        'UseAsQuantity' => $useAsQty ,
                        'Restriction' => $restriction ,
                    );
                    $this->Services_model->update_custom_fields($update_data , $id);
                  
                    $msg =  $this->lang->line('BE_GNRL_11')."~0";
                }
            }
        }	
        echo $msg;
    }

    public function customfieldvalues(){
        $id = $this->input->post_get('id') ?: 0;
        $this->data['serviceType'] = $this->input->post_get('sc') ?: 0;
        $this->data['rsValues'] = $this->Services_model->fetch_custom_field_values($id);

        $this->data['id'] = $id ;
        $this->data['view'] = 'admin/custom_field_values' ;
        $this->load->view('admin/layouts/default1' , $this->data);
      
    }

    public function customfieldvalue(){
        $id = $this->input->post_get('id') ?: 0;
        $fId = $this->input->post_get('fId') ?: 0;
        $val = '' ;
        $disable = '' ;
        if($id > 0)
        {
            $row = $this->Services_model->fetch_custom_field_values_by_reg_id($id);
            if (isset($row->RegValue) && $row->RegValue != '')
            {
                $val = stripslashes($row->RegValue);
                $disable = $row->DisableRegValue;
                $fId = $row->FieldId;
            }
        }

        $this->data['id'] = $id ;
        $this->data['fId'] = $fId ;
        $this->data['val'] = $val;
        $this->data['disable'] = $disable;

        $this->data['view'] = 'admin/custom_field_value' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function ajxcustomfieldval(){
        $purpose = $_POST['purpose'];

        if($purpose == 'save')
        {
            $val = $this->input->post('val');
            $disable = $this->input->post('disable');
            $fId = $this->input->post('fId');
            $id = $this->input->post('id');

            $msg = '';	
            $row = $this->Services_model->fetch_regvalueid_count($val , $fId , $id); 
         
            if ($row->TotalRecs > 0)
            {
                $msg = "Field Label '<b>".$val."</b>'". $this->lang->line('BE_GNRL_10')."~0"; 
            }
            else
            {
                if($id == 0)
                {
                    $insert_data = array(
                        'RegValue' => $val ,
                        'FieldId' => $fId ,
                        'DisableRegValue' => $disable
                    );
                    $this->Services_model->insert_custom_fields_values($insert_data);
                    
                    $msg =  $this->lang->line('BE_GNRL_11')."~1";
                }
                else
                {
                    $update_data = array(
                        'RegValue' => $val ,
                        'FieldId' => $fId ,
                        'DisableRegValue' => $disable ,
                    );
                    $this->Services_model->update_custom_fields_by_regvalueid($update_data , $id);
                  
                    $msg =  $this->lang->line('BE_GNRL_11')."~0";
                }
            }
        }	
        
        echo $msg;
    }

    public function smsintegration(){
        $type = $this->input->post_get('type') ?: 0;
        switch($type)
        {
            case '0':
                $header = $this->lang->line('BE_MENU_PCKGS');
                $btnRHdr = $this->lang->line('BE_CODE_2');
                break;
            case '1':
                $header = $this->lang->line('BE_PCK_25');
                $btnRHdr = $this->lang->line('BE_LBL_373');
                break;
            case '2':
                $header = $this->lang->line('BE_PCK_HD_7');
                $btnRHdr = $this->lang->line('BE_PCK_HD_6');
                break;
            case '3':
                $header = $this->lang->line('BE_MENU_USRS');
                $btnRHdr = $this->lang->line('BE_USR_HD');
                break;
        }
        $this->data['type'] = $type ;
        $this->data['btnRHdr'] = $btnRHdr;
        $this->data['header'] = $header ;
        $this->data['view'] = 'admin/sms_integration' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function ajxsmsintegration(){
        $purpose = $this->input->post_get('purpose');

        if($purpose == 'sms')
        {
            $val = $this->input->post_get('val') ?: 0;
            $type = $this->input->post_get('type') ?: 0;
            $strWhere = '';
            switch($type)
            {
                case '0':
                    $tbl = 'tbl_gf_packages';
                    $strWhere = ' WHERE sl3lbf = 0';
                    break;
                case '1':
                    $tbl = 'tbl_gf_packages';
                    $strWhere = ' WHERE sl3lbf = 1';
                    break;
                case '2':
                    $tbl = 'tbl_gf_log_packages';
                    break;
                case '3':
                    $tbl = 'tbl_gf_users';
                    break;
            }
            $this->Services_model->smsajax_custom_query($tbl , $val , $strWhere);
           
            $msg =  $this->lang->line('BE_GNRL_11');
        }	
        
        echo $msg;
    }
    
    public function resetprices(){
        $srvc = $this->input->post_get('srvc') ?: 0;
        switch($srvc)
        {
            case '0';
                $heading = $this->lang->line('BE_LBL_408');
                $tblCat = 'tbl_gf_package_category';
                $strWhereCat = " AND SL3BF = 0";
                $strWhereSrv = " AND sl3lbf = 0";
                $tblPckName = 'tbl_gf_packages';
                $colId = 'PackageId';
                $colTitle = 'PackageTitle';
                $disableCol = 'DisablePackage';
                break;
            case '1';
                $heading = $this->lang->line('BE_LBL_409');
                $tblCat = 'tbl_gf_package_category';
                $strWhereCat = " AND SL3BF = 1";
                $strWhereSrv = " AND sl3lbf = 1";
                $tblPckName = 'tbl_gf_packages';
                $colId = 'PackageId';
                $colTitle = 'PackageTitle';
                $disableCol = 'DisablePackage';
                break;
            case '2';
                $heading = $this->lang->line('BE_LBL_410');
                $tblCat = 'tbl_gf_log_package_category';
                $strWhereCat = '';
                $strWhereSrv = '';
                $tblName = 'tbl_gf_log_package_category';
                $tblPckName = 'tbl_gf_log_packages';
                $colId = 'LogPackageId';
                $colTitle = 'LogPackageTitle';
                $disableCol = 'DisableLogPackage';
                break;
        }

        $this->data['heading'] = $heading;
        $this->data['tblCat'] = $tblCat;
        $this->data['strWhereCat'] = $strWhereCat;
        $this->data['srvc'] = $srvc;
        $this->data['view'] = 'admin/reset_prices' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }


    public function ajxresetprices(){
        $purpose = $this->input->post('purpose');
        if ($purpose == 'update')
        {
            $categoryId = $this->input->post('categoryId') ?: 0;
            $serviceId = $this->input->post('serviceId') ?: 0;
            $groupId = $this->input->post('groupId') ?: 0;
            $srvc = $this->input->post('srvc') ?: 0;
            $groupPrices = $this->input->post('groupPrices') ?: 0;
            $strWhere = '';
            $strWhere1 = '';
            switch($srvc)
            {
                case '0': // IMEI services
                    $query = 'DELETE FROM  WHERE PackageId IN (SELECT PackageId FROM tbl_gf_packages WHERE sl3lbf = 0)';
                    $colPack = 'PackageId';
                    $tblPack = 'tbl_gf_packages';
                    $tblUserPr = 'tbl_gf_users_packages_prices';
                    break;
                case '1': // File services
                    $query = 'DELETE FROM tbl_gf_users_packages_prices WHERE PackageId IN (SELECT PackageId FROM tbl_gf_packages WHERE sl3lbf = 1)';
                    $colPack = 'PackageId';
                    $tblPack = 'tbl_gf_packages';
                    $tblUserPr = 'tbl_gf_users_packages_prices';
                    break;
                case '2': // Server services
                    $query = 'DELETE FROM ';
                    $colPack = 'LogPackageId';
                    $tblPack = 'tbl_gf_log_packages';
                    $tblUserPr = 'tbl_gf_users_log_packages_prices';
                    break;
            }
            if($categoryId > 0 && $serviceId > 0) // BOTH SELECTED
            {
                $strWhere = " WHERE $colPack = '$serviceId'";
                $strWhere1 = " AND PackageId = '$serviceId'";
            }
            else if($categoryId > 0 && $serviceId == 0) // ONLY CATEGORY SELECTED
            {
                $strWhere = " WHERE $colPack IN (SELECT $colPack FROM $tblPack Where CategoryId = '$categoryId')";
                $strWhere1 = " AND PackageId IN (SELECT $colPack FROM $tblPack Where CategoryId = '$categoryId')";
            }
            else if($categoryId == 0 && $serviceId == 0) // BOTH NOT SELECTED
            {
                if($srvc != '2'){
                    $strWhere = " WHERE PackageId IN (SELECT PackageId FROM tbl_gf_packages WHERE sl3lbf = '$srvc')";
                }
            }
            //echo "DELETE FROM $tblUserPr $strWhere";
            $this->Services_model->del_users_packages_prices_log($tblUserPr , $strWhere);
            
            if($groupPrices == '1')
            {
                if($groupId > 0){
                    $strWhere1 .= " AND PlanId = '$groupId'";
                }
                $this->Services_model->del_plans_packages_prices_by_servicetype($srvc , $strWhere1);
               
            }
        }

        echo $this->lang->line('BE_GNRL_11');
    }

    public function ajxgeneral(){
        $purpose = $this->input->post('purpose');
        $strWhere = '' ;
        if ($purpose == 'getsrvcs')
        {
            $categoryId = $this->input->post('categoryId') ?: 0;
            $sc = $this->input->post('sc') ?: 0;
            switch($sc)
            {
                case '0': // IMEI services
                    $strWhere .= " AND sl3lbf = 0";
                    $tblPckName = 'tbl_gf_packages';
                    $colId = 'PackageId';
                    $colTitle = 'PackageTitle';
                    $disableCol = 'DisablePackage';
                    $sortCol = 'PackOrderBy';
                    break;
                case '1': // File services
                    $strWhere .= " AND sl3lbf = 1";
                    $tblPckName = 'tbl_gf_packages';
                    $colId = 'PackageId';
                    $colTitle = 'PackageTitle';
                    $disableCol = 'DisablePackage';
                    $sortCol = 'PackOrderBy';
                    break;
                case '2': // File services
                    $tblPckName = 'tbl_gf_log_packages';
                    $colId = 'LogPackageId';
                    $colTitle = 'LogPackageTitle';
                    $disableCol = 'DisableLogPackage';
                    $sortCol = 'PackOrderBy';
                    break;
                case '3': // Retail services
                    $tblPckName = 'tbl_gf_retail_services';
                    $colId = 'PackageId';
                    $colTitle = 'PackageTitle';
                    $disableCol = 'DisablePackage';
                    $sortCol = 'OrderBy';
                    break;
            }

            $rsSrvcs = $this->Services_model->fetch_retail_services_packages($colId ,  $colTitle , $tblPckName , $disableCol , $strWhere , $categoryId , $sortCol, $colTitle);
           
            $msg = "<option value='0' selected='selected'>Please Choose Service...</option>";
            foreach($rsSrvcs as $row)
            {
                $msg .= "<option value='".$row->Id."'>".stripslashes($row->Value)."</option>";
            }
        }
        if ($purpose == 'rmvprf')
        {
            $orderNo = $this->input->post('orderNo') ?: 0;
            $sc = $this->input->post('sc') ?: 0;
            switch($sc)
            {
                case '0':
                    $tbl = 'tbl_gf_codes';
                    $colId = 'CodeId';
                    break;
                case '1':
                    $tbl = 'tbl_gf_codes_slbf';
                    $colId = 'CodeId';
                    break;
                case '2':
                    $tbl = 'tbl_gf_log_requests';
                    $colId = 'LogRequestId';
                    break;
            }
            $this->Services_model->update_codes_slbf_requests($tbl , $colId , $orderNo);
            
            $msg = "Profit has been removed for Order No. $orderNo";
        }
        echo $msg;
    }


    public function hideservices(){
        $message = '';
        $sc = $this->input->post('sc') ?: 0;
        $cldFrm = $this->input->post_get('cldFrm') ?: 0;
        $categoryId = $this->input->post_get('categoryId') ?: 0;
        $strWhereCat = '';
        $strWhere = '';
        switch($sc)
        {
            case '0': // IMEI services
                $strWhereCat = ' AND SL3BF = 0';
                $tblName = 'tbl_gf_package_category';
                $tblPckName = 'tbl_gf_packages';
                $colId = 'PackageId';
                $colTitle = 'PackageTitle';
                $disableCol = 'DisablePackage';
                $strWhere = ' AND sl3lbf = 0';
                $heading = $this->lang->line('BE_MENU_PCKGS');
                break;
            case '1': // File services
                $strWhereCat = ' AND SL3BF = 1';
                $tblName = 'tbl_gf_package_category';
                $tblPckName = 'tbl_gf_packages';
                $colId = 'PackageId';
                $colTitle = 'PackageTitle';
                $disableCol = 'DisablePackage';
                $strWhere = ' AND sl3lbf = 1';
                $heading = $this->lang->line('BE_PCK_25');
                break;
            case '2': // Server services
                $tblName = 'tbl_gf_log_package_category';
                $tblPckName = 'tbl_gf_log_packages';
                $colId = 'LogPackageId';
                $colTitle = 'LogPackageTitle';
                $disableCol = 'DisableLogPackage';
                $heading = $this->lang->line('BE_LBL_299');
                break;
        }
        if($this->input->post_get('btnSubmit') && $this->session->userdata('GSM_FSN_AdmId') && $cldFrm == '1')
        {
            $packageIds = $this->input->post_get('chkPacks') ?: 0;
            $selectedPackages = '0';
            if(is_array($packageIds))
                $selectedPackages = implode(",", $packageIds);
            
            $this->Services_model->update_log_packages($tblPckName , $categoryId);
           
            $this->Services_model->update_log_packages_by_id($tblPckName , $colId , $selectedPackages);
            $message = $this->lang->line('BE_GNRL_11');	
        }
        
        $this->data['message'] = $message;
        $this->data['heading'] = $heading ;
        $this->data['rsPacks'] = $this->Services_model->fetch_log_package_category($colId , $colTitle , $tblName , $tblPckName , $disableCol , $categoryId , $strWhere);
        $this->data['count'] = count($this->data['rsPacks']);
        $this->data['tblName'] = $tblName;
        $this->data['strWhereCat'] = $strWhereCat;
        $this->data['categoryId'] = $categoryId;
        $this->data['sc'] = $sc ;
        $this->data['view'] = 'admin/hide_services' ;
        $this->load->view('admin/layouts/default1' , $this->data);

    }

    public function unarchiveservices(){
        $message = '';
        $sc = $this->input->post_get('sc') ?: 0;
        $cldFrm = $this->input->post_get('cldFrm') ?: 0;
        $categoryId = $this->input->post_get('categoryId') ?: 0;
        $strWhere = '';
        $strWhereCat = '';
        switch($sc)
        {
            case '0': // IMEI services
                $strWhereCat = ' AND SL3BF = 0';
                $tblName = 'tbl_gf_package_category';
                $tblPckName = 'tbl_gf_packages';
                $colId = 'PackageId';
                $colTitle = 'PackageTitle';
                $disableCol = 'DisablePackage';
                $heading = $this->lang->line('BE_MENU_PCKGS');
                break;
            case '1': // File services
                $strWhereCat = ' AND SL3BF = 1';
                $tblName = 'tbl_gf_package_category';
                $tblPckName = 'tbl_gf_packages';
                $colId = 'PackageId';
                $colTitle = 'PackageTitle';
                $disableCol = 'DisablePackage';
                $heading = $this->lang->line('BE_PCK_25');
                break;
            case '2': // Server services
                $tblName = 'tbl_gf_log_package_category';
                $tblPckName = 'tbl_gf_log_packages';
                $colId = 'LogPackageId';
                $colTitle = 'LogPackageTitle';
                $disableCol = 'DisableLogPackage';
                $heading = $this->lang->line('BE_LBL_299');
                break;
        }
        if($this->input->post_get('btnSubmit') && $this->session->userdata('GSM_FSN_AdmId') && $cldFrm == '1')
        {
            $packageIds = $this->input->post_get('chkPacks') ?: 0;
            $selectedPackages = '0';
            if(is_array($packageIds))
                $selectedPackages = implode(",", $packageIds);
            if($selectedPackages == '0')
            {
                $message = 'Please select at least one service!';	
            }
            else
            {
                $this->Services_model->update_log_packages_by_archicvedpack($tblPckName ,$colId, $selectedPackages);
               
                $message = $this->lang->line('BE_GNRL_11');	
            }
        }
        if($categoryId > 0)
            $strWhere = " AND A.CategoryId = '$categoryId'";
        $this->data['rsPacks'] = $this->Services_model->fetch_log_packages_category_by_cat_id($colId , $colTitle, $tblName , $tblPckName , $strWhere);
        
        $this->data['count'] = count($this->data['rsPacks']);
        $this->data['message'] = $message;
        $this->data['heading'] = $heading;
        $this->data['strWhereCat'] = $strWhereCat;
        $this->data['tblName'] = $tblName;
        $this->data['sc'] = $sc;
        $this->data['categoryId'] = $categoryId;
        $this->data['view'] = 'admin/unarchive_services';
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function groupquickpricing(){
        $srvc = $this->input->post_get('srvc') ?: 0;
        switch($srvc)
        {
            case '0';
                $heading = 'IMEI Group Prices';
                break;
            case '1';
                $heading = 'File Group Prices';
                break;
            case '2';
                $heading = 'Server Group Prices';
                break;
        }

        $this->data['heading'] = $heading ;
        $this->data['srvc'] = $srvc;
        $this->data['view'] = 'admin/group_quick_pricing' ;
        $this->load->view('admin/layouts/default1' , $this->data);

    }

    public function serverservices(){
        $eu=0;
        $txtlqry = $message = '';
        if(!isset($start))
            $start = 0;

        if($this->input->post_get("start"))
            $start = $this->input->post_get("start");

        $eu = ($start - 0);
        $limit = $this->input->post_get('records') ?: 100;
        $txtlqry .= "&records=$limit";
        $thisp = $eu + $limit;
        $back = $eu - $limit;
        $next = $eu + $limit;

        $strWhere = '';
        $dis = $this->input->post_get('dis') ?: 0;
        $cldFrm = $this->input->post_get('cldFrm') ?: 0;
        $categoryId = $this->input->post_get('categoryId') ?: 0;
        $packageId = $this->input->post_get('packageId') ?: 0;
        $apiId = $this->input->post_get('apiId') ?: 0;
        $del = $this->input->post_get('del') ? 1 : 0;
        $srvStatus = $this->input->post_get('srvStatus') ?: '0';
        if($categoryId != '0')
            $strWhere .= " AND A.CategoryId = '$categoryId'";
        if($apiId != '0')
            $strWhere .= " AND A.APIId = '$apiId'";
        if($srvStatus != '2')
        {
            $strWhere .= " AND A.DisableLogPackage = '$srvStatus'";
            $txtlqry .= "&srvStatus=$srvStatus";
        }
        if($categoryId != '0')
        {
            $strWhere .= " AND A.CategoryId = '$categoryId'";
            $txtlqry .= "&categoryId=$categoryId";
        }
        if($packageId != '0')
        {
            $strWhere .= " AND A.LogPackageId = '$packageId'";
            $txtlqry .= "&packageId=$packageId";
        }
        if($apiId != '0')
        {
            $strWhere .= " AND A.APIId = '$apiId'";
            $txtlqry .= "&apiId=$apiId";
        }
        if($del == '1')
        {
            $id = $this->input->post_get('packId') ?: 0;
            $this->Services_model->updateLogPack($id);
            $message = 'Service has been archived successfully!';
        }
        if($this->input->post_get('btnSubmit') && $this->session->userdata('GSM_FSN_AdmId') && $cldFrm == '1')
        {
            $packageIds = $this->input->post_get('chkPacks') ?: 0;
            $currpackIds = $this->input->post_get('currPcks') ?: 0;
            $currDtTm = setDtTmWRTYourCountry();
            $selectedPackages = '0';
            if(is_array($packageIds)){
                $selectedPackages = implode(",", $packageIds);
            }
            $this->Services_model->update_log_packages_by_currpackIds($currDtTm , $currpackIds);
            $objDBCD14->executeQuery("UPDATE tbl_gf_log_packages SET DisableLogPackage = 0, EditedAt = '$currDtTm' WHERE LogPackageId IN (".$currpackIds.")");
            $this->Services_model->update_log_packages_by_selectedPackages($currDtTm , $selectedPackages);
          
            
            foreach($this->input->post('packagePrice') as $key=>$val)
            {
                if(is_float($val[0]) || is_numeric($val[0]))
                {
                    $packagePrice = $val[0];
                    $this->Services_model->update_log_packages_by_key($packagePrice ,$currDtTm , $key);
                    
                }
            }
            $message = $this->lang->line('BE_GNRL_11');	
        }
        $this->data['rsPacks'] = $this->Services_model->fetch_log_package_category_and_api($strWhere , $eu, $limit);
        
        $this->data['count'] = count($this->data['rsPacks']);

        $arrPCs = array();
        $rsPCs = $this->Services_model->fetch_precodes_by_type();
       
        foreach($rsPCs as $r)
        {
            $arrPCs[$r->ServiceId] = $r->TotalPCs;
        }

        $this->data['strWhere'] = $strWhere;
        $this->data['page_name'] = '' ;
        $this->data['back'] = $back;
        $this->data['start'] = $start ;
        $this->data['txtlqry'] = $txtlqry;
        $this->data['limit'] = $limit;
        $this->data['eu'] = $eu ;
        $this->data['pLast'] = '';
        $this->data['thisp'] = $thisp;
        $this->data['next'] = $next;
        $this->data['categoryId'] = $categoryId;
        $this->data['myNetworkId'] = '';
        $this->data['apiId'] = $apiId;
        $this->data['srvStatus'] = $srvStatus;
        $this->data['packageId'] = $packageId;
        $this->data['dis'] = $dis;
        $this->data['message'] = $message ;
        $this->data['view'] = 'admin/server_services' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function exportservices(){
        $ids = $this->input->post_get('ids') ?: 0;
        $type = $this->input->post_get('type') ?: 0;
        $strWhere = '';
        $idCol = 'PackageId';	
        $packCol = 'PackageTitle';	
        $priceCol = 'PackagePrice';	
        $timeCol = 'TimeTaken';
        $detCol = 'MustRead';
        $disCol = 'DisablePackage';
        $tblCat = 'tbl_gf_package_category';
        $tblSrv = 'tbl_gf_packages';
        if($type == '1')
        {
            $idCol = 'LogPackageId';	
            $packCol = 'LogPackageTitle';
            $priceCol = 'LogPackagePrice';
            $timeCol = 'DeliveryTime';
            $detCol = 'LogPackageDetail';
            $disCol = 'DisableLogPackage';
            $tblCat = 'tbl_gf_log_package_category';
            $tblSrv = 'tbl_gf_log_packages';
        }
        else if($type == '2')
        {
            $idCol = 'PackageId';	
            $packCol = 'PackageTitle';	
            $priceCol = 'PackagePrice';	
            $timeCol = 'TimeTaken';
            $detCol = 'MustRead';
            $disCol = 'DisablePackage';
            $tblCat = 'tbl_gf_manufacturer';
            $tblSrv = 'tbl_gf_retail_services';
        }
       
        $str = "Service ID,Category, Service Name,Price, Delivery Time, Service Detail\n";
        $rsPacks = $this->Services_model->export_services_custom_query($ids,$idCol, $packCol, $priceCol, $timeCol, $detCol , $tblCat , $tblSrv , $disCol , $strWhere);
      
        $count = count($rsPacks);
        foreach($rsPacks as $row)
        {
            $str .= $row->$idCol . ",";
            $str .= stripslashes($row->Category) . ",";
            $str .= stripslashes($row->$packCol) . ",";
            $str .= roundMe($row->$priceCol) . ",";
            $str .= stripslashes($row->$timeCol) . ",";
            $str .= stripslashes($row->$detCol) . "\n";
        }
        header("Pragma: public");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Disposition: attachment; filename="Services-'.date('Y-m-d H m s').'.csv"');
        print($str);
    }

    public function serverservice(){
        $id = $this->input->post_get('id') ?: 0;
		$tab = $this->input->post_get('tab') ?: 1;
		$this->data['tab'] = $tab;
        $applyAPI = $this->input->post_get('applyAPI') ?: 0;
        $rId = $this->input->post_get('rId') ?: 0;
        $currDtTm = $packageTitle = $strIgnoredPC = $preCodes = $extNetworkId = $redirect = $detail = $deliveryTime = $price = $message = $updatedAt = $costPrice = $supplier = $emailIDs = '';
        $apiId = $srvType = $apiSTypeId = $disablePackage = $newTplType = $successTplType = $cancelledTplType = $onlyAPIId = $pricePerQty = $cancelOrders = $verifyOrders = $categoryId = 0;
        $responseDelayTm = $cronDelayTm = $minQty = $cancelMins = $verifyMins = 1;
        $strPrCdWhr = '';
        $fName = '' ;
        $calPreCodes = '';
        $seoName= '' ;
        $metaTags = '' ;
        $metaKW = '' ;
        $sendSMS = '' ;
        $htmlTitle = '' ;
        $costPrFrmAPI = '' ;
        $apiType = '' ;
        $cldFrm = $this->input->post('cldFrm') ? $this->input->post('cldFrm') : 0;
        $del = $this->input->post_get('del') ? 1 : 0;

        if($del == '1' && $id > 0)
        {
            $PMAPIId = $this->input->post_get('PMAPIId') ?: 0;	
            $this->Services_model->del_service_api_pricing($id ,$PMAPIId , 2);

            $message = $this->lang->line('BE_GNRL_12');
        }
        if($this->input->post_get('txtTitle'))
        {
            $packageTitle = $this->input->post_get('txtTitle');
            $row = $this->Services_model->fetch_log_packages_by_title($packageTitle , $id);
            
            if (isset($row->LogPackageId) && $row->LogPackageId != '')
            {
                $message = "'$packageTitle'".$this->lang->line('BE_GNRL_10');
            }
            else
            {
                if($this->input->post('dupSrvcId') && $this->input->post['dupSrvcId'] > 0)
                {
                    $id = $this->Services_model->replicateSrvrService($this->input->post('dupSrvcId'), $packageTitle);
                    $message = 'Server Service has been replicated successfully';
                }
                else
                {
                    $categoryId = $this->input->post_get('categoryId');
                    $detail = $this->input->post_get('txtDetail');
                    $costPrice  = $this->input->post_get('txtCostPrice') ?: '0';
                    if($costPrice == '')
                        $costPrice = '0';
                    $supplier = $this->input->post_get('txtSupplier') ?: '';
                    $price = $this->input->post_get('txtPrice');
                    $deliveryTime = $this->input->post_get('txtDelTm');
                    $disablePackage =  $this->input->post_get('chkDisableLogPackage') ? 1 : 0;
                    $pricePerQty =  $this->input->post_get('chkPricePerQty') ? 1 : 0;
                    $costPrFrmAPI =  $this->input->post_get('chkFetchCPr') ? 1 : 0;
            
                    $currDtTm = setDtTmWRTYourCountry();
                    $srvType =  $this->input->post_get('rdServiceType');
                    $cancelOrders =  $this->input->post_get('cancelOrders') ? 1 : 0;
                    $verifyOrders =  $this->input->post_get('verifyOrders') ? 1 : 0;
                    $cancelMins =  $this->input->post('txtCancelTime') && $this->input->post('txtCancelTime') != '' ? $this->input->post('txtCancelTime')  : '60';
                    $verifyMins = $this->input->post('txtVerifyTime') && $this->input->post('txtVerifyTime') != '' ? $this->input->post('txtVerifyTime') : '60';
                    $redirect = $this->input->post('txtRedirection') ?: '';
                    $responseDelayTm = $this->input->post('txtResDelayTm');
                    $emailIDs = $this->input->post('txtEmails') ?: '';
                    $apiSTypeId = $this->input->post('apiServiceTypeId') ?: '0';
                    $minQty = $this->input->post('txtMinQty') ?: 1;
                    if(trim($minQty) == '')
                        $minQty = 1;
                    if($id == 0)
                    {
                        $insert_data = array(
                            'CategoryId' => $categoryId,
                            'LogPackageTitle' => $packageTitle ,
                            'LogPackageDetail' => $detail ,
                            'LogPackagePrice' => $price ,
                            'DisableLogPackage' =>  $disablePackage ,
                            'DeliveryTime' => $deliveryTime ,
                            'CronDelayTm' => $cronDelayTm ,
                            'CostPrice' => $costPrice,
                            'EditedAt' => $currDtTm,
                            'Supplier' => $supplier,
                            'CalculatePreCodes' => 0,
                            'SendSMS' => 0,
                            'PricePerQuantity' => $pricePerQty,
                            'ServiceType' => $srvType, 
                            'CancelOrders' => $cancelOrders ,
                            'VerifyOrders' => $verifyOrders ,
                            'OrderVerifyMins' => $verifyMins ,
                            'RedirectionURL' => $redirect,
                            'NewOrderEmailIDs' => $emailIDs ,
                            'CostPriceFromAPI' => $costPrFrmAPI ,
                            'MinimumQty' => $minQty ,
                            'OrderCancelMins' => $cancelMins
                        );
                          
                        $id = $this->Services_model->insert_log_packages($insert_data);	
                        $packageTitle = '';
                        $disablePackage = 0;
                        $pricePerQty = 0;
                        $deliveryTime = '';		
                        $detail = '';
                        $price = '0';
                        $categoryId = 0;
                        $costPrice = 0;
                        $supplier = '';
                        $srvType = 0;
                        $redirect = '';
                        $message = 'Server Service has been added successfully!';
                    }			
                    else
                    {
                        $update_data = array(
                            'CategoryId' => $categoryId,
                            'LogPackageTitle' => $packageTitle ,
                            'LogPackageDetail' => $detail ,
                            'LogPackagePrice' => $price ,
                            'DisableLogPackage' =>  $disablePackage ,
                            'DeliveryTime' => $deliveryTime ,
                            'CronDelayTm' => $cronDelayTm ,
                            'CostPrice' => $costPrice,
                            'EditedAt' => $currDtTm,
                            'Supplier' => $supplier,
                            'CalculatePreCodes' => 0,
                            'SendSMS' => 0,
                            'PricePerQuantity' => $pricePerQty,
                            'ServiceType' => $srvType, 
                            'CancelOrders' => $cancelOrders ,
                            'VerifyOrders' => $verifyOrders ,
                            'OrderVerifyMins' => $verifyMins ,
                            'RedirectionURL' => $redirect,
                            'NewOrderEmailIDs' => $emailIDs ,
                            'CostPriceFromAPI' => $costPrFrmAPI ,
                            'MinimumQty' => $minQty ,
                            'OrderCancelMins' => $cancelMins
                        );
                        $this->Services_model->update_log_packages_by_logpackageid($update_data , $id);
                       
                        $message = 'Server Service has been updated successfully!';
                    }
                    //=================================== RESET SERVICE DISCOUNTS ======================================//
                    if($this->input->post('chkResetDiscounts'))
                    {
                        $this->Services_model->del_users_log_packages_prices($id);
                        
                    }
                    //=================================== RESET SERVICE DISCOUNTS ======================================//
                }
            }
        }
        if ($this->input->post('hdStock') && $id > 0)
        {
            if($cldFrm == '1')
            {
                $arrPCIds = $this->input->post('chkPreCodes') ?: '';
                $strPCIds = '0';
                if(is_array($arrPCIds))
                {
                    $strPCIds = implode(',', $arrPCIds);
                    
                    $this->Services_model->del_precodes_by_ids($strPCIds);
                    $message = 'Pre Code(s) have been deleted successfully!';
                }
                else
                    $message = 'Please select a record!';
            }
            else if($cldFrm == '2')
            {
                $arrPCIds = $this->input->post('chkPreCodes') ?: '';
                $strPCIds = '0';
                if(is_array($arrPCIds))
                {
                    $strPCIds = implode(',', $arrPCIds);
                   
                    $this->Services_model->update_precode($currDt ,$strPCIds);
                    $message = 'Pre Code(s) have been assigned successfully!';
                }
                else
                    $message = 'Please select a record!';
            }
        }

        if ($this->input->post('apiId') && $id > 0)
        {
            $arrAPIId = explode('~', $this->input->post('apiId'));
            $apiId = $arrAPIId[0];
            $extNetworkId = $this->input->post('supplierPackId') != '' ?: 0;
            $cronDelayTm = $this->input->post('txtCronTiming');
            $exAPIId = $this->input->post('exAPIId');
            $exAPIServiceId = $this->input->post('exAPIServiceId');
            $apiSTypeId = $this->input->post('apiServiceTypeId') ?: '0';

            $update_data = array(
                'ExternalNetworkId' => $extNetworkId,
                'APIId' => $apiId ,
                'ServiceTypeId' => $apiSTypeId ,
                'CronDelayTm' => $cronDelayTm
            );
            $this->Services_model->update_log_packages_by_logpackageid($update_data , $id);
            
            //============================ APPLY API ON PENDING ORDERS ==================================//
            if($apiId > 0)
            {
                applyAPIOnPendingOrders('2', $apiId, $extNetworkId, $id);
            }
            //============================ APPLY API ON PENDING ORDERS ==================================//
            //=================================== CREATING API HISTORY ======================================//
            if($apiId > 0 && ($exAPIId != $apiId || $exAPIServiceId != $extNetworkId))
            {
                serviceAPIHistory($id, $apiId, $extNetworkId, $currDtTm, '2');
            }
            //=================================== CREATING API HISTORY ======================================//
            $message = 'API details have been updated successfully!';
        }

        if ($this->input->post('totalCurrencies') && $id > 0)
        {
            $totalCurrencies = $this->input->post('totalCurrencies');
            $strCurrencies = '';
            $strDefPriceNotice = '';
            $defaultCurrencyId = $this->input->post('defaultCurrId');
            $packageTitle = $this->input->post('hdPackTitle');
            
            if($price != '')
                $strDefPriceNotice = "('$defaultCurrencyId', '$id', '$packageTitle', '$price', '2', 'USER_ID')";
            
            for($j=0; $j<$totalCurrencies; $j++)
            {
                if($j > 0)
                    $strCurrencies .= ',';
                $cPrice = trim($this->input->post_get('txtCurrPrice'.$j));
                if(!is_numeric($this->input->post_get('txtPrice'.$j)))
                {
                    if($cPrice == '')
                        $cPrice = 0;
                    $strCurrencies .= "(".$id.",".$this->input->post_get('currencyId'.$j).",".$cPrice.")";
                    if($cPrice != 0)
                        $strDefPriceNotice .= ",'".$this->input->post('currencyId'.$j)."', '$id', '$packageTitle', '$cPrice', '2', 'USER_ID'";
                }
            }
            if($strCurrencies != '')
            {
                $this->Services_model->del_log_packages_currencies_by_pack_id($id);
               
                $this->Services_model->insert_log_packages_currencies($strCurrencies);
                
            }		
            if($strDefPriceNotice != '')
            {
                $rsPlanClients = $this->Services_model->fetch_distinct_users_prices($id);
                
                $strDefPriceNoticeNew = '';
                foreach($rsPlanClients as $row )
                {
                    if($strDefPriceNoticeNew != '')
                        $strDefPriceNoticeNew .= ',';
                    $strDefPriceNoticeNew .= str_replace('USER_ID', $row->UserId, $strDefPriceNotice);
                }
                if($strDefPriceNoticeNew != '')
                {
                    $this->Services_model->del_prices_notice($id);
                    $this->Services_model->insert_prices_notice($strDefPriceNoticeNew);
                }
            }
            //========================================== SAVING GROUP PRICES ==========================================//
            if($this->input->post('hdSavePrices') && $this->input->post('hdSavePrices') == '1')
            {
                if($this->input->post('totalGroups') && $this->input->post('totalGroups') > 0)
                {
                    $str = '';
                    $strPrices = '';
                    $strPriceNotice = '';
                    $tGroups = $this->input->post('totalGroups');
                    $defaultCurrencyId = $this->input->post('defaultCurrId');
                    $currencyCount = $this->input->post('currencyCount');
                    $packPrice = $this->input->post('hdPackPrice');
                    
                    for($j = 0; $j < $tGroups; $j++)
                    {
                        $dfPrice = trim($this->input->post('txtPrice'.$j.'_0'));
                        if(is_numeric($packPrice) && $defaultCurrencyId != '' && $packPrice > 0)
                        {
                            if($str != '')
                                $str .= ',';
                            if($strPrices != '')
                                $strPrices .= ',';
                            if($strPriceNotice != '')
                                $strPriceNotice .= ',';
                            $str .= "('$id', '$defaultCurrencyId', '".$this->input->post('planId'.$j)."','$dfPrice', '2')";
                            $strPrices .= "('".$this->input->post('planId'.$j)."', '$defaultCurrencyId', '$id', '$packageTitle', '$dfPrice', '2')";
                            $strPriceNotice .= "('".$this->input->post('planId'.$j)."', '$defaultCurrencyId', '$id', '$packageTitle', '$dfPrice', '2', 'USER_ID')";
                        }
                        for($z = 1; $z <= $currencyCount; $z++)
                        {
                            $price = trim($this->input->post('txtOtherPrice'.$j.'_'.$z));
                            if(is_numeric($price) && $price > 0)
                            {
                                if($str != '')
                                    $str .= ',';
                                if($strPrices != '')
                                    $strPrices .= ',';
                                if($strPriceNotice != '')
                                    $strPriceNotice .= ',';
                                $str .= "('$id', '".$this->input->post('otherCurrencyId'.$j.'_'.$z)."', 
                                    '".$this->input->post('planId'.$j)."', '$price', '2')";
                                $strPrices .= "('".$this->input->post('planId'.$j)."', '".$this->input->post('otherCurrencyId'.$j.'_'.$z)."', 
                                            '$id', '$packageTitle', '$price', '2')";
                                $strPriceNotice .= "('".$this->input->post('planId'.$j)."', '".$this->input->post('otherCurrencyId'.$j.'_'.$z)."', '$id', '$packageTitle', '$price', '2', 'USER_ID')";
                            }
                        }
                    }
                    if($str != '')
                    {
                        $this->Services_model->del_plans_packages_prices($id , 2);
                        $this->Services_model->insert_plans_packages_prices($str);
                        
                    }
                    if($strPrices != '' && $SEND_PRICE_EMAILS == '1')
                    {
                        $this->Services_model->del_plans_prices_email($planId , 2 ,$id);
                        $this->Services_model->insert_plans_prices_email($strPrices);
                        
                    }
                    if($strPriceNotice != '')
                    {
                        $rsPlanClients = $this->Services_model->fetch_userIds();
                       
                        $strPriceNoticeNew = '';
                        foreach($rsPlanClients as $row)
                        {
                            if($strPriceNoticeNew != '')
                                $strPriceNoticeNew .= ',';
                            $strPriceNoticeNew .= str_replace('USER_ID', $row->UserId, $strPriceNotice);
                        }
                        if($strPriceNoticeNew != '')
                        {
                            $this->Services_model->del_group_prices_notice(2 ,$id);
                            $this->Services_model->insert_group_prices_notice($strPriceNoticeNew);
                           
                        }
                    }
                }
            }
            //========================================== SAVING GROUP PRICES ==========================================//
            $message = 'Service Prices have been updated successfully!';
        }

        //=================================== SAVING PRICES ======================================//
        //=================================== EMAIL TEMPLATES ======================================//
        if ($this->input->post('txtNewTplSbj') && $id > 0)
        {
            $newTplType = $this->input->post('rdNewTplType');
            $sucTplType = $this->input->post('rdSuccessTplType');
            $canTplType = $this->input->post('rdCanTplType');
            $newTplSbj = $this->input->post('txtNewTplSbj');
            $newTplBody = $this->input->post('txtNewTplBody');
            $sucTplSbj = $this->input->post('txtSucTplSbj');
            $sucTplBody =$this->input->post('txtSucTplBody');
            $canTplSbj = $this->input->post('txtCanTplSbj');
            $canTplBody =$this->input->post('txtCanTplBody');
            $newSendCopy = $this->input->post('chkNewSendCopy') ? 1 : 0;
            $sucSendCopy = $this->input->post('chkSucSendCopy') ? 1 : 0;
            $canSendCopy = $this->input->post('chkCanSendCopy') ? 1 : 0;
            if($newTplType == '1')
            {
                $strTpls .= ", NewTplSubject = '$newTplSbj', NewTplBody = '$newTplBody', SendNewCopyToAdmin = '$newSendCopy'";
            }
            if($sucTplType == '1')
            {
                $strTpls .= ", SuccessTplSubject = '$sucTplSbj', SuccessTplBody = '$sucTplBody', SendSuccessCopyToAdmin = '$sucSendCopy'";
            }
            if($canTplType == '1')
            {
                $strTpls .= ", CancelTplSubject = '$canTplSbj', CancelTplBody = '$canTplBody', SendRejectCopyToAdmin = '$canSendCopy'";
            }
            
            $this->Services_model->update_log_packages_by_logpackageid_custom($newTplType ,$sucTplType, $canTplType ,$strTpls , $id);
            
            $message = 'Email Templates details have been saved successfully!';
        }

        //=================================== EMAIL TEMPLATES ======================================//

        //=================================== SMS ======================================//
        if ($this->input->post('btnSMS') && $id > 0)
        {
            $sendSMS = $this->input->post('btnSMS') ? 1 : 0;

            $update_data = array(
                'SendSMS' => $SendSMS,
            );
            $this->Services_model->update_log_packages_by_logpackageid($update_data , $id);
            
            $message = 'SMS information has been updated successfully!';
        }
        //=================================== SMS ======================================//

        //=================================== PRE CODES ======================================//
        if ($this->input->post('txtPreCodes') && $id > 0)
        {
            $arrPreCodes = explode("\n", $this->input->post("txtPreCodes"));
            $calPreCodes = $this->input->post('chkPreCodes') ? 1 : 0;
            $strPreCodes = '';
            $update_data = array(
                'CalculatePreCodes' => $calPreCodes
            );
            $this->Services_model->update_log_packages_by_logpackageid($update_data , $id);
            foreach($arrPreCodes as $key => $value)
            {
                $value = trim($value);
                if($value != '')
                {
                    $rwPC = $this->Services_model->fetch_precodes_by_service_type($value , $id); 
                   
                    if (isset($rwPC->PreCodeId) && $rwPC->PreCodeId != '')
                    {
                        $strIgnoredPC .= "$value<br />"; 
                    }
                    else
                    {
                        $insert_data = array(
                            'ServiceId' => $id ,
                            'PreCode' => $value ,
                            'ServiceType' => 2
                        );

                        $this->Services_model->insert_precodes_by_srv_id($insert_data);
                       
                    }
                }
            }
            $message = 'Pre Codes have been updated successfully!';
        }

        //=================================== PRE CODES ======================================//
        //========================================== ADDING CUSTOM FIELDS ==========================================//
        if ($this->input->post('fieldsForm') && $id > 0)
        {
            $selectedIds = sizeof($this->input->post('customFlds'));
            $strData = '';
            for($k = 0; $k < $selectedIds; $k++)
            {
                if($k > 0)
                    $strData .= ',';
                $strData .= "(".$id.",".$this->input->post('customFlds')[$k].", 2)";
            }
            $this->Services_model->del_package_custom_fields($id , 2);
           
            if($strData != '')
            {
                $this->Services_model->insert_package_custom_fields($strData);
               
            }
            $message = 'Service Fields have been updated successfully!';
        }

        //========================================== ADDING CUSTOM FIELDS ==========================================//

        //========================================== UPDATING SEO DATA ==========================================//
        if ($this->input->post('txtHTMLTitle') && $this->input->post('txtSEOName') && $id > 0)
        {
            $metaTags = $this->input->post('txtMetaTags');
            $seoName = $this->input->post('txtSEOName');
            $htmlTitle =$this->input->post('txtHTMLTitle');
            $metaKW = $this->input->post('txtMetaKW');
            $fName = $this->input->post('txtFileName');

            $update_data = array(
                'MetaKW' => $metaKW,
                'MetaTags' => $metaTags ,
                'SEOURLName' => $seoName ,
                'HTMLTitle' => $htmlTitle ,
                'FileName' => $fName
            );

            $this->Services_model->update_log_packages_by_logpackageid($update_data , $id);
        
            $message = 'SEO details have been updated successfully!';
        }

        //========================================== UPDATING SEO DATA ==========================================//


        //========================================== ADDING QUANTITY BASED PRICES ==========================================//
        if($this->input->post('btnBulkPrices'))
        {
            $rows = sizeof($this->input->post('txtPrice'));
            $tGroups = $this->input->post('ttlGroups');
            $strData = '';
            $strGData = '';
            for($k = 0; $k < $rows; $k++)
            {
                if($k > 0)
                {
                    $strData .= ',';
                    $strGData .= ',';
                    $minQty = $this->input->post('txtMaxQty')[$k-1] + 1;
                }
                else
                {
                    $minQty = '1';
                }
                $strData .= "('$id', '$minQty', '".$this->input->post('txtMaxQty')[$k]."', '".$this->input->post('txtPrice')[$k]."')";
                for($g = 1; $g <= $tGroups; $g++)
                {
                    $ctrlId = 'hdGrpId'.$g;
                    $ctrlNm = 'txtGPrice'.$g;
                    if($g > 1)
                    {
                        $strGData .= ',';
                    }
                    $strGData .= "('$id', '$minQty', '".$this->input->post('txtMaxQty')[$k]."', '".$this->input->post($ctrlId)[$k]."', '".$this->input->post($ctrlNm)[$k]."')";
                }

            }
           
            $this->Services_model->del_server_bulk_prices($id);
            $this->Serivces_model->del_server_bulk_group_prices($id);
           

            if($strData != '')
            {
                $this->Services_model->insert_server_bulk_prices($strData);
              
            }
            if($strGData != '')
            {
                $this->Services_model->insert_server_bulk_group_prices($strGData);
               
            }
            $message = $this->lang->line('BE_GNRL_11');
        }
        //========================================== ADDING QUANTITY BASED PRICES ==========================================//
        if($applyAPI == '1')
        {
            $crAPIId = $this->input->post_get('crAPIId') ?: 0;
            $crAPISrvId = $this->input->post_get('crAPISrvId') ?: 0;
            if($crAPIId > 0 && $crAPISrvId > 0)
            {
                $update_data = array(
                   'ExternalNetworkId' =>  $crAPISrvId ,
                   'APIId' => $crAPIId,
                );
                $this->Services_model->update_log_packages_by_logpackageid($update_data , $id);
                $message = $this->lang->line('BE_GNRL_11');
            }
        }

        if($id > 0)
        {
            $row = $this->Services_model->fetch_log_packages_and_api_data($id); 
          
            if (isset($row->LogPackageTitle) && $row->LogPackageTitle != '')
            {
                $categoryId = $row->CategoryId;
                $packageTitle = stripslashes($row->LogPackageTitle);
                $detail = stripslashes($row->LogPackageDetail);
                $deliveryTime = $row->DeliveryTime;
                $price = $row->LogPackagePrice;
                $costPrice = $row->CostPrice;
                $supplier = stripslashes($row->Supplier);
                //$comments = stripslashes($row->Comments);
                $disablePackage = $row->DisableLogPackage;
                $newTplType = $row->NewTplType;
                $successTplType = $row->SuccessTplType;
                $cancelledTplType = $row->CancelledTplType;
                $newTplSbj = stripslashes($row->NewTplSubject);
                $newTplBody = stripslashes($row->NewTplBody);
                $sucTplSbj = stripslashes($row->SuccessTplSubject);
                $sucTplBody = stripslashes($row->SuccessTplBody);
                $canTplSbj = stripslashes($row->CancelTplSubject);
                $costPrFrmAPI = $row->CostPriceFromAPI;
                $canTplBody = stripslashes($row->CancelTplBody);
                $newSendCopy = stripslashes($row->SendNewCopyToAdmin);
                $extNetworkId = $row->ExternalNetworkId;
                $sucSendCopy = stripslashes($row->SendSuccessCopyToAdmin);
                $canSendCopy = stripslashes($row->SendRejectCopyToAdmin);
                $updatedAt = $row->EditedAt;
                $calPreCodes = $row->CalculatePreCodes;
                $apiId = $row->APIId.'~'.$row->SendExternalId.'~'.$row->APIType;
                $onlyAPIId = $row->APIId;
                $apiType = $row->APIType;
                $extNtwkId = $row->SendExternalId;
                $sendSMS = $row->SendSMS;
                $pricePerQty = $row->PricePerQuantity;
                $srvType = $row->ServiceType;
                $responseDelayTm = $row->ResponseDelayTm;
                $cronDelayTm = $row->CronDelayTm;
                $cancelOrders = $row->CancelOrders;
                $verifyOrders = $row->VerifyOrders;
                $cancelMins = $row->OrderCancelMins;
                $verifyMins = $row->OrderVerifyMins;
                $redirect = $row->RedirectionURL;
                $htmlTitle = stripslashes($row->HTMLTitle);
                $metaKW = stripslashes($row->MetaKW);
                $seoName = $row->SEOURLName;
                $metaTags = stripslashes($row->MetaTags);
                $fName = $row->FileName;
                $emailIDs = $row->NewOrderEmailIDs;
                $apiSTypeId = $row->ServiceTypeId;
                $minQty = $row->MinimumQty;
            }
        }

        $this->data['rsCurrencies'] = $this->Services_model->fetch_currency_log_packages($id);
        
        $this->data['count'] = count( $this->data['rsCurrencies']);
        $del = $this->input->post_get('del') ? 1 : 0;
        $asgn = $this->input->post_get('asgn') ? 1 : 0;
        if($del == '1')
        {
            $this->Services_model->del_precodes_by_precodeId($rId);
            
            $message = 'Pre Code has been deleted successfully!';
        }
        if($asgn == '1')
        {
            $this->Services_model->update_precode($currDt ,$rId);
            $message = 'Pre Code has been assigned successfully!';
        }

        //========================================== ADDING FEATURES ==========================================//
        $srvcType = '2';
        if($this->input->post('packFeatures'))
        {
            $selectedIds = sizeof($this->input->post('packFeatures'));
            $strData = '';
            for($k = 0; $k < $selectedIds; $k++)
            {
                if($k > 0)
                    $strData .= ',';
                $strData .= "(".$id.",".$this->input->post('packFeatures')[$k].", '$srvcType')";
            }
            $this->Services_model->del_pack_selected_features($id , $srvcType);
           
            if($strData != '')
            {
                $this->Services_model->insert_pack_selected_features($strData);
                
            }
        }
        //========================================== ADDING FEATURES ==========================================//

        $txtlqry = '';
        $sqlwhere = '';
        $page_name = $this->input->server('PHP_SELF');

        if(!isset($start))
            $start = 0;

        if($this->input->post_get("start"))
            $start = $this->input->post_get("start");

        $eu = ($start - 0);
        $limit = 100;
        $thisp = $eu + $limit;
        $back = $eu - $limit;
        $next = $eu + $limit;
        $prdCde = $this->input->post_get('txtPrCde') ?: '';
        $prCdSt = $this->input->post_get('prCdSt') ?: '-1';
        $rId = $this->input->post_get('rId') ?: 0;
       
        if($prdCde != '')
        {
            $strPrCdWhr .= " AND PreCode LIKE '%$prdCde%'";
            $txtlqry .= "&txtPrCde=$prdCde";
        }
        if($prCdSt != '-1')
        {
            $strPrCdWhr .= " AND Assigned = '$prCdSt'";
            $txtlqry .= "&prCdSt=$prCdSt";
        }

        $txtlqry .= "&id=$id";
        $this->data['rsRpt'] = $this->Services_model->fetch_precodes_custom_query($id ,$strPrCdWhr,$eu, $limit);
       
        $this->data['totalRecords'] = count($this->data['rsRpt']);
        $totalStock = 0;
        if($this->data['totalRecords'] > 0)
        {
            $rwUAPCs = $this->Services_model->fetch_precodes_counts($strPrCdWhr , $id);
           
            $this->data['totalStock'] = $rwUAPCs->TotalPCs;
        }
        else
		{
			$this->data['totalStock'] = 0;
		}

        $this->data['limit'] = $limit;
        $this->data['strPrCdWhr'] = $strPrCdWhr;
        $this->data['thisp'] = $thisp;
        $this->data['back'] = $back;
        $this->data['next'] = $next;
        $this->data['txtlqry'] = $txtlqry;
        $this->data['start'] = $start ;
        $this->data['eu'] =$eu;
        $this->data['plast'] = '' ;
        $this->data['prdCde'] = $prdCde;
        $this->data['prCdSt'] = $prCdSt;
        $this->data['rsFtrs'] = $this->Services_model->fetch_service_pack_selected_features($id , $srvcType);
        $this->data['altAPIId'] = '';
        $this->data['fName'] = $fName;
        $this->data['calPreCodes'] = $calPreCodes;
        $this->data['seoName'] = $seoName;
        $this->data['metaTags'] = $metaTags;
        $this->data['metaKW'] = $metaKW;
        $this->data['sendSMS'] = $sendSMS;
        $this->data['htmlTitle'] = $htmlTitle;
        $this->data['apiSTypeId'] =$apiSTypeId;
        $this->data['cronDelayTm'] = $cronDelayTm;
        $this->data['costPrice'] = $costPrice;
        $this->data['pricePerQty'] = $pricePerQty;
        $this->data['costPrFrmAPI'] = $costPrFrmAPI;
        $this->data['verifyOrders'] = $verifyOrders;
        $this->data['verifyMins'] = $verifyMins;
        $this->data['disablePackage'] = $disablePackage;
        $this->data['minQty'] = $minQty;
        $this->data['supplier'] = $supplier;
        $this->data['deliveryTime'] = $deliveryTime;
        $this->data['redirect'] = $redirect;
        $this->data['emailIDs'] = $emailIDs;
        $this->data['srvType'] = $srvType;
        $this->data['responseDelayTm'] = $responseDelayTm;
        $this->data['detail'] = $detail;
        $this->data['cancelOrders'] = $cancelOrders;
        $this->data['cancelMins'] = $cancelMins;
        $this->data['apiType'] = $apiType;
        $this->data['extNetworkId'] = $extNetworkId;
        $this->data['onlyAPIId'] = $onlyAPIId;
        $this->data['categoryId'] = $categoryId;
        $this->data['packageTitle'] = $packageTitle;
        $this->data['id'] = $id;
        $this->data['cldFrm'] = $cldFrm;
        $this->data['strIgnoredPC'] = $strIgnoredPC;
        $this->data['updatedAt'] = $updatedAt;
        $this->data['message'] = $message ;
        $this->data['price'] = $price ;


        $this->data['srvrservices_tab_general'] = $this->load->view('admin/srvrservices_tab_general', $this->data, TRUE);
		$this->data['srvrservices_tab_prices'] = $this->load->view('admin/srvrservices_tab_prices', $this->data, TRUE);
		$this->data['srvrservices_tab_api'] = $this->load->view('admin/srvrservices_tab_api', $this->data, TRUE);
		$this->data['srvrservices_tab_fields'] = $this->load->view('admin/srvrservices_tab_fields', $this->data, TRUE);
	    $this->data['services_tab_seo'] = $this->load->view('admin/services_tab_seo', $this->data, TRUE);
	    $this->data['srvrservices_tab_precodes'] = $this->load->view('admin/srvrservices_tab_precodes', $this->data, TRUE);
        $this->data['srvrservices_tab_sms'] = $this->load->view ('admin/srvrservices_tab_sms', $this->data, TRUE);
	    $this->data['srvrsrvcs_precodes'] = $this->load->view ('admin/srvrsrvcs_precodes', $this->data, TRUE);
 
        $this->data['view'] = 'admin/server_service';
        $this->load->view('admin/layouts/default1' , $this->data);

    }


    public function server_qty_bulk(){
        $id = $this->input->post_get('id') ?: 0;
        $price = '';
        if($id > 0)
        {
			$rsBulkPrices = $this->Services_model->fetch_server_bulk_prices($id);
			$this->data['rsBulkPrices'] = $rsBulkPrices;
            $count = count($rsBulkPrices);
            if($count == 0)
            {
                $row = $this->Services_model->fetch_logpackageprice($id);
              
                if (isset($row->LogPackagePrice) && $row->LogPackagePrice != '')
                {
                    $price = $row->LogPackagePrice;
                }
            }
            $rwCrncy = fetch_currency_data();
            $MY_CURRENCY_ID = 0;
            if (isset($rwCrncy->CurrencyId) && $rwCrncy->CurrencyId != '')
            {
                $MY_CURRENCY_ID = $rwCrncy->CurrencyId;
            }
            $PLAN_PRICES_PER_PACK = getPlansPricesForService($id, '2');
            $GROUP_PRICES = array();
            $rs = $this->Services_model->fetch_server_bulk_prices($id);

            foreach($rs as $row)
            {
                $index = $row->MinQty.'-'.$row->MaxQty.'-'.$row->Price;
                //$GROUP_PRICES[$index] = roundMe($row->Price);
                $GROUP_PRICES[$index] = $row->Price;
            }

            $rsGroups = fetch_price_plan_data1();
            $this->data['totalGroups'] = count($rsGroups);
            $strGroupHdrs = '';
            $strGroupFlds = '';
            $strHGroupFlds = '';
            $i = 1;
            $arrGroups = array();

            foreach($rsGroups as $rw )
            {
                $strGroupHdrs .= '<th nowrap="nowrap">'.stripslashes($rw->PricePlan).'</th>';
                $nm = 'txtGPrice'.$i.'[]';
                $idNm = 'hdGrpId'.$i.'[]';
                $val = '';
                $idVal = $rw->PricePlanId;
                if(isset($PLAN_PRICES_PER_PACK[$rw->PricePlanId][$MY_CURRENCY_ID]) && $PLAN_PRICES_PER_PACK[$rw->PricePlanId][$MY_CURRENCY_ID] != '')
                    $val = $PLAN_PRICES_PER_PACK[$rw->PricePlanId][$MY_CURRENCY_ID];

                $strHGroupFlds .= '<td><input type="text" placeholder = "Price" class="form-control" maxlength="10" name="'.$nm.'" value="'.$val.'" /><input type="hidden" name="'.$idNm.'" value="'.$idVal.'" /></td>';
                $i++;
                $arrGroups[$rw->PricePlanId] = $rw->PricePlanId;
            }
			$this->data['arrGroups'] = $arrGroups;


        }
        $this->data['strHGroupFlds'] = $strHGroupFlds;
		$this->data['price'] = $price;
		$this->data['id'] = $id;
        $this->data['strGroupHdrs'] = $strGroupHdrs;
        $this->data['view'] = 'admin/server_qty_bulk' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function logrequest(){
        $id = $this->input->post_get('id') ?: 0;
        $uName = '';
        $pckTitle = '';
        $pRecord = '';
        $statusId = 0;
        $credits = '';
        $notes = '';
        $orderIdFrmServer = '';
        $message = '';
        $comments = '' ;
        if ($this->input->post('txtCode'))
        {
            $code = $this->input->post('txtCode');
            $notes = $this->input->post('txtNotes');
            $comments = $this->input->post('txtComments');
            $statusId = $this->input->post('statusId');
            $userId = $this->input->post('userId');
            $codeCr =$this->input->post('codeCr');
            $uName = $this->input->post('txtUN') ?: '';
            $boxUN = $this->input->post('txtBoxUN') ?: '';
            $serial = $this->input->post('txtSerial') ?: '';
            $strDeduct = '';
            $currDtTm = setDtTmWRTYourCountry();

            $pckTitle = $this->input->post('pckTitle');
            if($statusId == '3')
            {
                if(serverOrderRefunded($id) == '0')
                {
                    $dec_points = refundServerCredits($userId, $id, $pckTitle, $currDtTm, $codeCr, '1');
                    $strDeduct = ", Refunded = '1'";
                }
            }
            else
            {
                if(serverOrderRefunded($id) == '1')
                {
                    rebateServerCredits($userId, $id, $pckTitle, $currDtTm, $codeCr, '1');
                }
                $strDeduct = ", Refunded = '0'";
            }

            $col_val = $historyData = '';
            $ttlFields = $this->input->post("totalCustomFields") ?: '0';
            for($x = 1; $x <= $ttlFields; $x++)
            {
                $col = $this->input->post('colNm'.$x) ?: '';
                if($col != '')
                {
                    $val = $this->input->post('fld'.$x) ?: '';
                    $col_val .= ", $col = '$val'";
                    if($historyData != '')
                        $historyData .= '<br />';
                    $historyData .= $this->input->post('lbl'.$x).": ".$val;
                }
            }
            $this->Services_model->update_log_requests_by_id($statusId , $code , $notes , $comments , $currDtTm , $historyData , $strDeduct , $col_val , $id);
            
            if($statusId == '2' || $statusId == '3')
            {
                $row = $this->Services_model->fetch_log_requests_users_code_status($id);
              
                if (isset($row->UserEmail) && $row->UserEmail != '')
                {
                    if($statusId == '2')
                    {
                        send_push_notification('Your Server order has been completed!', $row->UserId);
                        successfulServerOrderEmail($row->UserEmail, stripslashes($row->CustomerName), $pckTitle, $code, $row->RequestedAt, $packageId, $row->AlternateEmail, $codeCr, $row->UserId, stripslashes($row->OrderData), $row->Comments, $id);
                        if($row->SMS_User == '1' && $_POST["SMS_Pack"] == '1' && $row->Phone!= '' && $PHONE_ADMIN != '')
                        {
                            checkAndSendSMS($row->Phone, $PHONE_ADMIN, '9', stripslashes($row->CustomerName), $pckTitle, '', $code, $row->RequestedAt, $row->Comments);
                        }
                    }
                    else if($statusId == '3')
                    {
                        send_push_notification('Your Server order has been cancelled!', $row->UserId);
                        rejectedServerOrderEmail($row->UserEmail, stripslashes($row->CustomerName), $pckTitle, $code, $row->RequestedAt, $packageId, $row->AlternateEmail, $codeCr, $dec_points, $objDBCD14, stripslashes($row->OrderData), $row->Comments, $id);
                        if($row->SMS_User == '1' && $_POST["SMS_Pack"] == '1' && $row->Phone!= '' && $PHONE_ADMIN != '')
                        {
                            checkAndSendSMS($row->Phone, $PHONE_ADMIN, '15', stripslashes($row->CustomerName), $pckTitle, '', $code, $row->RequestedAt, $row->Comments, $objDBCD14);
                        }
                    }
                }
            }
            $message = $this->lang->line('BE_GNRL_11');
        }
        if($id > 0)
        {
            $ROW_SERVER_ORDER = $this->Services_model->fetch_log_requests_packages_users_by_id($id);
           
            if (isset($ROW_SERVER_ORDER->UserId) && $ROW_SERVER_ORDER->UserId != '')
            {
                $userId = $ROW_SERVER_ORDER->UserId;
                $code = stripslashes($ROW_SERVER_ORDER->Code);
                $pckTitle = stripslashes($ROW_SERVER_ORDER->LogPackageTitle);
                $statusId = $ROW_SERVER_ORDER->StatusId;
                $credits = $ROW_SERVER_ORDER->Credits;
                $notes = stripslashes($ROW_SERVER_ORDER->Comments);
                $comments = stripslashes($ROW_SERVER_ORDER->Comments1);
                $SMS_Pack = $ROW_SERVER_ORDER->SendSMS;
                $orderDt = $ROW_SERVER_ORDER->RequestedAt;
                $replyDtTm = $ROW_SERVER_ORDER->ReplyDtTm;
                $orderIdFrmServer = $ROW_SERVER_ORDER->OrderIdFromServer;
                $msg4mSrvr = $ROW_SERVER_ORDER->MessageFromServer;
                $sent2OtherSrvr = $ROW_SERVER_ORDER->CodeSentToOtherServer;
                $apiName = stripslashes($ROW_SERVER_ORDER->OrderAPIName);
                $updatedBy = stripslashes($ROW_SERVER_ORDER->LastUpdatedBy);
                $userName = $ROW_SERVER_ORDER->UserName;
            }
            $this->data['rsFields'] = $this->Services_model->fetch_custom_field_by_disablefield();
            
        }
        $this->data['ROW_SERVER_ORDER'] = $ROW_SERVER_ORDER;
        $this->data['orderDt'] = $orderDt;
        $this->data['replyDtTm'] = $replyDtTm;
        $this->data['sent2OtherSrvr'] = $sent2OtherSrvr;
        $this->data['apiName'] = $apiName;
        $this->data['orderIdFrmServer'] = $orderIdFrmServer;
        $this->data['msg4mSrvr'] = $msg4mSrvr;
        $this->data['updatedBy'] = $updatedBy;
        $this->data['code'] = $code;
        $this->data['notes'] = $notes;
        $this->data['comments'] = $comments;
        $this->data['id'] = $id ;
        $this->data['userId'] = $userId;
        $this->data['credits'] = $credits;
        $this->data['pckTitle'] = $pckTitle;
        $this->data['SMS_Pack'] = $SMS_Pack;
        $this->data['userName'] = $userName;
        $this->data['message'] = $message ;

        $this->data['serverorder_tab1'] = $this->load->view('admin/serverorder_tab1' , $this->data , TRUE);
        $this->data['order_tab2'] = $this->load->view('admin/order_tab2' , $this->data , TRUE);
        $this->data['view'] = 'admin/log_request' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }


    public function ajxaltapi(){
        if($this->input->server('HTTP_X_REQUESTED_WITH') && $this->input->server('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest' )
        {
            $purpose = $this->input->post('purpose');
            $msg = '';
            if($purpose == 'save')
            {
                $id = $this->input->post('id') ?: 0;
                $sc = $this->input->post('sc') ?: 0;
                $apiId = $this->input->post('apiId') ?: 0;
                $srvcId = $this->input->post('srvcId') ?: 0;
                $altAPISrvcId = $this->input->post('altAPISrvcId') ?: 0;
               
                $row = $this->Services_model->fetch_alternate_apis_by_altapiid($apiId , $id , $srvcId , $sc);
                if (isset($row->AltAPISrvcId) && $row->AltAPISrvcId != '')
                    $msg = "API already exists against the service, it can not be added again!|0"; 
                else
                {
                    if($altAPISrvcId == 0)
                    {
                        $rw = $this->Services_model->fetch_alternate_apis_max();
                        
                        $sortBy = $rw->AltAPISortBy + 1;
                        $insert_data = array(
                            'AltServiceId' => $id,
                            'AltAPIId' => $apiId,
                            'AltAPIServiceId' =>$srvcId,
                            'AltServiceType' => $sc,
                            'AltAPISortBy' => $sortBy
                        );
                        $this->Services_model->insert_alternate_apis($insert_data);
                     
                        $msg =  'Alternate API has been added successfully!|1';
                    }			
                    else
                    {
                        $update_data = array(
                            'AltAPIId' => $apiId,
                            'AltAPIServiceId' => $srvcId,
                        );
                        $this->Services_model->update_alternate_apis_by_altapi($altAPISrvcId);
                       
                        $msg =  'Alternate API has been updated successfully!|1';
                    }
                }
            }	
            echo $msg;
        }
        else
        {
            echo 'Direct access not allowed.';
        }
    }

    public function alternateapi(){
        $MY_SERVICE_ID = $this->input->post_get('id') ?: 0;
        $sc = $this->input->post_get('sc') ?: 0;
        $pn = $this->input->post_get('pn') ? str_replace('S_P_', ' ', check_input($this->input->post_get('pn'), $this->db->conn_id)) : '';
        $altAPISrvcId = $this->input->post_get('altId') ? check_input($this->input->post_get('altId'), $this->db->conn_id) : 0;
        $altAPIId = 0;
        $altServiceId = 0;
        if($MY_SERVICE_ID > 0 && $altAPISrvcId != '0')
        {
            $row = $this->Services_model->fetch_alternate_api_by_srvcid($altAPISrvcId);
            
            if (isset($row->AltAPIId) && $row->AltAPIId != '')
            {
                $altAPIId = $row->AltAPIId;
                $altServiceId = $row->AltAPIServiceId;
            }
        }

        $this->data['altAPIId'] =$altAPIId;
        $this->data['sc'] = $sc ;
        $this->data['altServiceId'] = $altServiceId;
        $this->data['heading'] = '';
        $this->data['MY_SERVICE_ID'] = $MY_SERVICE_ID;
		$this->data['id'] = $MY_SERVICE_ID;
		$this->data['altAPISrvcId'] = $altAPISrvcId;
        $this->data['pn'] = $pn ;
        $this->data['view'] = 'admin/alternate_api' ;
        $this->load->view('admin/layouts/default1' , $this->data);

    }

    public function resetpackprices(){
        $userId = $this->input->post_get('userId') ?: 0;
        $sc = $this->input->post_get('sc') ?: 0;
        $message = '' ;
        if ($userId > 0)
        {
            $tbl = 'tbl_gf_users_packages_prices';
            if($sc == '2')
                $tbl = 'tbl_gf_users_log_packages_prices';
            $this->Services_model->executeQuery("DELETE FROM $tbl WHERE UserId = '$userId'");
            $message = $this->lang->line('BE_GNRL_11');
        }
        $this->data['userId'] = $userId ;
        $this->data['message'] = $message ;

        $this->data['view'] = 'admin/reset_pack_prices';
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function set_packs_for_user(){
        $userId = $this->input->post_get('userId') ?: 0;
        $message = '';
        $count = 0;
        $REMOVED_PACKS = array();
        $sc = $this->input->post_get('sc') ?: 0;
        $this->data['sc'] = $sc;
        $categoryId = $this->input->post_get('categoryId') ?: 0;
        switch($sc)
        {
            case '0': // IMEI services
                $strWhere = " AND sl3lbf = 0 AND ArchivedPack = '0'";
                $strWhereCat = " AND SL3BF = 0";
                $tblName = 'tbl_gf_package_category';
                $tblPckName = 'tbl_gf_packages';
                $colId = 'PackageId';
                $colTitle = 'PackageTitle';
                $disableCol = 'DisablePackage';
                break;
            case '1': // File services
                $strWhere = " AND sl3lbf = 1 AND ArchivedPack = '0'";
                $strWhereCat = " AND SL3BF = 1";
                $tblName = 'tbl_gf_package_category';
                $tblPckName = 'tbl_gf_packages';
                $colId = 'PackageId';
                $colTitle = 'PackageTitle';
                $disableCol = 'DisablePackage';
                break;
            case '2': // Server services
                $strWhere = '';
                $tblName = 'tbl_gf_log_package_category';
                $tblPckName = 'tbl_gf_log_packages';
                $colId = 'LogPackageId';
                $colTitle = 'LogPackageTitle';
                $disableCol = 'DisableLogPackage';
                break;
        }
        if($this->input->post_get('btnSave'))
        {
            $packIds = $this->input->post('chkPacks');
            $ids = $this->input->post('catBasedSrvcs');
            $totalPacks = count($packIds);
            $str = '';
            for ($j = 0; $j < $totalPacks; $j++)
            {
                if($j > 0)
                    $str .= ',';
                $str .= "(".$userId.",".$packIds[$j].", '$sc')";
            }
            $this->Services_model->executeQuery("DELETE FROM tbl_gf_user_packages WHERE UserId = '$userId' AND PackageId IN ($ids) AND ServiceType = '$sc'");
            //last_query();
            if($str != '') {
				$this->Services_model->executeQuery("INSERT INTO tbl_gf_user_packages (UserId, PackageId, ServiceType) VALUES " . $str);
				//last_query();
			}
            $message = $this->lang->line('BE_GNRL_11');
        }

        if($userId > 0)
        {
            $rsrmvdPcks = $this->Services_model->selectData("SELECT PackageId FROM tbl_gf_user_packages WHERE UserId = '$userId' AND ServiceType = '$sc'");
            foreach($rsrmvdPcks as $row )
            {
                $REMOVED_PACKS[$row->PackageId] = '1';
            }
        }
        if($categoryId != '0')
        {
            $this->data['rsPacks'] = $this->Services_model->selectData("SELECT $colId AS PackageId, $colTitle As PackageTitle FROM $tblPckName WHERE CategoryId = '$categoryId' AND $disableCol = 0 $strWhere ORDER BY PackOrderBy, $colTitle");
            $count = count($this->data['rsPacks']);
        }

        $this->data['rsCategories']=$this->Services_model->selectData("SELECT CategoryId AS Id, Category AS Value FROM $tblName WHERE DisableCategory = 0 AND ArchivedCategory = 0 $strWhereCat ORDER BY OrderBy, Category");
        $this->data['count'] = $count;
        $this->data['sc'] = $sc;
        $this->data['categoryId'] = $categoryId;
        $this->data['REMOVED_PACKS'] = $REMOVED_PACKS;
        $this->data['userId'] = $userId;
        $this->data['message'] =  $message ;
		$this->data['strCatBasedSrvcs'] =  "0";
        $this->data['view'] = 'admin/set_packs_for_user' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function userpackageprices()
    {

        $userId = $this->input->post_get('userId') ?: 0;
        $this->data['userId'] = $userId;
        $sc = $this->input->post_get('sc') ?: 0;
		$this->data['sc'] = $sc;
        $categoryId = $this->input->post_get('categoryId') ?: 0;
        $strWhere = '';
        $strWhereCat = '';
        $defaultCurrency = '0';
        $conversionRate = 0;
        $userName = '';
        $this->data['userName'] = $userName;
        $this->data['planId'] = "";
        $this->data['plan'] = "";
        $pricePlanId = 0;
        $count = 0;
        $pricePlan = '';
        $message = '';
        $tblName = '';
        $strWhereCat = '';
        switch ($sc) {
        case '0': // IMEI services
        $strWhere .= " AND sl3lbf = 0 AND ArchivedPack = '0'";
        $strWhereCat = " AND SL3BF = 0";
        $tblName = 'tbl_gf_package_category';
        $tblPckName = 'tbl_gf_packages';
        $tblUserPckPricesNm = 'tbl_gf_users_packages_prices';
        $colId = 'PackageId';
        $colTitle = 'PackageTitle';
        $colPrice = 'PackagePrice';
        $disableCol = 'DisablePackage';
        $servType = 'IMEI';
        break;
        case '1': // File services
        $strWhere .= " AND sl3lbf = 1 AND ArchivedPack = '0'";
        $strWhereCat = " AND SL3BF = 1";
        $tblName = 'tbl_gf_package_category';
        $tblPckName = 'tbl_gf_packages';
        $tblUserPckPricesNm = 'tbl_gf_users_packages_prices';
        $colId = 'PackageId';
        $colTitle = 'PackageTitle';
        $colPrice = 'PackagePrice';
        $disableCol = 'DisablePackage';
        $servType = 'File';
        break;
        case '2': // File services
        $tblName = 'tbl_gf_log_package_category';
        $tblPckName = 'tbl_gf_log_packages';
        $tblUserPckPricesNm = 'tbl_gf_users_log_packages_prices';
        $colId = 'LogPackageId';
        $colTitle = 'LogPackageTitle';
        $colPrice = 'LogPackagePrice';
        $disableCol = 'DisableLogPackage';
        $servType = 'Server';
        break;
        }
        $this->data['strWhereCat'] = $strWhereCat;
        $this->data['tblName'] = $tblName;

        $this->data['categoriesRecords'] = $this->Services_model->selectData("SELECT CategoryId AS Id, Category AS Value FROM $tblName WHERE DisableCategory = 0 AND ArchivedCategory = 0 $strWhereCat ORDER BY OrderBy, Category");
        if ($this->input->post_get('totalPackages')) {

        $str = '';
        $strPacks = '0';
        $arrPckIds = $this->input->post_get('chkPrice') ?: 0;
        $totalPckIds = 0;
        //if (is_array($arrPckIds))
        $totalPckIds = $this->input->post_get('totalPackages');		//count($arrPckIds);
			//echo "total packs=$totalPckIds";
        for ($i = 0; $i < $totalPckIds; $i++) {
			$str = '';
			$currentPack = '';
			//echo "here";
				$price = check_input($this->input->post_get('txtPrice' . $i));
			//if ($i > 0)
				//$str .= ',';

			if (is_numeric($price)) {
				$str .= "('$userId', '" . check_input($this->input->post_get('packageId' . $i)) . "', '$price')";
				$strPacks .= ", " . check_input($this->input->post_get('packageId' . $i));
				$currentPack = check_input($this->input->post_get('packageId' . $i));
			}
			//echo $str . "<br>" . $strPacks;
			if ($str != '') {
				$this->Services_model->executeQuery("DELETE FROM $tblUserPckPricesNm WHERE UserId = '$userId' AND $colId IN ($currentPack)");
				$this->Services_model->executeQuery("INSERT INTO $tblUserPckPricesNm (UserId, $colId, Price) VALUES $str");
				$message =  $this->lang->line('BE_GNRL_11');
			}

		}
        //=============================== SEND EMAILS TO CLIENT ABOUT PRICE CHANGE ===============================================//
        if ($strPacks != '0') {
        $row = $this->Services_model->getRow("SELECT CONCAT(FirstName, ' ', LastName) AS ClientName, UserEmail, CurrencyAbb FROM tbl_gf_users A, tbl_gf_currency B 
        WHERE A.CurrencyId = B.CurrencyId AND UserId = '$userId'");
        if (isset($row->UserEmail) && $row->UserEmail != '') {
        $emailMsg1 = 'Dear ' . $row->ClientName . ',<br /><br />Following ' . $servType . ' services prices have been changed in your account.<br /><br />';
        $rsUserPrices = $this->Services_model->selectData("SELECT A.$colId AS PackId, $colTitle AS Pack, Price FROM $tblPckName A, $tblUserPckPricesNm B WHERE A.$colId = B.$colId 
        AND A.$colId IN ($strPacks) AND UserId = '$userId' ORDER BY PackOrderBy, $colTitle");
        $strPrices = '';
        $emailMessage = '';
        $emailMsg = "";
        foreach ($rsUserPrices as $rowPr) {
			$emailMsg .= 'New Price for ' . stripslashes($rowPr->Pack) . ' is <b>' . $rowPr->Price . ' ' . $row->CurrencyAbb . '</b><br /><br />';
			$emailMessage = 'New Price for ' . stripslashes($rowPr->Pack) . ' is <b>' . $rowPr->Price . ' ' . $row->CurrencyAbb . '</b><br /><br />';
			//if ($strPrices != '')
			//$strPrices .= ',';
			$strPrices = "('" . $rowPr->PackId . "', '" . check_input($emailMessage) . "', '$sc', '$userId')";

			if ($strPrices != '') {
				$this->Services_model->executeQuery("DELETE FROM tbl_gf_client_prices_notice WHERE PackageId IN ($rowPr->PackId) AND ServiceType = '$sc' AND UserId = '$userId'");
				$this->Services_model->executeQuery("INSERT INTO tbl_gf_client_prices_notice (PackageId, Message, ServiceType, UserId) VALUES $strPrices");
			}
        }

        if ($this->data['rsStngs']->ServicePriceEmails == '1') {
        $arr = getEmailDetails();
        sendGeneralEmail($row->UserEmail, $arr[0], $arr[1], 'Customer', 'Service Price Changed', '', $emailMsg1 . $emailMsg);
        }


        }
        }
        //=============================== SEND EMAILS TO CLIENT ABOUT PRICE CHANGE ===============================================//
        }
        $row = $this->Services_model->getRow("SELECT A.PricePlanId, UserName, PricePlan, CurrencyId FROM tbl_gf_users A LEFT JOIN tbl_gf_price_plans B ON (A.PricePlanId = 
        B.PricePlanId) WHERE UserId = '$userId'");
        if (isset($row->UserName) && $row->UserName != '') {
        $pricePlanId = $row->PricePlanId;
        $pricePlan = $row->PricePlan;
        $userCurrencyId = $row->CurrencyId;
        $this->data['pricePlanId'] = $pricePlanId;
        $this->data['pricePlan'] = $pricePlan;
        $this->data['userCurrencyId'] = $userCurrencyId;
        }
        if ($userCurrencyId == '') {
        $currencyData = getDefaultCurrency();
        $arr = explode(',', $currencyData);
        $userCurrencyId = $arr[0];
        $userCurrency = $arr[1];
        $defaultCurrency = '1';
        $this->data['userCurrency'] = $userCurrency;
        $this->data['defaultCurrency'] = $defaultCurrency;
        } else {
        $currencyData = getCurrencyData($userCurrencyId);
        $arr = explode('|', $currencyData);
        $userCurrency = $arr[0];
        $this->data['conversionRate'] = $conversionRate = $arr[1];
        $this->data['userCurrency'] = $userCurrency;
        $this->data['defaultCurrency'] = $userCurrency;
        }
        if ($categoryId != '0') {
        $this->data['categoryId'] = $categoryId;
        $PACK_PRICES_USER = array();
        $PACK_PRICES_PLAN = array();
        $PACK_ORIGINAL_PRICES = array();
        $PACK_ORIGINAL_PRICES = getPackPricesInOtherCurrency($userCurrencyId, $sc);
        $rsUserSrvcs = $this->Services_model->selectData("SELECT $colId AS PackageId, $colTitle AS PackageTitle, $colPrice AS PackagePrice FROM $tblPckName WHERE 
        CategoryId = '$categoryId' AND $disableCol = 0 $strWhere ORDER BY PackOrderBy, $colTitle");
        $this->data['count'] = count($rsUserSrvcs);
        $this->data['rsUserSrvcs'] = $rsUserSrvcs;
        //============================================== PICK PRICES ===============================================//
        $strPackIds = '0';
        $rsUserSrvcIds = $this->Services_model->selectData("SELECT $colId AS PackageId FROM $tblPckName WHERE CategoryId = '$categoryId' AND $disableCol = 0 $strWhere");
        foreach($rsUserSrvcIds as $row) {
        if ($strPackIds == '')
        $strPackIds = $row->PackageId;
        else
        $strPackIds .= ', ' . $row->PackageId;
        }
        $this->data['strPackIds'] = $strPackIds;
        $rsSrvcsPrices = $this->Services_model->selectData("SELECT $colId AS PackageId, Price FROM $tblUserPckPricesNm WHERE UserId = '$userId' AND $colId IN ($strPackIds)");
        foreach($rsSrvcsPrices as $row) {
        $PACK_PRICES_USER[$row->PackageId] = $row->Price;
        }
        $this->data['PACK_PRICES_PLAN'] = $PACK_PRICES_PLAN;
        $this->data['PACK_PRICES_USER'] = $PACK_PRICES_USER;

        if ($pricePlanId > 0) // Getting Plans Prices
        {
        $rsPlanPrices = $this->Services_model->selectData("SELECT PackageId, Price FROM tbl_gf_plans_packages_prices WHERE PackageId IN ($strPackIds) AND PlanId = '$pricePlanId' 
        AND CurrencyId = '$userCurrencyId' AND ServiceType = '$sc'");
        foreach($rsPlanPrices as $row) {
        $PACK_PRICES_PLAN[$row->PackageId] = $row->Price;
        }
        $this->data['rsPlanPrices'] = $rsPlanPrices;

        $PACK_PRICES_PLAN_DEFAULT = array();
        $rsDefaultPlanPrices = $this->Services_model->selectData("SELECT PackageId, Price FROM tbl_gf_plans_packages_prices A, tbl_gf_currency B WHERE A.CurrencyId = B.CurrencyId AND 
        DefaultCurrency = 1 AND PackageId IN ($strPackIds) AND PlanId = '$pricePlanId' AND ServiceType = '$sc'");
        foreach ($rsDefaultPlanPrices as $row) {
        $PACK_PRICES_PLAN_DEFAULT[$row->PackageId] = $row->Price;
        }
        $this->data['PACK_PRICES_PLAN_DEFAULT'] = $PACK_PRICES_PLAN_DEFAULT;

        }
        $this->data['NoData'] = 0;
        }
        else
        {
        $this->data['NoData'] = 1;
        $this->data['categoryId'] = 0;
        $this->data['count'] = 0;
        }
        $this->data['message'] = $message;
        $this->data['pricePlan'] = $pricePlan;
        $this->data['view'] = 'admin/userpackageprices';
        $this->load->view('admin/layouts/default1', $this->data);
    }

    public function setpriceswithapi(){
        $serviceId = $this->input->post_get('serviceId') ?: 0;
        $srvcAPIId = $this->input->post_get('srvcAPIId') ?: 0;
        $sType = $this->input->post_get('sType') ?: 0;
        $PRICES = array();

        if($serviceId > 0)
        {
           
            $rsPrices =$this->Services_model->fetch_service_api_pricing($serviceId , $srvcAPIId , $sType);
            foreach($rsPrices as $row)
            {
                $PRICES[$row->ServiceId][$row->GroupId] = $row->PriceMargin.'|'.$row->MarginType;
            }

            $this->data['rsGroups'] = $this->Services_model->fetch_price_by_disablePricePlan();
             
            $this->data['totalGroups'] = count($this->data['rsGroups']);

            $defaultPrice = '';
            $defaultMarginType = '';
            if(isset($PRICES[$serviceId][0]) && $PRICES[$serviceId][0] != '')
            {
                $val = $PRICES[$serviceId][0];
                $arr = explode('|', $val);
                if(isset($arr[0]) && is_numeric($arr[0]))
                    $defaultPrice = roundMe($arr[0]);
                if(isset($arr[1]) && is_numeric($arr[1]))
                    $defaultMarginType = $arr[1];
            }
        }

        $this->data['serviceId'] = $serviceId;
        $this->data['srvcAPIId'] = $srvcAPIId;
        $this->data['sType'] = $sType;
        $this->data['PRICES'] = $PRICES;
        $this->data['defaultPrice'] = $defaultPrice ;
        $this->data['defaultMarginType'] = $defaultMarginType;
        $this->data['view'] = 'admin/set_prices_with_api' ;
        $this->load->view('admin/layouts/default1' , $this->data);

    }


    public function ajxpriceswithapi(){
        $purpose = $this->input->post('purpose');
        $strQry = '';
        if($purpose == 'save')
        {
            $sType = $this->input->post('sType');
            $plans = $this->input->post('plans');
            $prices = $this->input->post('prices');
            $marginType = $this->input->post('marginType');
            $serviceId = $this->input->post('serviceId');
            $serviceId = $this->input->post('serviceId');
            $defaultMarginType =$this->input->post('dMT');
            $defaultPrice = $this->input->post('dPrice');
            $srvcAPIId = $this->input->post('srvcAPIId');
            
            $arrGroups = explode(',', $plans);
            $arrPrices = explode(',', $prices);
            $arrMarginType = explode(',', $marginType);
            $totalPrices = sizeof($arrPrices);
            $strQry .= "('$serviceId', '$srvcAPIId', '0', '$defaultPrice', '$defaultMarginType', '$sType')";
            for($x = 1; $x < $totalPrices; $x++)
            {
                if(is_numeric($arrPrices[$x]))
                {
                    $strQry .= ", ('$serviceId', '$srvcAPIId', '".$arrGroups[$x]."', '".$arrPrices[$x]."', 
                    '".$arrMarginType[$x]."', '$sType')";
                }
            }
            $this->Services_model->delete_service_api_pricing($serviceId , $sType , $srvcAPIId);
            
            if($strQry != '')
            {
                $this->Services_model->insert_service_api_pricing($strQry);
               
            }
            $msg =  $this->lang->line('BE_GNRL_11');
        }	
        
        echo $msg;
    }

    /*
    public function resetpackprices()
	{

		$userId = check_input($this->input->post_get('userId')) ?: 0;
		$sc = check_input($this->input->post_get('sc')) ?: 0;
		if ($userId > 0)
		{
			$tbl = 'tbl_gf_users_packages_prices';
			if($sc == '2')
				$tbl = 'tbl_gf_users_log_packages_prices';
			$this->Services_model->executeQuery("DELETE FROM $tbl WHERE UserId = '$userId'");
			$this->data['message'] =  $this->lang->line('BE_GNRL_11');
		}

		$this->data['view'] = 'admin/resetpackprices';
		$this->load->view('admin/layouts/default1', $this->data);

	}
	public function setpcksforusers()
	{
		$userId = check_input($this->input->post_get('userId')) ?: 0;
		$this->data['userId'] = $userId;

		$message = '';
		$count = 0;
		$REMOVED_PACKS = array();
		$sc = check_input($this->input->post_get('sc')) ?: 0;
		$categoryId = check_input($this->input->post_get('categoryId')) ?: 0;
		$this->data['categoryId'] = $categoryId;
		$this->data['sc'] = $sc;
		$disableCol = $colTitle = $colId = $strWhere = $strWhereCat = $tblName = $tblPckName = '';
		switch($sc)
		{
			case '0': // IMEI services
				$strWhere .= " AND sl3lbf = 0 AND ArchivedPack = '0'";
				$strWhereCat = " AND SL3BF = 0";
				$tblName = 'tbl_gf_package_category';
				$tblPckName = 'tbl_gf_packages';
				$colId = 'PackageId';
				$colTitle = 'PackageTitle';
				$disableCol = 'DisablePackage';
				break;
			case '1': // File services
				$strWhere .= " AND sl3lbf = 1 AND ArchivedPack = '0'";
				$strWhereCat = " AND SL3BF = 1";
				$tblName = 'tbl_gf_package_category';
				$tblPckName = 'tbl_gf_packages';
				$colId = 'PackageId';
				$colTitle = 'PackageTitle';
				$disableCol = 'DisablePackage';
				break;
			case '2': // Server services
				$strWhere = '';
				$tblName = 'tbl_gf_log_package_category';
				$tblPckName = 'tbl_gf_log_packages';
				$colId = 'LogPackageId';
				$colTitle = 'LogPackageTitle';
				$disableCol = 'DisableLogPackage';
				break;
		}

		$rsCategories = $this->Services_model->selectData("SELECT CategoryId AS Id, Category AS Value FROM $tblName WHERE DisableCategory = 0 AND ArchivedCategory = 0 $strWhereCat ORDER BY OrderBy, Category");
		$this->data['rsCategories'] = $rsCategories;
		if($this->input->post_get('btnSave'))
		{
			$packIds = $this->input->post('chkPacks');
			$ids = $this->input->post('catBasedSrvcs');
			$totalPacks = count($packIds);
			$str = '';
			for ($j = 0; $j < $totalPacks; $j++)
			{
				if($j > 0)
					$str .= ',';
				$str .= "(".$userId.",".check_input($packIds[$j]).", '$sc')";
			}
			$this->Services_model->executeQuery("DELETE FROM tbl_gf_user_packages WHERE UserId = '$userId' AND PackageId IN ($ids) AND ServiceType = '$sc'");
			if($str != '')
				$this->Services_model->executeQuery("INSERT INTO tbl_gf_user_packages (UserId, PackageId, ServiceType) VALUES ".$str);
			$message =  $this->lang->line('BE_GNRL_11');
		}

		if($userId > 0)
		{
			$rsrmvdPcks = $this->Services_model->selectData("SELECT PackageId FROM tbl_gf_user_packages WHERE UserId = '$userId' AND ServiceType = '$sc'");
			foreach($rsrmvdPcks as $row)
			{
				$REMOVED_PACKS[$row->PackageId] = '1';
			}
		}
		if($categoryId != '0')
		{
			$rsPacks = $this->Services_model->selectData("SELECT $colId AS PackageId, $colTitle As PackageTitle FROM $tblPckName WHERE CategoryId = '$categoryId' AND $disableCol = 0 $strWhere ORDER BY PackOrderBy, $colTitle");
			$count = count($rsPacks);
		}
		$this->data['rsPacks'] = $rsPacks;
		$this->data['count'] = $count;
		$this->data['message'] = $message;
		$this->data['REMOVED_PACKS'] = $REMOVED_PACKS;

	}
	*/
}

?>
