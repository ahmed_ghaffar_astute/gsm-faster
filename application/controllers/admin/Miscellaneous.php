<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Miscellaneous extends MY_Controller{

    public function __construct() {
        parent::__construct();
        $this->load->model('Miscellaneous_model');

    }

    public function newscats(){
        //var_dump('here');die();
        $this->data['newsCat']=$this->Miscellaneous_model->getNewsCat();
        $this->data['count']=$this->Miscellaneous_model->countNewsCat();
        $this->data['IS_DEMO'] = false;
        $this->data['view'] = 'admin/news_category';
        $this->load->view('admin/layouts/default1', $this->data);
    }
    public function lookuplist(){
        $txtVal = '';
        $disable = 0;
        $id = ($this->input->get('id')) ? $this->input->get('id') : 0;
        $iFrm = ($this->input->get('iFrm')) ? $this->input->get('iFrm') : 0;
        $service = ($this->input->get('service')) ? $this->input->get('service') : '';
        $lookupcases=lookupcases($service,$iFrm);
        if($id > 0){
            $getCats=$this->Miscellaneous_model->getCategories($id,$lookupcases['tbl'],$lookupcases['idCol']);

            $txtVal=$getCats->Category;
            $disable=$getCats->DisableCategory;
        }

        $this->data['strLabel']=$lookupcases['strLabel'];
        $this->data['idCol']=$lookupcases['idCol'];
        $this->data['textCol']=$lookupcases['textCol'];
        $this->data['disableCol']=$lookupcases['disableCol'];
        $this->data['tbl']=$lookupcases['tbl'];
        $this->data['id']=$id;
        $this->data['txtVal']=$txtVal;
        $this->data['disable']=$disable;
        $this->data['IS_DEMO'] = false;
        $this->data['view'] = 'admin/lookuplist';
        $this->load->view('admin/layouts/default1', $this->data);
    }
    public function ajxlookuplist(){

        $id = $this->input->post('id');
        $txtBx = $this->input->post('txtVal');
        $idCol = $this->input->post('idField');
        $textCol = $this->input->post('nameField');
        $disableCol = $this->input->post('disableField');
        $disableVal = $this->input->post('disableVal');
        $tbl = $this->input->post('tblName');
        $msg=$this->Miscellaneous_model->fetch_ajaxlookuplist($id,$idCol,$tbl,$textCol,$txtBx,$disableCol,$disableVal);
        echo $msg;
        //redirect(base_url('admin/Miscellaneous/lookuplist?frmId=66&fTypeId=14&msg='.$msg));


    }

    public function viewNews(){
        $this->data['IS_DEMO']= $IS_DEMO = false;
        $this->data['message'] = $this->input->post_get('msg') ? $this->input->post_get('msg') : '';
        $this->data['type']= $type =$this->input->post_get('type') ? $this->input->post_get('type') : 0;

        //get all News
        $this->data['rsNews'] = $this->Miscellaneous_model->getAllNews($type);
        //var_dump($this->data['rsNews']);die();
        $this->data['view'] = 'admin/viewnews';
        $this->load->view('admin/layouts/default1', $this->data);
    }
    public function deleteNews(){
        $this->data['IS_DEMO']= $IS_DEMO = true;
        $this->data['del'] = $del = ($this->input->get('del')) ? 1 : 0;
        $this->data['id'] = $id = ($this->input->get('id')) ? $this->input->get('id') : 0;
            //delete news
        $this->Miscellaneous_model->deleteNews($id);
        $message = $this->lang->line('BE_GNRL_12');
        redirect(base_url('admin/Miscellaneous/viewnews?frmId=66&fTypeId=14&msg='.$message));
    }
    //newsPage
    public function news(){
        $this->load->library('FileManager');
        $newsTitle = '';
        $news = '&nbsp;';
        $shortDesc = '';
        $url = '';
        $disableNews = 0;
        $message = $this->input->post_get('msg') ? $this->input->post_get('msg') : 0;
        $type =$this->input->post_get('type') ? $this->input->post_get('type') : 0;
        $id = $this->input->post_get('id') ? $this->input->post_get('id') : 0;
        if($id > 0){

            //get news data against id
            $newsdata = $this->Miscellaneous_model->getNewsById($id);
            //var_dump($newsdata);die();
            foreach ($newsdata as $row){
                $this->data['newsTitle'] = $row->NewsTitle;
                $this->data['news'] = $row->News;
                $this->data['shortDesc'] = $row->ShortDescription;
                $THEME=$this->data['rsStngs']->Theme;
                if($row->NewsImage != ''){

                    $this->data['existingImage'] = base_url().'/uplds'.$THEME.'/'.$row->NewsImage;
                }else{
                    $this->data['existingImage'] = '';
                }

                $this->data['url'] = $row->ImageURL;
                $this->data['disableNews'] = $row->DisableNews;
                $this->data['htmlTitle'] = $row->HTMLTitle;
                $this->data['metaKW'] = $row->MetaKW;
                $this->data['seoName'] = $row->SEOURLName;
                $this->data['metaTags'] = $row->MetaTags;
                $this->data['fileName'] = $row->FileName;
                $this->data['categoryId'] = $row->CategoryId;
                $this->data['type'] = $row->NewsType;
                $this->data['atWebsite'] = $row->AtWebsite;
                $this->data['atIMEI'] = $row->AtIMEI;
                $this->data['atFile'] = $row->AtFile;
                $this->data['atServer'] = $row->AtServer;
            }
        }else{
            $this->data['newsTitle'] = '';
            $this->data['news'] = '';
            $this->data['shortDesc'] = '';
            $this->data['existingImage'] = '';
            $this->data['url'] = '';
            $this->data['disableNews'] = '';
            $this->data['htmlTitle'] = '';
            $this->data['metaKW'] = '';
            $this->data['seoName'] = '';
            $this->data['metaTags'] = '';
            $this->data['fileName'] = '';
            $this->data['categoryId'] = '';
            $this->data['type'] = '';
            $this->data['atWebsite'] = '';
            $this->data['atIMEI'] = '';
            $this->data['atFile'] = '';
            $this->data['atServer'] = '';
            $this->data['newsTitle']=$newsTitle;
            $this->data['news']=$news;
            $this->data['shortDesc']=$shortDesc;
            $this->data['url']=$url;
            $this->data['disableNews']=$disableNews;
        }

        //data for view
        $this->data['message'] = $message;
        $this->data['type']= $type ;
        $this->data['id'] = $id ;
        $this->data['view'] = 'admin/news_add_form';
        $this->load->view('admin/layouts/default1', $this->data);
    }
    //addNews Form
    public function addNewsdata(){
        $csrf_test_name = $this->input->post_get('csrf_test_name');
        $newsTitle = '';
        $news = '&nbsp;';
        $shortDesc = '';
        $url = '';
        $disableNews = 0;
        $message = '';
        $id = $this->input->post_get('id') ? $this->input->post_get('id') : 0;
        $type =$this->input->post_get('type') ? $this->input->post_get('type') : 0;
        $newsTitle = $this->input->post('txtTitle');
        //get NewsId against textTitle here
        $row = $this->Miscellaneous_model->getNewsByTitle($newsTitle,$type,$id);
        //var_dump($row);die;
        if($row!==NULL){
            if($row->NewsId && $row->NewsId != ''){
                $message = $newsTitle." ".$this->lang->line('BE_GNRL_10');
                redirect(base_url('admin/Miscellaneous/news?type=0&frmId=66&fTypeId=14&msg='.$message));
            }
        }

            $this->load->library('FileManager');
            $filemanager = new FileManager();
            $fileExt = $filemanager->getFileExtension("txtImage");
            if($fileExt == '.png' || $fileExt == '.jpg' || $fileExt == '.jpeg' || $fileExt == '.gif' || $fileExt == '')
            {

                $newsTitle = $this->input->post('txtTitle');
                $file = '';
                $news = $this->input->post('txtNews');
                $shortDesc = $this->input->post('txtShortDesc');
                $url = $this->input->post('txtURL');
                $categoryId = $this->input->post('categoryId');
                $existingImage = $this->input->post('existingImage');
                $disableNews = $this->input->post('chkDisableNews') ? 1 : 0;
                $metaTags = $this->input->post('txtMetaTags');
                $seoName = $this->input->post('txtSEOName');
                $htmlTitle = $this->input->post('txtHTMLTitle');
                $metaKW = $this->input->post('txtMetaKW');
                $fileName = $this->input->post('txtFileName');
                if($type == '0')
                    $atWebsite = $this->input->post('chkWebsite') ? 1 : 0;
                else
                    $atWebsite = $this->input->post('chkWebsite');
                $atIMEI = $this->input->post('chkIMEI') ? 1 : 0;
                $atFile = $this->input->post('chkFile') ? 1 : 0;
                $atServer = $this->input->post('chkServer') ? 1 : 0;

                $dtTm = setDtTmWRTYourCountry();
                if($id == 0)
                {
                    //call Models addNews function here
                    $id = $this->Miscellaneous_model->addNews($newsTitle,$news,$shortDesc,$url,$disableNews,$dtTm,$seoName,$metaKW,$htmlTitle,$fileName,$metaTags,$categoryId,$type,$atWebsite,$atIMEI,$atFile,$atServer);

                    if ($filemanager->getFileName('txtImage') != '')
                        $file =	'news/'.$id.$filemanager->getFileExtension("txtImage");
                }
                else
                {
                    if ($filemanager->getFileName('txtImage') != '')
                        $file =	'news/'.$id.$filemanager->getFileExtension("txtImage");

                    //call updateNews Function here
                    $this->Miscellaneous_model->updateNews($newsTitle,$news,$shortDesc,$disableNews,$metaTags,$metaKW,$seoName,$htmlTitle,$fileName,$categoryId,$atWebsite,$atIMEI,$atFile,$atServer,$id);
                }
                if($file != '')
                {
                    //update newsImage
                    $this->Miscellaneous_model->updateNewsImage($file,$id);
                }
                if ($filemanager->getFileName('txtImage') != '')
                {
                    $THEME=$this->data['rsStngs']->Theme;
                    if($existingImage != '')
                        @unlink(base_url()."/uplds'.$THEME.'/'.$existingImage.'");
                    $filemanager->uploadAs('txtImage', base_url()."/uplds'.$THEME.'/'.$file.'");
                }
                $message = $this->lang->line('BE_GNRL_11');
                redirect(base_url('admin/Miscellaneous/news?type=0&frmId=66&fTypeId=14&msg='.$message));
            }
            else
            {
                $message = $this->lang->line('BE_LBL_572');
                redirect(base_url('admin/Miscellaneous/news?type=0&frmId=66&fTypeId=14&msg='.$message));
            }

    }
    public function viewfaqs(){
        $message = $this->input->post_get('msg') ?: '';
        $del = $this->input->post_get('del') ? 1 : 0;
        $id = $this->input->post_get('id') ?: 0;
        $IS_DEMO = false;

        if($del == '1' && !$IS_DEMO)
        {
            $id = $this->input->post_get('id') ?: 0;
            //call delete function FAQS by id here
            $this->Miscellaneous_model->deletFaqs($id);
            $message = $this->lang->line('BE_GNRL_12');
            redirect(base_url('admin/Miscellaneous/viewfaqs?frmId=67&fTypeId=14&msg='.$message));
        }
        //get all FAQS here
        $rsFAQs = $this->Miscellaneous_model->getAllFaqs();

        $this->data['rsFAQs']= $rsFAQs;
        $this->data['IS_DEMO']= $IS_DEMO;
        $this->data['del'] = $del ;
        $this->data['id'] = $id ;
        $this->data['message'] = $message ;
        $this->data['view'] = 'admin/view_faqs';
        $this->load->view('admin/layouts/default1', $this->data);
    }
    public function faqs(){
        $id = $this->input->post_get('id') ?: 0;
        $question = '';
        $url = '';
        $disableFaq = 0;
        $answer = '';
        $message = $this->input->post_get('msg') ?: '';
        if($id > 0)
        {

            //call getFaqsById function here
            $row = $this->Miscellaneous_model->getFaqsById($id);
            if (isset($row->Question) && $row->Question != '')
            {
                $question = stripslashes($row->Question);
                $answer = stripslashes($row->Answer);
                $disableFaq = $row->DisableFAQ;
            }
        }

        $this->data['disableFaq']=$disableFaq;
        $this->data['answer']=$answer;
        $this->data['question']=$question;
        $this->data['id'] = $id ;
        $this->data['message'] = $message ;
        $this->data['view'] = 'admin/faqs_form';
        $this->load->view('admin/layouts/default1', $this->data);
    }
    public function addFaqs(){
        $csrf_test_name = $this->input->post_get('csrf_test_name');
        $id = $this->input->post_get('id') ?: 0;
        //var_dump($_POST);die();
        $question = $this->input->post_get('txtQuestion');
        $answer = $this->input->post_get('txtAnswer');
        $disableFaq = $this->input->post_get('chkDisableFAQ') ? 1 : 0;
        $dtTm = setDtTmWRTYourCountry();

        if($id == 0)
        {
            //call insertFaqs function here
            $faqs=$this->Miscellaneous_model->insertFaqs($question,$answer,$disableFaq);
            $question = '';
            $disableFaq = 0;
            $answer = '';
        }
        else
        {
            //call updateFaqs function here
            $faqs=$this->Miscellaneous_model->updateFaqs($question,$answer,$disableFaq,$id);
            //$objDBCD14->execute("UPDATE tbl_gf_faqs SET Question = '$question', Answer = '$answer', DisableFAQ = '$disableFaq' WHERE FAQId = '$id'");
        }
        $message = $this->lang->line('BE_GNRL_11');
        redirect(base_url('admin/Miscellaneous/faqs?frmId=67&fTypeId=14&msg='.$message));
    }
    public function knowledgebasecats(){
        //call getKnowledgeBaseCats function here
        $rsCats=$this->Miscellaneous_model->getKnowledgeBaseCats();
        $IS_DEMO=false;

        $this->data['rsCats']=$rsCats;
        $this->data['IS_DEMO']=$IS_DEMO;
        $this->data['view'] = 'admin/knowledgebasecats';
        $this->load->view('admin/layouts/default1', $this->data);
    }
    public function knbasecontents(){
        $message = '';
        $del = $this->input->post_get('del') ? 1 : 0;
        if($del == '1')
        {
            $id = $this->input->post_get('id') ?: 0;
            //call delete fucntion here
            $this->Miscellaneous_model->delKnowledgeBaseCats($id);
            $message = $this->lang->line('BE_GNRL_12');
        }
        //call getKnowledgeBaseContents function here
        $rsNews=$this->Miscellaneous_model->getKnowledgeBaseContents();
        //var_dump($rsNews);die;
        $IS_DEMO=false;

        $this->data['message']=$message;
        $this->data['rsNews']=$rsNews;
        $this->data['IS_DEMO']=$IS_DEMO;
        $this->data['view'] = 'admin/knowlege_base_contents';
        $this->load->view('admin/layouts/default1', $this->data);
    }

    public function knbasecontent(){
        $id = $this->input->post_get('id') ?: 0;
        $title = '';
        $contents = '&nbsp;';
        $categoryId = '';
        $message = $this->input->post_get('msg') ?: '';
        $disable = 0;
        $IS_DEMO=false;
        if($id > 0)
        {
            //call getKnowledgeBaseContentsById here
            $row = $this->Miscellaneous_model->getKnowledgeBaseContentsById($id);

            if (isset($row->Title) && $row->Title != '')
            {
                $title = stripslashes($row->Title);
                $contents = stripslashes($row->Contents);
                $categoryId = $row->CategoryId;
                $disable = $row->DisableKB;
            }
        }

        $this->data['message']=$message;
        $this->data['title']=$title;
        $this->data['id']=$id;
        $this->data['contents']=$contents;
        $this->data['categoryId']=$categoryId;
        $this->data['disable']=$disable;
        $this->data['IS_DEMO']=$IS_DEMO;
        $this->data['view'] = 'admin/knowledge_base_content';
        $this->load->view('admin/layouts/default1', $this->data);

    }
    public function insertKnBaseContents(){
        $id = $this->input->post_get('id') ?: 0;
        $txtTitle = $this->input->post_get('txtTitle');
        if ($txtTitle)
        {
            $title = $txtTitle;
            //call checkKnBaseTitle
            $row = $this->Miscellaneous_model->checkKnBaseContentsTitle($title,$id);
            if (isset($row->Id) && $row->Id != '')
            {
                $message = $title.' '.$this->lang->line('BE_GNRL_10');
            }
            else
            {
                $contents = $this->input->post_get('txtContents');
                $categoryId = $this->input->post_get('categoryId');
                $disable = $this->input->post_get('chkDisableKB') ? 1 : 0;

                if($id == 0)
                {
                    //call insertKnBaseDate here
                    $this->Miscellaneous_model->insertKnBaseContents($title,$contents,$categoryId,$disable);
                    $title = '';
                    $contents = '';
                    $categoryId = 0;
                    $disable = 0;
                }
                else
                {
                    //call updateKnBaseContents
                    $this->Miscellaneous_model->updateKnBaseContents($title,$contents,$categoryId,$disable,$id);
                }
                $message = $this->lang->line('BE_GNRL_11');
            }
        }

        redirect(base_url('admin/Miscellaneous/knbasecontent?frmId=69&fTypeId=14&msg='.$message));
    }
    public function newsltrcats(){
        $IS_DEMO=false;
        $message='';
        //cal getAllNewsLtrCats function here
        $rsCats = $this->Miscellaneous_model->getAllNewsLtrCats();

        $this->data['message']=$message;
        $this->data['rsCats']=$rsCats;
        $this->data['IS_DEMO']=$IS_DEMO;
        $this->data['view'] = 'admin/news_letter_categories';
        $this->load->view('admin/layouts/default1', $this->data);
    }
    public function newsletters(){
        $message = '';
        $IS_DEMO=false;
        $del = $this->input->post_get('del') ? 1 : 0;
        if($del == '1')
        {
            $id = $this->input->post_get('id') ?: 0;
            //call del function
            $this->Miscellaneous_model->deleteNewsletters($id);
            $message = $this->lang->line('BE_GNRL_12');
        }

        //cal getAllNewsLtrCats function here
        $rsLtrs = $this->Miscellaneous_model->getAllNewsletters();

        $this->data['message']=$message;
        $this->data['rsLtrs']=$rsLtrs;
        $this->data['IS_DEMO']=$IS_DEMO;
        $this->data['view'] = 'admin/manage_news_letter';
        $this->load->view('admin/layouts/default1', $this->data);
    }
    public function newsletter(){

        $id = $this->input->post_get('id') ?: 0;
        $message = $this->input->post_get('msg') ?: '';
        $existingImage='';
        $disableNewsLtr='';
        $categoryId='';
        $newsLtr='';
        $newsLtrTitle='';
        if($id > 0)
        {
            //call getNewsLtrById
            $this->Miscellaneous_model->getNewsLtrById($id);
            $row = $this->Miscellaneous_model->getNewsLtrById($id);

            if (isset($row->NewsLtrTitle) && $row->NewsLtrTitle != '')
            {
                $newsLtrTitle = stripslashes($row->NewsLtrTitle);
                $newsLtr = stripslashes($row->NewsLtr);
                $categoryId = $row->CategoryId;
                $disableNewsLtr = $row->DisableNewsLtr;

            }
        }

        $this->data['id']=$id;
        $this->data['existingImage']=$existingImage;
        $this->data['disableNewsLtr']=$disableNewsLtr;
        $this->data['categoryId']=$categoryId;
        $this->data['newsLtr']=$newsLtr;
        $this->data['newsLtrTitle']=$newsLtrTitle;
        $this->data['message']=$message;
        $this->data['view'] = 'admin/news_letter_form';
        $this->load->view('admin/layouts/default1', $this->data);
    }
    public function insertNewsletters(){
        $id = $this->input->post_get('id') ?: 0;
        $disableNewsLtr = 0;
        $newsLtrTitle = $existingImage = $message = '';
        $newsLtr = 'Create your newsletter here...';
        if ($this->input->post_get('txtNewsLtr'))
        {
            $newsLtrTitle = $this->input->post_get('txtTitle');
            //check title is unique or not
            $row = $this->Miscellaneous_model->getNewsLtrByTitle($newsLtrTitle,$id);
            if (isset($row->NewsLtrId) && $row->NewsLtrId != ''){
                $message = $newsLtrTitle ." ".$this->lang->line('BE_GNRL_10');

            }
            else
            {
                $categoryId = $this->input->post_get('categoryId');
                $newsLtr = $this->input->post_get('txtNewsLtr');
                $disableNewsLtr = $this->input->post_get('chkDisableNewsLtr') ? 1 : 0;
                if($id == 0)
                {
                    $this->Miscellaneous_model->addNewsLtr($newsLtrTitle,$newsLtr,$categoryId,$disableNewsLtr);
                    $message = $this->lang->line('BE_GNRL_11');
                }
                else
                {
                    $this->Miscellaneous_model->updateNewsLtr($newsLtrTitle,$newsLtr,$categoryId,$disableNewsLtr,$id);
                    $message = $this->lang->line('BE_GNRL_11');
                }
                redirect(base_url('admin/Miscellaneous/newsletter?id='.$id.'&frmId=71&fTypeId=14&msg='.$message));
            }
        }


        $this->data['view'] = 'admin/manage_news_letter';
        $this->load->view('admin/layouts/default1', $this->data);
    }

	public function sendnewsletter(){
		$this->data['view'] = 'admin/send_news_letter';
		$this->data['IS_DEMO'] = false;
        $this->load->view('admin/layouts/default1', $this->data);

	}

	public function ajxnletter(){
        set_time_limit(0);
        $ltrId = $this->input->post_get('ltrId');
        $planId = $this->input->post_get('planId');
        $email='';
        $msg = '';
        if($ltrId > 0)
        {
            //calling getNewsLetterById function
            $row = $this->Miscellaneous_model->getNewsLtrById($ltrId);

            if (isset($row->NewsLtrTitle) && $row->NewsLtrTitle != '')
            {
                $newsLtrTitle = stripslashes($row->NewsLtrTitle);
                $newsLtr = stripslashes($row->NewsLtr);
            }
            //call getEmailSettingById function
            $rwStngs = $this->Miscellaneous_model->getEmailSettingById('1');
            $nlViaCron = $rwStngs->SendNLViaCron;
            $strWhere = '';
            if($planId !='0')
                $strWhere = " AND PricePlanId = '$planId'";
            $arr = getEmailDetails();
            //call getUserDetail function
            $rsEmailIds = $this->Miscellaneous_model->getUserDetail($planId);
            $strQry = '';

            foreach($rsEmailIds as $row)
            {
                if($nlViaCron == '1')
                {
                    if($strQry != '')
                        $strQry .= ',';
                    $lnkToUnsub = "<p style='color:#999999; font-family:Arial; font-size:12px;'><br />If you don't want to receive these emails in the future, 
				please click <a target='_blank' href='http://".$arr[3]."/unsubscribe?v=".urlsafe_b64encode($row->UserId)."' 
				style='color:#000000; font-family:Arial; font-size:12px;'> here</a> to unsubscribe.</p>";
                    $strQry .= "('".$newsLtrTitle."',
				'".$newsLtr.$lnkToUnsub."','".$row->UserEmail."')";
                }
                else
                {
                    sendGeneralEmail($row['UserEmail'], $arr[0], $arr[1], 'Customer', $newsLtrTitle, '', $newsLtr);
                }
            }
            //call getSubscriptions function
            $rsEmailIds = $this->Miscellaneous_model->getSubscriptions();
            $strQry1 = '';
            //var_dump($rsEmailIds);die;
            if($rsEmailIds != false){
                foreach($rsEmailIds as $row)
                {
                    if($strQry1 != '')
                        $strQry1 .= ',';
                    $lnkToUnsub = "<p style='color:#999999; font-family:Arial; font-size:12px;'><br />If you don't want to receive these emails in the future, 
			please click <a target='_blank' href='http://".$arr[3]."/unsubscribe_w?v=".urlsafe_b64encode($row->Id)."' 
			style='color:#000000; font-family:Arial; font-size:12px;'> here</a> to unsubscribe.</p>";
                    $strQry1 .= "('".$newsLtrTitle."',
			'".$newsLtr.$lnkToUnsub."','".$row->Email."')";
                }
                $email=$row->Email;
            }
            if($strQry1 != '')
                $strQry = $strQry.', '.$strQry1;
            if($strQry != '' && $nlViaCron == '1')
            {
                //call insertNewsltrToSend function
                $this->Miscellaneous_model->insertNewsltrToSend($newsLtrTitle,($newsLtr.$lnkToUnsub),$email);
            }
            $data['msg']  =  $this->lang->line('BE_GNRL_11');
            $data['status']  =  1;

        }
        else{
            $data['msg']  =  $this->lang->line('BE_LBL_523');
            $data['status']  =  0;
        }
        echo json_encode($data);
        exit;
	}
	public function videos(){
        $message = '';
        $del = ($this->input->post_get('del'))? 1 : 0;
        if($del == '1')
        {
            $id = ($this->input->post_get('id')) ?: 0;
            //call deleteVideos function
            $this->Miscellaneous_model->deleteVideos($id);
            $message = $this->lang->line('BE_GNRL_12');
        }
        //call getAllVideos function
		$rsNews = $this->Miscellaneous_model->getAllVideos();
		$this->data['rsNews'] = $rsNews;
		$this->data['message'] = $message;
		$this->data['view'] = 'admin/manage_videos';
		$this->data['IS_DEMO'] = false;
		$this->load->view('admin/layouts/default1', $this->data);
    }
    public function video(){
		$id = $this->input->post_get('id') ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
		$title = $lnk = $message = '';
		$desc = '&nbsp;';
		$disable = 0;
		if ($this->input->post_get('txtTitle'))
		{
			$title = check_input($this->input->post_get('txtTitle'), $this->db->conn_id);
				//call getVideosByTitle
			$row = $this->Miscellaneous_model->getVideosByTitle($title,$id);
			//var_dump($row);die;
			if ($row != false)
			{
				$message = $title.' '.$this->lang->line('BE_GNRL_10');
			}
			else
			{
				$desc = check_input($this->input->post_get('txtDescription'), $this->db->conn_id, 1);
				$lnk = check_input($this->input->post_get('txtLnk'), $this->db->conn_id);
				$disable = $this->input->post_get('chkDisableVideo') ? 1 : 0;

				if($id == 0)
				{
					//call insertVideos()
					$this->Miscellaneous_model->insertVideos($title,$desc,$lnk,$disable);
					$title = '';
					$desc = '';
					$lnk = '';
					$disable = 0;
				}
				else
				{
					//call updateVideos()
					$this->Miscellaneous_model->updateVideos($title,$desc,$lnk,$disable,$id);
				}
				$message = $this->lang->line('BE_GNRL_11');
			}
		}
		if($id > 0)
		{
			$row = $this->Miscellaneous_model->getVideosById($id);
			if ($row->Title && $row->Title != '')
			{
				$title = stripslashes($row->Title);
				$desc = stripslashes($row->Description);
				$lnk = $row->YouTubeLnk;
				$disable = $row->DisableVideo;
			}
		}
		//var_dump($id,$row);die;
		$this->data['title'] = $title;
		$this->data['desc'] = $desc;
		$this->data['lnk'] = $lnk;
		$this->data['disable'] = $disable;
		$this->data['id'] = $id;
		$this->data['message'] = $message;
		$this->data['view'] = 'admin/video_add_form';
		$this->data['IS_DEMO'] = false;
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function flagcounter(){

		$message='';
		$submit = ($this->input->post('btnSave'))? true:false;

		if ($submit!=false)
		{
			$script = check_input($this->input->post_get('txtScript'), $this->db->conn_id);
			$disable = $this->input->post_get('disable') ? 1 : 0;
			//call updateFalgCounters()
			$this->Miscellaneous_model->updateFalgCounters($script,$disable,'1');
			$message = $this->lang->line('BE_GNRL_11');
		}
		//call getFlagCounersById()
		$row = $this->Miscellaneous_model->getFlagCounersById('1');
		$script = stripslashes($row->ScriptCode);
		$disable = $row->DisableFCounter;
		$this->data['message'] = $message;
		$this->data['script'] = $script;
		$this->data['disable'] = $disable;
		$this->data['view'] = 'admin/flagcounter_form';
		$this->data['IS_DEMO'] = false;
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function analytics(){
		$message = '';
		$submit = ($this->input->post('btnSave'))? true:false;
		if ($submit != false)
		{
			$code = check_input($this->input->post_get('txtCode'), $this->db->conn_id);
			$disable = ($this->input->post_get('chkDisable')) ? 1 : 0;
			//call updateAnalytics()
			$this->Miscellaneous_model->updateAnalytics($code,$disable,'1');
			$message = $this->lang->line('BE_GNRL_11');
		}
		$row = $this->Miscellaneous_model->getAnalyticsById('1');
		$code = stripslashes($row->Code);
		$disable = $row->DisableCode;
		$this->data['message'] = $message;
		$this->data['code'] = $code;
		$this->data['disable'] = $disable;
		$this->data['view'] = 'admin/analytics_add_form';
		$this->data['IS_DEMO'] = false;
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function marquee(){
		$message = '';
		$submit = ($this->input->post('btnSave'))? true:false;
		if ($submit != false)
		{
			$code = check_input($this->input->post_get('txtCode'), $this->db->conn_id);
			$position = '1';//check_input($_POST['rdPosition'], $objDBCD14->dbh);
			$disable = ($this->input->post_get('chkDisable')) ? 1 : 0;
			$this->Miscellaneous_model->updateMarquee($code,$position,$disable,'1');
			$message = $this->lang->line('BE_GNRL_11');
		}
		$row = $this->Miscellaneous_model->getMarqueeById('1');
		$code = stripslashes($row->Code);
		$position = $row->Position;
		$disable = $row->DisableMarquee;
		$this->data['message'] = $message;
		$this->data['code'] = $code;
		$this->data['position'] = $position;
		$this->data['disable'] = $disable;
		$this->data['view'] = 'admin/marquee_slider_form';
		$this->data['IS_DEMO'] = false;
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function gsm_fusion_api(){
		$filename = 'GSM_Fusion_API.pdf';
		$this->data['file_name'] = $filename;
		$this->load->view('admin/gsm_fusion_api' , $this->data);
	}
	public function apisamplecode(){
    	//var_dump('here');die;
		$filename = 'apisamplecode.rar';
		$this->data['file_name'] = $filename;
		$this->load->view('admin/api_sample_code' , $this->data);
	}
	public function nlsubscriptions(){
    	//var_dump('here');die;
		$message = '';
		$IS_DEMO = false;
		$del = $this->input->post_get('del') ? 1 : 0;
		if($del == '1' && !$IS_DEMO)
		{
			$id = $this->input->post_get('id') ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
			//call delSubscription()
			$this->Miscellaneous_model->delSubscription($id);
			$message = $this->lang->line('BE_GNRL_12');
		}
		$rsData = $this->Miscellaneous_model->getSubscription();
		$this->data['IS_DEMO'] = $IS_DEMO;
		$this->data['message'] = $message;
		$this->data['rsData'] = $rsData;
		$this->data['view'] = 'admin/manage_nlsubscriptions';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function downloads(){
		$message = '';
		$IS_DEMO = false;
		$del = ($this->input->post_get('del')) ? 1 : 0;
		if($del == '1' && !$IS_DEMO)
		{
			$id = ($this->input->post_get('id')) ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
			$this->Miscellaneous_model->delDownloads($id);
			$message = $this->lang->line('BE_GNRL_12');
		}
		$rsDwnlds = $this->Miscellaneous_model->getDownloads();
		$this->data['IS_DEMO'] = $IS_DEMO;
		$this->data['message'] = $message;
		$this->data['rsDwnlds'] = $rsDwnlds;
		$this->data['view'] = 'admin/manage_downloads';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function download(){
		$id = ($this->input->post_get('id')) ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
		$title = $message = $row = '';
		$disable = 0;
		$IS_DEMO = false;
		$THEME=$this->data['rsStngs']->Theme;
		$check= $this->input->post_get('txtTitle')?:'';
		if ($check !='')
		{
			$title = check_input($this->input->post_get('txtTitle'), $this->db->conn_id);
			$row = $this->Miscellaneous_model->getDownloadsByTitle($title,$id);
			if (isset($row->DownloadId) && $row->DownloadId != '')
			{
				$message = $title.' '.$this->lang->line('BE_GNRL_10');
			}
			else
			{
				$this->load->library('FileManager');
				$filemanager = new FileManager();
				$fileExt = $filemanager->getFileExtension("txtImage");
				$file = '';
				$disable = $this->input->post_get('chkDisable') ? 1 : 0;
				if($id == 0)
				{
					//insertDownloads()
					if ($filemanager->getFileName('txtImage') != ''){
						$file =	'downloads/'.$id.$filemanager->getFileExtension("txtImage");
						$filemanager->uploadAs('txtImage', base_url().'uplds'.$THEME.'/'.$file);
					}
					$id = $this->Miscellaneous_model->insertDownloads($title,$disable,$file);
				}
				else
				{
					if ($filemanager->getFileName('txtImage') != ''){
						$file =	'downloads/'.$id.$filemanager->getFileExtension("txtImage");
						@unlink(base_url()."/uplds'.$THEME.'/'.$file.'");
						$filemanager->uploadAs('txtImage', base_url().'uplds'.$THEME.'/'.$file);
					}
					// updatedownload()
					$this->Miscellaneous_model->updateDownloads($title,$disable,$file,$id);
				}
				$message = $this->lang->line('BE_GNRL_11');
			}
		}
		if($id > 0)
		{
			//getDownloads()
			$row = $this->Miscellaneous_model->getDownloadsById($id);
			if (isset($row->Title) && $row->Title != '')
			{
				$title = stripslashes($row->Title);
				$disable = $row->DisableDownload;
			}
		}
		$this->data['id'] = $id;
		$this->data['title'] = $title;
		$this->data['disable'] = $disable;
		$this->data['IS_DEMO'] = $IS_DEMO;
		$this->data['message'] = $message;
		$this->data['row'] = $row;
		$this->data['view'] = 'admin/download_add_form';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function databasebackup(){
		$message = '';


		$this->load->dbutil();

		$prefs = array(
			'format'      => 'zip',
			'filename'    => 'my_db_backup.sql'
		);


		$backup =& $this->dbutil->backup($prefs);

		$db_name = 'backup-on-'. date("Y-m-d-H-i-s") .'.zip';
		$save = base_url().'database_backup/'.$db_name;

		$this->load->helper('file');
		write_file($save, $backup);


		$this->load->helper('download');
		$message=$this->lang->line('BE_GNRL_11');
		force_download($db_name, $backup);
		$this->data['message'] = $message;
		$this->data['view'] = 'admin/database_backup';
		$this->load->view('admin/layouts/default1', $this->data);
	}
}
