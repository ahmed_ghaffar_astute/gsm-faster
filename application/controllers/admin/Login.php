<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
        parent::__construct();
       
        $this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }
    
    public function index(){
        
        /****************************************CHECK IP***********************************************/
        if($this->input->post('HTTP_HOST') != 'localhost')
        {
            //createIPHtaccess();
        }
        /****************************************CHECK IP***********************************************/
        $security_code = '';
        $rowSettings = $this->Admin_login_model->fetch_email_settings(); 
        $THEME = $rowSettings->Theme;
        $this->data['LOGIN_CAPTCHA'] = $rowSettings->AdminLoginCaptcha;
        $this->data['adminTitle'] = stripslashes($rowSettings->AdminTitle);
        $copyRight = stripslashes($rowSettings->Copyrights);
        $this->session->set_userdata('LANGUAGE_BE' , $rowSettings->LocalLanguage);
        $this->data['GOOGLE_CAPTCHA_SITE_KEY'] = $rowSettings->GoogleCaptchaSiteKey;
        $GOOGLE_CAPTCHA_SECRET_KEY = $rowSettings->GoogleCaptchaSecretKey;

        $rw = $this->Admin_login_model->fetch_logopath();
        if (isset($rw->LogoPath) && $rw->LogoPath != '')
        {
            $logoPath = FCPATH."uplds$THEME/".$rw->LogoPath;
        }
        $row = $this->Admin_login_model->fetch_logpath_by_id();
        if (isset($row->LogoPath) && $row->LogoPath != '')
            $this->session->userdata('AdminFavIcon' , FCPATH."uplds$THEME/".$row->LogoPath);

        $msg = 0;
        $message = '';
        $this->data['usrNm'] = '';
        $this->data['usrPwd'] = '';
        $this->data['IS_DEMO'] = false;
        $errorMsg = '';
        $rmbrMe = 0;
        if($this->input->post_get('msg'))
            $msg = $this->input->post_get('msg');
        $VERIFY_LOGIN = 0;
        $currDtTm = setDtTmWRTYourCountry();
        $arr = getEmailDetails();
        $ip = $this->input->server('REMOTE_ADDR');
        if($this->input->post('username')){
          
            $userName = $this->input->post('username');
            $userPwd = $this->input->post('password');   
            $errorMsg = username_validation($userName, 'Username');
            if($this->input->server('HTTP_HOST') != 'localhost' &&  $this->data['LOGIN_CAPTCHA'] == '1')
            {
                require_once APPPATH."libraries/recaptchalib.php";
                $response = null;
                $reCaptcha = new ReCaptcha($GOOGLE_CAPTCHA_SECRET_KEY);
                $response = $reCaptcha->verifyResponse(
                    $this->input->server("REMOTE_ADDR"),
                    $this->input->post("g-recaptcha-response")
                );
            
                if ($response != null && $response->success)
                {
                    // DO NOTHING
                }
                else
                    $errorMsg .= 'Invalid Value for Captcha';
            }
            if(trim($errorMsg) == '')
	        {
              
                $rowAdmin = $this->Admin_login_model->fetch_admin_details_by_username($userName);
                if (isset($rowAdmin->AdminId) && $rowAdmin->AdminId != '')
                {
                    if(isPasswordOK($userPwd, $rowAdmin->AdminUserPassword))
                    {
                        $this->session->set_userdata("AdminLoginId" , '');

                      
                        $insert_data = array(
                            'AdminId' => $rowAdmin->AdminId,
                            'LoginTime' => $currDtTm,
                            'IP' => $ip ,
                            'CldFrm' => 1 ,
                        );

                        $last_insert_id=$this->Admin_login_model->add_admin_details($insert_data);
                        $this->session->set_userdata("AdminLoginId" , $last_insert_id);
            
                        $row = $this->Admin_login_model->fetch_logopath();
                        if (isset($row->LogoPath) && $row->LogoPath != '')
                            $this->session->set_userdata('AdminLogo' , "uplds$THEME/".$row->LogoPath);
            
                        $this->session->set_userdata('IsAdmin' , $rowAdmin->IsAdmin);
                        $this->session->set_userdata('GSM_FSN_AdmId' , $rowAdmin->AdminId);
                        $this->session->set_userdata('AdminName' ,stripslashes($rowAdmin->FullName));
                        $this->session->set_userdata('AdminUserName' , $rowAdmin->AdminUserName);
                        $this->session->set_userdata('AdminRoleId' , $rowAdmin->RoleId);
                     
                        redirect(base_url('admin/dashboard'));
                    }
                    else
                    {
                        $msg = 2;
                        $emailMsg ='<p>Login Attempted At: '.$currDtTm.'</p>
                                    <p>Username: '.$userName.'</p>
                                    <p>IP Address: '.$ip.'</p>
                                    <p>Please click <a href="http://'.$arr[3].'/" target="_blank">here</a> to go to your website.</p>';
                        sendGeneralEmail($arr[4], $arr[0], $arr[1], 'Admin', 'Unsuccessful Admin Login Attempt at '.$arr[2], 'A recent login attempt failed. Details of the attempt are below!', $emailMsg);
                    }
                }
            }
        }
        else{
            /****************************************SEND LOGIN ACCESS PAGE EMAIL***********************************************/
            $emailMsg = "<p>Admin Login Page accessed at: $currDtTm <br /><br />IP Address: $ip</p>";
            sendGeneralEmail($arr[4], $arr[0], $arr[1], 'Admin', 'Admin Login Attempt at '.$arr[2], 'A recent login attempt. Details of the attempt are below!', $emailMsg);
            /****************************************SEND LOGIN ACCESS PAGE EMAIL***********************************************/
        }

        switch($msg)
        {
            case 1:
                $message = "Session has been expired, please re-login!";
                break;
            case 2:
                $message = "The username or password is incorrect.";
                break;
            case 3:
                $message = "You have been successfully logged out.";
                break;
            case 4:
                $message = "Password has been changed successfully!";
                break;
            case 5:
                $message = "Invalid Email Address!";
                break;
            case 6:
                $message = 'You have provided invalid sum of given numbers!';
                break;
            case 7:
                $message = 'Invalid Secret value for Google Authentication!';
                break;
        }

        if($message!=''){
			$this->session->set_flashdata('error_message', $message);
		}elseif($errorMsg!=''){
			$this->session->set_flashdata('error_message', $errorMsg);
		}
       /* $this->data['message'] = $message ;
        $this->data['errorMsg']   = $errorMsg;*/
        

        //d($this->data['message']);

        $this->data['view'] = 'admin/login' ;
        $this->load->view('admin/layouts/login_default' , $this->data);
            

    }
}
