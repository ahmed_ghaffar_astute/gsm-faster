<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends MY_Controller{
     
	public function __construct() {
		parent::__construct();
    }

    public function user(){
        $id = $this->input->post_get('id') ?: 0;
        $uName = $planId= $currencyId = $firstName = $lastName = $phone = $lastName = $startDt = $endDt = $message = $email = $ip = $pinCode = $notes = '';
        $disableUser = $reason = $autoFill = $userType = $currCnvr = $countryId = $mPlanId = $subscribed = $loginVerification = $transferCredits = $chkCanLoginFrmOther = $sendSMS = $loginVerification = 0;

        if($this->input->post('txtEmail') && $this->session->userdata('GSM_FSN_AdmId'))
        {
			$pwd = "";
            $subscribe = $this->input->post('chkSubscribe') ? 1 : 0;
            $canLoginFromOther = $this->input->post('chkCanLoginFrmOther') ? 1 : 0;
            $disableVal = $this->input->post('chkDisable') ? 1 : 0;
            $autoFill = $this->input->post('chkAutoFill') ? 1 : 0;
            $sendSMS = $this->input->post('chkSMS') ? 1 : 0;
            $allowCrdsTrnsfr = $this->input->post('chkTransfrCrdts') ? 1 : 0;
            $loginVerification = $this->input->post('chkLoginVerification') ? 1 : 0;
            $phone = check_input($this->input->post('txtPhone'), $this->db->conn_id);
            $planId = check_input($this->input->post('planId'), $this->db->conn_id);
            $listId = 0;
            $email = check_input($this->input->post('txtEmail'), $this->db->conn_id);
            $fName = check_input($this->input->post('txtFName'), $this->db->conn_id);
            $lName = check_input($this->input->post('txtLName'), $this->db->conn_id);
            $countryId = check_input($this->input->post('countryId'), $this->db->conn_id);
            $currencyId = check_input($this->input->post('currencyId'), $this->db->conn_id);
            $loginVerification = $this->input->post('chkLoginVerification') ? 1 : 0;
            if($this->input->post('txtPinCode') != '')
                $pinCode = encryptThe_String(check_input($this->input->post('txtPinCode'), $this->db->conn_id));
            if($this->input->post('txtPass') != '')
                $pwd = bFEncrypt(check_input($this->input->post('txtPass'), $this->db->conn_id));
            $uName = check_input($this->input->post('txtUName'), $this->db->conn_id);
            $reason = check_input($this->input->post('txtReason'), $this->db->conn_id);
            $notes = check_input($this->input->post('txtNotes'), $this->db->conn_id);
            $ips = '';
            $mPlanId = '0';
            $frmDt = '';
            $toDt = '';
            $message = '';	
            $row = $this->Clients_model->fetch_users_count_by_id($uName , $id);
            if ($row->TotalRecs > 0)
            {
                $message = "Username '<b>".$uName."</b>' ".$this->lang->line('BE_GNRL_10');
            }
            else
            {
                
                $result = $this->Clients_model->fetch_users_by_email($email , $id);
                if (isset($result->UserId) && $result->UserId != '')
                {
                    $message = "Email '<b>".$email."</b>' " .  $this->lang->line('BE_GNRL_10');
                }
                else
                {
                    $dtTm = setDtTmWRTYourCountry();
                    $ttlFields = $this->input->post("totalRegFields") ? check_input($this->input->post("totalRegFields"), $this->db->conn_id) : '0';
                    $cols = '';
                    for($x = 1; $x <= $ttlFields; $x++)
                    {
                        $col = $this->input->post('colNm'.$x) ? check_input(trim($this->input->post('colNm'.$x)), $this->db->conn_id) : '';
                        if($col != '')
                        {
                            $val = $this->input->post('fld'.$x) ? check_input(trim($this->input->post('fld'.$x)), $this->db->conn_id) : '';
                            $cols .= ", $col = '$val'";
                        }
                    }
                    if($id == 0)
                    {
                        $insert_array = array(
                            'FirstName' => $fName,
                            'LastName' => $lName,
                            'UserName' => $uName,
                            'UserEmail' => $email ,
                            'UserPassword' => $pwd ,
                            'CountryId' => $countryId ,
                            'Phone' => $phone ,
                            'DisableUser' => $disableVal ,
                            'AutoFillCredits' => $autoFill ,
                            'AddedAt' => $dtTm ,
                            'UpdatedAt' => $dtTm ,
                            'PricePlanId' => $planId ,
                            'PrPlanOfferId' => $mPlanId ,
                            'ListId' => $listId ,
                            'CurrencyId' => $currencyId ,
                            'CurrencyConversion' => $currCnvr ,
                            'UserType'  => $userType,
                            'AddedBy' => $this->session->userdata('GSM_FSN_AdmId'),
                            'UpdatedBy' => $this->session->userdata('GSM_FSN_AdmId'),
                            'IPs' => $ips ,
                            'Comments' => $reason ,
                            'PinCode' => $pinCode ,
                            'CanLoginFrmOtherCntry' => $canLoginFromOther ,
                            'Subscribed' => $subscribe ,
                            'SendSMS' => $sendSMS ,
                            'TransferCredits' => $allowCrdsTrnsfr ,
                            'Notes' => $notes ,
                            'TwoStepVerification' => $loginVerification
                        );
                        
                        $id = $this->Clients_model->insert_users_data($insert_array);
                        if($cols != ''){
                           
                            $this->Clients_model->update_users_custom_query($allowCrdsTrnsfr , $cols , $id);
                        }
                           
                        $message =  $this->lang->line('BE_GNRL_11');
                    }
                    else
                    {
                        $qry = '';
                        $update_data = array();
                        if($pwd != ''){
                            $update_data['UserPassword'] = $pwd;
                        }
                        if($col != ''){
                            $update_data[$col] = $val ;
                        }
                        $update_data['FirstName'] = $fName;
                        $update_data['LastName'] = $lName;
                        $update_data['Phone'] = $phone;
                        $update_data['UserName'] = $uName;
                        $update_data['UserEmail'] = $email;
                        $update_data['DisableUser'] = $disableVal;
                        $update_data['AutoFillCredits'] = $autoFill;
                        $update_data['CurrencyConversion'] = $currCnvr;
                        $update_data['UserType'] = $userType;
                        $update_data['PricePlanId'] = $planId;
                        $update_data['ListId'] = $listId;
                        $update_data['CountryId'] = $countryId;
                        $update_data['UpdatedAt'] = $dtTm;
                        $update_data['CurrencyId'] = $currencyId;
                        $update_data['PrPlanOfferId'] = $mPlanId;
                        $update_data['IPs'] = $ips;
                        $update_data['Subscribed'] = $subscribe;
                        $update_data['SendSMS'] = $sendSMS;
                        $update_data['Notes'] = $notes;
                        $update_data['UpdatedBy'] = $this->session->userdata('GSM_FSN_AdmId');
                        $update_data['LoginAttempts'] =  0;
                        $update_data['Comments'] = $reason;
                        $update_data['PinCode'] = $pinCode;
                        $update_data['TransferCredits'] = $allowCrdsTrnsfr;
                        $update_data['CanLoginFrmOtherCntry'] = $canLoginFromOther;
                        $update_data['TwoStepVerification'] = $loginVerification;
                       

                        $this->Clients_model->update_users($update_data , $id);

                        $message =  $this->lang->line('BE_GNRL_11');
                    }
                    //========================================== ADDING LOGIN COUNTRIES ==========================================//
                    if($this->input->post('loginCountries'))
                    {
                        $selectedIds = sizeof($this->input->post('loginCountries'));
                        $strData = '';
                        for($k = 0; $k < $selectedIds; $k++)
                        {
                            if($k > 0)
                                $strData .= ',';
                            $strData .= "('$id', '".check_input($this->input->post('loginCountries')[$k], $this->db->conn_id)."')";
                        }
                       
                        $this->Clients_model->del_user_login_countries($id);
                        if($strData != '')
                        {
                            $this->Clients_model->insert_login_countries($strData);
                            
                        }
                    }
                    //========================================== ADDING LOGIN COUNTRIES ==========================================//
                }
            }
        }
		$USER_VALUES = array();

		if($id > 0)
        {
            $row = $this->Clients_model->fetch_user_by_id($id);

            if (isset($row->UserName) && $row->UserName != '')
            {
                $uName = $row->UserName;
                $email = $row->UserEmail;
                $firstName = stripslashes($row->FirstName);
                $reason = stripslashes($row->Comments);
                $notes = stripslashes($row->Notes);
                $lastName = stripslashes($row->LastName);
                $phone = $row->Phone;
                $countryId = $row->CountryId;
                $disableUser = $row->DisableUser;
                $autoFill = $row->AutoFillCredits;
                $planId = $row->PricePlanId;
                $userType = $row->UserType;
                $currencyId = $row->CurrencyId;
                $ips = $row->IPs;
                $currCnvr = $row->CurrencyConversion;
                $ip = $row->IP;
                $pinCode = $row->PinCode != '' ? theString_Decrypt($row->PinCode) : '';
                $chkCanLoginFrmOther = $row->CanLoginFrmOtherCntry;
                $subscribed = $row->Subscribed;
                $sendSMS = $row->SendSMS;
                $transferCredits = $row->TransferCredits;
                $loginVerification = $row->TwoStepVerification;
                
                $rsCols = $this->Clients_model->fetch_registration_fields();


            }
			$this->data['rsFields'] = $this->Clients_model->fetch_registration_fields();
			foreach($this->data['rsFields'] as $rw )
			{
				$colName = $rw->FieldColName;
				$USER_VALUES[$rw->FieldColName] = $row->$colName;
			}

		}
		else
		{
			$this->data['rsFields'] = $this->Clients_model->fetch_registration_fields();

			foreach($this->data['rsFields'] as $rw )
			{
				$colName = $rw->FieldColName;
				$USER_VALUES[$rw->FieldColName] = "";
			}

		}
        

		$this->data['USER_VALUES'] = $USER_VALUES;
        $this->data['totalFields'] = count($this->data['rsFields']);
        $this->data['id'] = $id ;
        $this->data['uName'] = $uName;
        $this->data['email'] = $email;
        $this->data['firstName'] = $firstName;
        $this->data['lastName'] = $lastName;
        $this->data['phone'] = $phone;
        $this->data['ip'] = $ip;
        $this->data['currencyId'] = $currencyId;
        $this->data['countryId'] = $countryId;
        $this->data['autoFill'] = $autoFill;
        $this->data['chkCanLoginFrmOther'] = $chkCanLoginFrmOther;
        $this->data['subscribed'] = $subscribed;
        $this->data['sendSMS'] = $sendSMS;
        $this->data['transferCredits'] = $transferCredits;
        $this->data['loginVerification'] = $loginVerification;
        $this->data['pinCode'] = $pinCode;
        $this->data['disableUser'] = $disableUser;
        $this->data['notes'] = $notes;
        $this->data['reason'] = $reason;
        $this->data['planId'] = $planId;
        $this->data['message'] = $message ;
        $this->data['view'] = 'admin/user' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function users(){
        $strWhere = '';
        $fName = '';
        $lName = '';
        $uName = '';
        $txtlqry = '';
        $message = '';
        $email = '';
        $planId = '0';
        $this->data['page_name'] = $this->input->server('PHP_SELF');
        if(!isset($start))
            $start = 0;

        if($this->input->post_get("start"))
            $start = $this->input->post_get("start");
        $eu = ($start - 0);
        $limit = 50;
        $thisp = $eu + $limit;
        $back = $eu - $limit;
        $next = $eu + $limit;
        $gnrtKey = $this->input->post_get('gnrtKey') ? 1 : 0;
        $cldFrm = $this->input->post_get('cldFrm') ?: 0;
        $userId = $this->input->post_get('userId') ?: 0;
		$Id = $this->input->post_get('id') ?: 0;
		if($Id > 0)
		{
			$strWhere .= " AND UserId = '$Id'";
			$txtlqry .= "&userId=$Id";
		}
        if($userId > 0)
        {
            $strWhere .= " AND UserId = '$userId'";
            $txtlqry .= "&userId=$userId";
        }
        if($this->input->post_get('du') && $this->input->post_get('du') == '1')
        {
            $strWhere .= " AND DisableUser = 1";
            $txtlqry .= "&du=1";
        }
        $del = $this->input->post_get('del') ? 1 : 0;
        if($del == '1')
        {
            $ids = $this->input->post_get('id') ?: 0;
            updateUser($ids);
            $message = 'Client(s) have been archived successfully!';
        }
        if($this->input->post('btnSubmit') && $this->session->userdata('GSM_FSN_AdmId') && $cldFrm == '1')
        {
            $userIds = $this->input->post('chkUsers') ?: 0;
            $selectedUsers = '0';
            if(is_array($userIds))
                $selectedUsers = implode(",", $userIds);
            $allUserIds = $this->input->post('strUserIds') ?: 0;
            if($allUserIds != '0')
            {
                if(trim($allUserIds) == ''){
                    $allUserIds = '0';
                }
                if(trim($selectedUsers) == ''){
                    $selectedUsers = '0';
                }
                $update_data = array(
                  'DisableUser' => 0  
                );
                $where_in = array(
                    'UserId' => $allUserIds
                );

                $this->Clients_model->update_users($update_data , '' , $where_in);
                $update_data = array(
                    'DisableUser' => 1 
                );
                $where_in = array(
                      'UserId' => $selectedUsers
                );

                $this->Clients_model->update_users($update_data , '' , $where_in);
            }
            $message = $this->lang->line('BE_GNRL_11');	
        }

        if($this->input->post_get('txtEmail') && $this->input->post_get('txtEmail') != '')
        {
            $email = trim($this->input->post_get('txtEmail'));
            if($email != '')
            {
                $strWhere .= " AND UserEmail LIKE '%$email%'";
                $txtlqry .= "&txtEmail=$email";
            }
        }

        if($this->input->post_get('txtFName') && $this->input->post_get('txtFName') != '')
        {
            $fName = trim($this->input->post_get('txtFName'));
            if($fName != '')
            {
                $strWhere .= " AND FirstName LIKE '%$fName%'";
                $txtlqry .= "&txtFName=$fName";
            }
        }

        if($this->input->post_get('txtLName') && $this->input->post_get('txtLName') != '')
        {
            $lName = trim($this->input->post_get('txtLName'));
            if($lName != '')
            {
                $strWhere .= " AND LastName LIKE '%$lName%'";
                $txtlqry .= "&txtLName=$lName";
            }
        }
        if($this->input->post_get('txtUName') && $this->input->post_get('txtUName') != '')
        {
            $uName = trim($this->input->post_get('txtUName'));
            if($uName != '')
            {
                $strWhere .= " AND UserName LIKE '%$uName%'";
                $txtlqry .= "&txtUName=$uName";
            }
        }
        if($this->input->post_get('planId') && $this->input->post_get('planId') != '0')
        {
            $planId = $this->input->post_get('planId');
            $strWhere .= " AND A.PricePlanId = '$planId'";
            $txtlqry .= "&planId=$planId";
        }

        $this->data['rsCats'] = $this->Clients_model->fetch_users_price_plans_currency($strWhere , $eu, $limit);
        $this->data['count'] = count($this->data['rsCats']);
        if($this->data['count']  > 0)
        {
            $this->data['rsGroups'] = $this->Clients_model->fetch_price_plans();
        }

       
        $this->data['users_row']=$this->Clients_model->fetch_users_count_by_archived($strWhere);

        $this->data['userId'] = $userId ;
        $this->data['fName'] = $fName;
        $this->data['uName'] = $uName;
        $this->data['lName'] = $lName;
        $this->data['email'] = $email;
        $this->data['planId'] = $planId;
        $this->data['back'] = $back ; 
        $this->data['start'] = $start ;
        $this->data['txtlqry'] = $txtlqry;
        $this->data['limit'] = $limit ;
        $this->data['eu']= $eu ; 
        $this->data['pLast'] =  '' ;
        $this->data['thisp'] = $thisp; 
        $this->data['next'] = $next ;


        $this->data['message'] = $message ;
        $this->data['view'] = 'admin/users' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }


    public function downloadcustomers(){
        $gId = $this->input->post_get('gId') ?: 0;
        $ids = $this->input->post_get('ids') ?: 0;
        
        if($gId > 0)
            $strAnd = " AND PricePlanId = '$gId'";
        
        if($ids != '0')
            $strAnd = " AND UserId IN (".$ids.")";
        
        $str = "No.,Emai, UserName,Credits,Phone,Country\n";
        $rsEmails = $this->Clients_model->fetch_users_countries_currency($strAnd);
        
        foreach($rsEmails as $row)
        {
            $this->crypt_key($row->UserId);
            $myCredits = $this->decrypt($row->Credits);
            if($myCredits == '')
                $myCredits = '-';
            else
                $myCredits = $row->CurrencyAbb.' '.roundMe($myCredits);
            $str .= $row->UserEmail . ",";
            $str .= $row->UserName . ",";
            $str .= $myCredits . ",";
            $str .= $row->Phone . ",";
            $str .= $row->Country . "\n";
        }
        if($str != '')
        {
            header("Pragma: public");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header('Content-Disposition: attachment; filename="Customers-'.date('Y-m-d H m s').'.csv"');
            print($str);
        }
        else
            echo 'No Record Found!';
        exit; 
    }

    public function lgn_a_s_clint(){
        if($this->session->userdata('GSM_FUS_UserId') && $this->session->userdata('GSM_FUS_UserId')!='')
        {
            $this->session->unset_userdata('GSM_FUS_UserId');
            $this->session->unset_userdata('UserFullName');
            $this->session->unset_userdata('UserName');
            $this->session->unset_userdata('UserEmail');
            $this->session->unset_userdata('UserType');
            $this->session->unset_userdata('LANGUAGE');
            $this->session->unset_userdata('CLIENT_GF_IP');
            $this->session->unset_userdata('CL_SITE_TITLE');
            $this->session->unset_userdata('CL_LOGO_PATH');
            $this->session->unset_userdata('CL_COPY_RIGHTS');
            $this->session->unset_userdata('LIVE_CHAT_CODE');
            $this->session->unset_userdata('CLIENT_NEW_POPUP');
            $this->session->unset_userdata('ORDERED_FROM');
			$this->session->unset_userdata('UserFName');
            
        }
        $userId = $this->input->post_get("uId") ?: 0;
        $userId = sanitizeAndEscapingXXS($userId);
        $rowUser = $this->Clients_model->fetch_users_by_userid($userId);
       
        if (isset($rowUser->UserId) && $rowUser->UserId != '')
        {
            $uName = $rowUser->UserName;
            $this->session->set_userdata('GSM_FUS_UserId' , $rowUser->UserId)  ;
            $this->session->set_userdata('UserFullName' , stripslashes($rowUser->FullName));
			$this->session->set_userdata('UserFName' , stripslashes($rowUser->FirstName))  ;
            $this->session->set_userdata('UserName' , $rowUser->UserName);
            $this->session->set_userdata('UserEmail' , $rowUser->UserEmail) ;
            $this->session->set_userdata('LANGUAGE' , 'en');
            $this->session->set_userdata('CLIENT_GF_IP' , $this->input->server('REMOTE_ADDR')) ;
            $this->session->set_userdata('ORDERED_FROM' , '2')  ;
        
            $row = $this->Clients_model->fetch_maintenance_count();
           
            $rsStngs = $this->Clients_model->fetch_email_settings();
            $THEME = $rsStngs->Theme;
            $this->session->userdata('THEME' , $THEME);
            $rSetLogo = $this->Clients_model->fetch_logo_details();
           
            foreach($rSetLogo as $rw )
            {
                if (isset($rw->LogoPath) && $rw->LogoPath != '')
                {
                    if($rw->LogoId == '2')
                        $this->session->userdata('CL_LOGO_PATH' , "uplds$THEME/".$rw->LogoPath)  ;
                    if($rw->LogoId == '6')
                        $this->session->userdata('CL_FAV_ICO' , "uplds$THEME/".$rw->LogoPath);
                }
            }
            redirect(base_url('dashboard'));
        }
    }

    public function credithistory(){
        $userId = $this->input->post_get('userId') ?: 0;
       
        $this->data['key']=crypt_key($userId);
        $strWhere = $dtFrom = $dtTo = $message = '';
        $txtlqry = "&userId=".$userId;

        if($this->input->post_get('txtFromDt'))
        {
            $dtFrom = $this->input->post_get('txtFromDt') ?: '';
            $dtTo =  $this->input->post_get('txtToDt') ?: '';
            if($dtFrom != '' && $dtTo != '')
            {
                    $strWhere .= " And DATE(HistoryDtTm) >= '$dtFrom' AND DATE(HistoryDtTm) <= '$dtTo'";
                    $txtlqry .= "&txtFromDt=$dtFrom&txtToDt=$dtTo";
            }
            else
            {
                if($dtFrom != '')
                {
                    $strWhere .= " AND DATE(HistoryDtTm) >= '$dtFrom'";
                    $txtlqry .= "&txtFromDt=$dtFrom";
                }
                if($dtTo!= '')
                {
                    $strWhere .= " AND DATE(HistoryDtTm) <= '$dtTo'";
                    $txtlqry .= "&txtToDt=$dtTo";
                }
            }
        }
        $this->data['page_name'] = $this->input->server('PHP_SELF');
        if(!isset($start)){
            $start = 0;
        }
        if($this->input->post_get("start")){
            $start = $this->input->post_get("start");
        }
        $eu = ($start - 0);
        $limit = 50;
        $thisp = $eu + $limit;
        $back = $eu - $limit;
        $next = $eu + $limit;
        $this->data['rsHistory'] = $this->Clients_model->fetch_credit_history($userId ,  $strWhere , $eu, $limit);
        
        $this->data['userId'] = $userId;
        $this->data['message'] = $message ;
        $this->data['dtFrom'] = $dtFrom;
        $this->data['dtTo'] = $dtTo;
        $this->data['back'] = $back ;
        $this->data['start'] = $start;
        $this->data['txtlqry'] = $txtlqry ;
        $this->data['limit'] = $limit;
        $this->data['eu'] = $eu;
        $this->data['pLast'] = '' ;
        $this->data['thisp'] = $thisp;
        $this->data['next'] = $next ;
        $this->data['strWhere'] = $strWhere;
    
        
        $this->data['view'] = 'admin/credit_history' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }


    public function addcredits(){
        $userId = $this->input->post_get('userId') ?: 0;
        $count = $allowODL = 0;
        $transactionId = $comments = $message = $newCredits = $odl = $msg = '';
        
        $this->crypt_key($userId);

        if($this->input->post_get('txtCredits') && $this->session->userdata('GSM_FSN_AdmId'))
        {
            $newCredits = $this->input->post_get('txtCredits') ?: 0;
            if(!is_numeric($newCredits) && strlen($newCredits) >= 7)
            {
                $message = $this->lang->line('BE_LBL_22');
            }
            else if(is_numeric($newCredits))
            {
                if($newCredits > 0)
                {
                    $credits = 0;
                    $row = $this->Clients_model->fetch_user_by_id($userId);
                    if (isset($row->Credits) && $row->Credits != '')
                        $credits = $row->Credits;
                    $decCredits = $this->decrypt($credits);
                    $updatedCredits = 0;
                    if(is_numeric($newCredits))
                    {
                        $curr = $this->input->post('curr') ?: '';
                        if($this->input->post('rdCrdType') == '0')
                        {
                            $decCredits += $newCredits;
                            $updatedCredits = $newCredits;
                            $desc = '+ Add Funds ';
                            $type = 'added';
                            $pStatusId = $this->input->post('pStatusId') ?: 0;
                            if($pStatusId == '2')
                                $pMethodId = $this->input->post('pMethodId') ?: 0;
                            else
                                $pMethodId = 0;
                        }
                        else if($this->input->post('rdCrdType') == '1')
                        {
                            $decCredits -= $newCredits;
                            $updatedCredits = '-'.$newCredits;
                            $desc = '- Rebated Funds ';
                            $type = 'rebated';
                            $pStatusId = '3';
                            $pMethodId = 0;
                        }
                        if(is_numeric($decCredits))
                        {
                            send_push_notification('Credits have been '.$type.' in your account!', $userId);
                            $transId = $this->input->post('txtTransactionId') ?: '';
                            $comments = $this->input->post('txtComments') ?: '';
                            $encNewCredits = $this->encrypt($newCredits);
                            $encCredits = $this->encrypt($decCredits);
                            
                            $update_data= array(
                                'Credits' => $encCredits,
                            );

                            $this->Clients_model->update_users($update_data , $userId , '');

                            $currDtTm = setDtTmWRTYourCountry();
                            $arrCurrDt = explode(' ', $currDtTm);
                            $currDt = $arrCurrDt[0];

                            $strCol = '';
                            $strVal = '';
                            $strCol1 = '';
                            $strVal1 = '';
                            if($pStatusId == '1')
                            {
                                $strCol1 = ', PayableAmount';
                                $strVal1 = ", '$newCredits'";
                            }
                            if($pStatusId == '2')
                            {
                                $strCol = ', PaidDtTm';
                                $strVal = ", '$currDtTm'";
                            }
                            if($this->input->post('rdCrdType') == '0')
                            {
                            
                                $paymentId = $this->Clients_model->insert_payments($strCol, $strCol1 , $encNewCredits, $userId,$pMethodId, $pStatusId, $currDtTm, $transId, $comments, $curr, $strVal  ,$strVal1);

                                $desc .= " (Invoice #$paymentId)";
                                $msg = " (Invoice #$paymentId)";
                            }
                            $insert_array = array(
                                'ByAdmin' => 1 ,
                                'UserId' => $userId,
                                'Credits' => $updatedCredits ,
                                'Description' => $desc ,
                                'IMEINo' => '' ,
                                'HistoryDtTm' => $currDtTm ,
                                'CreditsLeft' => $encCredits ,
                                'Comments' => $comments,
                            );
                           $this->Clients_model->insert_credit_history($insert_array);
                            
                            $message = "<b>$updatedCredits</b> credits have been successfully $type for ".$this->input->post('user_name')." <b>$msg</b>, set as ".$this->input->post('hdPSTXT');
                            
                            if($this->input->post('chkEmail') && $this->input->post('rdCrdType') == '0')
                            {
                                $USER_EMAIL = $this->input->post('userName') ?: '';
                                if($USER_EMAIL != '')
                                {
                                    $uName = $this->input->post('uName') ?: '';
                                    $curr = $this->input->post('curr') ?: '';
                                    if($paymentId != '')
                                    {
                                        if($pMethodId > 0)
                                        {
                                            $rwPM = $this->Clients_model->fetch_payment_methods_by_id($pMethodId);
                                           
                                            $paymentMethod = $rwPM->PaymentMethod;
                                        }
                                        else{
                                            $paymentMethod = '-';
                                        }
                                        invoiceEmail($USER_EMAIL, $this->input->post('user_name'), $paymentId, $newCredits, $newCredits, $curr, $currDt, $paymentMethod, '6', '', '', $message);
                                    }
                                }
                            }
                        }
                        
                    }
                }
                else
                {
                    $message = $this->lang->line('BE_LBL_23');
                }
            }
        }
        if($this->input->post('btnODL'))
        {
        
            $allowODL = $this->input->post('chkODL') ? 1 : 0;
            $odl = $this->input->post('txtODL') ?: 0;
            echo $odl ;
            if(!is_numeric($odl))
            {
                $message = 'Invalid value for Overdraft limit.';
            }
            else if(is_numeric($odl))
            {
                $update_data = array(
                    'AllowNegativeCredits' => $allowODL ,
                    'OverdraftLimit' => $odl
                );

                $this->Clients_model->update_users($update_data , $userId );
                
                $message = 'Overdraft limit has been updated successfully!';
            }
        }

        $USER_EMAIL = '';
        $myCredits = '0';
        if($userId > 0)
        {
            $row = $this->Clients_model->fetch_users_currency_data($userId);
          
            if (isset($row->UserId) && $row->UserId != '')
            {
                $USER_EMAIL = $row->UserEmail;
                $currency = $row->Currency;
                $name = $row->Name;
                $myCredits = $this->decrypt($row->Credits);
                $allowODL = $row->AllowNegativeCredits;
                $odl = $row->OverdraftLimit;
            }
            if($currency == '')
            {
                $row = fetch_currency_data();
                if (isset($row->Currency) && $row->Currency != '')
                    $currency = $row->Currency;
            }
            if($myCredits == ''){
                $myCredits = '0';
            }
        }

        $this->data['message'] = $message;
        $this->data['userId'] = $userId;
        $this->data['USER_EMAIL'] = $USER_EMAIL;
        $this->data['name'] = $name;
        $this->data['currency'] = $currency;
        //$this->data['userName'] = $userName;
        $this->data['myCredits'] = $myCredits;
        $this->data['newCredits'] = $newCredits;
        $this->data['transactionId'] = $transactionId;
        $this->data['comments'] = $comments;
        $this->data['odl'] = $odl;
        $this->data['allowODL'] = $allowODL;


        $this->data['view'] = 'admin/add_credits' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function ajxuser(){
        $purpose = $this->input->post('purpose');

        if($purpose == 'updStatus')
        {
            $id = $this->input->post('id');
            $uStatus = $this->input->post('uStatus');
            $uStatus = $uStatus == '1' ? '0' : '1';
            $update_data = array(
                'DisableUser' => $uStatus
            );
            $this->Clients_model->update_users($update_data , $id);
            userStatusEmail($id);
            $msg =  $this->lang->line('BE_GNRL_11');
        }	
        echo $msg;
    }

    public function userapi(){
        $userId = $this->input->post_get('userId') ?: 0;
        $apis = $message = $apiKey = '';
        $disable = 0;
        $gnrtKey = $this->input->post_get('gnrtKey') ? 1 : 0;
        if($gnrtKey == '1' && $this->session->userdata('GSM_FSN_AdmId'))
        {
            for ($i=0; $i<2; $i++)
            {
                $apiKey .= chr(rand(65,90)).chr(rand(48,57));
            }
            $apiKey .= '-';
            for ($i=0; $i<2; $i++)
            {
                $apiKey .= chr(rand(65,90)).chr(rand(48,57));
            }
            $apiKey .= '-';
            for ($i=0; $i<2; $i++)
            {
                $apiKey .= chr(rand(65,90)).chr(rand(48,57));
            }
            $apiKey .= '-';
            for ($i=0; $i<2; $i++)
            {
                $apiKey .= chr(rand(65,90)).chr(rand(48,57));
            }
            $apiKey .= '-';
            for ($i=0; $i<2; $i++)
            {
                $apiKey .= chr(rand(65,90)).chr(rand(48,57));
            }
            $encAPIKey = encryptThe_String($apiKey);

            $update_data = array(
                'APIKey' => $encAPIKey
            );

            $this->Clients_model->update_users($update_data , $userId);
         
            $message = 'API Key has been generated successfully!';
        }

        if($this->input->post('btnSave'))
        {
            $apiAllow = $this->input->post('rdAPIAllow') ? 1 : 0;
            $update_data = array(
                'AllowAPI' => $apiAllow
            );

            $this->Clients_model->update_users($update_data , $userId);
            $arrIPs = explode("\n", $this->input->post("txtIPs"));
            $strIPs = '';
            for($i=0; $i<sizeof($arrIPs); $i++)
            {
                if(trim($arrIPs[$i]) != '')
                {
                    if($i > 0)
                        $strIPs .= ',';
                    $strIPs .= "(".$userId.",'".trim($arrIPs[$i])."')";
                }
            }
            $this->Clients_model->delete_user_api_ips($userId);
            
            if($strIPs != '')
            {
                $this->Clients_model->insert_api_ips($strIPs);
                
            }
            if($this->input->post('chkEmail'))
            {
                $row = $this->Clients_model->fetch_users_concatenated_data($userId);
        
                if (isset($row->UserEmail) && $row->UserEmail != '')
                {
                    apiKeyGnrtnEmail($row->UserEmail, stripslashes($row->CustomerName), theString_Decrypt($row->APIKey));
                }
            }
            $message = $this->lang->line('BE_GNRL_11');
        }
        if($userId > 0)
        {
            $row = $this->Clients_model->fetch_user_by_id($userId);
            if (isset($row->AllowAPI))
            {
                $apiAllow = $row->AllowAPI;
                if($row->APIKey)
                    $apiKey = theString_Decrypt($row->APIKey);
            }
            $rsIPs = $this->Clients_model->fetch_user_api_ips($userId);
            
            $ips = '';
            foreach($rsIPs as $row )
            {
                if($ips == '')
                    $ips = $row->IP;
                else
                    $ips .= "\n".$row->IP;
            }
        }

        $this->data['message'] = $message ;
        $this->data['userId'] = $userId;
        $this->data['apiAllow'] = $apiAllow;
        $this->data['apiKey'] = $apiKey;
        $this->data['ips'] = $ips;
        $this->data['view'] = 'admin/user_api' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function payments(){
        $pMethodId = 0;
        $invoicesBy = '-1';
        $strWhere = $message = $txtlqry = $fName = $lName = $uName = $strWhere1 = $pyrEml = $byAdmin = '';
        $this->data['page_name'] = $this->input->server('PHP_SELF');
            

        if($this->input->post_get('start'))
            $start = $this->input->post_get('start');
        else
            $start = 0;

        $eu = ($start - 0);
        $limit = $this->input->post('records') ?: 50;
        $thisp = $eu + $limit;
        $back = $eu - $limit;
        $next = $eu + $limit;
        $dtFrom = $this->input->post_get('txtFromDt') ?: '';
        $dtTo = $this->input->post_get('txtToDt') ?: '';
        $transNo = $this->input->post_get('txtTransNo')?: '';
        $paymentId = $this->input->post_get('txtPaymentId') ?: '';

        $cldFrm = $this->input->post_get('cldFrm') ?: 0;
        if($this->input->post_get('byAdmin'))
            $byAdmin =  $this->input->post_get('byAdmin');
        $userId = $this->input->post_get('userId') ?: 0;
        $pStatusId = $this->input->post_get('pStatusId') ?: 0;
        if($byAdmin != '')
        {
            $strWhere = " AND ByAdmin = '$byAdmin'";
            $txtlqry .= "&byAdmin=$byAdmin";
        }
        if($transNo != '')
        {
            $strWhere = " AND TransactionId = '$transNo'";
            $txtlqry .= "&txtTransNo=$transNo";
        }
        if($userId > 0)
        {
            $strWhere .= " AND A.UserId = '$userId'";
            $txtlqry .= "&userId=$userId";
        }
        if($pStatusId != '0')
        {
            $strWhere .= " AND A.PaymentStatus = '$pStatusId'";
            $txtlqry .= "&pStatusId=$pStatusId";
        }
        if($paymentId != '')
        {
            $strWhere .= " AND PaymentId LIKE '%$paymentId%'";
            $txtlqry .= "&txtPaymentId=$paymentId";
        }

        if($this->input->post_get('pMethodId'))
        {
            $ids = '0';
            $arrIds = array();
            if($this->input->post('chkPayments'))
            {
                $arrIds = $this->input->post('chkPayments') ?: 0;
                $ids = implode(',', $arrIds);
            }
            if($cldFrm == '1')
            {
              
                $this->Clients_model->del_payments($ids);
                $message = 'Payment(s) have been deleted succesfully!';
            }
            else if($cldFrm == '2')
            {
                $update_data = array(
                    'PaymentStatus' => 2
                );

                $where_in = array(
                    'PaymentId' => $ids ,
                );

                $this->Clients_model->update_payments($update_data , '' , $where_in);    
                $message = 'Payment(s) have been updated succesfully!';
            }
            else if($cldFrm == '3')
            {
                $update_data = array(
                    'PaymentStatus' => 1
                );

                $where_in = array(
                    'PaymentId' => $ids ,
                );
                $this->Clients_model->update_payments($update_data , '' , $where_in);

                $message = 'Payment(s) have been updated succesfully!';
                
            }
            else if($cldFrm == '4')
            {
                $update_data = array(
                    'PaymentStatus' => 3
                );

                $where_in = array(
                    'PaymentId' => $ids ,
                );
                $this->Clients_model->update_payments($update_data , '' , $where_in);
                
                $totalIds = sizeof($arrIds);
                for($v = 0; $v < $totalIds; $v++)
                {
                    rebateUserCredits($arrIds[$v]);
                }
                $message = 'Payment(s) have been updated succesfully!';
            }
            $TICKED_IDS = sizeof($arrIds);
            
            if($TICKED_IDS > 0)
            {
                if($cldFrm == '2' || $cldFrm == '3')
                {
                    for($k = 0; $k < $TICKED_IDS; $k++)
                    {
                        if($_POST['txtTransID'.$arrIds[$k]] != $_POST['hdTransID'.$arrIds[$k]])
                        {
                            $transID = $this->input->post('txtTransID'.$arrIds[$k]);

                            $update_data = array(
                                'TransactionId' => $transID
                            );
            
                            $where = array(
                                'PaymentId' => $arrIds[$k] ,
                            );
                            $this->Clients_model->update_payments($update_data , $where );

                        }
                    }
                }
            }
            $fName = $this->input->post_get('txtFName') ? trim($this->input->post_get('txtFName')) : '';
            $lName = $this->input->post_get('txtLName') ? trim($this->input->post_get('txtLName')) : '';
            $uName = $this->input->post_get('txtUName') ? trim($this->input->post_get('txtUName')) : '';
            $pyrEml = $this->input->post_get('txtPyrEml') ? trim($this->input->post_get('txtPyrEml')) : '';
            $pMethodId = $this->input->post_get('pMethodId') ? $this->input->post_get('pMethodId') : 0 ;
            $invoicesBy = $this->input->post_get('invoicesBy') ? $this->input->post_get('invoicesBy') : 0;


            if($dtFrom != '')
                $txtlqry .= "&txtFromDt=$dtFrom";
            if($dtTo != '')
                $txtlqry .= "&txtToDt=$dtTo";
            if($dtFrom != '' && $dtTo != '')
                $strWhere .= " And DATE(PaymentDtTm) >= '" . $dtFrom . "' AND DATE(PaymentDtTm) <= '" . $dtTo . "'";
            else
            {
                if($dtFrom != '')
                    $strWhere .= " AND DATE(PaymentDtTm) >= '$dtFrom'";
                if($dtTo!= '')
                    $strWhere .= " AND DATE(PaymentDtTm) <= '$dtTo'";
            }
            if($fName != '')
            {
                $strWhere .= " AND FirstName LIKE '%$fName%'";
                $txtlqry .= "&txtFName=$fName";
            }
            if($lName != '')
            {
                $strWhere .= " AND LastName LIKE '%$lName%'";
                $txtlqry .= "&txtLName=$lName";
            }
            if($uName != '')
            {
                $strWhere .= " AND B.UserName LIKE '%$uName%'";
                $txtlqry .= "&txtUName=$uName";
            }
            if($pyrEml != '')
            {
                $strWhere .= " AND A.PayerEmail LIKE '%$pyrEml%'";
                $txtlqry .= "&txtPyrEml=$pyrEml";
            }
            if($pMethodId != '0')
            {
                $strWhere .= " AND A.PaymentMethod = '$pMethodId'";
                $txtlqry .= "&pMethodId=$pMethodId";
            }
            if($invoicesBy != '-1')
            {
                $strWhere .= " AND ByAdmin = '$invoicesBy'";
                $txtlqry .= "&invoicesBy=$invoicesBy";
            }
        }

        $strWherePmnts = " AND PaymentStatusId = '1'";
        if (array_key_exists("321",  $this->data['ARR_ADMIN_FORMS']))
        {
            $strWherePmnts = '';
        }
       
        $this->data['rsPayments'] = $this->Clients_model->fetch_users_status_and_payments($strWherePmnts , $strWhere , $eu, $limit); 
        $this->data['count'] = count($this->data['rsPayments']);

        $this->data['userId'] = $userId;
        $this->data['message'] = $message;
        $this->data['fName'] = $fName;
        $this->data['lName'] = $lName;
        $this->data['uName'] = $uName;
        $this->data['paymentId'] = $paymentId;
        $this->data['strWherePmnts'] = $strWherePmnts;
        $this->data['dtFrom'] = $dtFrom;
        $this->data['dtTo'] = $dtTo;
        $this->data['pyrEml'] = $pyrEml;
        $this->data['invoicesBy'] = $invoicesBy;
        $this->data['transNo'] = $transNo;
        $this->data['byAdmin'] = $byAdmin;
        $this->data['userId'] = $userId;
        $this->data['strWhere'] = $strWhere ;
        $this->data['back'] = $back;
        $this->data['start'] = $start;
        $this->data['txtlqry'] = $txtlqry;
        $this->data['limit'] = $limit;
        $this->data['eu'] = $eu;
        $this->data['pLast'] = '';
        $this->data['thisp'] = $thisp;
        $this->data['next'] = $next;
      
        $this->data['view'] = 'admin/payments' ;
        $this->load->view('admin/layouts/default1' , $this->data);

    }


    public function payment(){
        $id = $this->input->post_get('id') ?: 0;
        $userId = 0;
        $credits = '';
        $pMethodId = 0;
        $pStatusId = 0;
        $transactionId = '';
        $comments = '';
        $message = '';
		$amount1 = '' ;
		$comments1 = '' ;
        $transactionId1 = '' ;
        if($this->input->post('btnAddPmnt'))
        {
       
            $amount = $this->input->post('txtAmount1') ?: 0;
            if(is_numeric($amount) && $amount > 0)
            {
                $invAmnt = $this->input->post('invAmnt') ?: 0;
                $pMethodId1 = $this->input->post('pMethodId1') ?: 0;
                $transactionId1 = $this->input->post('txtTransactionId1') ?: 0;
                $comments1 = $this->input->post('txtComments1') ?: 0;
                $currDtTm = setDtTmWRTYourCountry();
                
                $insert_data = array(
                    'InvoiceId' => $id ,
                    'InvAmount' => $amount ,
                    'InvTransactionId' => $transactionId1 ,
                    'InvPaymentMethodId' => $pMethodId1,
                    'InvDtTm' => $currDtTm ,
                    'InvComments' => $comments1
                );

                $this->Clients_model->insert_payment_details($insert_data);
              
                
                $payableAmount = $invAmnt - $amount;
                
                $update_data = array(
                    'PayableAmount' => $payableAmount,
                );

                $where = array(
                    'PaymentId' =>  $id
                );
                
                $this->Clients_model->update_payments($update_data , $where);


                $message = $this->lang->line('BE_GNRL_11');
            }
            else
            {
                $message = $this->lang->line('BE_LBL_627');
            }
        }

        if($id > 0)
        {
            $row = $this->Clients_model->fetch_payments_users_data($id);
          
            if (isset($row->UserId) && $row->UserId != '')
            {
               
                $this->crypt_key($row->UserId);
                $myCredits = $this->decrypt($row->Credits);
                if($myCredits == '')
                    $myCredits = '-';
                $userId = $row->UserId;
                $pMethodId = $row->PaymentMethod;
                $pStatusId = $row->PaymentStatus;
                $transactionId = $row->TransactionId;
                $comments = stripslashes($row->Comments);
                $creditsTransferred = $row->CreditsTransferred;
                $byAdmin = $row->ByAdmin;
                $amountPayable = $row->PayableAmount == '' ? 0 : $row->PayableAmount;
                $currency = $row->Currency;
                $paidDtTm = $row->PaidDtTm;
                $pEmail = stripslashes($row->PayerEmail);
                $userName = $row->UserName;
            }
        }

        $this->data['id'] = $id ;
        $this->data['message'] = $message ;
        $this->data['pStatusId'] = $pStatusId;
        $this->data['byAdmin'] = $byAdmin;
        $this->data['userId'] = $userId;
        $this->data['userName'] = $userName;
        $this->data['pMethodId'] = $pMethodId;
        $this->data['transactionId'] = $transactionId;
        $this->data['pEmail']  = $pEmail;
        $this->data['myCredits']  = $myCredits;
        $this->data['amountPayable'] = $amountPayable;
        $this->data['paidDtTm'] = $paidDtTm;
        $this->data['creditsTransferred'] = $creditsTransferred;
        $this->data['comments'] = $comments;
        $this->data['pMethodId'] = $pMethodId;
        $this->data['transactionId1'] = $transactionId1;
        $this->data['amount1'] = $amount1;
        $this->data['comments1'] = $comments1;

        $this->data['rsPayments'] = $this->Clients_model->fetch_payment_details_method($id);
       


        $this->data['invoice_tab_general'] = $this->load->view('admin/invoice_tab_general' , $this->data , TRUE);
        $this->data['invoice_tab_addpmnt'] = $this->load->view('admin/invoice_tab_addpmnt' , $this->data , TRUE);
        $this->data['invoice_tab_payments'] = $this->load->view('admin/invoice_tab_payments' , $this->data , TRUE);

        $this->data['view'] = 'admin/payment' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }


    public function ajxpayment(){
        $purpose = $this->input->post('purpose');
       
        if($purpose == 'save')
        {
            $qry = '';
            $id = $this->input->post('id');
            $transactionId = $this->input->post('transactionId');
            $pMethodId = $this->input->post('pMethodId');
            $pStatusId = $this->input->post('pStatusId');
            $comments = $this->input->post('comments');
            $transferCredits = $this->input->post('transferCredits');
            $pEmail = $this->input->post('pEmail')?: '';
            $notify = $this->input->post('notify');
            $msg = '';

            if($transferCredits == '1')
            {
                $rwPm = $this->Clients_model->fetch_payments_by_id($id);
               
                if (isset($rwPm->CreditsTransferred) && $rwPm->CreditsTransferred != '1')
                {
                    if (isset($rwPm->UserId) && $rwPm->UserId != '')
                    {
                        $userId = $rwPm->UserId;
                        $this->crypt_key($userId);
                        $myCredits = $this->decrypt($rwPm->Credits);
                        if($myCredits == '')
                            $myCredits = '0';
            
                        $credits = 0;
                        $rw = $this->Clients_model->fetch_users_credits();
                       
                        if (isset($rw->Credits) && $rw->Credits != '')
                            $credits = $rw->Credits;
                        $decCredits = $crypt->decrypt($credits);
                        $decCredits += $myCredits;
                        $encCredits = $crypt->encrypt($decCredits);
            
                        $update_data = array(
                            'Credits'=> $encCredits
                        );

                        $this->Clients_model->update_users($update_data , $userId );
                        $currDtTm = setDtTmWRTYourCountry();
            
                        $hstDesc = "+ Add Funds (Invoice #$id)";
                        
                        $insert_array = array(
                            'ByAdmin' => 1 ,
                            'UserId' => $userId,
                            'Credits' => $myCredits,
                            'Description' => $hstDesc ,
                            'IMEINo' => '' ,
                            'HistoryDtTm' => $currDtTm,
                            'CreditsLeft' => $encCredits,
                            'PaymentId' => $id
                        );

                        $this->Clients_model->insert_credit_history($insert_array);
                        
                        $qry = ", CreditsTransferred = 1";
                        send_push_notification($myCredits.' Credits have been added in your account!', $userId);
                    }
                }
            }
            
            $rwPm = $this->Clients_model->fetch_payments_users_methods($id);
           
            if($notify == '1')
            {
                $notificationType = $this->input->post('notificationType');
                if($notificationType == '0')
                {
                    $invType = 3;
                }
                else if($notificationType == '1')
                {
                    $invType = 4;
                }
                $userId = $rwPm->UserId;
                $this->crypt_key($userId);
                $myCredits = $this->decrypt($rwPm->Credits);
                if($myCredits == '')
                    $myCredits = '0';
                $INV_AMOUNT = $this->decrypt($rwPm->Amount);
                $INV_CURRENCY = stripslashes($rwPm->Currency);
                $PAYMENT_METHOD = stripslashes($rwPm->PaymentMethod);
                $INV_DT = $rwPm->PaymentDtTm;
                invoiceEmail($rwPm->UserEmail, $rwPm->UserName, $id, $myCredits, $INV_AMOUNT, $INV_CURRENCY, $INV_DT, $PAYMENT_METHOD, $invType);
               
            }

            $dtTm = setDtTmWRTYourCountry();
            $strCol = '';
            $update_data = array();
            if($pStatusId == '2')
            {

                $update_data['PaidDtTm'] = $dtTm;
            }
            else if($pStatusId == '3')
            {
                rebateUserCredits($id);
            }
            $update_data['PaymentMethod'] =  $pMethodId;
            $update_data['PaymentStatus'] =$pStatusId;
            $update_data['TransactionId'] =  $transactionId;
            $update_data['CreditsTransferred'] =  1;
            $update_data['UpdatedAt'] =  $dtTm;
            $update_data['PayerEmail'] =  $pEmail;
            $update_data['Comments'] =  $comments;

            $where = array(
                'PaymentId' => $id
            );
            
            $this->Clients_model->update_payments($update_data , $where);
           
            $msg =  $this->lang->line('BE_GNRL_11');
        }	
        
        echo $msg;
    }

    public function invoice(){

    }

    public function autopayments(){
		$strWhere = '';
		$fName = '';
		$ip = '';
		$uName = '';
		$txtlqry = '';
		$message = '';
		$email = '';
		$autoPmnts = '' ;
		$canAddCrdts = '' ;
		$countryId = '0';
        $this->data['page_name'] = $this->input->server('PHP_SELF');
        if(!isset($start))
            $start = 0;

        if($this->input->post_get('start'))
            $start = $this->input->post_get('start');
        $eu = ($start - 0);
        $limit = 100;
        $thisp = $eu + $limit;
        $back = $eu - $limit;
        $next = $eu + $limit;
        $uId = $this->input->post_get('uId') ? $this->input->post_get('uId') : 0;
        $cldFrm = $this->input->post_get('cldFrm') ? $this->input->post_get('cldFrm') : 0;
        if($this->input->post('chkUsers') && $this->session->userdata('GSM_FSN_AdmId') && $cldFrm > 0)
        {
            $userIds = $this->input->post('chkUsers') ?: 0;
            $selectedUsers = '0';
            if(is_array($userIds))
            {
                $selectedUsers = implode(",", $userIds);
                switch($cldFrm)
                {
                    case '1':
                        $col = 'AutoFillCredits';
                        $val = '1';
                        break;
                    case '2':
                        $col = 'AutoFillCredits';
                        $val = '0';
                        break;
                    case '3':
                        $col = 'CanAddCredits';
                        $val = '1';
                        break;
                    case '4':
                        $col = 'CanAddCredits';
                        $val = '0';
                        break;
                }
                $update_data = array(
                    $col => $val
                );
                $where_in = array(
                    'UserId' => $selectedUsers
                );
                $this->Clients_model->update_users($update_data , '' , $where_in);
               
            }
            $message = $this->lang->line('BE_GNRL_11');	
        }
        if($this->input->post_get('ap') && $this->input->post_get('ap') == '0' || $this->input->post_get('ap') == '1')
        {
            $update_data = array(
                'AutoFillCredits' => $this->input->post_get('ap'),
            );
            $where = array(
                'UserId' => $uId,
            );
            $this->Clients_model->update_users($update_data , $where);
           
            $message = $this->lang->line('BE_GNRL_11');	
        }
        if($this->input->post_get('ac') && $this->input->post_get('ac') == '0' || $this->input->post_get('ac') == '1')
        {
            $update_data = array(
                'CanAddCredits' =>$this->input->post_get('ac'),
            );
            $where = array(
                'UserId' => $uId,
            );
            $this->Clients_model->update_users($update_data , $where);
            
            $message = $this->lang->line('BE_GNRL_11');	
        }
        if($this->input->post_get('txtEmail') && $this->input->post_get('txtEmail') != '')
        {
            $email = trim($this->input->post_get('txtEmail'));
            if($email != '')
            {
                $strWhere .= " AND UserEmail LIKE '%".$email."%'";
                $txtlqry .= "&txtEmail=$email";
            }
        }
        if($this->input->post_get('autoPmnts') && $this->input->post_get('autoPmnts') != '-1')
        {
            $autoPmnts = $this->input->post_get('autoPmnts');
            $strWhere .= " AND AutoFillCredits = '$autoPmnts'";
            $txtlqry .= "&autoPmnts=$autoPmnts";
        }
        if($this->input->post_get('canAddCrdts') && $this->input->post_get('canAddCrdts') != '-1')
        {
            $canAddCrdts = $this->input->post_get('canAddCrdts');
            $strWhere .= " AND CanAddCredits = '$canAddCrdts'";
            $txtlqry .= "&canAddCrdts=$canAddCrdts";
        }
        if($this->input->post_get('txtIP') && $this->input->post_get('txtIP') != '')
        {
            $ip = trim($this->input->post_get('txtIP'));
            if($ip != '')
            {
                $strWhere .= " AND IP LIKE '%$ip%'";
                $txtlqry .= "&txtIP=$ip";
            }
        }
        if($this->input->post_get('txtUName') && $this->input->post_get('txtUName') != '')
        {
            $uName = trim($this->input->post_get('txtUName'));
            if($uName != '')
            {
                $strWhere .= " AND UserName LIKE '%".$uName."%'";
                $txtlqry .= "&txtUName=$uName";
            }
        }
        if($this->input->post_get('countryId') && $this->input->post_get('countryId') != '0')
        {
            $countryId = $this->input->post_get('countryId');
            $strWhere .= " AND A.CountryId = '$countryId'";
            $txtlqry .= "&countryId=$countryId";
        }
        $this->data['rsClnts'] = $this->Clients_model->fetch_users_countries($strWhere , $eu, $limit);
       
        $this->data['count'] = count($this->data['rsClnts']);
        $this->data['message'] = $message ;
        $this->data['uName'] = $uName;
        $this->data['email'] = $email;
        $this->data['countryId'] = $countryId;
        $this->data['ip'] =$ip;
        $this->data['autoPmnts'] = $autoPmnts;
        $this->data['canAddCrdts'] = $canAddCrdts;
        $this->data['strWhere'] = $strWhere;
    
        $this->data['back'] = $back;
        $this->data['start'] = $start;
        $this->data['txtlqry'] = $txtlqry;
        $this->data['limit'] = $limit;
        $this->data['eu'] = $eu;
        $this->data['pLast'] = '' ;
        $this->data['thisp'] = $thisp;
        $this->data['next'] = $next;
      


        $this->data['view'] = 'admin/autopayments' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function clientgroup(){
        $id = $this->input->post_get('id') ?: 0;
        $plan = '';
        $disable = $sPrices = $fPrices = $iPrices = 0;
        if($id > 0)
        {
            $row = $this->Clients_model->fetch_price_plans_by_id($id);
           
            $plan = $row->PricePlan;
            $iPrices = $row->IMEIPricesAtWeb;
            $fPrices = $row->FilePricesAtWeb;
            $sPrices = $row->ServerPricesAtWeb;
            $disable = $row->DisablePricePlan;
        }

        $this->data['id'] = $id;
        $this->data['plan'] = $plan;
        $this->data['iPrices'] = $iPrices;
        $this->data['fPrices'] = $fPrices;
        $this->data['sPrices'] = $sPrices;
        $this->data['disable'] = $disable;

        $this->data['view'] = 'admin/client_group' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function ajxclientgroup(){
        $purpose = $this->input->post_get('purpose');
        $msg = '';
        if ($purpose == 'save')
        {
            $id = $this->input->post_get('id');
            $group = $this->input->post_get('group');
            $iPrices = $this->input->post_get('iPrices');
            $fPrices = $this->input->post_get('fPrices');
            $sPrices = $this->input->post_get('sPrices');
            $disableVal = $this->input->post_get('disableVal');
           
            $row = $this->Clients_model->fetch_price_plans_by_id_and_group($group , $id);
            if (isset($row->PricePlanId) && $row->PricePlanId != '')
            {
                $msg = "'.$group.' ".$this->lang->line('BE_GNRL_10')."~0";
            }
            else
            {
                if($id == 0)
                {
                    $insert_data = array(
                        'PricePlan' => $group ,
                        'DisablePricePlan' => $disableVal ,
                        'IMEIPricesAtWeb' => $iPrices ,
                        'FilePricesAtWeb' => $fPrices ,
                        'ServerPricesAtWeb' => $sPrices
                    );

                    $this->Clients_model->insert_price_plans($insert_data);
                  
                    $msg =  $this->lang->line('BE_GNRL_11')."~1";
                }			
                else
                {
                    $update_data = array(
                        'PricePlan' => $group ,
                        'DisablePricePlan' => $disableVal ,
                        'IMEIPricesAtWeb' => $iPrices ,
                        'FilePricesAtWeb' => $fPrices ,
                        'ServerPricesAtWeb' => $sPrices
                    );
                
                    $where = array(
                        'PricePlanId' => $id ,
                    );
                    $this->Clients_model->update_price_plans($update_data ,  $where);

                    $msg = $this->lang->line('BE_GNRL_11')."~0";
                }
            }
            echo $msg;
        }
    }


    public function clientgroups(){
        $del = $this->input->post_get('del') ? 1 : 0;
        $message = '';
        if($del == '1')
        {
            $id = $this->input->post_get('id') ?: 0;
            $this->Clients_model->deletePricePlan($id);
            $message = $this->lang->line('BE_GNRL_12');
        }
        $this->data['rsGroups'] = $this->Clients_model->fetch_all_price_plans(); 
       
        $this->data['count'] = count($this->data['rsGroups']);
        $this->data['message'] = $message;
        
        $this->data['view'] = 'admin/clientgroups' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function plansprices(){
        $sc = $this->input->post_get('sc') ?: 0;
        $planId = $this->input->post_get('planId') ?: 0;
        $plan = $this->input->post_get('plan') ? $this->input->post_get('plan') : '';
        $categoryId = $this->input->post('categoryId') ?: 0;
        $strWhere = '';
        $strWhereCat = '';
        $count = 0;
        $message = '';
        $defaultCurrencyId = 0;
        $rsCurrencies = $this->Clients_model->fetch_currency_data();
        $currencyCount = count($rsCurrencies);
        $CURR_IDS = array();
        $CURR_ABV = array();
        $CURR_RT = array();
        $CURR_CONV_RATES = array();
        $index = 0;

        switch($sc)
        {
            case '0': // IMEI services
                $strWhere .= " AND sl3lbf = 0";
                $strWhereCat = " AND SL3BF = 0";
                $tblName = 'tbl_gf_package_category';
                $tblPckName = 'tbl_gf_packages';
                $colId = 'PackageId';
                $colTitle = 'PackageTitle';
                $colPrice = 'PackagePrice';
                $colDisable = 'DisablePackage';
                break;
            case '1': // File services
                $strWhere .= " AND sl3lbf = 1";
                $strWhereCat = " AND SL3BF = 1";
                $tblName = 'tbl_gf_package_category';
                $tblPckName = 'tbl_gf_packages';
                $colId = 'PackageId';
                $colTitle = 'PackageTitle';
                $colPrice = 'PackagePrice';
                $colDisable = 'DisablePackage';
                break;
            case '2': // File services
                $tblName = 'tbl_gf_log_package_category';
                $tblPckName = 'tbl_gf_log_packages';
                $colId = 'LogPackageId';
                $colTitle = 'LogPackageTitle';
                $colPrice = 'LogPackagePrice';
                $colDisable = 'DisableLogPackage';
                break;
            case '3': // Ecommerce Products
                $tblName = 'tbl_gf_category';
                $colId = 'ProductId';
                $colTitle = 'ProductName';
                $tblPckName = 'tbl_gf_products';
                break;
        }
        foreach($rsCurrencies as $row)
        {
            $CURR_CONV_RATES[$row->CurrencyId] = $row->ConversionRate;
            if($row->DefaultCurrency != '1')
            {
                $CURR_IDS[$index] = $row->CurrencyId;
                $CURR_ABV[$index] = $row->CurrencyAbb;
                $CURR_RT[$index] = $row->ConversionRate;
                $index++;
            }
            else
            {
                $defaultCurrencyTitle = $row->CurrencyAbb;
                $defaultCurrencyId = $row->CurrencyId;
            }
        }
        if($this->input->post('btnSubmitPrices'))
        {
            $postArr = convertPostToArray();
            $categoryId = $postArr['categoryId'];
            $str = '';
            $strPrices = '';
            $strPriceNotice = '';
            $strPacks = '0';
            $arrPckIds = $this->input->post_get('chkPrice') ?: 0;
            $totalPckIds = 0;
            if(is_array($arrPckIds))
                $totalPckIds = count($arrPckIds);
            $packIds = 0;

            for($i = 0; $i < $totalPckIds; $i++)
            {
                $data = explode('|', $arrPckIds[$i]);
                $price = trim($postArr['txtPrice'.$data[1].'_0']);
                if(is_numeric($price))
                {
                    $strPacks .= ", ".$postArr['packageId'.$data[1]];
                }
            }
            $PACKS = array();
            $rsPacks = $this->clients_model->fetch_rspacks($colId , $colTitle , $tblPckName, $strPacks);
           
            foreach($rsPacks as $row)
            {		
                $PACKS[$row->PackageId] = stripslashes($row->PackageTitle);
            }

            for($i = 0; $i < $totalPckIds; $i++)
            {
                $data = explode('|', $arrPckIds[$i]);
                if($i > 0)
                {
                    $str .= ',';
                    $strPrices .= ',';
                    $strPriceNotice .= ',';
                }
                $price = trim($postArr['txtPrice'.$data[1].'_0']);
                if(is_numeric($price))
                {
                    $str .= "(".$planId.", ".$defaultCurrencyId.", ".$postArr['packageId'.$data[1]].", '$price', '$sc')";
                    $strPrices .= "('$planId', '$defaultCurrencyId', '".$postArr['packageId'.$data[1]]."', '".$PACKS[$postArr['packageId'.$data[1]]]."', '$price', '$sc')";
                    $strPacks .= ", ".$postArr['packageId'.$data[1]];
                    $strPriceNotice .= "('$planId', '$defaultCurrencyId', '".$postArr['packageId'.$data[1]]."', '".$PACKS[$postArr['packageId'.$data[1]]]."', '$price', '$sc', 'USER_ID')";

                }
                for($z = 1; $z < $currencyCount; $z++)
                {
                    $str .= ',';
                    $strPrices .= ',';
                    $strPriceNotice .= ',';
                    $price = trim($postArr['txtOtherPrice'.$data[1].'_'.$z]);
                    if(is_numeric($price))
                    {
                        $str .= "('$planId', '".$postArr['otherCurrencyId'.$data[1].'_'.$z]."', '".$postArr['packageId'.$data[1]]."', '$price', '$sc')";
                        $strPrices .= "('$planId', '".$postArr['otherCurrencyId'.$data[1].'_'.$z]."', '".$postArr['packageId'.$data[1]]."', '".$PACKS[$postArr['packageId'.$data[1]]]."', '$price', '$sc')";
                        $strPriceNotice .= "('$planId', '".$postArr['otherCurrencyId'.$data[1].'_'.$z]."', '".$postArr['packageId'.$data[1]]."', '".$PACKS[$postArr['packageId'.$data[1]]]."', '$price', '$sc', 'USER_ID')";				
                    }
                    
                }
            }
            if($str != '')
            {
                $this->Clients_model->del_plans_packages_prices($planId , $sc , $strPacks);   
                $this->Clients_model->insert_plans_packages_prices($str);
               
            }
            if($strPrices != '' && $SEND_PRICE_EMAILS == '1')
            {
                $this->Clients_model->del_plans_prices_email($planId , $sc , $strPacks);
                $this->Clients_model->insert_plans_prices_email($strPrices);
               
            }
            if($strPriceNotice != '')
            {
                $this->Clients_model->del_group_prices_notice($planId , $sc , $strPacks);
                
                $rsPlanClients = $this->Clients_model->fetch_users_by_planid($planId);
               
                $strPriceNoticeNew = '';
                foreach($rsPlanClients as $row )
                {
                    if($strPriceNoticeNew != '')
                        $strPriceNoticeNew .= ',';
                    $strPriceNoticeNew .= str_replace('USER_ID', $row->UserId, $strPriceNotice);
                }
                if($strPriceNoticeNew != ''){
                    $this->Clients_model->insert_group_prices_notice($strPriceNoticeNew);
                    
                }
                    
            }
            $message = $this->lang->line('BE_GNRL_11');
        }

        if($categoryId != '0')
        {
            $strWhere .= " AND CategoryId = '$categoryId'";
            if($sc == '3')
            {
                $rSet = $this->Clients_model->fetch_product_categories_prices($planId , $defaultCurrencyId ,$sc , $strWhere);
                
            }
            else
            {
                $rSet = $this->Clients_model->fetch_packages_plans_packages_prices($colId ,$colTitle , $colPrice , $tblPckName , $planId , $defaultCurrencyId , $sc ,  $colDisable ,$strWhere);
            }
            $this->data['rSet'] = $rSet ;
            $count = count($this->data['rSet']);
            $PACK_PRICES = getCurrencyPricesForPlans($planId, $sc);
        }

        $this->data['message'] = $message ;
        $this->data['plan'] = $plan;
        $this->data['strWhereCat'] = $strWhereCat;
        $this->data['tblName'] = $tblName;
        $this->data['categoryId'] = $categoryId;
        $this->data['CURR_ABV'] = $CURR_ABV;
        $this->data['defaultCurrencyTitle'] = $defaultCurrencyTitle;
        $this->data['defaultCurrencyId'] = $defaultCurrencyId;
        $this->data['CURR_RT'] = $CURR_RT;
        $this->data['CURR_IDS'] = $CURR_IDS ;
        $this->data['sc'] = $sc;
        $this->data['currencyCount'] = $currencyCount;
        $this->data['planId'] = $planId;
        $this->data['plan'] = $plan;
        $this->data['count'] = $count;
        
        $this->data['view'] = 'admin/plansprices' ;
        $this->load->view('admin/layouts/default1' , $this->data);

    }

    public function resetpasswords(){
        $i = $this->input->post_get('i') ?: 0;
        $strHeader = $this->lang->line('BE_LBL_414');
        if($i == '1')
        {
            $strHeader = $this->lang->line('BE_LBL_711');
        }

        $this->data['i'] = $i ;
        $this->data['strHeader'] = $strHeader;
        $this->data['view'] = 'admin/reset_password' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function ajxresetpwds(){
        $purpose = $this->input->post_get('purpose');
		$subject='';
        @set_time_limit(0);
        if($purpose == 'rstPwds')
        {
            $i = $this->input->post_get('i') ? $this->input->post_get('i') : 0;
            $strTbl = 'tbl_gf_users';
            $strDCol = 'DisableUser';
            $strECol = 'UserEmail';
            $strIdCol = 'UserId';
            $strPCol = 'UserPassword';
            if($i == '1')
            {
                $strTbl = 'tbl_gf_suppliers';
                $strDCol = 'DisableSupplier';
                $strECol = 'SupplierEmail';
                $strIdCol = 'SupplierId';
                $strPCol = 'SupplierPassword';
            }
            $arr = getEmailDetails();
            $rsUsers = $this->Clients_model->fetch_suppliers_users($strIdCol , $strECol , $strTbl , $strDCol);
          
            $strQry = '';
            foreach($rsUsers as $row)
            {
                if($strQry != '')
                    $strQry .= ',';

                $pwd = generatePassword(10, 3);
                $encPwd = bFEncrypt($pwd);
                $this->Clients_model->update_suppliers_users($strTbl ,  $strPCol , $encPwd , $strIdCol , $row->UserId);
               

                $emailMsg = '<p>Dear '.stripslashes($row->FirstName).' '.stripslashes($row->LastName).',</p>
                <p>Your '.$arr[2].' account password is <b>'.$pwd.'</b></p>
                <p>If you have any questions, please feel free to contact us at <a href="mailto:'.$arr[4].'">'.$arr[4].'</a></p>
                <p>Thank you!</p>
                <p>'.$arr[2].'</p>';
                sendGeneralEmail($arr[4], $arr[0], $arr[1], 'Customer', 'Your '.$arr[2].' Password Information', $emailMsg, '');
                $strQry .= "('".$subject."', '".$emailMsg."','".$row->$strECol."')";
            }
            if($strQry != '')
            {
                $this->Clients_model->insert_newsletter($strQry);
               
            }
            $msg =  $this->lang->line('BE_GNRL_11');
        }	
        if($purpose == 'rstPins')
        {
            $arr = getEmailDetails();
            $rsUsers = $this->Clients_model->fetch_users_by_disable();
            
            foreach($rsUsers as $row )
            {
                $pin = generatePin(4);
                $encPin = encryptThe_String($pin);
                
                $update_data = array(
                    'PinCode' => $encPin 
                );
              
                $this->Clients_model->update_users($update_data , $row->UserId );
            
                if (isset($row->UserEmail) && $row->UserEmail != '')
                {
                    $emailMsg = '<p>Your '.$arr[2].' account pin code is <b>'.$pin.'</b></p>';
                    sendGeneralEmail($row->UserEmail, $arr[0], $arr[1], stripslashes($row->FirstName).' '.stripslashes($row->LastName), 'Your '.$arr[2].' Pin Code Information', 
                    $emailMsg, '');
                }
            }
            $msg =  $this->lang->line('BE_GNRL_11');
        }	
        if($purpose == 'rstPrics')
        {
            $i = $this->input->post('i') ?: 0;
            $qry = '';

            $this->Clients_model->del_users_packages_prices($i);
            
            $msg =  $this->lang->line('BE_GNRL_11');

        }	
        echo $msg;
    }

    public function resetpins(){
        $this->data['view'] = 'admin/reset_pins' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function newspopup(){
        $message = '';
        if ($this->input->post('txtNews'))
        {
            $news = $this->input->post('txtNews');
            $disable = $this->input->post('chkDisableNews') ? 1 : 0;
            $update_data = array(
                'News' => $news,
                'DisableNews' => $disable
            );
            $this->Clients_model->update_user_news_popup($update_data);
            
            $message = $this->lang->line('BE_GNRL_11');
        }
        $row = $this->Clients_model->fetch_user_news_popup();
      
        $this->data['news'] = stripslashes($row->News);
        $this->data['disable'] = $row->DisableNews;
        $this->data['message']= $message ;
        $this->data['view'] = 'admin/news_popup' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function emailintegrations(){
             
        $this->data['rsPMs'] = $this->Clients_model->fetch_email_integrations();
        $this->data['count'] = count($this->data['rsPMs']);
        
        $this->data['view'] = 'admin/email_integrations' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function emailintegration(){
        $id = $this->input->post_get('id') ?: 0;
        $apiName = '';
        $apiKey = '';
        $groupId = '';
        $row = $this->Clients_model->fetch_email_integrations_by_id($id);
      
        if (isset($row->Id) && $row->Id > 0)
        {
            $this->data['apiName'] = stripslashes($row->APIName);
            $this->data['apiKey'] = stripslashes($row->APIKey);
            $this->data['groupId'] = stripslashes($row->GroupId);
        }

        $this->data['id'] = $id ;

        $this->data['view'] = 'admin/email_integration' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function ajxemailint(){
        $purpose = $this->input->post('purpose');

        if($purpose == 'save')
        {
            $id = $this->input->post('id');
            if($id > 0)
            {
                $apiName = $this->input->post('apiName');
                $apiKey = $this->input->post('apiKey');
                $group = $this->input->post('group');
                $row = $this->Clients_model->fetch_email_integrations_by_api($apiName , $id);
               
                if (isset($row->Id) && $row->Id != '')
                    $msg = "'$apiName'".$this->lang->line('BE_GNRL_10')."~1"; 
                else
                {
                    $update_data = array(
                        'APIkey' => $apiKey,
                        'APIName' => $apiName ,
                        'GroupId' => $group
                    );
                    $this->Clients_model->update_email_integrations($update_data , $id);
                   
                    $msg =  $this->lang->line('BE_GNRL_11')."!~0";
                }
            }
            else
                $msg =  "Invalid request!~1";
        }
        else if($purpose == 'sync')
        {
            $id = $this->input->post('id');
            $planId = $this->input->post('planId');
            if($id > 0)
            {
                $row =  $this->Clients_model->fetch_email_integrations_by_id($id);
                if (isset($row->APIKey) && $row->APIKey != '')
                {
                    $apiKey = $row->APIKey;
                    $groupId = $row->GroupId;
                    $strWhere = '';
                    if($planId != '0')
                        $strWhere = " AND PricePlanId = '$planId'";
                    $arr = getEmailDetails();
                    $rsEmailIds = $this->Clients_model->fetch_users_by_userarchived($strWhere);
                   
                    $strXML = '';

                    if($id == '1')
                    {
                        // SEMS BLAST
                        foreach($rsEmailIds as $row )
                        {
                            $strXML .= "
                                <subscriber>
                                    <email>".$row->UserEmail."</email>
                                    <firstName>".$row->FirstName."</firstName>
                                    <lastName>".$row->LastName."</lastName>
                                </subscriber>";
                        }
                        if($strXML != '')
                        {
                            $strXML = '
                            <xml>
                            <apiKey>'.$apiKey.'</apiKey>
                            <groupID>'.$groupId.'</groupID>
                            <subscribers>'.$strXML.'</subscribers></xml>';
                            $objCurl = curl_init();
                            curl_setopt( $objCurl, CURLOPT_HEADER, false );
                            curl_setopt( $objCurl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
                            @curl_setopt( $objCurl, CURLOPT_FOLLOWLOCATION, true );
                            curl_setopt( $objCurl, CURLOPT_RETURNTRANSFER, true );
                            curl_setopt( $objCurl, CURLOPT_URL, 'http://cp.semsblast.com/api/v3.0/subscribers/addSubscribers/');
                            curl_setopt( $objCurl, CURLOPT_POST, true );
                            curl_setopt( $objCurl, CURLOPT_POSTFIELDS, $strXML);
                            $result = curl_exec($objCurl);
                            curl_close($objCurl);
                            $arrayData = createArrayFromXML($result);
                        }
                    }
                    $msg =  $this->lang->line('BE_GNRL_11')."!~0" ;
                }
            }
            else
                $msg =  "Invalid request!~1";
        }
        echo $msg;
    }

    public function resetclientprices(){
        $this->data['view'] = 'admin/reset_client_prices' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function unarchiveclients(){
        $message = $strWhere = $fName = $lName = $email = $uName = $txtlqry = '';
        $cldFrm = $this->input->post_get('cldFrm') ? $this->input->post_get('cldFrm') : 0;

        if($this->input->post_get('txtEmail') && $this->input->post_get('txtEmail') != '')
        {
            $email = trim($this->input->post_get('txtEmail'));
            if($email != '')
            {
                $strWhere .= " AND UserEmail LIKE '%$email%'";
                $txtlqry .= "&txtEmail=$email";
            }
        }
        if($this->input->post_get('txtFName') && $this->input->post_get('txtFName') != '')
        {
            $fName = trim($this->input->post_get('txtFName'));
            if($fName != '')
            {
                $strWhere .= " AND FirstName LIKE '%$fName%'";
                $txtlqry .= "&txtFName=$fName";
            }
        }
        if($this->input->post_get('txtLName') && $this->input->post_get('txtLName') != '')
        {
            $lName = trim($this->input->post_get('txtLName'));
            if($lName != '')
            {
                $strWhere .= " AND LastName LIKE '%$lName%'";
                $txtlqry .= "&txtLName=$lName";
            }
        }
        if($this->input->post_get('txtUName') && $this->input->post_get('txtUName') != '')
        {
            $uName = trim($this->input->post_get('txtUName'));
            if($uName != '')
            {
                $strWhere .= " AND UserName LIKE '%$uName%'";
                $txtlqry .= "&txtUName=$uName";
            }
        }
        if($this->input->post_get('btnSubmit') && $this->session->userdata('GSM_FSN_AdmId') && $cldFrm == '1')
        {
            $userIds = $this->input->post_get('chkUsers') ? $this->input->post_get('chkUsers') : 0;
            $selectedUsers = '0';
            if(is_array($userIds))
                $selectedUsers = implode(",", $userIds);
            if($selectedUsers == '0')
            {
                $message = 'Please select at least one client!';	
            }
            else
            {
                $update_data = array(
                    'UserArchived' =>  0
                );

                $where_in = array(
                    'UserId' => $selectedUsers
                );

                $this->Clients_model->update_users($update_data , '' , $where_in);
                $message = $this->lang->line('BE_GNRL_11');	
            }
        }
        $this->data['rsClients'] = $this->Clients_model->fetch_rsClients($strWhere);
		$this->data['count'] = count($this->data['rsClients']);

        $this->data['message'] = $message ;
        $this->data['fName'] = $fName;
        $this->data['lName'] = $lName;
        $this->data['uName'] = $uName;
        $this->data['email'] = $email;

        $this->data['view'] = 'admin/unarchive_clients' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    
    }

}
?>
