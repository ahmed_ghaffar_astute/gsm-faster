<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cms extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('cms_model');

	}
	public function brandlogos(){
		//var_dump('here');die;
		$message = '';
		$IS_DEMO = false;
		$del = $this->input->post_get('del') ? 1 : 0;
		if($del == '1' && !$IS_DEMO)
		{
			$id = $this->input->post_get('id') ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
			$this->cms_model->deleteLogos($id);
			$message = $this->lang->line('BE_GNRL_12');
		}
		$rsData = $this->cms_model->getAllLogos();
		$this->data['message'] = $message;
		$this->data['rsData'] = $rsData;
		$this->data['IS_DEMO'] = $IS_DEMO;
		$this->data['view'] = 'admin/manage_logos';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function addbrandlogo(){
		$THEME=$this->data['rsStngs']->Theme;
		$this->load->library('FileManager');
		$id = ($this->input->post_get('id')) ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
		$existingImage = '';
		$message = '';
		$IS_DEMO = false;
		//var_dump($_FILES);
		if ($this->input->post_get('existingImage'))
		{
			$filemanager = new FileManager();
			$fileExt = $filemanager->getFileExtension("txtImage");
			if($fileExt == '.png' || $fileExt == '.jpg' || $fileExt == '.jpeg' || $fileExt == '.gif' || $fileExt == '')
			{
				$file = '';
				$randNm = rand_string(10);
				if($id == 0)
				{
					if ($filemanager->getFileName('txtImage') != '')
						$file =	'brand-logos/'.$randNm.$filemanager->getFileExtension("txtImage");
					if($file != '')
					{
						$this->cms_model->insertBrandLogo($file);
					}
				}
				else
				{
					if ($filemanager->getFileName('txtImage') != '')
						$file =	'brand-logos/'.$randNm.$filemanager->getFileExtension("txtImage");

					if($file != '')
					{
						$this->cms_model->updateBrandLogo($file,$id);
					}
				}
				if ($filemanager->getFileName('txtImage') != '')
				{
					if($existingImage != '')
						@unlink(base_url().'uplds'.$THEME.'/brand-logos/'.$existingImage);
					$filemanager->uploadAs('txtImage', FCPATH.'uplds'.$THEME.'/'.$file);
				}
				$message = $this->lang->line('BE_GNRL_11');
			}
			else
			{
				$message = $this->lang->line('BE_LBL_572');
			}
		}
		if($id > 0)
		{
			$row = $this->cms_model->getBrandLogoById($id);
			if ($row->BrandImage && $row->BrandImage != '')
			{
				if($row->BrandImage != '')
					$existingImage = "uplds".$THEME."/".$row->BrandImage;
			}
		}

		$this->data['id'] = $id;
		$this->data['existingImage'] = $existingImage;
		$this->data['message'] = $message;
		$this->data['IS_DEMO'] = $IS_DEMO;
		$this->data['view'] = 'admin/add_logo_form';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function pages(){
		//var_dump('here');die;
		$THEME=$this->data['rsStngs']->Theme;
		$this->load->library('FileManager');
		$message = '';
		$IS_DEMO =false;
		$type = ($this->input->post_get('type')) ? check_input($this->input->post_get('type'), $this->db->conn_id) : 0;
		$rsCats = $this->cms_model->getAllPages($type);

		$this->data['type'] = $type;
		$this->data['message'] = $message;
		$this->data['rsCats'] = $rsCats;
		$this->data['IS_DEMO'] = $IS_DEMO;
		$this->data['view'] = 'admin/manage_pages';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function page(){
		//var_dump('here');die;
		$message = '';
		$THEME=$this->data['rsStngs']->Theme;
		$this->load->library('FileManager');
		$IS_DEMO =false;
		$id = ($this->input->post_get('id')) ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
		$type = ($this->input->post_get('type')) ? check_input($this->input->post_get('type'), $this->db->conn_id) : 0;
		$lnkTitle = $message = $pageTitle = $pageText = $metaTags = $message = $existingImage = $file = $metaKW = $fileName = $url = $htmlTitle = $seoName ='';
		$subMenuLnk = $disablePage = $headerLnk = $headerLnkCP = $footerLnk = $footerLnkCP = $actRtl = 0;
		$check = $this->input->post_get('txtLnkTitle')?'yes':'no';
		if ($check != 'no')
		{
			$lnkTitle = check_input($this->input->post_get('txtLnkTitle'), $this->db->conn_id);
			$row =  $this->cms_model->getPageByData($lnkTitle,$disablePage,$type,$id);
			$check_pageid = ($row != false)? $row->PageId:'';
			//var_dump($row);die;
			if ($check_pageid != '')
				$message = "Page Title <b>'" . $lnkTitle . "'</b> ".$this->lang->line('BE_GNRL_10');
			else
			{
				$filemanager = new FileManager();
				$fileExt = $filemanager->getFileExtension("txtImage");
				if($fileExt == '.png' || $fileExt == '.jpg' || $fileExt == '.jpeg' || $fileExt == '.gif' || $fileExt == '')
				{
					$pageTitle = check_input($this->input->post_get('txtPageTitle'), $this->db->conn_id);
					$pageText = check_input($this->input->post_get('txtPageText'), $this->db->conn_id, 1);
					$metaTags = check_input($this->input->post_get('txtMetaTags'), $this->db->conn_id);
					$seoName = check_input($this->input->post_get('txtSEOName'), $this->db->conn_id);
					$htmlTitle = check_input($this->input->post_get('txtHTMLTitle'), $this->db->conn_id);
					$metaKW = check_input($this->input->post_get('txtMetaKW'), $this->db->conn_id);
					$fileName = check_input($this->input->post_get('txtFileName'), $this->db->conn_id);
					$disablePage = ($this->input->post_get('chkDisable')) ? 1 : 0;
					$headerLnk = ($this->input->post_get('chkHeaderLnk')) ? 1 : 0;
					$footerLnk = ($this->input->post_get('chkFooterLnk')) ? 1 : 0;
					$subMenuLnk = ($this->input->post_get('chkSubMenuLnk')) ? 1 : 0;
					$headerLnkCP = ($this->input->post_get('chkHeaderLnkCP')) ? 1 : 0;
					$footerLnkCP = ($this->input->post_get('chkFooterLnkCP')) ? 1 : 0;
					$actRtl = ($this->input->post_get('chkActRtl')) ? 1 : 0;
					$existingImage = check_input($this->input->post_get('existingImage'), $this->db->conn_id);
					$url = check_input($this->input->post_get('txtURL'), $this->db->conn_id);

					if($id == 0)
					{
						$pageId = $this->cms_model->insertPage($lnkTitle,$pageTitle, $fileName, $metaTags, $pageText, $headerLnk, $footerLnk, $headerLnkCP, $footerLnkCP,$disablePage,$seoName,$metaKW,$htmlTitle,$type, $actRtl, $url,$subMenuLnk);
						if ($filemanager->getFileName('txtImage') != '')
							$file =	'pages/'.$pageId.$filemanager->getFileExtension("txtImage");
						$disablePage = $subMenuLnk = 0;
						$pageText = $metaTags = $lnkTitle = $fileName = $seoName = $htmlTitle = $metaKW = $url = $lnkTitle = $pageTitle = '';
					}
					else
					{
						if ($filemanager->getFileName('txtImage') != '')
							$file =	'pages/'.$id.$filemanager->getFileExtension("txtImage");
						$this->cms_model->updatePage($lnkTitle,$pageTitle, $fileName, $metaTags, $pageText, $headerLnk, $footerLnk, $headerLnkCP, $footerLnkCP,$disablePage,$seoName,$metaKW,$htmlTitle, $actRtl, $url,$subMenuLnk,$id);
						$pageId = $id;
					}
					if($file != '')
					{
						$this->cms_model->updatePageImgByID($file,$pageId);
					}
					if ($filemanager->getFileName('txtImage') != '')
					{
						if($existingImage != '')
							@unlink(FCPATH.'/uplds'.$THEME.'/'.$existingImage);
						$filemanager->uploadAs('txtImage', FCPATH.'/uplds'.$THEME.'/'.$file);
					}
					$message = $this->lang->line('BE_GNRL_11');
				}
				else
					$message = $this->lang->line('BE_LBL_572');
			}
		}
		if($id > 0)
		{
			$row = $this->cms_model->getPageImgByID($id);
			//var_dump($row);die;
			if ($row->LinkTitle && $row->LinkTitle != '')
			{
				$lnkTitle = stripslashes($row->LinkTitle);
				$pageTitle = stripslashes($row->PageTitle);
				$pageText = stripslashes($row->PageText);
				$disablePage = $row->DisablePage;
				$headerLnk = $row->HeaderLink;
				$footerLnk = $row->FooterLink;
				$headerLnkCP = $row->HeaderLinkCP;
				$footerLnkCP = $row->FooterLinkCP;
				$subMenuLnk = $row->SubMenuLink;
				$actRtl = $row->ActivateRetail;
				$fileName = $row->FileName;
				if($row->Image != '')
					$existingImage = '/uplds'.$THEME.'/'.$row->Image;
				$htmlTitle = stripslashes($row->HTMLTitle);
				$metaKW = stripslashes($row->MetaKW);
				$seoName = $row->SEOURLName;
				$metaTags = stripslashes($row->MetaTags);
				$type = $row->PageType;
				$url = $row->URL;
			}
		}

		$rsCats = $this->cms_model->getAllPages($type);

		$this->data['id'] = $id;
		$this->data['url'] = $url;
		$this->data['metaTags'] = $metaTags;
		$this->data['seoName'] = $seoName;
		$this->data['metaKW'] = $metaKW;
		$this->data['htmlTitle'] = $htmlTitle;
		$this->data['existingImage'] = $existingImage;
		$this->data['fileName'] = $fileName;
		$this->data['actRtl'] = $actRtl;
		$this->data['subMenuLnk'] = $subMenuLnk;
		$this->data['footerLnkCP'] = $footerLnkCP;
		$this->data['headerLnkCP'] = $headerLnkCP;
		$this->data['footerLnk'] = $footerLnk;
		$this->data['headerLnk'] = $headerLnk;
		$this->data['disablePage'] = $disablePage;
		$this->data['pageText'] = $pageText;
		$this->data['pageTitle'] = $pageTitle;
		$this->data['lnkTitle'] = $lnkTitle;
		$this->data['type'] = $type;
		$this->data['message'] = $message;
		$this->data['rsCats'] = $rsCats;
		$this->data['IS_DEMO'] = $IS_DEMO;
		$this->data['view'] = 'admin/add_page_form';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function socialmedialinks(){
		$message = '';
		$IS_DEMO = false;
		$type = $this->input->post_get('type') ? check_input($this->input->post_get('type'), $this->db->conn_id) : 0;
		$del = $this->input->post_get('del') ? 1 : 0;
		if($del == '1')
		{
			$id = $this->input->post_get('id') ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
			$this->cms_model->deleteSocailLinks($id);
			/*$objDBCD14->query("DELETE FROM tbl_gf_socialmedia WHERE Id = '$id'");*/
			$message = $this->lang->line('BE_GNRL_12');
		}

		$rsMLinks =  $this->cms_model->getSocailLinksBytype($type);
		$this->data['type'] = $type;
		$this->data['message'] = $message;
		$this->data['rsMLinks'] = $rsMLinks;
		$this->data['IS_DEMO'] = $IS_DEMO;
		$this->data['view'] = 'admin/manage_social_links';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function socialmedialink()
	{
		$THEME=$this->data['rsStngs']->Theme;
		$id = $this->input->post_get('id') ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
		$type = $this->input->post_get('type') ? check_input($this->input->post_get('type'), $this->db->conn_id) : 0;
		$this->data['id'] = $id;
		$title = '';
		$news = '&nbsp;';
		$url = '';
		$disable = 0;
		$existingImage = '';
		$message = '';
		$appId = '';
		if ($this->input->post_get('existingImage'))
		{
			$title = check_input($this->input->post_get('txtTitle'), $this->db->conn_id);
			$row = $this->cms_model->getSocialMediaByTitleAndType($title, $type, $id);

			if (isset($row->Id) && $row->Id != '')
			{
				$message = "'" .$title . "' ". $this->lang->line('BE_GNRL_10');
			}
			else
			{
				$this->load->library('FileManager');
				$filemanager = new FileManager();
				$fileExt = $filemanager->getFileExtension("txtImage");
				if($fileExt == '.png' || $fileExt == '.jpg' || $fileExt == '.jpeg' || $fileExt == '.gif' || $fileExt == '')
				{
					$file = '';
					$url = check_input($_REQUEST['txtURL'], $this->db->conn_id);
					$existingImage = check_input($_REQUEST['existingImage'], $this->db->conn_id);
					$disable = isset($_REQUEST['chkDisableRecord']) ? 1 : 0;
					$appId = check_input($_POST['txtAppId'], $this->db->conn_id);

					$dtTm = setDtTmWRTYourCountry();
					if($id == 0)
					{
						$mediaId = $this->cms_model->insertDataGetID("INSERT INTO tbl_gf_socialmedia (Title, Link, DisableRecord, MediaType, APPID) VALUES ('$title', '$url', '$disable', '$type', '$appId')");
						$this->data['title'] = '';
						$this->data['url'] = '';
						$this->data['appId'] = '';
						$this->data['disable'] = 0;

						if ($filemanager->getFileName('txtImage') != '')
							$file =	'socialmedia/'.$mediaId.$filemanager->getFileExtension("txtImage");
					}
					else
					{
						if ($filemanager->getFileName('txtImage') != '')
							$file =	'socialmedia/'.$id.$filemanager->getFileExtension("txtImage");
						$this->cms_model->insertData("UPDATE tbl_gf_socialmedia SET Title = '$title', Link = '$url', DisableRecord = '$disable', APPID = '$appId' WHERE Id = $id");
						$mediaId = $id;
					}
					if($file != '')
					{
						$this->cms_model->UpdateData("UPDATE tbl_gf_socialmedia SET Image = '$file' WHERE Id = $mediaId");
					}
					if ($filemanager->getFileName('txtImage') != '')
					{
						if($existingImage != '')
							@unlink(FCPATH . "$this->UPLOAD_FOLDER$THEME/$existingImage");

						$filemanager->uploadAs('txtImage', FCPATH . $this->UPLOAD_FOLDER . $THEME . "/" . $file);
					}
					$message = $this->lang->line("BE_GNRL_11");
				}
				else
					$message = $this->lang->line("BE_LBL_572");
			}
		}
		if($id > 0)
		{
			$row = $this->cms_model->getSocialMediaById($id); //$objDBCD14->queryUniqueObject('SELECT * FROM tbl_gf_socialmedia WHERE Id = '.$id);
			if ($row)
			{
				$this->data['title'] = stripslashes($row->Title);
				$this->data['appId'] = stripslashes($row->APPID);
				if($row->Image != '')
					$this->data['existingImage'] = base_url("") . $this->UPLOAD_FOLDER . $THEME . "/" .$row->Image;
				$this->data['url'] = $row->Link;
				$this->data['disable'] = $row->DisableRecord;
			}
		}
		$this->data['message'] = $message;
		$this->data['view'] = 'admin/add_sociallink_form';
		$this->load->view('admin/layouts/default1', $this->data);

	}
	public function reviews(){
		$message = '';
		$type = ($this->lang->line('type')) ? check_input($this->lang->line('type'), $this->db->conn_id) : 0;
		$del = ($this->lang->line('del')) ? 1 : 0;
		if($del == '1')
		{
			$id = ($this->lang->line('id')) ? check_input($this->lang->line('id'), $this->db->conn_id) : 0;
			$this->cms_model->deleteReview('DELETE FROM tbl_gf_reviews WHERE ReviewId = '.$id);
			$message = $this->lang->line('BE_GNRL_12');
		}
		$rsReviews = $this->cms_model->getData("SELECT ReviewId, CustomerName, ReviewDate, Review, Enabled FROM tbl_gf_reviews WHERE ReviewType = '$type' ORDER BY ReviewId DESC");
		$this->data['message'] = $message;
		$this->data['type'] = $type;
		$this->data['rsReviews'] = $rsReviews;
		$this->data['IS_DEMO'] = 'false';
		$this->data['view'] = 'admin/reviews';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function review(){
		$id = ($this->input->post_get('id')) ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
		$type = ($this->input->post_get('type')) ? check_input($this->input->post_get('type'), $this->db->conn_id) : 0;
		$name = '';
		$enabled = 0;
		$review = '';
		$price = '';
		$message = '';
		$rating = 5;
		$dt = '';
		if (($this->input->post_get('txtName')))
		{
			$name = addslashes(check_input($this->input->post_get('txtName'), $this->db->conn_id));
			$review = addslashes(check_input($this->input->post_get('txtReview'), $this->db->conn_id));
			$enabled = ($this->input->post_get('chkEnable')) ? 1 : 0;
			$rating = check_input($this->input->post_get('rdRating'), $this->db->conn_id);
			if($id == 0)
			{
				$dt = setDtTmWRTYourCountry();
				$this->cms_model->insertData("INSERT INTO tbl_gf_reviews (CustomerName, Review, ReviewDate, Rating, Enabled, ReviewType) VALUES ('$name', '$review', '$dt', '$rating', '$enabled', '$type')");
				$name = '';
				$enabled = 0;
				$review = '';
				$rating = '';
			}
			else
			{
				$this->cms_model->UpdateData("UPDATE tbl_gf_reviews SET CustomerName = '$name', Review = '$review', Rating = $rating, Enabled = $enabled WHERE ReviewId = $id");
			}
			$message = $this->lang->line('BE_GNRL_11');
		}
		if($id > 0)
		{
			$row = $this->cms_model->gatRowData('SELECT * FROM tbl_gf_reviews WHERE ReviewId = '.$id);
			if (isset($row->ReviewId) && $row->ReviewId != '')
			{
				$name = stripslashes($row->CustomerName);
				$review = stripslashes($row->Review);
				$rating = stripslashes($row->Rating);
				$enabled = $row->Enabled;
				$type = $row->ReviewType;
			}
		}

		$this->data['id'] = $id;
		$this->data['message'] = $message;
		$this->data['dt'] = $dt;
		$this->data['name'] = $name;
		$this->data['review'] = $review;
		$this->data['rating'] = $rating;
		$this->data['enabled'] = $enabled;
		$this->data['type'] = $type;
		$this->data['IS_DEMO'] = false;
		$this->data['view'] = 'admin/review';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function logos(){
		$rsLogos = $this->cms_model->getData('SELECT LogoId, LogoTitle, DisableLogo, ForAdminPanel FROM tbl_gf_logo ORDER BY LogoTitle');
		$this->data['rsLogos'] = $rsLogos;
		$this->data['IS_DEMO'] = false;
		$this->data['view'] = 'admin/logos';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function logo(){
		$id = ($this->input->post_get('id')) ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
		$logoTitle = '';
		$logoType = 0;
		$disableLogo = 0;
		$existingLogo = '';
		$message = '';
		$dtTm = '';
		$THEME=$this->data['rsStngs']->Theme;
		$this->load->library('FileManager');
		if (($this->input->post_get('existingLogo')))
		{
			$file = '';
			$logoTitle = check_input($this->input->post_get('txtTitle'), $this->db->conn_id);
			$logoType = check_input($this->input->post_get('rdLogoType'), $this->db->conn_id);
			$existingLogo = check_input($this->input->post_get('existingLogo'), $this->db->conn_id);
			$disableLogo = ($this->input->post_get('chkDisableLogo')) ? 1 : 0;

			$filemanager = new FileManager();
			$dtTm = setDtTmWRTYourCountry();
			if($id > 0)
			{
				$fileExt = $filemanager->getFileExtension("txtLogo");
				if($fileExt == '.png' || $fileExt == '.PNG' || $fileExt == '.JPG' || $fileExt == '.jpg' || $fileExt == '.jpeg' || $fileExt == '.gif')
				{
					if ($filemanager->getFileName('txtLogo') != '')
						$file =	'logos/'.$id.$filemanager->getFileExtension("txtLogo");
					$this->cms_model->updateData("UPDATE tbl_gf_logo SET LogoTitle = '$logoTitle', ForAdminPanel = '$logoType', DisableLogo = '$disableLogo' WHERE LogoId = '$id'");
					$logoId = $id;
					if($file != '')
					{
						$this->cms_model->updateData("UPDATE tbl_gf_logo SET LogoPath = '$file' WHERE LogoId = '$logoId'");
					}
					if ($filemanager->getFileName('txtLogo') != '')
					{
						if($existingLogo != '')
							@unlink("/uplods".$THEME.'/'.$existingLogo);
						$filemanager->uploadAs('txtLogo', FCPATH."/uplds".$THEME.'/'.$file);
					}
					$message = $this->lang->line('BE_GNRL_11');
				}
				else
				{
					$message = $this->lang->line('BE_LBL_572');
				}
			}
		}
		if($id > 0)
		{
			$row = $this->cms_model->gatRowData("SELECT * FROM tbl_gf_logo WHERE LogoId = '$id'");
			if (isset($row->LogoTitle) && $row->LogoTitle != '')
			{
				$logoTitle = stripslashes($row->LogoTitle);
				$existingLogo = "/uplds".$THEME."/".$row->LogoPath;
				$logoType = $row->ForAdminPanel;
				$disableLogo = $row->DisableLogo;
				if($logoType == '1' && $existingLogo != '')
				{
					$this->session->set_userdata('AdminLogo',$existingLogo);
				}
			}
		}

		$row = $this->cms_model->gatRowData('SELECT LogoPath FROM tbl_gf_logo WHERE DisableLogo = 0 AND ForAdminPanel = 1 ORDER BY LogoId DESC');
		if (($row->LogoPath) && $row->LogoPath != '')
			$this->session->set_userdata('AdminLogo', '/uplds'.$THEME.'/'.$row->LogoPath);

		$this->data['id'] = $id;
		$this->data['message'] = $message;
		$this->data['logoTitle'] = $logoTitle;
		$this->data['dtTm'] = $dtTm;
		$this->data['logoType'] = $logoType;
		$this->data['disableLogo'] = $disableLogo;
		$this->data['existingLogo'] = $existingLogo;
		$this->data['logoTitle'] = $logoTitle;
		$this->data['IS_DEMO'] = false;
		$this->data['view'] = 'admin/logo';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function banners(){
		$message = '';
		$type = $this->input->post_get('type') ? check_input($this->input->post_get('type'), $this->db->conn_id) : 0;
		$del = ($this->input->post_get('del')) ? 1 : 0;
		if($del == '1' && $this->session->userdata('GSM_FSN_AdmId'))
		{
			$id = ($this->input->post_get('id')) ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
			$this->db->query("DELETE FROM tbl_gf_banner WHERE BannerId = '$id'");
			$message = $this->lang->line('BE_GNRL_12');
		}
		$rsBanners = $this->cms_model->getData("SELECT BannerId, BannerTitle, BannerTitle_Local, DisableBanner, OrderBy FROM tbl_gf_banner WHERE BannerType = '$type' ORDER BY OrderBy");

		$this->data['type'] = $type;
		$this->data['message'] = $message;
		$this->data['rsBanners'] = $rsBanners;
		$this->data['IS_DEMO'] = false;
		$this->data['view'] = 'admin/banners';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function banner(){
		$THEME=$this->data['rsStngs']->Theme;
		$this->load->library('FileManager');
		$id = ($this->input->post_get('id')) ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
		$add_form = ($this->input->post_get('id')) ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
		$type = ($this->input->post_get('type')) ? check_input($this->input->post_get('type'), $this->db->conn_id) : 0;
		$detail = '';
		$title = '';
		$url = '';
		$disableBanner = 0;
		$existingBanner = '';
		$message = '';
//		var_dump($this->input->post_get('existingBanner'));die;
		if ($this->input->post_get('existingBanner') && $this->session->userdata('GSM_FSN_AdmId'))
		{
			$file = '';
			$title = check_input($this->input->post_get('txtTitle'), $this->db->conn_id);
			$url = check_input($this->input->post_get('txtURL'), $this->db->conn_id);
			$existingBanner = check_input($this->input->post_get('existingBanner'), $this->db->conn_id);
			$disableBanner = ($this->input->post_get('chkDisableBanner')) ? 1 : 0;
			$detail = check_input($this->input->post_get('txtDetail'), $this->db->conn_id, 1);

			$filemanager = new FileManager();
			$fileExt = $filemanager->getFileExtension("txtBanner");
			if($fileExt == '.JPG' || $fileExt == '.jpg' || $fileExt == '.jpeg' || $fileExt == '.png' || $fileExt == '.gif' || $fileExt == '')
			{
				if($id == 0)
				{

					$bannerId = $this->cms_model->insertDataGetID("INSERT INTO tbl_gf_banner (BannerTitle, DisableBanner, URL, Detail, BannerType) VALUES ('$title', '$disableBanner', '$url', '$detail', '$type')");
					$title = '';

					$disableBanner = 0;
					if ($filemanager->getFileName('txtBanner') != '')
						$file =	'banners/'.$bannerId.$filemanager->getFileExtension("txtBanner");
				}
				else
				{

					if ($filemanager->getFileName('txtBanner') != 'add')
						$file =	'banners/'.$id.$filemanager->getFileExtension("txtBanner");
					$this->cms_model->UpdateData("UPDATE tbl_gf_banner SET BannerTitle = '$title', Detail = '$detail', URL = '$url', DisableBanner = $disableBanner WHERE BannerId = '$id'");
					$bannerId = $id;
				}
				if($file != '')
					$this->cms_model->UpdateData("UPDATE tbl_gf_banner SET BannerPath = '$file' WHERE BannerId = $bannerId");
				if ($filemanager->getFileName('txtBanner') != '')
				{
					if($existingBanner != 'add')
						@unlink("/uplds".$THEME."/".$existingBanner);
					$filemanager->uploadAs('txtBanner', FCPATH."/uplds".$THEME."/".$file);
				}
				$message = $this->lang->line('BE_GNRL_11');
			}
			else
				$message = $this->lang->line('BE_LBL_572');
		}
		if($id > 0)
		{
			$row = $this->cms_model->gatRowData('SELECT * FROM tbl_gf_banner WHERE BannerId = '.$id);
			if (isset($row->BannerTitle) && $row->BannerTitle != '')
			{
				$title = stripslashes($row->BannerTitle);
				$detail = stripslashes($row->Detail);
				$existingBanner = "/uplds".$THEME."/".$row->BannerPath;
				$disableBanner = $row->DisableBanner;
				$url = $row->URL;
				$type = $row->BannerType;
			}
		}

		$this->data['type'] = $type;
		$this->data['message'] = $message;
		$this->data['existingBanner'] = $existingBanner;
		$this->data['disableBanner'] = $disableBanner;
		$this->data['url'] = $url;
		$this->data['detail'] = $detail;
		$this->data['title'] = $title;
		$this->data['id'] = $id;
		$this->data['IS_DEMO'] = false;

			$this->data['view'] = 'admin/banner';
			$this->load->view('admin/layouts/default1', $this->data);


	}

}
