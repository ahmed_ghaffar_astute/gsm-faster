<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Smsgateway extends MY_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('sms_gateway_model');

	}
	public function smsgateway(){
		$id = $this->input->post_get('id') ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
		$gateway = '';
		$userName = '';
		$disable = 0;
		$message = '';
		$password = '';
		$row = $this->sms_gateway_model->getSmsGatwayById($id);
		if (isset($row->SMSGatewayId) && $row->SMSGatewayId > 0)
		{
			$gateway = stripslashes($row->SMSGateway);
			$userName = stripslashes($row->Username);
			$password = stripslashes($row->Password);
			$disable = $row->DisableSMSGateway;
		}
		$this->data['id'] = $id;
		$this->data['message'] = $message;
		$this->data['password'] = $password;
		$this->data['disable'] = $disable;
		$this->data['userName'] = $userName;
		$this->data['gateway'] = $gateway;
		$this->data['IS_DEMO'] = false;
		$this->data['view'] = 'admin/smsgateway_form';
		$this->load->view('admin/layouts/default1' , $this->data);
	}
	public function smsgateways(){
		$message = '';
		$rsPMs = $this->sms_gateway_model->getSmsGatway();
		$this->data['message'] = $message;
		$this->data['rsPMs'] = $rsPMs;
		$this->data['IS_DEMO'] = false;
		$this->data['view'] = 'admin/manage_smsgateway';
		$this->load->view('admin/layouts/default1' , $this->data);
	}
	public function ajxsmsgateway(){
		$id = check_input($this->input->post_get('id'), $this->db->conn_id);
		if($id > 0)
		{
			$uName = check_input($this->input->post_get('uName'), $this->db->conn_id);
			$pwd = check_input($this->input->post_get('pwd'), $this->db->conn_id);
			$gateway = check_input($this->input->post_get('gateway'), $this->db->conn_id);
			$disable = check_input($this->input->post_get('disable'), $this->db->conn_id);
			$row = $this->sms_gateway_model->getSmsGatwayId($gateway,$id);
			if (isset($row->SMSGatewayId) && $row->SMSGatewayId != '')
				$msg = $gateway.' '.$this->lang->line('BE_GNRL_10').'~1';
			else
			{
				$update_data['Password'] = $pwd;
				$update_data['SMSGateway'] = $gateway;
				$update_data['Username'] = $uName;
				$update_data['DisableSMSGateway'] = $disable;
				$this->sms_gateway_model->updateSmsGateway($update_data,$id);
				/*$objDBCD14->execute("UPDATE tbl_gf_sms_gateways SET Password = '$pwd', SMSGateway = '$gateway', Username = '$uName', DisableSMSGateway
				 = '$disable' WHERE SMSGatewayId = '$id'");*/
				if($disable == '0')
				{
					$update_data['DisableSMSGateway'] = $disable;
					$this->sms_gateway_model->updateSmsGateway($update_data,$id);
					/*$objDBCD14->execute("UPDATE tbl_gf_sms_gateways SET DisableSMSGateway = '1' WHERE SMSGatewayId <> '$id'");*/
				}
				$msg =  $this->lang->line('BE_GNRL_11').'!~0';
			}
		}
		else
			$msg =  "Invalid request!~1";
		echo $msg;
	}

}
