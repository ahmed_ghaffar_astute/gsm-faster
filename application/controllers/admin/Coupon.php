<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Coupon extends MY_Controller {

    public function __construct(){
        parent::__construct();

        $this->load->helper('image'); 
        $this->load->model('common_model');
        $this->load->model('Coupon_model');
        $this->load->model('Product_model');

    }

    function index(){
        $data = array();
        $data['page_title'] = 'Coupons';
        $data['current_page'] = 'Coupons';

        $row=$this->Coupon_model->coupon_list();;

        $config = array();
        $config["base_url"] = base_url() . 'admin/coupon';
        $config["total_rows"] = count($row);
        $config["per_page"] = 12;

        $config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;

        $config['enable_query_strings'] = TRUE;
        $config['page_query_string'] = FALSE;

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
         
        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
         
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
         
        $config['next_link'] = '';
        $config['next_tag_open'] = '<span class="nextlink">';
        $config['next_tag_close'] = '</span>';

        $config['prev_link'] = '';
        $config['prev_tag_open'] = '<span class="prevlink">';
        $config['prev_tag_close'] = '</span>';

        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';

        $config['num_tag_open'] = '<li style="margin:3px">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        $page=($page-1) * $config["per_page"];
        
        if($this->input->post('search_value')!=''){
            $keyword=addslashes(trim($this->input->post('search_value')));

            $data['coupon_list'] = $this->Coupon_model->coupon_list('id', 'DESC', '', '', $keyword);
        }
        else{
            $data["links"] = $this->pagination->create_links();
            $data['coupon_list'] = $this->Coupon_model->coupon_list('id', 'DESC', $config["per_page"], $page);
        }

        $this->load->view('admin/layouts/default1' , 'admin/page/coupon', $data); // :blush:
    } 

    function addForm()
    {
        $this->form_validation->set_rules('coupon_code', 'Enter Coupon Code', 'trim|required');

        if($this->form_validation->run() == FALSE)
        {
            $messge = array('message' => $this->lang->line('input_required'),'class' => 'alert alert-danger');
                $this->session->set_flashdata('response_msg', $messge);
            redirect(base_url() . 'coupon/add', 'refresh');
        }
        else
        {
            $config['upload_path'] =  'assets/images/coupons/';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';

            $image = date('dmYhis').'_'.rand(0,99999).".".pathinfo($_FILES['file_name']['name'], PATHINFO_EXTENSION);

            $config['file_name'] = $image;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file_name')) {
                $messge = array('message' => $this->upload->display_errors(),'class' => 'alert alert-danger');
                $this->session->set_flashdata('response_msg', $messge);

                redirect(base_url() . 'admin/coupon/add', 'refresh');
            } 
            else
            {  
                $upload_data = $this->upload->data();
            }

            $this->load->helper("date");

            $data = array(
                'coupon_desc'  => $this->input->post('coupon_desc'),
                'coupon_code'  => $this->input->post('coupon_code'),
                'coupon_image'  => $image,
                'coupon_per'  => $this->input->post('coupon_per'),
                'coupon_amt'  => $this->input->post('coupon_amt'),
                'max_amt_status'  => $this->input->post('max_amt_status'),
                'coupon_max_amt'  => $this->input->post('coupon_max_amt'),
                'cart_status'  => $this->input->post('cart_status'),
                'coupon_cart_min'  => $this->input->post('coupon_cart_min'),
                'coupon_limit_use'  => $this->input->post('coupon_limit_use'),
                'created_at'  =>  strtotime(date('d-m-Y h:i:s A'))
            );

            $data = $this->security->xss_clean($data);

            if($this->common_model->insert($data, 'tbl_coupon')){
                $messge = array('message' => $this->lang->line('add_msg'),'class' => 'alert alert-success');
                $this->session->set_flashdata('response_msg', $messge);

            }
            else{
                $messge = array('message' => $this->lang->line('add_error'),'class' => 'alert alert-danger');
                $this->session->set_flashdata('response_msg', $messge);
            }
            
            redirect(base_url() . 'admin/coupon/add', 'refresh');   
        }
    }

    public function coupon_form()
    {
        $data = array();

        $id =  $this->uri->segment(4);

        $data['page_title'] = 'Coupons';
        if($id==''){
            $data['current_page'] = 'Add Coupon';
        }
        else{
            $data['coupon'] = $this->Coupon_model->single_coupon($id);

            $data['current_page'] = 'Edit Coupon';
        }
        $this->load->view('admin/layouts/default1' , 'admin/page/coupon_form', $data); // :blush:
    }



    //-- update users info
    public function editForm($id)
    {
        $data = $this->Coupon_model->single_coupon($id);

        if($_FILES['file_name']['error']!=4){
            
            unlink('assets/images/coupons/'.$data[0]->banner_image);
            // unlink('assets/images/coupons/thumbs'.$data[0]->coupon_image);

            $config['upload_path'] =  'assets/images/coupons/';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';

            $image = date('dmYhis').'_'.rand(0,99999).".".pathinfo($_FILES['file_name']['name'], PATHINFO_EXTENSION);

            $config['file_name'] = $image;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file_name')) {
                $messge = array('message' => $this->upload->display_errors(),'class' => 'alert alert-danger');
                $this->session->set_flashdata('response_msg', $messge);

                redirect(base_url() . 'admin/coupon/edit/'.$id, 'refresh');
            }

        }
        else{
            $image=$data[0]->coupon_image;
        }

        $this->load->helper("date");

        $data = array(
            'coupon_desc'  => $this->input->post('coupon_desc'),
            'coupon_code'  => $this->input->post('coupon_code'),
            'coupon_image'  => $image,
            'coupon_per'  => $this->input->post('coupon_per'),
            'coupon_amt'  => $this->input->post('coupon_amt'),
            'max_amt_status'  => $this->input->post('max_amt_status'),
            'coupon_max_amt'  => $this->input->post('coupon_max_amt'),
            'cart_status'  => $this->input->post('cart_status'),
            'coupon_cart_min'  => $this->input->post('coupon_cart_min'),
            'coupon_limit_use'  => $this->input->post('coupon_limit_use')
        );

        $data = $this->security->xss_clean($data);

        if($this->common_model->update($data, $id,'tbl_coupon')){
            $messge = array('message' => $this->lang->line('update_msg'),'class' => 'alert alert-success');
            $this->session->set_flashdata('response_msg', $messge);

        }
        else{
            $messge = array('message' => $this->lang->line('update_error'),'class' => 'alert alert-danger');
            $this->session->set_flashdata('response_msg', $messge);
        }

        redirect(base_url() . 'admin/coupon/edit/'.$id, 'refresh');
        
    }

    
    //-- active user
    public function active($id) 
    {
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'tbl_coupon');
        echo 'Enable successfully...';
    }

    //-- deactive user
    public function deactive($id) 
    {
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'tbl_coupon');
        echo 'Disable successfully...';
    }

    //-- delete coupon
    public function delete($id)
    {
        echo $this->Coupon_model->delete($id);
    }
    


}
