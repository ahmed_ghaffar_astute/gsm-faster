<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends MY_Controller {

    private $app_logo;

    private $app_name;

    private $app_email;

    private $contact_email;

    private $order_email;

    public function __construct(){
        parent::__construct();
        check_login_user();
        $this->load->helper('image'); 
        $this->load->model('common_model');
        $this->load->model('Order_model');
        $this->load->model('Product_model');
        $this->load->model('Api_model','api_model');

        $this->load->helper("date");

        /*
        SmtpSettings();

        $smtp_setting = $this->api_model->smtp_settings();

        $app_setting = $this->api_model->app_details();

        $this->app_name=$app_setting->app_name;
        $this->app_email=$smtp_setting->smtp_email;
        $this->contact_email=$app_setting->app_email;
        $this->order_email=$app_setting->app_order_email;
		*/
    }

    public function get_status_title($id){
        return $this->common_model->selectByidParam($id,'tbl_status_title','title');
    }

    function index(){

        $this->data['page_title'] = 'Orders';
        $this->data['current_page'] = 'Orders';
        $this->data['order_list'] = $this->Order_model->order_list();
        $this->data['view'] = 'admin/page/orders';
        $this->load->view('admin/layouts/default1', $this->data); // :blush:
    } 

    public function order_summary(){
        $data = array();

        $order_no =  $this->uri->segment(3);

        if(!empty($this->Order_model->get_order($order_no))){
            $data['page_title'] = 'Orders';
            $data['current_page'] = 'Order Summary';

            $data['order_data'] = $this->Order_model->get_order($order_no);

            $data_update=array('is_seen' => 1);

            $this->common_model->updateByids($data_update, array('order_unique_id' => $order_no),'tbl_order_details');

            $this->load->view('admin/layouts/default1', 'admin/page/view_order', $data); // :blush:
        }
        else{
            redirect('admin/orders', 'refresh');
        }
    }

    public function print_order(){
        $data = array();

        $order_no =  $this->uri->segment(4);

        $data['page_title'] = 'Orders';
        $data['current_page'] = 'Order Summary';
        $data['order_data'] = $this->Order_model->get_order($order_no);
        $this->load->view('admin/page/print_order', $data); // :blush:

        /*
        $html = $this->output->get_output();
                // Load pdf library
        $this->load->library('pdf');
        $this->pdf->loadHtml($html);
        $this->pdf->setPaper('A4', 'portrait');
        $this->pdf->render();
        // Output the generated PDF (1 = download and 0 = preview)
        $this->pdf->stream("html_contents.pdf", array("Attachment"=> 0));

        */

    }

    public function download_invoice(){

        $data = array();

        $order_no =  $this->uri->segment(2);
        
        $data['page_title'] = 'Orders';
        $data['current_page'] = 'Order Summary';
        $data['order_data'] = $this->Order_model->get_order($order_no);
        $this->load->view('download_invoice', $data); // :blush:
        
        $html = $this->output->get_output();
        
        // Load pdf library
        $this->load->library('pdf');
        $this->pdf->loadHtml($html);
        $this->pdf->setPaper('A4', 'portrait');
        $this->pdf->render();

        $file_name="Invoice-".$order_no.".pdf";

        $this->pdf->stream($file_name, array("Attachment"=> 1));

    }

    public function order_status_form($order_id,$product_id=0) 
    {
        $data['status_titles'] = $this->Order_model->get_titles(true);
        $data['product_id'] = $product_id;
        $data['order_data'] = $this->Order_model->get_product_status($order_id,$product_id);
        $data['delivery_date'] = $this->common_model->selectByidParam($order_id,'tbl_order_details','delivery_date');
        $this->load->view('admin/page/order_status_update', $data); // :blush:
    }

    public function update_product_status() 
    {
        
        $status=$this->input->post('order_status');

        if($status=='other_status'){
            $data_status = array(
                'title' => $this->input->post('order_status_title')
            );

            $data_status = $this->security->xss_clean($data_status);

            $status=$this->common_model->insert($data_status, 'tbl_status_title');
        }

        $status_desc='';

        $products_arr=array();

        $where=array('order_id' => $this->input->post('order_id'));

        $row_trn=$this->common_model->selectByids($where, 'tbl_transaction');

        $this->load->helper("date");

        if($this->input->post('product_id')==0){

            // for full order

            $data_arr = array(
                'order_id' => $this->input->post('order_id'),
                'user_id' => $this->input->post('user_id'),
                'product_id' => '0',
                'status_title' => $status,
                'status_desc' => trim($this->input->post('status_desc')),
                'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
            );

            $data_usr = $this->security->xss_clean($data_arr);

            $this->common_model->insert($data_usr, 'tbl_order_status');

            $data_arr=$this->common_model->selectByids(array('order_id' => $this->input->post('order_id'), 'pro_order_status <>' => 5),'tbl_order_items');

            $refund_amt=$refund_per=$new_payable_amt=$total_refund_amt=$total_refund_per=0;

            foreach ($data_arr as $key => $value) {

                $data = array(
                    'order_id' => $value->order_id,
                    'user_id' => $value->user_id,
                    'product_id' => $value->product_id,
                    'status_title' => $status,
                    'status_desc' => trim($this->input->post('status_desc')),
                    'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
                );

                $data = $this->security->xss_clean($data);

                $this->common_model->insert($data, 'tbl_order_status');

                $data_pro = array(
                    'pro_order_status' => $status
                ); 

                $data_pro = $this->security->xss_clean($data_pro);

                $this->common_model->updateByids($data_pro, array('order_id' => $this->input->post('order_id'), 'product_id' => $value->product_id, 'user_id' => $value->user_id),'tbl_order_items');

                $img_file=$this->_create_thumbnail('assets/images/products/',$this->common_model->selectByidsParam(array('id' => $value->product_id),'tbl_product','product_slug'),$this->common_model->selectByidsParam(array('id' => $value->product_id),'tbl_product','featured_image'),100,100);

                // $p_items['product_img']='http://apptific.com/testing/ecommerce_app/images/analog-watch-of-lois-caron_360x360.png';

                $p_items['product_title']=$this->common_model->selectByidsParam(array('id' => $value->product_id),'tbl_product','product_title');
                $p_items['product_img']=base_url().$img_file;
                $p_items['product_qty']=$value->product_qty;
                $p_items['product_price']=$value->product_price;
                $p_items['product_size']=$value->product_size;

                array_push($products_arr, $p_items);

                if($status=='4'){
                    $this->Product_model->updateTotalSale($value->product_id);
                }

                if($status==5){

                    if($row_trn->gateway!='COD' || $row_trn->gateway!='cod'){

                        $row_ord=$this->common_model->selectByid($this->input->post('order_id'), 'tbl_order_details');

                        $where=array('order_id' => $this->input->post('order_id'),'product_id' => $value->product_id);

                        $row_pro=$this->common_model->selectByids($where, 'tbl_order_items');

                        foreach ($row_pro as $key_pro => $value_pro) {

                            $actual_pay_amt=($row_ord->payable_amt-$row_ord->delivery_charge);

                            $product_per=$new_payable_amt=0;

                            if($row_ord->coupon_id!=0){

                                $product_per=round(($value_pro->total_price/$row_ord->total_amt)*100);  //44
                                $refund_per=round(($product_per/100)*$row_ord->discount_amt); //22
                                $refund_amt=$value_pro->total_price-$refund_per; //38 
                                $new_payable_amt=$row_ord->payable_amt-$refund_amt;

                            }
                            else{
                                $refund_amt=$value_pro->total_price;
                                $new_payable_amt=($row_ord->payable_amt-$refund_amt);
                            }

                            $data_arr = array(
                                'bank_id' => '0',
                                'user_id' => $this->input->post('user_id'),
                                'order_id' => $this->input->post('order_id'),
                                'order_unique_id' => $row_ord->order_unique_id,
                                'product_id' => $value->product_id,
                                'product_title' => $value_pro->product_title,
                                'product_amt' => $value_pro->total_price,
                                'refund_pay_amt' => $refund_amt,
                                'gateway' => $row_trn->gateway,
                                'refund_reason' => trim($this->input->post('status_desc')),
                                'last_updated' => strtotime(date('d-m-Y h:i:s A',now())),
                                'request_status' => '-1',
                                'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
                            );

                            $data_update = $this->security->xss_clean($data_arr);

                            $this->common_model->insert($data_update, 'tbl_refund');

                        }
                    }

                }

            }

            $data_ord = array(
                'order_status' => $status
            ); 

            $data_ord = $this->security->xss_clean($data_ord);
            $this->common_model->update($data_ord, $this->input->post('order_id'),'tbl_order_details');    

            $status_desc=trim($this->input->post('status_desc'));

            if($status==5){

                $where=array('order_id' => $this->input->post('order_id'), 'pro_order_status <> ' => 5);

                if(count($this->common_model->selectByids($where, 'tbl_order_items')) == 1){

                    $data_update = array(
                        'order_status' => '5',
                        'new_payable_amt'  =>  '0',
                        'refund_amt'  =>  $refund_amt,
                        'refund_per'  =>  $refund_per
                    );

                    $data = array(
                        'order_id' => $this->input->post('order_id'),
                        'user_id' => $this->input->post('user_id'),
                        'product_id' => '0',
                        'status_title' => '5',
                        'status_desc' => trim($this->input->post('status_desc')),
                        'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
                    );

                    $data = $this->security->xss_clean($data);

                    $this->common_model->insert($data, 'tbl_order_status');

                }
                else{
                    $data_update = array(
                        'new_payable_amt'  =>  $new_payable_amt,
                        'refund_amt'  =>  $refund_amt,
                        'refund_per'  =>  $refund_per
                    );
                }

                $this->common_model->update($data_update, $this->input->post('order_id'),'tbl_order_details');
            }
            

        }
        else{

            if($status==5){

                if($row_trn->gateway!='COD' || $row_trn->gateway!='cod'){

                    $row_ord=$this->common_model->selectByid($this->input->post('order_id'), 'tbl_order_details');

                    $where=array('order_id' => $this->input->post('order_id'),'product_id' => $this->input->post('product_id'));

                    $row_pro=$this->common_model->selectByids($where, 'tbl_order_items')[0];

                    $this->load->helper("date");

                    $actual_pay_amt=($row_ord->payable_amt-$row_ord->delivery_charge);

                    $product_per=$refund_amt=$new_payable_amt=0;

                    if($row_ord->coupon_id!=0){

                        $product_per=round(($row_pro->total_price/$row_ord->total_amt)*100);  //44
                        $refund_amt=round(($product_per/100)*$row_ord->discount_amt); //22
                        $refund_amt=$row_pro->total_price-$refund_amt; //38 
                        $new_payable_amt=$row_ord->payable_amt-$refund_amt;


                    }
                    else{
                        $refund_amt=$row_pro->total_price;
                        $new_payable_amt=($row_ord->payable_amt-$refund_amt);
                    }

                    $data_arr = array(
                        'bank_id' => '0',
                        'user_id' => $this->input->post('user_id'),
                        'order_id' => $this->input->post('order_id'),
                        'order_unique_id' => $row_ord->order_unique_id,
                        'product_id' => $this->input->post('product_id'),
                        'product_title' => $row_pro->product_title,
                        'product_amt' => $row_pro->total_price,
                        'refund_pay_amt' => $refund_amt,
                        'gateway' => $row_trn->gateway,
                        'refund_reason' => trim($this->input->post('status_desc')),
                        'last_updated' => strtotime(date('d-m-Y h:i:s A',now())),
                        'request_status' => '-1',
                        'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
                    );

                    $data_update = $this->security->xss_clean($data_arr);
                    $this->common_model->insert($data_update, 'tbl_refund');
                }

            }

            $data = array(
                'order_id' => $this->input->post('order_id'),
                'user_id' => $this->input->post('user_id'),
                'product_id' => $this->input->post('product_id'),
                'status_title' => $status,
                'status_desc' => trim($this->input->post('status_desc')),
                'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
            );

            $data = $this->security->xss_clean($data);

            $this->common_model->insert($data, 'tbl_order_status');

            $data_pro = array(
                'pro_order_status' => $status
            ); 

            $data_pro = $this->security->xss_clean($data_pro);

            $this->common_model->updateByids($data_pro, array('order_id' => $this->input->post('order_id'), 'product_id' => $this->input->post('product_id'), 'user_id' => $this->input->post('user_id')),'tbl_order_items');

            if($status=='4'){
                $this->Product_model->updateTotalSale($value->product_id);
            }

            $data_arr1=$this->common_model->selectByids(array('order_id' => $this->input->post('order_id'), 'pro_order_status <>' => 5),'tbl_order_items');

            if(count($data_arr1)==1){

                $data = array(
                    'order_id' => $this->input->post('order_id'),
                    'user_id' => $this->input->post('user_id'),
                    'product_id' => '0',
                    'status_title' => $status,
                    'status_desc' => trim($this->input->post('status_desc')),
                    'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
                );

                $data = $this->security->xss_clean($data);

                $this->common_model->insert($data, 'tbl_order_status');

                $data_ord = array(
                    'order_status' => $status
                );

                $data_ord = $this->security->xss_clean($data_ord);
                $this->common_model->update($data_ord, $this->input->post('order_id'),'tbl_order_details');
            }
            else{

                if($status!=5){

                }
            }
        }
       
        $data_ord=$this->common_model->selectByid($this->input->post('order_id'),'tbl_order_details');
       
        $data_email=array();

        $arr_address=$this->common_model->selectByid($data_ord->order_address, 'tbl_addresses');

        $delivery_address=$arr_address->building_name.', '.$arr_address->road_area_colony.',<br/>'.$arr_address->pincode.'<br/>'.$arr_address->city.', '.$arr_address->state;

        $data_email['users_name']=$arr_address->name;

        $data_email['order_unique_id']=$data_ord->order_unique_id;
        $data_email['order_date']=date('d M, Y',$data_ord->order_date);

        $data_email['delivery_date']=date('d M, Y',$data_ord->delivery_date);

        $data_email['delivery_address']=$delivery_address;
        $data_email['discount_amt']=$data_ord->discount_amt;
        $data_email['total_amt']=$data_ord->total_amt;
        $data_email['delivery_charge']=$data_ord->delivery_charge;
        $data_email['payable_amt']=$data_ord->new_payable_amt;

        $data_email['status_desc']=$status_desc;
        $data_email['order_status']=$status;
        $data_email['status_title']=$this->get_status_title($status);

        $data_email['products']=$products_arr;
            
        if($this->input->post('send_mail')!=''){
            //  send email

            $this->email->from($this->app_email, $this->app_name);

            $this->email->to($arr_address->email); // replace it with receiver mail id

            $subject = $this->app_name.'- Order Status Update';

            $this->email->subject($subject); // replace it with relevant subject

            $body = $this->load->view('emails/order_status.php',$data_email,TRUE);

            $this->email->message($body); 

            if($this->email->send()){
            }
        }


        echo "true";
    }

    // update order delivery date
    public function update_delivery_date() 
    {
        $delivery_date=$this->input->post('delivery_date');
        $order_id=$this->input->post('order_id');

        $data = array(
            'delivery_date'  =>  strtotime($delivery_date)
        );

        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $order_id,'tbl_order_details');
        echo 'success';

    }

    
    
    //-- delete order
    public function delete($id)
    {
        echo $this->Order_model->delete($id);
    }

    //-- delete order
    public function delete_ord_status()
    {
        if(!empty($this->input->post())){
            echo $this->Order_model->delete_ord_status($this->input->post('order_id'),$this->input->post('status_id'));    
        }
        else{
            echo 'Invalid Request';
        }
    }

    function convertpdf()
    {

        $this->load->helper('pdf_helper');
        $this->load->view('admin/page/pdfreport', $data);
    }

    public function _create_thumbnail($path, $thumb_name, $fileName, $width, $height) 
    {
        $source_path = $path.$fileName;

        $ext = pathinfo($fileName, PATHINFO_EXTENSION);

        $thumb_name=$thumb_name.'_'.$width.'x'.$height.'.'.$ext;

        $thumb_path=$path.'thumbs/'.$thumb_name;

        if(!file_exists($thumb_path)){
            $this->load->library('image_lib');
            $config['image_library']  = 'gd2';
            $config['source_image']   = $source_path;       
            $config['new_image']      = $thumb_path;               
            $config['create_thumb']   = FALSE;
            $config['maintain_ratio'] = FALSE;
            $config['width']          = $width;
            $config['height']         = $height;
            $this->image_lib->initialize($config);
            if (! $this->image_lib->resize()) { 
                echo $this->image_lib->display_errors();
            }        
        }

        return $thumb_path;
    }

    public function order_notify()
    {
        $row=$this->api_model->get_unseen_orders(3);

        $html='';

        foreach ($row as $key => $value) {

            $old_title='New order is placed by '.$value->user_name;

            if(strlen($old_title) > 30){
              $title=substr(stripslashes($old_title), 0, 30).'...';  
            }else{
              $title=$old_title;
            }

            $html.='<li><a title="'.$old_title.'" href="'.site_url("admin/orders/".$value->order_unique_id).'"><div class="message"><div class="content"><div class="title">'.$title.'</div><div class="description"><span>ORDER ID:</span> '.$value->order_unique_id.'</div><div class="description"><span>Date:</span> '.date('d M, Y', $value->order_date).'</div></div></div></a></li>';
        }

        $response=array('count' => count($row), 'content' => $html);

        echo base64_encode(json_encode($response));
    }
}
