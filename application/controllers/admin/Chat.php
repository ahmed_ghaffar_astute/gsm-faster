<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Chat extends MY_Controller{
	public function __construct() {
		parent::__construct();
		$this->load->model('chats_model');
	}
	public function livechat(){
		$message = '';
		$check = $this->input->post_get('btnSave')?'yes':"No";
		$InAdminPanel = 0;
		$id = 1;
		if ($check!='No')
		{
			$script = check_input($this->input->post_get('txtScript'), $this->db->conn_id);
			$atWebsite = $this->input->post_get('chkWebsite') ? 1 : 0;
			$inClient = $this->input->post_get('chkClient') ? 1 : 0;
			$this->chats_model->updateChat($script,$atWebsite,$inClient,$InAdminPanel,$id);
			$message = $this->lang->line('BE_GNRL_11');

		}
		//var_dump($check,$this->input->post_get('btnSave'));die;
		$row = $this->chats_model->getChat($id);
		$script = stripslashes($row->ScriptCode);
		$disable = $row->DisableChat;
		$atWebsite = $row->AtWebsite;
		$inClient = $row->InClientPanel;

		$this->data['disable'] = $disable;
		$this->data['inClient'] = $inClient;
		$this->data['atWebsite'] = $atWebsite;
		$this->data['script'] = $script;
		$this->data['IS_DEMO'] = false;
		$this->data['message'] = $message;
		$this->data['check'] = $check;
		$this->data['view'] = 'admin/livechat_form';
		$this->load->view('admin/layouts/default1', $this->data);
	}

}
