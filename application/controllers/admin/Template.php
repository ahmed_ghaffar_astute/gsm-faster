<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends MY_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('template_model');

	}
	public function emailtpls(){
		$rsTpls = $this->template_model->getAllTemplates();
		$this->data['rsTpls'] = $rsTpls;
		$this->data['IS_DEMO'] = false;
		$this->data['view'] = 'admin/manage_templates';
		$this->load->view('admin/layouts/default1', $this->data);
	}
	public function emailtpl(){
		$id = $this->input->post_get('id') ? check_input($this->input->post_get('id'), $this->db->conn_id) : 0;
		$name = '';
		$subject = '';
		$contents = '';
		$message = '';
		$check = $this->input->post_get('btnSub')?:'No';
		if ($check != 'No')
		{
			$name = check_input($this->input->post_get('txtName'), $this->db->conn_id);
			$row = $this->template_model->getTemplateIdByName($name,$id);
			if (isset($row->TemplateId) && $row->TemplateId != '')
			{
				$message = $name. " ".$this->lang->line('BE_GNRL_10');
			}
			else
			{
				$sendCopy = $this->input->post_get('chkSendCopy') ? 1 : 0;
				$subject = check_input($this->input->post_get('txtSubject'), $this->db->conn_id);
				$contents = check_input($this->input->post_get('txtContents'), $this->db->conn_id, 1);
				$this->template_model->updateTemplate($name,$subject,$contents,$sendCopy,$id);
				$message = $this->lang->line('BE_GNRL_11');
			}
		}
		if($id > 0)
		{
			$row = $this->template_model->getTemplateById($id);
			if (isset($row->TemplateName) && $row->TemplateName != '')
			{
				$name= stripslashes($row->TemplateName);
				$subject = stripslashes($row->Subject);
				$contents = stripslashes($row->Contents);
				$sendCopy = $row->SendCopyToAdmin;
			}
		}

		$this->data['id'] = $id;
		$this->data['name'] = $name;
		$this->data['subject'] = $subject;
		$this->data['contents'] = $contents;
		$this->data['sendCopy'] = $sendCopy;
		$this->data['message'] = $message;
		$this->data['IS_DEMO'] = false;
		$this->data['view'] = 'admin/edit_email_template';
		$this->load->view('admin/layouts/default1', $this->data);
	}

}
