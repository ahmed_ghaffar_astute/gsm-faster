<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller{
     
	public function __construct() {
		parent::__construct();
    }
 
    public function index(){
        
        $DEFAULT_CURR_SYM = '';
        $DEFAULT_CURR_ABB = '';
        $DEFAULT_CURR_ID = '';       
        
        $rwCurrency = $this->Admin_dashboard_model->fetch_currency_data();
        if(isset($rwCurrency->CurrencyId) && $rwCurrency->CurrencyId != '')
        {
            $DEFAULT_CURR_ID = $rwCurrency->CurrencyId;
            $DEFAULT_CURR_SYM = stripslashes($rwCurrency->CurrencySymbol);
            $DEFAULT_CURR_ABB = stripslashes($rwCurrency->CurrencyAbb);
        }

        $TD_INV_CREDITS = 0;
        $rsTdInv = $this->Admin_dashboard_model->fetch_payments_and_currency_data($DEFAULT_CURR_ID);
        foreach($rsTdInv as $row)
        {
            $this->crypt_key($row->UserId);
            $invCredits = $this->decrypt($row->Credits);
            $TD_INV_CREDITS += $invCredits;
        }
        $MN_INV_CREDITS = 0;
        $rsMnInv = $this->Admin_dashboard_model->fetch_payments_and_currency_by_PaymentDtTm($DEFAULT_CURR_ID);
        foreach($rsMnInv as $row)
        {
            $this->crypt_key($row->UserId);
            $invCredits = $this->decrypt($row->Credits);
            $MN_INV_CREDITS += $invCredits;
        }

        $YR_INV_CREDITS = 0;
        $rsYrInv = $this->Admin_dashboard_model->fetch_payments_and_currency_by_interval($DEFAULT_CURR_ID);
        foreach($rsYrInv as $row)
        {
            $this->crypt_key($row->UserId);
            $invCredits = $this->decrypt($row->Credits);
            $YR_INV_CREDITS += $invCredits;
        }

        $this->data['DEFAULT_CURR_SYM'] = $DEFAULT_CURR_SYM;
        $this->data['TD_INV_CREDITS'] = $TD_INV_CREDITS;
        $this->data['MN_INV_CREDITS'] = $MN_INV_CREDITS;
        $this->data['YR_INV_CREDITS'] = $YR_INV_CREDITS;
        $this->data['DEFAULT_CURR_ABB'] = $DEFAULT_CURR_ABB;


        $IMEI_ORDERS_COUNT_TODAY = array();
        $yesterday = @date('Y-m-d H:i:s', @strtotime($this->data['currDt']) - 1 * 3600);
        $arrYesDt = explode(' ', $yesterday);
        $yesDtOnly = $arrYesDt[0];

        $arrCurrDt = explode(' ', $this->data['currDt']);
        $currDtOnly = $arrCurrDt[0];
        $arrCurrMY = explode('-', $currDtOnly);
        $this->data['thisMonth'] = $arrCurrMY[0].'-'.$arrCurrMY[1].'-01';
        $this->data['thisYear'] = $arrCurrMY[0].'-01-01';
        $colSize = 2;
        $IMEI_ORDERS_COUNT_TODAY = array();
        $FILE_ORDERS_COUNT_TODAY = array();
        $FILE_ORDERS_2_B_VERIFIED = 0;
        $SRVR_ORDERS_COUNT_TODAY = array();
        $TOTAL_IMEI_ORDERS_P_IP = 0;
        $TOTAL_FILE_ORDERS_P_IP = 0;
        $TOTAL_SRVR_ORDERS_P_IP = 0;
        if($this->data['rsStngs']->IMEIServices == '1')
        {
            $row = $this->Admin_dashboard_model->fetch_code_by_verify();
            $this->data['IMEI_ORDERS_2_B_VERIFIED'] = $row->V;

            $row = $this->Admin_dashboard_model->fetch_code_by_codestatusid($this->data['currDt']);
            $this->data['TOTAL_IMEI_ORDERS_M'] = $row->Orders;

            $row = $this->Admin_dashboard_model->fetch_code_by_year($this->data['currDt']);
            $this->data['TOTAL_IMEI_ORDERS_Y'] = $row->Orders;

            $row = $this->Admin_dashboard_model->fetch_codes_sum();
            $this->data['TOTAL_IMEI_ORDERS_P_IP'] = $row->P == '' ? 0 : $row->P + $row->IP == '' ? 0 : $row->IP;

            $row = $this->Admin_dashboard_model->fetch_codes_sum_by_ids($this->data['currDt']);
            $TOTAL_IMEI_ORDERS_TODAY = 0;
            if(isset($row->CA))
            {
                $IMEI_ORDERS_COUNT_TODAY[2] = $row->CA;
                $IMEI_ORDERS_COUNT_TODAY[3] = $row->NA;
                $IMEI_ORDERS_COUNT_TODAY[4] = $row->IP;
                $IMEI_ORDERS_COUNT_TODAY[1] = $row->P;
                $this->data['TOTAL_IMEI_ORDERS_TODAY'] = $IMEI_ORDERS_COUNT_TODAY[1] + $IMEI_ORDERS_COUNT_TODAY[2] + $IMEI_ORDERS_COUNT_TODAY[3] + $IMEI_ORDERS_COUNT_TODAY[4];
            }
        }
        if($this->data['rsStngs']->FileServices == '1')
        {
            $colSize += 2;
            $row = $this->Admin_dashboard_model->fetch_code_by_verify_slbf();
            $this->data['FILE_ORDERS_2_B_VERIFIED'] = $row->V;

            $row = $this->Admin_dashboard_model->fetch_code_slbf_by_codestatusid($this->data['currDt']);
            $this->data['TOTAL_FILE_ORDERS_M'] = $row->Orders;

            $row = $this->Admin_dashboard_model->fetch_code_slbf_by_year($this->data['currDt']); 
            $this->data['TOTAL_FILE_ORDERS_Y'] = $row->Orders;

            $row = $this->Admin_dashboard_model->fetch_codes_slbf_sum();
            $this->data['TOTAL_FILE_ORDERS_P_IP'] = $row->P == '' ? 0 : $row->P + $row->IP == '' ? 0 : $row->IP;

            $row =  $this->Admin_dashboard_model->fetch_codes_slbf_sum_by_ids($this->data['currDt']);
            
            $TOTAL_FILE_ORDERS_TODAY = 0;
            if(isset($row->CA))
            {
                $FILE_ORDERS_COUNT_TODAY[2] = $row->CA;
                $FILE_ORDERS_COUNT_TODAY[3] = $row->NA;
                $FILE_ORDERS_COUNT_TODAY[4] = $row->IP;
                $FILE_ORDERS_COUNT_TODAY[1] = $row->P;
                $this->data['TOTAL_FILE_ORDERS_TODAY'] = $FILE_ORDERS_COUNT_TODAY[1] + $FILE_ORDERS_COUNT_TODAY[2] + $FILE_ORDERS_COUNT_TODAY[3] + $FILE_ORDERS_COUNT_TODAY[4];
            }


        }

        if($this->data['rsStngs']->ServerServices == '1')
        {
            $colSize += 2;
            
            $row = $this->Admin_dashboard_model->fetch_log_requests_by_id();
            $this->data['SERVER_ORDERS_2_B_VERIFIED'] = $row->V;
            
            $row = $this->Admin_dashboard_model->fetch_log_requests_by_month($this->data['currDt']);
            $TOTAL_SRVR_ORDERS_M = $row->Orders;

            $row = $this->Admin_dashboard_model->fetch_log_requests_by_year($this->data['currDt']);
            $this->data['TOTAL_SRVR_ORDERS_Y'] = $row->Orders;

            $row =  $this->Admin_dashboard_model->fetch_log_requests_sum_by_id();
            $this->data['TOTAL_SRVR_ORDERS_P_IP'] = $row->P == '' ? 0 : $row->P + $row->IP == '' ? 0 : $row->IP;

            $row = $this->Admin_dashboard_model->fetch_log_requests_sum_by_ids($this->data['currDt']);
            
            $TOTAL_SRVR_ORDERS_TODAY = 0;
            if(isset($row->CA))
            {
                $SRVR_ORDERS_COUNT_TODAY[2] = $row->CA;
                $SRVR_ORDERS_COUNT_TODAY[3] = $row->NA;
                $SRVR_ORDERS_COUNT_TODAY[4] = $row->IP;
                $SRVR_ORDERS_COUNT_TODAY[1] = $row->P;
                $this->data['TOTAL_SRVR_ORDERS_TODAY'] = $SRVR_ORDERS_COUNT_TODAY[1] + $SRVR_ORDERS_COUNT_TODAY[2] + $SRVR_ORDERS_COUNT_TODAY[3] + $SRVR_ORDERS_COUNT_TODAY[4];
            }
        }

        if($colSize == '2')
	        $colSize = 4;
        if($colSize == '4')
            $colSize = 8;
        
        

        $this->data['currDtOnly'] = $currDtOnly;
        $this->data['IMEI_ORDERS_COUNT_TODAY'] = $IMEI_ORDERS_COUNT_TODAY;
        $this->data['SRVR_ORDERS_COUNT_TODAY'] = $SRVR_ORDERS_COUNT_TODAY;


        $this->data['rsIMEIReqs'] = $this->Admin_dashboard_model->fetch_codes_packages_and_category();
        $this->data['rsFileReqs'] =  $this->Admin_dashboard_model->fetch_slbf_codes_packages_and_category();
      
        $this->data['rsServerReqs'] = $this->Admin_dashboard_model->fetch_log_requests_packages_and_category();
        $this->data['rsPCs'] = $this->Admin_dashboard_model->fetch_precodes_logpackages();


        $this->data['UNPAID_ADMIN_INV'] = 0;
        $this->data['UNPAID_USR_INV'] = 0;
        $rsInvoices = $this->Admin_dashboard_model->fetch_users_payments_record($DEFAULT_CURR_ID);
       
        $unpaidCInv = 0;
        $unpaidAInv = 0;
		//d($rsInvoices);
        foreach($rsInvoices as $row)
        {
        	if($row->Amount == "")
			{
				continue;
			}
            $this->crypt_key($row->UserId);
            $invoiceAmount = $this->decrypt($row->Amount);
            if($row->ByAdmin == '1')
            {
                $unpaidAInv += $invoiceAmount;
                $this->data['UNPAID_ADMIN_INV'] = $unpaidAInv;
            }
            else if($row->ByAdmin == '0')
            {
                $unpaidCInv += $invoiceAmount;
                $this->data['UNPAID_USR_INV'] = $unpaidCInv;
            }
        }
        $row = $this->Admin_dashboard_model->fetch_disable_and_archived_users();
        $this->data['USER_ACT'] = $row->PU;
        $NEW_IMEI_ORDERS = 0;
        $NEW_FILE_ORDERS = 0;
        $NEW_SERVER_ORDERS = 0;
        if($this->data['rsStngs']->IMEIServices == '1')
        {
            $row = $this->Admin_dashboard_model->fetch_sum_codes();
          
            $this->data['NEW_IMEI_ORDERS'] = $row->P;
        }
        if($this->data['rsStngs']->FileServices == '1')
        {
            $row = $this->Admin_dashboard_model->fetch_sum_codesslbf();
            $this->data['NEW_FILE_ORDERS'] = $row->P;
        }
        if($this->data['rsStngs']->ServerServices == '1')
        {
            $row = $this->Admin_dashboard_model->fetch_sum_logrequests();
            $this->data['NEW_SERVER_ORDERS'] = $row->P;
        }
        $row = $this->Admin_dashboard_model->fetch_user_invoices_sum();

				
        $this->data['PENDING_INV'] = $row->UserInvoices == '' ? 0 : $row->UserInvoices;
        $this->data['PENDING_INV_ADMIN'] = $row->AdminInvoices == '' ? 0 : $row->AdminInvoices;


        $this->data['view'] = 'admin/dashboard_new';
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function changepassword(){
        $class = 'alert-danger';
        $message = '';
        if ($this->input->post('txtOldPass'))
        {
            $oldPassword = $this->input->post_get('txtOldPass');
            $password = $this->input->post_get('txtNewPass');
            $confirmPassword = $this->input->post_get('txtConfirmPass');
          
            if($confirmPassword == $password)
            {
                $row = $this->Admin_dashboard_model->fetch_admin_pass_by_id();
                
                if (isset($row->AdminUserPassword) && $row->AdminUserPassword != '')
                {
                    if(isPasswordOK($oldPassword, $row->AdminUserPassword))
                    {
                        $encryptedPass = bFEncrypt($password);

                        $update_data = array(
                            'AdminUserPassword' => $encryptedPass
                        );

                        $where = array(
                            'AdminId' => $this->session->userdata('GSM_FSN_AdmId')
                        );


                        $this->Admin_dashboard_model->update_admin_password($update_data , $where);
                       
                        $message = $this->lang->line('BE_GNRL_11');
                        $class = 'alert-success';
                    }
                    else
                        $message = $this->lang->line('BE_PWD_4');
                }
                else
                    $message = $this->lang->line('BE_PWD_4');
            }
        }

        $this->data['message'] = $message ;
        $this->data['class']  =$class ;

        $this->data['view'] = 'admin/change_password' ;
        $this->load->view('admin/layouts/default1' , $this->data);
    }

    public function refreshapiorders(){
        if($this->data['rsStngs']->IMEIServices == '1'){
            $update_data = array(
                'HiddenStatus' => 0
            );

            $where = array(
                'CodeStatusId' => 4
            );

            $where_in = array(
                'HiddenStatus' => array(1,2)
            );
            $this->Admin_dashboard_model->update_codes($update_data, $where , $where_in);
           

        }
        if($this->data['rsStngs']->FileServices == '1'){
            $update_data = array(
                'HiddenStatus' => 0
            );

            $where = array(
                'CodeStatusId' => 4
            );

            $where_in = array(
                'HiddenStatus' => array(1,2)
            );
            $this->Admin_dashboard_model->update_codes_slbf($update_data, $where , $where_in);
        }
        if($this->data['rsStngs']->ServerServices == '1'){
            $update_data = array(
                'HiddenStatus' => 0
            );

            $where = array(
                'StatusId' => 4
            );

            $where_in = array(
                'HiddenStatus' => array(1,2)
            );
            $this->Admin_dashboard_model->update_log_requests($update_data, $where , $where_in);
        }

        $this->data['view'] = 'admin/refresh_api_orders' ;
        $this->load->view('admin/layouts/default1' , $this->data);
            
    }


    public function signout(){
        if($this->session->userdata("AdminLoginId") != '')
        {
            $logoutDtTm = setDtTmWRTYourCountry();
            $update_data = array(
                'LogoutTime' => $logoutDtTm
            );

            $where = array(
                'Id' => $this->session->userdata('AdminLoginId')
            );

            $this->Admin_dashboard_model->update_admin_log( $update_data , $where);
           
        }

        $this->session->unset_userdata('GSM_FSN_AdmId');
        $this->session->unset_userdata('AdminName');
        $this->session->unset_userdata('AdminUserName');
        $this->session->unset_userdata('AdminLoginId');
        $this->session->unset_userdata('AdminLogo');
        $this->session->unset_userdata('LOCAL_LANGUAGE');
        $this->session->unset_userdata('LOCAL_LANG_NAME');
        $this->session->unset_userdata('IsAdmin');
    
        $this->session->sess_destroy();
        redirect(base_url('admin/login'));
    }

    public function onlinecustomers(){
        $txtlqry = '';
        $page_name = $this->input->server('PHP_SELF');
        if(!isset($start))
            $start = 0;
        if($this->input->post_get("start"))
            $start = $this->input->post_get("start");
        $eu = ($start - 0);
        $limit = 50;
        $thisp = $eu + $limit;
        $back = $eu - $limit;
        $next = $eu + $limit;
        $currDt = setDtTmWRTYourCountry();
        $this->data['rSet'] = $this->Admin_dashboard_model->fetch_users_log($currDt);

        $row = $this->Admin_dashboard_model->fetch_users_log_count($currDt);
      
        $this->data['totalRows'] = $row->TotalRecs;
        $this->data['txtlqry'] = $txtlqry;
        $this->data['page_name'] = $page_name;
        $this->data['start'] = $start;
        $this->data['eu'] = $eu;
        $this->data['limit'] = $limit;
        $this->data['thisp'] = $thisp;
        $this->data['back'] = $back;
        $this->data['next'] = $next;
        $this->data['pLast'] = '';
        $this->data['view'] = 'admin/online_customers' ;
        $this->load->view('admin/layouts/default1' , $this->data);
        
    }

	public function logout() {
		$this->session->sess_destroy();
		redirect(base_url('admin/login'));
	}


}
