<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends MY_Controller {

    public function __construct(){
        parent::__construct();
        //
        $this->load->helper('image'); 
        $this->load->model('common_model');
        $this->load->model('Category_model');
    }

    function index(){
        
        $this->data['page_title'] = $this->lang->line('category_lbl');
        $this->data['current_page'] = $this->lang->line('category_lbl');
        
        $row=$this->Category_model->category_list('id','DESC');

        $config = array();
        $config["base_url"] = base_url() . 'admin/category';
        $config["total_rows"] = count($row);
        $config["per_page"] = 12;

        $config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;

        $config['enable_query_strings'] = TRUE;
        $config['page_query_string'] = FALSE;

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
         
        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
         
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
         
        $config['next_link'] = '';
        $config['next_tag_open'] = '<span class="nextlink">';
        $config['next_tag_close'] = '</span>';

        $config['prev_link'] = '';
        $config['prev_tag_open'] = '<span class="prevlink">';
        $config['prev_tag_close'] = '</span>';

        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';

        $config['num_tag_open'] = '<li style="margin:3px">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        $page=($page-1) * $config["per_page"];

        if($this->input->post('search_value')!=''){
            $keyword=addslashes(trim($this->input->post('search_value')));

            $this->data['category_list'] = $this->Category_model->category_list('id', 'DESC', '', '', $keyword);
        }
        else{
            $this->data["links"] = $this->pagination->create_links();
            $this->data['category_list'] = $this->Category_model->category_list('id', 'DESC', $config["per_page"], $page);    
        }
		$this->data['view'] = 'admin/page/category' ;
        //$this->load->view('admin/layouts/default1' , $this->data); // :blush:
		$this->load->view('admin/layouts/default1' , $this->data);
    }

    function addForm()
    {
        $this->form_validation->set_rules('title', 'Enter Category Title', 'trim|required');

        if($this->form_validation->run() == FALSE)
        {
            $messge = array('message' => $this->lang->line('input_required'),'class' => 'alert alert-danger');
            $this->session->set_flashdata('response_msg', $messge);
            redirect(base_url() . 'admin/category/add');
        }
        else
        {
            $config['upload_path'] =  'assets/images/category/';
            $config['allowed_types'] = 'jpg|png|jpeg|PNG|JPG|JPEG';

            $image = date('dmYhis').'_'.rand(0,99999).".".pathinfo($_FILES['file_name']['name'], PATHINFO_EXTENSION);

            $config['file_name'] = $image;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file_name')) {
                $messge = array('message' => $this->upload->display_errors(),'class' => 'alert alert-danger');
                $this->session->set_flashdata('response_msg', $messge);

                redirect(base_url() . 'category/add', 'refresh');
            }
            else
            {
                $upload_data = $this->upload->data();
            }

            $this->load->helper("date");

            $slug = url_title($this->input->post('title'), 'dash', TRUE);

            $data = array(
                'category_name' => $this->input->post('title'),
                'category_slug' => $slug,
                'category_image' => $image,
                'product_features' => ($this->input->post('product_features')!='') ? implode(',', $this->input->post('product_features')) : '',
                'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
            );

            $data = $this->security->xss_clean($this->data);

            if($this->common_model->insert($data, 'tbl_category')){
                $messge = array('message' => $this->lang->line('add_msg'),'class' => 'alert alert-success');
                $this->session->set_flashdata('response_msg', $messge);

            }
            else{
                $messge = array('message' => $this->lang->line('error_add'),'class' => 'alert alert-danger');
                $this->session->set_flashdata('response_msg', $messge);
            }

            redirect(base_url() . 'admin/category/add', 'refresh');
        }
    }


    public function get_category()
    {
        $data = array();
        $this->data['page_title'] = 'Categories';
        $this->data['current_page'] = 'Categories';
        $this->data['category_list'] = $this->Category_model->category_list();
        $this->load->view('admin/layouts/default1' , 'admin/page/category', $this->data); // :blush:
    }

    public function category_form()
    {
        $data = array();

        $id =  $this->uri->segment(4);

        $this->data['page_title'] = $this->lang->line('category_lbl');
        if($id==''){
            $this->data['current_page'] = $this->lang->line('add_category_lbl');;
        }
        else{
            $this->data['category'] = $this->Category_model->single_category($id);

            $this->data['current_page'] = $this->lang->line('edit_category_lbl');;
        }
		$this->data['view'] = 'admin/page/category_form' ;
        $this->load->view('admin/layouts/default1' , $this->data); // :blush:
    }



    //-- update users info
    public function editForm($id)
    {
    	//echo "here";
        $data = $this->Category_model->single_category($id);

        if($_FILES['file_name']['error']!=4){
            
            $config['upload_path'] =  'assets/images/category/';
            $config['allowed_types'] = 'jpg|png|jpeg|PNG|JPG|JPEG';

            if(file_exists('assets/images/category/'.$data[0]->category_image)){
                unlink('assets/images/category/'.$data[0]->category_image);

                $mask = $data[0]->category_slug.'*_*';
                array_map('unlink', glob('assets/images/category/thumbs/'.$mask));
            }

            $image = date('dmYhis').'_'.rand(0,99999).".".pathinfo($_FILES['file_name']['name'], PATHINFO_EXTENSION);

            $config['file_name'] = $image;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file_name')) {
                $messge = array('message' => $this->upload->display_errors(),'class' => 'alert alert-danger');
                $this->session->set_flashdata('response_msg', $messge);

                redirect(base_url() . 'admin/category/edit/'.$id);
            }

        }
        else{
            $image=$data[0]->category_image;
        }

        $this->load->helper("date");

        $slug = url_title($this->input->post('title'), 'dash', TRUE);

        $data = array(
            'category_name' => $this->input->post('title'),
            'category_slug' => $slug,
            'category_image' => $image,
            'product_features' => ($this->input->post('product_features')!='') ? implode(',', $this->input->post('product_features')) : ''
        );

        $data = $this->security->xss_clean($data);

        if($this->common_model->update($data, $id,'tbl_category')){
            $messge = array('message' => $this->lang->line('update_msg'),'class' => 'alert alert-success');
            $this->session->set_flashdata('response_msg', $messge);

        }
        else{
            $messge = array('message' => $this->lang->line('update_error'),'class' => 'alert alert-danger');
            $this->session->set_flashdata('response_msg', $messge);
        }

        redirect(base_url() . 'admin/category/edit/'.$id, 'refresh');
        
    }

    
    //-- active
    public function active($id) 
    {
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($this->data);
        $this->common_model->update($data, $id,'tbl_category');
        echo $this->lang->line('enable_msg');
    }

    //-- deactive
    public function deactive($id) 
    {
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($this->data);
        $this->common_model->update($data, $id,'tbl_category');
        echo $this->lang->line('disable_msg');
    }

    //-- deactive
    public function category_on_home() 
    {

        $data = array(
            'set_on_home' => $this->input->post('status')
        );
        $data = $this->security->xss_clean($this->data);
        $this->common_model->update($data, $this->input->post('id'),'tbl_category');
        
        if($this->input->post('status')==1){

            $messge = array('message' => $this->lang->line('category_show'),'class' => 'success');
            $this->session->set_flashdata('response_msg', $messge);

        }
        else{
            $messge = array('message' => $this->lang->line('category_remove'),'class' => 'success');
            $this->session->set_flashdata('response_msg', $messge);
        }
    }


    //-- delete category
    public function delete($id)
    {
        echo $this->Category_model->delete($id);
    }


}
