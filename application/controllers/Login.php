<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	}

	public function index() {
		$data = array();
		if($this->input->method() == 'post') {
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required');

			if($this->form_validation->run()) {
				$userName = $this->input->post('username');
				$pwd = $this->input->post('password');
				if(!ipBlocked()) {
					
					$rowSettings = $this->General_model->getLoginAttempts();
				
					$LOGIN_ATTEMPTS = $rowSettings->LoginAttempts;
				
					$rowUser = $this->General_model->getUserByUsername($userName);
					// last_query();
					// die ;
				//	d($rowUser);
				//die ;
					if ($rowUser && $rowUser->DisableUser == '0') {
						
						if($rowUser->LoginAttempts < $LOGIN_ATTEMPTS) {
							if (isset($rowUser->UserId) && $rowUser->UserId != '') {
								if(isPasswordOK($pwd, $rowUser->UserPassword)) {
									
									$IP = $_SERVER['REMOTE_ADDR'];
									$letMeLogin = true;
									$this->General_model->updateUserLoginAttempts($rowUser->UserId);
									$arr = getEmailDetails();
									$flashdataSet = false;
									if($_SERVER['HTTP_HOST'] != 'localhost' || 1) {
										
										if($rowUser->CanLoginFrmOtherCntry == '0') {
											$countryISO = '00';
											if (!empty($IP)) {
												$countryISO = getLocationInfoByIp($IP);
											}
											if($countryISO != '00') {
												$rwUsrCntry = $this->General_model->getUserCountry($rowUser->UserId , $countryISO);
												if ($rwUsrCntry->UserValidity == 0) {
													send_push_notification('A recent login attempt failed due to the Country you are trying to login from is Not allowed.', $rowUser->UserId);
													$letMeLogin = false;
													$this->session->set_flashdata('error_message', "Your Country IP does not match with our records.");
													$flashdataSet = true;
													$emailMsg = 'A recent login attempt failed due to the Country you are trying to login from is Not allowed.<br />
													Details of the attempt are below<br />
													Login Attempted At: '.setDtTmWRTYourCountry().'<br />
													IP Address: '.$IP.'<br />
													Please click <a href="'.base_url('login/ipwhitelisting').'?tkn1='.urlsafe_b64encode($rowUser->UserId).'&tkn2='.urlsafe_b64encode($countryISO).'" target="_blank">here</a> to add this IP Country to your whitelist.<br />';
													sendGeneralEmail($rowUser->UserEmail, $arr[0], $arr[1], $rowUser->FullName, 'Unsuccessful Customer Login Attempt '.$arr[2], '', $emailMsg);
												}
											}
										}
									}
									if($letMeLogin) {
										
										if($rowUser->TwoStepVerification == '1') {
											$this->session->set_userdata('passCode', substr(number_format(time() * rand(), 0, '', ''), 0, 4));
											$this->session->set_userdata('loginRequest', substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(12 / strlen($x)))), 1, 12));
											$emailMsg = 'Your Login verification Code is <b>'.stripslashes($this->session->userdata('passCode')).'</b>';
											sendGeneralEmail($rowUser->UserEmail, $arr[0], $arr[1], 'Customer', 'Login Verfication Code', '', $emailMsg);
											redirect(base_url('login/loginverification').'?tkn1='.urlsafe_b64encode($rowUser->UserId).'&tkn2='.$this->session->userdata('loginRequest'));
										}else{
											$this->session->set_userdata(array(
												'GSM_FUS_UserId' => $rowUser->UserId,
												'UserFullName' => stripslashes($rowUser->FullName),
												'UserFName' => stripslashes($rowUser->FirstName),
												'UserName' => $rowUser->UserName,
												'UserEmail' => $rowUser->UserEmail,
												'LANGUAGE' => $rowUser->UserLang,
												'gsmfsn_CSRFToken' => md5(uniqid(rand(), true)),
												'CLIENT_GF_IP' => $IP,
												'CLIENT_NEW_POPUP' => '0',
												'ORDERED_FROM' => '1',
											));
											$rwLog = $this->General_model->getLogIdByUserIdAndIP($IP);
											$this->General_model->saveUserLog();
											$this->session->set_userdata('UserLoginId', $this->General_model->saveUserLoginLog());
											send_push_notification('A successful login attempt to your account from IP '.$IP, $rowUser->UserId);
											$this->getlogoandtitle_cp();
											redirect(base_url('dashboard'));
										}
									}else{
										send_push_notification('A recent login attempt failed!.', $rowUser->UserId);
										failedLoginAttempt($userName);
										if(!$flashdataSet) {
											$this->session->set_flashdata('error_message', "The username or password is incorrect.");
										}
									}
								}else{
									send_push_notification('A recent login attempt failed!.', $rowUser->UserId);
									failedLoginAttempt($userName);
									$this->session->set_flashdata('error_message', "The username or password is incorrect.");
								}
							}else{
								send_push_notification('A recent login attempt failed!', $rowUser->UserId);
								failedLoginAttempt($userName);
								$this->session->set_flashdata('error_message', "The username or password is incorrect.");
							}
						}else{
							$this->session->set_flashdata('error_message', "Your account has been blocked due to continous failed attempts to login.");
							$userId = '0';
							$rwUser = $this->General_model->getUserInfoByUserName($userName);
							if(isset($rwUser->UserId) && $rwUser->UserId != '')
								$userId = $rwUser->UserId;
							if($userId != '0')
							{
								send_push_notification('Account Blocked due to failed login attempts!', $userId);
								$this->General_model->blockUser($userId);
								$this->General_model->addBlockedIps($userId);
							}
						}
					}else if (isset($rowUser->DisableUser) && $rowUser->DisableUser == '1') {
						if($rowUser->Comments != '') {
							$reason = $rowUser->Comments;
							$this->session->set_flashdata('error_message', $reason);
						} else {
							$this->session->set_flashdata('error_message', 'The username or password is incorrect.');
						}
					} else {
						$this->session->set_flashdata('error_message', 'The username or password is incorrect.');
					}
				}else{
					failedLoginAttempt($userName);
					$this->session->set_flashdata('error_message', 'Your IP has been blocked to access your account, please contact Administrator to login into your account.');
				}
			}
		}
		$data['view'] = 'login';
		$this->load->view('layouts/login_default', $data);
	}

	public function loginverification() {
		$data = array();
		if($this->input->get('tkn2') == $this->session->userdata('loginRequest')) {
			if($this->input->method() == 'post') {
				$this->form_validation->set_rules('passcode', 'Login Verification Code', 'trim|required|integer');
				if($this->form_validation->run()) {
					$user_Id = $this->input->get('tkn1');
					$user_Id = $user_Id ?: 0;
					$loginRequest = $this->input->get('tkn2');
					$user_Id = urlsafe_b64decode($user_Id);
					$passcode = $this->input->post('passcode');
					$sess_passcode = $this->session->userdata('passCode');
					$sess_loginrequest = $this->session->userdata('loginRequest');

					if($user_Id && $passcode == $sess_passcode && $loginRequest == $sess_loginrequest) {
						$rowUser = $this->General_model->getUserInfoByUserId($user_Id);
						$IP = $this->input->server('REMOTE_ADDR');
						$this->session->set_userdata(array(
							'GSM_FUS_UserId' => $rowUser->UserId,
							'UserFullName' => stripslashes($rowUser->FullName),
							'UserFName' => stripslashes($rowUser->FirstName),
							'UserName' => $rowUser->UserName,
							'UserEmail' => $rowUser->UserEmail,
							'LANGUAGE' => $rowUser->UserLang,
							'gsmfsn_CSRFToken' => md5(uniqid(rand(), true)),
							'CLIENT_GF_IP' => $IP,
							'CLIENT_NEW_POPUP' => '0',
							'ORDERED_FROM' => '1',
						));
						$rwLog = $this->General_model->getLogIdByUserIdAndIP($IP);
						$this->General_model->saveUserLog();
						$this->session->set_userdata('UserLoginId', $this->General_model->saveUserLoginLog());
						send_push_notification('A successful login attempt to your account from IP '.$IP, $rowUser->UserId);
						$this->getlogoandtitle_cp();
						$this->session->unset_userdata('passCode');
						$this->session->unset_userdata('loginRequest');
						redirect(base_url('dashboard'));
					}else{
						$this->session->set_flashdata('error_message', "The verification code you provided is wrong.");
					}
					redirect(base_url('login/loginverification?'.$this->input->server('QUERY_STRING')));
				}
			}
			$data['view'] = 'verify_login';
			$this->load->view('layouts/login_default', $data);
		}else{
			redirect(base_url('login'));
		}
	}

	public function ipwhitelisting() {
		$user_Id = $this->input->get('tkn1');
		$user_Id = $user_Id ?: 0;
		$iso = $this->input->get('tkn2');
		$iso = $iso ?: 0;
		$user_Id = urlsafe_b64decode($user_Id);
		$iso = urlsafe_b64decode($iso);

		if($iso) {
			$rowAC = $this->General_model->getUserInfoByUserId($user_Id);
			if (isset($rowAC->UserId) && $rowAC->UserId != '') {
				$this->General_model->deleteUserLoginCountries($rowAC->UserId, $iso);
				$this->General_model->saveUserLoginCountries($rowAC->UserId, $iso);
				$this->session->set_flashdata('message', "Your IP country has been added into your white list. Now you can login with this IP's Country.");
			} else {
				$this->session->set_flashdata('error_message', "Invalid Link!");
			}
		}
		redirect(base_url('login'));
	}

	private function getlogoandtitle_cp() {
		$rsStngs = $this->General_model->getEmailSettings();
		$THEME = $rsStngs->Theme;

		$this->session->set_userdata('THEME', $THEME);

		$rSetLogo = $this->General_model->getLogoIdAndLogoPath();
		foreach($rSetLogo as $rw) {
			if (isset($rw->LogoPath) && $rw->LogoPath != '')
			{
				if($rw->LogoId == '2') {
					$this->session->set_userdata('CL_LOGO_PATH', FCPATH . "uploads/$THEME/".$rw->LogoPath);
				}
				if($rw->LogoId == '6') {
					$this->session->set_userdata('CL_FAV_ICO', FCPATH . "uploads/$THEME/".$rw->LogoPath);
				}
			}
		}
	}

	public function logout(){
		if($this->session->userdata('UserLoginId')){
			$logoutDtTm = setDtTmWRTYourCountry();
			$this->User_model->update_logout_time($$logoutDtTm);
		}

		$array_items = array(
			'GSM_FUS_UserId' => '',
			'GTOOL_UsrPwd' => '' , 
			'UserFullName' => '' ,
			'UserFName' => '' ,
			'UserName' => '' ,
			'UserEmail' => '' ,
			'LANGUAGE' => '',
			'UserLoginId'=> '' ,
			'CL_LOGO_PATH' => '' ,
			'LIVE_CHAT_CODE' => '',
			'CLIENT_NEW_POPUP' => ''		
		);
		$this->session->unset_userdata($array_items);
		redirect(base_url('login'));

	}

	public function login_security(){
		$errorMsg = '';
		$message = '';
		$id = $this->input->post_get('id') ?: '0';	
		if($id != '0')
		{	
			$this->User_model->delete_user_login_countries($id);
		
			$this->data['message'] = 'Country has been removed from the list!';
		}
		if (isset($_POST['countryId']))
		{
			if ($_POST['countryId'] != '0' && $_POST['countryId'] != '')
			{
				$iso = $this->input->post('countryId');
				$this->User_model->delete_user_login_countries($iso);
				$this->User_model->insert_user_country_login($iso);
				$this->data['message'] = 'Country has been added into your login countries list!';
			
			}
			else{
				$this->data['message'] = 'Please select a country!';
			}
		}
		$this->data['rsCntrs'] = $this->User_model->get_countries();
		
		$this->data['view'] = 'login_security';
		$this->load->view('layouts/default' , $this->data);
	}
}
