<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//LOCATION : application - controller - Auth.php

class Site extends MY_Controller {


    private $app_logo;

    private $app_name;

    private $app_email;

    private $contact_email;

    private $order_email;

    public function __construct(){
        error_reporting(0);

        parent::__construct();
        $this->load->model('Common_model','common_model');
        $this->load->model('Api_model','api_model');
        $this->load->model('Coupon_model');
        $this->load->model('Offers_model');
        //$this->load->model('Banner_model');
        $this->load->model('Product_model');
        $this->load->model('Sub_Category_model');
        //$this->load->model('Users_model');
        $this->load->model('Order_model');
        //$this->load->model('Setting_model');
        $this->load->library('user_agent');
        $this->load->library('pagination');

        //SmtpSettings();

        //$app_setting = $this->api_model->app_details();

//        $web_setting = $this->Setting_model->get_web_details();

        //$smtp_setting = $this->api_model->smtp_settings();

        /*
        $this->app_name=$app_setting->app_name;
        $this->app_email=$smtp_setting->smtp_email;
        $this->contact_email=$app_setting->app_email;
        $this->order_email=$app_setting->app_order_email;
*/
        define('APP_CURRENCY', "USD");
        define('CURRENCY_CODE', "US $");

    }

    private function get_random_code()
    {
        $code_feed = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyv0123456789";
        $code_length = 8;  // Set this to be your desired code length
        $final_code = "";
        $feed_length = strlen($code_feed);

        for($i = 0; $i < $code_length; $i ++) {
            $feed_selector = rand(0,$feed_length-1);
            $final_code .= substr($code_feed,$feed_selector,1);
        }
        return $final_code;
    }

    public function get_status_title($id){
        return $this->common_model->selectByidParam($id,'tbl_status_title','title');
    }

    public function getCount($table,$where=''){

        if($where=='')
            return $this->common_model->get_count($table);
        else
            return $this->common_model->get_count_by_ids($where,$table);
    }

    public function index(){
        //$data = array();

        $this->data['page_title'] = 'Shop';
        $this->data['current_page'] = 'Shop';

		$this->data['category_list'] = $this->common_model->selectWhere('tbl_category', array('status' => '1'), 'DESC', 'id');
		$this->data['offers_list'] = $this->common_model->selectWhere('tbl_offers', array('status' => '1'), 'DESC', 'id');
        //$this->data['banner_list'] = $this->common_model->selectWhere('tbl_banner', array('status' => '1'), 'DESC', 'id');
        $this->data['todays_deal'] = $this->api_model->todays_deals();

        $this->data['home_categories'] = $this->common_model->selectByids(array('set_on_home' => '1','status' => '1'),'tbl_category','category_name','ASC');
		$this->data['view'] = 'site/pages/home';

        $this->load->view('layouts/default', $this->data); // :blush:
    }

    public function page_not_found(){
        $data = array();
        $data['page_title'] = 'Page not found';
        $data['current_page'] = 'Page not found';

        $this->load->view('layouts/default', 'site/pages/404', $data); // :blush:
    }


    public function banners(){
        $data = array();
        $data['page_title'] = 'Banners';
        $data['current_page'] = 'Banners';
        $data['banner_list'] = $this->common_model->selectWhere('tbl_banner', array('status' => '1'), 'DESC', 'id');
        $this->load->view('layouts/default', 'site/pages/banners', $data); // :blush:
    }

    public function offers(){
        $data = array();
        $data['page_title'] = 'Offers';
        $data['current_page'] = 'Offers';
        $data['offers_list'] = $this->common_model->selectWhere('tbl_offers', array('status' => '1'), 'DESC', 'id');
        $this->load->view('layouts/default', 'site/pages/offers', $data); // :blush:
    }

    public function category(){

        $data = array();
        $data['page_title'] = 'Categories';
        $data['current_page'] = 'Categories';
        $data['category_list'] = $this->common_model->selectWhere('tbl_category', array('status' => '1'), 'DESC', 'id');
        $this->load->view('layouts/default', 'site/pages/category', $data); // :blush:
    }

    public function sub_category(){

        $where=array('category_slug' => $this->uri->segment(2));

        $cat_id =  $this->common_model->getIdBySlug($where, 'tbl_category');

        $data = array();
        $data['page_title'] = 'Sub Categories';
        $data['current_page'] = $this->common_model->selectByidParam($cat_id,'tbl_category','category_name');
        $data['sub_category_list'] = $this->Sub_Category_model->get_subcategories($cat_id);
        $data['category_slug'] = $this->common_model->selectByidParam($cat_id,'tbl_category','category_slug');

        $data['sharing_img'] = base_url('assets/images/category/'.$this->common_model->selectByidParam($cat_id,'tbl_category','category_image'));

        $this->load->view('layouts/default', 'site/pages/sub_category', $data); // :blush:
    }

    // for product search
    public function search(){

        $keyword=stripslashes(trim($this->input->get('keyword')));

        if($keyword!=''){
            $data = array();
            $data['page_title'] = 'Search';
            $data['current_page'] = 'Search Result for '.ucwords($keyword);

            $config = array();
            $config["base_url"] = base_url() . 'search-result';
            $config["total_rows"] = count($this->api_model->product_search($keyword));
            $config["per_page"] = 12;

            $config['num_links'] = 4;
            $config['use_page_numbers'] = TRUE;
            $config['reuse_query_string'] = TRUE;

            $config['enable_query_strings'] = TRUE;
            $config['page_query_string'] = TRUE;
             
            $config['full_tag_open'] = '<ul class="page-number">';
            $config['full_tag_close'] = '</ul>';
             
            $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
             
            $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
             
            $config['next_link'] = '';
            $config['next_tag_open'] = '<span class="nextlink">';
            $config['next_tag_close'] = '</span>';

            $config['prev_link'] = '';
            $config['prev_tag_open'] = '<span class="prevlink">';
            $config['prev_tag_close'] = '</span>';

            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';

            $config['num_tag_open'] = '<li style="margin:3px">';
            $config['num_tag_close'] = '</li>';

            $this->pagination->initialize($config);

            $page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 1;

            $page=($page-1) * $config["per_page"];

            $page2 = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

            if($page2 != 1){
                $page2-=1;
            }

            $data["links"] = $this->pagination->create_links();

            $row=$this->api_model->product_search($keyword, $config["per_page"], $page);

            if(!empty($this->input->get('sort')))
            {
                $row=$this->api_model->product_search($keyword, $config["per_page"], $page,$this->input->get('sort'));
            }


            $data["show_result"] = 'Showing '.$page2.'–'.count($row).' of '.count($this->api_model->product_search($keyword)).' results';  

            $data['product_list']=$row;

            $this->load->view('layouts/default', 'site/pages/search', $data); // :blush:
        }
        else{
            $data = array();
            $data['page_title'] = 'Page not found';
            $data['current_page'] = 'Page not found';

            $this->load->view('layouts/default', 'site/pages/404', $data); // :blush:

        }
    }

    // Get Sub Categories
    public function get_sub_category($cat_id){
        return $this->Sub_Category_model->get_subcategories($cat_id);
    }

    // get single product
    public function single_product($product_slug){

        $where=array('product_slug' => $product_slug);

        $product_id =  $this->common_model->getIdBySlug($where, 'tbl_product');

        $row_pro=$this->common_model->selectByid($product_id,'tbl_product');

        if($row_pro->status==1){
            $old_p_ids=explode(',', $this->session->userdata('product_id'));

            if(!in_array($product_id, $old_p_ids)){
                array_push($old_p_ids, $product_id);
            }

            $old_p_ids=implode(',', $old_p_ids);

            $this->session->set_userdata(array('product_id' => $old_p_ids));


            //$data = array();
            $this->data['page_title'] = ucwords($product_slug);
            $this->data['current_page'] = ucwords($product_slug);
            $this->data['product'] = $row_pro;

            $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

            $where= array('user_id' => $user_id, 'product_id' => $product_id);

            $review=$this->common_model->selectByids($where, 'tbl_rating');

            if(!empty($review)){

                $where= array('type' => 'review', 'parent_id' => $review[0]->id);

                $images=$this->common_model->selectByids($where, 'tbl_product_images');

                array_push($review, $images);

                $this->data['my_review'] = $review;
            }
            else{
                $this->data['my_review']=array();
            }

            $this->data['product_rating'] = $this->api_model->get_product_review($product_id);

            $where=array('category_id' => $this->data['product']->category_id, 'sub_category_id' => $this->data['product']->sub_category_id, 'id !=' => $this->data['product']->id);

            $this->data['related_products'] = $this->common_model->selectByids($where,'tbl_product');

            $this->data['sharing_img'] = base_url('assets/images/products/'.$this->data['product']->featured_image);

            $this->Product_model->_set_view($this->data['product']->id);
			$this->data['view'] = 'site/pages/single_product';

			$this->load->view('layouts/default', $this->data); // :blush:

        }
        else{
            show_404();
        }
    }


    // get product list by ids

    public function get_products(){

        $product_ids=explode(',', $this->session->userdata('product_id'));

        return $this->common_model->selectByidsIN($product_ids, 'tbl_product');
    }

    //  get wishlist

    public function get_wishlist(){

        if(!check_user_login()){

            redirect('login-register', 'refresh');
        }

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        //$data = array();
        $this->data['page_title'] = 'Wishlist';
        $this->data['current_page'] = 'Wishlist';
        $this->data['wishlist'] = $this->api_model->get_wishlist($user_id);

        $this->load->view('layouts/default', 'site/pages/wishlist', $this->data); // :blush:
    }

    public function wishlist_action(){

        if(!check_user_login()){

            echo 'login_now';
        }
        else{
            $this->load->helper("date");

            $product_id=$this->input->post('product_id');

            $user_id=$this->session->userdata('GSM_FUS_UserId');

            $where = array('user_id' => $user_id , 'product_id' => $product_id);

            $row=$this->common_model->selectByids($where,'tbl_wishlist');

            $response=array();

            if(!empty($row)){

                $this->common_model->deleteByids($where, 'tbl_wishlist');

                $count=count($this->common_model->selectByids($where,'tbl_wishlist'));

                $response=array('total_items' => strval($count),'success' => '1', 'msg' => $this->lang->line('remove_wishlist'), "is_favorite" => false);

                $message = array('message' => $this->lang->line('remove_wishlist'),'class' => 'alert alert-success');
                $this->session->set_flashdata('wishlist_msg', $message);

            }
            else{
                //perform insertion
                $data_arr = array(
                    'user_id' => $user_id,
                    'product_id' => $product_id,
                    'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
                );

                $data_usr = $this->security->xss_clean($data_arr);

                $last_id = $this->common_model->insert($data_usr, 'tbl_wishlist');

                $count=count($this->common_model->selectByids($where,'tbl_wishlist'));

                $response=array('total_items' => strval($count),'success' => '1', 'msg' => $this->lang->line('add_wishlist'), "is_favorite" => true);

                $message = array('message' => $this->lang->line('add_wishlist'),'class' => 'alert alert-success');
                $this->session->set_flashdata('wishlist_msg', $message);
            }

            echo json_encode($response);
        }
    }

    // product quick preview
    public function quick_view(){

        $product_id=$this->input->post('product_id');

        $row=$this->common_model->selectByid($product_id,'tbl_product');

        $title=$desc=$old_price=$price=$size_view='';

        if(strlen($row->product_title) > 20){
            $title=substr(stripslashes($row->product_title), 0, 20).'...';  
        }else{
            $title=$row->product_title;
        }

        if(strlen($row->product_desc) > 300){
            $desc=substr(stripslashes($row->product_desc), 0, 300);  
        }else{
            $desc=$row->product_desc;
        }

        // for offers
        $data_ofr=$this->calculate_offer($row->offer_id,$row->product_mrp);

        $arr_ofr=json_decode($data_ofr);

        if($row->product_mrp > $arr_ofr->selling_price){
          $price='<span class="old-price">'.CURRENCY_CODE.' '.$row->product_mrp.'</span> 
                                            <span class="new-price">'.CURRENCY_CODE.' '.$arr_ofr->selling_price.'</span>';
        }
        else{
          $price='<span class="new-price">'.CURRENCY_CODE.' '.$row->product_mrp.'</span>';
        }

        $img_file=$this->_create_thumbnail('assets/images/products/',$row->product_slug,$row->featured_image,250,250);

        $img_file_sm=$this->_create_thumbnail('assets/images/products/',$row->product_slug,$row->featured_image,100,100);

        $full_img='<div id="quick_'.$row->id.'" class="tab-pane fade in active">
                          <div class="modal-img img-full"> <img src="'.base_url().$img_file.'" alt=""> </div>
                        </div>';

        $thumb_img='<li class="active img_click"><a data-toggle="tab" href="#quick_'.$row->id.'"><img src="'.base_url().$img_file_sm.'" alt=""></a></li>';


        $img_file2=$this->_create_thumbnail('assets/images/products/',$row->id,$row->featured_image2,250,250);

        $img_file2_sm=$this->_create_thumbnail('assets/images/products/',$row->id,$row->featured_image2,100,100);

        $full_img.='<div id="featured_img" class="tab-pane fade">
                          <div class="modal-img img-full"> <img src="'.base_url().$img_file2.'" alt=""> </div>
                        </div>';

        $thumb_img.='<li class="img_click"><a data-toggle="tab" href="#featured_img"><img src="'.base_url().$img_file2_sm.'" alt=""></a></li>';


        // get gallery data

        $where = array('parent_id' => $product_id,'type' => 'product');

        $row_img=$this->common_model->selectByids($where,'tbl_product_images');

        $i=1;
        foreach ($row_img as $key => $value) {

            $img_big=$this->_create_thumbnail('assets/images/products/gallery/',$value->id,$value->image_file,250,250);

            $img_small=$this->_create_thumbnail('assets/images/products/gallery/',$value->id,$value->image_file,100,100);

            $full_img.='<div id="quick_gallery_'.$key.'" class="tab-pane fade">
                          <div class="modal-img img-full"> <img src="'.base_url().$img_big.'" alt=""> </div>
                        </div>';

            $thumb_img.='<li class="img_click"><a data-toggle="tab" href="#quick_gallery_'.$key.'"><img src="'.base_url().$img_small.'" alt=""></a></li>';

        }



        $response['status']=1;

        $response['html_code']='<div class="modal-details">
                                  <div class="row"> 
                                    <div class="col-md-5 col-sm-5"> 
                                      <div class="tab-content" style="overflow:hidden">
                                        '.$full_img.'
                                      </div>
                                      <div class="modal-product-tab">
                                        <ul class="modal-tab-menu-active">
                                          '.$thumb_img.'
                                        </ul>
                                      </div>
                                    </div>
                                    <div class="col-md-7 col-sm-7">
                                      <div class="product-info">
                                        <h2>'.$title.'</h2>
                                        <div class="product-price">'.$price.'</div>
                                        <div class="cart-description">'.$desc.'</div>
                                        <div class="social-share" style="display:none">
                                          <h3>Share this Product</h3>
                                          <ul class="socil-icon2">
                                            <li><a href="http://www.facebook.com/sharer/sharer.php?u=http://www.viaviweb.in/envato/cc/live_tv_demo/home.php"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>';

        echo json_encode($response);

    }

    public function cart_action(){

        $response=array();

        if(!check_user_login()){
            echo 'login_now';
        }
        else{

            $product_id=$this->input->post('product_id');    

            $where = array('id' => $product_id);

            $row=$this->common_model->selectByids($where,'tbl_product');

            if($row){

                $row=$row[0];

                $title=$old_price=$price=$size_view='';

                if(strlen($row->product_title) > 40){
                    $title=substr(stripslashes($row->product_title), 0, 40).'...';  
                }else{
                    $title=$row->product_title;
                }

                if($row->you_save_amt!='0'){
                  $price='<span class="new-price">'.CURRENCY_CODE.' '.$row->selling_price.'</span> 
                        <span class="old-price">'.CURRENCY_CODE.' '.$row->product_mrp.'</span>';
                }
                else{
                  $price='<span class="new-price">'.CURRENCY_CODE.' '.$row->product_mrp.'</span>';
                }

                $img_file=$this->_create_thumbnail('assets/images/products/',$row->product_slug,$row->featured_image,250,250);


                if($row->product_size !=''){

                    $size=$selected_size='';
                    $i=1;
                    foreach (explode(',', $row->product_size) as $key => $value) {
                        if($i==1){
                            $selected_size=$value;
                            $size.='<div class="radio_btn selected" data-value="'.$value.'">'.$value.'</div>';
                            $i=0;
                        }
                        else{
                            $size.='<div class="radio_btn" data-value="'.$value.'">'.$value.'</div>';
                        }
                    }

                    $size_view.='<br/>
                                  <div class="clearfix"></div>
                                  <p style="font-weight: 600;margin-bottom:0px">Select Size</p>
                                  <div class="radio-group">
                                    '.$size.'
                                    <br/>
                                    <input type="hidden" id="radio-value" name="product_size" value="'.$selected_size.'" />
                                  </div><br/>';
                }

                $preview_url='';

                if(isset($_SERVER['HTTP_REFERER']))
                { 
                    $preview_url=str_replace(base_url().'site/register','',$_SERVER['HTTP_REFERER']);
                }

                $response['status']=1;
                $response['html_code']='<div class="modal-details">
                                        <div class="row"> 
                                          <div class="col-md-12 col-sm-12">
                                            <div class="product-info">
                                              
                                              <div class="col-md-3 col-sm-3 col-xs-12">
                                                <img src="'.base_url().$img_file.'" />
                                              </div>
                                              <div class="col-md-9 col-sm-9 col-xs-12">
                                                <h3>'.$title.'</h3>
                                                <div class="product-price">'.$price.'</div>
                                                <div class="add-to-cart quantity">
                                                    <form class="add-quantity" action="'.site_url('site/add_to_cart').'" method="post" id="cartForm">
                                                     <input type="hidden" name="preview_url" value="'.$preview_url.'">
                                                      <input type="hidden" name="product_id" value="'.$product_id.'" />
                                                      <div class="quantity modal-quantity">
                                                        <label>Quantity</label>
                                                        <input type="number" min="1" max="'.$row->max_unit_buy.'" value="1" onkeypress="return isNumberKey(event)" name="product_qty">
                                                      </div>
                                                      '.$size_view.'
                                                      <div class="add-to-link btn_add_cart" style="clear:both">
                                                        <button class="form-button" data-text="add to cart">add to cart</button>
                                                      </div>
                                                    </form>
                                                  </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>';
            }
            echo json_encode($response);
        }
        
    }

    // cart activities
    public function add_to_cart(){

        $user_id=0;

        if(isUserLoggedIn()==0){
            $message=array('success' => '0','msg' => 'Login required!!!');
        }
        else{
			$CI = &get_instance();
			$user_id = $CI->session->userdata('GSM_FUS_UserId');
            $this->load->helper("date");

            if($this->input->post('product_id')){
                if (!$this->check_cart($this->input->post('product_id'), $user_id)) {

                    $data_arr = array(
                        'product_id' => $this->input->post('product_id'),
                        'user_id' => $user_id,
                        'product_qty' => $this->input->post('product_qty'),
                        'product_size' => $this->input->post('product_size'),
                        'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
                    );

                    $data_usr = $this->security->xss_clean($data_arr);

                    $cart_id = $this->common_model->insert($data_usr, 'tbl_cart');

                    // $message = array('message' => $this->lang->line('add_cart'),'class' => 'success');

                    $message=array('success' => '1','msg' => $this->lang->line('add_cart'));
                    
                    // $this->session->set_flashdata('cart_msg', $message);

                }
                else{

                    $data_arr = array(
                        'product_qty' => $this->input->post('product_qty'),
                        'product_size' => $this->input->post('product_size'),
                        'last_update' => strtotime(date('d-m-Y h:i:s A',now()))
                    );

                    $data_usr = $this->security->xss_clean($data_arr);

                    $where = array('product_id ' => $this->input->post('product_id'), 'user_id' => $user_id);

                    $updated_id=$this->common_model->updateByids($data_usr, $where,'tbl_cart');

                    // $message = array('message' => $this->lang->line('update_cart'),'class' => 'success');

                    $message=array('success' => '1','msg' => $this->lang->line('update_cart'));
                    
                    // $this->session->set_flashdata('cart_msg', $message);
                }

                $where = array('user_id' => $user_id , 'product_id' => $this->input->post('product_id'));

                $this->common_model->deleteByids($where, 'tbl_wishlist');

                //redirect($this->input->post('preview_url'), 'refresh');
            }
            else{
                //redirect('/', 'refresh');
            }
        }

        echo json_encode($message);

    }

    public function update_cart(){

        $this->load->helper("date");

        $data_arr = array(
            'product_qty' => $this->input->post('product_qty'),
            'last_update' => strtotime(date('d-m-Y h:i:s A',now()))
        );

        $data_usr = $this->security->xss_clean($data_arr);

        $where = array('id' => $this->input->post('cart_id'));

        $updated_id=$this->common_model->updateByids($data_usr, $where,'tbl_cart');

        $message = array('message' => $this->lang->line('update_cart'),'class' => 'success');
        
        $this->session->set_flashdata('cart_msg', $message);

        redirect('my-cart', 'refresh');
    }

    public function remove_cart($id){

        $where = array('id ' => $id);

        $this->common_model->deleteByids($where, 'tbl_cart');

        $message=array('success' => '1','msg' => $this->lang->line('remove_cart'));
                
        echo json_encode($message);
    }

    public function get_cart($limit=0){

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';
        return $this->common_model->get_cart($user_id,'','DESC',$limit);
    }

    public function my_cart(){

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        if($user_id==0){
            redirect('login-register', 'refresh');
        }

        $this->data['page_title'] = 'My Cart';
        $this->data['current_page'] = 'My Cart';
        $this->data['my_cart'] = $this->common_model->get_cart($user_id);
		$this->data['view'] = 'site/pages/my_cart';
        $this->load->view('layouts/default', $this->data); // :blush:

    }

    public function checkout(){

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        if($user_id==0){
            redirect('login-register', 'refresh');
        }

        $this->data['page_title'] = 'Checkout';
        $this->data['current_page'] = 'Checkout';
        $this->data['my_cart'] = $this->common_model->get_cart($user_id);
        $this->data['addresses'] = $this->common_model->get_addresses($user_id);

        if(!empty($this->data['my_cart'])) {
			$this->data['view'] = 'site/pages/checkout';
        	$this->load->view('layouts/default', $this->data); // :blush:

		}
        else
            redirect('/', 'refresh');


    }

    // todays deals products

    public function todays_deals_list(){


        $data['category_list'] = $this->common_model->selectWhere('tbl_category', array('status' => '1'), 'DESC', 'id');

        $this->load->library('pagination');
        
        $row = $this->api_model->todays_deals();

        if(empty($row))
        {
            $this->load->view('layouts/default', 'site/pages/no_products', $data); // :blush:
            return;
        }

        $data['page_title'] = "Today's Deals";
        $data['current_page'] = "Today's Deals";

        $config = array();
        $config["base_url"] = base_url() . 'todays-deals/';
        $config["per_page"] = 12;

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 1;

        $page=($page-1) * $config["per_page"];

        $page2 = ($this->uri->segment(2)) ? $this->uri->segment(2) : 1;

        if($page2 != 1){
            $page2-=1;
        }

        $row_all=$this->api_model->todays_deals();

        $brands=array();

        foreach ($row_all as $key => $value) {
            $brands[]=$value->brand_id;
        }

        $data['brand_count_items']=array_count_values($brands);

        $data['brand_list']=$this->common_model->selectByidsIN(array_unique($brands), 'tbl_brands');

        $row=array();

        
        if(!empty($this->input->get('sortByBrand'))){

            $brands_ids=implode(',', $this->input->get('sortByBrand'));

            $price_arr=array();

            foreach ($row_all as $key => $value) {

                $price=$value->selling_price;

                array_push($price_arr, $price);

                $product_ids[]=$value->id;

            }

            // $where
            $row_all=$this->common_model->selectByidsIN($product_ids, 'tbl_product','','',$brands_ids);
            $row=$this->common_model->selectByidsIN($product_ids, 'tbl_product',$config["per_page"], $page,$brands_ids);


            $data['price_min']=min($price_arr);
            $data['price_max']=max($price_arr);

            if($this->input->get('price_filter')!='' && $this->input->get('sort')!=''){

                $price_filter=(explode('-', $this->input->get('price_filter')));

                $min_price=$price_filter[0];
                $max_price=$price_filter[1];

                $row_all=$this->api_model->productsFilters($product_ids, 'tbl_product','', '', $min_price,$max_price,'',$this->input->get('sort'));

                $row=$this->api_model->productsFilters($product_ids, 'tbl_product',$config["per_page"], $page,$min_price,$max_price,$brands_ids,$this->input->get('sort'));


            }
            else if($this->input->get('price_filter')!='' && $this->input->get('sort')==''){

                $price_filter=(explode('-', $this->input->get('price_filter')));

                $min_price=$price_filter[0];
                $max_price=$price_filter[1];

                $row_all=$this->api_model->productsFilters($product_ids, 'tbl_product','', '', $min_price,$max_price,'',$this->input->get('sort'));

                $row=$this->api_model->productsFilters($product_ids, 'tbl_product',$config["per_page"], $page,$min_price,$max_price,$brands_ids);

            }
            else if($this->input->get('price_filter')=='' && $this->input->get('sort')!=''){

                $row_all=$this->api_model->productsFilters($product_ids, 'tbl_product','', '', '','','',$this->input->get('sort'));

                $row=$this->api_model->productsFilters($product_ids, 'tbl_product',$config["per_page"], $page,'','',$brands_ids,$this->input->get('sort'));
            }
        }
        else if(!empty($this->input->get('sort')))
        {

            $row_all=$this->api_model->todays_deals('','','',$this->input->get('sort'));

            $price_arr=array();

            foreach ($row_all as $key => $value) {

                $price=$value->selling_price;
                array_push($price_arr, $price);

                $product_ids[]=$value->id;

            }

            $data['price_min']=min($price_arr);  
            $data['price_max']=max($price_arr);

            $row=$this->api_model->todays_deals($config["per_page"], $page,'',$this->input->get('sort'));

            if($this->input->get('price_filter')!=''){

                $price_filter=(explode('-', $this->input->get('price_filter')));

                $min_price=$price_filter[0];
                $max_price=$price_filter[1];

                $row_all=$this->api_model->productsFilters($product_ids, 'tbl_product','', '', $min_price,$max_price,'',$this->input->get('sort'));

                $row=$this->api_model->productsFilters($product_ids, 'tbl_product',$config["per_page"], $page,$min_price,$max_price,'',$this->input->get('sort'));

            }

        }
        else{

            $price_arr=array();

            foreach ($row_all as $key => $value) {

                $price=$value->selling_price;
                array_push($price_arr, $price);

                $product_ids[]=$value->id;

            }

            $data['price_min']=min($price_arr);  
            $data['price_max']=max($price_arr);

            $row=$this->common_model->selectByidsIN($product_ids, 'tbl_product',$config["per_page"], $page);

            if($this->input->get('price_filter')!=''){
                $price_filter=(explode('-', $this->input->get('price_filter')));

                $min_price=$price_filter[0];
                $max_price=$price_filter[1];

                $row_all=$this->api_model->productsFilters($product_ids, 'tbl_product','', '',$min_price,$max_price);

                $row=$this->api_model->productsFilters($product_ids, 'tbl_product',$config["per_page"], $page,$min_price,$max_price);
            }

        }

        $config["total_rows"] = count($row_all);

        // echo count($row_all);

        $config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
         
        $config['full_tag_open'] = '<ul class="page-number">';
        $config['full_tag_close'] = '</ul>';
         
        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
         
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
         
        $config['next_link'] = '';
        $config['next_tag_open'] = '<span class="nextlink">';
        $config['next_tag_close'] = '</span>';

        $config['prev_link'] = '';
        $config['prev_tag_open'] = '<span class="prevlink">';
        $config['prev_tag_close'] = '</span>';

        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';

        $config['num_tag_open'] = '<li style="margin:3px">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data['product_list']=$row; 

        $data["show_result"] = 'Showing '.$page2.'–'.count($row).' of '.count($row_all).' results';

        $this->load->view('layouts/default', 'site/pages/products', $data); // :blush:
    }

    // banner wise products
    public function banner_products(){

        $data['category_list'] = $this->common_model->selectWhere('tbl_category', array('status' => '1'), 'DESC', 'id');

        $this->load->library('pagination');
        
        $slug =  $this->uri->segment(2);

        $where = array('banner_slug ' => $slug);

        $data['banner_info'] = $this->common_model->selectByids($where, 'tbl_banner');

        if(empty($data['banner_info']))
        {
            show_404();
        }

        $data['page_title'] = $data['banner_info'][0]->banner_title;
        $data['current_page'] = ucwords($data['banner_info'][0]->banner_title);

        $data['sharing_img'] = base_url('assets/images/banner/'.$data['banner_info'][0]->banner_image);

        if($data['banner_info'][0]->product_ids!=''){
            $product_ids=explode(',',$data['banner_info'][0]->product_ids);

            $config = array();
            $config["base_url"] = base_url() . 'banners/'.$slug;
            $config["per_page"] = 12;

            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

            $page=($page-1) * $config["per_page"];

            $page2 = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

            if($page2 != 1){
                $page2-=1;
            }

            $row_all=$this->api_model->products_by_banner($data['banner_info'][0]->id);

            $brands=array();

            foreach ($row_all as $key => $value) {
                $brands[]=$value->brand_id;
            }

            $data['brand_count_items']=array_count_values($brands);

            $data['brand_list']=$this->common_model->selectByidsIN(array_unique($brands), 'tbl_brands');

            $row=array();

            if(!empty($this->input->get('sortByBrand'))){

                $brands_ids=implode(',', $this->input->get('sortByBrand'));

                // $where
                $row_all=$this->common_model->selectByidsIN($product_ids, 'tbl_product','','',$brands_ids);
                $row=$this->common_model->selectByidsIN($product_ids, 'tbl_product',$config["per_page"], $page,$brands_ids);

                $price_arr=array();

                foreach ($row_all as $key => $value) {

                    $price=$value->selling_price;

                    array_push($price_arr, $price);

                }

                $data['price_min']=min($price_arr);
                $data['price_max']=max($price_arr);

                if($this->input->get('price_filter')!='' && $this->input->get('sort')!=''){

                    $price_filter=(explode('-', $this->input->get('price_filter')));

                    $min_price=$price_filter[0];
                    $max_price=$price_filter[1];

                    $row=$this->api_model->products_by_banner($data['banner_info'][0]->id, $config["per_page"], $page,$brands_ids,$min_price,$max_price,$this->input->get('sort'));

                    $row_all=$this->api_model->products_by_banner($data['banner_info'][0]->id, '','',$brands_ids,$min_price,$max_price);


                }
                else if($this->input->get('price_filter')!='' && $this->input->get('sort')==''){

                    $price_filter=(explode('-', $this->input->get('price_filter')));

                    $min_price=$price_filter[0];
                    $max_price=$price_filter[1];

                    $row=$this->api_model->products_by_banner($data['banner_info'][0]->id, $config["per_page"], $page,$brands_ids,$min_price,$max_price);

                    $row_all=$this->api_model->products_by_banner($data['banner_info'][0]->id, '','',$brands_ids,$min_price,$max_price);

                }
                else if($this->input->get('price_filter')=='' && $this->input->get('sort')!=''){

                    $row=$this->api_model->products_by_banner($data['banner_info'][0]->id, $config["per_page"], $page,$brands_ids,'','',$this->input->get('sort'));

                    $row_all=$this->api_model->products_by_banner($data['banner_info'][0]->id, '','',$brands_ids,'','');
                }
            }
            else if(!empty($this->input->get('sort')))
            {

                $row_all=$this->api_model->products_by_banner($data['banner_info'][0]->id,'','','','','',$this->input->get('sort'));

                if(!empty($row_all)){
                    $brands=array();

                    foreach ($row_all as $key => $value) {
                        $brands[]=$value->brand_id;
                    }

                    $data['brand_count_items']=array_count_values($brands);

                    $data['brand_list']=$this->common_model->selectByidsIN(array_unique($brands), 'tbl_brands');

                    
                    $price_arr=array();

                    foreach ($row_all as $key => $value) {

                        $price=$value->selling_price;

                        array_push($price_arr, $price);

                    }

                    $data['price_min']=min($price_arr);  
                    $data['price_max']=max($price_arr); 

                    $row=$this->api_model->products_by_banner($data['banner_info'][0]->id,$config["per_page"], $page,'','','',$this->input->get('sort'));

                    if($this->input->get('price_filter')!=''){

                        $price_filter=(explode('-', $this->input->get('price_filter')));

                        $min_price=$price_filter[0];
                        $max_price=$price_filter[1];

                        $brands_ids=implode(',', $this->input->get('sortByBrand'));

                        $row=$this->api_model->products_by_banner($data['banner_info'][0]->id, $config["per_page"], $page,$brands_ids,$min_price,$max_price,$this->input->get('sort'));

                        $row_all=$this->api_model->products_by_banner($data['banner_info'][0]->id, '','',$brands_ids,$min_price,$max_price);
                    }
                    
                }
                else{
                    $this->load->view('layouts/default', 'site/pages/no_products'); // :blush:
                    return;
                }
            }
            else{
                
                $price_arr=array();

                foreach ($row_all as $key => $value) {

                    $price=$value->selling_price;
                    array_push($price_arr, $price);

                }

                $data['price_min']=min($price_arr);
                $data['price_max']=max($price_arr);

                $row=$this->api_model->products_by_banner($data['banner_info'][0]->id,$config["per_page"], $page);

                if($this->input->get('price_filter')!=''){
                    $price_filter=(explode('-', $this->input->get('price_filter')));

                    $min_price=$price_filter[0];
                    $max_price=$price_filter[1];

                    $row_all=$this->api_model->products_by_banner($data['banner_info'][0]->id,'', '',$min_price,$max_price);

                    $row=$this->api_model->products_by_banner($data['banner_info'][0]->id,$config["per_page"], $page,'',$min_price,$max_price);
                }

            }

            $config["total_rows"] = count($row_all);

            // echo count($row_all);

            $config['num_links'] = 4;
            $config['use_page_numbers'] = TRUE;
            $config['reuse_query_string'] = TRUE;
             
            $config['full_tag_open'] = '<ul class="page-number">';
            $config['full_tag_close'] = '</ul>';
             
            $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
             
            $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
             
            $config['next_link'] = '';
            $config['next_tag_open'] = '<span class="nextlink">';
            $config['next_tag_close'] = '</span>';

            $config['prev_link'] = '';
            $config['prev_tag_open'] = '<span class="prevlink">';
            $config['prev_tag_close'] = '</span>';

            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';

            $config['num_tag_open'] = '<li style="margin:3px">';
            $config['num_tag_close'] = '</li>';

            $this->pagination->initialize($config);

            $data["links"] = $this->pagination->create_links();

            $data['product_list']=$row; 

            $data["show_result"] = 'Showing '.$page2.'–'.count($row).' of '.count($row_all).' results';

            $this->load->view('layouts/default', 'site/pages/products', $data); // :blush:
        }
        else{
            $this->load->view('layouts/default', 'site/pages/no_products'); // :blush:
            return;
        }

    }

    // offer wise products

    public function offer_products(){

        $data['category_list'] = $this->common_model->selectWhere('tbl_category', array('status' => '1'), 'DESC', 'id');

        $this->load->library('pagination');
        
        $slug =  $this->uri->segment(2);

        $where = array('offer_slug' => $slug);

        $data['offer_info'] = $this->common_model->selectByids($where, 'tbl_offers');

        if(empty($data['offer_info']))
        {
            $this->load->view('layouts/default', 'site/pages/404'); // :blush:
            return;
        }

        $data['page_title'] = $data['offer_info'][0]->offer_title;
        $data['current_page'] = ucwords($data['offer_info'][0]->offer_title);

        $data['sharing_img'] = base_url('assets/images/offers/'.$data['offer_info'][0]->offer_image);

        $total_rows=count($this->api_model->products_by_offer($data['offer_info'][0]->id));

        $config = array();
        $config["base_url"] = base_url() . 'offers/'.$slug;
        $config["total_rows"] = $total_rows;
        $config["per_page"] = 12;

        $config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
         
        $config['full_tag_open'] = '<ul class="page-number">';
        $config['full_tag_close'] = '</ul>';
         
        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
         
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
         
        $config['next_link'] = '';
        $config['next_tag_open'] = '<span class="nextlink">';
        $config['next_tag_close'] = '</span>';

        $config['prev_link'] = '';
        $config['prev_tag_open'] = '<span class="prevlink">';
        $config['prev_tag_close'] = '</span>';

        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';

        $config['num_tag_open'] = '<li style="margin:3px">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        $page=($page-1) * $config["per_page"];

        $page2 = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        $row=array();

        if(!empty($this->input->get('sortByBrand')))
        {
            // when having sorting by brands
            $row_all=$this->api_model->products_by_offer($data['offer_info'][0]->id);

            if(!empty($row_all))
            {
                $brands=array();

                foreach ($row_all as $key => $value) {
                    $brands[]=$value->brand_id;
                }

                $data['brand_count_items']=array_count_values($brands);

                $data['brand_list']=$this->common_model->selectByidsIN(array_unique($brands), 'tbl_brands');

                $brands_ids=implode(',', $this->input->get('sortByBrand'));

                $row=$this->api_model->products_by_offer($data['offer_info'][0]->id, $config["per_page"], $page, $brands_ids);

                $price_arr=array();

                foreach ($row_all as $key => $value) {

                    $price=$value->selling_price;

                    array_push($price_arr, $price);

                }

                $data['price_min']=min($price_arr);  
                $data['price_max']=max($price_arr);

                if($this->input->get('price_filter')!='' && $this->input->get('sort')!=''){

                    $price_filter=(explode('-', $this->input->get('price_filter')));

                    $min_price=$price_filter[0];
                    $max_price=$price_filter[1];

                    $row=$this->api_model->products_by_offer($data['offer_info'][0]->id, $config["per_page"], $page,$brands_ids,$min_price,$max_price,$this->input->get('sort'));

                    $row_all=$this->api_model->products_by_offer($data['offer_info'][0]->id, '','',$brands_ids,$min_price,$max_price);


                }
                else if($this->input->get('price_filter')!='' && $this->input->get('sort')==''){

                    $price_filter=(explode('-', $this->input->get('price_filter')));

                    $min_price=$price_filter[0];
                    $max_price=$price_filter[1];

                    $row=$this->api_model->products_by_offer($data['offer_info'][0]->id, $config["per_page"], $page,$brands_ids,$min_price,$max_price);

                    $row_all=$this->api_model->products_by_offer($data['offer_info'][0]->id, '','',$brands_ids,$min_price,$max_price);

                }
                else if($this->input->get('price_filter')=='' && $this->input->get('sort')!=''){

                    $row=$this->api_model->products_by_offer($data['offer_info'][0]->id, $config["per_page"], $page,$brands_ids,'','',$this->input->get('sort'));

                    $row_all=$this->api_model->products_by_offer($data['offer_info'][0]->id, '','',$brands_ids,'','');
                }

            }
            else
            {
                $this->load->view('layouts/default', 'site/pages/no_products'); // :blush:
                return;
            }

        }
        else if(!empty($this->input->get('sort')))
        {

            $row_all=$this->api_model->products_by_offer($data['offer_info'][0]->id,'','','','','',$this->input->get('sort'));

            if(!empty($row_all)){
                $brands=array();

                foreach ($row_all as $key => $value) {
                    $brands[]=$value->brand_id;
                }

                $data['brand_count_items']=array_count_values($brands);

                $data['brand_list']=$this->common_model->selectByidsIN(array_unique($brands), 'tbl_brands');

                $row=$this->api_model->products_by_offer($data['offer_info'][0]->id, $config["per_page"], $page,'','','',$this->input->get('sort'));

                $price_arr=array();

                foreach ($row_all as $key => $value) {

                    $price=$value->selling_price;

                    array_push($price_arr, $price);

                }

                $data['price_min']=min($price_arr);  
                $data['price_max']=max($price_arr); 

                if($this->input->get('price_filter')!=''){

                    $price_filter=(explode('-', $this->input->get('price_filter')));

                    $min_price=$price_filter[0];
                    $max_price=$price_filter[1];

                    $brands_ids=implode(',', $this->input->get('sortByBrand'));

                    $row=$this->api_model->products_by_offer($data['offer_info'][0]->id, $config["per_page"], $page,$brands_ids,$min_price,$max_price,$this->input->get('sort'));

                    $row_all=$this->api_model->products_by_offer($data['offer_info'][0]->id, '','',$brands_ids,$min_price,$max_price);
                }
            }
            else{
                $this->load->view('layouts/default', 'site/pages/no_products'); // :blush:
                return;
            }
        }
        else
        {
            $row_all=$this->api_model->products_by_offer($data['offer_info'][0]->id);

            if(!empty($row_all))
            {
                $brands=array();

                foreach ($row_all as $key => $value) {
                    $brands[]=$value->brand_id;
                }

                $data['brand_count_items']=array_count_values($brands);

                $data['brand_list']=$this->common_model->selectByidsIN(array_unique($brands), 'tbl_brands');

                $row=$this->api_model->products_by_offer($data['offer_info'][0]->id, $config["per_page"], $page);

                $price_arr=array();

                foreach ($row_all as $key => $value) {

                    $price=$value->selling_price;

                    array_push($price_arr, $price);

                }

                $data['price_min']=min($price_arr);  
                $data['price_max']=max($price_arr);

                if($this->input->get('price_filter')!=''){

                    $price_filter=(explode('-', $this->input->get('price_filter')));

                    $min_price=$price_filter[0];
                    $max_price=$price_filter[1];

                    $row=$this->api_model->products_by_offer($data['offer_info'][0]->id, $config["per_page"], $page,'',$min_price,$max_price);

                    $row_all=$this->api_model->products_by_offer($data['offer_info'][0]->id, '','','',$min_price,$max_price);

                }

            }
            else
            {
                $this->load->view('layouts/default', 'site/pages/no_products'); // :blush:
                return;
            }
        }

        $config["total_rows"] = count($row_all);

        // echo count($row_all);

        $config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
         
        $config['full_tag_open'] = '<ul class="page-number">';
        $config['full_tag_close'] = '</ul>';
         
        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
         
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
         
        $config['next_link'] = '';
        $config['next_tag_open'] = '<span class="nextlink">';
        $config['next_tag_close'] = '</span>';

        $config['prev_link'] = '';
        $config['prev_tag_open'] = '<span class="prevlink">';
        $config['prev_tag_close'] = '</span>';

        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';

        $config['num_tag_open'] = '<li style="margin:3px">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data['product_list']=$row;

        $data["show_result"] = 'Showing '.$page2.'–'.count($row).' of '.count($row_all).' results';

        $this->load->view('layouts/default', 'site/pages/products', $data); // :blush:
    }


    // category and sub category products

    public function cat_sub_product($category_slug, $sub_category_slug=''){
        
        $data['category_list'] = $this->common_model->selectWhere('tbl_category', array('status' => '1'), 'DESC', 'id');

        if($sub_category_slug!=''){

            $where = array('category_slug ' => $category_slug);

            $category_id=$this->common_model->selectByidsParam($where, 'tbl_category', 'id');

            $where = array('sub_category_slug ' => $sub_category_slug);

            $sub_category_id=$this->common_model->selectByidsParam($where, 'tbl_sub_category', 'id');

            if($category_id!='' && $sub_category_id!=''){
                $data['page_title'] = $category_slug.' | '.$sub_category_slug;
                $data['current_page'] = ucwords($category_slug.' | '.$sub_category_slug);

                $data['sharing_img'] = base_url('assets/images/sub_category/'.$this->common_model->selectByidsParam(array('id' => $sub_category_id), 'tbl_sub_category', 'sub_category_image'));

                $config = array();
                $config["base_url"] = base_url().'category/'.$category_slug.'/'.$sub_category_slug;
                
                $config["per_page"] = 12;

                $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 1;

                $page=($page-1) * $config["per_page"];

                $page2 = ($this->uri->segment(4)) ? $this->uri->segment(4) : 1;

                $row=array();

                if(!empty($this->input->get('sortByBrand'))){

                    $row_all=$this->api_model->productList_cat_sub($category_id, $sub_category_id);

                    if(!empty($row_all)){

                        $brands=array();

                        foreach ($row_all as $key => $value) {
                            $brands[]=$value->brand_id;
                        }

                        $data['brand_count_items']=array_count_values($brands);

                        $data['brand_list']=$this->common_model->selectByidsIN(array_unique($brands), 'tbl_brands');

                        $brands_ids=implode(',', $this->input->get('sortByBrand'));

                        $row=$this->api_model->productList_cat_sub($category_id, $sub_category_id, $config["per_page"], $page, $brands_ids);

                        $data['product_list']=$row; 

                        $price_arr=array();

                        foreach ($row_all as $key => $value) {

                            $price=$value->selling_price;

                            array_push($price_arr, $price);

                        }

                        $min=min($price_arr);
                        $max=max($price_arr);

                        $data['price_min']=$min;  
                        $data['price_max']=$max;

                        $row_all=$this->api_model->productList_cat_sub($category_id, $sub_category_id,'','',$brands_ids);

                        if($this->input->get('price_filter')!='' && $this->input->get('sort')!=''){

                            $price_filter=(explode('-', $this->input->get('price_filter')));

                            $min_price=$price_filter[0];
                            $max_price=$price_filter[1];

                            $row_all=$this->api_model->productList_cat_sub($category_id, $sub_category_id,'','',$brands_ids,$min_price,$max_price);

                            $row=$this->api_model->productList_cat_sub($category_id, $sub_category_id, $config["per_page"], $page, $brands_ids,$min_price,$max_price,$this->input->get('sort'));

                        }
                        else if($this->input->get('price_filter')!='' && $this->input->get('sort')==''){

                            $price_filter=(explode('-', $this->input->get('price_filter')));

                            $min_price=$price_filter[0];
                            $max_price=$price_filter[1];

                            $row_all=$this->api_model->productList_cat_sub($category_id, $sub_category_id,'','',$brands_ids,$min_price,$max_price);

                            $row=$this->api_model->productList_cat_sub($category_id, $sub_category_id, $config["per_page"], $page, $brands_ids,$min_price,$max_price);

                        }
                        else if($this->input->get('price_filter')=='' && $this->input->get('sort')!=''){
                            $row_all=$this->api_model->productList_cat_sub($category_id, $sub_category_id,'','',$brands_ids);

                            $row=$this->api_model->productList_cat_sub($category_id, $sub_category_id, $config["per_page"], $page, $brands_ids,'','',$this->input->get('sort'));
                        }

                    }
                    else{
                        $this->load->view('layouts/default', 'site/pages/no_products'); // :blush:
                        return;
                    }
                }
                else if(!empty($this->input->get('sort')))
                {

                    $row_all=$this->api_model->productList_cat_sub($category_id, $sub_category_id,'','','','','',$this->input->get('sort'));

                    if(!empty($row_all)){
                        $brands=array();

                        foreach ($row_all as $key => $value) {
                            $brands[]=$value->brand_id;
                        }

                        $data['brand_count_items']=array_count_values($brands);

                        $data['brand_list']=$this->common_model->selectByidsIN(array_unique($brands), 'tbl_brands');

                        $row=$this->api_model->productList_cat_sub($category_id, $sub_category_id, $config["per_page"], $page,'','','',$this->input->get('sort'));

                        $price_arr=array();

                        foreach ($row_all as $key => $value) {

                            $price=$value->selling_price;

                            array_push($price_arr, $price);

                        }

                        $min=min($price_arr);
                        $max=max($price_arr);

                        $data['price_min']=$min;  
                        $data['price_max']=$max; 

                        if($this->input->get('price_filter')!=''){

                            $price_filter=(explode('-', $this->input->get('price_filter')));

                            $min_price=$price_filter[0];
                            $max_price=$price_filter[1];

                            $brands_ids=implode(',', $this->input->get('sortByBrand'));

                            $row_all=$this->api_model->productList_cat_sub($category_id, $sub_category_id,'','',$brands_ids,$min_price,$max_price);

                            $row=$this->api_model->productList_cat_sub($category_id, $sub_category_id, $config["per_page"], $page, $brands_ids,$min_price,$max_price,$this->input->get('sort'));

                        }
                    }
                    else{
                        $this->load->view('layouts/default', 'site/pages/no_products'); // :blush:
                        return;
                    }
                }
                else{
                    $row_all=$this->api_model->productList_cat_sub($category_id, $sub_category_id);

                    if(!empty($row_all)){
                        $brands=array();

                        foreach ($row_all as $key => $value) {
                            $brands[]=$value->brand_id;
                        }

                        $data['brand_count_items']=array_count_values($brands);

                        $data['brand_list']=$this->common_model->selectByidsIN(array_unique($brands), 'tbl_brands');

                        $row=$this->api_model->productList_cat_sub($category_id, $sub_category_id, $config["per_page"], $page);

                        $price_arr=array();

                        foreach ($row_all as $key => $value) {

                            $price=$value->selling_price;

                            array_push($price_arr, $price);

                        }

                        $data['price_min']=min($price_arr);  
                        $data['price_max']=max($price_arr); 

                        if($this->input->get('price_filter')!=''){

                            $price_filter=(explode('-', $this->input->get('price_filter')));

                            $min_price=$price_filter[0];
                            $max_price=$price_filter[1];

                            $row=$this->api_model->productList_cat_sub($category_id, $sub_category_id, $config["per_page"], $page,'',$min_price,$max_price);

                            $row_all=$this->api_model->productList_cat_sub($category_id, $sub_category_id, '','','',$min_price,$max_price);

                        }

                        
                    }
                    else{
                        $this->load->view('layouts/default', 'site/pages/no_products'); // :blush:
                        return;
                    }
                }

                $config["total_rows"] = count($row_all);

                // echo count($row_all);

                $config['num_links'] = 4;
                $config['use_page_numbers'] = TRUE;
                $config['reuse_query_string'] = TRUE;
                 
                $config['full_tag_open'] = '<ul class="page-number">';
                $config['full_tag_close'] = '</ul>';
                 
                $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                 
                $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';
                 
                $config['next_link'] = '';
                $config['next_tag_open'] = '<span class="nextlink">';
                $config['next_tag_close'] = '</span>';

                $config['prev_link'] = '';
                $config['prev_tag_open'] = '<span class="prevlink">';
                $config['prev_tag_close'] = '</span>';

                $config['cur_tag_open'] = '<li class="active"><a href="#">';
                $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';

                $config['num_tag_open'] = '<li style="margin:3px">';
                $config['num_tag_close'] = '</li>';

                $this->pagination->initialize($config);

                

                $data["links"] = $this->pagination->create_links();

                $data['product_list']=$row;

                $data["show_result"] = 'Showing '.$page2.'–'.count($row).' of '.count($row_all).' results';

                $this->load->view('layouts/default', 'site/pages/products', $data); // :blush:
            }
            else{
                $this->load->view('layouts/default', 'site/pages/404'); // :blush:
                return;
            }
        }
        else{
            $this->load->view('layouts/default', 'site/pages/404'); // :blush:
            return;
        }

    }

    public function get_cat_sub_product($category_id, $sub_category_id=''){
        return $this->api_model->productList_cat_sub($category_id, $sub_category_id, '10', '0','','','','newest');
    }

    public function product_rating($product_id){

        $res=array();

        $where = array('product_id ' => $product_id);

        if($row_rate = $this->common_model->selectByids($where, 'tbl_rating')){
            foreach ($row_rate as $key => $value) {
                $rate_db[] = $value;
                $sum_rates[] = $value->rating;
            }

            $rate_times = count($rate_db);
            $sum_rates = array_sum($sum_rates);
            $rate_value = $sum_rates/$rate_times;

            $res['rate_times']=strval($rate_times);
            $res['total_rate']=strval($sum_rates);
            $res['rate_avg']=strval(round($rate_value));
        }
        else{
            $res['rate_times']="0";
            $res['total_rate']="0";
            $res['rate_avg']="0";   
        }
        return json_encode($res);
    }
    
    function is_favorite($user_id, $product_id){
        $where = array('user_id ' => $user_id , 'product_id' => $product_id);

        $count=count($this->common_model->selectByids($where,'tbl_wishlist'));

        if($count > 0){
            return true;
        }
        else{
            return false;
        }
    }

    function check_email(){

        $email=$this->input->post('email');  

        $row = $this->common_model->check_email($email);

        if(empty($row)) {
            echo "true";
        }
        else{
            echo "false";
        }
    }

    function sent_code(){

        $this->load->helper("date");

        $email=$this->input->post('email');  

        $random_code=rand(1000,5000);

        $where = array('user_email ' => $email);

        $count=count($this->common_model->selectByids($where,'tbl_verify_code'));

        if($count > 0){

            //perform updation
            $data = array(
                'user_email' => $email,
                'verify_code' => $random_code,
                'created_at' => strtotime(date('d-m-Y h:i:s A',now())),
                'is_verify' => '0',
            );

            $data_usr = $this->security->xss_clean($data);

            $updated_id=$this->common_model->updateByids($data_usr, $where,'tbl_verify_code');
            
        }
        else{
            
            //perform insertion
            $data = array(
                'user_email' => $email,
                'verify_code' => $random_code,
                'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
            );

            $data_usr = $this->security->xss_clean($data);

            $last_id = $this->common_model->insert($data_usr, 'tbl_verify_code');

        }

        $data_arr = array(
            'email' => $email,
            'otp' => $random_code
        );

        $this->email->from($this->app_email, $this->app_name);

        $this->email->to($email); // replace it with receiver mail id

        $subject = $this->app_name.'- Email Verification Code';

        $this->email->subject($subject); // replace it with relevant subject

        $body = $this->load->view('admin/emails/email_verify.php',$data_arr,TRUE);

        $this->email->message($body); 

        if($this->email->send()){
           $res=array('success' => '1','msg' => 'Verification code is successfully sent to your email...'); 
        }
        else{
            $res=array('success' => '0','msg' => 'Sorry email is not sent');
        }

        return json_encode($res);
    }

    function verify_code(){

        $email=$this->input->post('email');

        $code=$this->input->post('code');

        $where = array('user_email' => $email, 'verify_code' => $code, 'is_verify' => '0');

        $count=count($this->common_model->selectByids($where,'tbl_verify_code'));

        if($count > 0){

            $data = array(
                'is_verify' => '1',
            );

            $data_usr = $this->security->xss_clean($data);

            $where = array('user_email' => $email);

            $updated_id=$this->common_model->updateByids($data_usr, $where,'tbl_verify_code');

            echo 'true';
        }
        else{
            echo 'false';
        }
    }

    // login and register form view
    public function login_register_form(){

        if($this->session->userdata('GSM_FUS_UserId')){
            redirect('/', 'refresh');
        }

        $data = array();
        $data['page_title'] = 'Login-Register';
        $data['current_page'] = 'Login-Register';
        $this->load->view('layouts/default', 'site/pages/login_register', $data); // :blush:
    }

    // login form
    public function login(){

        $this->form_validation->set_rules('email', 'Email is required', 'required');  
        $this->form_validation->set_rules('password', 'Password is required', 'required'); 

        if($this->form_validation->run())  
        {
            if($_POST){

                $row_usr=$this->common_model->selectByids(array('user_email' => $this->input->post('email')), 'tbl_users')[0];

                //-- if valid
                if($row_usr){

                    if($row_usr->user_password==md5($this->input->post('password'))){

                        if($row_usr->status=='1'){
                             $data = array(
                                'user_id' => $row_usr->id,
                                'user_name' => $row_usr->user_name,
                                'user_email' =>$row_usr->user_email,
                                'is_user_login' => TRUE
                            );
                            $this->session->set_userdata($data);

                            $message = array('message' => 'Password is invalid !!!','status' => '1','preview_url' => $this->input->post('preview_url'));
                            
                            echo base64_encode(json_encode($message));
                        }
                        else{
                            $message = array('message' => 'Your account is currently deactive !!!','status' => '0');
                            echo base64_encode(json_encode($message));   
                        }
                    }
                    else{

                        $message = array('message' => 'Password is invalid !!!','status' => '0');
                        echo base64_encode(json_encode($message));
                    }
                }else{
                    $message = array('message' => 'Email is not found !!!','status' => '0');
                    echo base64_encode(json_encode($message));
                }
            }
        }
        else  
        {  
            $message = array('message' => 'Enter all required fields !!!','status' => '0');
            echo base64_encode(json_encode($message));
        }
    }

    // registration form
    public function register(){

        $this->form_validation->set_rules('user_name', 'Enter name', 'trim|required');
        $this->form_validation->set_rules('user_email', 'Enter email', 'trim|required');
        $this->form_validation->set_rules('user_phone', 'Enter phone no', 'trim|required');
        $this->form_validation->set_rules('user_password', 'Enter password', 'trim|required');

        if($this->form_validation->run())  
        {
            if($_POST){ 

                $data = array(
                    'user_name'  => $this->input->post('user_name'),
                    'user_email'  => $this->input->post('user_email'),
                    'user_phone'  => $this->input->post('user_phone'),
                    'user_password'  => md5($this->input->post('user_password')),
                    'created_at'  =>  strtotime(date('d-m-Y h:i:s A'))
                );

                $data = $this->security->xss_clean($data);

                if($this->common_model->insert($data, 'tbl_users')){
                    $message = array('message' => $this->lang->line('register_success'),'class' => 'alert alert-success');
                    $this->session->set_flashdata('response_msg', $message);

                }
                else{
                    $message = array('message' => $this->lang->line('register_failed'),'class' => 'alert alert-danger');
                    $this->session->set_flashdata('response_msg', $message);
                }

                redirect('login-register', 'refresh');
            }
        }
        else  
        {  
            $message = array('message' => 'Enter all required fields','class' => 'alert alert-danger');
                $this->session->set_flashdata('response_msg', $message);
            redirect('login-register', 'refresh');
        }
    } 

     // address form
    public function addAddress(){

        $this->load->helper("date");

        $this->form_validation->set_rules('billing_name', 'Enter name', 'trim|required');
        $this->form_validation->set_rules('billing_mobile_no', 'Enter mobile no.', 'trim|required');
        $this->form_validation->set_rules('building_name', 'House no., Building Name', 'trim|required');
        $this->form_validation->set_rules('road_area_colony', 'Enter road, area, colony', 'trim|required');
        $this->form_validation->set_rules('pincode', 'Enter pincode/zipcode', 'trim|required');
        $this->form_validation->set_rules('city', 'Enter city', 'trim|required');
        $this->form_validation->set_rules('district', 'Enter district', 'trim|required');
        $this->form_validation->set_rules('state', 'Enter state', 'trim|required');

        if($this->form_validation->run())  
        {
            if($_POST){ 

                if($row = $this->common_model->get_addresses($this->session->userdata('GSM_FUS_UserId'))){
                    $data_arr = array(
                        'user_id' => $this->session->userdata('GSM_FUS_UserId'),
                        'pincode' => $this->input->post('pincode'),
                        'building_name' => $this->input->post('building_name'),
                        'road_area_colony' => $this->input->post('road_area_colony'),
                        'city' => $this->input->post('city'),
                        'district' => $this->input->post('district'),
                        'state' => $this->input->post('state'),
                        'landmark' => $this->input->post('landmark'),
                        'name' => $this->input->post('billing_name'),
                        'email' => $this->input->post('billing_email'),
                        'mobile_no' => $this->input->post('billing_mobile_no'),
                        'address_type' => $this->input->post('address_type'),
                        'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
                    );
                }
                else{
                    $data_arr = array(
                        'user_id' => $this->session->userdata('GSM_FUS_UserId'),
                        'pincode' => $this->input->post('pincode'),
                        'building_name' => $this->input->post('building_name'),
                        'road_area_colony' => $this->input->post('road_area_colony'),
                        'city' => $this->input->post('city'),
                        'district' => $this->input->post('district'),
                        'state' => $this->input->post('state'),
                        'landmark' => $this->input->post('landmark'),
                        'name' => $this->input->post('billing_name'),
                        'email' => $this->input->post('billing_email'),
                        'mobile_no' => $this->input->post('billing_mobile_no'),
                        'address_type' => $this->input->post('address_type'),
                        'is_default' => 'true',
                        'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
                    );
                }

                $data_usr = $this->security->xss_clean($data_arr);

                $address_id = $this->common_model->insert($data_usr, 'tbl_addresses');

                $message = array('message' => $this->lang->line('add_success'),'class' => 'alert alert-success');
                $this->session->set_flashdata('response_msg', $message);

                if(isset($_SERVER['HTTP_REFERER']))
                    redirect($_SERVER['HTTP_REFERER']);
                else
                    redirect('/', 'refresh');
            }
        }
        else  
        {  
            $message = array('message' => 'Enter all required fields','class' => 'alert alert-danger');
            $this->session->set_flashdata('response_msg', $message);

            if(isset($_SERVER['HTTP_REFERER']))
                redirect($_SERVER['HTTP_REFERER']);
            else
                redirect('/', 'refresh');

        }
    }

    public function edit_address(){

        $this->load->helper("date");

        $this->form_validation->set_rules('billing_name', 'Enter name', 'trim|required');
        $this->form_validation->set_rules('billing_mobile_no', 'Enter mobile no.', 'trim|required');
        $this->form_validation->set_rules('building_name', 'House no., Building Name', 'trim|required');
        $this->form_validation->set_rules('road_area_colony', 'Enter road, area, colony', 'trim|required');
        $this->form_validation->set_rules('pincode', 'Enter pincode/zipcode', 'trim|required');
        $this->form_validation->set_rules('city', 'Enter city', 'trim|required');
        $this->form_validation->set_rules('district', 'Enter district', 'trim|required');
        $this->form_validation->set_rules('state', 'Enter state', 'trim|required');

        if($this->form_validation->run())  
        {
            if($_POST){ 

                $data_arr = array(
                    'pincode' => $this->input->post('pincode'),
                    'building_name' => $this->input->post('building_name'),
                    'road_area_colony' => $this->input->post('road_area_colony'),
                    'city' => $this->input->post('city'),
                    'district' => $this->input->post('district'),
                    'state' => $this->input->post('state'),
                    'landmark' => $this->input->post('landmark'),
                    'name' => $this->input->post('billing_name'),
                    'email' => $this->input->post('billing_email'),
                    'mobile_no' => $this->input->post('billing_mobile_no'),
                    'address_type' => $this->input->post('address_type'),
                    'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
                );
                
                $data_usr = $this->security->xss_clean($data_arr);

                $address_id = $this->common_model->update($data_usr, $this->input->post('address_id'),'tbl_addresses');

                $response = array('status' => 1,'msg' => $this->lang->line('update_success'));
                echo base64_encode(json_encode($response));
                return;

            }
        }
        else  
        {  
            $response = array('status' => 0,'msg' => 'Enter all required fields');
            echo base64_encode(json_encode($response));
            return;

        }
    } 

    public function delete_address($address_id){

        $row=$this->common_model->selectByid($address_id,'tbl_addresses');
    
        if($row->is_default=='true'){

            $data_arr=$this->common_model->selectByids(array('user_id'=>$row->user_id),'tbl_addresses');

            if(count($data_arr) > 0){

                $this->common_model->delete($address_id,'tbl_addresses');

                $data_arr1 = array(
                    'is_default' => 'true'
                );

                $data_usr1 = $this->security->xss_clean($data_arr1);

                $where=array('user_id' => $row->user_id);

                $max_id=$this->common_model->getMaxId('tbl_addresses',$where);

                $updated_id = $this->common_model->update($data_usr1, $max_id, 'tbl_addresses');
            }
        }
        else{
            $this->common_model->delete($address_id,'tbl_addresses');
        }

        $message = array('message' => $this->lang->line('delete_success'),'class' => 'alert alert-success');
        $this->session->set_flashdata('response_msg', $message);

        if(isset($_SERVER['HTTP_REFERER']))
            redirect($_SERVER['HTTP_REFERER']);
        else
            redirect('/', 'refresh');
        
    }

    function logout(){
        
        $array_items = array('user_id', 'user_name', 'user_email', 'is_user_login');

        $this->session->unset_userdata($array_items);

        if(isset($_SERVER['HTTP_REFERER']))
            redirect($_SERVER['HTTP_REFERER']);
        else
            redirect('/', 'refresh');
    }

    // check cart
    public function check_cart($product_id, $user_id){

        $where = array('product_id' => $product_id, 'user_id' => $user_id);
        
        if($this->common_model->selectByids($where, 'tbl_cart')){
            return true;
        }
        else{
            return false;
        }

    }

    public function get_coupons()
    {
        return $this->Coupon_model->coupon_list();
    }

    public function apply_coupon(){

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        if($user_id==0){
            redirect('login-register', 'refresh');
        }

        $coupon_code=$this->input->post('coupon_code');
        $cart_amt=$this->input->post('cart_amt');

        $where=array('coupon_code' => $coupon_code);

        if($row=$this->common_model->selectByids($where,'tbl_coupon')){

            $row=$row[0];

            $where = array('user_id ' => $user_id , 'coupon_id' => $row->id);

            $count_use=count($this->common_model->selectByids($where,'tbl_order_details'));

            if($row->coupon_limit_use >= $count_use)
            {

                if($row->coupon_per!='' || $row->coupon_per!='0')
                {
                    // for percentage coupons

                    if($row->cart_status=='true'){

                        if($cart_amt >= $row->coupon_cart_min){

                            $payable_amt=$discount=0;

                            if($row->max_amt_status){

                                // count discount price after coupon apply;
                                $discount=($row->coupon_per/100) * $cart_amt;

                                if($discount > $row->coupon_max_amt){
                                    $discount=$row->coupon_max_amt;
                                    $payable_amt=$cart_amt-$discount;
                                }
                                else{
                                    $payable_amt=round($cart_amt - (($row->coupon_per/100) * $cart_amt),2);
                                }
                            }
                            else{
                                $discount=($row->coupon_per/100) * $cart_amt;
                                $payable_amt=($cart_amt - (($row->coupon_per/100) * $cart_amt));
                            }

                            $data_save=$this->user_total_save($user_id);

                            $arr_save=json_decode($data_save);

                            if($discount!=0){
                                $save_msg='You will save '.CURRENCY_CODE.' '.strval($discount).' on this order !';
                            }

                            $response=array('success' => '1','msg' => $this->lang->line('applied_coupon'),'coupon_id' => $row->id,'coupons_msg'=>'','you_save_msg' =>$save_msg, "price" => $cart_amt, "payable_amt" => strval($payable_amt),"discount" => $row->coupon_per,"discount_amt" => strval($discount));
                        }
                        else{
                            $response=array('success' => '0','msg' => $this->lang->line('no_coupon'));
                        }
                    }
                    else{

                        $payable_amt=$discount=0;

                        if($row->max_amt_status){

                            // count discount price after coupon apply;
                            $discount=sprintf("%.2f", (($row->coupon_per/100) * $cart_amt));

                            if($discount > $row->coupon_max_amt){
                                $discount=$row->coupon_max_amt;
                                $payable_amt=number_format((float)($cart_amt-$discount), 2, '.', '')+number_format((float)$this->input->post('delivery'), 2, '.', '');
                            }
                            else{
                                $payable_amt=number_format((float)($cart_amt - (($row->coupon_per/100) * $cart_amt)), 2, '.', '')+number_format((float)$this->input->post('delivery'), 2, '.', '');
                            }
                        }
                        else{
                            $discount=sprintf("%.2f", (($row->coupon_per/100) * $cart_amt));
                            $payable_amt=number_format((float)(($row->coupon_per/100) * $cart_amt), 2, '.', '')+number_format((float)$this->input->post('delivery'), 2, '.', '');
                        }

                        $data_save=$this->user_total_save($user_id);

                        $arr_save=json_decode($data_save);

                        if($discount!=0){
                            $save_msg='You will save '.CURRENCY_CODE.' '.strval($discount).' on this order !';
                        }


                        $response=array('success' => '1','msg' => $this->lang->line('applied_coupon'),'coupon_id' => $row->id,'coupons_msg'=>'','you_save_msg' =>$save_msg, "price" => $cart_amt, "payable_amt" => strval($payable_amt),"discount" => $row->coupon_per,"discount_amt" => strval($discount));

                    }
                }
            }
            else{
                $response=array('success' => '0','msg' => $this->lang->line('use_limit_over'));
            }
        }
        else{
            $response=array('success' => '0','msg' => $this->lang->line('no_coupon'));
        }

        echo json_encode($response);

    }

    private function user_total_save($user_id)
    {
        $res=array();
        
        $row=$this->common_model->get_cart($user_id);

        $total_amt=$delivery_charge=$you_save=0;

        foreach ($row as $key => $value) {

            $data_ofr=$this->calculate_offer($this->get_single_info(array('id' => $value->product_id),'offer_id','tbl_product'),$value->product_mrp*$value->product_qty);

            $arr_ofr=json_decode($data_ofr);

            $total_amt+=$arr_ofr->selling_price;

            $delivery_charge+=$value->delivery_charge;

            $you_save+=$arr_ofr->you_save;
        }

        $res['total_item']=strval(count($row));
        $res['price']=strval($total_amt);
        $res['delivery_charge']=($delivery_charge!=0)?$delivery_charge:'Free';
        $res['payable_amt']=strval($total_amt+$delivery_charge);

        $res['you_save']=strval($you_save);

        return json_encode($res);
    }


    public function get_single_info($ids, $param, $table_nm)
    {
        $data= $this->common_model->selectByids($ids, $table_nm);
        if(!empty($data)){
            return $data[0]->$param;    
        }else{
            return '';
        }
    }

    public function _create_thumbnail($path, $thumb_name, $fileName, $width, $height) 
    {
        $source_path = $path.$fileName;

        $ext = pathinfo($fileName, PATHINFO_EXTENSION);

        $thumb_name=$thumb_name.'_'.$width.'x'.$height.'.'.$ext;

        $thumb_path=$path.'thumbs/'.$thumb_name;

        if(!file_exists($thumb_path)){
            $this->load->library('image_lib');
            $config['image_library']  = 'gd2';
            $config['source_image']   = $source_path;       
            $config['new_image']      = $thumb_path;               
            $config['create_thumb']   = FALSE;
            $config['maintain_ratio'] = FALSE;
            $config['width']          = $width;
            $config['height']         = $height;
            $this->image_lib->initialize($config);
            if (! $this->image_lib->resize()) { 
                echo $this->image_lib->display_errors();
            }        
        }

        return $thumb_path;
    }

    public function calculate_offer($offer_id,$mrp)
    {
        $res=array();
        if($offer_id!=0){
            $offer = $this->Offers_model->single_offer($offer_id);
            $res['selling_price']=round($mrp - (($offer->offer_percentage/100) * $mrp),2);

            $res['you_save']=round($mrp-$res['selling_price'],2);
            $res['you_save_per']=$offer->offer_percentage;    
        }
        else{
            $res['selling_price']=$mrp;
            $res['you_save']=0;
            $res['you_save_per']=0;
        }
        return json_encode($res);
    }


    public function is_purchased($user_id, $product_id){

        $where = array('user_id' => $user_id, 'product_id' => $product_id, 'pro_order_status' => '4');

        if(count($this->common_model->selectByids($where,'tbl_order_items'))){
            return true;
        }
        else{
            return false;
        }


    }

    public function get_pincode_data(){

        // From URL to get webpage contents. 
        $url = "http://www.postalpincode.in/api/pincode/".$this->input->post('pincode'); 

        // Initialize a CURL session. 
        $ch = curl_init();  

        // Return Page contents. 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        //grab URL and pass it to the variable. 
        curl_setopt($ch, CURLOPT_URL, $url); 

        $response=array();

        if($result = curl_exec($ch)){
            $result1=json_decode($result);

            if($result1->PostOffice!=NULL){

                $response['status']='1';

                foreach ($result1->PostOffice as $key => $value) {

                    $response['city']=$value->Circle;
                    $response['district']=$value->District;
                    $response['state']=$value->State;
                    $response['country']=$value->Country;
                    break;
                }  
            }
            else{
                $response['status']='0';
                $response['massage']='No data found !';
            }
        }
        else{
            $response['status']='0';
            $response['massage']='No data found !'; 
        }

        
        echo json_encode($response);
        return;

    }
    
    // get order unique id

    private function get_order_unique_id()
    {
        $code_feed = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyv0123456789";
        $code_length = 8;  // Set this to be your desired code length
        $final_code = "";
        $feed_length = strlen($code_feed);

        for($i = 0; $i < $code_length; $i ++) {
            $feed_selector = rand(0,$feed_length-1);
            $final_code .= substr($code_feed,$feed_selector,1);
        }
        return $final_code;
    }

    // add product review

    function product_review(){

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        if($user_id==0){
            redirect('login-register', 'refresh');
        }

        $this->load->helper("date");

        $product_id=$this->input->post("product_id");
        $rate=trim($this->input->post("rating"));
        $review_desc=stripslashes(trim($this->input->post("message")));

        $where= array('user_id' => $user_id, 'product_id' => $product_id);

        $row=$this->common_model->selectByids($where, 'tbl_rating');

        if(count($row)==0)
        {
            // add new review
            $data_arr = array(
                'product_id' => $product_id,
                'user_id' => $user_id,
                'rating' => $rate,
                'rating_desc' => $review_desc,
                'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
            );

            $data_usr = $this->security->xss_clean($data_arr);

            $review_id = $this->common_model->insert($data_usr, 'tbl_rating');

            if(!empty($_FILES['product_images'])){
                $files = $_FILES;
                $cpt = count($_FILES['product_images']['name']);
                for($i=0; $i<$cpt; $i++)
                {
                    $_FILES['product_images']['name']= $files['product_images']['name'][$i];
                    $_FILES['product_images']['type']= $files['product_images']['type'][$i];
                    $_FILES['product_images']['tmp_name']= $files['product_images']['tmp_name'][$i];
                    $_FILES['product_images']['error']= $files['product_images']['error'][$i];
                    $_FILES['product_images']['size']= $files['product_images']['size'][$i];  

                    $image = date('dmYhis').'_'.rand(0,99999)."_review.".pathinfo($files['product_images']['name'][$i], PATHINFO_EXTENSION);

                    $config['file_name'] = $image;  

                    // File upload configuration
                    $uploadPath = 'assets/images/review_images/';
                    $config['upload_path'] = $uploadPath;
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';

                    // Load and initialize upload library
                    $this->load->library('upload');
                    $this->upload->initialize($config);   

                    if($this->upload->do_upload('product_images')){

                        $data_img = array(
                            'parent_id' => $review_id,
                            'image_file' => $image,
                            'type' => 'review'
                        );

                        $data_img = $this->security->xss_clean($data_img);
                        $this->common_model->insert($data_img, 'tbl_product_images');
                    }
                }   
            }

            $message = array('success' => '1','message' => $this->lang->line('review_submit'),'class' => 'alert alert-success');
            $this->session->set_flashdata('response_msg', $message);


            // product review

            $this->Product_model->set_product_review($product_id);


        }
        else
        {
            // update old review
            $data_arr = array(
                'product_id' => $product_id,
                'user_id' => $user_id,
                'rating' => $rate,
                'rating_desc' => $review_desc
            );

            $data_usr = $this->security->xss_clean($data_arr);

            $this->common_model->update($data_usr, $row[0]->id,'tbl_rating');

            $review_id = $row[0]->id;

            if(!empty($_FILES['product_images'])){
                $files = $_FILES;
                $cpt = count($_FILES['product_images']['name']);
                for($i=0; $i<$cpt; $i++)
                {           

                    $_FILES['product_images']['name']= $files['product_images']['name'][$i];
                    $_FILES['product_images']['type']= $files['product_images']['type'][$i];
                    $_FILES['product_images']['tmp_name']= $files['product_images']['tmp_name'][$i];
                    $_FILES['product_images']['error']= $files['product_images']['error'][$i];
                    $_FILES['product_images']['size']= $files['product_images']['size'][$i];  

                    $image = date('dmYhis').'_'.rand(0,99999)."_review.".pathinfo($files['product_images']['name'][$i], PATHINFO_EXTENSION);

                    $config['file_name'] = $image;  

                    // File upload configuration
                    $uploadPath = 'assets/images/review_images/';
                    $config['upload_path'] = $uploadPath;
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';

                    // Load and initialize upload library
                    $this->load->library('upload');
                    $this->upload->initialize($config);   

                    if($this->upload->do_upload('product_images')){

                        $data_img = array(
                            'parent_id' => $review_id,
                            'image_file' => $image,
                            'type' => 'review'
                        );

                        $data_img = $this->security->xss_clean($data_img);
                        $this->common_model->insert($data_img, 'tbl_product_images');
                    }
                }   
            }

            $message = array('success' => '1','message' => $this->lang->line('review_updated'),'class' => 'alert alert-success');
            $this->session->set_flashdata('response_msg', $message);

            // product review
            $this->Product_model->set_product_review($product_id,'edit');

        }

        echo json_encode($message);

    }

    function edit_review(){

        $id=$this->input->post("review_id");

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        if($user_id==0){
            redirect('login-register', 'refresh');
        }

        $rate=trim($this->input->post("rating"));
        $review_desc=stripslashes(trim($this->input->post("message")));

        $data_arr = array(
            'rating' => $rate,
            'rating_desc' => str_replace("'", "", $review_desc)
        );

        $data_usr = $this->security->xss_clean($data_arr);

        $this->common_model->update($data_usr, $id,'tbl_rating');

        $message = array('success' => '1','message' => $this->lang->line('review_updated'));

        echo base64_encode(json_encode($message));
    }

    function remove_review_image(){

        $id=$this->input->post("id");

        $row=$this->common_model->selectByid($id, 'tbl_product_images');

        
        if(file_exists('assets/images/review_images/'.$row->image_file))
            unlink('assets/images/review_images/'.$row->image_file);

        $this->common_model->delete($id,'tbl_product_images');
        
        
        $message = array('success' => '1','message' => $this->lang->line('delete_success'),'class' => 'alert alert-success');

        echo json_encode($message);

    }

    function remove_review(){

        $id=$this->input->post("review_id");

        $row_img=$this->common_model->selectByids(array('parent_id' => $id, 'type' => 'review'), 'tbl_product_images');

        foreach ($row_img as $key => $value) {
            if(file_exists('assets/images/review_images/'.$value->image_file))
                unlink('assets/images/review_images/'.$value->image_file);

            $this->common_model->delete($value->id,'tbl_product_images');
        }
        
        $this->common_model->delete($id,'tbl_rating');
        
        $message = array('success' => '1','message' => $this->lang->line('delete_success'));

        echo json_encode($message);

    }



    // cash on delivery order

    function place_order(){

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        if($user_id==0){
            redirect('login-register', 'refresh');
        }

        $cart_ids=explode(',', $this->input->post('cart_ids'));

        if(!empty($this->common_model->check_cart($cart_ids))){
            $order_unique_id='ORD'.$this->get_order_unique_id().rand(0,1000);
            $this->load->helper("date");

            

            $data_arr = array(
                'user_id' => $this->session->userdata('GSM_FUS_UserId'),
                'coupon_id' => $this->input->post('coupon_id'),
                'order_unique_id' => $order_unique_id,
                'order_address' => $this->input->post('order_address'),
                'total_amt' => $this->input->post('total_amt'),
                'discount' => $this->input->post('discount'),
                'discount_amt' => $this->input->post('discount_amt'),
                'payable_amt' => $this->input->post('payable_amt'),
                'new_payable_amt' => $this->input->post('payable_amt'),
                'delivery_date' => strtotime(date('d-m-Y h:i:s A', strtotime('+7 days'))),
                'order_date' => strtotime(date('d-m-Y h:i:s A',now())),
                'delivery_charge' => $this->input->post('delivery_charge')
            );

            $data_ord = $this->security->xss_clean($data_arr);

            $order_id = $this->common_model->insert($data_ord, 'tbl_order_details');

            $products_arr=array();

            foreach ($cart_ids as $cart_id) {

                $row_cart=$this->common_model->cart_item($cart_id);

                $product_mrp=0;
                
                $total_price=$row_cart->product_qty*$this->common_model->selectByidsParam(array('id' => $row_cart->product_id),'tbl_product','selling_price');

                $product_mrp=$this->common_model->selectByidsParam(array('id' => $row_cart->product_id),'tbl_product','selling_price');

                $data_order = array(
                    'order_id'  =>  $order_id,
                    'user_id' => $this->session->userdata('GSM_FUS_UserId'),
                    'product_id'  =>  $row_cart->product_id,
                    'product_title'  =>  $this->common_model->selectByidsParam(array('id' => $row_cart->product_id),'tbl_product','product_title'),
                    'product_qty'  =>  $row_cart->product_qty,
                    'product_price'  =>  $product_mrp,
                    'product_size'  =>  $row_cart->product_size,
                    'total_price'  =>  $total_price
                );

                $data_ord_detail = $this->security->xss_clean($data_order);

                $order_detail_id = $this->common_model->insert($data_ord_detail, 'tbl_order_items');

                $img_file=$this->_create_thumbnail('assets/images/products/',$this->common_model->selectByidsParam(array('id' => $row_cart->product_id),'tbl_product','product_slug'),$this->common_model->selectByidsParam(array('id' => $row_cart->product_id),'tbl_product','featured_image'),300,300);

                $p_items['product_title']=$this->common_model->selectByidsParam(array('id' => $row_cart->product_id),'tbl_product','product_title');
                $p_items['product_img']=base_url().$img_file;
                $p_items['product_qty']=$row_cart->product_qty;
                $p_items['product_price']=$product_mrp;
                $p_items['delivery_charge']=$this->input->post('delivery_charge');
                $p_items['product_size']=$row_cart->product_size;
                $p_items['product_color']=$this->common_model->selectByidsParam(array('id' => $row_cart->product_id),'tbl_product','color');
                $p_items['delivery_date']=date('d M, Y').'-'.date('d M, Y', strtotime('+7 days'));

                array_push($products_arr, $p_items);

                $this->common_model->delete($cart_id,'tbl_cart');
            }

            $data_arr = array(
                'user_id' => $this->session->userdata('GSM_FUS_UserId'),
                'email' => $this->session->userdata('user_email'),
                'order_id' => $order_id,
                'order_unique_id' => $order_unique_id,
                'gateway' => $this->input->post('payment_method'),
                'payment_amt' => $this->input->post('payable_amt'),
                'payment_id' => '0',
                'date' => strtotime(date('d-m-Y h:i:s A',now())),
                'status' => '1'
            );

            $data_usr = $this->security->xss_clean($data_arr);

            $this->common_model->insert($data_usr, 'tbl_transaction');

            $data_update = array(
                'order_status'  =>  '1',
            );

            $this->common_model->update($data_update, $order_id,'tbl_order_details');

            $data_arr = array(
                'order_id' => $order_id,
                'user_id' => $this->session->userdata('GSM_FUS_UserId'),
                'product_id' => '0',
                'status_title' => '1',
                'status_desc' => $this->lang->line('0'),
                'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
            );

            $data_usr = $this->security->xss_clean($data_arr);

            $this->common_model->insert($data_usr, 'tbl_order_status');

            $where = array('order_id' => $order_id);

            $row_items=$this->common_model->selectByids($where, 'tbl_order_items');

            foreach ($row_items as $key2 => $value2) {
                $data_arr = array(
                    'order_id' => $order_id,
                    'user_id' => $value2->user_id,
                    'product_id' => $value2->product_id,
                    'status_title' => '1',
                    'status_desc' => $this->lang->line('0'),
                    'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
                );

                $data_usr = $this->security->xss_clean($data_arr);

                $this->common_model->insert($data_usr, 'tbl_order_status');

                $data_pro = array(
                    'pro_order_status' => '1'
                ); 

                $data_pro = $this->security->xss_clean($data_pro);

                $this->common_model->updateByids($data_pro, array('order_id' => $order_id, 'product_id' => $value2->product_id),'tbl_order_items');

            }

            $data_email=array();

            $arr_address=$this->common_model->selectByid($this->input->post('order_address'), 'tbl_addresses');

            $delivery_address=$arr_address->building_name.', '.$arr_address->road_area_colony.',<br/>'.$arr_address->pincode.'<br/>'.$arr_address->city.', '.$arr_address->state;

            $data_email['users_name']=$arr_address->name;

            $data_email['order_unique_id']=$order_unique_id;
            $data_email['order_date']=date('d M, Y');
            $data_email['delivery_address']=$delivery_address;
            $data_email['discount_amt']=$this->input->post('discount_amt');
            $data_email['total_amt']=$this->input->post('total_amt');
            $data_email['delivery_charge']=$this->input->post('delivery_charge');
            $data_email['payable_amt']=$this->input->post('payable_amt');

            $data_email['products']=$products_arr;

            $this->email->from($this->app_email, $this->app_name);

            $this->email->to($arr_address->email); // replace it with receiver mail id

            $subject = $this->app_name.'- Order Summary';

            $this->email->subject($subject); // replace it with relevant subject
/*
            $body = $this->load->view('emails/order_summary.php',$data_email,TRUE);

            $this->email->message($body); 
/*
            if($this->email->send()){
               $res_json=array('success' => '1','msg' => 'Order summary sent to your email...', 'order_unique_id' => $order_unique_id); 
            }
            else{
                $res_json=array('success' => '0','msg' => 'Sorry email is not sent', 'order_unique_id' => $order_unique_id,'error' => $this->email->print_debugger());
                exit(1);
            }
*/
			$res_json=array('success' => '1','msg' => 'Order summary sent to your email...', 'order_unique_id' => $order_unique_id);
            $this->session->set_flashdata('success_msg','Order Placed...');

            echo json_encode($res_json);
        }
        else{

            $res_json=array('success' => '-1','msg' => 'Your cart is empty',);

            echo json_encode($res_json);

        }
    }

    // download order invoice
    public function download_invoice(){

        $data = array();

        $order_no =  $this->uri->segment(2);
        
        $data['page_title'] = 'Orders';
        $data['current_page'] = 'Order Summary';
        $data['order_data'] = $this->Order_model->get_order($order_no);
        $this->load->view('download_invoice', $data); // :blush:
        
        $html = $this->output->get_output();
        
        // Load pdf library
        $this->load->library('pdf');
        $this->pdf->loadHtml($html);
        $this->pdf->setPaper('A4', 'portrait');
        $this->pdf->render();

        $file_name="Invoice-".$order_no.".pdf";

        $this->pdf->stream($file_name, array("Attachment"=> 0));

    }

    public function my_order($order_unique_id=NULL){

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        if($user_id==0){
            redirect('login-register', 'refresh');
        }

        //$data = array();
        $this->data['page_title'] = 'My Orders';
        $this->data['current_page'] = 'My Orders';

        if($order_unique_id!=NULL){
            $this->data['my_order'] = $this->api_model->get_order($order_unique_id);

            if(empty($this->data['my_order'])){
                if(isset($_SERVER['HTTP_REFERER']))
                    redirect($_SERVER['HTTP_REFERER']);
                else
                    redirect('/', 'refresh');
            }

            $this->data['order_address'] = $this->common_model->selectByid($this->data['my_order'][0]->order_address, 'tbl_addresses');

            $this->data['status_titles'] = $this->Order_model->get_titles(true);

            $this->data['order_status'] = $this->Order_model->get_product_status($this->data['my_order'][0]->order_id,0);

            $this->data['bank_details'] = $this->common_model->selectByids(array('user_id' => $user_id), 'tbl_bank_details');

			$this->data['view'] = 'site/pages/order_detail';

            $this->load->view('layouts/default', $this->data); // :blush:
        }
        else{
            $this->data['my_orders'] = $this->api_model->get_my_orders($user_id);

            $this->data['bank_details'] = $this->common_model->selectByids(array('user_id' => $user_id), 'tbl_bank_details');

			$this->data['view'] = 'site/pages/my_orders';
            $this->load->view('layouts/default', $this->data); // :blush:
        }

        
    }

    public function my_account(){

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        if($user_id==0){
            redirect('login-register', 'refresh');
        }

        $data = array();
        $data['page_title'] = 'My Account';
        $data['current_page'] = 'My Account';

        $data['user_data'] = $this->common_model->selectByid($user_id, 'tbl_users');

        $this->load->view('layouts/default', 'site/pages/my_account', $data); // :blush:

    }

    public function my_addresses(){

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        if($user_id==0){
            redirect('login-register', 'refresh');
        }

        $data = array();
        $data['page_title'] = 'My Addresses';
        $data['current_page'] = 'My Addresses';

        $data['addresses'] = $this->common_model->get_addresses($user_id);

        $this->load->view('layouts/default', 'site/pages/addresses', $data); // :blush:

    }

    public function saved_bank_accounts(){

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        if($user_id==0){
            redirect('login-register', 'refresh');
        }

        $data = array();
        $data['page_title'] = 'Saved Bank';
        $data['current_page'] = 'Saved Bank';

        $data['bank_details'] = $this->common_model->selectByids(array('user_id' => $user_id), 'tbl_bank_details');

        $this->load->view('layouts/default', 'site/pages/saved_cards.php', $data); // :blush:

    }

    public function my_reviews(){

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        if($user_id==0){
            redirect('login-register', 'refresh');
        }

        $data = array();
        $data['page_title'] = 'My Reviews & Rating';
        $data['current_page'] = 'My Reviews';

        $where= array('user_id' => $user_id);

        $data['my_review']=$this->common_model->selectByids($where, 'tbl_rating');

        $this->load->view('layouts/default', 'site/pages/my_reviews.php', $data); // :blush:

    }

    public function product_reviews(){

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        if($user_id==0){
            redirect('login-register', 'refresh');
        }

        $data = array();
        $data['page_title'] = 'Product Reviews';
        $data['current_page'] = 'Product Reviews';

        $product_slug =  $this->uri->segment(2);

        $config = array();
        $config["base_url"] = base_url() . 'product-reviews/'.$product_slug;
        $config["per_page"] = 10;

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        $page=($page-1) * $config["per_page"];

        $where=array('product_slug' => $product_slug);

        $product_id =  $this->common_model->getIdBySlug($where, 'tbl_product');

        if($this->input->get('sort')!=''){
            $data['reviews']=$this->api_model->get_product_review($product_id, $this->input->get('sort'));
        }
        else{
            $data['reviews']=$this->api_model->get_product_review($product_id,'',$config["per_page"],$page); 
        }

        $config["total_rows"] = count($this->api_model->get_product_review($product_id));

        $config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
         
        $config['full_tag_open'] = '<ul class="page-number">';
        $config['full_tag_close'] = '</ul>';
         
        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
         
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
         
        $config['next_link'] = '';
        $config['next_tag_open'] = '<span class="nextlink">';
        $config['next_tag_close'] = '</span>';

        $config['prev_link'] = '';
        $config['prev_tag_open'] = '<span class="prevlink">';
        $config['prev_tag_close'] = '</span>';

        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';

        $config['num_tag_open'] = '<li style="margin:3px">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data['product_row']=$this->common_model->selectByid($product_id, 'tbl_product');

        $this->load->view('layouts/default', 'site/pages/product_reviews.php', $data); // :blush:

    }

    public function change_password(){

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        if($user_id==0){
            redirect('login-register', 'refresh');
        }

        $response=array();

        extract($this->input->post());

        if(count($this->common_model->selectByids(array('user_password' => md5($old_password), 'id' => $user_id), 'tbl_users')) > 0){

            $data_update = array(
                'user_password'  =>  md5($new_password)
            );

            $this->common_model->update($data_update, $user_id,'tbl_users');


            $response=array('status' => 1, 'msg' => 'Password Changed successfully...');
            
        }
        else{

            $response=array('status' => 0, 'msg' => 'Old password is not found !', 'class' => 'err_old_password');
            
        }

        echo base64_encode(json_encode($response));

    }

    public function update_profile(){

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        if($user_id==0){
            redirect('login-register', 'refresh');
        }

        $response=array();

        $row=$this->common_model->selectByid($user_id, 'tbl_users');

        if($_FILES['file_name']['error']!=4){
            
            if(file_exists('assets/images/users/'.$row->user_image OR $row->user_image!=''))
                unlink('assets/images/users/'.$row->user_image);

            $config['upload_path'] =  'assets/images/users/';
            $config['allowed_types'] = 'jpg|png|jpeg';

            $image = date('dmYhis').'_'.rand(0,99999).".".pathinfo($_FILES['file_name']['name'], PATHINFO_EXTENSION);

            $config['file_name'] = $image;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file_name')) {
                $response = array('status' => 0,'msg' => $this->upload->display_errors());
                echo base64_encode(json_encode($response));
                return;
            }
        }
        else{
            $image=$row->user_image;
        }


        $data = array(
            'user_name'  => $this->input->post('user_name'),
            'user_email'  => $this->input->post('user_email'),
            'user_phone'  => $this->input->post('user_phone'),
            'user_image'  => $image
        );

        $data_update = $this->security->xss_clean($data);

        $this->common_model->update($data_update, $user_id,'tbl_users');

        $response=array('status' => 1, 'msg' => 'Profile updated successfully...');

        echo base64_encode(json_encode($response));

    }

    // user's forgot password code
    public function forgot_password(){
        
        //-- check duplicate email
        $user_info = $this->common_model->check_email($this->input->post('registered_email'));

        if (!empty($user_info)) {

            $info['new_password']=get_random_password();

            $updateData = array(
                'user_password' => md5($info['new_password'])
            );

            $data_arr = array(
                'email' => $user_info[0]->user_email,
                'password' => $info['new_password']
            );

            if($this->common_model->update($updateData, $user_info[0]->id,'tbl_users')){

                $this->email->from($this->app_email, $this->app_name);

                $this->email->to($this->input->post('registered_email')); // replace it with receiver mail id

                $subject = $this->app_name.'- Forgot Password';

                $this->email->subject($subject); // replace it with relevant subject

                $body = $this->load->view('admin/emails/forgot_password.php',$data_arr,TRUE);

                $this->email->message($body); 

                if($this->email->send()){

                    $message = array('success' => '1','message' => 'Password is successfully sent to your email...','class' => 'alert alert-success');
                    $this->session->set_flashdata('response_msg', $message);
                }
                else{
                    $message = array('success' => '0','message' => $this->lang->line('email_not_sent'),'class' => 'alert alert-danger');
                }
            }
            
        }
        else{
            $message = array('success' => '0','message' => $this->lang->line('email_not_found'),'class' => 'alert alert-danger');
        }

        echo json_encode($message);

    }

    public function order_status($order_id, $product_id=NULL){

        $where=array('order_id' => $order_id);

        return $this->common_model->selectWhere('tbl_order_status',$where,'DESC');

    }

    // cancel product order
    public function cancel_product(){
        $order_id=$this->input->post('order_id');
        $product_id=$this->input->post('product_id');

        $reason=$this->input->post('reason');
        $bank_id=$this->input->post('bank_id');

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        if($user_id==0){
            redirect('login-register', 'refresh');
        }

        $this->load->helper("date");


        $where=array('order_id' => $order_id);

        $row_trn=$this->common_model->selectByids($where, 'tbl_transaction')[0];

        $row_ord=$this->common_model->selectByid($order_id, 'tbl_order_details');

        if($product_id!='0'){
            // for perticular product order cancel

            $where=array('order_id' => $order_id,'product_id' => $product_id);

            $row_pro=$this->common_model->selectByids($where, 'tbl_order_items')[0];

            $this->load->helper("date");

            $product_per=$refund_amt=$refund_per=$new_payable_amt=0;

            if($row_ord->coupon_id!=0){

                $product_per=round(($row_pro->total_price/$row_ord->total_amt)*100);  //44
                $refund_per=round(($product_per/100)*$row_ord->discount_amt); //22
                $refund_amt=$row_pro->total_price-$refund_per; //38 
                $new_payable_amt=$row_ord->new_payable_amt-$refund_amt;

            }
            else{
                $refund_amt=$row_pro->total_price;
                $new_payable_amt=($row_ord->new_payable_amt-$refund_amt);
            }

            if($row_trn->gateway=='COD' || $row_trn->gateway=='cod'){
                $bank_id=0;
                $status=1;
            }
            else{
                $status=0;
            }

            $data_arr = array(
                'bank_id' => $bank_id,
                'user_id' => $user_id,
                'order_id' => $order_id,
                'order_unique_id' => $row_ord->order_unique_id,
                'product_id' => $product_id,
                'product_title' => $row_pro->product_title,
                'product_amt' => $row_pro->total_price,
                'refund_pay_amt' => $refund_amt,
                'refund_per' => $refund_per,
                'gateway' => $row_trn->gateway,
                'refund_reason' => $reason,
                'last_updated' => strtotime(date('d-m-Y h:i:s A',now())),
                'request_status' => $status,
                'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
            );

            $data_update = $this->security->xss_clean($data_arr);

            $this->common_model->insert($data_update, 'tbl_refund');

            $where=array('order_id' => $order_id, 'pro_order_status <> ' => 5);

            if(count($this->common_model->selectByids($where, 'tbl_order_items')) == 1){

                $data_update = array(
                    'order_status' => '5',
                    'new_payable_amt'  =>  '0',
                    'refund_amt'  =>  $refund_amt,
                    'refund_per'  =>  $refund_per
                );

                $data = array(
                    'order_id' => $order_id,
                    'user_id' => $user_id,
                    'product_id' => '0',
                    'status_title' => '5',
                    'status_desc' => $this->lang->line('ord_cancel'),
                    'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
                );

                $data = $this->security->xss_clean($data);

                $this->common_model->insert($data, 'tbl_order_status');

            }
            else{
                $data_update = array(
                    'new_payable_amt'  =>  $new_payable_amt,
                    'refund_amt'  =>  $refund_amt,
                    'refund_per'  =>  $refund_per
                );
            }

            $this->common_model->update($data_update, $order_id,'tbl_order_details');

            $data_pro = array(
                'pro_order_status' => '5'
            ); 

            $data_pro = $this->security->xss_clean($data_pro);

            $this->common_model->updateByids($data_pro, array('order_id' => $order_id, 'product_id' => $product_id),'tbl_order_items');

            $data = array(
                'order_id' => $order_id,
                'user_id' => $user_id,
                'product_id' => $product_id,
                'status_title' => '5',
                'status_desc' => $this->lang->line('pro_ord_cancel'),
                'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
            );

            $data = $this->security->xss_clean($data);

            $this->common_model->insert($data, 'tbl_order_status');

            echo json_encode(array('success' => 1, 'msg' => 'Your product order is cancelled successfully...'));

        }
        else{

            $where=array('order_id' => $order_id, 'pro_order_status <> ' => 5);

            $row_pro=$this->common_model->selectByids($where, 'tbl_order_items');

            $refund_amt=$refund_per=$new_payable_amt=$total_refund_amt=$total_refund_per=0;

            foreach ($row_pro as $key => $value) {

                $actual_pay_amt=($row_ord->payable_amt-$row_ord->delivery_charge);

                $product_per=$new_payable_amt=0;

                if($row_ord->coupon_id!=0){

                    $product_per=round(($value->total_price/$row_ord->total_amt)*100);  //44
                    $refund_per=round(($product_per/100)*$row_ord->discount_amt); //22
                    $refund_amt=$value->total_price-$refund_per; //38 
                    $new_payable_amt=$row_ord->payable_amt-$refund_amt;

                }
                else{
                    $refund_amt=$value->total_price;
                    $new_payable_amt=($row_ord->payable_amt-$refund_amt);
                }

                if($row_trn->gateway=='COD' || $row_trn->gateway=='cod'){
                    $bank_id=0;
                    $status=1;
                }
                else{
                    $status=0;
                }

                $total_refund_amt+=$refund_amt;
                $total_refund_per+=$refund_per;

                $data_arr = array(
                    'bank_id' => $bank_id,
                    'user_id' => $user_id,
                    'order_id' => $order_id,
                    'order_unique_id' => $row_ord->order_unique_id,
                    'product_id' => $value->product_id,
                    'product_title' => $value->product_title,
                    'product_amt' => $value->total_price,
                    'refund_pay_amt' => $refund_amt,
                    'refund_per' => $refund_per,
                    'gateway' => $row_trn->gateway,
                    'refund_reason' => $reason,
                    'last_updated' => strtotime(date('d-m-Y h:i:s A',now())),
                    'request_status' => $status,
                    'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
                );

                $data_update = $this->security->xss_clean($data_arr);

                $this->common_model->insert($data_update, 'tbl_refund');

                $data = array(
                    'order_id' => $order_id,
                    'user_id' => $user_id,
                    'product_id' => $value->product_id,
                    'status_title' => '5',
                    'status_desc' => $this->lang->line('pro_ord_cancel'),
                    'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
                );

                $data = $this->security->xss_clean($data);

                $this->common_model->insert($data, 'tbl_order_status');

                $data_pro = array(
                    'pro_order_status' => '5'
                ); 

                $data_pro = $this->security->xss_clean($data_pro);

                $this->common_model->updateByids($data_pro, array('order_id' => $order_id, 'product_id' => $value->product_id),'tbl_order_items');

            }


            $data_update = array(
                'order_status' => '5',
                'new_payable_amt'  =>  '0',
                'refund_amt'  =>  $total_refund_amt,
                'refund_per'  =>  $total_refund_per
            );

            $data_update = $this->security->xss_clean($data_update);
            $this->common_model->update($data_update, $order_id,'tbl_order_details');

            $data = array(
                'order_id' => $order_id,
                'user_id' => $user_id,
                'product_id' => '0',
                'status_title' => '5',
                'status_desc' => $this->lang->line('ord_cancel'),
                'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
            );

            $data = $this->security->xss_clean($data);

            $this->common_model->insert($data, 'tbl_order_status');
            
            echo json_encode(array('success' => 1, 'msg' => 'Your order is cancelled successfully...'));

        }

        

    }

    // claim refund request
    public function claim_refund(){

        $this->load->helper("date");

        $order_id=$this->input->post('order_id');
        $product_id=$this->input->post('product_id');

        $bank_id=$this->input->post('bank_id');

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        if($user_id==0){
            redirect('login-register', 'refresh');
        }

        $data_pro = array(
            'bank_id' => $bank_id,
            'last_updated' => strtotime(date('d-m-Y h:i:s A',now())),
            'request_status' => '0'
        ); 

        $data_pro = $this->security->xss_clean($data_pro);

        $this->common_model->updateByids($data_pro, array('order_id' => $order_id, 'product_id' => $product_id),'tbl_refund');

        echo json_encode(array('success' => 1, 'msg' => $this->lang->line('claim_msg')));

    }

    public function add_new_bank(){

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        if($user_id==0){
            redirect('login-register', 'refresh');
        }

        $this->load->helper("date");

        extract($this->input->post());

        $where=array('user_id' => $user_id, 'account_no' => $account_no, 'bank_ifsc' => $bank_ifsc);

        $row=$this->common_model->selectByids($where, 'tbl_bank_details');

        if(count($row)==0){
            // add new
            $this->form_validation->set_rules('bank_name', 'Enter bank name', 'trim|required');
            $this->form_validation->set_rules('account_no', 'Enter bank account number', 'trim|required');
            $this->form_validation->set_rules('account_type', 'Select account type', 'trim|required');
            $this->form_validation->set_rules('holder_name', 'Enter bank holder name', 'trim|required');
            $this->form_validation->set_rules('holder_mobile', 'Enter holder mobile number', 'trim|required');

            if($this->form_validation->run() == FALSE)
            {
                $message = array('message' => $this->lang->line('input_required'),'success' => '0');
            }
            else{
                if($this->input->post('is_default')!=''){
                    $is_default=1;
                }
                else{
                    $is_default=0;   
                }

                $where=array('user_id' => $user_id);
                $row_data=$this->common_model->selectByids($where, 'tbl_bank_details');

                if(count($row_data) > 0){
                    if($is_default==1){
                        $data_arr = array(
                            'is_default' => 0
                        ); 

                        $data_arr = $this->security->xss_clean($data_arr);

                        $this->common_model->updateByids($data_arr, array('user_id' => $user_id),'tbl_bank_details');
                    }
                }
                else{
                    $is_default=1;
                }

                $data_arr = array(
                    'user_id' => $user_id,
                    'bank_holder_name' => $holder_name,
                    'bank_holder_phone' => $holder_mobile,
                    'bank_holder_email' => $holder_email,
                    'account_no' => $account_no,
                    'account_type' => $account_type,
                    'bank_ifsc' => $bank_ifsc,
                    'bank_name' => $bank_name,
                    'is_default' => $is_default,
                    'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
                );

                $data_usr = $this->security->xss_clean($data_arr);

                $last_id = $this->common_model->insert($data_usr, 'tbl_bank_details');
                
                $message = array('message' => 'Added successfully...','success' => '1');
            }

        }
        else{
            $message = array('message' => 'Bank details is already exist !!!','success' => '0');
        }

        echo json_encode($message);

    }

    public function edit_bank_account(){

        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

        if($user_id==0){
            redirect('login-register', 'refresh');
        }

        $this->load->helper("date");

        extract($this->input->post());

        $this->form_validation->set_rules('bank_name', 'Enter bank name', 'trim|required');
        $this->form_validation->set_rules('account_no', 'Enter bank account number', 'trim|required');
        $this->form_validation->set_rules('account_type', 'Select account type', 'trim|required');
        $this->form_validation->set_rules('holder_name', 'Enter bank holder name', 'trim|required');
        $this->form_validation->set_rules('holder_mobile', 'Enter holder mobile number', 'trim|required');

        if($this->form_validation->run() == FALSE)
        {
            $message = array('message' => $this->lang->line('input_required'),'success' => '0');
        }
        else{

            if($this->input->post('is_default')!=''){
                $is_default=1;
            }
            else{
                $is_default=0;
            }

            $where=array('user_id' => $user_id);
            $row_data=$this->common_model->selectByids($where, 'tbl_bank_details');

            if(count($row_data) > 0){
                if($is_default==1){
                    $data_arr = array(
                        'is_default' => 0
                    ); 

                    $data_arr = $this->security->xss_clean($data_arr);

                    $this->common_model->updateByids($data_arr, array('user_id' => $user_id),'tbl_bank_details');
                }
            }
            else{
                $is_default=1;
            }

            $data_arr = array(
                'user_id' => $user_id,
                'bank_holder_name' => $holder_name,
                'bank_holder_phone' => $holder_mobile,
                'bank_holder_email' => $holder_email,
                'account_no' => $account_no,
                'account_type' => $account_type,
                'bank_ifsc' => $bank_ifsc,
                'bank_name' => $bank_name,
                'is_default' => $is_default
            );

            $data_usr = $this->security->xss_clean($data_arr);

            $this->common_model->update($data_usr, $bank_id,'tbl_bank_details');

            if($this->input->post('is_default')==''){
                $where=array('user_id' => $user_id, 'id <>' => $bank_id);
                $max_id=$this->common_model->getMaxId('tbl_bank_details',$where);

                $data_arr = array(
                    'is_default' => 1
                ); 

                $data_arr = $this->security->xss_clean($data_arr);

                $this->common_model->update($data_arr, $max_id,'tbl_bank_details');


            }

            $message = array('message' => $this->lang->line('update_success'),'success' => '1');
        }

        echo base64_encode(json_encode($message));

    }

    public function remove_bank_account(){

        $id =  $this->input->post("bank_id");

        $row=$this->common_model->selectByid($id,'tbl_bank_details');

        if($row->is_default=='1'){

            $data_arr=$this->common_model->selectByids(array('user_id'=>$row->user_id),'tbl_bank_details');

            if(count($data_arr) > 0){

                $this->common_model->delete($id,'tbl_bank_details');

                $data_arr1 = array(
                    'is_default' => '1'
                );

                $data_usr1 = $this->security->xss_clean($data_arr1);

                $where=array('user_id' => $row->user_id);

                $max_id=$this->common_model->getMaxId('tbl_bank_details',$where);

                $updated_id = $this->common_model->update($data_usr1, $max_id, 'tbl_bank_details');
            }
        }
        else{

            $this->common_model->delete($id, 'tbl_bank_details');
        }

        $message = array('message' => $this->lang->line('bank_remove'),'success' => '1');
        echo json_encode($message);
    }

    public function about_us(){

        $this->load->model('Setting_model');

        $data = array();
        $data['page_title'] = 'About Us';
        $data['current_page'] = 'About Us';
        $data['settings_row'] = $this->Setting_model->get_web_details()->about_content;

        $this->load->view('layouts/default', 'site/pages/page', $data); // :blush:
    }

    public function faq(){

        $this->load->model('Setting_model');

        $data = array();
        $data['page_title'] = "FAQ's";
        $data['current_page'] = "FAQ's";
        $data['faq_row'] = $this->common_model->selectByids(array('status' => '1','type' => 'faq'), 'tbl_faq');
        $this->load->view('layouts/default', 'site/pages/faq', $data); // :blush:
    }

    public function payments(){

        $this->load->model('Setting_model');

        $data = array();
        $data['page_title'] = "Payment FAQ's";
        $data['current_page'] = "Payment FAQ's";
        $data['faq_row'] = $this->common_model->selectByids(array('status' => '1','type' => 'payment'), 'tbl_faq');
        $this->load->view('layouts/default', 'site/pages/faq', $data); // :blush:
    }

    public function terms_of_use(){

        $this->load->model('Setting_model');

        $data = array();
        $data['page_title'] = "Terms-Of-Use";
        $data['current_page'] = "Terms-Of-Use";
        $data['settings_row'] = $this->Setting_model->get_web_details()->terms_of_use_content;
        $this->load->view('layouts/default', 'site/pages/page', $data); // :blush:
    }

    public function privacy(){

        $this->load->model('Setting_model');

        $data = array();
        $data['page_title'] = "Privacy";
        $data['current_page'] = "Privacy";
        $data['settings_row'] = $this->Setting_model->get_web_details()->privacy_content;
        $this->load->view('layouts/default', 'site/pages/page', $data); // :blush:
    }

    public function cancellation(){

        $this->load->model('Setting_model');

        $data = array();
        $data['page_title'] = "Cancellation";
        $data['current_page'] = "Cancellation";
        $data['settings_row'] = $this->Setting_model->get_web_details()->cancellation_content;
        $this->load->view('layouts/default', 'site/pages/page', $data); // :blush:
    }

    public function refund_return_policy(){

        $this->load->model('Setting_model');

        $data = array();
        $data['page_title'] = "Refund & Return Policy";
        $data['current_page'] = "Refund & Return Policy";
        $data['settings_row'] = $this->Setting_model->get_web_details()->refund_return_policy;
        $this->load->view('layouts/default', 'site/pages/page', $data); // :blush:
    }

    public function contact_us(){

        $this->load->model('Setting_model');

        $data = array();
        $data['page_title'] = 'Contact Us';
        $data['current_page'] = 'Contact Us';
        $data['contact_subjects'] = $this->common_model->select('tbl_contact_sub','DESC');
        $data['settings_row'] = $this->Setting_model->get_details();
        $this->load->view('layouts/default', 'site/pages/contact_us', $data); // :blush:
    }

    public function contact_form(){
        $this->load->helper("date");

        $data_arr = array(
            'contact_name' => $this->input->post('name'),
            'contact_email' => $this->input->post('email'),
            'contact_subject' => addslashes($this->input->post('subject_id')),
            'contact_msg' => addslashes($this->input->post('message')),
            'created_at' => strtotime(date('d-m-Y h:i:s A',now()))
        );

        $data_usr = $this->security->xss_clean($data_arr);

        $last_id = $this->common_model->insert($data_usr, 'tbl_contact_list');

        $this->email->from($this->app_email, $this->app_name);

        $this->email->to($this->contact_email); // replace it with receiver mail id

        $subject = $this->app_name.' - Contact';

        $this->email->subject($subject); // replace it with relevant subject

        $data_arr = array_merge($data_arr, array("subject"=>$this->common_model->selectByidParam($this->input->post('subject_id'),'tbl_contact_sub','title')));;

        $body = $this->load->view('admin/emails/contact_form.php',$data_arr,TRUE);

        $this->email->message($body); 

        if($this->email->send()){
           $message=array('success' => '1','msg' => $this->lang->line('contact_msg_success')); 
        }
        else{
            print_r($this->email->print_debugger());

            $message=array('success' => '0','msg' => $this->lang->line('error_data_save'));
        }

        echo json_encode($message);
    }


    public function convert_currency($to_currency, $from_currency, $amount){
        $req_url = 'https://api.exchangerate-api.com/v4/latest/'.$to_currency;
        $response_json = file_get_contents($req_url);

        $price=number_format($amount,2);

        if(false !== $response_json) {

            try {

            $response_object = json_decode($response_json);

            return $price = number_format(round(($amount * $response_object->rates->$from_currency), 2),2);

            }
            catch(Exception $e) {
                print_r($e);
            }

        }
    }
}
