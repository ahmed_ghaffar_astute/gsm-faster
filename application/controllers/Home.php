<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Home_Controller {

	public function __construct() {
		parent::__construct();
	
	}

	public function index(){
		$this->data['view'] = 'template/home' ;
		$this->load->view('template/layouts/default' , $this->data);
	  
	}

	public function news(){

	}

	public function imei_services(){
		if($this->data['rsStngs']->IMEIServices == '0'){
			redirect(base_url('home'));
		}
		$currId = $this->input->post("cId") ?: '0';
		$categoryId = $this->input->post("categoryId") ?: '';
		$catId = $this->input->post("catId") ?: '';

		$currId = sanitizeAndEscapingXXS($currId);
		$categoryId = sanitizeAndEscapingXXS($categoryId);
		$catId = sanitizeAndEscapingXXS($catId);

		$strAnd = '';
		$strWhere = '';
		if($categoryId != '')
		{
			$strWhere = " SEOURLName = '$categoryId'";
			$strAnd = " AND Cat.SEOURLName = '$categoryId'";
		}
		if(is_numeric($catId) && $catId != '0')
		{
			$strWhere = " CategoryId = '$catId'";
			$strAnd = " AND A.CategoryId = '$catId'";
		}
		if($strWhere != '')
		{
			$row = $this->Home_model->fetch_package_category_data($strWhere);

			$META_KW_CONTENTS = stripslashes($row->MetaKW);
			$META_DESC_CONTENTS = stripslashes($row->MetaTags);
			$TITLE = stripslashes($row->HTMLTitle);
		}
		else
		{
			$row = $this->Home_model->fetch_pages_data();

			$META_KW_CONTENTS = stripslashes($row->MetaKW);
			$META_DESC_CONTENTS = stripslashes($row->MetaTags);
			$TITLE = stripslashes($row->HTMLTitle);
		}

		$rsCats = $this->Home_model->fetch_package_cat_by_slbf();

		if($catId != '')
			$strWhere = " AND A.CategoryId = '$catId'";

		if(!isset($_SESSION['GSM_FUS_UserId']))
			$strWhere .= " AND HideServiceAtWeb = 0";

		$this->data['rsPackages'] = $this->Home_model->fetch_package_category_packages($strWhere);


		$strGWC = " AND IMEIPricesAtWeb = '1'";
		$SERVICE_TYPE = '0';

		$arrGroups = array();
		$strGroups = '';
		$rsGroups = $this->Home_model->fetch_price_plans($strGWC);

		foreach($rsGroups as  $row )
		{
			$arrGroups[$row->PricePlanId] = stripslashes($row->PricePlan);
			$strGroups .= '<th nowrap="nowrap">'.stripslashes($row->PricePlan).'</th>';
		}
		$arrGPrices = array();
		$rsGPrices = $this->Home_model->fetch_plans_packages_prices_currency($SERVICE_TYPE);

		foreach($rsGPrices as $row )
		{
			$arrGPrices[$row->PlanId][$row->PackageId] = $row->Price;
		}

		$this->data['strGroups'] = $strGroups;
		$this->data['arrGPrices'] = $arrGPrices;
		$this->data['arrGroups'] = $arrGroups;

		$this->data['view'] = 'template/imei_services' ;
		$this->load->view('template/layouts/default' , $this->data);
	}


	public function servicedtl(){
		$id = $this->input->post_get('id') ?: 0;
		if(!is_numeric($id))
			$strAnd = " SEOURLName = '$id'";
		if($id > 0)
			$strAnd = " PackageId = '$id'";

		$title_detail = '';
		$img_detail = '';
		$desc_detail = '';
		$imgURL = '';
		$rs_detail = $this->Home_model->fetch_packages_data($strAnd);
		
		if (isset($rs_detail->PackageTitle) && $rs_detail->PackageTitle != '')
		{
			$this->data['packId'] = $rs_detail->PackageId;
			$this->data['packTitle'] = stripslashes($rs_detail->PackageTitle);
			$this->data['packDesc'] =  stripslashes($rs_detail->MustRead);
			$this->data['delTime'] =  stripslashes($rs_detail->TimeTaken);
			if($rs_detail->MetaKW != '')
				$META_KW_CONTENTS = stripslashes($rs_detail->MetaKW);
			if($rs_detail->MetaTags != '')
				$META_DESC_CONTENTS = stripslashes($rs_detail->MetaTags);
			if($rs_detail->HTMLTitle != '')
				$SITE_TITLE = stripslashes($rs_detail->HTMLTitle);

			$this->data['packPrice'] = $rs_detail->PackagePrice;
			$this->data['packImg'] = $rs_detail->PackageImage;
			$rwCrncy = $this->Home_model->fetch_currency_data();
		
			if (isset($rwCrncy->CurrencySymbol) && $rwCrncy->CurrencySymbol != '')
			{
				$this->data['currSymbol'] = $rwCrncy->CurrencySymbol;
			}
		}
		$this->data['retailService'] = '' ;
		$this->data['view'] = 'template/service_dtl';
		$this->load->view('template/layouts/default' , $this->data);
	}

	public function file_services(){
		if($this->data['rsStngs']->FileServices == '0')
		{
			redirect(base_url('home'));
			
		}
		$currId = $this->input->post("cId") ?: '0';
		$categoryId = $this->input->post("categoryId") ?: '';
		$catId = $this->input->post("catId") ?: '';
		
		$strAnd = '';
		$strWhere = '';
		if($categoryId != '')
		{
			$strWhere = " SEOURLName = '$categoryId'";
			$strAnd = " AND Cat.SEOURLName = '$categoryId'";
		}
		if(is_numeric($catId) && $catId != '0')
		{
			$strWhere = " CategoryId = '$catId'";
			$strAnd = " AND A.CategoryId = '$catId'";
		}
		if($strWhere != '')
		{
			$row = $this->Home_model->fetch_package_category_data($strWhere);
			
			$META_KW_CONTENTS = stripslashes($row->MetaKW);
			$META_DESC_CONTENTS = stripslashes($row->MetaTags);
			$TITLE = stripslashes($row->HTMLTitle);
		}
		else
		{
			$row = $this->Home_model->fetch_pages_data();
		
			$META_KW_CONTENTS = stripslashes($row->MetaKW);
			$META_DESC_CONTENTS = stripslashes($row->MetaTags);
			$TITLE = stripslashes($row->HTMLTitle);
		}
		$this->data['rsCats'] = $this->Home_model->fetch_package_category_by_disablecategory();
		
		if($catId != '')
			$strWhere = " AND A.CategoryId = '$catId'";

		if(!isset($_SESSION['GSM_FUS_UserId']))
			$strWhere .= " AND HideServiceAtWeb = 0";

		$this->data['rsPackages'] = $this->Home_model->package_category_and_packages($strWhere);

		$strGWC = " AND FilePricesAtWeb = '1'";
		$SERVICE_TYPE = '1';

		$arrGroups = array();
		$strGroups = '';
		$rsGroups = $this->Home_model->fetch_price_plans($strGWC);
		
		foreach($rsGroups as $row )
		{
			$arrGroups[$row->PricePlanId] = stripslashes($row->PricePlan);
			$strGroups .= '<th nowrap="nowrap">'.stripslashes($row->PricePlan).'</th>';
		}
		$arrGPrices = array();
		$rsGPrices = $this->Home_model->fetch_plans_packages_prices_currency($SERVICE_TYPE);
		
		foreach($rsGPrices as $row )
		{
			$arrGPrices[$row->PlanId][$row->PackageId] = $row->Price;
		}

		$this->data['strGroups'] = $strGroups;
		$this->data['arrGroups'] = $arrGroups;
		$this->data['arrGPrices'] = $arrGPrices;

		$this->data['view'] = 'template/file_services' ;
		$this->load->view('template/layouts/default' , $this->data);
	
	}

	public function server_services(){
		if($this->data['rsStngs']->ServerServices == '0')
		{
			redirect(base_url('home'));
			
		}
		$currId = $this->input->post("cId") ?: '0';
		$categoryId = $this->input->post("categoryId") ?: '';
		$catId = $this->input->post("catId") ?: '';
	
		$strAnd = '';
		$strWhere = '';
		if($categoryId != '')
		{
			$strWhere = " SEOURLName = '$categoryId'";
			$strAnd = " AND Cat.SEOURLName = '$categoryId'";
		}
		if(is_numeric($catId) && $catId != '0')
		{
			$strWhere = " CategoryId = '$catId'";
			$strAnd = " AND A.CategoryId = '$catId'";
		}
		if($strWhere != '')
		{
			$row = $this->Home_model->fetch_log_package_cat_data($strWhere);
		
			$META_KW_CONTENTS = stripslashes($row->MetaKW);
			$META_DESC_CONTENTS = stripslashes($row->MetaTags);
			$TITLE = stripslashes($row->HTMLTitle);
		}
		else
		{
			$row = $this->Home_model->fetch_pages_data();
			
			$META_KW_CONTENTS = stripslashes($row->MetaKW);
			$META_DESC_CONTENTS = stripslashes($row->MetaTags);
			$TITLE = stripslashes($row->HTMLTitle);
		}

		$this->data['rsCats'] = $this->Home_model->fetch_log_pack_cat_by_disable_cat();
		

		if($catId != '')
			$strWhere = " AND A.CategoryId = '$catId'";

		if($this->session->userdata('GSM_FUS_UserId'))
			$strWhere .= " AND HideServiceAtWeb = 0";

		$this->data['rsPackages'] = $this->Home_model->fetch_log_pack_cat_and_packages($strWhere);
	
		$strGWC = " AND ServerPricesAtWeb = '1'";
		$SERVICE_TYPE = '2';

		$arrGroups = array();
		$this->data['arrGroups'] = $arrGroups;
		$this->data['view'] = 'template/server_services' ;
		$this->load->view('template/layouts/default' , $this->data);

	}

	public function server_service_dtl(){
		$packId = $this->input->post_get('packId') ?: 0;
		$id = $this->input->post_get('id') ?: 0;


	
		$this->data['packDesc'] =  ''  ;
		$this->data['delTime'] =  '' ;
		$this->data['packPrice']= '' ;
		$this->data['packTitle'] = '' ;
		$this->data['currSymbol'] = '' ;

		if(!is_numeric($id))
			$strAnd = " SEOURLName = '$id'";
		else
			$strAnd = " LogPackageId = '$id'";
		$rs_detail = $this->Home_model->fetch_log_packages_by_disablepack($strAnd);

		

		if (isset($rs_detail->LogPackageTitle) && $rs_detail->LogPackageTitle != '')
		{

			$this->data['packTitle'] = stripslashes($rs_detail->LogPackageTitle);
			
			if($rs_detail->MetaKW != '')
				$META_KW_CONTENTS = stripslashes($rs_detail->MetaKW);
			if($rs_detail->MetaTags != '')
				$META_DESC_CONTENTS = stripslashes($rs_detail->MetaTags);
			if($rs_detail->HTMLTitle != '')
				$SITE_TITLE = stripslashes($rs_detail->HTMLTitle);

			$this->data['packDesc'] =  stripslashes($rs_detail->LogPackageDetail);
			$this->data['delTime'] =  stripslashes($rs_detail->DeliveryTime);
			$this->data['packPrice'] = $rs_detail->LogPackagePrice;
			$rwCrncy = $this->Home_model->fetch_currency_data();
			
			if (isset($rwCrncy->CurrencySymbol) && $rwCrncy->CurrencySymbol != '')
			{
				$this->data['currSymbol'] = $rwCrncy->CurrencySymbol;
			}
		}

		$this->data['packImg'] = '';
		$this->data['view'] = 'template/server_service_dtl' ;
		$this->load->view('template/layouts/default' , $this->data);
	}

	public function page(){
		$id = $this->input->post_get('id') ?: 0;
		$this->data['pTitle'] = '';
		$this->data['pText'] = '';
		$strAnd = '' ;
		$pageImg = '';
		if(!is_numeric($id)){
			$strAnd = " SEOURLName = '$id'";
		}
		if($id > 0){
			$strAnd = " PageId = '$id'";
		}

		
		$row = $this->Home_model->fetch_page_data_by_id($strAnd);
	
		if (isset($row->PageTitle) && $row->PageTitle != '')
		{
			$this->data['pTitle'] = $row->PageTitle;
			$this->data['pText'] = stripslashes($row->PageText);
			if($row->MetaKW != '')
				$META_KW_CONTENTS = stripslashes($row->MetaKW);
			if($row->MetaTags != '')
				$META_DESC_CONTENTS = stripslashes($row->MetaTags);
			if($row->HTMLTitle != '')
				$SITE_TITLE = stripslashes($row->HTMLTitle);
			$pageImg = $row->Image;
			$this->data['actRtl'] = $row->ActivateRetail;
		}
		$this->data['rwPB'] = $this->Home_model->fetch_banner_data();
		
		$this->data['view']= 'template/page' ;

		$this->load->view('template/layouts/default' , $this->data);
		
	}

	public function faq(){
		$this->data['rsFaqs'] = $this->Home_model->fetch_faq_data();

		$this->data['view'] = 'template/faqs' ;
		$this->load->view('template/layouts/default' , $this->data);
		
	}

}
