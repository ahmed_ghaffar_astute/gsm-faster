<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
	}


	public function threshold_amount()
	{

		
		$this->data['amount_row'] = $this->User_model->threshold_amount();
		
		$this->data['view'] = 'threshold_amount';
		$this->load->view('layouts/default', $this->data);

	}

	public function ajx_threshold_amount(){
		$purpose = $this->input->post('purpose');
		$msg = '';
		if ($purpose == 'thdamount') {
			$amount = $this->input->post('amount');
			$user_id = $this->session->userdata('GSM_FUS_UserId');
			$result = $this->User_model->update_threshold_amount($amount, $user_id);
		
			$message = $this->lang->line('CUST_GEN_MSG') ;

			echo $message ;
		}
	}

	public function add_credits()
	{

		$CONVERSION_RATE = 1;
		$comments = $strBlockedPIds = '';
		$this->data['message'] = '';
		$rwBPMs = $this->User_model->get_blocked_ips();
		if (isset($rwBPMs->BlockedPMethodIds) && $rwBPMs->BlockedPMethodIds != '') {
			$strBlockedPIds = " AND PaymentMethodId NOT IN ($rwBPMs->BlockedPMethodIds)";
		}
		$this->data['rsPMethods'] = $this->User_model->get_payment_details_3($strBlockedPIds);

		if ($this->input->post_get('pId')) {
			$pId = $this->input->post_get('pId');
			$description = '';
			$row = $this->User_model->get_payment_desc($pId);
			if (isset($row->Description) && $row->Description != '')
				$this->data['message'] = '<p>' . stripslashes($row->Description) . '</p>';
		}

		if ($this->input->post_get('mc')) {
			$this->data['message'] = '<p>Invalid value for credits!</p>';
		}

		$minCr = $this->data['settings']->MinCredits;
		$maxCr = $this->data['settings']->MaxCredits;

		if (is_numeric($this->data['settings']->MinCredits))
			$minCr = $minCr * $this->data['userDetails']->ConversionRate;
		if (is_numeric($this->data['settings']->MaxCredits))
			$maxCr = $maxCr * $this->data['userDetails']->ConversionRate;

		$this->data['minCr'] = $minCr;
		$this->data['maxCr'] = $maxCr;


		$this->data['view'] = 'add_credits';
		$this->load->view('layouts/default', $this->data);
	}

	public function submitpayment()
	{

		$baseURL = base_url();
		$dtTm = setDtTmWRTYourCountry();
		$offerId = 0;
		if ($this->input->post_get('isOffer')) {
			// IS OFFER
			$credits = $this->input->post_get('txtBOCredits');
			$amount = $this->input->post_get('grdTotalBO');
			$paymentMethod = $this->input->post_get('offerType');
			$offerId = $this->input->post_get('offerId');
		} else {
			// IS NOT AN OFFER
			$credits = $this->input->post_get('txtCredits');
			//$amount = check_input($_REQUEST['grdTotal'], $objDBCD14->dbh);
			$paymentMethod = $this->input->post_get('pMethod');
		}

		$rwStngs = $this->User_model->get_min_max_credits();
		$MIN_CREDITS = $rwStngs->MinCredits * $this->input->post('clcr');
		$MAX_CREDITS = $rwStngs->MaxCredits * $this->input->post('clcr');

		if ($credits < $MIN_CREDITS)
			redirect(base_url('page/add_credits?mc=1'));
		if ($credits > $MAX_CREDITS)
			redirect(base_url('page/add_credits?mc=1'));

		$rstrctdCurrId = 0;
		$rstrctdCurr = '';
		$vatPercentage = 0;
		$myCurrency = $this->input->post_get('myCurrency') ?: '';
		$row = $this->User_model->get_payment_and_method_detail($paymentMethod);
		if (isset($row->PaymentMethodId) && $row->PaymentMethodId != '') {
			$accountId = $row->Username;
			$percentage = $credits * ($row->Fee / 100);
			$amount = roundMe($percentage + $credits);
			$payTypeId = $row->PayMethodTypeId;
			$API_UserName = $row->APIUsername;
			$API_Password = $row->APIPassword;
			$API_Signature = $row->APISignature;
			$pmntMthd = $row->PaymentMethod;
			if ($row->RestrictedCurrency > 0) {
				$rstrctdCurrId = $row->RestrictedCurrency;
				$rstrctdCurr = $row->CurrencyAbb;
			}
			/***********************************************************************/
			if ($row->Vat > 0) {
				$vatPercentage = $amount * ($row->Vat / 100);
				$amount = roundMe($percentage + $credits + $vatPercentage);
			}
			/***********************************************************************/
		} else {
			$this->data['error'] = "<br /><br />Wrong Payment Method";
		}

		if ($credits > 0 && $amount > 0) {
			$row = $this->User_model->get_user_email_settings();
			if (isset($row->FromName) && $row->FromName != '') {
				$frmName = $row->FromName;
			}
		}
		$this->crypt_key($this->session->userdata('GSM_FUS_UserId'));
		$encCredits = $this->encrypt($credits);
		$encAmount = $this->encrypt($amount);

		$insert_array = array(
			'Credits' => $encCredits,
			'Amount' => $encAmount,
			'UserId' => $this->user_id,
			'PaymentMethod' => $paymentMethod,
			'PaymentDtTm' => $dtTm,
			'OfferId' => $offerId,
			'Currency' => $myCurrency,
			'AccountId' => $accountId,
			'Vat' => $vatPercentage
		);

		$invId = $this->User_model->insert_payment_details($insert_array);
		$arr = getEmailDetails();
		//Bank transfer = 3 , WesternUnion = 7 , MoneyGram = 6 ,
		//PayPal
		if ($payTypeId == '1') {
			$autoFill = 0;
			$row = $this->User_model->get_autocredits();
			if (isset($row->AutoFillCredits) && $row->AutoFillCredits != '')
				$autoFill = $row->AutoFillCredits;

			$successRetPage = $baseURL . 'dashboard';
			$cancelRetPage = $baseURL . 'page/order_cancelled';
			$itmNm = 'Add Store Credits';
			echo "	<html><body><form name='frmOrder' id='frmOrder' action='https://www.paypal.com/cgi-bin/webscr' method='post'>
                    <input type='hidden' name='rm' value='2' />
                    <input type='hidden' name='no_shipping' value='2' />
                    <input type='hidden' name='cmd' value='_xclick'>
                    <input type='hidden' name='business' value='" . $accountId . "'>
                    <input type='hidden' name='item_name' value='" . $itmNm . "'>
                    <input type='hidden' name='item_number' value='" . $invId . "'>
                    <input type='hidden' name='amount' value=" . roundMe($amount) . ">
                    <input type='hidden' name='currency_code' value='" . $myCurrency . "'>
                    <input type='hidden' name='charset' value='utf-8'>
                    <input type='hidden' name='return' value='" . $successRetPage . "'>
                    <input type='hidden' name='cancel_return' value='" . $cancelRetPage . "'>";
			if ($autoFill == 1) {
				$notificationPage = $baseURL . 'page/paymentnotification';
				echo "<input type='hidden' name='notify_url' value='" . $notificationPage . "'>";
			} else {
				$notificationPage = $baseURL . 'page/paymentnotificationviaemailonly';
				echo "<input type='hidden' name='notify_url' value='" . $notificationPage . "'>";
			}
			echo "</form></body></html><script language='javascript' type='text/javascript'>document.getElementById('frmOrder').submit();</script>";
			exit();
		} //Mass Paypal
		else if ($payTypeId == '2') {
			invoiceEmail($this->session->userdata('UserEmail'), $this->session->userdata('UserName'), $invId, $credits, $amount, $myCurrency, $dtTm, $pmntMthd);
			redirect(base_url('page/invoice?id=' . $invId));
		} //Money Bookers
		else if ($payTypeId == '5') {
			$autoFill = 0;
			$row = $this->User_model->get_autocredits();
			if (isset($row->AutoFillCredits) && $row->AutoFillCredits != '')
				$autoFill = $row->AutoFillCredits;
			$successRetPage = $baseURL . 'page/order_placed';
			$cancelRetPage = $baseURL . 'page/order_cancelled';
			$itmNm = $frmName . ' Credits';
			echo "	<html><body><form name='frmOrder' id='frmOrder' action='https://www.moneybookers.com/app/payment.pl' method='post'>
                    <input type='hidden' name='pay_to_email' value='" . $accountId . "'>
                    <input type='hidden' name='language' value='EN'>
                    <input type='hidden' name='amount' value=" . roundMe($amount) . ">
                    <input type='hidden' name='currency' value='" . $myCurrency . "'>
                    <input type='hidden' name='detail1_description' value='" . $itmNm . "'>
                    <input type='hidden' name='detail1_text' value='" . $itmNm . "'>";
			if ($autoFill == 1) {
				$notificationPage = $baseURL . 'ipn_skrill.php?pId=' . urlsafe_b64encode($invId);
				echo "<input type='hidden' name='status_url' value='" . $notificationPage . "'>";
			}
			echo "	</body></html></form><script language='javascript' type='text/javascript'>document.getElementById('frmOrder').submit();</script>";
			exit();
		} else if ($payTypeId == '9') {
			// ==================================
			// PayPal Express Checkout Module
			// ==================================

			//'------------------------------------
			//' The paymentAmount is the total value of
			//' the shopping cart, that was set
			//' earlier in a session variable
			//' by the shopping cart page
			//'------------------------------------

			$this->session->set_userdata("Payment_Amount", $amount);
			$this->session->set_userdata("API_UserName", $API_UserName);
			$this->session->set_userdata("API_Password", $API_Password);
			$this->session->set_userdata("API_Signature", $API_Signature);

			//'------------------------------------
			//' The currencyCodeType and paymentType
			//' are set to the selections made on the Integration Assistant
			//'------------------------------------
			//$myCurrency = "USD";

			//'------------------------------------
			//' The returnURL is the location where buyers return to when a
			//' payment has been succesfully authorized.
			//'
			//' This is set to the value entered on the Integration Assistant
			//'------------------------------------
			$returnURL = $baseURL . 'page/ppexpresscheckoutrvw';

			//'------------------------------------
			//' The cancelURL is the location buyers are sent to when they hit the
			//' cancel button during authorization of payment during the PayPal flow
			//'
			//' This is set to the value entered on the Integration Assistant
			//'------------------------------------
			$cancelURL = $baseURL . 'page/order_cancelled';

			//'------------------------------------
			//' Calls the SetExpressCheckout API call
			//'
			//' The CallShortcutExpressCheckout function is defined in the file PayPalFunctions.php,
			//' it is included at the top of this file.
			//'-------------------------------------------------
			$amount = roundMe_2Digits($amount);
			$resArray = CallShortcutExpressCheckout($amount, $myCurrency, "Sale", $returnURL, $cancelURL, $invId, 'Add Store Credits');

			$ack = strtoupper($resArray["ACK"]);
			if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
				RedirectToPayPal($resArray["TOKEN"]);
			} else {
				//Display a user friendly Error on the page using any of the following error information returned by PayPal
				$ErrorCode = urldecode($resArray["L_ERRORCODE0"]);
				$ErrorShortMsg = urldecode($resArray["L_SHORTMESSAGE0"]);
				$ErrorLongMsg = urldecode($resArray["L_LONGMESSAGE0"]);
				$ErrorSeverityCode = urldecode($resArray["L_SEVERITYCODE0"]);

				$this->data['error'] = "SetExpressCheckout API call failed. ";
				$this->data['Detailed_Error_Message'] = "Detailed Error Message: " . $ErrorLongMsg;
				$this->data['Short_Error_Message'] = "Short Error Message: " . $ErrorShortMsg;
				$this->data['Error_Code'] = "Error Code: " . $ErrorCode;
				$this->data['Error_Severity_Code'] = "Error Severity Code: " . $ErrorSeverityCode;
			}
		} else if ($payTypeId == '10') // DAL PAY
		{
			$itmNm = $frmName . ' Credits';
			$val = $invId . '|' . $paymentMethod;
			echo "	<html><body><form name='frmOrder' id='frmOrder' action='https://secure.dalpay.is/cgi-bin/order2/processorder1.pl' method='post'>
                    <INPUT NAME='mer_id' TYPE='HIDDEN' VALUE='" . $accountId . "'>
                    <INPUT NAME='pageid' TYPE='HIDDEN' VALUE='1'>
                    <INPUT NAME='cust_country_code' TYPE='HIDDEN' VALUE='" . $myCurrency . "'>
                    <INPUT NAME='cust_email' TYPE='HIDDEN' VALUE='" . $_SESSION['UserEmail'] . "'>
                    <INPUT NAME='item1_desc' TYPE='HIDDEN' VALUE='" . $itmNm . "'>
                    <INPUT NAME='item1_price' TYPE='HIDDEN' VALUE='" . roundMe($amount) . "'>
                    <INPUT NAME='item1_qty' TYPE='HIDDEN' VALUE='1'>
                    <INPUT NAME='user1' TYPE='HIDDEN' VALUE='" . $itmNm . "'>
                    <INPUT NAME='user2' TYPE='HIDDEN' VALUE='$val'>";
			echo "	</body></html></form><script language='javascript' type='text/javascript'>document.getElementById('frmOrder').submit();</script>";
			exit;
		} else if ($payTypeId == '11')// STRIPE
		{
			$itmNm = $frmName . ' Credits';
			echo "	<html><body>" . form_open(base_url('page/stripe_credits'), "name=frmOrder id=frmOrder") . "
                    <input type='hidden' name='amount' value='" . $amount . "'/>
                    <input type='hidden' name='service' value='" . $itmNm . "'/>
                    <input type='hidden' name='pMId' value='" . $paymentMethod . "'/>
                    <input type='hidden' name='invId' value='" . $invId . "'>
                    <input type='hidden' name='currency' value='" . $myCurrency . "'/>";
			echo form_close() . "</body></html><script language='javascript' type='text/javascript'>document.getElementById('frmOrder').submit();</script>";
			exit;
		} else if ($payTypeId == '12')// PAYPAL PRO
		{
			$itmNm = $frmName . ' Credits';
			echo "	<html><body>" . form_open(base_url('page/paypalpro'), "name=frmOrder id=frmOrder") . "
                    <input type='hidden' name='amount' value='" . $amount . "'/>
                    <input type='hidden' name='service' value='" . $itmNm . "'/>
                    <input type='hidden' name='pMId' value='" . $paymentMethod . "'/>
                    <input type='hidden' name='invId' value='" . $invId . "'>
                    <input type='hidden' name='currency' value='" . $myCurrency . "'/>";
			echo form_close() . "</body></html><script language='javascript' type='text/javascript'>document.getElementById('frmOrder').submit();</script>";
			exit;
		} else if ($payTypeId == '13') // WORLD PAY
		{
			$itmNm = $frmName . ' Credits';
			$val = $invId . '|' . $paymentMethod;
			echo "	<html><body><form name='frmOrder' id='frmOrder' action='https://secure.worldpay.com/wcc/purchase' method='post'>
                    <INPUT NAME='instId' TYPE='HIDDEN' VALUE='" . $accountId . "'>
                    <input type='hidden' name='cartId' value='" . $invId . "'>
                    <INPUT NAME='currency' TYPE='HIDDEN' VALUE='" . $myCurrency . "'>
                    <INPUT NAME='desc' TYPE='HIDDEN' VALUE='" . $itmNm . "'>
                    <INPUT NAME='amount' TYPE='HIDDEN' VALUE='" . roundMe($amount) . "'>
                    <INPUT NAME='name' TYPE='HIDDEN' VALUE='" . $itmNm . "'>";
			echo "	</form></body></html><script language='javascript' type='text/javascript'>document.getElementById('frmOrder').submit();</script>";
			exit;
		} else if ($payTypeId == '14')// BITMAP
		{
			$itmNm = $frmName . ' Credits';
			echo "	<html><body>" . form_open(base_url('page/bitmap'), "name=frmOrder id=frmOrder") . "
                    <input type='hidden' name='amount' value='" . $amount . "'/>
                    <input type='hidden' name='service' value='" . $itmNm . "'/>
                    <input type='hidden' name='pMId' value='" . $paymentMethod . "'/>
                    <input type='hidden' name='invId' value='" . $invId . "'>
                    <input type='hidden' name='rstrctdCurrId' value='" . $rstrctdCurrId . "'>
                    <input type='hidden' name='currency' value='" . $myCurrency . "'/>";
			echo form_close() . "</body></html><script language='javascript' type='text/javascript'>document.getElementById('frmOrder').submit();</script>";
			exit;
		} else if ($payTypeId == '15')// URL REDIRECTION
		{
			redirect($accountId);
		} else {
			invoiceEmail($this->session->userdata('UserEmail'), $this->session->userdata('UserName'), $invId, $credits, $amount, $myCurrency, $dtTm, $pmntMthd);

			redirect(base_url('page/add_credits?pId=' . $paymentMethod));
		}

		$this->data['view'] = 'submit_payment';
		$this->load->view('layouts/default', $this->data);

	}

	public function bitmap()
	{
		$rstrctdCurrId = $this->input->post('rstrctdCurrId') ?: '';
		$cRate = '';
		$cCurr = 1;
		if (($this->input->post('invId')) && ($this->input->post('pMId'))) {
			$invId = $this->input->post('invId') ?: '0';
			$amount = $this->input->post('amount') ?: '0';
			$srvcTitle = $this->input->post('service') ?: '';
			$pMId = $this->input->post('pMId') ?: '0';
			$currency = $this->input->post('currency') ?: '';

			if ($invId == '0') {
				redirect(base_url('page/add_credits'));
			}
		}
		if ($rstrctdCurrId > 0) {
			$rwCurr = $this->User_model->get_curreny_data($rstrctdCurrId);
			if (isset($rwCurr->CurrencyAbb) && $rwCurr->CurrencyAbb != '') {
				$cCurr = $rwCurr->CurrencyAbb;
				$cRate = $rwCurr->ConversionRate;
			}
		}
		if (($this->input->post('invId')) && ($this->input->post('txtLName')) && ($this->input->post('btnPPPro'))) {
			$invId = ($this->input->post('invId')) ? $purifier->purify(check_input($this->input->post('invId'), $objDBCD14->dbh)) : '0';

			$rs = $this->User_model->get_payment_on_status($invId);
			if (isset($rs->PaymentId) && $rs->PaymentId != '') {

				$userId = $this->user_id;
				@$this->crypt_key($userId);
				$ÌNV_CREDITS = $this->decrypt($rs->Credits);
				if ($ÌNV_CREDITS == '')
					$ÌNV_CREDITS = '0';
				$INV_AMOUNT = $this->decrypt($rs->Amount);
				$INV_CURRENCY = stripslashes($rs->Currency);
				$PAYMENT_METHOD = stripslashes($rs->PaymentMethod);
				$PAYMENT_METHOD_ID = $rs->PaymentMethodId;
				$INV_DT = $rs->PaymentDtTm;
				$foreignAm = $INV_AMOUNT;
				$foreignCurr = $INV_CURRENCY;
				if ($rstrctdCurrId > 0) {
					$INV_CURRENCY = $cCurr;
					$INV_AMOUNT = roundMe_2Digits($INV_AMOUNT * $cRate);
				}
				include APPPATH . 'script/process_bitmap.php';
			}
		}

		$this->data['view'] = 'bitmap';
		$this->load->view('layouts/default', $this->data);
	}

	public function paypalpro()
	{
		if (($this->input->post('invId')) && ($this->input->post('pMId'))) {
			$invId = $this->input->post('invId') ?: '0';
			$amount = $this->input->post('amount') ?: '0';
			$srvcTitle = $this->input->post('service') ?: '';
			$pMId = $this->input->post('pMId') ?: '0';
			$currency = $this->input->post('currency') ?: '';
			if ($invId == '0') {
				redirect(base_url('page/add_credits'));
			}
		}
		if (($this->input->post('invId')) && ($this->input->post('carholder_name')) && ($this->input->post('card_number'))) {
			$invId = $this->input->post('invId') ?: '0';

			$rs = $this->User_model->get_payment_on_status($invId);
			if (isset($rs->PaymentId) && $rs->PaymentId != '') {

				$userId = $this->session->userdata('GSM_FUS_UserId');
				$this->crypt_key($userId);
				$ÌNV_CREDITS = $crypt->decrypt($rs->Credits);
				if ($ÌNV_CREDITS == '')
					$ÌNV_CREDITS = '0';
				$INV_AMOUNT = $crypt->decrypt($rs->Amount);
				$INV_CURRENCY = stripslashes($rs->Currency);
				$PAYMENT_METHOD = stripslashes($rs->PaymentMethod);
				$PAYMENT_METHOD_ID = $rs->PaymentMethodId;
				$INV_DT = $rs->PaymentDtTm;

				include APPPATH . 'scripts/process_paypalpro.php';
			}
		}
		$this->data['view'] = 'stripe_credits';
		$this->load->view('layouts/default', $this->data);

	}


	public function stripe_credits()
	{
		if (($this->input->post('invId'))) {
			$this->data['invId'] = $this->input->post('invId') ?: '0';
			$this->data['amount'] = $this->input->post('amount') ?: '0';
			$this->data['srvcTitle'] = $this->input->post('service') ?: '';
			$this->data['pMId'] = $this->input->post('pMId') ?: '0';
			$this->data['currency'] = $this->input->post('currency') ?: '';
		}
		$dKey = '';
		$row = $this->User_model->get_api_password($pMId);
		if (isset($row->APIPassword) && $row->APIPassword != '') {
			$this->data['dKey'] = $row->APIPassword;
		}

		$this->data['view'] = 'stripe_credits';
		$this->load->view('layouts/default', $this->data);
	}


	public function charge_credits()
	{
		@header('P3P: CP="CAO PSA OUR"');
		$pMId = $this->input->post('pMId') ?: '0';
		$amount = $this->input->post('amount') ?: '0';
		$currency = $this->input->post('currency') ?: '';
		$invId = $this->input->post('invId') ?: '0';
		$apiKey = '';

		$row = $this->User_model->get_apiusername($pMId);
		if (isset($row->APIUserName) && $row->APIUserName != '') {
			$apiKey = $row->APIUserName;
		}
		try {
			require_once(APPPATH . 'libraries/stripe/stripe/lib/Stripe.php');

			Stripe::setApiKey($apiKey);

			$stripeResponse = Stripe_Charge::create(array(
				"amount" => (int)$amount * 100,
				"currency" => $currency,
				"card" => $this->input->post('stripeToken'),
				"description" => "Order #" . $invId
			));
			if (($this->input->post('invId'))) {
				if ($stripeResponse['amount_refunded'] == 0 && empty($stripeResponse['failure_code']) && $stripeResponse['paid'] == 1 && $stripeResponse['captured'] == 1 && $stripeResponse['status'] == 'succeeded') {
					$transactionId = $stripeResponse["balance_transaction"];
					$rs = get_payment_method_by_type_id($invId);

					if (isset($rs->UserId) && $rs->UserId != '') {

						$userId = $rs->UserId;
						$this->crypt_key($userId);
						$myCredits = $this->decrypt($rs->Credits);
						if ($myCredits == '')
							$myCredits = '0';
						$INV_AMOUNT = $this->decrypt($rs->Amount);
						$INV_CURRENCY = stripslashes($rs->Currency);
						$PAYMENT_METHOD = stripslashes($rs->PaymentMethod);
						$INV_DT = $rs->PaymentDtTm;

						if ($INV_AMOUNT == $PAYMENT_AMOUNT && $receiver_email == $rs->UserName) {
							$credits = 0;
							$userName = '';
							$userEmail = '';
							$rsCredits = $this->User_model->get_user_details($userId);
							if (isset($rsCredits->Credits) && $rsCredits->Credits != '') {
								$credits = $rsCredits->Credits;
								$userName = $rsCredits->Name;
								$userEmail = $rsCredits->UserEmail;
								$uName = $rsCredits->UserName;
								$autoFill = $rsCredits->AutoFillCredits;
							}
							if ($autoFill == '1') {
								$decCredits = $this->decrypt($credits);
								$decCredits += $myCredits;
								$encCredits = $this->encrypt($decCredits);

								$this->User_model->update_user_credits($userId);
								$dtTm = setDtTmWRTYourCountry();


								$insert_data = array(
									'UserId' => $userId,
									'Credits' => $myCredits,
									'Description' => "+ Add Funds (Invoice #" . $invId . ')',
									'IMEINo' => '',
									'HistoryDtTm' => $dtTm,
									'CreditsLeft' => $encCredits,
									'PaymentId' => $invId
								);
								$this->User_model->insert_credit_history($insert_data);
								$update_data = array(
									'ReceiverEmail' => $receiver_email,
									'PayerEmail' => $payer_email,
									'TransactionId' => $transactionId,
									'PaymentStatus' => 2,
									'CreditsTransferred' => 1,
									'UpdatedAt' => $dtTm,
									'ShippingAddress' => $shippingAddr,
								);
								$this->User_model->update_gf_payments($update_data, $invId);
								send_push_notification($myCredits . ' Credits have been added in your account!', $userId);

								$arr = getEmailDetails();
								//========================== CHECK LAST PAYER EMAIL ================================================//
								if ($payer_email != '')
									checkLastPayerEmail($invId, $userId, $payer_email, $arr, $uName);
								//========================== CHECK LAST PAYER EMAIL ================================================//
								if ($userEmail != '') {
									invoiceEmail($userEmail, $userName, $invId, $myCredits, $INV_AMOUNT, $INV_CURRENCY, $INV_DT, $PAYMENT_METHOD, 1);
									invoiceEmail($arr[4], 'Admin', $invId, $myCredits, $INV_AMOUNT, $INV_CURRENCY, $INV_DT, $PAYMENT_METHOD, 1, $uName);
								}
							}
						}
					}
				}
				redirect(base_url('page/client_invoices'));
			}
		} catch (Stripe_CardError $e) {
			$this->data['Stripe_CardError'] = '<div class="center"><img src="close.png"/><br><p style="text-align:center;">Votre paiement pour cette transaction à échoué, SVP veuillez recommencer !</p><a class="center button" href="' . base_url('page/add_credits') . '">Try Again!</a></div>';
		} catch (Stripe_InvalidRequestError $e) {
			// Invalid parameters were supplied to Stripe's API
			$this->data['Stripe_InvalidRequestError'] = '<div class="center"><img src="close.png"/><br><p style="text-align:center;">Votre paiement pour cette transaction à échoué, SVP veuillez recommencer !</p><a class="center button" href="' . base_url('page/add_credits') . '">Please Try Again!</a></div>';
		} catch (Stripe_ApiConnectionError $e) {
			// Network communication with Stripe failed
			$this->data['Stripe_ApiConnectionError'] = '<div class="center"><img src="close.png"/><br><p style="text-align:center;">Votre paiement pour cette transaction à échoué, SVP veuillez recommencer !</p><a class="center button" href="' . base_url('page/add_credits') . '">Try Again3!</a></div>';
		} catch (Stripe_Error $e) {
			$this->data['Stripe_Error'] = '<div class="center"><img src="close.png"/><br><p style="text-align:center;">Votre paiement pour cette transaction à échoué, SVP veuillez recommencer !</p><a class="center button" href="' . base_url('page/add_credits') . '">Try Again4!</a></div>';
			// Display a very generic error to the user, and maybe send
			// yourself an email
		} catch (Exception $e) {
			$this->data['Exception'] = '<div class="center"><img src="close.png"/><br><p style="text-align:center;">Votre paiement pour cette transaction à échoué, SVP veuillez recommencer !</p><a class="center button" href="' . base_url('page/add_credits') . '">Try Again5!</a></div>';
			// Something else happened, completely unrelated to Stripe
		}

		$this->data['view'] = 'charge_credits';
		$this->load->view('layouts/default', $this->data);

	}


	public function invoice()
	{
		$INV_LOGO = '';
		$validTransaction = false;
		$amountPaid = 0;
		$id = $this->input->post_get('id') ?: '0';
		$this->data['id'] = $id;
		if ($id > 0) {

			$row = $this->User_model->get_payments_detail($id);

			if (isset($row->PaymentId) && $row->PaymentId != '') {
				$UserId = $row->UserId;
				$this->data['UserId'] = $UserId;
				$Credits = $row->Credits;
				$this->data['Credits'] = $Credits;
				$Amount = $row->Amount;
				$this->data['Amount'] = $Amount;
				$payerEmail = $row->PayerEmail == '' ? '-' : $row->PayerEmail;
				$this->data['pMethod'] = $row->PaymentMode;
				$pStatusId = $row->PaymentStatus;
				$this->data['pStatusId'] = $pStatusId;
				$transactionId = $row->TransactionId;
				$comments = stripslashes($row->Comments);
				$this->data['invDate'] = $row->InvoiceDate;
				$this->data['currency'] = $row->Currency;
				$this->data['paidDtTm'] = $row->PaidDtTm;
				$this->data['byAdmin'] = $row->ByAdmin;
				$this->data['balance'] = $row->PayableAmount == '' ? 0 : $row->PayableAmount;
				$validTransaction = true;
				$pMethodId = $row->PaymentMethodId;
				$this->data['pMethodId'] = $pMethodId;
				$pMethodTypeId = $row->PayMethodTypeId;
				$this->data['pMethodTypeId'] = $pMethodTypeId;
				$vat = $row->Vat;
			}

			$this->data['message'] = masspayment($payerEmail, $UserId, $Credits, $Amount, $transactionId, $pStatusId, $pMethodTypeId, $pMethodId, $this->data['userDetails']->CurrencyAbb, $id);
			$row = $this->User_model->get_alluser_data();
			if (isset($row->UserId) && $row->UserId != '') {
				$email = $row->UserEmail;
				$firstName = stripslashes($row->FirstName);
				$lastName = stripslashes($row->LastName);
				$address = stripslashes($row->Address);
				$phone = stripslashes($row->Phone);
				$country = stripslashes($row->Country);
				$city = stripslashes($row->City);
				$state = stripslashes($row->State);
				$zip = stripslashes($row->Zip);
			}
			$rsInvLogo = $this->User_model->get_logo_path();
			if (isset($rsInvLogo->LogoPath) && $rsInvLogo->LogoPath != '')
				$INV_LOGO = "uplds" . $this->session->userdata('THEME') . "/" . $rsInvLogo->LogoPath;

			$this->data['amountPaid'] = '';
			$rsPP = $this->User_model->get_sum_invamount($id);
			if (isset($rsPP->AmountPaid) && $rsPP->AmountPaid != '')
				$this->data['amountPaid'] = $rsPP->AmountPaid;

			$this->data['rsPayments'] = $this->User_model->get_payments_count($id);
		}
		$this->data['rsPPRcvrs'] = $this->User_model->get_reciever_info();

		$this->data['ADMIN_COMPANY'] = $this->data['settings']->Company;
		$this->data['ADMIN_ADD'] = $this->data['settings']->Address;
		$this->data['ADMIN_EMAIL'] = $this->data['settings']->ToEmail;
		$this->data['INV_PAY_TO'] = $this->data['settings']->PayTo;

		$this->data['view'] = 'invoice';
		$this->load->view('layouts/default', $this->data);

	}

	public function paymentnotificationviaemailonly()
	{
		$paymentId = '0';
		$req = 'cmd=_notify-validate';
		$post = $this->input->post();
		foreach ($post as $key => $value) {
			$value = urlencode(stripslashes($value));
			$req .= "&$key=$value \n";
			if ($key == "item_number")
				$paymentId = $value;
			else if ($key == "payer_email")
				$payer_email = str_replace('%40', '@', $value);
			if ($key == "txn_id")
				$transactionId = $value;
		}
		$header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
		$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
		$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
		$fp = fsockopen('ssl://www.paypal.com', 443, $errno, $errstr, 30);
		$receiver_email = $this->input->post('receiver_email');
		if (!$fp) {
			echo 'HTTP ERROR';
		} else {
			fputs($fp, $header . $req);
			while (!feof($fp)) {
				$res = fgets($fp, 1024);
				if (strcmp($res, "VERIFIED") == 0) {
					if ($paymentId != '0') {
						$rs = $this->User_model->get_user_payments($paymentId);
						if (isset($rs->UserId) && $rs->UserId != '') {
							$userName = $rs->UserName;
							$userEmail = $rs->UserEmail;

							$userId = $rs->UserId;
							$this->crypt_key($userId);
							$myCredits = $this->decrypt($rs->Credits);
							if ($myCredits == '')
								$myCredits = '0';
							$INV_AMOUNT = $this->decrypt($rs->Amount);
							$INV_CURRENCY = stripslashes($rs->Currency);
							$INV_DT = $rs->PaymentDtTm;

							$dtTm = setDtTmWRTYourCountry();
							$update_data = array(
								'ReceiverEmail' => $receiver_email,
								'PayerEmail' => $payer_email,
								'TransactionId' => $transactionId,
								'UpdatedAt' => $dtTm,
								'PaymentStatus' => 5,
							);
							$this->User_model->update_gf_payments($update_data, $paymentId);

							$arr = getEmailDetails();
							//========================== CHECK LAST PAYER EMAIL ================================================//
							if ($payer_email != '')
								checkLastPayerEmail($paymentId, $userId, $payer_email, $arr, $userName);
							//========================== CHECK LAST PAYER EMAIL ================================================//
							if ($userEmail != '') {
								invoiceEmail($arr[4], 'Admin', $paymentId, $myCredits, $INV_AMOUNT, $INV_CURRENCY, $INV_DT, 'PayPal', 5, $userName);
							}
						}
					}
				}
			}

			fclose($fp);
		}
	}

	public function paymentnotification()
	{
		$transactionId = '';
		$paymentId = '0';
		$address = '';
		$city = '';
		$state = '';
		$zip = '';
		$country = '';
		$payer_status = '';
		$payment_status = '';
		$req = 'cmd=_notify-validate';
		$post = $this->input->post();
		foreach ($post as $key => $value) {
			$value = urlencode(stripslashes($value));
			$req .= "&$key=$value \n";
			if ($key == "txn_id")
				$transactionId = $value;
			else if ($key == "item_number")
				$paymentId = $value;
			else if ($key == "payer_email")
				$payer_email = str_replace('%40', '@', $value);
			else if ($key == "address_street")
				$address = str_replace('%40', '@', $value);
			else if ($key == "address_city")
				$city = str_replace('%40', '@', $value);
			else if ($key == "address_state")
				$state = str_replace('%40', '@', $value);
			else if ($key == "address_zip")
				$zip = str_replace('%40', '@', $value);
			else if ($key == "address_country")
				$country = str_replace('%40', '@', $value);
			else if ($key == "payer_status")
				$payer_status = trim(str_replace('%40', '@', $value));
			else if ($key == "payment_status")
				$payment_status = trim(str_replace('%40', '@', $value));
			else if ($key == "mc_gross")
				$PAYMENT_AMOUNT = trim($value);
			$str .= $req;
		}

		$shippingAddr = $address . ' ' . $city . ' ' . $state . ' ' . $zip . ' ' . $country;
		$receiver_email = $this->input->post('receiver_email');

		$url = "http://www.paypal.com/cgi-bin/webscr";
		$ch = curl_init();    // Starts the curl handler
		curl_setopt($ch, CURLOPT_URL, $url); // Sets the paypal address for curl
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Returns result to a variable instead of echoing
		//curl_setopt($ch, CURLOPT_TIMEOUT, 3); // Sets a time limit for curl in seconds (do not set too low)
		curl_setopt($ch, CURLOPT_POST, 1); // Set curl to send data using post
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req); // Add the request parameters to the post
		$result = curl_exec($ch); // run the curl process (and return the result to $result
		curl_close($ch);

		if ((strcmp($result, "VERIFIED") == 0 || strcmp(trim($result, "VERIFIED")) == 0 || strpos($result, "VERIFIED") !== 0) && $payment_status == 'Completed') {
			//sendMail22('hammad.akbar@gmail.com', 'PayPal IPN - GSMFASTEST', 'Support@gsmfastest.com', 'PayPal IPN POST Values', 'IIINNNNNNNNNNNNNNNNNN'.$paymentId);
			if ($paymentId != '0') {
				$rs = $this->User_model->get_payments_and_method($paymentId);
				if (isset($rs->UserId) && $rs->UserId != '') {

					$userId = $rs->UserId;
					$this->crypt_key($userId);
					$myCredits = $this->decrypt($rs->Credits);
					if ($myCredits == '')
						$myCredits = '0';
					$INV_AMOUNT = $this->decrypt($rs->Amount);
					$INV_CURRENCY = stripslashes($rs->Currency);
					$PAYMENT_METHOD = stripslashes($rs->PaymentMethod);
					$INV_DT = $rs->PaymentDtTm;

					if ($INV_AMOUNT == $PAYMENT_AMOUNT && $receiver_email == $rs->UserName) {
						$credits = 0;
						$userName = '';
						$userEmail = '';
						$rsCredits = $this->User_model->get_user_details($userId);
						if (isset($rsCredits->Credits) && $rsCredits->Credits != '') {
							$credits = $rsCredits->Credits;
							$userName = $rsCredits->Name;
							$userEmail = $rsCredits->UserEmail;
							$uName = $rsCredits->UserName;
							$autoFill = $rsCredits->AutoFillCredits;
						}
						if ($autoFill == '1') {
							$decCredits = $this->decrypt($credits);
							$decCredits += $myCredits;
							$encCredits = $this->encrypt($decCredits);

							$this->User_model->update_user_credits($encCredits, $userId);
							$dtTm = setDtTmWRTYourCountry();

							$data_array = array(
								'UserId' => $userId,
								'Credits' => $myCredits,
								'Description' => '+ Add Funds (Invoice #' . $paymentId . ')',
								'IMEINo' => '',
								'HistoryDtTm' => $dtTm,
								'CreditsLeft' => $encCredits,
								'PaymentId' => $paymentId
							);
							$this->User_model->insert_credit_history($data_array);

							$update_data = array(
								'ReceiverEmail' => $receiver_email,
								'PayerEmail' => $payer_email,
								'TransactionId' => $transactionId,
								'PaymentStatus' => 2,
								'CreditsTransferred' => 1,
								'UpdatedAt' => $dtTm,
								'ShippingAddress' => $shippingAddr
							);

							$this->User_model->update_gf_payments($update_data, $paymentId);


							send_push_notification($myCredits . ' Credits have been added in your account!', $userId);

							$arr = getEmailDetails();
							//========================== CHECK LAST PAYER EMAIL ================================================//
							if ($payer_email != '')
								checkLastPayerEmail($paymentId, $userId, $payer_email, $arr, $uName, $BACKEND_FOLDER);
							//========================== CHECK LAST PAYER EMAIL ================================================//
							if ($userEmail != '') {
								invoiceEmail($userEmail, $userName, $paymentId, $myCredits, $INV_AMOUNT, $INV_CURRENCY, $INV_DT, $PAYMENT_METHOD, 1);
								invoiceEmail($arr[4], 'Admin', $paymentId, $myCredits, $INV_AMOUNT, $INV_CURRENCY, $INV_DT, $PAYMENT_METHOD, 1, $BACKEND_FOLDER, $uName);
							}
						}

					}
				}
			}
		}

	}

	public function payment($paymentId)
	{
		$id = $paymentId ?: 0;
		$userId = 0;
		$credits = '';
		$pMethodId = 0;
		$pStatusId = 0;
		$transactionId = '';
		$comments = '';
		$this->data['message'] = '';
		if (($this->input->post('btnAddPmnt'))) {
			$amount = $this->input->post('txtAmount1') ?: 0;
			if (is_numeric($amount) && $amount > 0) {
				$invAmnt = $this->input->post('invAmnt') ?: 0;
				$pMethodId1 = $this->input->post('pMethodId1') ?: 0;
				$transactionId1 = $this->input->post('txtTransactionId1') ?: 0;
				$comments1 = $this->input->post('txtComments1') ?: 0;
				$currDtTm = setDtTmWRTYourCountry();

				$insert_data = array(
					'InvoiceId' => $id,
					'InvAmount' => $amount,
					'InvTransactionId' => $transactionId1,
					'InvPaymentMethodId' => $pMethodId1,
					'InvDtTm' => $currDtTm,
					'InvComments' => $comments1
				);
				$payableAmount = $invAmnt - $amount;

				$this->User_model->add_update_payment_details($insert_data, $payableAmount, $id);


				$this->data['message'] = $this->lang->line('BE_GNRL_11');
			} else {
				$this->data['message'] = $this->lang->line('BE_LBL_627');
			}

		}
		if ($id > 0) {
			$row = $this->User_model->fetch_user_row($id);
			if (isset($row->UserId) && $row->UserId != '') {
				$this->crypt_key($row->UserId);
				$myCredits = $this->decrypt($row->Credits);
				if ($myCredits == '')
					$myCredits = '-';
				$userId = $row->UserId;
				$pMethodId = $row->PaymentMethod;
				$this->data['pStatusId'] = $row->PaymentStatus;
				$transactionId = $row->TransactionId;
				$comments = stripslashes($row->Comments);
				$creditsTransferred = $row->CreditsTransferred;
				$this->data['byAdmin'] = $row->ByAdmin;
				$amountPayable = $row->PayableAmount == '' ? 0 : $row->PayableAmount;
				$currency = $row->Currency;
				$paidDtTm = $row->PaidDtTm;
				$pEmail = stripslashes($row->PayerEmail);
				$userName = $row->UserName;
			}
		}

		$this->data['view'] = 'payment';
		$this->load->view('layouts/default', $this->data);
	}

	public function order_cancelled()
	{
		$this->data['errorMsg'] = '';
		$this->data['view'] = 'order_cancelled';
		$this->load->view('layouts/default', $this->data);
	}

	public function order_placed()
	{
		$this->data['message'] = '';
		$this->data['view'] = 'order_placed';
		$this->load->view('layouts/default', $this->data);
	}


	public function ppexpresscheckoutrvw()
	{

		/*==================================================================
        PayPal Express Checkout Call
        ===================================================================
        */
		// Check to see if the Request object contains a variable named 'token'
		$token = $this->input->post_get('token') ?: '';
		$this->data['errorMsg'] = '';
		// If the Request object contains the variable 'token' then it means that the user is coming from PayPal site.
		if ($token != "") {
			$API_UserName = $this->session->userdata("API_UserName");
			$API_Password = $this->session->userdata("API_Password");
			$API_Signature = $this->session->userdata("API_Signature");


			/*
            '------------------------------------
            ' Calls the GetExpressCheckoutDetails API call
            '
            ' The GetShippingDetails function is defined in PayPalFunctions.jsp
            ' included at the top of this file.
            '-------------------------------------------------
            */


			$resArray = GetShippingDetails($token);

			$ack = strtoupper($resArray["ACK"]);

			if ($ack == "SUCCESS" || $ack == "SUCESSWITHWARNING") {
				/*
                ' The information that is returned by the GetExpressCheckoutDetails call should be integrated by the partner into his Order Review
                ' page
                */
				$this->session->set_userdata("PAYER_EMAIL", $resArray["EMAIL"]); // ' Email address of payer.
				$this->session->set_userdata("RECEIVER_EMAIL", $resArray["PAYMENTREQUEST_0_SELLERPAYPALACCOUNTID"]);
				$payerId = $resArray["PAYERID"]; // ' Unique PayPal customer account identification number.
				$payerStatus = $resArray["PAYERSTATUS"]; // ' Status of payer. Character length and limitations: 10 single-byte alphabetic characters.
				$salutation = $resArray["SALUTATION"]; // ' Payer's salutation.
				$firstName = $resArray["FIRSTNAME"]; // ' Payer's first name.
				$middleName = $resArray["MIDDLENAME"]; // ' Payer's middle name.
				$lastName = $resArray["LASTNAME"]; // ' Payer's last name.
				$suffix = $resArray["SUFFIX"]; // ' Payer's suffix.
				$cntryCode = $resArray["COUNTRYCODE"]; // ' Payer's country of residence in the form of ISO standard 3166 two-character country codes.
				$business = $resArray["BUSINESS"]; // ' Payer's business name.
				$shipToName = $resArray["SHIPTONAME"]; // ' Person's name associated with this address.

				$this->session->userdata("SHIPPING_ADDR", $resArray["SHIPTOSTREET"] . ', ' . $resArray["SHIPTOCITY"] . ', ' . $resArray["SHIPTOSTATE"] . ', ' . $resArray["SHIPTOZIP"] . ', ' . $resArray["SHIPTOCOUNTRYNAME"] . ' (' . $resArray["ADDRESSSTATUS"] . ')');
				/*

                $shipToStreet		= $resArray["SHIPTOSTREET"]; // ' First street address.
                $shipToStreet2		= $resArray["SHIPTOSTREET2"]; // ' Second street address.
                $shipToCity			= $resArray["SHIPTOCITY"]; // ' Name of city.
                $shipToState		= $resArray["SHIPTOSTATE"]; // ' State or province
                $shipToCntryCode	= $resArray["SHIPTOCOUNTRYCODE"]; // ' Country code.
                $shipToZip			= $resArray["SHIPTOZIP"]; // ' U.S. Zip code or other country-specific postal code.
                $addressStatus 		= $resArray["ADDRESSSTATUS"]; // ' Status of street address on file with PayPal
                */
				$this->session->userdata("invId", $resArray["INVNUM"]); // ' Your own invoice or tracking number, as set by you in the element of the same name in SetExpressCheckout request .
				$phonNumber = $resArray["PHONENUM"]; // ' Payer's contact telephone number. Note:  PayPal returns a contact telephone number only if your Merchant account profile settings require that the buyer enter one.
				$CURRENCY = $resArray['CURRENCYCODE'];
			} else {
				//Display a user friendly Error on the page using any of the following error information returned by PayPal
				$ErrorCode = urldecode($resArray["L_ERRORCODE0"]);
				$ErrorShortMsg = urldecode($resArray["L_SHORTMESSAGE0"]);
				$ErrorLongMsg = urldecode($resArray["L_LONGMESSAGE0"]);
				$ErrorSeverityCode = urldecode($resArray["L_SEVERITYCODE0"]);

				$this->data['errorMsg'] = "GetExpressCheckoutDetails API call failed.<br />
                            Detailed Error Message: $ErrorLongMsg.<br />
                            Short Error Message: $ErrorShortMsg.<br />
                            Error Code: $ErrorCode.<br />
                            Error Severity Code: $ErrorSeverityCode.<br />";
			}
		}
		$url = base_url('page/ppexpcheckout_confirm');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		$response = json_decode($response, true);
		if ($response['status'] == 1) {
			redirect($response['url']);
		} else if ($response['status'] == 2) {
			$this->data['message'] = $response['message'];
		} else if ($response['status'] == 3) {
			$this->data['message'] = $response['message'];
		} else if ($response['status'] == 4) {
			$this->data['message'] = $response['message'];
		}
		$this->data['view'] = 'ppexpresscheckoutrvw';
		$this->load->view('layouts/deafult', $this->data);
	}

	public function ppexpcheckout_confirm()
	{
		$response = array();
		$API_UserName = $this->session->userdata("API_UserName");
		$API_Password = $this->session->userdata("API_Password");
		$API_Signature = $this->session->userdata("API_Signature");

		$PaymentOption = "PayPal";
		$paymentId = $this->session->userdata("invId") ?: 0;
		if ($PaymentOption == "PayPal" && $paymentId > 0) {
			$finalPaymentAmount = $this->session->userdata("Payment_Amount");
			$resArray = ConfirmPayment($finalPaymentAmount);
			$ack = strtoupper($resArray["ACK"]);
			if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
				/*
                '********************************************************************************************************************
                '
                ' THE PARTNER SHOULD SAVE THE KEY TRANSACTION RELATED INFORMATION LIKE
                '                    transactionId & orderTime
                '  IN THEIR OWN  DATABASE
                ' AND THE REST OF THE INFORMATION CAN BE USED TO UNDERSTAND THE STATUS OF THE PAYMENT
                '
                '********************************************************************************************************************
                */

				$transactionId = $resArray["PAYMENTINFO_0_TRANSACTIONID"]; // ' Unique transaction ID of the payment. Note:  If the PaymentAction of the request was Authorization or Order, this value is your AuthorizationID for use with the Authorization & Capture APIs.
				$transactionType = $resArray["PAYMENTINFO_0_TRANSACTIONTYPE"]; //' The type of transaction Possible values: l  cart l  express-checkout
				$paymentType = $resArray["PAYMENTINFO_0_PAYMENTTYPE"];  //' Indicates whether the payment is instant or delayed. Possible values: l  none l  echeck l  instant
				$orderTime = $resArray["PAYMENTINFO_0_ORDERTIME"];  //' Time/date stamp of payment
				$amt = $resArray["PAYMENTINFO_0_AMT"];  //' The final amount charged, including any shipping and taxes from your Merchant Profile.
				$currencyCode = $resArray["PAYMENTINFO_0_CURRENCYCODE"];  //' A three-character currency code for one of the currencies listed in PayPay-Supported Transactional Currencies. Default: USD.
				$feeAmt = $resArray["PAYMENTINFO_0_FEEAMT"];  //' PayPal fee amount charged for the transaction
				$settleAmt = $resArray["PAYMENTINFO_0_SETTLEAMT"];  //' Amount deposited in your PayPal account after a currency conversion.
				$taxAmt = $resArray["PAYMENTINFO_0_TAXAMT"];  //' Tax charged on the transaction.
				$exchangeRate = $resArray["PAYMENTINFO_0_EXCHANGERATE"];  //' Exchange rate if a currency conversion occurred. Relevant only if your are billing in their non-primary currency. If the customer chooses to pay with a currency other than the non-primary currency, the conversion occurs in the customer's account.

				/*
                ' Status of the payment:
                        'Completed: The payment has been completed, and the funds have been added successfully to your account balance.
                        'Pending: The payment is pending. See the PendingReason element for more information.
                */

				$paymentStatus = $resArray["PAYMENTINFO_0_PAYMENTSTATUS"];

				///////////////////////////////////////////////////////////////////////////////////////////////////////////////
				$req = '';
				foreach ($resArray as $key => $value) {
					$value = urlencode(stripslashes($value));
					$req .= "&$key=$value \n";
				}
				if ($paymentStatus == 'Completed') {
					//sendMail22('hammad.akbar@gmail.com', 'Express Checkout PayPal - GSMFASTEST', 'support@gsmfastest.com', 'Payment Status', 'INNNNNNNNNNNN');
					// UPDATE STATUSES
					$rs = $this->User_model->get_credits_on_paymentstatus($paymentId);
					if (isset($rs->UserId) && $rs->UserId != '') {
						$userId = $rs->UserId;
						$this->crypt_key($userId);
						$myCredits = $this->decrypt($rs->Credits);
						if ($myCredits == '')
							$myCredits = '0';

						$INV_AMOUNT = $this->decrypt($rs->Amount);
						$INV_CURRENCY = stripslashes($rs->Currency);
						$INV_DT = $rs->PaymentDtTm;
						$credits = 0;
						$userName = '';
						$userEmail = '';
						$autoFill = 0;
						$rsCredits = $this->User_model->get_user_details($userId);
						if (isset($rsCredits->UserName) && $rsCredits->UserName != '') {
							$credits = $rsCredits->Credits;
							$userName = $rsCredits->UserName;
							$userEmail = $rsCredits->UserEmail;
							$autoFill = $rsCredits->AutoFillCredits;
						}
						$decCredits = $this->decrypt($credits);
						$decCredits += $myCredits;
						$encCredits = $this->encrypt($decCredits);

						if ($autoFill == '1')
							$this->User_model->update_user_credits($encCredits, $userId);

						$dtTm = setDtTmWRTYourCountry();

						$shippingAddr = $this->session->userdata("SHIPPING_ADDR");
						$payer_email = $this->session->userdata("PAYER_EMAIL");
						$receiver_email = $this->session->userdata("RECEIVER_EMAIL");

						$this->session->unset_userdata('invId');
						$this->session->unset_userdata('SHIPPING_ADDR');
						$this->session->unset_userdata('PAYER_EMAIL');
						$this->session->unset_userdata('RECEIVER_EMAIL');
						$arr = getEmailDetails();
						if ($autoFill == '1') {


							$insert_data = array(
								'UserId' => $userId,
								'Credits' => $myCredits,
								'Description' => '+ Add Funds (Invoice #' . $paymentId . ')',
								'IMEINo' => '',
								'CreditsLeft' => $encCredits,
								'PaymentId' => $paymentId
							);

							$this->User_model->insert_credit_history($insert_data, $paymentId);

							$update_data = array(
								'ReceiverEmail' => $receiver_email,
								'PayerEmail' => $payer_email,
								'TransactionId' => $transactionId,
								'PaymentStatus' => 2,
								'CreditsTransferred' => 1,
								'UpdatedAt' => $dtTm,
								'ShippingAddress' => $shippingAddr,
							);
							$this->User_model->update_gf_payments($update_data, $paymentId);


							send_push_notification($myCredits . ' Credits have been added in your account!', $userId);

							invoiceEmail($userEmail, $userName, $paymentId, $myCredits, $INV_AMOUNT, $INV_CURRENCY, $INV_DT, 'PayPal Express Checkout', 1);
							invoiceEmail($arr[4], 'Admin', $paymentId, $myCredits, $INV_AMOUNT, $INV_CURRENCY, $INV_DT, 'PayPal Express Checkout', 1, $userName);
							$response['message'] = "Thank you for your payment. Your credits have been added sucessfully into your account!";
						} else {
							$objDBCD14->execute("UPDATE tbl_gf_payments SET ReceiverEmail = '$receiver_email', PayerEmail = '$payer_email', TransactionId = '$transactionId', 
                            UpdatedAt = '$dtTm', ShippingAddress = '$shippingAddr', PaymentStatus = '5' WHERE PaymentId = '$paymentId'");

							invoiceEmail($arr[4], 'Admin', $paymentId, $myCredits, $INV_AMOUNT, $INV_CURRENCY, $INV_DT, 'PayPal', 5, $userName);
							$response['message'] = "Thank you for your payment. Your credits will be added soon by Admin!";
						}
						$response['status'] = 1;
						$response['url'] = ' page/invoice?ac="' . $autoFill . '"&id="' . $paymentId . '" ';
					}
				} else if ($paymentStatus == 'Pending') {
					//sendMail22('hammad.akbar@gmail.com', 'Express Checkout PayPal - GSMFASTEST', 'support@gsmfastest.com', 'Payment Status', 'ELSEEEEEEEEEEEEE');
					/*
                    'The reason the payment is pending:
                    '  none: No pending reason
                    '  address: The payment is pending because your customer did not include a confirmed shipping address and your Payment Receiving Preferences is set such that you want to manually accept or deny each of these payments. To change your preference, go to the Preferences section of your Profile.
                    '  echeck: The payment is pending because it was made by an eCheck that has not yet cleared.
                    '  intl: The payment is pending because you hold a non-U.S. account and do not have a withdrawal mechanism. You must manually accept or deny this payment from your Account Overview.
                    '  multi-currency: You do not have a balance in the currency sent, and you do not have your Payment Receiving Preferences set to automatically convert and accept this payment. You must manually accept or deny this payment.
                    '  verify: The payment is pending because you are not yet verified. You must verify your account before you can accept this payment.
                    '  other: The payment is pending for a reason other than those listed above. For more information, contact PayPal customer service.
                    */
					$pendingReason = $resArray["PAYMENTINFO_0_PENDINGREASON"];
					$reasonCode = $resArray["PAYMENTINFO_0_REASONCODE"];
					$response['status'] = 2;
					$response['message'] = "Your payment is pending due to the following reason:<br/>$pendingReason.";
				}
			} else {
				//Display a user friendly Error on the page using any of the following error information returned by PayPal
				$ErrorCode = urldecode($resArray["L_ERRORCODE0"]);
				$ErrorShortMsg = urldecode($resArray["L_SHORTMESSAGE0"]);
				$ErrorLongMsg = urldecode($resArray["L_LONGMESSAGE0"]);
				$ErrorSeverityCode = urldecode($resArray["L_SEVERITYCODE0"]);

				$response['status'] = 3;
				$response['message'] = "GetExpressCheckoutDetails API call failed.<br />
                            Detailed Error Message: $ErrorLongMsg.<br />
                            Short Error Message: $ErrorShortMsg.<br />";
			}
		} else {
			$response['status'] = 4;
			$response['message'] = "Invalid Transaction!";
		}
		echo json_encode($response);
		exit();
	}

	public function transfer_credits()
	{
		if ($this->input->method(TRUE) == 'POST') {
			$data = $this->input->post();
			$this->form_validation->set_rules('txtUName', 'Username', 'required');
			$this->form_validation->set_rules('txtEmail', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('txtCredits', 'Credits', 'required');
			if ($this->form_validation->run()) {
				if (!is_numeric($data['txtCredits'])) {
					$this->session->set_flashdata('error_message', $this->lang->line('CUST_CH_141'));
					redirect(base_url('page/transfer_credits'));
				} else if (is_numeric($data['txtCredits']) && $data['txtCredits'] > 0) {
					$rowFee = $this->User_model->get_transfer_fee(1);
					$crdtsTrnsfrFee = $rowFee->CreditsTransferFee == '' ? 0 : $rowFee->CreditsTransferFee;
					$percentage = $data['txtCredits'] * ($crdtsTrnsfrFee / 100);
					$amountToBeDeducted = roundMe($data['txtCredits'] + $percentage);
					$myCredits = 0;
					if ($userDetails->Credits != '') {
						$myCredits = $this->decrypt($userDetails->Credits);
						$myCredits = number_format($myCredits, 2, '.', '');
					}

					if ($amountToBeDeducted > $myCredits) {
						$this->session->set_flashdata('error_message', $this->lang->line('CUST_LBL_314'));
						redirect(base_url('page/transfer_credits'));
					} else {
						$uName = $data['txtUName'];
						$email = $data["txtEmail"];
						$userCredits = 0;

						$row = $this->User_model->fetch_other_userdata($uName, $email);
						if (isset($row->UserId) && $row->UserId > 0) {
							if ($row->CurrencyId == $userDetails->CurrencyId) // SAME CURRENCIES
							{
								$this->crypt_key($row->UserId);
								if ($row->Credits != '')
									$userCredits = $row->Credits;
								$convertedCredits = roundMe($credits);
								$decUserCredits = $this->decrypt($userCredits);
								$decUserCredits += $convertedCredits;
								$desc = 'Credits Added by User ' . $this->session->usedata('UserName');

								if (is_numeric($decUserCredits)) {
									$comments = $data['txtComments'];
									$currDtTm = setDtTmWRTYourCountry();

									//================================== Transferring Credits ====================================//
									$encCredits = $this->encrypt($decUserCredits);
									$this->User_model->update_user_data(array('Credits' => $encCredits), $row->UserId);
									$this->User_model->add_credit_history(array(
										'ByAdmin' => 0,
										'UserId' => $this->user_id,
										'Credits' => $convertedCredits,
										'Description' => $desc,
										'IMEINo' => '',
										'HistoryDtTm' => $currDtTm,
										'CreditsLeft' => $encCredits,
										'Comments' => $comments,
									));


									if ($row->UserEmail != '') {
										$this->updateCreditsEmail($row->UserEmail, $row->UserName, $convertedCredits, $decUserCredits, $currDtTm, 'added');
									}
									//================================== Transferring Credits ====================================//

									//================================== Deducting Credits ====================================//
									$creditsNow = $myCredits - $amountToBeDeducted;
									$desc = 'Credits transferred to User ' . $row->UserName;
									$this->crypt_key($this->session->userdata('GSM_FUS_UserId'));
									$encCreditsNow = $this->encrypt($creditsNow);

									$this->User_model->update_user_data(array('Credits' => $encCreditsNow), $this->session->userdata('GSM_FUS_UserId'));
									$this->User_model->add_credit_history(array(
										'ByAdmin' => 0,
										'UserId' => $this->user_id,
										'Credits' => '-' . $amountToBeDeducted,
										'Description' => $desc,
										'IMEINo' => '',
										'HistoryDtTm' => $currDtTm,
										'CreditsLeft' => $encCreditsNow,
										'Comments' => $comments,
										'FeePercentage' => $crdtsTrnsfrFee . '%'
									));

									//================================== Deducting Credits ====================================//
									$message = $this->lang->line('CUST_GEN_MSG');
								}
							} else {
								$message = 'Currency for ' . $row->UserName . ' is not ' . $userDetails->CurrencyAbb . '. ' . $row->UserName . ' should has ben same currency as yours. Amounts can not be transferred to different currency!';
								$this->session->set_flashdata('message', $message);
								redirect(base_url('page/transfer_credits'));
							}

						} else {
							$this->session->set_flashdata('error_message', $this->lang->line('CUST_LBL_315'));
							redirect(base_url('page/transfer_credits'));
						}
					}
				} else {
					$this->session->set_flashdata('error_message', $this->lang->line('CUST_CH_141'));
					redirect(base_url('page/transfer_credits'));
				}
			}
		}

		$this->data['view'] = 'transfer_credits';
		$this->load->view('layouts/default', $this->data);
	}

	public function product_and_services()
	{

		$sc = $this->input->post_get('sc') ?: 0;
		$categoryId = $this->input->post_get('categoryId') ?: 0;

		$this->data['products'] = $this->User_model->get_product_and_services($sc, $categoryId);
		switch ($sc) {
			case '0': // IMEI services
				$this->data['heading'] = $this->lang->line('CUST_CH_146');
				break;
			case '1': // File services
				$this->data['heading'] = $this->lang->line('CUST_CH_147');
				break;
			case '2': // Server services
				$this->data['heading'] = $this->lang->line('CUST_CH_148');
				break;
		}


		$this->data['view'] = 'client_services';
		$this->load->view('layouts/default', $this->data);
	}

	public function login_history()
	{
		$this->data['rsLogRecrds'] = $this->User_model->fetch_login_history();

		$this->data['view'] = 'login_history';
		$this->load->view('layouts/default', $this->data);

	}


	public function my_tickets()
	{

		$sId = $this->input->post_get('sId') ?: 0;

		$this->data['rsTckts'] = $this->User_model->get_tickets($sId);

		$this->data['view'] = 'tickets';
		$this->load->view('layouts/default', $this->data);
	}

	public function ticket_detail()
	{
		$id = $this->input->get('id') ?: 0;

		if ($this->input->method(TRUE) == 'POST') {
			if ($this->input->post('btnReply') && $this->input->post('cldFrm') == '1') {
				$id = $this->input->post('tcktId');
				$message = $this->input->post('txtReply') ?: '';
				if (trim($message) == '') {
					$msg = 'Reply can not be empty!';
					$this->session->set_flashdata('error_message', $msg);
					redirect(base_url('page/ticket_detail?id=' . $id));
				} else {
					$message = convertNextLineToBRTag($message);
					$dtTm = setDtTmWRTYourCountry();
					$ip = $this->input->server('REMOTE_ADDR');

					$last_insert_id = $this->User_model->add_ticket_data($id, $message, $dtTm, $ip);
					send_push_notification('Your ticket has been replied!', $this->user_id);
					ticketDetailsEmail($last_insert_id, '2');

					$this->session->set_flashdata('message', $this->lang->line('CUST_GEN_MSG'));
					redirect(base_url('page/ticket_detail?id=' . $id));
				}
			} else if ($this->input->post('btnClose') && $cldFrm == '2') {
				$this->User_model->update_ticket_status($id);
				send_push_notification('Your ticket has been closed!', $this->user_id);
				$this->session->set_flashdata('message', $this->lang->line('CUST_GEN_MSG'));
				redirect(base_url('page/ticket_detail?id=' . $id));
			}

		}


		$this->data['ticket_row'] = $this->User_model->get_ticket_info($id);
		$this->data['rsTckts'] = $this->User_model->get_comments($this->data['ticket_row']->TicketNo, $this->user_id);
		$this->data['status'] = $this->data['rsTckts'][0]->TcktStatus;
		$this->data['statusId'] = $this->data['rsTckts'][0]->StatusId;
		$this->data['tcktId'] = $id;
		$this->data['view'] = 'ticket_reply';
		$this->load->view('layouts/default', $this->data);

	}

	public function submit_ticket()
	{

		if ($this->input->method('POST') == TRUE) {
			$this->form_validation->set_rules('txtSubject', 'Subject', 'required');
			$this->form_validation->set_rules('dId', 'Department', 'required');
			$this->form_validation->set_rules('catId', 'Category', 'required');
			$this->form_validation->set_rules('priorityId', 'Priority', 'required');
			$this->form_validation->set_rules('txtMessage', 'Message', 'required');

			if ($this->form_validation->run()) {
				$dId = $this->input->post('dId') ?: 0;
				$dtTm = setDtTmWRTYourCountry();
				$tcktNo = rand_integers(8);
				$ip = $this->input->server('REMOTE_ADDR');
				$msg = convertNextLineToBRTag($this->input->post('txtMessage'));
				$catId = $this->input->post('catId');
				$priority_id = $this->input->post('priorityId');
				$subject = $this->input->post('txtSubject');


				$data = array(
					'TicketNo' => $tcktNo,
					'UserId' => $this->user_id,
					'DepartmentId' => $dId,
					'CategoryId' => $catId,
					'PriorityId' => $priority_id,
					'Name' => '',
					'Email' => '',
					'Subject' => $subject,
					'Message' => $msg,
					'DtTm' => $dtTm,
					'IP' => $ip,
					'StatusId' => 1,
					'ByAdmin' => 0
				);

				$last_ticket_id = $this->User_model->add_user_ticket($data);
				ticketDetailsEmail($last_ticket_id, 0);

				$this->data['message'] = 'Ticket has been created successfully!';
				//redirect(base_url('page/submit_ticket'));
			}

		}

		$this->data['id'] = $this->input->post_get('dId') ?: 0;
		$this->data['departments'] = $this->User_model->get_department();
		$this->data['categories'] = $this->User_model->get_category();
		$this->data['priorities'] = $this->User_model->get_priority();

		$this->data['view'] = 'submit_ticket';
		$this->load->view('layouts/default', $this->data);
	}

	public function my_statements()
	{

		$this->data['controller_instanse'] = $this;
		$this->data['rsHistory'] = $this->User_model->get_all_statements();


		$this->data['view'] = 'statements';
		$this->load->view('layouts/default', $this->data);
	}

	public function downloads()
	{

		$this->data['rsDwnlds'] = $this->User_model->get_files();

		$this->data['view'] = 'downloads';
		$this->load->view('layouts/default', $this->data);
	}

	public function client_invoices()
	{
		$this->data['label'] = $this->lang->line('CUST_LBL_217');
		$this->data['controller_instanse'] = $this;
		$this->data['invoices'] = $this->User_model->get_client_invoices();
		$this->data['type'] = 'client';
		$this->data['view'] = 'invoices';
		$this->load->view('layouts/default', $this->data);
	}

	public function admin_invoices()
	{

		$this->data['label'] = $this->lang->line('CUST_LBL_188');
		$this->data['controller_instanse'] = $this;
		$this->data['invoices'] = $this->User_model->get_admin_invoices();
		$this->data['type'] = 'admin';
		$this->data['view'] = 'invoices';
		$this->load->view('layouts/default', $this->data);
	}

	public function invoice_detail($id = 0, $type)
	{

		if ($id > 0) {
			$this->data['id'] = $id;
			$payment_details = $this->User_model->get_payment_details($id);
		
			$mykey = crypt_key($payment_details->UserId);
			$payment_details->Credits = decrypt($payment_details->Credits, $mykey);
			if($payment_details->Credits == ''){
				$payment_details->Credits = 0;
			}
			$payment_details->Amount = decrypt($payment_details->Amount, $mykey);
			if($payment_details->Amount == ''){
				$payment_details->Amount = 0 ;
			}

		

			$this->data['payment'] = $payment_details;
			$this->data['balance'] = $this->data['payment']->PayableAmount == '' ? 0 : $this->data['payment']->PayableAmount;
			$this->data['controller_instanse'] = $this;
			$this->data['amountPaid'] = 0;
			if ($this->input->method(TRUE) == 'POST') {
				$PayerEmail = $this->data['payment']->PayerEmail == '' ? '-' : $this->data['payment']->PayerEmail;
				$message = masspayment($PayerEmail, $this->data['payment']->UserId, $this->data['payment']->Credits, $this->data['payment']->Amount, $this->data['payment']->TransactionId, $this->data['payment']->PaymentStatus, $this->data['payment']->PayMethodTypeId, $this->data['payment']->PaymentMethodId, $this->data['userDetails']->CurrencyAbb, $id);
				$this->session->set_flashdata('message', $this->lang->line('CUST_GEN_MSG'));
				redirect(base_url('page/invoice_detail/' . $id . '/' . $type));
			}

			$this->data['user_info'] = $this->User_model->get_userdata();

			$rsInvLogo = $this->User_model->get_logopath();
			if (isset($rsInvLogo->LogoPath) && $rsInvLogo->LogoPath != '') {
				$INV_LOGO = "uplds" . $this->session->userdata('THEME') . "/" . $rsInvLogo->LogoPath;
			}
			$rsPP = $this->User_model->get_sum_payment($id);
			if (isset($rsPP->AmountPaid) && $rsPP->AmountPaid != '') {
				$this->data['amountPaid'] = $rsPP->AmountPaid;
			}
			$this->data['rsPayments'] = $this->User_model->get_payment_details_2($id);

		}
		$this->data['type'] = $type;
		$this->data['rsPPRcvrs'] = $this->User_model->get_reciever_data();

		$this->data['view'] = 'invoice_detail';
		$this->load->view('layouts/default', $this->data);
	}

	public function imei_services()
	{

		if ($this->input->server('HTTP_HOST') != 'localhost') {
			if (!checkCountryRestriction($this->session->userdata('GSM_FUS_UserId'), $this->session->userdata('UserEmail'), $this->session->userdata('UserName'), 'IMEI')) {
				redirect(base_url('page/error'));
			}
		}
		$MY_CURRENCY_ID = $this->data["userDetails"]->CurrencyId;
		$imei_data = array();
		$IMEI_TYPE = '0';
		$this->data['mustRead'] = '';
		$imeiOption = '1';
		$categoryId = $this->input->post('categoryId') ?: 0;
		$myNetworkId = $this->input->post('packageId') ?: 0;
		$this->data['myNetworkId'] = $this->input->post('packageId') ?: 0;
		$csrfTkn = $this->input->post('csrfToken') ?: '';
		$this->data['altEmail'] = $this->input->post("txtAltEmail") ?: '';
		if ($this->input->post("rdIMEIType")) {
			$imeiOption = $this->input->post("rdIMEIType");
		}
		$this->data['imei'] = $this->input->post("txtIMEI") ?: '';
		$this->data['multiIMEIs'] = $this->input->post("imei") ?: '';
		$this->data['notes'] = $this->input->post("txtNotes") ?: '';
		$this->data['comments'] = $this->input->post("txtComments") ?: '';
		$errorMsg = '';
		$this->data['lastDgt'] = $this->input->post("txtIMEILastDigit") ?: '';
		$msg = $this->input->post("msg") ?: '';

		$this->data['imeiMxLn'] = '15';
		$imeiCls = 'input70pc';
		$this->data['message'] = '';

		if ($this->data['settings']->CheckSumIMEI == '1') {
			$this->data['imeiMxLn'] = '14';
			$imeiCls = 'input60pc';
		}
		if ($imeiOption == '1')
			$this->data['multiIMEIs'] = '';
		else if ($imeiOption == '2')
			$imei = '';
		$this->data['cldFrm'] = $this->input->post("cldFrm") ?: 0;
		$this->data['cldFm'] = $this->input->post("cldFm") ?: 0;
		$this->data['isError'] = 0;
		$this->data['ORDER_COMPLETED'] = '1';

		if ($this->input->post('packageId') && $this->session->userdata('GSM_FUS_UserId') && $this->session->userdata('ORDERED_FROM') == '1') {
			if ($this->data['cldFrm'] == '2') {
				$cols_cfields = $vals_cfields = $historyData = '';
				$ttlFields = $this->input->post("totalCustomFields") ?: '0';
				for ($x = 1; $x <= $ttlFields; $x++) {
					$col = $this->input->post('colNm' . $x) ?: '';
					if ($col != '') {
						$cols_cfields .= ", $col";
						$val = $this->input->post('fld' . $x) ?: '';
						$vals_cfields .= ", '$val'";
						$isMandatory = $this->input->post('mndtry' . $x) ?: '0';
						$chkRestriction = $this->input->post('rstrctn' . $x) ?: '0';
						if ($isMandatory == '1' && $val == '') {
							$errorMsg .= "Value for " . $this->input->post('lbl' . $x) . " can not be empty.<br />";
						}
						if ($chkRestriction == '1') // DIGITS ONLY
						{
							$errorMsg .= digits_validation($val, $this->input->post('lbl' . $x));
						} else if ($chkRestriction == '2') // CHARACTERS ONLY
						{
							$errorMsg .= characters_validation($val, $this->input->post('lbl' . $x));
						} else if ($chkRestriction == '3') // DIGITS & CHARACTERS ONLY
						{
							$errorMsg .= validateAlphabetsAndDigits($val, $this->input->post('lbl' . $x));
						}
						if ($historyData != '') {
							$historyData .= '<br />';
						}
						$historyData .= $this->input->post('lbl' . $x) . ": " . $val;
					}
				}
				if ($this->input->post('hdTOC') == '1')// I terms and conditions check is mandatory
				{
					if (!$this->input->post('chkTOC'))
						$errorMsg .= "Click on Agree with Terms Of Services.<br /><br />";
				}
				if (trim($errorMsg) == '') {
					if ($this->data['userDetails']->PinCode != '') {
						$pinCode = $this->input->post('txtPinCode') ?: 0;
						$rowPC = $this->User_model->get_pincode();
						if (isset($rowPC->PinCode) && $this->theString_Decrypt($rowPC->PinCode) == $pinCode && $pinCode != '') {
							$CALLED_FROM_ORDER_PAGE = '1';

							$data = array();

							if (isset($_POST['imeiFType']) && $this->session->userdata('GSM_FUS_UserId') && $this->
								session->userdata('ORDERED_FROM') == '1') {
								if ($this->input->server('HTTP_HOST') != 'localhost') {
									if (!checkCountryRestriction($this->session->userdata('GSM_FUS_UserId'), $this->
									session->userdata('UserEmail'), $this->session->userdata('UserName'), 'IMEI')) {
										redirect(base_url('page/error'));
									}
								}
								$data['apiId'] = 0;
								$apiType = 0;
								$apiKey = '';
								$serverURL = '';
								$accountId = '';
								$apiAction = '';
								$responseUrl = '';
								$toolForUB = '';
								$packageTitle = '';
								$packCostPrice = 0;
								$msgFromServer = '';
								$ifCodeSentToServer = 0;
								$orderIdFromServer = '';
								$toolId = '';
								$DELAY_TIME = '1';
								$CRON_DELAY_TIME = '1';
								$NEW_ORDER_EMAILS = '';
								$IMEI_F_TYPE = $this->input->post('imeiFType') ?: '0';
								$PRE_ORDER = $this->input->post('hdPreOrder') ?: '0';
								$mepId = '';
								$mepValue = '';
								$modelId = '';
								$modelValue = '';
								$serialNo = '';
								$modelValueToShow = '';
								$apiName = '';
								$IMEI_SERIES = '';
								$mobileId = 0;

								$rsPackTitle = $this->User_model->get_package_data($myNetworkId);
								if (isset($rsPackTitle->PackageTitle) && $rsPackTitle->PackageTitle != '') {
									$packageTitle = stripslashes($rsPackTitle->PackageTitle);
									$packCostPrice = $rsPackTitle->CostPrice == '' ? 0 : $rsPackTitle->CostPrice;
									$SERVICE_TYPE = $rsPackTitle->ServiceType; // 0 for DATABASE, 1 for RECALCULATE
									$DELAY_TIME = $rsPackTitle->ResponseDelayTm == 0 ? '1' : $rsPackTitle->
									ResponseDelayTm;
									$CRON_DELAY_TIME = $rsPackTitle->CronDelayTm == 0 ? '1' : $rsPackTitle->
									CronDelayTm;
									$NEW_ORDER_EMAILS = $rsPackTitle->NewOrderEmailIDs;
									$IMEI_SERIES = trim($rsPackTitle->IMEISeries);
								}
								$rsAPI = $this->User_model->get_api_data($myNetworkId);
								if (isset($rsAPI->APIId) && $rsAPI->APIId != '') {
									$data['apiId'] = $rsAPI->APIId;
									$apiType = $rsAPI->APIType;
									$apiKey = $rsAPI->APIKey;
									$serverURL = $rsAPI->ServerURL;
									$accountId = $rsAPI->AccountId;
									$serviceId = $rsAPI->ServiceId;
									$apiAction = $rsAPI->APIAction;
									$responseUrl = $rsAPI->ResponseURL;
									$apiName = $rsAPI->APITitle;
									$extNwkId = $rsAPI->ExternalNetworkId != '' ? $rsAPI->ExternalNetworkId : 0;
								}

								if (isset($_POST['brandId']) && $_POST['brandId'] != '0') {
									$mobileId = $this->input->post('brandId') ?: '0';
								}
								if ($this->input->post('modelId') && $this->input->post('modelId') != '0') {
									$modelId = $this->input->post('modelId') ?: '0';
									$modelValueToShow = $this->input->post('mdlVal') ?: '0';
								}
								$arrIMEIS = array();
								$duplicateIMEIs = array();
								$invalidIMEIs = array();
								$DUP_IMEIS = array();
								$ODLIMEIs = array();
								$ODL_COUNTER = 0;
								$totalIMEIs = 0;
								if ($IMEI_F_TYPE == '0') {
									if ($this->input->post("rdIMEIType") == '1') {
										if ($this->input->post('chkSm') && $this->input->post('chkSm') == '1')
											$arrIMEIS[0] = $this->input->post("txtIMEI") . $this->input->post("txtIMEILastDigit");
										else
											$arrIMEIS[0] = $this->input->post("txtIMEI");
									} else {
										$arrIMEIS = explode("\n", $this->input->post("imei"));
										$r = 0;
										foreach ($arrIMEIS as $v) {
											$arrIMEIS[$r] = trim($v);
											$r++;
										}
										$duplicateIMEIs = array_repeat($arrIMEIS);
										$arrIMEIS = array_values(array_filter(array_unique($arrIMEIS))); // remvoving duplication then empty values and then re-numbering the array index
									}
									$totalIMEIs = sizeof($arrIMEIS);
								} else
									if ($IMEI_F_TYPE == '1') {
										if ($this->input->post('chkSm') && $this->input->post('chkSm') == '1') {
											$arrIMEIS[0] = $this->input->post("txtIMEI") . $this->input->post("txtIMEILastDigit");
										} else {
											$arrIMEIS[0] = $this->input->post("txtIMEI");
										}
										$totalIMEIs = sizeof($arrIMEIS);
									} else
										if ($IMEI_F_TYPE == '2') {
											$arrIMEIS = explode("\n", $this->input->post("imei"));
											$r = 0;
											foreach ($arrIMEIS as $v) {
												$arrIMEIS[$r] = trim($v);
												$r++;
											}
											$duplicateIMEIs = array_repeat($arrIMEIS);
											$arrIMEIS = array_values(array_filter(array_unique($arrIMEIS))); // remvoving duplication then empty values and then re-numbering the array index
											$totalIMEIs = sizeof($arrIMEIS);
										} else
											if ($IMEI_F_TYPE == '3') {
												$mnLen = $this->input->post('customFldMinLen') ?: 0;
												$mxLen = $this->input->post('customFldMaxLen') ?: 0;
												$custFldRes = $this->input->post('custFldRstrctn') ?: 0;
												$arrCustomData = explode("\n", $this->input->post("txtCustomFld"));
												$r = 0;
												$rr = 0;
												$arrTEMP = array();
												foreach ($arrCustomData as $v) {
													$IS_VALID = true;
													$arrValue = trim($v);
													$strCustError = '';
													if ($mnLen != 0 && $mnLen != '') {
														if (strlen($arrValue) < $mnLen) {
															$invalidIMEIs[$rr] = $arrValue;
															$rr++;
															$IS_VALID = false;
														}
													}
													if ($mxLen != 0) {
														if (strlen($arrValue) > $mxLen) {
															$invalidIMEIs[$rr] = $arrValue;
															$rr++;
															$IS_VALID = false;
														}
													}
													// CHECK IF THERE IS ANY RESTRICTION AT CUSTOM FIELD.
													$strCustError = checkCustomFieldRestriction($custFldRes, $objForm, $arrValue);
													if ($strCustError != '') {
														$invalidIMEIs[$rr] = $arrValue;
														$rr++;
														$IS_VALID = false;
													}
													if ($IS_VALID)
														$arrIMEIS[$r] = $arrValue;
													$r++;
												}
												$duplicateIMEIs = array_repeat($arrIMEIS);
												$arrIMEIS = array_values(array_filter(array_unique($arrIMEIS))); // remvoving duplication then empty values and then re-numbering the array index

												$totalIMEIs = sizeof($arrIMEIS);
											}

								$personalRecord = $this->input->post('txtPersonalRecord');
								$phoneLockedOn = $this->input->post('txtOprName') ?: '';
								$countryId = $this->input->post('countryId') ?: 0;
								$notes = $this->input->post('txtNotes');
								$comments = $this->input->post('txtComments');
								$allowDupIMEIs = $this->input->post('hdDupIMEI') ?: 0;
								$packagePrice = 0;
								$USER_ID = $_SESSION['GSM_FUS_UserId'];

								$packagePrice = getpackprice($myNetworkId, $MY_CURRENCY_ID, $IMEI_TYPE);
								$dupMsg = '';
								$odlMsg = '';
								$strInsertOrders = '';
								$strCreditHistory = '';
								$strEmailContents = '';
								$strInsert = '0';
								$suppPurchasePrice = '0';
								$supplierId = '0';

								list($suppPackId, $suppPurchasePrice, $supplierId) = ifPackIdAssignedToSupplier($myNetworkId,
									0);
								if ($suppPackId != '0')
									$strInsert = '1';

								$STR_IMEIS = '0';
								$currDtTm = setDtTmWRTYourCountry();

								for ($j = 0; $j < $totalIMEIs; $j++) {
									if (trim($arrIMEIS[$j]) != '')
										$STR_IMEIS .= ", '" . trim($arrIMEIS[$j]) . "'";
								}
								if ($allowDupIMEIs == '0') {
									$rsIfExists = $this->User_model->get_codes($myNetworkId, $STR_IMEIS);

									foreach ($rsIfExists as $row) {
										$DUP_IMEIS[$row->IMEINo] = '1';
									}
								}
								$rwUNC = $this->User_model->fetch_negative_user_credits();
								$ALLOWNGTVCRDTS = $rwUNC->AllowNegativeCredits;

								$ODL = $rwUNC->OverdraftLimit;

								$PLACED_ORDERS_COUNT = 0;

								for ($j = 0; $j < $totalIMEIs; $j++) {
									$UNLOCK_CODE = '';
									$strUpdate = '';
									$myIMEI = trim($arrIMEIS[$j]);
									$allowIMEIEntry = true;

									if (isset($DUP_IMEIS[$myIMEI]) && $DUP_IMEIS[$myIMEI] == '1') {
										$allowIMEIEntry = false;
										if ($dupMsg == '')
											$dupMsg = $myIMEI;
										else
											$dupMsg .= 'LB ' . $myIMEI;
									}

									if ($allowIMEIEntry && trim($myIMEI) != '') {
										//$PLACED_ORDERS_COUNT++;
										if (isset($_POST['imeiFType']) && $this->session->userdata('GSM_FUS_UserId') && $CALLED_FROM_ORDER_PAGE == '1' && $this->session->userdata('ORDERED_FROM') == '1') {
											$keys = crypt_key($this->session->userdata('GSM_FUS_UserId'));

											$myCredits = decrypt($this->data['userDetails']->Credits, $keys);
											//$myCredits = number_format($myCredits, 2, '.', '');
											if ($myCredits == '')
												$myCredits = 0;

											$finalCr = $myCredits - $packagePrice;
											if ($finalCr < 0) {
												if ($ALLOWNGTVCRDTS == '0') {
													$message = $this->lang->line('CUST_CODE_MSG2');
												} else if ($ALLOWNGTVCRDTS == '1') {
													$REMAINING_CREDITS = abs($finalCr);
													if ($REMAINING_CREDITS <= $ODL) {
														// check limit
														if (isset($_POST['imeiFType']) && $this->session->userdata('GSM_FUS_UserId') && $CALLED_FROM_ORDER_PAGE == '1' && $this->session->userdata('ORDERED_FROM') == '1') {
															$PLACED_ORDERS_COUNT++;
															if ($strInsertOrders != '') {
																$strInsertOrders .= ',';
															}
															if ($strCreditHistory != '') {
																$strCreditHistory .= ',';
															}
															$keys = crypt_key($this->session->userdata('GSM_FUS_UserId'));
															$encryptedCredits = encrypt($finalCr, $keys);
															$profit = 0;
															$packPriceToDefaultCurr = 0;
															$orderType = $SERVICE_TYPE;
															//========================================== Calculate Profit ===================================================//
															if ($packCostPrice != '0') {
																$packPriceToDefaultCurr = roundMe($packagePrice / $CONVERSION_RATE); //Convert local Currency into default currency as per currency rate
																$profit = $packPriceToDefaultCurr - $packCostPrice;
															}
															//========================================== Calculate Profit ===================================================//

															//========================================== CHECK IMEI SERIES ===================================================//
															if ($IMEI_SERIES != '') {
																if (ifIMEIRestricted($myIMEI, $IMEI_SERIES)) {
																	$orderType = '2';
																}
															}
															//========================================== CHECK IMEI SERIES ===================================================//
															$strHistData = $myIMEI . $historyData;

															$strInsertOrders .= "('$personalRecord', '1', '" . $this->session->userdata('GSM_FUS_UserId') . "', '$packagePrice', '$phoneLockedOn', '$myIMEI', '$myNetworkId', '$notes', '$comments', 
															'$countryId', '$currDtTm', '$altEmail', '" . $this->session->userdata['CLIENT_GF_IP'] . "', '" . $data['apiId'] . "', '$apiName', '$serverURL', '$accountId', '$apiType', '$apiKey', '$extNwkId', '0', '0', 
															'$strInsert', '$suppPurchasePrice', '$PRE_ORDER', '$supplierId', '$orderType', '$DELAY_TIME', '$packCostPrice', '$profit', '$packPriceToDefaultCurr', 
															'$CRON_DELAY_TIME', '$mobileId', '$modelId', '$modelValueToShow', '$strHistData', '" . $this->session->userdata('ORDERED_FROM') . "' $vals_cfields)";

															$strCreditHistory .= "('" . $this->session->userdata('GSM_FUS_UserId') . "', '$packagePrice', 'New IMEI Order for $packageTitle', '$strHistData', 
															'$myNetworkId', '$currDtTm', '$encryptedCredits', '$myCurrency', '" . $this->session->userdata['CLIENT_GF_IP'] . "')";

															if ($this->data['settings']->SendNewIMEIOrderEmail == '1') {
																$strEmailContents .= $myIMEI . '<br />';
															}
															$myCredits = $finalCr;
														} else {
															redirect(base_url('dashboard'));
														}
													} else {
														$ODLIMEIs[$ODL_COUNTER++] = $myIMEI;
														if ($odlMsg == '')
															$odlMsg = $myIMEI;
														else
															$odlMsg .= 'LB ' . $myIMEI;
														//$message = $CUST_CODE_MSG2;			
													}
												}
											} else {
												if (isset($_POST['imeiFType']) && $this->session->userdata('GSM_FUS_UserId') && $CALLED_FROM_ORDER_PAGE == '1' && $this->session->userdata('ORDERED_FROM') == '1') {
													$PLACED_ORDERS_COUNT++;
													if ($strInsertOrders != '') {
														$strInsertOrders .= ',';
													}
													if ($strCreditHistory != '') {
														$strCreditHistory .= ',';
													}
													$keys = crypt_key($this->session->userdata('GSM_FUS_UserId'));
													$encryptedCredits = encrypt($finalCr, $keys);
													$profit = 0;
													$packPriceToDefaultCurr = 0;
													$orderType = $SERVICE_TYPE;
													//========================================== Calculate Profit ===================================================//
													if ($packCostPrice != '0') {
														$packPriceToDefaultCurr = roundMe($packagePrice / $CONVERSION_RATE); //Convert local Currency into default currency as per currency rate
														$profit = $packPriceToDefaultCurr - $packCostPrice;
													}
													//========================================== Calculate Profit ===================================================//

													//========================================== CHECK IMEI SERIES ===================================================//
													if ($IMEI_SERIES != '') {
														if (ifIMEIRestricted($myIMEI, $IMEI_SERIES)) {
															$orderType = '2';
														}
													}
													//========================================== CHECK IMEI SERIES ===================================================//
													$strHistData = $myIMEI . $historyData;

													$strInsertOrders .= "('$personalRecord', '1', '" . $this->session->userdata('GSM_FUS_UserId') . "', '$packagePrice', '$phoneLockedOn', '$myIMEI', '$myNetworkId', '$notes', '$comments', 
	'$countryId', '$currDtTm', '$altEmail', '" . $this->session->userdata['CLIENT_GF_IP'] . "', '" . $data['apiId'] . "', '$apiName', '$serverURL', '$accountId', '$apiType', '$apiKey', '$extNwkId', '0', '0', 
	'$strInsert', '$suppPurchasePrice', '$PRE_ORDER', '$supplierId', '$orderType', '$DELAY_TIME', '$packCostPrice', '$profit', '$packPriceToDefaultCurr', 
	'$CRON_DELAY_TIME', '$mobileId', '$modelId', '$modelValueToShow', '$strHistData', '" . $this->session->userdata('ORDERED_FROM') . "' $vals_cfields)";

													$strCreditHistory .= "('" . $this->session->userdata('GSM_FUS_UserId') . "', '$packagePrice', 'New IMEI Order for $packageTitle', '$strHistData', 
	'$myNetworkId', '$currDtTm', '$encryptedCredits', '$myCurrency', '" . $this->session->userdata['CLIENT_GF_IP'] . "')";

													if ($this->data['settings']->SendNewIMEIOrderEmail == '1') {
														$strEmailContents .= $myIMEI . '<br />';
													}
													$myCredits = $finalCr;
												} else {
													redirect(base_url('dashboard'));
												}
											}
										}
									}
								}
								if ($strInsertOrders != '' && $strCreditHistory != '') {
									$this->User_model->add_codes_data($strInsertOrders, $cols_cfields);
									$this->User_model->update_user_credits($encryptedCredits);
									$this->User_model->add_credit_history_iemi($strCreditHistory);
								}
								if ($strEmailContents != '' && $this->data['settings']->SendNewIMEIOrderEmail ==
									'1') {
									newIMEIOrderEmail($this->session->userdata('UserEmail'), $this->session->userdata('UserName'),
										$packageTitle, $strEmailContents, '', $packagePrice, $finalCr, $currDtTm, $myNetworkId,
										$altEmail, $NEW_ORDER_EMAILS, $notes);
								}

								if ($PLACED_ORDERS_COUNT > 0)
									$message = "ThankSPyouSPforSPplacingSP($PLACED_ORDERS_COUNT)SPIMEISPorders.";
								if ($dupMsg != '') {
									$ORDER_COMPLETED = 0;
									$message .= "LBLBRepeatedSPIMEI(s)SPORSPInvalidSPData:SPLB$dupMsg";
								}
								if ($dupMsg == '' && (sizeof($duplicateIMEIs) > 0 || sizeof($invalidIMEIs) > 0)) {
									$ORDER_COMPLETED = 0;
									$message .= "LBLBRepeatedSPIMEI(s)SPORSPInvalidSPData: ";
								}
								if ($odlMsg != '' && sizeof($ODLIMEIs) > 0) {
									$ORDER_COMPLETED = 0;
									$message .= "LBLBYourSPOverdraftSPLimitSPhasSPbeenSPfinishedSPforSPIMEI(s): ";
								}

								foreach ($duplicateIMEIs as $dupVal) {
									if ($dupVal != '') {
										$ORDER_COMPLETED = 0;
										$message .= "LB$dupVal";
									}
								}
								foreach ($invalidIMEIs as $imeiVal) {
									if ($imeiVal != '') {
										$ORDER_COMPLETED = 0;
										$message .= "LB$imeiVal";
									}
								}
								foreach ($ODLIMEIs as $oldIMEI) {
									if ($oldIMEI != '') {
										$ORDER_COMPLETED = 0;
										$message .= "LB$oldIMEI";
									}
								}

								$data['message'] = $message;


							} else {
								echo "here";
								//redirect(base_url('dashboard'));
							}


							$imei_data = $data;
							$this->data['message'] = $imei_data['message'];
							$imeiOption = '1';
							$imei = '';
							$this->data['multiIMEIs'] = '';
							$countryId = 0;
							$this->data['notes'] = '';
							$this->data['comments'] = '';
							$this->data['myNetworkId'] = 0;
							$this->data['altEmail'] = '';
							$lastDgt = '';
						} else {
							$this->data['message'] = $this->lang->line('CUST_LBL_179');
							$msg = $this->lang->line('CUST_LBL_179');
							$this->data['isError'] = 1;
						}
					} else {
						$CALLED_FROM_ORDER_PAGE = '1';
						$imei_data = submit_imei_order($myNetworkId, $IMEI_TYPE, $this->data['userDetails']->CurrencyId, $CALLED_FROM_ORDER_PAGE, $historyData, $this->data['altEmail'], $vals_cfields, $this->data['userDetails']->CurrencyAbb, $cols_cfields);
						$this->data['message'] = $imei_data['message'];
						$imeiOption = '1';
						$imei = '';
						$this->data['multiIMEIs'] = '';
						$countryId = 0;
						$this->data['notes'] = '';
						$this->data['comments'] = '';
						$this->data['myNetworkId'] = 0;
						$this->data['altEmail'] = '';
						$lastDgt = '';
					}
				} else
					$this->data['ORDER_COMPLETED'] = 0;
			}

		}
		//get package prices section

		$PACK_PRICES_PLAN = array();
		$PACK_PRICES_USER = array();
		$PACK_PRICES_BASE = array();
		$USER_ID = $this->session->userdata('GSM_FUS_UserId');
		//call get services ids


		$sType = 0;
		$rsIds = $this->User_model->selectData("SELECT PackageId FROM tbl_gf_packages WHERE sl3lbf = '$sType' AND DisablePackage = 0");
		$strServiceIds = '0';

		foreach ($rsIds as $row) {
			$strServiceIds .= ', ' . $row->PackageId;
		}

//============================================================ CHECK IF PRICE SET AGAINST USER ==========================================================//
		$rsPackPrices = $this->User_model->selectData("SELECT PackageId, Price FROM tbl_gf_users_packages_prices WHERE UserId = '$USER_ID' AND PackageId IN ($strServiceIds)");
		foreach ($rsPackPrices as $row) {
			$PACK_PRICES_USER[$row->PackageId] = roundMe($row->Price);
		}
//============================================================ CHECK IF PRICE SET AGAINST USER ==========================================================//

//=============================================================== PICK DEFAULT GROUP PRICE ==============================================================//
		$USER_CURRENCY_RATE = 1;

		$rwUsrRate = $this->User_model->getRow("SELECT ConversionRate FROM tbl_gf_users A, tbl_gf_currency B WHERE (A.CurrencyId = B.CurrencyId) AND UserId = '$USER_ID'");
		if (isset($rwUsrRate->ConversionRate) && $rwUsrRate->ConversionRate != '')
			$USER_CURRENCY_RATE = $rwUsrRate->ConversionRate;

		$rsPlanPr_DEFAULT = $this->User_model->selectData("SELECT PackageId, Price FROM tbl_gf_users A, tbl_gf_plans_packages_prices B, tbl_gf_currency C WHERE A.PricePlanId = B.PlanId 
						AND B.CurrencyId = C.CurrencyId AND DefaultCurrency = 1 AND UserId = '$USER_ID' AND ServiceType = '$IMEI_TYPE' AND PackageId IN ($strServiceIds)");
		foreach ($rsPlanPr_DEFAULT as $row) {
			$PACK_PRICES_PLAN[$row->PackageId] = roundMe($row->Price * $USER_CURRENCY_RATE);
		}
//=============================================================== PICK DEFAULT GROUP PRICE ==============================================================//

//========================================================= CHECK IF USER PRICE SET IN PRICE PLAN =======================================================//
		$rsPlanPr = $this->User_model->selectData("SELECT PackageId, Price FROM tbl_gf_users A, tbl_gf_plans_packages_prices B WHERE A.PricePlanId = B.PlanId AND B.CurrencyId = 
						'$MY_CURRENCY_ID' AND ServiceType = '$IMEI_TYPE' AND UserId = '$USER_ID' AND PackageId IN ($strServiceIds) ");
		foreach ($rsPlanPr as $row) {
			$PACK_PRICES_PLAN[$row->PackageId] = roundMe($row->Price);
		}
//========================================================= CHECK IF USER PRICE SET IN PRICE PLAN =======================================================//

//================================================================ GET DEFAULT PACK PRICES ==============================================================//
		$rsPackPrices = $this->User_model->selectData("SELECT A.PackageId, Price, PackagePrice FROM tbl_gf_packages A LEFT JOIN tbl_gf_packages_currencies B 
							ON (A.PackageId = B.PackageId AND CurrencyId = '$MY_CURRENCY_ID') WHERE sl3lbf = '$IMEI_TYPE' AND A.PackageId IN ($strServiceIds)");
		foreach ($rsPackPrices as $row) {
			if ($row->Price == '')
				$PACK_PRICES_BASE[$row->PackageId] = roundMe($row->PackagePrice * $this->data['userDetails']->ConversionRate);
			else
				$PACK_PRICES_BASE[$row->PackageId] = roundMe($row->Price);
		}

		//end package prices
		$this->data['PACK_PRICES_BASE'] = $PACK_PRICES_BASE;
		$this->data['PACK_PRICES_PLAN'] = $PACK_PRICES_PLAN;
		$this->data['PACK_PRICES_USER'] = $PACK_PRICES_USER;

		$this->data['apiId'] = isset($imei_data['apiId']) ? $imei_data['apiId'] : 0;
		$this->data['rsPackages'] = $this->User_model->get_imei_services();

		$this->data['view'] = 'imei_services';
		$this->load->view('layouts/default', $this->data);
	}

	public function ajximeiorder()
	{
		$purpose = $this->input->post_get('purpose');

		if ($purpose == 'getData') {
			$PCK_TITLE = 'PackageTitle';
			$TIME_TAKEN = 'TimeTaken';
			$IMP_INFO = 'MustRead';
			$packId = $this->input->post('packId') ?: '0';
			$usrCurrId = $this->input->post('usrCurrId') ?: '0';
			$cnvrsnRt = $this->input->post('cnvrsnRt') ?: '0';
			$themeStyle = $this->input->post('themeStyle') ?: '0';

			$msg = '';
			$mustRead = '';
			$timeTaken = '';
			$apiId = 0;
			$pckTitle = '';
			$dupIMEIs = 0;
			$extNtwrkId = 0;
			$strMobiles = '';
			$strBrands = '';
			$apiType = 0;
			$customFldId = 0;
			$redirectTo = '';

			$rsPackage = $this->User_model->get_packages($PCK_TITLE, $TIME_TAKEN, $IMP_INFO, $packId);

			if (isset($rsPackage->PackageTitle) && $rsPackage->PackageTitle != '') {
				$mustRead = htmlspecialchars_decode(stripslashes($rsPackage->MustRead));
				$timeTaken = stripslashes($rsPackage->TimeTaken);
				$apiId = $rsPackage->APIId;
				$pckTitle = stripslashes($rsPackage->PackageTitle);
				$dupIMEIs = $rsPackage->DuplicateIMEIsNotAllowed;
				$imeiFType = $rsPackage->IMEIFieldType;
				$pckImg = $rsPackage->PackageImage;
				$preCode = $rsPackage->CalculatePreCodes;
				$apiType = $rsPackage->APIType;
				$extNtwrkId = $rsPackage->ExternalNetworkId == '' ? 0 : $rsPackage->ExternalNetworkId;
				$customFldId = $rsPackage->CustomFieldId;
				$redirectTo = $rsPackage->RedirectionURL;
				$toc = $rsPackage->TOCs;
				$strFeatures = $rsPackage->ServiceType == '0' ? '<i class="fa fa-reorder" title="Database"></i>&nbsp;&nbsp;&nbsp;' : '';
				$strFeatures .= $rsPackage->VerifyOrders == '1' ? '<i class="fa fa-refresh" title="Verifiable"></i>&nbsp;&nbsp;&nbsp;' : '';
				$strFeatures .= $rsPackage->CancelOrders == '1' ? '<i class="fa fa-times" title="Can be cancelled"></i>' : '';
			}
			$myNetworkId = $packId;
			$USER_ID = $this->session->userdata('GSM_FUS_UserId');
			$IMEI_TYPE = 0;
			$MY_CURRENCY_ID = $usrCurrId;
			$CONVERSION_RATE = $cnvrsnRt;

			$packagePrice = getpackprice($myNetworkId, $MY_CURRENCY_ID, $IMEI_TYPE);

			if ($pckImg != '') {
				$rowSettings = $this->User_model->get_theme();
				$pckImg = 'uplds' . $rowSettings->Theme . '/' . $rsPackage->PackageImage;
			}

			$brandAPIId = 0;
			$rsBrands = $this->User_model->get_brands($packId);
			foreach ($rsBrands as $row) {
				$brandAPIId = $row->APIId;
				$strBrands .= "<option value='" . $row->Id . "'>" . stripslashes($row->Value) . "</option>";
			}

			//=================================== CUSTOM FIELDS ============================================//
			$rsFields = $this->User_model->get_custom_fields($packId);

			$totalFields = count($rsFields);
			$i = 1;
			$strFields = '';

			foreach ($rsFields as $row) {
				if ($themeStyle == '0')
					$strFields .= '<div class="col-sm-12"><label>' . $row->FieldLabel . ':';
				else if ($themeStyle == '1')
					$strFields .= '<div class="control-group"><label class="control-label">' . $row->FieldLabel . ':';
				else if ($themeStyle == '2')
					$strFields .= '<div class="control-group"><label class="col-md-3 control-label">' . $row->FieldLabel . ':';
				if ($row->Mandatory == '1') $strFields .= '*';
				if ($themeStyle == '0')
					$strFields .= '</label>';
				else if ($themeStyle == '1')
					$strFields .= '</label><div class="controls">';
				else if ($themeStyle == '2')
					$strFields .= '</label><div class="col-md-9">';
				if ($row->FieldType == 'Text Box') {
					$strMaxLen = '';
					if ($row->MaxLength > 0)
						$strMaxLen = "maxlength = '" . $row->MaxLength . "'";
					if ($row->MinLength > 0 && is_numeric($row->MinLength))
						$strMaxLen .= "minlength = '" . $row->MinLength . "'";
					$strFields .= '<input type="text" placeholder="Enter ' . $row->FieldLabel . '" name="fld' . $i . '" ' . $strMaxLen . ' class="form-control" />';
				} else if ($row->FieldType == 'Text Area')
					$strFields .= '<textarea name="fld' . $i . '" class="form-control"></textarea>';
				else if ($row->FieldType == 'Drop Down') {
					$strFields .= '<select name="fld' . $i . '" class="chosen">';
					$strFields .= '<option value="0">Please Select</option>';
					$rsValues = $objDBCD14->query("SELECT RegValue FROM tbl_gf_custom_field_values WHERE DisableRegValue = 0 AND FieldId = '" . $row->FieldId . "' ORDER BY RegValue");
					while ($rw = $objDBCD14->fetchNextObject($rsValues)) {
						$strFields .= '<option value="' . $rw->RegValue . '">' . $rw->RegValue . '</option>';
					}
					$strFields .= '</select>';
				} else if ($row->FieldType == 'Radio Button') {
					$rsValues = $objDBCD14->query("SELECT RegValue FROM tbl_gf_custom_field_values WHERE DisableRegValue = 0 AND FieldId = '" . $row->FieldId . "' ORDER BY RegValue");
					while ($rw = $objDBCD14->fetchNextObject($rsValues)) {
						$strFields .= '<input type="radio" class="form-control" name="fld' . $i . '" checked="checked" value="' . $rw->RegValue . '">' . $rw->RegValue . '&nbsp;&nbsp;&nbsp;';
					}
				}
				if ($row->FInstructions != '')
					$strFields .= '<small>' . stripslashes($row->FInstructions) . '</small>';
				if ($themeStyle == '0')
					$strFields .= '</div>';
				else if ($themeStyle == '1')
					$strFields .= '</div></div>';
				else if ($themeStyle == '2')
					$strFields .= '</div></div><br /><br />';
				$strFields .= '<input type="hidden" name="colNm' . $i . '" value="' . $row->FieldColName . '" />';
				$strFields .= '<input type="hidden" name="mndtry' . $i . '" value="' . $row->Mandatory . '" />';
				$strFields .= '<input type="hidden" name="rstrctn' . $i . '" value="' . $row->Restriction . '" />';
				$strFields .= '<input type="hidden" name="lbl' . $i . '" value="' . $row->FieldLabel . '" />';
				$i++;
			}
			$strFields .= '<input type="hidden" name="totalCustomFields" value="' . $totalFields . '" />';
			//=================================== CUSTOM FIELDS ============================================//

			//=================================== CUSTOM FIELD INCASE OF NO IMEI ============================================//
			$strCustomFld = $mxLn = $mnLn = '';
			$cusFldRes = 0;

			if ($imeiFType == '3' && $customFldId > 0) {
				$rwCustFld = $this->User_model->get_fields_by_id($customFldId);
				if (isset($rwCustFld->FieldLabel) && $rwCustFld->FieldLabel != '') {
					if ($rwCustFld->MinLength > 0 && is_numeric($rwCustFld->MinLength))
						$mnLn = $rwCustFld->MinLength;
					if ($rwCustFld->MaxLength > 0 && $rwCustFld->MaxLength != '')
						$mxLn = $rwCustFld->MaxLength;
					$cusFldRes = $rwCustFld->Restriction;
					$strCustomFld = '<label id="lblCustomFld">' . $rwCustFld->FieldLabel . '</label>
                    <textarea placeholder="Please Enter ' . $rwCustFld->FieldLabel . '" rows="5" class="form-control" name="txtCustomFld"></textarea>';
				}
			}
			//=================================== CUSTOM FIELD INCASE OF NO IMEI ============================================//

			//=================================== FEATURES ============================================//
			$strFeatures .= getServiceFeatures($packId, 0);
			//=================================== FEATURES ============================================//

			$msg = $mustRead . "~" . $timeTaken . "~" . $apiId . "~" . $pckTitle . "~" . $strCustomFld . "~" . $dupIMEIs . "~" . $imeiFType . "~" . $pckImg . "~" . $strMobiles . "~" . $preCode . "~" . $strBrands . "~" . $extNtwrkId . "~" . $brandAPIId . "~" . $packagePrice . "~" . $strFields . "~" . $redirectTo . "~" . $toc . "~" . $strFeatures . "~" . $mxLn . "~" . $cusFldRes . "~" . $mnLn;

		} else if ($purpose == 'getMdls') {
			$brandAPIId = $this->input->post('brandAPIId') ?: '0';
			$brandId = $this->input->post('brandId') ?: '0';
			$packId = $this->input->post('packId') ?: '0';
			$strModels = '';
			$rsModels = $this->User_model->get_models($brandId, $packId);
			$t = 0;
			$selectedModel = '';
			foreach ($rsModels as $row) {
				if ($t == 0) {
					$selectedModel = stripslashes($row->Value);
				}
				$strModels .= "<option value='" . $row->Id . "'>" . stripslashes($row->Value) . "</option>";
				$t++;
			}
			$msg = $strModels . '~GSMF~' . $selectedModel;
		}

		echo $msg;
		exit();

	}

	public function file_services()
	{
		if ($this->input->server('HTTP_HOST') != 'localhost') {
			if (!checkCountryRestriction($this->session->userdata('GSM_FUS_UserId'), $this->session->userdata('UserEmail'), $this->session->userdata('UserName'), 'File')) {
				redirect(base_url('page/404'));
			}
		}
		$IMEI_TYPE = '1';
		$this->data['mustRead'] = '';
		$this->data['dupMsg'] = '';
		$this->data['message'] = '';
		$this->data['msg'] = '';
		$categoryId = $this->input->post("categoryId") ?: 0;
		$myNetworkId = $this->input->post("packageId") ?: 0;
		$this->data['myNetworkId'] = $myNetworkId;
		$notes = $this->input->post("txtNotes") ?: '';
		$this->data['notes'] = $notes;
		$comments = $this->input->post("txtComments") ?: '';
		$this->data['comments'] = $comments;
		$cldFrm = $this->input->post("cldFrm") ?: 0;
		$altEmail = $this->input->post("txtAltEmail") ?: '';
		$this->data['altEmail'] = $altEmail;


		if (($this->input->post('packageId')) && $this->session->userdata('GSM_FUS_UserId') && $this->session->userdata('ORDERED_FROM') == '1') {
			if ($cldFrm == '2') {
				if ($this->data['userDetails']->PinCode != '') {
					$pinCode = $this->input->post('txtPinCode') ?: 0;
					$rowPC = $this->User_model->get_user_pincode();
					if (isset($rowPC->PinCode) && theString_Decrypt($rowPC->PinCode) == $pinCode && $pinCode != '') {
						include APPPATH . 'scripts/submitfileorder.php';
					} else {
						$this->data['message'] = $this->lang->line('CUST_LBL_179');
					}
				} else {
					include APPPATH . 'scripts/submitfileorder.php';
				}

			}
		}

		$this->data['strPackIds'] = getUserPacksIds($this->session->userdata('GSM_FUS_UserId'), 1);
		$rsNews = $this->User_model->get_shortdescription();

		$this->data['success'] = '';
		if ($this->input->post_get('success') && $this->input->post_get('success') == '1') {
			$this->data['success'] = '<div class="alert alert-success">' . $this->lang->line('CUST_LBL_181') . '</div>';
		}

		$this->data['view'] = 'file_services';
		$this->load->view('layouts/default', $this->data);

	}

	public function ajxfileorder()
	{
		$purpose = $this->input->post_get('purpose');

		if ($purpose == 'getData') {
			$packId = $this->input->post('packId') ?: '0';
			$usrCurrId = $this->input->post('usrCurrId') ?: '0';
			$cnvrsnRt = $this->input->post('cnvrsnRt') ?: '0';
			$msg = '';
			$mustRead = '';
			$timeTaken = '';
			$apiId = 0;
			$pckTitle = '';
			$refresh = 0;
			$dupIMEIs = 0;
			$extNtwrkId = 0;
			$strMobiles = '';
			$strBrands = '';
			$apiType = 0;
			$rsPackage = $this->User_model->get_packages_data($packId);
			if (isset($rsPackage->PackageTitle) && $rsPackage->PackageTitle != '') {
				$mustRead = htmlspecialchars_decode(stripslashes($rsPackage->MustRead));
				$timeTaken = stripslashes($rsPackage->TimeTaken);
				$pckTitle = stripslashes($rsPackage->PackageTitle);
				$redirectTo = $rsPackage->RedirectionURL;
				$strFeatures = $rsPackage->ServiceType == '0' ? '<i class="fa fa-reorder" title="Database"></i>&nbsp;&nbsp;&nbsp;' : '';
				$strFeatures .= $rsPackage->VerifyOrders == '1' ? '<i class="fa fa-refresh" title="Verifiable"></i>&nbsp;&nbsp;&nbsp;' : '';
				$strFeatures .= $rsPackage->CancelOrders == '1' ? '<i class="fa fa-times" title="Can be cancelled"></i>' : '';
			}
			//=================================== FEATURES ============================================//
			$strFeatures .= getServiceFeatures($packId, 1);
			//=================================== FEATURES ============================================//
			$msg = $pckTitle . "~" . $timeTaken . "~" . $mustRead . "~" . $redirectTo . "~" . $strFeatures;
		}
		echo $msg;
	}

	public function server_services()
	{
		if ($this->input->server('HTTP_HOST') != 'localhost') {
			if (!checkCountryRestriction($this->session->userdata('GSM_FUS_UserId'), $this->session->userdata('UserEmail'), $this->session->userdata('UserName'), 'Server')) {
				redirect(base_url('page/404'));
			}
		}
		$this->data['message'] = '';
		$categoryId = $this->input->post("categoryId") ?: 0;
		$myNetworkId = $this->input->post("packageId") ?: 0;
		$altEmail = $this->input->post("txtAltEmail") ?: '';
		$logPackageId = $this->input->post('packageId') ?: '0';
		$notes = $this->input->post('txtNotes') ?: '';
		$comments = $this->input->post("txtComments") ?: '';
		$cldFrm = $this->input->post("cldFrm") ?: 0;
		$cldFm = $this->input->post("cldFm") ?: 0;
		$this->data['strPackIds'] = getUserPacksIds($this->session->userdata('GSM_FUS_UserId'), 1);
		$packagePrice = '';
		$pckDetail = '-';
		$errorMsg = '';
		//$strPackIds = " AND LogPackageId NOT IN (".getUserPacksIds($_SESSION['GSM_FUS_UserId'], 2, $objDBCD14).")";
		$USER_ID = $this->session->userdata('GSM_FUS_UserId');
		$SERIAL_NO = '';
		if (($this->input->post('packageId')) && $this->session->userdata('GSM_FUS_UserId') && $this->session->userdata('ORDERED_FROM') == '1') {
			if ($cldFrm == '2') {
				$isQuantity = false;
				$PACK_QUANTITY = 1;
				$col_val = '';
				$historyData = '';
				$ttlFields = $this->input->post("totalCustomFields") ?: '0';
				$PRE_ORDER = $this->input->post('hdPreOrder') ?: '0';

				$MIN_QTY = $this->input->post('min_qty') ?: '1';
				if (!is_numeric($MIN_QTY))
					$MIN_QTY = 1;
				for ($x = 1; $x <= $ttlFields; $x++) {
					$col = $this->input->post('colNm' . $x) ?: '';
					if ($col != '') {
						$val = $this->input->post('fld' . $x) ?: '';
						$USEASQTY = $this->input->post('useAsQty' . $x) ?: '0';

						$col_val .= ", $col = '$val'";
						if ($col == 'quantity_G7V7') {
							if (!is_numeric($val)) {
								$errorMsg .= "Invalid value of Quantity.<br />";
							} else {
								if ($MIN_QTY > 1) {
									if ($val < $MIN_QTY)
										$errorMsg .= "Minimum Quantity should be $MIN_QTY<br />";
									else {
										$isQuantity = true;
										$PACK_QUANTITY = $val;
									}
								} else {
									$isQuantity = true;
									$PACK_QUANTITY = $val;
								}
							}
						} else if ($col == 'serialno_U4L6') {
							$SERIAL_NO = $val;
						}
						if ($USEASQTY == '1') {
							if (!is_numeric($val)) {
								$errorMsg .= "Invalid value of " . $this->input->post('lbl' . $x) . ".<br />";
							} else {
								$isQuantity = true;
								$PACK_QUANTITY = $val;
							}
						}

						$isMandatory = $this->input->post('mndtry' . $x) ?: '0';
						$chkRestriction = $this->input->post('rstrctn' . $x) ?: '0';
						if ($isMandatory == '1' && $val == '')
							$errorMsg .= "Value for " . $this->input->post('lbl' . $x) . " can not be empty.<br />";
						if ($chkRestriction == '1') // DIGITS ONLY
						{
							$errorMsg .= digits_validation($val, $this->input->post('lbl' . $x));
						} else if ($chkRestriction == '2') // CHARACTERS ONLY
						{
							$errorMsg .= characters_validation($val, $_POST['lbl' . $x]);
						} else if ($chkRestriction == '3') // DIGITS & CHARACTERS ONLY
						{
							$errorMsg .= validateAlphabetsAndDigits($val, $_POST['lbl' . $x]);
						}
						if ($historyData != '')
							$historyData .= '<br />';
						$historyData .= $this->input->post['lbl' . $x] . ": " . $val;
					}
				}
				if (trim($errorMsg) == '') {
					if ($this->data['userDetails']->PinCode != '') {
						$pinCode = $this->input->post('txtPinCode') ?: 0;
						$rowPC = $this->User_model->get_pincode();
						if (isset($rowPC->PinCode) && theString_Decrypt($rowPC->PinCode) == $pinCode && $pinCode != '') {
							include APPPATH . 'scripts/submitserverorder.php';
							$notes = '';
							$comments = '';
							$logPackageId = 0;
							$packagePrice = '';
							$myNetworkId = 0;
						} else {
							$this->data['message'] = $this->lang->line('CUST_LBL_179');
						}
					} else {
						include APPPATH . 'scripts/submitserverorder.php';
						$myNetworkId = 0;
					}
				}

			}
		}
		$this->data['timeTaken'] = '';
		$this->data['myNetworkId'] = $myNetworkId;
		$this->data['success'] = '';
		if ($this->input->post_get('success') && $this->input->post_get('success') == '1') {
			$this->data['success'] = '<div class="alert alert-success">' . $this->lang->line('CUST_LBL_185') . '</div>';
		}
		$this->data['errorMsg'] = $errorMsg;
		$this->data['notes'] = $notes;
		$this->data['comments'] = $comments;
		$this->data['altEmail'] = $altEmail;
		$this->data['cldFm'] = $cldFm;
		$this->data['rsNews'] = $this->User_model->get_ShortDescription_by_server();

		$this->data['view'] = 'server_services';
		$this->load->view('layouts/default', $this->data);
	}

	public function ajxserverorder()
	{
		$purpose = $this->input->post('purpose');
		$msg = '';
		if ($purpose == 'getData') {
			$packId = $this->input->post('packId') ?: '0';
			$themeStyle = $this->input->post('themeStyle') ?: '0';
			$mustRead = '';
			$timeTaken = '';
			$rsPackage = $this->User_model->get_rs_log_packages($packId);
			if (isset($rsPackage->LogPackageTitle) && $rsPackage->LogPackageTitle != '') {
				$packTitle = stripslashes($rsPackage->LogPackageTitle);
				$mustRead = stripslashes($rsPackage->LogPackageDetail);
				$timeTaken = stripslashes($rsPackage->DeliveryTime);
				$preCode = $rsPackage->CalculatePreCodes;
				$redirectTo = $rsPackage->RedirectionURL;
				$minQty = $rsPackage->MinimumQty;
				$strFeatures = $rsPackage->ServiceType == '0' ? '<i class="fa fa-reorder" title="Database"></i>&nbsp;&nbsp;&nbsp;' : '';
				$strFeatures .= $rsPackage->VerifyOrders == '1' ? '<i class="fa fa-refresh" title="Verifiable"></i>&nbsp;&nbsp;&nbsp;' : '';
				$strFeatures .= $rsPackage->CancelOrders == '1' ? '<i class="fa fa-times" title="Can be cancelled"></i>' : '';
			}
			$rsFields = $this->User_model->get_package_custom_field($packId);
			$totalFields = count($rsFields);
			$i = 1;
			$strFields = '';
			$qtyField = false;
			foreach ($rsFields as $row) {
				if ($themeStyle == '0')
					$strFields .= '<div class="form-group"><label>' . $row->FieldLabel . ':';
				else if ($themeStyle == '1')
					$strFields .= '<div class="control-group"><label class="control-label">' . $row->FieldLabel . ':';
				if ($row->Mandatory == '1') $strFields .= '*';
				if ($themeStyle == '0')
					$strFields .= '</label>';
				else if ($themeStyle == '1')
					$strFields .= '</label><div class="controls">';
				if ($row->FieldType == 'Text Box') {
					$strMaxLen = '';
					if ($row->MaxLength > 0)
						$strMaxLen = "maxlength = '" . $row->MaxLength . "'";
					if ($row->FieldColName == 'quantity_G7V7') {
						$strFields .= '<input type="text" placeholder="Enter ' . $row->FieldLabel . '" name="fld' . $i . '" id="ordrQty" onblur="calculateAmountPerQty();" ' . $strMaxLen . ' class="form-control" /><img src="assets/img/loading.gif" border="0" id="spnQtyLdr" alt="Please wait..." style="display:none;" />';
						if ($minQty > 1) {
							$strFields .= '<br><b>Minimum Quantity Required is ' . $minQty . '</b>';
						}
						$qtyField = true;
					} else
						$strFields .= '<input type="text" placeholder="Enter ' . $row->FieldLabel . '" name="fld' . $i . '" ' . $strMaxLen . ' class="form-control" />';
				} else if ($row->FieldType == 'Text Area')
					$strFields .= '<textarea name="fld' . $i . '" class="form-control"></textarea>';
				else if ($row->FieldType == 'Drop Down') {
					$strFields .= '<select id="ddOrdrQty" name="fld' . $i . '" class="form-control" ';
					if ($row->UseAsQuantity == '1')
						$strFields .= 'onchange="calculateAmountAsPerQty();">';
					else
						$strFields .= '>';
					$rsValues = $this->User_model->get_regvalue($row->FieldId);

					if ($row->UseAsQuantity == '1')
						$strFields .= '<option value="" selected="selected">Please Select ' . $row->FieldLabel . '</option>';

					foreach ($rsValues as $rw) {
						$strFields .= '<option value="' . $rw->RegValue . '">' . $rw->RegValue . '</option>';
					}
					$strFields .= '</select>';
					if ($row->UseAsQuantity == '1') {
						$strFields .= '<img src="' . base_url() . 'assets/img/loading.gif" border="0" id="spnDDQtyLdr" alt="Please wait..." style="display:none;" />';
					}

				} else if ($row->FieldType == 'Radio Button') {
					$rsValues = $this->User_model->get_regvalue($row->FieldId);
					foreach ($rsValues as $rw) {
						$strFields .= '<input type="radio" class="form-control" name="fld' . $i . '" value="' . $rw->RegValue . '">&nbsp;' . $rw->RegValue . '&nbsp;&nbsp;&nbsp;';
					}
				}
				if ($row->FInstructions != '')
					$strFields .= '<br><small>' . stripslashes($row->FInstructions) . '</small>';
				if ($themeStyle == '0')
					$strFields .= '</div>';
				else if ($themeStyle == '1')
					$strFields .= '</div></div>';
				$strFields .= '<input type="hidden" name="colNm' . $i . '" value="' . $row->FieldColName . '" />';
				$strFields .= '<input type="hidden" name="mndtry' . $i . '" value="' . $row->Mandatory . '" />';
				$strFields .= '<input type="hidden" name="rstrctn' . $i . '" value="' . $row->Restriction . '" />';
				$strFields .= '<input type="hidden" name="lbl' . $i . '" value="' . $row->FieldLabel . '" />';
				$strFields .= '<input type="hidden" name="useAsQty' . $i . '" value="' . $row->UseAsQuantity . '" />';
				$i++;
			}
			$strFields .= '<input type="hidden" name="totalCustomFields" value="' . $totalFields . '" />';
			if ($qtyField) {
				$groupId = $this->input->post('grp') ?: '0';
				if ($groupId > 0)
					$rsBulkPrices = $this->User_model->get_min_max_prices_by_group_id($packId, $groupId);
				else
					$rsBulkPrices = $this->User_model->get_min_max_prices_by_service_id($packId);
				$strBulkPrices = '';
				if ($rsBulkPrices) {
					$strBulkPrices = '
                    <table class="table table-bordered table-striped" width="100%">
                        <tr><td colspan="3"><h3>Special Bulk Prices</h3></td>
                        <tr>
                            <th width="33%" nowrap="nowrap">From Qty</th>
                            <th width="33%" nowrap="nowrap">To Qty</th>
                            <th width="33%">Credits</th>
                        </tr>';
					foreach ($rsBulkPrices as $row) {
						$strBulkPrices .= '
                            <tr>
                                <td>' . $row->MinQty . '</td>
                                <td>' . $row->MaxQty . '</td>
                                <td>' . $row->Price . '</td>
                            </tr>';
					}
					$strBulkPrices .= '</table>';
				}
			}
			//=================================== FEATURES ============================================//
			$strFeatures .= getServiceFeatures($packId, 2);
			//=================================== FEATURES ============================================//
			$msg = $mustRead . "~" . $timeTaken . "~" . $strFields . "~" . $preCode . "~" . $packTitle . "~" . $redirectTo . "~" . $strBulkPrices . "~" . $strFeatures . "~" . $minQty;
		} else if ($purpose == 'getQtyPrice') {
			$packId = $this->input->post('packId') ?: '0';
			$quantity = $this->input->post('qty') ?: '0';
			$msg = bulkQuantity_ServerService($packId, $quantity, $this->session->userdata('GSM_FUS_UserId'));
		}
		echo $msg;
	}

	public function imei_orders()
	{

		$strWhere = '';
		$this->data['serviceType'] = '0';
		$txtlqry = '';
		$message = '';

		$dtFrom = $this->input->post('txtFromDt') ?: '';
		$this->data['dtFrom'] = $dtFrom;

		$dtTo = $this->input->post('txtToDt') ?: '';
		$this->data['dtTo'] = $dtTo;

		$cancel = $this->input->post_get('cancel') ? 1 : 0;

		$sortBy = $this->input->post('sortBy') ?: '';
		$this->data['sortBy'] = $sortBy;

		$orderByType = $this->input->post('orderByType') ?: 'DESC';
		$this->data['orderByType'] = $orderByType;

		$strSortBy = $sortBy == '' ? " ORDER BY CodeId DESC" : " ORDER BY $sortBy $orderByType";
		$notes = $this->input->post('txtNotes') ?: '';

		if ($this->input->post('btnVerifySave') && $this->input->post('txtVData') != '') {
			$codeId = $this->input->post('orderId') ?: 0;
			$vData = $this->input->post('txtVData') ?: '';
			$row = $this->User_model->get_code_packages($codeId);
			if ($row->ReplyDtTm != '') {
				$diffInMins = minutesDiffInDates($row->ReplyDtTm, $currDtTm = setDtTmWRTYourCountry());
				if ($diffInMins <= $row->OrderVerifyMins) {
					$ip = $this->input->server('REMOTE_ADDR');
					$this->User_model->update_gf_codes($vData, $ip, $codeId);

					orderVerificationEmail(0, 'admincp/', $codeId, stripslashes($row->PackageTitle), stripslashes($row->Code), $row->Credits, $ip, $row->RequestedAt, $row->IMEINo, $vData);
					$message = $this->lang->line('CUST_GEN_MSG');
				} else
					$message = 'Invalid Operation';
			} else
				$message = 'Invalid Operation';
		}

		if ($cancel == '1' && $this->session->userdata('GSM_FUS_UserId')) {
			$orderId = $this->input->post_get('orderId') ?: 0;
			$row = $this->User_model->get_cancel_code_packages($orderId);

			if ($row->RequestedAt != '') {
				$diffInMins = minutesDiffInDates($row->RequestedAt, $currDtTm = setDtTmWRTYourCountry());
				if ($diffInMins <= $row->OrderCancelMins) {
					if (isset($row->CodeId) && $row->CodeId == $orderId) {
						if (imeiOrderRefunded($orderId) == '0') {

							$this->db->update_cancel_codes($orderId);

							$currDtTm = setDtTmWRTYourCountry($objDBCD14);
							$dec_points = refundIMEICredits($this->session->userdata('GSM_FUS_UserId'), $orderId, $row->IMEINo, stripslashes($row->PackageTitle), $currDtTm, $row->PackageId, $row->Credits, '0', 'Cancelled By User');

							rejectedIMEIOrderEmail($this->session->userdata('UserEmail'), $this->session->userdata('UserFullName'), stripslashes($row->PackageTitle), $row->IMEINo, stripslashes($row->Code), $row->RequestedAt, $row->PackageId, $row->AlternateEmail, $row->Credits, $dec_points);
							$message = $this->lang->line('CUST_GEN_MSG');
						}
					} else {
						$message = $this->lang->line('CUST_LBL_210');
					}
				} else
					$message = 'Invalid Operation';
			} else
				$message = 'Invalid Operation';
		}

		$packageId = $this->input->post("packageId") ?: '0';
		$this->data['packageId'] = $this->input->post("packageId") ?: '0';
		$codeStatusId = $this->input->post("codeStatusId") ?: '0';
		$this->data['codeStatusId'] = $codeStatusId;
		$srlNo = $this->input->post("srlNo") ?: '';
		$imei = $this->input->post("imei") ?: '';
		$oId = $this->input->post_get("oId") ?: '';
		$this->data['oId'] = $oId;
		if (trim($oId) != '') {
			$strWhere .= " AND CodeId = '$oId'";
			$txtlqry .= "&oId=$oId";
		}
		if (trim($imei) != '') {
			$strWhere .= " AND IMEI LIKE '%$imei%'";
			$txtlqry .= "&imei=$imei";
		}
		if (trim($srlNo) != '') {
			$strWhere .= " AND SerialNo LIKE '%$srlNo%'";
			$txtlqry .= "&srlNo=$srlNo";
		}
		if ($packageId != '0') {
			$strWhere .= " AND A.PackageId = $packageId";
			$txtlqry .= "&packageId=$packageId";
		}
		if ($codeStatusId != '0') {
			$strWhere .= " AND A.CodeStatusId IN($codeStatusId)";
			$txtlqry .= "&codeStatusId=$codeStatusId";
		}
		if (trim($notes) != '') {
			$strWhere .= " AND Comments LIKE '%$notes%'";
			$txtlqry .= "&txtNotes=$notes";
		}

		if ($this->input->post('txtIMEI') && $this->input->post('txtIMEI') <> '') {
			$arrIMEIS = explode("\n", $this->input->post("txtIMEI"));
			$imeis = '';
			for ($i = 0; $i < sizeof($arrIMEIS); $i++) {
				if ($imeis == '')
					$imeis .= "'" . substr(trim($arrIMEIS[$i]), 0, 15) . "'";
				else {
					if (trim($arrIMEIS[$i]) != '')
						$imeis .= ",'" . substr(trim($arrIMEIS[$i]), 0, 15) . "'";
				}
			}
			if ($imeis != '0')
				$strWhere .= " AND IMEINo IN(" . $imeis . ")";
		}

		if (($this->input->post('txtFromDt'))) {
			if ($dtFrom != '' && $dtTo != '') {
				$strWhere .= " And DATE(RequestedAt) >= '$dtFrom' AND DATE(RequestedAt) <= '$dtTo'";
				$txtlqry .= "&txtFromDt=$dtFrom&txtToDt=$dtTo";
			} else {
				if ($dtFrom != '') {
					$strWhere .= " AND DATE(RequestedAt) >= '$dtFrom'";
					$txtlqry .= "&txtFromDt=$dtFrom";
				}
				if ($dtTo != '') {
					$strWhere .= " AND DATE(RequestedAt) <= '$dtTo'";
					$txtlqry .= "&txtToDt=$dtTo";
				}
			}
		}

		$page_name = $this->input->server('PHP_SELF');

		if (!isset($start))
			$start = 0;
		if (($this->input->post('start')))
			$start = $this->input->post("start");

		$eu = ($start - 0);
		$limit = $this->input->post('records') && is_numeric($this->input->post('records')) ?: 50;
		$this->data['limit'] = $limit;

		$txtlqry .= "&records=$limit";
		$thisp = $eu + $limit;
		$back = $eu - $limit;
		$next = $eu + $limit;
		$USER_ID = $this->session->userdata('GSM_FUS_UserId');
		$cldFrmLogin = true;
		$strLimit = " limit $eu, $limit";

		$PACKS = array();
		$rsPacks = $this->User_model->sl3lbf_packages();
		foreach ($rsPacks as $rw) {
			$PACKS[$rw->PackageId] = stripslashes($rw->PackageTitle) . '-->' . $rw->CancelOrders . '-->' . $rw->VerifyOrders . '-->' . $rw->OrderVerifyMins . '-->' . $rw->OrderCancelMins;
		}
		$this->data['PACKS'] = $PACKS;
		$STATUSES = array();
		$rsStatuses = $this->User_model->get_code_status();
		foreach ($rsStatuses as $rw) {
			$STATUSES[$rw->CodeStatusId] = stripslashes($rw->CodeStatus);
		}
		$this->data['STATUSES'] = $STATUSES;
		$this->data['strPckWhere'] = ' AND sl3lbf = 0';

		$this->data['rsCodes'] = $this->User_model->get_order_codes($strWhere, $strSortBy, $strLimit);


		$this->session->set_userdata('IMEI_ORDRS_WHRE', $strWhere);
		$this->data['totalCodes'] = count($this->data['rsCodes']);
		$this->data['view'] = 'imei_orders';
		$this->load->view('layouts/default', $this->data);
	}

	public function exportimeiorders()
	{

		$strWhere = '';
		$str = '';

		$ids = $this->input->post_get('ids') ?: '';
		$cFrm = $this->input->post_get('cFrm') ?: '0';

		if ($ids != '')
			$strWhere = " AND CodeId IN ($ids)";
		if ($cFrm == '1')
			$strWhere .= $this->session->userdata('IMEI_ORDRS_WHRE');

		$rs = $this->User_model->get_codes_data($strWhere);
		$str .= "Service,IMEI,Credits,Model,Status,Code,Date,Notes\n";
		foreach ($rs as $row) {
			$imei = str_replace('<br />', '', $row->IMEINo);
			$code = str_replace('<br />', '', $row->Code);
			$myCode = '';

			$arrCodes = explode("\n", $code);
			for ($j = 0; $j < sizeof($arrCodes); $j++) {
				$value = trim($arrCodes[$j]);
				if ($value != '') {
					if ($myCode == '')
						$myCode = $value;
					else
						$myCode .= ' - ' . $value;
				}
			}
			$str .= $row->PackageTitle . ",";
			$str .= strval($imei) . ",";
			$str .= $row->Credits . ",";
			$str .= $row->ModelNo . ",";
			$str .= $row->CodeStatus . ",";
			$str .= $myCode . ",";
			$str .= $row->RequestedAt . ",";
			$str .= $row->Comments . "\n";
		}

		header("Pragma: public");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-Disposition: attachment; filename="CodesHistory-' . date('Y-m-d H:m:s') . '.csv"');
		print($str);


	}

	public function file_orders()
	{
		if ($this->data['settings'] == '0') {
			redirect(base_url('dashboard'));
		}
		$this->data['dupMsg'] = '';
		$this->data['msg'] = '';
		$strWhere = $txtlqry = $message = '';
		$serviceType = '1';
		$verify = $this->input->post_get('verify') ? 1 : 0;
		$cancel = $this->input->post_get('cancel') ? 1 : 0;
		$dtFrom = $this->input->post_get('txtFromDt') ?: '';
		$dtTo = $this->input->post('txtToDt') ?: '';
		$sortBy = $this->input->post('sortBy') ?: '';
		$orderByType = $this->input->post_get('orderByType') ?: 'DESC';
		$strSortBy = $sortBy == '' ? " ORDER BY CodeId DESC" : " ORDER BY $sortBy $orderByType";

		if ($verify == '1') {
			$codeId = $this->input->post_get('CodeId') ?: 0;
			$row = $this->User_model->get_slbf_code_by_id($codeId);
			if ($row->ReplyDtTm != '') {
				$diffInMins = minutesDiffInDates($row->ReplyDtTm, $currDtTm = setDtTmWRTYourCountry());
				if ($diffInMins <= $row->OrderVerifyMins) {
					$ip = $_SERVER['REMOTE_ADDR'];
					$this->User_model->update_slbf_codes($ip, $codeId);

					orderVerificationEmail('1', $codeId, stripslashes($row->PackageTitle), stripslashes($row->Code), $row->Credits, $ip, $row->RequestedAt, $row->IMEINo);
					$message = $this->lang->line('CUST_GEN_MSG');
				} else
					$message = 'Invalid Operation';
			} else
				$message = 'Invalid Operation';
		}
		if ($cancel == '1' && $this->session->userdata('GSM_FUS_UserId')) {
			$orderId = $this->input->post_get('orderId') ?: 0;
			$row = $this->User_model->get_slbf_packages($orderId);
			if ($row->RequestedAt != '') {
				$diffInMins = minutesDiffInDates($row->RequestedAt, $currDtTm = setDtTmWRTYourCountry());
				if ($diffInMins <= $row->OrderCancelMins) {
					if (isset($row->CodeId) && $row->CodeId == $orderId) {
						if (fileOrderRefunded($orderId) == '0') {
							$update_data = array(
								'CodeStatusId' => 3,
								'Refunded' => 1,
								'CancelledByUser' => 1,
								'CheckDuplication' => 0,
								'Code' => 'Cancelled By User'
							);
							$this->db->update_slbf_codes_by_status($update_data, $orderId);

							$currDtTm = setDtTmWRTYourCountry();
							$dec_points = refundFileCredits($this->session->userdata('GSM_FUS_UserId'), $orderId, $row->IMEINo, stripslashes($row->PackageTitle), $currDtTm, $row->PackageId, $row->Credits, '0', 'Cancelled By User');

							rejectedFileOrderEmail($this->session->userdata('UserEmail'), $this->session->userdata('UserFullName'), stripslashes($row->PackageTitle), $row->IMEINo, stripslashes($row->Code),
								$row->RequestedAt, $row->PackageId, $row->AlternateEmail, $row->Credits, $dec_points);
							$message = $this->lang->line('CUST_GEN_MSG');
						}
					} else {
						$message = $this->lang->line('CUST_LBL_210');
					}
				} else
					$message = 'Invalid Operation';
			} else
				$message = 'Invalid Operation';
		}
		$packageId = $this->input->post_get("packageId") ?: '0';
		$codeStatusId = $this->input->post_get("codeStatusId") ?: '0';
		$imeiNo = $this->input->post_get("txtIMEI") ?: '';
		$oId = $this->input->post_get("oId") ?: '';
		if (trim($oId) != '') {
			$strWhere .= " AND CodeId = '$oId'";
			$txtlqry .= "&oId=$oId";
		}
		if (trim($imeiNo) != '' && $imeiNo <> 'Search IMEI')
			$strWhere = " AND IMEINo LIKE '%" . $purifier->purify(check_input($imeiNo, $objDBCD14->dbh)) . "%'";
		if ($packageId != '0')
			$strWhere .= " AND A.PackageId = '" . $purifier->purify(check_input($packageId, $objDBCD14->dbh)) . "'";
		if ($codeStatusId != '0')
			$strWhere .= " AND A.CodeStatusId = '" . $purifier->purify(check_input($codeStatusId, $objDBCD14->dbh)) . "'";
		if (isset($_REQUEST['txtFromDt'])) {
			if ($dtFrom != '' && $dtTo != '') {
				$strWhere .= " And DATE(RequestedAt) >= '$dtFrom' AND DATE(RequestedAt) <= '$dtTo'";
				$txtlqry .= "&txtFromDt=$dtFrom&txtToDt=$dtTo";
			} else {
				if ($dtFrom != '') {
					$strWhere .= " AND DATE(RequestedAt) >= '$dtFrom'";
					$txtlqry .= "&txtFromDt=$dtFrom";
				}
				if ($dtTo != '') {
					$strWhere .= " AND DATE(RequestedAt) <= '$dtTo'";
					$txtlqry .= "&txtToDt=$dtTo";
				}
			}
		}
		$this->data['page_name'] = $_SERVER['PHP_SELF'];

		if (!isset($start))
			$start = 0;
		if ($this->input->post_get("start"))
			$start = $this->input->post_get("start");

		$eu = ($start - 0);
		$limit = $this->input->post_get('records') && is_numeric($this->input->post_get('records')) && $this->input->post_get('records') != '0' ? $this->input->post_get($records) : 50;
		$txtlqry .= "&records=$limit";
		$this->data['thisp'] = $eu + $limit;
		$this->data['back'] = $eu - $limit;
		$this->data['next'] = $eu + $limit;
		$strPckWhere = ' AND sl3lbf = 1';

		$this->data['rsCodes'] = $this->User_model->get_slbf_packages_and_code_data($strWhere, $strPckWhere, $strSortBy, $eu, $limit);
		$this->data['totalCodes'] = count($this->data['rsCodes']);
		$STATUSES = array();
		$rsStatuses = $this->User_model->get_code_status();
		foreach ($rsStatuses as $rw) {
			$STATUSES[$rw->CodeStatusId] = stripslashes($rw->CodeStatus);
		}

		$this->data['success'] = '';
		if ($this->input->post_get('success') && $this->input->post_get('success') == '1') {
			$this->data['success'] = '<div class="alert alert-success">' . $this->lang->line('CUST_LBL_181') . '</div>';
		}

		$this->data['sortBy'] = $sortBy;
		$this->data['orderByType'] = $orderByType;
		$this->data['oId'] = $oId;
		$this->data['dtTo'] = $dtTo;
		$this->data['dtFrom'] = $dtFrom;
		$this->data['codeStatusId'] = $codeStatusId;
		$this->data['STATUSES'] = $STATUSES;
		$this->data['packageId'] = $packageId;
		$this->data['limit'] = $limit;
		$this->data['eu'] = $eu;
		$this->data['txtlqry'] = $txtlqry;
		$this->data['start'] = $start;
		$this->data['strWhere'] = $strWhere;
		$this->data['strPckWhere'] = $strPckWhere;
		$this->data['message'] = $message;
		$this->data['view'] = 'file_orders';
		$this->load->view('layouts/default', $this->data);

	}

	public function downloadsl3replyfile()
	{
		$id = $this->input->post_get('id') ?: 0;
		$file = '';
		$row = $this->User_model->get_replyfile($id);
		if (isset($row->ReplyFile) && $row->ReplyFile != '')
			$file = $row->ReplyFile;
		if ($file != '')
			downloadFile($file);
	}


	public function verifyorders()
	{
		$strWhere = '';
		$imei = '';
		$packageId = 0;
		$codeStatusId = 0;
		$searchType = 0;
		$txtlqry = '';
		$sqlwhere = '';
		$verifyPage = '1';
		$tblOrders = 'tbl_gf_codes';
		$this->data['page_name'] = $this->input->server('PHP_SELF');
		$searchType = $this->input->post_get('searchType') ?: '0';
		$uId = $this->input->post_get('uId') ?: 0;

		if ($uId != '0') {
			$strWhere .= " AND A.UserId = '$uId'";
			$txtlqry .= "&uId=$uId";
		}

		if (!isset($start))
			$start = 0;

		if ($this->input->post_get("start"))
			$start = $this->input->post_get("start");

		$eu = ($start - 0);
		$limit = $this->input->post_get('records') ?: 50;
		$thisp = $eu + $limit;
		$back = $eu - $limit;
		$next = $eu + $limit;
		$message = '';
		if ($this->input->post_get('hdsbmt') && $this->session->userdata('GSM_FSN_AdmId')) {
			$codeIds = $this->input->post_get('chkCodes') ?: 0;
			$totalCodes = 0;
			if (is_array($codeIds))
				$totalCodes = count($codeIds);

			$codeIds_reply = $this->input->post_get('chkReply') ?: 0;
			$totalCodes_reply = 0;
			if (is_array($codeIds_reply))
				$totalCodes_reply = count($codeIds_reply);
			if ($totalCodes_reply == 0)
				$message = $this->input->post('BE_LBL_577');
			else {
				for ($i = 0; $i < $totalCodes; $i++) {
					$updateStatus = true;
					$data = explode('|', $codeIds[$i]);
					$codeId = $data[0];
					$code = $this->input->post_get('txtCode' . $data[4]);
					if ($codeIds_reply != '0') {
						if (in_array($codeIds[$i], $codeIds_reply))
							$codeStatusId = '3';
						else
							$updateStatus = false;
					} else
						$updateStatus = false;
					if ($updateStatus) {
						$message = updateorderstatus($data, $codeStatusId, $codeId, $code);
						$this->User_model->update_gf_codes_by_code_id($codeId);
					}
				}
				for ($i = 0; $i < $totalCodes_reply; $i++) {
					$data = explode('|', $codeIds_reply[$i]);
					$codeId = $data[0];
					$code = $this->input->post_get('txtCode' . $data[4]);
					$codeStatusId = '2';
					$updateStatus = true;
					if ($codeIds != '0') {
						if (in_array($codeIds_reply[$i], $codeIds))
							$updateStatus = false;
					}
					if ($updateStatus) {
						$message = updateorderstatus($data, $codeStatusId, $codeId, $code);
						$this->User_model->update_gf_codes_by_code_id($codeId);
					}
				}
				$message = $this->lang->line('BE_GNRL_11');
			}
		}
		if ($this->input->post_get('txtIMEI')) {
			$imei = trim($this->input->post_get('txtIMEI'));
			$packageId = $this->input->post_get('packageId');
			$codeStatusId = $this->input->post_get('codeStatusId');
			$uName = $this->input->post_get('txtUName') ? trim($this->input->post_get('txtUName')) : '';
			if ($uName != '') {
				$strWhere = " AND UserName LIKE '%" . $uName . "%'";
				$txtlqry .= "&txtUName=$uName";
			}
			if ($imei != '') {
				$strWhere = " AND IMEINo LIKE '%" . $imei . "%'";
				$txtlqry .= "&txtIMEI=$imei";
			}
			if ($packageId != '0') {
				$strWhere .= " AND A.PackageId = '$packageId'";
				$txtlqry .= "&packageId=$packageId";
			}
			if ($codeStatusId != '0') {
				$strWhere .= " AND A.CodeStatusId = '$codeStatusId'";
				$txtlqry .= "&codeStatusId=$codeStatusId";
			}
		}

		$rsCodes = $this->User_model->get_codes_packages_and_userdata($strWhere, $eu, $limit);

		$this->data['next'] = $next;
		$this->data['thisp'] = $thisp;
		$this->data['pLast'] = '';
		$this->data['eu'] = $eu;
		$this->data['limit'] = $limit;
		$this->data['start'] = $start;
		$this->data['back'] = $back;
		$this->data['count'] = count($rsCodes);
		$this->data['strWhere'] = $strWhere;
		$this->data[rsCodes] = $rsCodes;
		$this->data['uId'] = $uId;
		$this->data['IS_DEMO'] = '';
		$this->data['txtlqry'] = $txtlqry;
		$this->data['message'] = $message;
		$this->data['view'] = 'verifyorders';
		$this->load->view('layouts/default', $this->data);

	}

	public function server_orders()
	{
		if ($this->data['settings']->ServerServices == '0') {
			redirect(base_url('dashboard'));
		}
		$PCK_TITLE = 'LogPackageTitle';
		$CD_STATUS = 'CodeStatus';
		$CATEGORY = 'Category';
		$this->data['myNetworkId'] = 0;
		$message = $strWhere = $txtlqry = '';
		$logPackageId = $this->input->post('logPackageId') ?: '0';
		$statusId = $this->input->post('statusId') ?: '0';
		$verify = $this->input->post_get('verify') ? 1 : 0;
		$cancel = $this->input->post_get('cancel') ? 1 : 0;
		$dtFrom = $this->input->post('txtFromDt') ?: '';
		$dtTo = $this->input->post('txtToDt') ?: '';
		$sortBy = $this->input->post('sortBy') ?: '';
		$orderByType = $this->input->post('orderByType') ?: 'DESC';
		$strSortBy = $sortBy == '' ? " ORDER BY LogRequestId DESC" : " ORDER BY $sortBy $orderByType";
		$bxUsrNm = $this->input->post_get('bxUsrNm') ?: '';
		$srlNo = $this->input->post_get('srlNo') ?: '';
		$usrNm = $this->input->post_get('usrNm') ?: '';
		$oId = $this->input->post_get('oId') ?: '';
		if ($verify == '1') {
			$codeId = $this->input->post_get('CodeId') ?: 0;
			$row = $this->User_model->fetch_requests_and_packages($codeId);
			if ($row->ReplyDtTm != '') {
				$diffInMins = minutesDiffInDates($row->ReplyDtTm, $currDtTm = setDtTmWRTYourCountry());
				if ($diffInMins <= $row->OrderVerifyMins) {
					$ip = $this->input->server('REMOTE_ADDR');
					$this->User_model->update_log_request($codeId, $ip);
					orderVerificationEmail('2', $codeId, stripslashes($row->LogPackageTitle), stripslashes($row->Code), $row->Credits, $ip, $row->RequestedAt);
					$message = $this->lang->line('CUST_GEN_MSG');
				} else
					$message = 'Invalid Operation';
			} else
				$message = 'Invalid Operation';
		}
		if ($cancel == '1' && $this->session->userdata('GSM_FUS_UserId')) {
			$orderId = $this->input->post_get('orderId') ?: 0;
			$row = $this->User_model->fetch_log_requests_and_packages($orderId);
			if ($row->RequestedAt != '') {
				$diffInMins = minutesDiffInDates($row->RequestedAt, $currDtTm = setDtTmWRTYourCountry());
				if ($diffInMins <= $row->OrderCancelMins) {
					if (isset($row->LogRequestId) && $row->LogRequestId == $orderId) {
						if (serverOrderRefunded($orderId) == '0') {
							$this->User_model->update_log_request_by_orderID($orderId);

							$currDtTm = setDtTmWRTYourCountry();
							$dec_points = refundServerCredits($this->session->userdata('GSM_FUS_UserId'), $orderId, stripslashes($row->LogPackageTitle), $currDtTm, $row->Credits, '0', 'Cancelled By User');

							rejectedServerOrderEmail($this->session->userdata('UserEmail'), $this->session->userdata('UserFullName'), stripslashes($row->LogPackageTitle), stripslashes($row->Code), $row->RequestedAt, $row->LogPackageId, $row->AlternateEmail, $row->Credits, $dec_points, $row->OrderData);

							$message = $this->lang->line('CUST_GEN_MSG');
						}
					} else {
						$message = $this->lang->line('CUST_LBL_210');
					}
				} else
					$message = 'Invalid Operation';
			} else
				$message = 'Invalid Operation';
		}
		if ($logPackageId != '0')
			$strWhere .= " AND A.LogPackageId = '" . $logPackageId . "' ";
		if ($statusId != '0')
			$strWhere .= " AND A.StatusId = '" . $statusId . "' ";
		if (isset($_REQUEST['txtFromDt'])) {
			if ($dtFrom != '' && $dtTo != '') {
				$strWhere .= " And DATE(RequestedAt) >= '" . $dtFrom . "' AND DATE(RequestedAt) <= '" . $dtTo . "'";
				$txtlqry .= "&txtFromDt=$dtFrom&txtToDt=$dtTo";
			} else {
				if ($dtFrom != '') {
					$strWhere .= " AND DATE(RequestedAt) >= '" . $dtFrom . "'";
					$txtlqry .= "&txtFromDt=$dtFrom";
				}
				if ($dtTo != '') {
					$strWhere .= " AND DATE(RequestedAt) <= '" . $dtTo . "'";
					$txtlqry .= "&txtToDt=$dtTo";
				}
			}
		}
		if (trim($oId) != '') {
			$strWhere .= " AND LogRequestId = '$oId'";
			$txtlqry .= "&oId=$oId";
		}
		if (trim($bxUsrNm) != '') {
			$strWhere .= " AND BoxUserName LIKE '%" . $bxUsrNm . "%'";
			$txtlqry .= "&bxUsrNm=$bxUsrNm";
		}
		if (trim($srlNo) != '') {
			$strWhere .= " AND SerialNo LIKE '%" . $srlNo . "%'";
			$txtlqry .= "&srlNo=$srlNo";
		}
		if (trim($usrNm) != '') {
			$strWhere .= " AND SUsername LIKE '%" . $usrNm . "%'";
			$txtlqry .= "&usrNm=$usrNm";
		}

		$page_name = $this->session->userdata('PHP_SELF');

		if (!isset($start))
			$start = 0;
		if ($this->input->post_get("start"))
			$start = $this->input->post_get("start");

		$eu = ($start - 0);
		$limit = $this->input->post_get('records') && is_numeric($this->input->post_get('records')) ? $this->input->post_get('records') : 50;
		$txtlqry .= "&records=$limit";
		$thisp = $eu + $limit;
		$back = $eu - $limit;
		$next = $eu + $limit;
		$CUST_COL_LBL = array();
		$rsFields = $this->User_model->get_fields_data();
		$strLimit = " limit $eu, $limit";
		$colIndex = 0;
		$strCustomCols = '';
		foreach ($rsFields as $row) {
			$strCustomCols .= ', ' . $row->FieldColName . ' AS Col' . $colIndex;
			$CUST_COL_LBL['Col' . $colIndex] = $row->FieldLabel;
			$colIndex++;
		}

		$this->data['colIndex'] = $colIndex;
		$this->data['dtTo'] = $dtTo;
		$this->data['dtFrom'] = $dtFrom;
		$this->data['orderByType'] = $orderByType;
		$this->data['sortBy'] = $sortBy;
		$this->data['limit'] = $limit;
		$this->data['oId'] = $oId;
		$this->data['rsCodes'] = $this->User_model->get_log_request_packages_status($strWhere, $strSortBy, $strLimit, $strCustomCols);
		$this->data['totalCodes'] = count($this->data['rsCodes']);

		$this->data['CUST_COL_LBL'] = $CUST_COL_LBL;
		$this->data['codestatusdata'] = $this->User_model->get_code_status1();
		$this->data['codeStatusId'] = '';
		$this->data['view'] = 'server_orders';
		$this->load->view('layouts/default', $this->data);
	}

	public function downloadSOrders()
	{
		$ids = isset($_REQUEST['ids']) ? check_input($_REQUEST['ids'], $objDBCD14->dbh) : 0;
		$strIMEIs = '';
		$strCustomCols = '';
		$colIndex = 1;
		$CUST_COL_LBL = array();
		$rsFields = $objDBCD14->query("SELECT FieldLabel, FieldColName FROM tbl_gf_custom_fields WHERE ServiceType = 2 ORDER BY FieldLabel");
		while ($row = $objDBCD14->fetchNextObject($rsFields)) {
			$strCustomCols .= ', ' . $row->FieldColName . ' AS Col' . $colIndex;
			$CUST_COL_LBL['Col' . $colIndex] = $row->FieldLabel;
			$colIndex++;
		}
		$rsData = $objDBCD14->query("SELECT A.LogPackageId, LogPackageTitle $strCustomCols FROM tbl_gf_log_requests A, tbl_gf_log_packages B WHERE 
                            A.LogPackageId = B.LogPackageId AND LogRequestId IN ($ids) ORDER BY PackOrderBy, LogPackageTitle, LogRequestId DESC");
		$prevPackId = 0;
		while ($row = $objDBCD14->fetchNextObject($rsData)) {

			if ($prevPackId > 0) {
				if ($row->LogPackageId != $prevPackId) {
					$strIMEIs .= "\r\n\r\n================================================================\r\nService: " . $row->LogPackageTitle .
						"\r\n================================================================\r\n";
				}
			} else {
				$strIMEIs .= "================================================================\r\nService: " . $row->LogPackageTitle .
					"\r\n================================================================\r\n";
			}
			if ($row->Code != '')
				$strIMEIs .= ' ' . $row->Code;
			for ($i = 1; $i < $colIndex; $i++) {
				$colName = 'Col' . $i;
				if (isset($row->$colName) && $row->$colName != '') {
					if (isset($CUST_COL_LBL[$colName]) && $CUST_COL_LBL[$colName] != '')
						$strIMEIs .= ' ' . $CUST_COL_LBL[$colName] . ':';
					$strIMEIs .= ' ' . $row->$colName;
				}
			}
			$strIMEIs .= "\r\n";
			$prevPackId = $row->LogPackageId;
		}
		$objDBCD14->close();
		if ($strIMEIs != '') {
			$fileName = rand(100000000, 999999999);
			$myFile = "imeifiles/$fileName.txt";
			$fh = fopen($myFile, 'w') or die("can't open file");
			fwrite($fh, $strIMEIs);
			fclose($fh);

			$file = 'imeifiles/' . $fileName . '.txt';
			$strData = '';
			$fh = fopen($file, 'r');
			$strData = fread($fh, filesize($file));
			fclose($fh);
			header("Pragma: public");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header('Content-Disposition: attachment; filename="' . $fileName . '.txt"');
			echo $strData;
		}
		exit;
	}

	public function exportserverorders()
	{
		$strWhere = '';
		$ids = $this->input->post_get('ids') ?: '';
		if ($ids != '')
			$strWhere = " AND LogRequestId IN ($ids)";
		$rs = $this->User_model->get_log_request_packages_status_by_ids($strWhere);
		$str .= "Package,Credits,Username,Serial No.,Box Username,Status,Code,Date\n";
		foreach ($rs as $row) {
			$code = str_replace('<br />', '', $row->Code);
			$myCode = '';

			$arrCodes = explode("\n", $code);
			for ($j = 0; $j < sizeof($arrCodes); $j++) {
				$value = trim($arrCodes[$j]);
				if ($value != '') {
					if ($myCode == '')
						$myCode = $value;
					else
						$myCode .= ' - ' . $value;
				}
			}
			$str .= $row->LogPackageTitle . ",";
			$str .= $row->Credits . ",";
			$str .= $row->SUsername . ",";
			$str .= $row->SerialNo . ",";
			$str .= $row->BoxUserName . ",";
			$str .= $row->CodeStatus . ",";
			$str .= $myCode . ",";
			$str .= $row->RequestedAt . "\n";
		}
		header("Pragma: public");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-Disposition: attachment; filename="LogRequests-' . date('Y-m-d H m s') . '.csv"');
		print($str);
	}


	public function downloadIMEIs()
	{
		$ids = $this->input->post_get('ids') ?: 0;
		$strIMEIs = '';

		$this->User_model->update_download($ids);

		$strCustomCols = '';
		$colIndex = 1;

		$rsFields = $this->User_model->get_field_col();
		foreach ($rsFields as $row) {
			$strCustomCols .= ', ' . $row->FieldColName . ' AS Col' . $colIndex;
			$colIndex++;
		}
		$rsData = $this->User_model->fetch_code_package_by_ids($strCustomCols, $ids);
		$prevPackId = 0;
		foreach ($rsData as $row) {

			if ($prevPackId > 0) {
				if ($row->PackageId != $prevPackId) {
					$strIMEIs .= "\r\n\r\n================================================================\r\nService: " . $row->PackageTitle .
						"\r\n================================================================\r\n";
				}
			} else {
				$strIMEIs .= "================================================================\r\nService: " . $row->PackageTitle .
					"\r\n================================================================\r\n";
			}
			if ($row->IMEINo != '')
				$strIMEIs .= $row->IMEINo;
			if ($row->Code != '')
				$strIMEIs .= ' ' . $row->Code;
			for ($i = 1; $i < $colIndex; $i++) {
				$colName = 'Col' . $i;
				if (isset($row->$colName) && $row->$colName != '')
					$strIMEIs .= ' ' . $row->$colName;
			}
			$strIMEIs .= "\r\n";
			$prevPackId = $row->PackageId;
		}
		if ($strIMEIs != '') {
			$fileName = rand(100000000, 999999999);
			$myFile = FCPATH . "imeifiles/$fileName.txt";
			$fh = fopen($myFile, 'w') or die("can't open file");
			fwrite($fh, $strIMEIs);
			fclose($fh);

			$file = FCPATH . 'imeifiles/' . $fileName . '.txt';
			$strData = '';
			$fh = fopen($file, 'r');
			$strData = fread($fh, filesize($file));
			fclose($fh);
			header("Pragma: public");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header('Content-Disposition: attachment; filename="' . $fileName . '.txt"');
			echo $strData;
			exit;
		}


	}

	public function iodetail()
	{
		$id = $this->input->post_get('id') ?: 0;
		$cr = $this->input->post_get('cr') ?: '';
		if ($id > 0) {
			$this->data['id'] = $id;
			$row = $this->User_model->get_code_data($id);
			if (isset($row->PackageTitle) && $row->PackageTitle != '') {
				$modelNo = $row->ModelNo == '' ? $row->Model : $row->ModelNo;
				$this->data['imeiNo'] = $row->IMEINo;
				$this->data['code'] = $row->Code == '' ? '-' : stripslashes($row->Code);
				$pckTitle = stripslashes($row->PackageTitle);
				$this->data['credits'] = $row->Credits;
				$this->data['comments'] = stripslashes($row->Comments1);
				$this->data['notes'] = stripslashes($row->Comments);
				$this->data['oDate'] = $row->RequestedAt;
				$this->data['replyDtTm'] = $row->ReplyDtTm == '' ? '-' : $row->ReplyDtTm;
				$serialNo = $row->SerialNo;
				$other = $row->OtherValue;
				$prd = $row->PRD;
			}
			$rwCurr = $this->User_model->get_user_data();
			if (isset($rwCurr->CurrencyAbb) && $rwCurr->CurrencyAbb != '')
				$this->data['curr'] = $rwCurr->CurrencyAbb;

			$this->data['rsFields'] = $this->User_model->get_custom_field();
		}

		$this->load->view('iodetail', $this->data);


	}

	public function fodetail()
	{
		$id = $this->input->post_get('id') ?: 0;
		if ($id > 0) {
			$row = $this->User_model->get_slbf_and_packages($id);
			if (isset($row->PackageTitle) && $row->PackageTitle != '') {
				$this->data['imeiNo'] = $row->IMEINo;
				$this->data['notes'] = stripslashes($row->Comments);
				$this->data['oDate'] = $row->RequestedAt;
				$this->data['replyDtTm'] = $row->ReplyDtTm == '' ? '-' : $row->ReplyDtTm;
				$this->data['code'] = $row->Code == '' ? '-' : stripslashes($row->Code);
				$pckTitle = stripslashes($row->PackageTitle);
				$this->data['credits'] = $row->Credits;
				$this->data['comments'] = stripslashes($row->Comments1);
			}
			$rwCurr = $this->User_model->get_currency_by_id();
			if (isset($rwCurr->CurrencyAbb) && $rwCurr->CurrencyAbb != '')
				$this->data['curr'] = $rwCurr->CurrencyAbb;
		}
		$this->data['id'] = $id;
		$this->load->view('fodetail', $this->data);
	}

	public function sodetail()
	{
		$id = $this->input->post_get('id') ?: 0;
		$cr = $this->input->post_get('cr') ?: '';
		if ($id > 0) {
			$row = $this->User_model->get_log_request_and_packages($id);
			if (isset($row->LogPackageTitle) && $row->LogPackageTitle != '') {
				$this->data['uName'] = stripslashes($row->SUsername);
				$this->data['boxUN'] = stripslashes($row->BoxUserName);
				$this->data['serial'] = stripslashes($row->SerialNo);
				$this->data['pckTitle'] = stripslashes($row->LogPackageTitle);
				$this->data['code'] = $row->Code == '' ? '-' : stripslashes($row->Code);
				$this->data['credits'] = $row->Credits;
				$this->data['comments'] = stripslashes($row->Comments1);
				$this->data['notes'] = stripslashes($row->Comments);
				$this->data['oDate'] = $row->RequestedAt;
				$this->data['replyDtTm'] = $row->ReplyDtTm == '' ? '-' : $row->ReplyDtTm;
			}
			$rwCurr = $this->User_model->get_currency_by_id();
			if (isset($rwCurr->CurrencyAbb) && $rwCurr->CurrencyAbb != '')
				$this->data['curr'] = $rwCurr->CurrencyAbb;
			$this->data['rsFields'] = $this->User_model->get_custom_field_by_servicetype();
		}

		$this->data['id'] = $id;
		$this->load->view('sodetail', $this->data);
	}

	public function login_security()
	{
		$errorMsg = '';
		$this->data['message'] = '';
		$id = $this->input->post_get('id') ?: '0';
		if ($id != '0') {
			$this->User_model->delete_user_login_countries($id);

			$this->data['message'] = 'Country has been removed from the list!';
		}
		if (($this->input->post('countryId'))) {
			if ($this->input->post('countryId') != '0' && $this->input->post('countryId') != '') {
				$iso = $this->input->post('countryId');
				$this->User_model->delete_user_login_countries($iso);
				$this->User_model->insert_user_country_login($iso);
				$this->data['message'] = 'Country has been added into your login countries list!';

			} else {
				$this->data['message'] = 'Please select a country!';
			}
		}
		$this->data['rsCntrs'] = $this->User_model->get_countries();

		$this->data['view'] = 'login_security';
		$this->load->view('layouts/default', $this->data);
	}

	public function changepassword()
	{
		$errorMsg = $message = '';
		if ($this->input->post('txtOldPass')) {
			$oldPassword = $this->input->post('txtOldPass');
			$password = $this->input->post('txtNewPass');
			$confirmPassword = $this->input->post('txtConfirmPass');
			//$errorMsg = $objForm->password_validation($password, 2, 'New Password');
			//$errorMsg .= $objForm->password_validation($confirmPassword, 2, 'Confirm Password');
			if ($errorMsg == '') {

				$dtTm = setDtTmWRTYourCountry();
				if ($confirmPassword == $password) {
					$row = $this->User_model->get_userpassword();
					if (isset($row->UserPassword) && $row->UserPassword != '') {
						if (isPasswordOK($oldPassword, $row->UserPassword)) {
							$encryptedPass = bFEncrypt($password);
							$this->User_model->update_userpassword($encryptedPass, $dtTm);
							$message = $this->lang->line('CUST_LBL_42');
						} else
							$message = $this->lang->line('CUST_LBL_43');
					} else
						$message = $this->lang->line('CUST_LBL_43');
				} else
					$message = $this->lang->line('CUST_LBL_44');
			}
		}

		$this->data['errorMsg'] = '';
		$this->data['message'] = $message;
		$this->data['view'] = 'change_password';
		$this->load->view('layouts/default', $this->data);
	}

	public function myprofile()
	{
		$message = $errorMsg = '';
		$loginVerification = 0;
		$USER_VALUES = array();
		if ($this->input->post('txtFName')) {
			$firstName = $this->input->post("txtFName") ?: '';
			$lastName = $this->input->post("txtLName") ?: '';
			$phone = $this->input->post("txtPhone") ?: '';
			$countryId = $this->input->post("countryId") ?: '';
			$lang = $this->input->post("language") ?: '';
			$subscribe = $this->input->post("chkSubscribe") ? '1' : '0';
			$loginVerification = $this->input->post('chkLoginVerification') ? 1 : 0;
			$cPanel = $this->input->post("cPanel") ?: '0';

			$ttlFields = $this->input->post("totalRegFields") ?: '0';

			$errorMsg .= name_validation($firstName, $this->lang->line('CUST_ACC_2'));
			$errorMsg .= name_validation($lastName, $this->lang->line('CUST_ACC_3'));
			if ($phone == '')
				$errorMsg .= $this->lang->line('CUST_LBL_308');

			$cols = '';
			for ($x = 1; $x <= $ttlFields; $x++) {
				$col = $this->input->post('colNm' . $x) ?: '';
				if ($col != '') {
					$val = $this->input->post('fld' . $x) ?: '';
					$cols .= ", $col = '$val'";
					$isMandatory = $this->input->post('mndtry' . $x) ?: '0';
					if ($isMandatory == '1' && $val == '')
						$errorMsg .= "Value for " . $this->input->post('lbl' . $x) . " can not be empty.<br />";
				}
			}
			if (trim($errorMsg) == '') {
				$dtTm = setDtTmWRTYourCountry();
				$strThemeColor = '';
				if ($this->input->post('themeColor')) {
					$themeColor = $this->input->post("themeColor");
					$strThemeColor = ", ThemeColor = '$themeColor'";
				}


				$this->User_model->update_userprofiledata($firstName, $lastName, $phone, $countryId, $dtTm, $lang, $subscribe, $loginVerification, $cPanel, $strThemeColor, $cols);

				$this->session->set_userdata('UserFullName', stripslashes($firstName) . ' ' . stripslashes($lastName));
				$this->session->set_userdata('UserFName', stripslashes($firstName));
				$this->session->set_userdata('LANGUAGE', $lang);
				$message = $this->lang->line('CUST_GEN_MSG');

				if ($this->input->post('themeColor') || $this->input->post('olderPanel') != $cPanel) {
					/*redirect(base_url('dashboard'));*/
					$this->data['message'] = $message;

				}
			}
		}
		if ($this->session->userdata('GSM_FUS_UserId') > 0) {
			$row = $this->User_model->get_all_userdata();
			if (isset($row->UserEmail) && $row->UserEmail != '') {
				$email = $row->UserEmail;
				$this->data['fName'] = stripslashes($row->FirstName);
				$this->data['lName'] = stripslashes($row->LastName);
				$this->data['phone'] = stripslashes($row->Phone);
				$this->data['countryId'] = $row->CountryId;
				$this->data['lang'] = $row->UserLang;
				$this->data['subscribed'] = $row->Subscribed;
				$this->data['themeColor'] = $row->ThemeColor;
				$this->data['loginVerification'] = $row->TwoStepVerification;
				$this->data['cPanel'] = $row->CPanel;
				$this->data['rsCols'] = $this->User_model->get_registration_fields();
				foreach ($this->data['rsCols'] as $rw) {
					$colName = $rw->FieldColName;
					$USER_VALUES[$colName] = $row->$colName;
				}
			}
		}
		$this->data['USER_VALUES'] = $USER_VALUES;
		$this->data['errorMsg'] = $errorMsg;
		$this->data['message'] = $message;
		$this->data['rsFields'] = $this->User_model->get_registration_fields();
		$this->data['totalFields'] = count($this->data['rsFields']);

		$this->data['view'] = 'my_profile';
		$this->load->view('layouts/default', $this->data);
	}

	public function ticketdtls()
	{
		if (($this->input->post('tcktId')))
			$tcktId = $purifier->purify(check_input($this->input->post('tcktId'), $objDBCD14->dbh));
		$cldFrm = ($this->input->post('cldFrm')) ? $purifier->purify(check_input($this->input->post('cldFrm'), $objDBCD14->dbh)) : 0;
		$row = $objDBCD14->queryUniqueObject("SELECT DepartmentId, CategoryId, PriorityId, TicketNo, Subject, Name, Email, UserId, TcktCategory, DtTm, DeptName FROM tbl_gf_tickets A,
					tbl_gf_tckt_dept B, tbl_gf_tckt_category E WHERE A.DepartmentId = B.DeptId AND A.CategoryId = E.TcktCategoryId AND TicketId = '$tcktId'");
		if (isset($row->TicketNo) && $row->TicketNo != '') {
			$dId = $row->DepartmentId;
			$cId = $row->CategoryId;
			$pId = $row->PriorityId;
			$tcktNo = $row->TicketNo;
			$subject = stripslashes($row->Subject);
			$name = stripslashes($row->Name);
			$email = $row->Email;
			$userId = $row->UserId;
			$category = stripslashes($row->TcktCategory);
			$department = stripslashes($row->DeptName);
			$dt = $row->DtTm;
		}
		if (($this->input->post('btnReply')) && $cldFrm == '1') {
			$message = ($this->input->post('txtReply')) ? $purifier->purify(check_input($this->input->post('txtReply'), $objDBCD14->dbh)) : '';
			if (trim($message) == '') {
				$strColor = 'color:red';
				$msg = 'Reply can not be empty!';
			} else {
				$message = convertNextLineToBRTag($message);
				$dtTm = setDtTmWRTYourCountry($objDBCD14);
				$ip = $_SERVER['REMOTE_ADDR'];
				$objDBCD14->execute("INSERT INTO tbl_gf_tickets (TicketNo, UserId, DepartmentId, CategoryId, PriorityId, Name, Email, Subject, Message, DtTm, IP, StatusId, AId, ReadByAdmin, 
		ByAdmin) VALUES ('$tcktNo', '$userId', '$dId', '$cId', '$pId', '$name', '$email', '$subject', '$message', '$dtTm', '$ip', '4', '1', 0, 0)");
				send_push_notification('Your ticket has been replied!', $_SESSION['GSM_FUS_UserId'], $objDBCD14);

				$TICKET_ID = $objDBCD14->lastInsertedId();
				$objDBCD14->execute("UPDATE tbl_gf_tickets SET StatusId = '4', ReadByAdmin = 0 WHERE TicketNo = '$tcktNo'");
				ticketDetailsEmail($TICKET_ID, '2', $objDBCD14);
				//customerRepliedTicketEmail($department, $subject, $message, $dId, $objDBCD14);
				$msg = $CUST_GEN_MSG;
			}
		} else if (($this->input->post('btnClose')) && $cldFrm == '2') {
			$objDBCD14->execute("UPDATE tbl_gf_tickets SET StatusId = '2', ReadByAdmin = 0 WHERE TicketNo = '$tcktNo'");
			send_push_notification('Your ticket has been closed!', $_SESSION['GSM_FUS_UserId'], $objDBCD14);
			$msg = $CUST_GEN_MSG;
		}
		$rsTckts = $objDBCD14->query("SELECT TicketId, Name, F.UserName, CONCAT(F.FirstName, ' ', F.LastName) AS ClientName, A.StatusId, Message, DtTm, A.IP, TcktStatus, ByAdmin 
						FROM tbl_gf_tckt_status B, tbl_gf_tickets A LEFT JOIN tbl_gf_users F ON (A.UserId = F.UserId) WHERE A.StatusId = B.TcktStatusId 
						AND A.UserId = '" . $_SESSION['GSM_FUS_UserId'] . "' AND TicketNo = '$tcktNo' ORDER BY TicketId DESC");

		$totalReplies = $objDBCD14->numRows($rsTckts);
		$rwTS = $objDBCD14->queryUniqueObject("SELECT A.StatusId, TcktStatus FROM tbl_gf_tckt_status B, tbl_gf_tickets A WHERE A.StatusId = B.TcktStatusId AND TicketNo = '$tcktNo'
			AND UserId = '" . $_SESSION['GSM_FUS_UserId'] . "' ORDER BY TicketId DESC");
		if (isset($rwTS->StatusId) && $rwTS->StatusId > 0) {
			$statusId = $rwTS->StatusId;
			$status = $rwTS->TcktStatus;
		}

	}
}
