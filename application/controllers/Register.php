<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	}

	public function index() {
		$data = array();
		if($this->input->method() == 'post') {
			$this->form_validation->set_rules('txtUName', 'Username', 'trim|required|is_unique[tbl_gf_users.UserName]', array('is_unique' => 'This %s already exists.'));
			$this->form_validation->set_rules('txtEmail', 'Email', 'trim|required|valid_email|is_unique[tbl_gf_users.UserEmail]', array('is_unique' => 'This %s already exists.'));
			$this->form_validation->set_rules('txtPassword', 'Password', 'trim|required|min_length[5]');
			$this->form_validation->set_rules('txtRePassword', 'Confirm Password', 'trim|required|matches[txtPassword]');
			$this->form_validation->set_rules('txtFName', 'First name', 'trim|required');
			$this->form_validation->set_rules('txtLName', 'Last name', 'trim|required');
			$this->form_validation->set_rules('txtPhone', 'Phone', 'trim|required');
			$this->form_validation->set_rules('countryId', 'Country', 'trim|required');
			$this->form_validation->set_rules('currencyId', 'Currency', 'trim|required');
			$this->form_validation->set_rules('language', 'Language', 'trim|required');

			if($this->form_validation->run()) {
				$data['uName'] = $this->input->post('txtUName');
				$data['email'] = $this->input->post('txtEmail');
				$data['firstName'] = $this->input->post('txtFName');
				$data['lastName'] = $this->input->post('txtLName');
				$data['phone'] = $this->input->post('txtPhone');
				$data['currencyId'] = $this->input->post('currencyId');
				$countryInfo = $this->input->post('countryId');
				$arrCountry = explode('|', $countryInfo);
				$data['countryId'] = $arrCountry[0];
				$country = $arrCountry[1];
				$password = $this->input->post('txtPassword');
				$cPwd = $this->input->post('txtRePassword');
				$data['lang'] = $this->input->post('language');
				$ttlFields = $this->input->post('totalRegFields');
				$ttlFields = $ttlFields ?: 0;

				$data['dtTm'] = @date('Y-m-d H:i:s');
				$data['password'] = bFEncrypt($password);
				$data['ip'] = $_SERVER['REMOTE_ADDR'];

				$userId = $this->General_model->addUser($data);

				$rwISO = $this->General_model->fetchISOByCountryId($data['countryId']);
				$iso = $rwISO->ISO;

				$this->General_model->saveUserLoginCountries($userId, $iso);

				$rsStngs = $this->General_model->getEmailSettings();
				$ALLOWUSERTOLOGIN = $rsStngs->AllowNewUserToLogIn;

				registrationEmail($data['email'], $data['firstName'].' '.$data['lastName'], $data['phone'], $userId, $data['uName'], $ALLOWUSERTOLOGIN, $country, $data['ip']);

				if($ALLOWUSERTOLOGIN == '1') {
					$this->session->set_flashdata('message', "An email has been sent to your email address, please click the link in your email to activate your account!");
				}else{
					$this->session->set_flashdata('message', "Thank you for signing up, Your account is under review and will be activated once an admin approves it!");
				}
				redirect(base_url('login'));
			}
		}
		$data['view'] = 'register';
		$this->load->view('layouts/login_default', $data);
	}

	public function activateclient() {
		$user_Id = $this->input->get('tkn1');
		$user_Id = $user_Id ?: 0;
		$user_Name = $this->input->get('tkn2');
		$user_Name = $user_Name ?: 0;
		$user_Id = urlsafe_b64decode($user_Id);
		$user_Name = urlsafe_b64decode($user_Name);

		$rowAC = $this->General_model->fetchUserByUserIdAndUserName($user_Id, $user_Name);

		if (isset($rowAC->UserId) && $rowAC->UserId != '') {
			$days = differenceInDates($rowAC->AddedAt, date('Y-m-d H:i:s'));
			if($days == '0') {
				$this->General_model->enableUser($rowAC->UserId);
				$this->session->set_flashdata('message', "Thank you for registering with us. Your account has been activated!");
			} else {
				$this->session->set_flashdata('error_message', "Invalid Link!");
			}
		} else {
			$this->session->set_flashdata('error_message', "Invalid Link!");
		}

		redirect(base_url('login'));
	}
}
