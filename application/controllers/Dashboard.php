<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {



	public function __construct() {
		parent::__construct();

		$this->load->model('Common_model','common_model');
		$this->load->model('Offers_model');

		define('APP_CURRENCY', "USD");
		define('CURRENCY_CODE', "US $");

	}

	public function index() {
       
		$this->data['view'] = 'dashboard';


        $objHeaderFooterPages = $this->Pages_model->getHeaderFooterPages();
        $this->data['headerLinks']="";
        $this->data['footerLinks']="";
        //last_query();
        foreach ($objHeaderFooterPages->result() as $row)
        {
            if($row->PageId == '18')
            {//Question: What is Page 18
                $this->data['title18'] = $row->PageTitle;
                $this->data['text18'] = stripslashes($row->PageText);
                $this->data['img18'] = $row->Image;
            }
            else
            {
                $lnk = '';
                if($row->FileName == '')
                    $lnk = "page.php?id=".$row->PageId;
                if($row->SEOURLName != '')
                    $lnk = $row->SEOURLName.'.html';
                else if($row->FileName != '')
                    $lnk = $row->FileName;
                if($row->HeaderLinkCP == '1')
                    $this->data['headerLinks'] .= " <li><a href='$lnk'>".$row->LinkTitle."</a></li>";
                if($row->FooterLinkCP == '1')
                    $this->data['footerLinks'] .= "<li><a href='$lnk'>".$row->LinkTitle."</a></li>";
            }
        }
        $objLiveChat = $this->Util_model->getLiveChat();
        if (isset($objLiveChat->ScriptCode) && $objLiveChat->ScriptCode != '')
        {
            $this->data['LIVE_CHAT'] = stripslashes($objLiveChat->ScriptCode);
        }
        $userReceipts = $this->User_model->getUserReceipts($this->session->userdata('GSM_FUS_UserId'));
        $totalReceipts = 0;
        foreach ($userReceipts->result() as $row) {
            $amount = $this->decrypt($row->Amount);
            if(is_numeric($amount))
            	$totalReceipts = $totalReceipts + $amount;

        }
        $this->data['totalReceipts'] = $totalReceipts;
        $userId = $this->session->userdata('GSM_FUS_UserId');
        $this->data['userLockedCount'] = $this->User_model->getUserLockedCount($userId);

       
        //$this->data['userLockedAmount'] = $this->User_model->getUserLockedAmount($userId);

        $userTotalOrders = 0;
        $totalPendingOrders = 0;
        $totalCompletedOrders = 0;
        $totalRejectedOrders = 0;
        $totalInprocessOrders = 0;

        $userOrdersCount = $this->User_model->getUserImeiOrdersCountByStatus($userId);

       

        foreach ($userOrdersCount->result() as $rowOrderCount)
        {
            $userTotalOrders += $rowOrderCount->OrdersCount;
            if($rowOrderCount->CodeStatusId==1)
            {
                $totalPendingOrders += $rowOrderCount->OrdersCount;
            }
            elseif ($rowOrderCount->CodeStatusId==2)
            {
                $totalCompletedOrders += $rowOrderCount->OrdersCount;
            }
            elseif ($rowOrderCount->CodeStatusId==3)
            {
                $totalRejectedOrders += $rowOrderCount->OrdersCount;
            }
            elseif ($rowOrderCount->CodeStatusId==4)
            {
                $totalInprocessOrders += $rowOrderCount->OrdersCount;
            }

        }

        $userOrdersCount = $this->User_model->getUserFileServiceOrdersCountByStatus($userId);

        foreach ($userOrdersCount->result() as $rowOrderCount)
        {
            $userTotalOrders += $rowOrderCount->OrdersCount;
            if($rowOrderCount->CodeStatusId==1)
            {
                $totalPendingOrders += $rowOrderCount->OrdersCount;
            }
            elseif ($rowOrderCount->CodeStatusId==2)
            {
                $totalCompletedOrders += $rowOrderCount->OrdersCount;
            }
            elseif ($rowOrderCount->CodeStatusId==3)
            {
                $totalRejectedOrders += $rowOrderCount->OrdersCount;
            }
            elseif ($rowOrderCount->CodeStatusId==4)
            {
                $totalInprocessOrders += $rowOrderCount->OrdersCount;
            }

        }

        $userOrdersCount = $this->User_model->getUserServerOrdersCountByStatus($userId);

        foreach ($userOrdersCount->result() as $rowOrderCount)
        {
            $userTotalOrders += $rowOrderCount->OrdersCount;
            if($rowOrderCount->StatusId==1)
            {
                $totalPendingOrders += $rowOrderCount->OrdersCount;
            }
            elseif ($rowOrderCount->StatusId==2)
            {
                $totalCompletedOrders += $rowOrderCount->OrdersCount;
            }
            elseif ($rowOrderCount->StatusId==3)
            {
                $totalRejectedOrders += $rowOrderCount->OrdersCount;
            }
            elseif ($rowOrderCount->StatusId==4)
            {
                $totalInprocessOrders += $rowOrderCount->OrdersCount;
            }

        }
        if($userTotalOrders!==0){
			$pendingPercentage=($totalPendingOrders/$userTotalOrders)*100;
			$completedPercentage=($totalCompletedOrders/$userTotalOrders)*100;
			$rejectedPercentage=($totalRejectedOrders/$userTotalOrders)*100;
			$inprocessPercentage=($totalInprocessOrders/$userTotalOrders)*100;
		}else{
			$pendingPercentage = 0;
			$completedPercentage = 0;
			$rejectedPercentage = 0;
			$inprocessPercentage = 0;
		}
        $this->data['ordersPercentages']['pendingPercentage'] = $pendingPercentage;
        $this->data['ordersPercentages']['completedPercentage'] = $completedPercentage;
        $this->data['ordersPercentages']['rejectedPercentage'] = $rejectedPercentage;
        $this->data['ordersPercentages']['inprocessPercentage'] = $inprocessPercentage;

        $this->data['ordersTotal']['pending'] = $totalPendingOrders;
        $this->data['ordersTotal']['completed'] = $totalCompletedOrders;
        $this->data['ordersTotal']['rejected'] = $totalRejectedOrders;
        $this->data['ordersTotal']['inprocess'] = $totalInprocessOrders;

        if($this->data['userDetails']->CPanel == '1')
        {

        }

        $this->data['latestFiveImeiOrders'] = $this->User_model->getLatestFiveImeiOrders($this->user_id);
        $this->data['latestFiveTickets'] = $this->User_model->getLatestFiveTickets($this->user_id);
        //last_query();
        /*
        echo "<pre>";
        print_r($this->data['settings']);
        exit();
        */
		$this->load->view('layouts/default', $this->data);
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}






	public function calculate_offer($offer_id,$mrp)
	{
		$res=array();
		if($offer_id!=0){
			$offer = $this->Offers_model->single_offer($offer_id);
			$res['selling_price']=round($mrp - (($offer->offer_percentage/100) * $mrp),2);

			$res['you_save']=round($mrp-$res['selling_price'],2);
			$res['you_save_per']=$offer->offer_percentage;
		}
		else{
			$res['selling_price']=$mrp;
			$res['you_save']=0;
			$res['you_save_per']=0;
		}
		return json_encode($res);
	}
	public function get_single_info($ids, $param, $table_nm)
	{
		$data= $this->common_model->selectByids($ids, $table_nm);
		if(!empty($data)){
			return $data[0]->$param;
		}else{
			return '';
		}
	}
	public function _create_thumbnail($path, $thumb_name, $fileName, $width, $height)
	{
		$source_path = $path.$fileName;

		$ext = pathinfo($fileName, PATHINFO_EXTENSION);

		$thumb_name=$thumb_name.'_'.$width.'x'.$height.'.'.$ext;

		$thumb_path=$path.'thumbs/'.$thumb_name;

		if(!file_exists($thumb_path)){
			$this->load->library('image_lib');
			$config['image_library']  = 'gd2';
			$config['source_image']   = $source_path;
			$config['new_image']      = $thumb_path;
			$config['create_thumb']   = FALSE;
			$config['maintain_ratio'] = FALSE;
			$config['width']          = $width;
			$config['height']         = $height;
			$this->image_lib->initialize($config);
			if (! $this->image_lib->resize()) {
				echo $this->image_lib->display_errors();
			}
		}

		return $thumb_path;
	}

}
