<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('CUST_BC_HEADING'); ?>
				(<?php echo $userDetails->CurrencyAbb; ?>)</h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>

					<?php echo form_open(base_url('page/submitpayment'), "name=frm id=frm"); ?>
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="row">
								<div class="col-lg-12 col-sm-12">
									
									<div class="form-group">
										<label class="control-label"><?php echo $this->lang->line('CUST_BC_1'); ?>
											(<?php echo $userDetails->CurrencyAbb; ?>):<span class="required_field">*</span></label>
										<input type="text" class="form-control" required="required"
											   style="font-size:24px; color:#E81516;"
											   value="<?php echo roundMe($minCr); ?>" maxlength="10" name="txtCredits"
											   id="txtCredits"/>
									</div>
									<div class="form-group">
										<label class="control-label">Payment Method<span class="required_field">*</span></label>
										<select class="form-control select2" name="pMethod" id="pMethod" required
												style="width: 100%; color: inherit;">
											<option value="">Please Select Payment Method</option>
											<?php
											$bq = $strHiddenFlds = $details = '';
											$k = 0;
											foreach ($rsPMethods as $row) {
												$fee = $row->Fee;
												if (isset($FEES[$row->PaymentMethodId]) && $FEES[$row->PaymentMethodId] != '' && is_numeric($FEES[$row->PaymentMethodId])) {
													$fee = $FEES[$row->PaymentMethodId];
												}
												$imgName = base_url() . 'assets/images/mobile-payment.png';
												if ($row->PayImage != '') {
													$imgName = "uplds$THEME/" . $row->PayImage;
												}
												$strHiddenFlds .= '<input type="hidden" id="hdFee' . $row->PaymentMethodId . '" name="hdFee' . $row->PaymentMethodId . '" value="' . $fee . '" /><input type="hidden" id="hdVat' . $row->PaymentMethodId . '" name="hdVat' . $row->PaymentMethodId . '" value="' . $row->Vat . '" />'; ?>
												<option imgName="<?php echo $imgName; ?>"
														details2='<?php echo nl2br(stripslashes($row->Description)); ?>'
														value="<?php echo $row->PaymentMethodId; ?>"><?php echo $row->PaymentMethod; ?></option>
												<?php $k++;
											} ?>
										</select>
									</div>
								</div>
								<div class="col-lg-12" id="dvInfo" style="display:none;">
									<div class="card m-b-30 card-info">
										<div class="card-body" style="background-color:#F5F5F5;">
											<blockquote class="card-bodyquote">
												<footer><strong>PAYMENT DETAILS:</strong></footer>
												<p id="dvPDtls"></p>
											</blockquote>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6" id="dvCalculations" style="display:none;">
							<div class="card m-b-20">
								<div class="card-body">
									<div class="row">
										<div class="col-sm-12">
											<table cellspacing="0" cellpadding="10" class="clsBuyCredits table">
												<tbody>
												<tr>
													<td width="65%">Your Current Balance
														(<?php echo $this->data['userDetails']->CurrencySymbol; ?>):
													</td>
													<td width="35%" align="right"><h5 style="color:#44519E;"><span
																class="calculate balance"><?php echo roundMe($this->data['userDetails']->Credits); ?></span> <?php echo $this->data['userDetails']->CurrencyAbb; ?>
														</h5></td>
												</tr>
												<tr>
													<td class="taxname"></td>
													<td align="right"><h5 style="color:#44519E;"><span
																class="calculate tax"></span> <?php echo $this->data['userDetails']->CurrencyAbb; ?>
													</td>
												</tr>
												<tr>
													<td class="vatname"></td>
													<td align="right"><h5 style="color:#E81516;"><span
																class="calculate vat"></span> <?php echo $this->data['userDetails']->CurrencyAbb; ?>
													</td>
												</tr>
												<tr style="display:none;">
													<td><small class="taxname2"></small></td>
													<td align="right"><h5 style="color:#E81516;"><span
																class="calculate tax2"></span> <?php echo $this->data['userDetails']->CurrencyAbb; ?>
													</td>
												</tr>
												<tr>
													<td></td>
													<td align="right">____________________</td>
												</tr>
												<tr>
													<td>Balance will be:</td>
													<td align="right"><h5 style="color:#E81516;"><span
																class="calculate total"></span> <?php echo $this->data['userDetails']->CurrencyAbb; ?>
														</h5></td>
												</tr>
												<tr>
													<td colspan="2" class="text-right">
														<h4 style="color:green;">Total have to pay
															now <?php echo $this->data['userDetails']->CurrencySymbol; ?>
															<span class="total-paynow"
																  id="tdTotalToPay"> 0.0 </span> <?php echo $this->data['userDetails']->CurrencyAbb; ?>
														</h4>
													</td>
												</tr>
												<tr>
													<td colspan="2">
														<div>
															<button type="submit" class="clsBigBtn btn btn-primary"
																	id="btnBuyCredits"
																	name="btnBuyCredits"><?php echo $this->lang->line('CUST_LBL_41'); ?></button>
															<?php echo $strHiddenFlds; ?>
															<input type="hidden" id="currSymbol" name="currSymbol"
																   value="<?php echo $this->data['userDetails']->CurrencySymbol; ?>">
															<input type="hidden" id="myCurrency" name="myCurrency"
																   value="<?php echo $this->data['userDetails']->CurrencyAbb; ?>">
															<input type="hidden" id="amount" name="amount" value="0">
															<input type="hidden" id="grdTotal" name="grdTotal"
																   value="0">
															<input type="hidden" id="clcr" name="clcr"
																   value="<?php echo $this->data['userDetails']->ConversionRate; ?>">
														</div>
													</td>
												</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
			<!--/col-md-3-->
			<!--/col-md-3-->
		</div>
		<!--/row-->
	</div>
</div>

<!--/page-content end-->
<script language="javascript" src="<?php echo base_url('assets/js/functions.js'); ?>"></script>
<script language="javascript" src="<?php echo base_url('assets/js/addcredits.js'); ?>"></script>
