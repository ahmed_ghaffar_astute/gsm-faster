<div role="main" class="main">

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Order Cancelled</h1>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="lead">
                	<?php if($errorMsg != ''){
                        echo $errorMsg;
                    } 
                    else{
                        echo 'Your order has been cancelled!';
                    }    
                    ?>  
                </p>
            </div>
        </div>
    </div>
