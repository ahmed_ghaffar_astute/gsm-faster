<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('CUST_CRP_HEADING'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
								   style="border-collapse: collapse; border-spacing: 0; width: 100%;">
								<thead>
								<tr class="bg-primary">
									<th><?php echo $this->lang->line('CUST_CRP_5'); ?></th>
									<th><?php echo $this->lang->line('CUST_CRP_1'); ?></th>
									<th><?php echo $this->lang->line('CUST_CH_1'); ?></th>
									<th><?php echo $this->lang->line('CUST_LBL_192'); ?></th>
									<th>Fee</th>
									<th><?php echo $this->lang->line('CUST_LBL_246'); ?></th>
								</tr>
								</thead>
								<tbody>
								<? foreach ($rsHistory as $row) {
									if ($row->CreditsLeft != '')
										$creditsLeft = $controller_instanse->decrypt($row->CreditsLeft);
									else
										$creditsLeft = '-';
									$arrDt = explode(' ', $row->HistoryDtTm);
									?>
									<tr>
										<td nowrap="nowrap"><?php echo convertDate($arrDt[0]); ?></td>
										<td><?php echo stripslashes($row->Description); ?></td>
										<td nowrap="nowrap"><?php echo $row->IMEINo == '' ? '-' : $row->IMEINo; ?></td>
										<td nowrap="nowrap"><?php echo $userDetails->CurrencySymbol . $row->Credits; ?></td>
										<td nowrap="nowrap"><?php echo $row->FeePercentage == '' ? '-' : $row->FeePercentage; ?></td>
										<td nowrap="nowrap"><?php echo $userDetails->CurrencySymbol . $creditsLeft; ?></td>
									</tr>
									<?
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
					<!--/col-md-3-->
					<!--/col-md-3-->
				</div>
				<!--/row-->
			</div>
		</div>
	</div>
</div>
