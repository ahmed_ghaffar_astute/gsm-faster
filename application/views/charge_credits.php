<div class="page-content">
    <div class="row">
            <div class="col-md-12">
                <div class="block-web">
                    <?php if($Stripe_CardError){
                        echo $Stripe_CardError ;
                    }else if(Stripe_InvalidRequestError){
                        echo $Stripe_InvalidRequestError ;
                    }else if(Stripe_ApiConnectionError){
                        echo $Stripe_ApiConnectionError;
                    }else if($Stripe_Error){
                        echo $Stripe_Error ;
                    }else if($Exception){
                        echo $Exception;
                    }   
                    ?>
                </div>
            </div>
    </div>
</div>