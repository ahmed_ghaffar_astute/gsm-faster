<div class="widget widget-shop-categories">
  <h3 class="widget-shop-title">My Account</h3>
  	<div class="widget-content">
        <ul class="product-categories my_profile_detail_widget">
        	<li>
                <i class="fa fa-angle-right"></i>
            	<a class="rx-default <?php if(isset($current_page) && strcmp($current_page,'My Account')==0){ echo 'active';} ?>" href="<?=base_url('/my-account')?>">My Profile</a>
            </li>
            <li>
                <i class="fa fa-angle-right"></i>
            	<a class="rx-default <?php if(isset($current_page) && strcmp($current_page,'My Addresses')==0){ echo 'active';} ?>" href="<?=base_url('/my-addresses')?>">Addresses</a>
            </li>
            <li>
                <i class="fa fa-angle-right"></i>
            	<a class="rx-default <?php if(isset($current_page) && strcmp($current_page,'Saved Bank')==0){ echo 'active';} ?>" href="<?=base_url('/saved-bank-accounts')?>">Saved Bank Account</a>
            </li>
            <li>
                <i class="fa fa-angle-right"></i>
            	<a class="rx-default <?php if(isset($current_page) && strcmp($current_page,'My Reviews')==0){ echo 'active';} ?>" href="<?=base_url('/my-reviews')?>">My Reviews & Rating</a>
            </li>
            <li>
                <i class="fa fa-angle-right"></i>
                <a class="rx-default" href="<?=base_url('/my-orders')?>">My Orders</a>
            </li>
        </ul>
	</div>
</div>