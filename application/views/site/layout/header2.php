<?php 
    define('APP_NAME', $this->db->get_where('tbl_web_settings', array('id' => '1'))->row()->site_name);
    define('APP_FAVICON', $this->db->get_where('tbl_web_settings', array('id' => '1'))->row()->web_favicon);
    define('APP_LOGO', $this->db->get_where('tbl_web_settings', array('id' => '1'))->row()->web_logo_2);
    define('APP_LOGO_2', $this->db->get_where('tbl_web_settings', array('id' => '1'))->row()->web_logo_1);
    define('PROFILE_IMG', $this->db->get_where('tbl_admin', array('id' => '1'))->row()->image);

    if($sharing_img!=''){
      $sharing_img=$sharing_img;
      $sharing_wp_img=$sharing_img;
    }
    else{
      $sharing_img=base_url('assets/images/facebook_share_banner.png');
      $sharing_wp_img=base_url('assets/images/wp_share_banner.png');
    }

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="author" content="">
	  <meta name="description" content="">
	  <meta http-equiv="Content-Type"content="text/html;charset=UTF-8"/>
	  <meta name="viewport"content="width=device-width, initial-scale=1.0">
    <title> <?php if(isset($current_page)){ echo $current_page.' | ';} ?><?php echo APP_NAME;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="<?=base_url('assets/images/').APP_FAVICON?>"/>

    <meta property="og:type" content="article" />
    <meta property="og:title" content="<?=$this->db->get_where('tbl_web_settings', array('id' => '1'))->row()->site_name?>" />
    <meta property="og:description" content="<?=$this->db->get_where('tbl_web_settings', array('id' => '1'))->row()->site_description?>" />
    <meta property="og:image" itemprop="image" content="<?=$sharing_wp_img?>" />
    <meta property="og:url" content="<?=current_url()?>" />
    <meta property="og:image:width" content="1024" />
    <meta property="og:image:height" content="1024" />
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:image" content="<?=$sharing_img?>">
    <link rel="image_src" href="<?=$sharing_wp_img?>">


    <link rel="stylesheet" href="<?=base_url('assets/site_assets/css/ionicons.min.css');?>">
    <link rel="stylesheet" href="<?=base_url('assets/site_assets/css/font-awesome.min.css')?>">

    <link rel="stylesheet" href="<?=base_url('assets/site_assets/css/animate.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/site_assets/css/jquery-ui.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/site_assets/css/meanmenu.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/site_assets/css/normalize.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/site_assets/css/nivo-slider.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/site_assets/css/owl.carousel.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/site_assets/css/slick.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/site_assets/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/site_assets/css/default.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/site_assets/css/style.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/site_assets/css/cust_style.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/site_assets/css/responsive.css')?>">
    <script src="<?=base_url('assets/site_assets/js/vendor/modernizr-2.8.3.min.js')?>"></script>

    <script src="<?=base_url('assets/site_assets/js/vendor/jquery-1.12.4.min.js')?>"></script>

    <script src="<?=base_url('assets/site_assets/js/notify.min.js')?>"></script>

    <!-- Sweetalert popup -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/sweetalert/sweetalert.css')?>">
    <script type="text/javascript" src="<?=base_url('assets/sweetalert/sweetalert.min.js')?>"></script>
	
	 <!-- Google Fonts -->
	 <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
	
    <script src="<?=base_url('assets/site_assets/js/bootstrap.min.js')?>"></script>
    <script src="<?=base_url('assets/site_assets/js/cust_javascript.js')?>"></script>

    <script type="text/javascript">
      var Settings = {
        base_url: '<?= base_url() ?>',
        currency_code: '<?= CURRENCY_CODE ?>'
      }
    </script>

  </head>

<!-- <body oncontextmenu="return false"> -->
<body>
<div class="wrapper"> 
  <div class="se-pre-con"></div>
  <div class="process_loader"></div>
  <header>
    <div class="header-container"> 
      <div class="header-middel-area">
        <div class="container">
          <div class="row"> 
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="logo"> <a href="<?=base_url('/')?>"><img src="<?=base_url('assets/images/').APP_LOGO?>" alt=""></a> </div>
            </div>
            <div class="col-md-7 col-sm-6 col-xs-12">
              <div class="search-box-area">
                <form accept-charset="utf-8" action="<?=base_url('search-result')?>" id="search_form" method="get">
                  <div class="search-box" style="width: 100%">
                    <input type="text" name="keyword" id="search" placeholder="Search for Products..." value="<?=$this->input->get('keyword')!='' ? $this->input->get('keyword') : ''?>" required="">
                    <button type="submit"><i class="ion-ios-search-strong"></i></button>
                  </div>
                </form>
              </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-12">
              <div class="mini-cart-area">
                <ul>
                  <?php 
                    $ci =& get_instance();
                  ?>
                   <li>
                    <?php 
                      if(!check_user_login()){
                    ?>
                    <a href="javascript:void(0)">
                      <i class="ion-android-person"></i>
                    </a>
                    <?php 
                    }
                    else{

                      $user_img=$this->db->get_where('tbl_users', array('id' => $this->session->userdata('GSM_FUS_UserId')))->row()->user_image;

                      if($user_img=='' OR !file_exists('assets/images/users/'.$user_img)){
                        $user_img=base_url('assets/images/photo.jpg');
                      }
                      else{
                        $user_img=base_url('assets/images/users/'.$user_img);
                      }


                      ?>
                      <a href="javascript:void(0)" style="background-image: url('<?=$user_img?>');background-size: cover;">
                      </a>
                      <?php
                    }
                    ?>
                    <ul class="cart-dropdown user_login">
                      <?php 
                        if(!check_user_login()){
                      ?>
                      <li class="cart-button"> <a href="<?php echo site_url('login-register'); ?>" class="button2">Login &amp; Register</a>
                      </li>
                      <?php }else{ ?>
                      <li class="cart-item"><a href="<?php echo site_url('my-account'); ?>"><i class="ion-android-person"></i> My Account</a></li>
                      <li class="cart-item"><a href="<?php echo site_url('my-orders'); ?>"><i class="ion-bag"></i> My Orders</a></li>
                      <li class="cart-item"><a href="<?php echo site_url('my-cart'); ?>"><i class="ion-ios-cart-outline"></i> Shopping Cart</a></li>
                      <li class="cart-item"><a href="<?php echo site_url('wishlist'); ?>"><i class="ion-ios-list-outline"></i> Wishlist</a></li>
                      <li class="cart-item"><a href="<?=site_url('site/logout')?>" onclick="return confirm('Are you sure to logout?')"><i class="ion-log-out"></i> Logout</a></li>
                      <?php } ?> 
                    </ul>
                  </li>
                  <li style="<?php if(!check_user_login() OR empty($ci->get_cart())){ echo 'pointer-events: none;';}?>">
                    <a href="javascript:void(0)">
                      <i class="ion-android-cart"></i>
                      <span class="cart-add"><?=count($ci->get_cart())?></span>
                    </a>
                    <ul class="cart-dropdown">
                      <?php 

                        if(check_user_login()){

                          $row=$ci->get_cart(3);
                          if(!empty($row))
                          {
                          foreach ($row as $key => $value) {

                            // for offers
                            $data_ofr=$ci->calculate_offer($ci->get_single_info(array('id' => $value->product_id),'offer_id','tbl_product'),$value->product_mrp);

                            $arr_ofr=json_decode($data_ofr);

                            $img_file=$ci->_create_thumbnail('assets/images/products/',$ci->get_single_info(array('id' => $value->product_id),'product_slug','tbl_product'),$value->featured_image,50,50);


                        ?>
                        <li class="cart-item">
                          <div class="cart-img" style="width: auto"> <a href=""><img src="<?=base_url().$img_file?>" alt="" style="width: 68px;height: 68px"></a> </div>
                          <div class="cart-content">
                            <h4>
                              <a href="<?php echo site_url('product/'.$ci->get_single_info(array('id' => $value->product_id),'product_slug','tbl_product')); ?>" title="<?=$ci->get_single_info(array('id' => $value->product_id),'product_title','tbl_product')?>">
                                <?php 
                                  if(strlen($value->product_title) > 20){
                                    echo substr(stripslashes($value->product_title), 0, 20).'...';  
                                  }else{
                                    echo $value->product_title;
                                  }
                                ?>
                              </a>
                            </h4>
                            <p class="cart-quantity">Qty: <?=$value->product_qty?></p>
                            <p class="cart-price"><?=CURRENCY_CODE.' '.$arr_ofr->selling_price*$value->product_qty?></p>
                          </div>
                          <div class="cart-close"> <a href="<?php echo site_url('remove-to-cart/'.$value->id); ?>" class="btn_remove_cart" title="Remove"><i class="ion-android-close"></i></a> </div>
                        </li>
                        
                        <?php } ?>
                        <?php 
                          if(count($ci->get_cart()) > 3){
                              echo '<li class="cart-item text-center">
                                      <h4 style="font-weight: 500">You have '.(count($ci->get_cart())-3).' more items your cart..</h4>
                                    </li>';
                          }
                        ?>
                        
                        <li class="cart-button"> <a href="<?php echo site_url('my-cart'); ?>" class="button2">View cart</a> <a href="<?php echo site_url('checkout'); ?>" class="button2">Check out</a> 
                        </li>
                        <?php 
                        }
                        else{
                          ?>
                          <li class="cart-item text-center">
                            <h4 style="font-weight: 500">Cart is empty !!!</h4>
                          </li>
                          <?php
                          }
                        }   // end of session check
                        else{
                          ?>
                          <li class="cart-item text-center">
                            <h4 style="font-weight: 500">You have not login now !!!</h4>
                          </li>
                          <?php
                        }
                      ?>

                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="header-bottom-area header-sticky">
        <div class="container">
          <div class="row">
            <div class="col-md-12"> 
              <div class="logo-sticky"> 
                <a href="<?=base_url('/')?>">
                  <img src="<?=base_url('assets/images/').APP_LOGO?>" alt="">
                </a> 
              </div>
              <div class="main-menu-area">
                <nav>
                  <ul class="main-menu">
                    <li <?php if(isset($current_page) && $current_page=='Home'){ echo 'class="active"';} ?>><a href="<?=base_url('/')?>">Home</a></li>
                    <li <?php if(isset($current_page) && $current_page=='Categories'){ echo 'class="active"';} ?>><a href="<?=base_url('/category')?>">Categories</a></li>
                    <li <?php if(isset($current_page) && $current_page=='Offers'){ echo 'class="active"';} ?>><a href="<?=base_url('/offers')?>">Offers</a></li>
                    <li <?php if(isset($current_page) && $current_page=="Today's Deals"){ echo 'class="active"';} ?>><a href="<?=base_url('/todays-deals')?>">Today's Deals</a></li>
                    <li <?php if(isset($current_page) && $current_page=='Contact Us'){ echo 'class="active"';} ?>><a href="<?php echo site_url('contact-us'); ?>">Contact Us</a></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="mobile-menu-area hidden-sm hidden-md hidden-lg">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="mobile-menu">
                <nav>
                  <ul>
                    <li <?php if(isset($current_page) && $current_page=='Home'){ echo 'class="active"';} ?>><a href="<?=base_url('/')?>">Home</a></li>
                    <li <?php if(isset($current_page) && $current_page=='Categories'){ echo 'class="active"';} ?>><a href="<?=base_url('/category')?>">Categories</a></li>
                    <li <?php if(isset($current_page) && $current_page=='Offers'){ echo 'class="active"';} ?>><a href="<?=base_url('/offers')?>">Offers</a></li>
                    <li <?php if(isset($current_page) && $current_page=='Shop'){ echo 'class="active"';} ?>><a href="<?=base_url('/todays-deals')?>">Today's Deals</a></li>
                    <li <?php if(isset($current_page) && $current_page=='Contact Us'){ echo 'class="active"';} ?>><a href="<?php echo site_url('contact-us'); ?>">Contact Us</a></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
