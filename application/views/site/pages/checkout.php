<?php 
  $this->load->view('site/layout/breadcrumb'); 

  $ci =& get_instance();

?>

<!--<script src="<?=base_url('assets/site_assets/js/cities.js')?>"></script>-->

<div class="checkout-area mt-20">
<div class="container">
  <div class="row"> 
    <div class="col-md-7">
      <div class="checkout-form-area mt-25">
        <div class="checkout-title">
          <h3>Billing details</h3>
        </div>
        <div class="address_details_block">
          <?php 
              $order_address_id=0;
              foreach ($addresses as $key => $value) {

                if($value->is_default=='true'){
                  $order_address_id=$value->id;
                }

            ?>
          <div class="address_details_item">
            <label class="container">
              <input type="radio" name="radio" class="address_radio" value="<?=$value->id?>" <?php echo $value->is_default=='true' ? 'checked="checked"' : ''; ?>>
              <span class="checkmark"></span>
            </label>
            
            <div class="address_list">
              <span><?=$value->name?> <?=$value->mobile_no?></span>
              <div class="address_list_edit">
                <!-- <a href="#">Edit</a> -->
                <a href="" class="btn_delete_address" data-id="<?=$value->id?>">Delete</a>
              </div>
              <p>
                <?=$value->building_name.', '.$value->road_area_colony.', '.$value->city.', '.$value->district.', '.$value->state.' - '.$value->pincode;?>
              </p>
            </div>
            
          </div>
          <?php } ?>
          <div class="address_details_item">
            <a href="" class="btn_new_address" style="font-size: 16px">
              <div class="address_list" style="padding: 15px 5px">
                <i class="fa fa-plus"></i> Add New Address
              </div>
            </a>
          </div>

          <div class="ceckout-form add_addresss_block" style="background: #eee;padding: 20px;<?php if(empty($addresses)){ echo 'display: block';}else{  echo 'display: none'; } ?>" >
				<?php echo form_open(base_url('site/addAddress') , "name=address_form id=address_form"); ?>
              <div class="billing-fields">

                <div class="row">
                  <div class="col-md-6">
                    <p>
                      <label>Name<span class="required">*</span></label>
                    </p>
                    <input type="text" placeholder="Enter name" name="billing_name" value="" required="">
                  </div>
                  <div class="col-md-6">
                    <p>
                      <label>Mobile no.<span class="required">*</span></label>
                    </p>
                    <input type="text" placeholder="Enter Mobile no." name="billing_mobile_no" value="" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="10" required="">
                  </div>
                  <div class="col-md-12">
                    <p>
                      <label>Email<span class="required">*</span></label>
                    </p>
                    <input type="email" placeholder="Enter Email" name="billing_email" required="" value="">
                  </div>
                  <div class="col-md-12">
                    <p>
                      <label>Address (House no., Building Name)<span class="required">*</span></label>
                    </p>
                    <textarea placeholder="House no., Building Name" name="building_name" style="background: #fff" required=""></textarea>
                  </div>
                  <div class="col-md-12">
                    <p>
                      <label>Road, Area, Colony<span class="required">*</span></label>
                    </p>
                    <input type="text" placeholder="Enter road, area, colony" name="road_area_colony" value="" required="">
                  </div>
                  <div class="col-md-12">
                    <p>
                      <label>Landmark (Optional)</label>
                    </p>
                    <input type="text" placeholder="Enter landmark (optional)" name="landmark" value="">
                  </div>
                  <div class="col-md-6">
                    <p>
                      <label>Pincode/Zipcode<span class="required">*</span></label>
                    </p>
                    <input type="text" placeholder="Enter pincode/zipcode" name="pincode" value="" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="7" required="">
                  </div>
                  <div class="col-md-6">
                    <p>
                      <label>City<span class="required">*</span></label>
                    </p>
                    <input type="text" placeholder="Enter city" name="city" value="" required="">
                  </div>
                  <div class="col-md-6">
                    <p>
                      <label>District<span class="required">*</span></label>
                    </p>
                    <input type="text" placeholder="Enter district" name="district" value="" required="">
                  </div>
                  <div class="col-md-6">
                    <p>
                      <label>State<span class="required">*</span></label>
                    </p>
						<input type="text" placeholder="Enter State" name="state" value="" required="">
                  </div>
                  <div class="col-md-12">
                    <p>
                      <label>Address Type<span class="required">*</span></label>
                    </p>
                      <div class="clearfix"></div>
                      <label class="radio-inline">
                        <input type="radio" name="address_type" value="Home Address" readonly="" style="width: 20px;height: 15px" checked>Home (All day delivery)
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="address_type" readonly="" value="Office Address" style="width: 20px;height: 15px">Office (Delivery between 10 AM - 5 PM)
                      </label>
                  </div>
                </div>
                <br/>
                
                <div class="form-fild">
                  <div class="add-to-link">
                    <button class="form-button" type="submit" data-text="save">Save</button>
                    <button class="form-button close_form" type="button">close</button>
                  </div>
                </div>
              </div>               
            <?php echo form_close(); ?>
          </div>

        </div>

      </div>
    </div>
	<div class="col-md-5">
		<div class="your-order-fields mt-25">
		  <div class="your-order-title">
			<h3>Your Order</h3>
		  </div>
		  <div class="your-order-table table-responsive">
			<table>
			  <thead>
				<tr>
				  <th class="product-name">Product</th>
				  <th class="product-total">Total</th>
				</tr>
			  </thead>
			  <tbody>
          <?php 

            $total_cart_amt=$delivery_charge=0;

            $cart_ids='';

            foreach ($my_cart as $key => $value) {

              $cart_ids.=$value->id.',';

          ?>
  				<tr class="cart_item">
  				  <td class="product-name" style="width: 70%">
              <a href="<?php echo site_url('remove-to-cart/'.$value->id); ?>" class="btn_remove_cart" style="font-size: 16px;color: red" title="Remove product !">
                <strong>&times;</strong>
              </a>
              <?php 
                if(strlen($value->product_title) > 25){
                  echo substr(stripslashes($value->product_title), 0, 25).'...';  
                }else{
                  echo $value->product_title;
                }
              ?>
              <strong class="product-quantity"> ×<?=$value->product_qty?></strong></td>
  				  <td class="product-total">
              <span class="amount">
                <?php 
                  echo CURRENCY_CODE.' '.$value->selling_price;
                ?>
              </span>
            </td>
  				</tr>
          <?php 
            $total_cart_amt+=$value->selling_price*$value->product_qty;
            $delivery_charge+=$value->delivery_charge;
          }

          $cart_ids=rtrim($cart_ids,',');

          $total_cart_amt+=$delivery_charge;
          ?>
			  </tbody>
			  <tfoot>
				<tr class="cart-subtotal">
				  <th>Subtotal</th>
				  <td><span class="amount"><?=CURRENCY_CODE.' '.($total_cart_amt-$delivery_charge);?></span></td>
				</tr>
				<tr class="shipping">
				  <th>Delivery Charge</th>
				  <td data-title="Delivery Charge"><p><?=($delivery_charge!=0)?'+ '.CURRENCY_CODE.$delivery_charge:'Free';?></p></td>
				</tr>
				<tr class="order-total">
				  <th>Total</th>
				  <td><strong><span class="total-amount"><?=CURRENCY_CODE.' '.$total_cart_amt;?></span></strong></td>
				</tr>
				<tr class="apply_msg" style="display: none">
				  <td colspan="2">
					<h4 class="text-center msg_2" style="font-weight: 500;color: green;margin-bottom: 15px;"></h4>
				  </td>
				</tr>
				<tr class="apply_button">
				  <td colspan="2">
					<a href="" data-toggle="modal" data-target="#coupons_detail">
					  <img src="<?=base_url('assets/images/coupon-icon.png')?>" style="width: 30px;height: 30px">
					  Apply Available Coupon Code!
					</a>
				  </td>
				</tr>
				<tr class="remove_coupon" style="display: none;">
				  <td colspan="2">
					<a href="" style="color: red">
					  &times; Remove Coupon Code!
					</a>
				  </td>
				</tr>
			  </tfoot>
			</table>
		  </div>
		</div>
		<div class="checkout-payment mb-50">
      <div class="your-order-title">
        <h3>Payment</h3>
      </div>

			<form method="POST" name="place_order">
        <input type="hidden" name="coupon_id" value="0">
        <input type="hidden" name="order_address" value="<?=$order_address_id?>">
        <input type="hidden" name="total_amt" value="<?=$total_cart_amt-$delivery_charge?>">
        <input type="hidden" name="discount" value="0">
        <input type="hidden" name="discount_amt" value="0">
        <input type="hidden" name="payable_amt" value="<?=$total_cart_amt?>">
        <input type="hidden" name="cart_ids" value="<?=$cart_ids?>">
        <input type="hidden" name="delivery_charge" value="<?=$delivery_charge?>">
  		  <ul>
			<li class="payment_method">
			  <input id="payment_method_cod" class="input-radio" name="payment_method" checked="checked" value="cod" type="radio">
			  <label for="payment_method_cod">Cash on Delivery</label>
			  <div class="pay-box payment_method_cod">
				<div class="col-md-12">
				  <div class="col-md-3 col-sm-2" style="padding:0 8px">
					<label style="margin-top: 10px;letter-spacing: 2px"><span class="_lblnum1"><?=rand(0,10)?></span> + <span class="_lblnum2"><?=rand(5,10)?></span> = </label>
				  </div>
				  <div class="col-md-4 col-sm-4 col-xs-12" style="padding:0 8px 0 0">
					<input type="text" name="" class="form-control input_txt">
				  </div>    
				  <div class="col-md-4 col-sm-6"></div>
				</div>            
			  </div>
			</li>
       <?php

	   //payment settings
        if(1==1)// && $this->db->get_where('tbl_settings', array('id' => '1'))->row()->paypal_status!='false' AND $this->db->get_where('tbl_settings', array('id' => '1'))->row()->paypal_client_id!='' AND $this->db->get_where('tbl_settings', array('id' => '1'))->row()->paypal_secret_key!='')
        {
      ?>
			<li class="payment_method">
			  <input id="payment_method_paypal" class="input-rado" name="payment_method" value="paypal" data-order_button_text="Proceed to PayPal" type="radio">
			  <label for="payment_method_paypal"> PayPal <a href="#"><img src="<?=base_url('assets/site_assets/img/payment/payment2.png')?>" alt="" style="max-width: 50%"/></a></label>
			  <div class="pay-box payment_method_paypal">
				  <p>Pay via PayPal; you can pay with your credit card if you don’t have a PayPal account.</p>
			  </div>
			</li>
      <?php } ?>
      <?php 
        if(1==1) //$this->db->get_where('tbl_settings', array('id' => '1'))->row()->stripe_status!='false' AND $this->db->get_where('tbl_settings', array('id' => '1'))->row()->stripe_key!='' AND $this->db->get_where('tbl_settings', array('id' => '1'))->row()->stripe_secret!='')
        {
      ?>
			<li class="payment_method">
				<input id="payment_method_stripe" class="input-rado" name="payment_method" value="stripe" data-order_button_text="Proceed to Stripe" type="radio">
				<label for="payment_method_stripe"> Stripe <a href="#"><img src="<?=base_url('assets/site_assets/img/payment/stripe-payment-icon.png')?>" alt="" style="max-width: 50%"/></a></label>            
				<div class="pay-box payment_method_stripe">
				  <p>Pay via Stripe; you can pay with your credit card if you don’t have a Stripe account.</p>
				</div>
			</li>
      <?php } ?>
          <button class="order-btn btn_place_order" type="submit">Place Order</button>
  		  </ul>
			</form>
		</div>
	</div>
  </div>
</div>
</div>


<div id="coupons_detail" class="modal fade" role="dialog" style="z-index: 9999999;background: rgba(0,0,0,0.8);">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
        <div class="modal-body">
          <div class="modal-details">
            <div class="row"> 
              <div class="col-md-12 col-sm-12">
                <div class="product-info">
                  <h3>Available Coupons</h3>
                  <br/>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <td class="text-center"><b>Code</b></td>
                          <td class="text-center"><b>Maximum Discount (Amount)</b></td>
                          <td class="text-center">Apply</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                          $row_coupon=$ci->get_coupons();
                          $total_amt=$total_cart_amt-$delivery_charge;
                          foreach ($row_coupon as $key => $value) {

                            if($value->cart_status=='true'){
                              if($value->coupon_cart_min > $total_amt){
                                continue;
                              }
                            }
                        ?>
                        <tr>
                          <td class="text-center">
                            <?=$value->coupon_code?>
                          </td>
                          <td class="text-center">
                            <?=CURRENCY_CODE.' '.$value->coupon_max_amt?>
                          </td>
                          <td class="text-center">
                            <a href="javascript:void(0)" data-coupon="<?=$value->coupon_code?>" data-cartamt="<?=$total_amt?>" data-delivery="<?=$delivery_charge?>" class="btn btn-primary btn-sm btn_apply_coupon" style="border-radius: 3px">Apply</a>
                          </td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<div id="stripeModal" class="modal" style="z-index: 9999999;background: rgba(0,0,0,0.8);">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" style="margin:15px 0px 0px 20px;font-weight:600;font-size:20px">Payment Details</h2> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">

            <form role="form" action="<?=base_url()?>stripe/stripePost" method="post" class="require-validation" data-cc-on-file="false" data-stripe-publishable-key="123<?php //if($this->db->get_where('tbl_settings', array('id' => '1'))->row()->stripe_key!=''){ echo $this->db->get_where('tbl_settings', array('id' => '1'))->row()->stripe_key; } ?>" id="stripe-form">
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
				<input type="hidden" name="coupon_id" value="0">
              <input type="hidden" name="order_address" value="<?=$order_address_id?>">
              <input type="hidden" name="total_amt" value="<?=$total_cart_amt-$delivery_charge?>">
              <input type="hidden" name="discount" value="0">
              <input type="hidden" name="discount_amt" value="0">
              <input type="hidden" name="payable_amt" value="<?=$total_cart_amt?>">
              <input type="hidden" name="cart_ids" value="<?=$cart_ids?>">
              <input type="hidden" name="delivery_charge" value="<?=$delivery_charge?>">
              <input type="hidden" name="payment_method" value="stripe">
                <div class='form-row row'>
                    <div class='col-xs-12 form-group required'>
                        <label class='control-label'>Name on Card</label> 
                        <input class='form-control' name="card_name" size='4' type='text' value="<?=$this->session->userdata('user_name')?>">
                    </div>
                </div>

                <div class='form-row row'>
                    <div class='col-xs-12 form-group card required'>
                        <label class='control-label'>Card Number</label> 
                        <input autocomplete='off' name="card_no" class='form-control card-number' size='20' type='text' value="4242424242424242">
                    </div>
                </div>

                <div class='form-row row'>
                    <div class='col-xs-12 col-md-4 form-group cvc required'>
                        <label class='control-label'>CVC</label> 
                        <input autocomplete='off' name="cvvNumber" class='form-control card-cvc' placeholder='ex. 311' size='4' value="123" type='text'>
                    </div>
                    <div class='col-xs-12 col-md-4 form-group expiration required'>
                        <label class='control-label'>Expiration Month</label> 
                        <input class='form-control card-expiry-month' name="ccExpiryMonth" placeholder='MM' size='2' type='text' value="12">
                    </div>
                    <div class='col-xs-12 col-md-4 form-group expiration required'>
                        <label class='control-label'>Expiration Year</label> 
                        <input class='form-control card-expiry-year' name="ccExpiryYear" placeholder='YYYY' size='4' type='text' value="2020">
                    </div>
                </div>

                <div class='form-row row'>
                    <div class='col-md-12 error form-group hide'>
                        <div class='alert-danger alert'>Please correct the errors and try
                            again.</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button class="order-btn" type="submit">Place Order</button>
                    </div>
                </div>
                     
            </form>
      </div>
    </div>
  </div>
</div>


<div id="orderConfirm" class="modal" style="z-index: 9999999;background: rgba(0,0,0,0.8);text-align:center;">
  <div class="modal-dialog modal-confirm" style="width: fit-content">
    <div class="modal-content">
      <div class="modal-header">
        <img src="<?=base_url('assets/images/success-icon.png')?>" style="width: 70px">
        <h4 class="modal-title">Order is placed!</h4> 
      </div>
      <div class="modal-body" style="padding-top: 0px">
        <p class="text-center" style="font-size: 18px;color: green;font-weight: 600;margin-bottom: 5px">Thank you for order...</p>
        <p style="margin-bottom: 5px;text-align: center;">Your order has been confirmed. Check your email for detials.</p>
        <p style="color: #000;margin-bottom: 5px;font-size: 16px;text-align: center;"><strong>Order No:</strong> <span class="ord_no_lbl"></span></p>
      </div>
      <div class="modal-footer" style="text-align: center;">
        <button class="btn btn-danger btn_track">Track Order</button>
        <button class="btn btn-primary btn_orders" onclick="location.href='<?=base_url('my-orders')?>'">My Orders</button>
      </div>
    </div>
  </div>
</div>


<script src="https://js.stripe.com/v2/"></script>

<script type="text/javascript">
  
  // for stripe payment
  var $form = $(".require-validation");
  $('form.require-validation').bind('submit', function(e) {

    $(".process_loader").show();

    e.preventDefault();

    var $form         = $(".require-validation"),
        inputSelector = ['input[type=email]', 'input[type=password]', 'input[type=text]', 'input[type=file]','textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;
        $errorMessage.addClass('hide');
 
        $('.has-error').removeClass('has-error');
        $inputs.each(function(i, el) {
        var $input = $(el);
        if ($input.val() === '') {
          $input.parent().addClass('has-error');
          $errorMessage.removeClass('hide');
          e.preventDefault();
        }
    });
     
    if (!$form.data('cc-on-file')) {
      e.preventDefault();
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));
      Stripe.createToken({
        number: $('.card-number').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('.card-expiry-month').val(),
        exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);
    }
    
    });

  function stripeResponseHandler(status, response) {

    $(".process_loader").show();

      if (response.error) {

        $(".process_loader").hide();

        $('.error')
            .removeClass('hide')
            .find('.alert')
            .text(response.error.message);
      } else {

        var token = response['id'];
        $form.find('input[type=text]').empty();
        // $form.get(0).submit();
        
        $("#stripeModal").modal("hide");
        $.ajax({
          type:'POST',
          url:$form.attr("action"),
          data:$form.serialize(),
          success:function(res){
            var obj = $.parseJSON(res);
            if(obj.success=='1'){
              $(".process_loader").hide();
              $("#orderConfirm .ord_no_lbl").text(obj.order_unique_id);

              $("#orderConfirm .btn_track").click(function(event){
                window.location.href='<?=base_url().'my-orders/'.$data['order_unique_id']?>';
              });

              $("#orderConfirm").fadeIn();
            }
            else{
              if(obj.order_unique_id!=''){
                $(".process_loader").hide();
                $("#orderConfirm .ord_no_lbl").text(obj.order_unique_id);

                $("#orderConfirm .btn_track").click(function(event){
                  window.location.href='<?=base_url().'my-orders/'.$data['order_unique_id']?>';
                });

                $("#orderConfirm").fadeIn();
              }
              else{
                $(".process_loader").hide();
                swal("Something gone wrong!", obj.msg);  
              }
              
            }

          }
        });

      }
  }

</script>

<?php
  if($this->session->flashdata('response_msg')) {
    $message = $this->session->flashdata('response_msg');
    ?>
      <script type="text/javascript">
        var _msg='<?=$message['message']?>';
        var _class='<?=$message['class']?>';

        $('.notifyjs-corner').empty();
        $.notify(
          _msg, 
          { position:"top right",className: _class }
        ); 
      </script>
    <?php
  }
?>

<?php
  if($this->session->flashdata('payment_msg')) {
    $data = $this->session->flashdata('payment_msg');
    ?>
    <script type="text/javascript">
      $("#orderConfirm .ord_no_lbl").text('<?=$data['order_unique_id']?>');

      $("#orderConfirm .btn_track").click(function(e){
        window.location.href='<?=base_url().'my-orders/'.$data['order_unique_id']?>';
      });

      $("#orderConfirm").fadeIn();
    </script>
    <?php
  } 
?>
