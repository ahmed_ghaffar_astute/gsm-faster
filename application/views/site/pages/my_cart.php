<?php 
  
  $this->load->view('site/layout/breadcrumb'); 

  $ci =& get_instance();

  $total_cart_amt=$delivery_charge=0;

?>
<div class="shopping-cart-area mt-20">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <?php 
          $is_items=false;
          if(!empty($my_cart))
          {
            $is_items=true;
        ?>
        <form class="shop-form cartUpdate" method="post" action="<?=site_url('site/update_cart')?>">

          <input type="hidden" name="cart_id" value="">
          <input type="hidden" name="product_qty" value="">

          <div class="wishlist-table table-responsive">
            <table>
              <thead>
                <tr>
                  <th class="product-remove"></th>
                  <th class="product-cart-img"> <span class="nobr">Product Image</span> </th>
                  <th class="product-name"> <span class="nobr">Product Name</span> </th>
                  <th class="product-quantity"> <span class="nobr">Quantity</span> </th>
                  <th class="product-price"> <span class="nobr"> Unit Price </span> </th>
                  <th class="product-total-price"> <span class="nobr"> Total Price </span> </th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  
                  foreach ($my_cart as $key => $value) {


                    $img_file=$ci->_create_thumbnail('assets/images/products/',$ci->get_single_info(array('id' => $value->product_id),'product_slug','tbl_product'),$value->featured_image,50,50);

                ?>
                <tr>
                  <td class="product-remove"><a href="<?php echo site_url('remove-to-cart/'.$value->id); ?>" class="btn_remove_cart">×</a></td>
                  <td class="product-cart-img"><a href="javascript:void(0)"><img src="<?=base_url().$img_file?>" alt="" style="width: 50px;height: 50px"></a></td>
                  <td class="product-name">
                    <a href="<?php echo site_url('product/'.$ci->get_single_info(array('id' => $value->product_id),'product_slug','tbl_product')); ?>">                      
                      <?php 
                        if(strlen($value->product_title) > 30){
                          echo substr(stripslashes($value->product_title), 0, 30).'...';  
                        }else{
                          echo $value->product_title;
                        }
                      ?>
                    </a>
                  </td>
                  <td class="product-quantity">
                    <input type="hidden" name="crt_id" value="<?=$value->id?>">
                    <select class="product_qty">
                      <?php 
                        for ($i=1; $i <= $value->max_unit_buy; $i++) { 
                      ?>
                      <option value="<?=$i?>" <?php if($i==$value->product_qty){ echo 'selected';} ?>><?=$i?></option>
                      <?php } ?>
                    </select>
                  </td>
                  <td class="product-price">
                    <span>

                      <?php 

                        $actual_price='';

                        if($value->you_save_amt!='0'){
                          ?>
                          <ins><?=CURRENCY_CODE.' '.$actual_price=$value->selling_price?></ins>
                          &nbsp;
                          <del><?=CURRENCY_CODE.' '.$value->product_mrp;?></del>
                          <?php
                        }
                        else{
                          ?>
                          <ins><?=CURRENCY_CODE.' '.$actual_price=$value->product_mrp;?></ins>
                          <?php
                          
                        }
                      ?>
                    </span>
                  </td>
                  <td class="product-total-price"><span><?=CURRENCY_CODE.' '.$actual_price*$value->product_qty?></span></td>
                </tr>
                <?php

                  $total_cart_amt+=$actual_price*$value->product_qty;

                  $delivery_charge+=$value->delivery_charge;

                  } 

                  $total_cart_amt+=$delivery_charge;

                ?>
              </tbody>
            </table>
          </div>
        </form>
        <?php 
          }else{
          ?>
          <center style="margin-bottom: 50px;">
            <img src="<?=base_url('assets/images/empty-cart.png')?>" style="opacity: .5">
            <br/>
            <a href="<?=base_url('/')?>"><img src="<?=base_url('assets/images/continue-shopping-button.png')?>"></a>
          </center>
          <?php
        } ?>
      </div>
    </div>
  </div>
  <?php 
    if($is_items)
    {
  ?>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6">
      </div>
      <div class="col-md-6 col-sm-6">
        <div class="shopping-cart-total">
          <h2>Cart Totals</h2>
          <div class="shop-table table-responsive">
            <table>
              <tbody>
                <tr class="cart-subtotal">
                  <td data-title="Price (<?=count($my_cart)?> items)"><span><?=CURRENCY_CODE.' '.($total_cart_amt-$delivery_charge)?></span></td>
                </tr>
                <tr class="shipping">
                  <td data-title="Delivery Charge"><span><?=($delivery_charge!=0)?'+ '.CURRENCY_CODE.$delivery_charge:'Free';?></span></td>
                </tr>
                <tr class="discount" style="display: none;">
                  <td data-title="Discount"><span><?=($delivery_charge!=0)?'- '.CURRENCY_CODE.$delivery_charge:'Free';?></span></td>
                </tr>
                <tr class="order-total">
                  <td data-title="Payable Amount"><span><strong><span><?=CURRENCY_CODE.' '.$total_cart_amt?></strong></span></td>
                </tr>
              </tbody>
            </table>
          </div>
          <h4 class="text-center msg_2" style="font-weight: 500;color: green;margin-bottom: 15px;"></h4>
          <div class="proceed-to-checkout">

            <?php
              echo form_open('checkout', ['id' => 'frmUsers','method' => 'POST']);
              echo form_input(['type' => 'hidden', 'name' => 'delivery_charge', 'class' => 'form-control', 'value' => ($delivery_charge!=0) ? $delivery_charge:'Free']);
              echo form_input(['type' => 'hidden', 'name' => 'checkout_type', 'class' => 'form-control', 'value' => 'fromCart']);
              ?>
              <button type="submit" class="checkout-button">Proceed to checkout</button>
              <?php
                echo form_close();
              ?>  
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
</div>

<div id="coupons_detail" class="modal fade" role="dialog" style="z-index: 9999999">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          
        </div>
    </div>
  </div>
</div>



<script type="text/javascript">


  function apply_coupon(_coupon_code, _cart_amt, href) {
      $.ajax({
        url:href,
        data: {"coupon_code": _coupon_code, "cart_amt": _cart_amt},
        type:'post',
        success:function(res){

          $(".code_err").hide();

          var obj = $.parseJSON(res); 

          if(obj.success=='1'){
              $("#coupons_detail").modal("hide");
              $('.notifyjs-corner').empty();
              $.notify(
                obj.msg, 
                { position:"top right",className: 'success' }
              ); 

              $(".discount").find("span").html("&#8360;. "+obj.discount_amt);

              $("input[name='discount']").val(obj.discount_amt);

              $("input[name='payable_amt']").val(obj.payable_amt);

              $(".discount").show();

              $(".order-total").find("span").html("&#8360;. "+obj.payable_amt);

              $(".msg_2").text(obj.you_save_msg);

              $("input[name='coupon_code']").val(_coupon_code);
          }
          else{

              $(".code_err").html('<i class="fa fa-exclamation-circle"></i> '+obj.msg).slideDown();
          }

        },
        error : function(res) {
            alert("error");
        }

      });
  }

  $(".get_coupons").click(function(e){
    $("#coupons_detail .modal-body").html($(this).next(".modal-details").html());

    // applying coupon

    $(".btn_apply_coupon").click(function(e){

      e.preventDefault();

      href = '<?=base_url()?>site/apply_coupon';

      var _coupon_code=$(this).data("coupon");
      var _cart_amt=$(this).data("cartamt");

      apply_coupon(_coupon_code, _cart_amt, href);
      
    });

  });

  $(".product_qty").change(function(e){
    $("input[name='cart_id']").val($(this).prev("input").val());
    $("input[name='product_qty']").val($(this).val());
    $(this).parents(".cartUpdate").submit();

  });

  $(".btn_apply_coupon1").click(function(e){

      e.preventDefault();

      var _coupon_code=$("input[name='coupon_code']").val();

      if(_coupon_code!=''){
          href = '<?=base_url()?>site/apply_coupon';
      
          var _cart_amt=$(this).data("cartamt");

          apply_coupon(_coupon_code, _cart_amt, href);
          
      }
      else{
        $("input[name='coupon_code']").focus();
        $(".code_err").slideDown();
      }
  });

  $("input[name='coupon_code']").blur(function(e){
      $(".code_err").hide();
  })


</script>



<?php
  if($this->session->flashdata('cart_msg')) {
    $message = $this->session->flashdata('cart_msg');
    ?>
      <script type="text/javascript">
        var _msg='<?=$message['message']?>';
        var _class='<?=$message['class']?>';

        $('.notifyjs-corner').empty();
        $.notify(
          _msg, 
          { position:"top right",className: _class }
        ); 
      </script>
    <?php
  }
?>