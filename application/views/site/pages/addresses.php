<?php 
  $this->load->view('site/layout/breadcrumb'); 
  $ci =& get_instance();

  // print_r($addresses);

?>

<div class="product-list-grid-view-area mt-20">
	  <div class="container">
	    <div class="row"> 
			<div class="col-lg-3 col-md-3 mb_40"> 
		        <?php $this->load->view('site/layout/sidebar_my_account'); ?>
			</div>
			<div class="col-lg-9 col-md-9">
				<div class="my_profile_area_detail">
					<div class="checkout-form-area">
						<div class="checkout-title">
						  <h3>Address Details</h3>
						</div>
						<div class="address_details_block">

						  <?php
						  	foreach ($addresses as $key => $value) {
						  ?>
						  <div class="address_details_item">
							<div class="address_list">
							  <div class="home_address"><?=$value->address_type?></div>
							  <span><?=$value->name?> <?=$value->mobile_no?></span>
							  <div class="address_list_edit">
								<a href="javascript:void(0)" class="btn_edit_address" data-stuff='<?php echo htmlentities(json_encode($value)); ?>'>Edit</a>
								<a href="javascript:void(0)" class="btn_delete_address" data-id="<?=$value->id?>">Delete</a>
							  </div>
							  <p>
							  	<?=$value->building_name.', '.$value->road_area_colony.', '.$value->city.', '.$value->district.', '.$value->state.' - '.$value->pincode;?>
							  </p>

							</div>            
						  </div>
						  <?php } ?>
						  <div class="address_details_item">
							<a href="" class="btn_new_address">
							  <div class="address_list" style="padding: 15px 5px">
								<i class="fa fa-plus"></i> Add New Address
							  </div>
							</a>
						  </div>

						  <div class="ceckout-form add_addresss_block" style="<?php if(empty($addresses)){ echo 'display: block';}else{  echo 'display: none'; } ?>" >
							<form action="<?php echo site_url('site/addAddress'); ?>" method="post" name="address_form">
							  <div class="billing-fields">

								<div class="row">
								  <div class="col-md-6">
									<p>
									  <label>Name<span class="required">*</span></label>
									</p>
									<input type="text" placeholder="Enter name" name="billing_name" value="" required="">
								  </div>
								  <div class="col-md-6">
									<p>
									  <label>Mobile no.<span class="required">*</span></label>
									</p>
									<input type="text" placeholder="Enter Mobile no." name="billing_mobile_no" value="" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="10" required="">
								  </div>
								  <div class="col-md-12">
									<p>
									  <label>Email<span class="required">*</span></label>
									</p>
									<input type="email" placeholder="Enter Email" name="billing_email" required="" value="">
								  </div>
								  <div class="col-md-12">
									<p>
									  <label>Address (House no., Building Name)<span class="required">*</span></label>
									</p>
									<textarea placeholder="House no., Building Name" name="building_name" style="background: #fff" required=""></textarea>
								  </div>
								  <div class="col-md-12">
									<p>
									  <label>Road, Area, Colony<span class="required">*</span></label>
									</p>
									<input type="text" placeholder="Enter road, area, colony" name="road_area_colony" value="" required="">
								  </div>
								  <div class="col-md-12">
									<p>
									  <label>Landmark (Optional)</label>
									</p>
									<input type="text" placeholder="Enter landmark (optional)" name="landmark" value="">
								  </div>
								  <div class="col-md-6">
									<p>
									  <label>Pincode/Zipcode<span class="required">*</span></label>
									</p>
									<input type="text" placeholder="Enter pincode/zipcode" name="pincode" value="" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="7" required="">
								  </div>
								  <div class="col-md-6">
									<p>
									  <label>City<span class="required">*</span></label>
									</p>
									<input type="text" placeholder="Enter city" name="city" value="" required="">
								  </div>
								  <div class="col-md-6">
									<p>
									  <label>District<span class="required">*</span></label>
									</p>
									<input type="text" placeholder="Enter district" name="district" value="" required="">
								  </div>
								  <div class="col-md-6">
									<p>
									  <label>State<span class="required">*</span></label>
									</p>
									<select name="state" id="state" data-placeholder="Choose state...." tabindex="-1" style="background: #fff;height: 46px;" required="">
									  <option value="0">Select state</option>
									  <?php 
										$state_arr = array("Andaman & Nicobar", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chandigarh", "Chhattisgarh", "Dadra & Nagar Haveli", "Daman & Diu", "Delhi", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu & Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Orissa", "Pondicherry", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Tripura", "Uttar Pradesh", "Uttaranchal", "West Bengal");
									  ?>
									  <?php 
										foreach ($state_arr as $key => $value) {
										  ?>
										  <option value="<?=$value?>"><?=$value?></option>
										  <?php
										}
									  ?>
									</select>
								  </div>
								  <div class="col-md-12">
									<p>
									  <label>Address Type<span class="required">*</span></label>
									</p>
									  <div class="clearfix"></div>
									  <label class="radio-inline">
										<input type="radio" name="address_type" value="Home Address" readonly="" style="width: 20px;height: 15px" checked>Home (All day delivery)
									  </label>
									  <label class="radio-inline">
										<input type="radio" name="address_type" readonly="" value="Office Address" style="width: 20px;height: 15px">Office (Delivery between 10 AM - 5 PM)
									  </label>
								  </div>
								</div>
								<br/>
								
								<div class="form-fild">
								  <div class="add-to-link">
									<button class="form-button" type="submit" data-text="save">Save</button>
									<button class="form-button close_form" type="button">close</button>
								  </div>
								</div>
							  </div>               
							</form>
						  </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="edit_address" class="modal fade" role="dialog" style="z-index: 99999">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="modal-details">
					<div style="background: none;border:none;">
						<form action="" method="post" id="edit_address_form">
							
						  <input type="hidden" name="address_id">

						  <div class="billing-fields">

							<div class="row">
							  <div class="col-md-6">
								<p>
								  <label>Name<span class="required">*</span></label>
								</p>
								<input type="text" placeholder="Enter name" name="billing_name" value="" required="">
							  </div>
							  <div class="col-md-6">
								<p>
								  <label>Mobile no.<span class="required">*</span></label>
								</p>
								<input type="text" placeholder="Enter Mobile no." name="billing_mobile_no" value="" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="10" required="">
							  </div>
							  <div class="col-md-12">
								<p>
								  <label>Email<span class="required">*</span></label>
								</p>
								<input type="email" placeholder="Enter Email" name="billing_email" required="" value="">
							  </div>
							  <div class="col-md-12">
								<p>
								  <label>Address (House no., Building Name)<span class="required">*</span></label>
								</p>
								<textarea placeholder="House no., Building Name" name="building_name" style="background: #fff" required=""></textarea>
							  </div>
							  <div class="col-md-12">
								<p>
								  <label>Road, Area, Colony<span class="required">*</span></label>
								</p>
								<input type="text" placeholder="Enter road, area, colony" name="road_area_colony" value="" required="">
							  </div>
							  <div class="col-md-12">
								<p>
								  <label>Landmark (Optional)</label>
								</p>
								<input type="text" placeholder="Enter landmark (optional)" name="landmark" value="">
							  </div>
							  <div class="col-md-6">
								<p>
								  <label>Pincode/Zipcode<span class="required">*</span></label>
								</p>
								<input type="text" placeholder="Enter pincode/zipcode" name="pincode" value="" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="7" required="">
							  </div>
							  <div class="col-md-6">
								<p>
								  <label>City<span class="required">*</span></label>
								</p>
								<input type="text" placeholder="Enter city" name="city" value="" required="">
							  </div>
							  <div class="col-md-6">
								<p>
								  <label>District<span class="required">*</span></label>
								</p>
								<input type="text" placeholder="Enter district" name="district" value="" required="">
							  </div>
							  <div class="col-md-6">
								<p>
								  <label>State<span class="required">*</span></label>
								</p>
								<select name="state" id="state" data-placeholder="Choose state...." tabindex="-1" style="background: #fff;height: 46px;" required="">
								  <option value="0">Select state</option>
								  <?php 
									$state_arr = array("Andaman & Nicobar", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chandigarh", "Chhattisgarh", "Dadra & Nagar Haveli", "Daman & Diu", "Delhi", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu & Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Orissa", "Pondicherry", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Tripura", "Uttar Pradesh", "Uttaranchal", "West Bengal");
								  ?>
								  <?php 
									foreach ($state_arr as $key => $value) {
									  ?>
									  <option value="<?=$value?>"><?=$value?></option>
									  <?php
									}
								  ?>
								</select>
							  </div>
							  <div class="col-md-12">
								<p>
								  <label>Address Type<span class="required">*</span></label>
								</p>
								  <div class="clearfix"></div>
								  <label class="radio-inline">
									<input type="radio" name="address_type" value="Home Address" readonly="" style="width: 20px;height: 15px" checked>Home (All day delivery)
								  </label>
								  <label class="radio-inline">
									<input type="radio" name="address_type" readonly="" value="Office Address" style="width: 20px;height: 15px">Office (Delivery between 10 AM - 5 PM)
								  </label>
							  </div>
							</div>
							<br/>
							
							<div class="form-fild">
							  <div class="add-to-link">
								<button class="form-button" type="submit" data-text="save">Save</button>
								<button class="form-button" type="button" data-dismiss="modal">close</button>
							  </div>
							</div>
						  </div>               
						</form>
					  </div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(".btn_edit_address").click(function(e){
		var data=$(this).data('stuff');
		//console.log(data);
		$('#edit_address').find("input[name='address_id']").val(data['id']);
		$('#edit_address').find("input[name='billing_name']").val(data['name']);
		$('#edit_address').find("input[name='billing_mobile_no']").val(data['mobile_no']);
		$('#edit_address').find("input[name='billing_email']").val(data['email']);
		$('#edit_address').find("textarea[name='building_name']").val(data['building_name']);
		$('#edit_address').find("input[name='road_area_colony']").val(data['road_area_colony']);

		$('#edit_address').find("input[name='landmark']").val(data['landmark']);
		$('#edit_address').find("input[name='pincode']").val(data['pincode']);
		$('#edit_address').find("input[name='city']").val(data['city']);
		$('#edit_address').find("input[name='district']").val(data['district']);
		$('#edit_address').find('#state option[value="'+data['state']+'"]').prop('selected', true);

		$('#edit_address').find("input[name=address_type][value='"+data['address_type']+"']").prop("checked",true);


		$('#edit_address').modal({
	        backdrop: 'static',
	        keyboard: false
	    })
	});

	$("#edit_address_form").submit(function(e){
		e.preventDefault();

		$(".process_loader").show();

		var formData = new FormData($(this)[0]);

		var href = '<?=base_url()?>site/edit_address';
		
		$.ajax({
	        url: href,
	        processData: false,
	        contentType: false,
	        type: 'POST',
	        data: formData,
	        success: function(data){

	          var obj = $.parseJSON(atob(data));
	          $(".process_loader").hide();
	          $('#edit_address').modal("hide");

	          if(obj.status==1){
          		swal({ title: "Updated!", text: obj.msg, type: "success" }, function(){ location.reload(); });
	          }
	          else{
	          	swal("Something gone wrong!", obj.msg);
	          }

	        }
	    });

	});
</script>

<?php
  if($this->session->flashdata('response_msg')) {
    $message = $this->session->flashdata('response_msg');
    ?>
      <script type="text/javascript">
        var _msg='<?=$message['message']?>';
        var _class='<?=$message['class']?>';

        $('.notifyjs-corner').empty();
        $.notify(
          _msg, 
          { position:"top right",className: _class }
        ); 
      </script>
    <?php
  }
?>
