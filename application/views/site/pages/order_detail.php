<?php 
  $this->load->view('site/layout/breadcrumb'); 

  $ci =& get_instance();

  // print_r($bank_details);

?>
<div class="wishlist-table-area mt-20 mb-50">
	<div class="container">
	  <div class="slingle_product_block row">
		  <div class="col-md-6 col-sm-6 col-xs-12 slingle_item_address_part single_bdr_right bdr_top0">
			<div class="single_address_list">
			  <span class="delivery_address_title">Order Details :-</span>
			  <div class="address_detail_product_item">
				<span>Order ID: <?=$my_order[0]->order_unique_id?> </span>
				<p>Order Date: <?=date("M d, Y",$my_order[0]->order_date)?> </p>
				<?php 
					if($my_order[0]->order_status!=5){

				?>
				<p>Payable Amount: <?=CURRENCY_CODE.' '.$my_order[0]->new_payable_amt?> </p>
				<?php 
                  }
                  $_lbl_class='label-primary';
                  $_lbl_title=$ci->get_status_title($my_order[0]->order_status);

                  switch ($my_order[0]->order_status) {
                      case '1':
                          $_lbl_class='label-default';
                          break;
                      case '2':
                          $_lbl_class='label-primary';
                          break;
                      case '3':
                          $_lbl_class='label-warning';
                          break;

                      case '4':
                          $_lbl_class='label-success';
                          break;
                      
                      default:
                          $_lbl_class='label-danger';
                          break;
                  }

                ?>

				<p>Order Status: <label class="label <?=$_lbl_class?>"><?=$_lbl_title?></label></p>

				<?php 
					echo $my_order[0]->order_status==4 ? '<p>Delivered on: '.date("M jS, y",$my_order[0]->delivery_date).' </p><button type="button" class="form-button btn_download" data-id="'.$my_order[0]->order_unique_id.'">Download Invoice</button>' : '<p>Delivery expected by: '.date("M jS, y",$my_order[0]->delivery_date).' </p>';
				?>
				<?php 
					if($my_order[0]->order_status < 4){
						echo '<a href="" class="form-button cancle_order_btn product_cancel" style="margin-top:10px" data-order="'.$my_order[0]->id.'" data-product="0" data-unique="'.$my_order[0]->order_unique_id.'" data-gateway="'.$ci->get_single_info(array('order_id' => $my_order[0]->id),'gateway','tbl_transaction').'">
							Cancel Order
							</a>';
					}
				?>


			  </div>
			</div>
		  </div>
		  <div class="col-md-6 col-sm-6 col-xs-12 slingle_item_address_part bdr_top0">
			<div class="single_address_list">
			  <span class="delivery_address_title">Delivery Address :-</span>
			  <div class="address_detail_product_item">
				<span><?=$order_address->name?> </span>
				<!-- <p><?=$order_address->email?></p> -->
				<!-- <p class="label label-success"><?=$order_address->address_type?></p> -->
				<div class="product_address">
					<?=$order_address->building_name.', '.$order_address->road_area_colony.', '.$order_address->city.', '.$order_address->district.', '.$order_address->state.' - '.$order_address->pincode;?>
				</div>
				<span class="user_contact">Phone Number : <?=$order_address->mobile_no?></span>
			  </div>
			</div>
		  </div>
		  <div class="clearfix"></div>
		  <?php 
		  	foreach ($my_order as $key => $value) {

		  		$img_file=$ci->_create_thumbnail('assets/images/products/',$ci->get_single_info(array('id' => $value->product_id),'product_slug','tbl_product'),$ci->get_single_info(array('id' => $value->product_id),'featured_image','tbl_product'),200,200);

		  ?>
		  <div class="col-md-12 details_part_product_img slingle_item_address_part">
			  <div class="row">
				<div class="col-md-1 col-sm-2 col-xs-4">
					<div class="product_img_part">
						<a href="<?php echo site_url('product/'.$ci->get_single_info(array('id' => $value->product_id),'product_slug','tbl_product')); ?>" target="_blank"><img src="<?=base_url().$img_file?>" alt=""></a>	
					</div>					
				</div>

				<div class="col-md-5 col-sm-5 col-xs-8">
				  <a href="<?php echo site_url('product/'.$ci->get_single_info(array('id' => $value->product_id),'product_slug','tbl_product')); ?>" target="_blank"><?=$value->product_title?></a>				  
				  <div>Price: <?=CURRENCY_CODE.' '.$value->product_price?></div>
				  <div>Qty: <?=$value->product_qty?></div>
				  <?php 
				  	if($value->product_size!='')
				  	{
				  		echo '<div>Size: '.$value->product_size.'</div>';
				  	}
				  ?>
				</div>

				<?php
					if($value->pro_order_status!='4' && $value->pro_order_status!='5'){
						?>
						<div class="col-md-5 col-sm-5 col-xs-12 col-md-offset-1 text-right">				 							
							<a href="javascript:void(0)" class="form-button pull-right btn-danger product_cancel" data-order="<?=$value->order_id?>" data-product="<?=$value->product_id?>" data-unique="<?=$value->order_unique_id?>" data-gateway="<?=$ci->get_single_info(array('order_id' => $value->order_id),'gateway','tbl_transaction')?>">Cancel</a>
						</div>
						<?php
					}
					else if($value->pro_order_status=='5'){
						$cancelled_on=$ci->get_single_info(array('order_id' => $value->order_id, 'product_id' => $value->product_id),'created_at','tbl_refund');
						?>
						<div class="col-md-6 col-sm-5 col-xs-12">
							<span style="color: red;">This product has been cancelled on <?=date('d-m-Y h:i A',$cancelled_on)?></span>
							<br>
							<strong>Reason:</strong>
							<?php echo '<label style="">'.$ci->get_single_info(array('order_id' => $value->order_id, 'product_id' => $value->product_id),'refund_reason','tbl_refund').'</label>';?>
							<?php 
								if($ci->get_single_info(array('order_id' => $value->order_id, 'product_id' => $value->product_id),'gateway','tbl_refund')!='cod')
								{
									switch ($ci->get_single_info(array('order_id' => $value->order_id, 'product_id' => $value->product_id),'request_status','tbl_refund')) {
									  case '0':
										  $_lbl_title='Pending';
										  $_lbl_class='label-warning';
										  break;
									  case '2':
										  $_lbl_title='Process';
										  $_lbl_class='label-primary';
										  break;
									  case '1':
										  $_lbl_title='Completed';
										  $_lbl_class='label-success';
										  break;
									  case '-1':
										  $_lbl_title='Wating for claim';
										  $_lbl_class='btn-danger';
								  }
								  ?>
									<br/>
									Refund Status: <label class="label <?=$_lbl_class?>"><?=$_lbl_title?></label>
									<?php 
										if($ci->get_single_info(array('order_id' => $value->order_id, 'product_id' => $value->product_id),'request_status','tbl_refund')=='-1')
										{
											echo '<button class="btn btn-warning btn-sm btn_claim" data-order="'.$value->order_id.'" data-product="'.$value->product_id.'"  style="border-radius: 50px;outline: none">Claim Refund</button>';
										}
								}
							?>
						</div>
						<?php
					}
				?>

			  </div>
			  <hr style="margin: 10px 0px">
			  <div class="row">
			  	<?php 
					if($value->pro_order_status!='5')
					{
					?>
						<div class="product_timeline_block" style="box-shadow: none;">
							<section class="cd-horizontal-timeline">
							  	<div class="timeline">
							  	  <?php

							  	  	foreach ($status_titles as $key1 => $value1) {
							  	  		if($value1->id=='5')
							  	  			break;
							  	  ?>
								  <div class="dot <?php if($value1->id<=$value->pro_order_status){ echo 'active_dot';}else{ echo 'deactive_dot'; } ?>" id="<?=$value1->id?>" style="<?php if($value->pro_order_status < $value1->id){ echo 'pointer-events: none;cursor: default;';}?>">
								  	<span></span>
									<date style="width: max-content"><?=$value1->title?></date>
								  </div>
								  <?php } ?>
								  <?php 
								  	if($value->pro_order_status=='4'){
								  		?>
								  		<div class="inside" style="width: 100% !important"></div>
								  		<?php
								  	}
								  	else{
								  		?>
								  		<div class="inside" style="width: <?=(20*$value->pro_order_status+2)?>% !important"></div>
								  		<?php
								  	}
								  ?>
								  
								</div>

								<?php 
									$display_first=true;
									foreach ($status_titles as $key1 => $value1) {

										$where=array('order_id' => $my_order[0]->order_id,'status_title' => $value1->id);
								?>
								<?php 
									if($ci->get_single_info($where,'status_desc','tbl_order_status')!='')
									{
										?>
										<article class="modal <?=$value1->id?>" style="<?php if($value1->id==$value->pro_order_status){ echo 'display: block';}?>">
										  <date><?=date("M jS,y",$ci->get_single_info($where,'created_at','tbl_order_status'))?></date>
										  <h2><?=$value1->title?></h2>
										  <p><?=$ci->get_single_info($where,'status_desc','tbl_order_status')?></p>
										</article>
										<?php
									}
									else{
										?>
										<article class="modal <?=$value1->id?>" style="<?php echo $display_first ? 'display: block' : '';  ?>">
											<h2>Order status is not available...</h2>
										</article>
										<?php
									}
								?>
								
								<?php $display_first=false; } ?>
							</section>
						</div>
					<?php 
					}
					else{
						?>
						<div class="product_timeline_block">
							<section class="cd-horizontal-timeline">
							  	<div class="timeline">
							  	  <?php

							  	  	foreach ($status_titles as $key2 => $value2) {

							  	  		if($value2->id!='5' && $value2->id!='1')
							  	  			continue;
							  	  ?>
								  <div class="dot <?php if($value2->id<=$value->pro_order_status){ echo 'active_dot';}else{ echo 'deactive_dot'; } ?>" id="<?=$value2->id?>">
								  	<span></span>
									<date style="width: max-content"><?=$value2->title?></date>
								  </div>
								  <?php } ?>
								  <div class="inside" style="width: <?=(20*($value->pro_order_status-3))+2?>% !important"></div>
								</div>

								<?php 
									$display_first=true;
									foreach ($status_titles as $key2 => $value2) {

										$where=array('order_id' => $my_order[0]->order_id,'status_title' => $value2->id);

								?>
								<?php 
									if($ci->get_single_info($where,'status_desc','tbl_order_status')!='')
									{
										?>
										<article class="modal <?=$value2->id?>" style="<?php if($value2->id==$value->pro_order_status){ echo 'display: block';}?>">
										  <date><?=date("M jS,y",$ci->get_single_info($where,'created_at','tbl_order_status'))?></date>
										  <h2><?=$value2->title?></h2>
										  <p><?=$ci->get_single_info($where,'status_desc','tbl_order_status')?></p>
										</article>
										<?php
									}
									else{
										?>
										<article class="modal <?=$value2->id?>" style="<?php echo $display_first ? 'display: block' : '';  ?>">
											<h2>Order status is not available...</h2>
										</article>
										<?php
									}
								?>
								
								<?php $display_first=false; } ?>
							</section>
						</div>
						<?php
						}
					?>
			  </div>
			</div>
		  <?php } ?>		  
		</div>
		
	</div>

	<div id="orderCancel" class="modal" style="z-index: 9999999;background: rgba(0,0,0,0.5);overflow-y: auto;">
	  <div class="modal-dialog modal-confirm">
	    <div class="modal-content">
	      <div class="modal-header">
	        <img src="<?=base_url('assets/images/shopping-cancel-512.png')?>" style="width: 70px">
	        <h4 class="modal-title cancelTitle">Are you sure to cancel selected product order ?</h4>
	        <h5>ORDER ID: <span class="order_unique_id">a4s5a4a12</span></h5> 
	      </div>
	      <div class="modal-body" style="padding:0px;padding-top:20px;">
	      	<div class="msg_holder">
	      		
	      	</div>
	      	<form id="">
	      		<input type="hidden" name="order_id" value="">
	      		<input type="hidden" name="product_id" value="">
	      		<input type="hidden" name="gateway" value="">
	      		<div class="row">
	      			<div class="col-md-12">
	      				<div class="form-group">
							<label for="bank_name">Reason <span>*</span>:</label>
	      					<textarea class="form-control" name="reason" rows="4" placeholder="Reason for cancellation"></textarea>	
	      				</div>
	      			</div>
	      			<div class="col-md-12 bank_details" style="display: none">
	      				<div class="address_details_block">
	      					<?php 
	      						foreach ($bank_details as $key => $row_bank) {
	      					?>
		                    <div class="address_details_item">
					            <label class="container">
					              <input type="radio" name="bank_acc_id" class="address_radio" value="<?=$row_bank->id?>" <?php if($row_bank->is_default=='1'){ echo 'checked="checked"';} ?>>
					              <span class="checkmark"></span>
					            </label>
		            
					            <div class="address_list">
					              <span style="margin-bottom: 0px"><?=$row_bank->bank_name?> (Acc No. <?=$row_bank->account_no?>)</span>
					              <p style="margin-bottom: 0px">Account Type: <?=ucfirst($row_bank->account_type)?> | IFSC: <?=$row_bank->bank_ifsc?></p>
					              <p style="margin-bottom: 10px">Holder Name: <?=$row_bank->bank_holder_name?> | Mobile No.: <?=$row_bank->bank_holder_phone?></p>
					            </div>
				          	</div>
				          	<?php } ?>
		                    <div class="address_details_item">
					            <a href="" class="btn_new_account" style="font-size: 16px">
					              <div class="address_list" style="padding: 15px 5px">
					                <i class="fa fa-plus"></i> Add New Account
					              </div>
					            </a>
					        </div>
	        			</div>
	      			</div>
	      		</div>
	      	</form>

	      	<form method="post" accept-charset="utf-8" action="<?php echo site_url('site/add_new_bank'); ?>" class="bank_form" style="display: none;">
	      		<div class="row">
	      			<div class="col-md-12">
	      				<div class="form-group">
							<label for="bank_name">Bank Name <span>*</span>:</label>
							<input type="text" class="form-control" required="required" name="bank_name">
						</div>
	      			</div>
	      			<div class="col-md-12">
	      				<div class="form-group">
							<label for="account_no">Bank Account No. <span>*</span>:</label>
							<input type="text" class="form-control" required="required" name="account_no">
						</div>
	      			</div>
	      			<div class="col-md-6">
	      				<div class="form-group">
							<label for="bank_name">Account Type <span>*</span>:</label>
							<select class="form-control" required="required" name="account_type">
								<option value="saving">Saving</option>
								<option value="current">Current</option>
							</select>
						</div>
	      			</div>
	      			<div class="col-md-6">
	      				<div class="form-group">
							<label for="bank_ifsc">IFSC <span>*</span>:</label>
							<input type="text" class="form-control" required="required" name="bank_ifsc">
						</div>
	      			</div>
	      			<div class="col-md-12">
	      				<div class="form-group">
							<label for="holder_name">Name <span>*</span>:</label>
							<p class="hint_lbl">(Name must match the Name linked to this Bank Account)</p>
							<input type="text" class="form-control" required="required" name="holder_name">
						</div>
	      			</div>
	      			<div class="col-md-12">
	      				<div class="form-group">
							<label for="holder_name" style="margin-bottom: 0px">Mobile Number <span>*</span>:</label>
							<p class="hint_lbl">(Mobile Number must match the Mobile Number linked to this Bank Account)</p>
							<input type="text" class="form-control" required="required" name="holder_mobile">
						</div>
	      			</div>
	      			<div class="col-md-12">
	      				<div class="form-group">
							<label for="holder_email">Email ID <span>*</span>:</label>
							<p class="hint_lbl">(Email must match the Email linked to this Bank Account)</p>
							<input type="text" class="form-control" name="holder_email">
						</div>
	      			</div>
	      			<div class="col-md-12">
	      				<label class="container_checkbox">Set default for Refund Payment
						  <input type="checkbox" checked="checked" name="is_default">
						  <span class="checkmark"></span>
						</label>
	      			</div>
	      			<div class="col-md-12">
	      				<br/>
	      				<div class="form-group">
	      					<button type="submit" class="btn btn-primary">Save</button>
	      					<button type="button" class="btn btn-warning btn_cancel_form">Cancel</button>
	      				</div>
	      			</div>
	      		</div>
	      	</form>
	      </div>
	      <div class="modal-footer">
	        <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
	        <button class="btn btn-primary cancel_order">Cancel Order</button>
	      </div>
	    </div>
	  </div>
	</div>

	<div id="claimRefund" class="modal" style="z-index: 9999999;background: rgba(0,0,0,0.5);overflow-y: auto;">
	  <div class="modal-dialog modal-confirm">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h4 class="modal-title">Choose you refund account</h4>
	      </div>
	      <div class="modal-body" style="padding-top: 0px">
	      	<div class="msg_holder">
	      		
	      	</div>
	      	<form id="">
	      		<input type="hidden" name="order_id" value="">
	      		<input type="hidden" name="product_id" value="">
	      		<div class="row">
	      			<div class="col-md-12 bank_details" style="display: none">
	      				<div class="address_details_block">
	      					<?php 
	      						foreach ($bank_details as $key => $row_bank) {
	      					?>
		                    <div class="address_details_item">
					            <label class="container">
					              <input type="radio" name="bank_acc_id" class="address_radio" value="<?=$row_bank->id?>" <?php if($row_bank->is_default=='1'){ echo 'checked="checked"';} ?>>
					              <span class="checkmark"></span>
					            </label>
		            
					            <div class="address_list">
					              <span style="margin-bottom: 0px"><?=$row_bank->bank_name?> (Acc No. <?=$row_bank->account_no?>)</span>
					              <p style="margin-bottom: 0px">Account Type: <?=ucfirst($row_bank->account_type)?> | IFSC: <?=$row_bank->bank_ifsc?></p>
					              <p style="margin-bottom: 10px">Holder Name: <?=$row_bank->bank_holder_name?> | Mobile No.: <?=$row_bank->bank_holder_phone?></p>
					            </div>
				          	</div>
				          	<?php } ?>
		                    <div class="address_details_item">
					            <a href="" class="btn_new_account" style="font-size: 16px">
					              <div class="address_list" style="padding: 15px 5px">
					                <i class="fa fa-plus"></i> Add New Account
					              </div>
					            </a>
					        </div>
	        			</div>
	      			</div>
	      		</div>
	      	</form>

	      	<form method="post" accept-charset="utf-8" action="<?php echo site_url('site/add_new_bank'); ?>" class="bank_form" style="<?=(count($bank_details)!=0) ? 'display: none;' : 'display: block;'?>">
	      		<div class="row">
	      			<div class="col-md-12">
	      				<div class="form-group">
							<label for="bank_name">Bank Name <span>*</span>:</label>
							<input type="text" class="form-control" required="required" id="bank_name" name="bank_name">
						</div>
	      			</div>
	      			<div class="col-md-12">
	      				<div class="form-group">
							<label for="account_no">Bank Account No. <span>*</span>:</label>
							<input type="text" class="form-control" onkeypress="return isNumberKey(event)" autocomplete="off" required="required" id="account_no" name="account_no">
						</div>
	      			</div>
	      			<div class="col-md-6">
	      				<div class="form-group">
							<label for="bank_name">Account Type <span>*</span>:</label>
							<select class="form-control" required="required" name="account_type">
								<option value="saving">Saving</option>
								<option value="current">Current</option>
							</select>
						</div>
	      			</div>
	      			<div class="col-md-6">
	      				<div class="form-group">
							<label for="bank_ifsc">IFSC <span>*</span>:</label>
							<input type="text" class="form-control" required="required" id="bank_ifsc" name="bank_ifsc">
						</div>
	      			</div>
	      			<div class="col-md-12">
	      				<div class="form-group">
							<label for="holder_name">Name <span>*</span>:</label>
							<p class="hint_lbl">(Name must match the Name linked to this Bank Account)</p>
							<input type="text" class="form-control" required="required" id="holder_name" name="holder_name">
						</div>
	      			</div>
	      			<div class="col-md-12">
	      				<div class="form-group">
							<label for="holder_name" style="margin-bottom: 0px">Mobile Number <span>*</span>:</label>
							<p class="hint_lbl">(Mobile Number must match the Mobile Number linked to this Bank Account)</p>
							<input type="text" onkeypress="return isNumberKey(event)" maxlength="10" class="form-control" required="required" id="holder_mobile" name="holder_mobile">
						</div>
	      			</div>
	      			<div class="col-md-12">
	      				<div class="form-group">
							<label for="holder_email">Email ID <span>*</span>:</label>
							<p class="hint_lbl">(Email must match the Email linked to this Bank Account)</p>
							<input type="text" class="form-control" id="holder_email" name="holder_email">
						</div>
	      			</div>
	      			<div class="col-md-12">
	      				<label class="container_checkbox">Set default for Refund Payment
						  <input type="checkbox" checked="checked" name="is_default">
						  <span class="checkmark"></span>
						</label>
	      			</div>
	      			<div class="col-md-12">
	      				<br/>
	      				<div class="form-group">
	      					<button type="submit" class="btn btn-primary">Save</button>
	      					<button type="button" class="btn btn-warning btn_cancel_form">Cancel</button>
	      				</div>
	      			</div>
	      		</div>
	      	</form>
		  </div>
	      <div class="modal-footer">
	        <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
	        <button class="btn btn-primary claim_refund">Claim Refund</button>
	      </div>
	    </div>
	  </div>
	</div>

</div>




<script src="<?=base_url('assets/site_assets/js/timeline.js')?>"></script>

<script type="text/javascript">

	// Submit Bank Form
	$(".bank_form").submit(function(e){
		e.preventDefault();


		$(".msg_holder").html('');

		var _form=$(this);

		href=$(this).attr("action");

		$.ajax({
          type:'POST',
          url:href,
          data:$(this).serialize(),
          success:function(res){
            var obj = $.parseJSON(res);
            if(obj.success=='1'){

            	_form.find("input, textarea").val("");
            	$(".bank_form").hide();

            	$(".msg_holder").html('<div class="alert alert-success alert-dismissible" role="alert"><span class="err_txt">'+obj.message+'</span><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true" style="color: red;margin-right: 15px">&times;</span></button></div>');
            }
            else{
            	$(".msg_holder").html('<div class="alert alert-danger alert-dismissible" role="alert"><span class="err_txt">'+obj.message+'</span><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true" style="color: red;margin-right: 15px">&times;</span></button></div>');	
            }
            
          }
        });

	});

	$(".btn_cancel_form").click(function(e){
		e.preventDefault();
		$(".bank_form").hide();
	});


	$('#orderCancel, #claimRefund').on('hidden.bs.modal', function () {
	  $("body").css("overflow-y","auto");
	  $(".bank_form").hide();
	  $(".bank_details").hide();
	  $(".msg_holder").html('');
	  $("textarea[name='reason']").css("border-color","#ccc");
	  $("textarea").val('');
	});

	$(".btn_claim").click(function(e){
		e.preventDefault();

		if($(this).data("gateway")!='cod'){
			$(".bank_details").show();
		}
		else{
			$(".bank_details").hide();	
		}

		$("#claimRefund").modal("show");

		var order_id=$(this).data("order");
	    var product_id=$(this).data("product");

	    $("#claimRefund input[name='order_id']").val(order_id);
	    $("#claimRefund input[name='product_id']").val(product_id);

		$("body").css("overflow-y","hidden");

	});

	$(".claim_refund").click(function(e){
		e.preventDefault();

		var _btn=$(this);

		_btn.attr("disabled", true);

		_btn.text("Please wait...");

		var _bank_id=$(this).parents("#claimRefund").find("input[name='bank_acc_id']:checked").val();

		var flag=false;

		$('.notifyjs-corner').empty();

		if(!flag){

			var order_id=$(this).parents("#claimRefund").find("input[name='order_id']").val();
	    	var product_id=$(this).parents("#claimRefund").find("input[name='product_id']").val();

			var href = '<?=base_url()?>site/claim_refund';
			
        	$.ajax({
				type:'POST',
				url:href,
				data:{"order_id":order_id, "product_id": product_id, 'bank_id':_bank_id},
				success:function(res){

					var obj = $.parseJSON(res);

					_btn.attr("disabled", false);
					_btn.text("Claim Refund");

					if(obj.success==1){

					 $("#claimRefund").modal("hide");
					 swal({
				          title: "Done !", 
				          text: obj.msg, 
				          type: "success"
				      },function() {
				          location.reload();
				      });
					}
				}
			});
		}
	});

</script>



