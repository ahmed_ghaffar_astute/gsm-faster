<?php 
  $this->load->view('site/layout/breadcrumb'); 
  $ci =& get_instance();
?>

<div class="product-list-grid-view-area mt-20">
  <div class="container">
    <?php 
      if(isset($_GET['sortByBrand']) || isset($_GET['price_filter']) || isset($_GET['sort']))
      {
    ?>
      <p style="font-size: 16px">Filters:
        <br/>
        <?php 
          if(isset($_GET['sortByBrand']))
          {
            foreach ($_GET['sortByBrand'] as $key => $value) {
            ?>
            <span class="tag label label-danger" style="background-color: rgba(255,82,82,1);font-weight: 500;margin: 5px">
              <span><?=$ci->get_single_info(array('id' => $value), 'brand_name', 'tbl_brands')?></span>
              <a href="" style="color: #FFF" data-action="brands" data-id="<?=$value?>" class="remove_filter"><i class="fa fa-close"></i></a> 
            </span>
            <?php
            }
          }
        ?>

        <?php 
          if(isset($_GET['price_filter']))
          {
            ?>
            <span class="tag label label-danger" style="background-color: rgba(255,82,82,1);font-weight: 500;margin: 5px">
              <span>Price Range:&nbsp;&nbsp;<?=ucwords($_GET['price_filter'])?></span>
              <a href="" style="color: #FFF" data-action="price" class="remove_filter"><i class="fa fa-close"></i></a> 
            </span>
            <?php
          }
        ?>

        <?php 
          if(isset($_GET['sort']))
          {
            ?>
            <span class="tag label label-danger" style="background-color: rgba(255,82,82,1);font-weight: 500;margin: 5px">
              <span>Sort By:&nbsp;&nbsp;<?=ucwords($_GET['sort'])?></span>
              <a href="" style="color: #FFF" data-action="sort" class="remove_filter"><i class="fa fa-close"></i></a> 
            </span>
            <?php
          }
        ?>

        
      </p>
      <hr style="margin: 10px 0px" />
      <?php } ?>
    <div class="row"> 

      <div class="col-lg-3 col-md-3"> 
        <div class="widget widget-shop-categories">
          <h3 class="widget-shop-title">Shop By Categories</h3>
          <div class="widget-content">
            <ul class="product-categories">
              <?php
                
                $n=1;
                foreach ($category_list as $key => $row) 
                {

                  if($n > 5){
                    break;
                  }

                  $n++;

                  $counts=$ci->getCount('tbl_sub_category', array('category_id' => $row->id, 'status' => '1'));

                  if($counts > 0)
                  {
                    $url=base_url('category/'.$row->category_slug);  
                  }
                  else{
                    $url=base_url('category/products/'.$row->id);
                  }

              ?>
              <li>
                <i class="fa fa-angle-right"></i>
                <a href="<?=$url?>">
                  <?php 
                    if(strlen($row->category_name) > 30){
                      echo substr(stripslashes($row->category_name), 0, 30).'...';  
                    }else{
                      echo $row->category_name;
                    }
                  ?>
                </a>
              </li>
              <?php }
                if(count($category_list) > 5)
                {
                ?>
                  <li>
                    <i class="fa fa-angle-right"></i>
                    <a class="rx-default" href="<?=base_url('/category')?>">View All</a>
                  </li>
              <?php } ?>
            </ul>
          </div>
        </div>
        <div class="widget widget-price-slider">
          <h3 class="widget-title">Filter by Price</h3>
          <div class="widget-content">
            <div class="price-filter">
              <?php 
                if(isset($_GET['price_filter'])){

                  $price_filter=(explode('-', $_GET['price_filter']));

                  $min_price=$price_filter[0];
                  $max_price=$price_filter[1];

                }
                else{
                  $min_price=$price_min;
                  $max_price=$price_max;
                }
              ?>
              <form action="" method="get" id="price_filter_form">
                <div id="slider-range"></div>
                <span>Price:
                <input id="amount" class="amount" type="text" readonly="" data-currency="<?=CURRENCY_CODE?>" data-min="<?=floor($price_min)?>" data-max="<?=ceil($price_max)?>" data-min2="<?=floor($min_price)?>" data-max2="<?=ceil($max_price)?>">
                <input type="hidden" name="price_filter" id="price_filter" value="">
                </span>

                <?php
                  if(isset($_GET['sortByBrand']) && isset($_GET['sort'])){
                    foreach ($_GET['sortByBrand'] as $key => $value) {
                      echo '<input type="hidden" name="sortByBrand[]" value="'.$value.'">';
                    }
                    echo '<input type="hidden" name="sort" value="'.$_GET['sort'].'">';
                  }
                  else if(isset($_GET['sortByBrand']) && !isset($_GET['sort'])){
                    foreach ($_GET['sortByBrand'] as $key => $value) {
                      echo '<input type="hidden" name="sortByBrand[]" value="'.$value.'">';
                    }
                  }
                  else if(!isset($_GET['sortByBrand']) && isset($_GET['sort'])){
                    echo '<input type="hidden" name="sort" value="'.$_GET['sort'].'">';
                  }
                ?>

                <input class="price-button" value="Filter" type="submit">
              </form>
            </div>
          </div>
        </div>
        <div class="widget widget-brand">
          <h3 class="widget-title">Brands</h3>
          <div class="widget-content">
            <form action="" id="brand_sort" method="get">
              <ul class="brand-menu">
                <?php 

                  $checked='';
                  
                  foreach ($brand_list as $key => $value) {

                    if(!empty($_GET['sortByBrand'])){
                      if(in_array($value->id,$_GET['sortByBrand'])){
                        $checked='checked="checked"';
                      }
                      else{
                        $checked='';
                      }
                    }
                ?>
                <li>
                  <label>
                      <input type="checkbox" name="sortByBrand[]" class="brand_sort" <?=$checked?> value="<?=$value->id?>">
                      <?=$value->brand_name?> 
                      <span class="pull-right">(<?=$brand_count_items[$value->id]?>)</span>
                  </label>
                </li>
                <?php } ?>
              </ul>

              <?php
                if(isset($_GET['sort']) && isset($_GET['price_filter'])){
                  echo '<input type="hidden" name="sort" value="'.$_GET['sort'].'">';
                  echo '<input type="hidden" name="price_filter" value="'.$_GET['price_filter'].'">';
                }
                else if(isset($_GET['sort']) && !isset($_GET['price_filter'])){
                  echo '<input type="hidden" name="sort" value="'.$_GET['sort'].'">';
                }
                else if(!isset($_GET['sort']) && isset($_GET['price_filter'])){
                  echo '<input type="hidden" name="price_filter" value="'.$_GET['price_filter'].'">';
                }
              ?>

            </form>
          </div>
        </div>
        
        
          <?php 
            if($this->db->get_where('tbl_web_settings', array('id' => '1'))->row()->product_ad=='true')
            {
          ?>
          <div>
            <div class="widget widget-brand">
              <?php 
                echo $this->db->get_where('tbl_web_settings', array('id' => '1'))->row()->product_banner_ad;
              ?>
            </div>
          </div>
          <?php } ?>
          <div class="clearfix"></div>
      </div>
      <div class="col-lg-9 col-md-9">
        <div class="shop-tab-menu">
          <div class="row"> 
            <div class="col-md-3 col-sm-3 col-lg-4 col-xs-12">
              <div class="shop-tab">
                <ul>
                  <li class="active"><a data-toggle="tab" href="#grid-view"><i class="ion-android-apps"></i></a></li>
                  <li><a data-toggle="tab" href="#list-view"><i class="ion-navicon-round"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-9 col-sm-9 col-lg-8 hidden-xs text-right">
              <div class="toolbar-form">
                <form action="" method="get" id="sort_filter_form">

                  <?php
                    if(isset($_GET['sortByBrand']) && isset($_GET['price_filter'])){
                      foreach ($_GET['sortByBrand'] as $key => $value) {
                        echo '<input type="hidden" name="sortByBrand[]" value="'.$value.'">';
                      }
                      echo '<input type="hidden" name="price_filter" value="'.$_GET['price_filter'].'">';
                    }
                    else if(isset($_GET['sortByBrand']) && !isset($_GET['price_filter'])){
                      foreach ($_GET['sortByBrand'] as $key => $value) {
                        echo '<input type="hidden" name="sortByBrand[]" value="'.$value.'">';
                      }
                    }
                    else if(!isset($_GET['sortByBrand']) && isset($_GET['price_filter'])){
                      echo '<input type="hidden" name="price_filter" value="'.$_GET['price_filter'].'">';
                    }
                  ?>

                  <div class="toolbar-select"> <span>Sort by:</span>
                    <select data-placeholder="Sort by..." class="order-by list_order" name="sort" tabindex="1">
                      <option value="newest" <?php echo (isset($_GET['sort']) && strcmp($_GET['sort'], 'newest')==0) ? 'selected' : '' ?>>Newest First</option>
                      <option value="low-high" <?php echo (isset($_GET['sort']) && strcmp($_GET['sort'], 'low-high')==0) ? 'selected' : '' ?>>Low to High Price</option>
                      <option value="high-low" <?php echo (isset($_GET['sort']) && strcmp($_GET['sort'], 'high-low')==0) ? 'selected' : '' ?>>High to Low Price</option>
                      <option value="top" <?php echo (isset($_GET['sort']) && strcmp($_GET['sort'], 'top')==0) ? 'selected' : '' ?>>Top Selling</option>
                    </select>
                  </div>
                </form>
              </div>
              <div class="show-result">
                <p><?=$show_result;?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="shop-product-area">
          <div class="tab-content"> 
            <div id="grid-view" class="tab-pane fade in active">
              <div class="row">
                <div class="product-container"> 
                  <?php

                    $ci =& get_instance();
                    foreach ($product_list as $key => $row) {

                      $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

                      // $img_file='assets/images/products/'.$row->featured_image;

                      $img_file=$ci->_create_thumbnail('assets/images/products/',$row->product_slug,$row->featured_image,300,300);

                      $img_file2=$ci->_create_thumbnail('assets/images/products/',$row->id,$row->featured_image2,300,300);

                  ?>
                  <div class="col-md-3 col-sm-3 col-xs-12 item-col2">
                    <div class="single-product">
                      <div class="product-img"> <a href="<?php echo site_url('product/'.$row->product_slug); ?>" title="<?=$row->product_title?>" target="_blank"> <img class="first-img" src="<?=base_url().$img_file?>" alt=""> <img class="hover-img" src="<?=base_url().$img_file2?>" alt=""> </a>
                        <ul class="product-action">
                          <?php 
                            if(check_user_login() && $ci->is_favorite($this->session->userdata('GSM_FUS_UserId'), $row->id)){
                              ?>
                              <li><a href="" class="btn_wishlist" data-id="<?=$row->id?>" data-toggle="tooltip" title="Remove to Wishlist" style="background-color: #ff5252"><i class="ion-android-favorite-outline"></i></a></li>
                              <?php
                            }
                            else if($ci->check_cart($row->id,$user_id)){
                              ?>
                              <li><a href="javascript:void(0)" data-toggle="tooltip" title="Already in Cart"><i class="ion-android-favorite-outline"></i></a></li>
                              <?php
                            } 
                            else{
                              ?>
                              <li><a href="" class="btn_wishlist" data-id="<?=$row->id?>" data-toggle="tooltip" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                              <?php
                            } 
                          ?>

                          <li><a href="" class="btn_quick_view" data-id="<?=$row->id?>" title="Quick View"><i class="ion-android-expand"></i></a></li>
                        </ul>
                      </div>
                      <div class="product-content">
                        <h2>
                          <a href="<?php echo site_url('product/'.$row->product_slug); ?>" title="<?=$row->product_title?>" target="_blank">
                             <?php 
                                if(strlen($row->product_title) > 16){
                                  echo substr(stripslashes($row->product_title), 0, 16).'...';  
                                }else{
                                  echo $row->product_title;
                                }
                              ?>
                          </a>
                        </h2>
                        <div class="rating"> 

                          <?php 
                            for ($x = 0; $x < 5; $x++) { 
                              if($x < $row->rate_avg){
                                ?>
                                <i class="fa fa-star" style="color: #F9BA48"></i>
                                <?php  
                              }
                              else{
                                ?>
                                <i class="fa fa-star"></i>
                                <?php
                              }
                              
                            }
                          ?>
                        </div>
                        <div class="product-price"> 
                          <?php 
                            if($row->you_save_amt!='0'){
                              ?>
                              <span class="new-price"><?=CURRENCY_CODE.' '.$row->selling_price?></span> 
                              <span class="old-price"><?=CURRENCY_CODE.' '.$row->product_mrp;?></span>
                              
                              <?php
                            }
                            else{
                              ?>
                              <span class="new-price"><?=CURRENCY_CODE.' '.$row->product_mrp;?></span>
                              <?php
                              
                            }
                          ?>

                          <?php 
                            if(!$ci->check_cart($row->id,$user_id)){
                              ?>
                              <a class="button add-btn btn_cart" data-id="<?=$row->id?>" data-maxunit="<?=$row->max_unit_buy?>" style="" href="javascript:void(0)" data-toggle="tooltip" title="Add to Cart">add to cart</a>
                              <?php
                            }
                            else{
                              $cart_id=$ci->get_single_info(array('product_id' => $row->id, 'user_id' => $user_id),'id','tbl_cart');
                              ?>
                              <a class="button add-btn btn_remove_cart" style="" href="<?php echo site_url('remove-to-cart/'.$cart_id); ?>" data-toggle="tooltip" title="Remove to Cart">remove to cart</a>
                              <?php
                            }
                          ?>
                           </div>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <div id="list-view" class="tab-pane fade">
              <div class="row">
                <div class="all-prodict-item-list pt-10"> 
                  <div class="row">
                    <?php

                      $ci =& get_instance();
                      foreach ($product_list as $key => $row) {

                        $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

                        // $img_file='assets/images/products/'.$row->featured_image;

                        $img_file=$ci->_create_thumbnail('assets/images/products/',$row->product_slug,$row->featured_image,300,300);

                        $img_file2=$ci->_create_thumbnail('assets/images/products/',$row->id,$row->featured_image2,300,300);

                    ?>
                    <div class="col-md-12">
                      <div class="single-item">
                      <div class="product-img img-full">
                        <div class="col-md-4 col-sm-5"> <a href="<?php echo site_url('product/'.$row->product_slug); ?>" title="<?=$row->product_title?>" target="_blank"> <img class="first-img" src="<?=base_url().$img_file?>" alt="" style="height: 280px;width: 280px"> <img class="hover-img" src="<?=base_url().$img_file2?>" alt="" style="height: 280px;width: 280px"> </a></div>
                        <div class="col-md-8 col-sm-7">
                        <div class="product-content-2">
                          <h2>
                            <a href="<?php echo site_url('product/'.$row->product_slug); ?>" title="<?=$row->product_title?>" target="_blank">
                               <?php 
                                  if(strlen($row->product_title) > 40){
                                    echo substr(stripslashes($row->product_title), 0, 40).'...';  
                                  }else{
                                    echo $row->product_title;
                                  }
                                ?>
                            </a>
                          </h2>
                          <div class="rating"> 

                            <?php 
                              for ($x = 0; $x < 5; $x++) { 
                                if($x < $row->rate_avg){
                                  ?>
                                  <i class="fa fa-star" style="color: #F9BA48"></i>
                                  <?php  
                                }
                                else{
                                  ?>
                                  <i class="fa fa-star"></i>
                                  <?php
                                }
                              }
                            ?>
                          </div>
                          <div class="product-price mb-15">
                            <?php 
                            if($row->you_save_amt!='0'){
                              ?>
                              <span class="new-price"><?=CURRENCY_CODE.' '.$row->selling_price?></span> 
                              <span class="old-price"><?=CURRENCY_CODE.' '.$row->product_mrp;?></span>
                              
                              <?php
                            }
                            else{
                              ?>
                              <span class="new-price"><?=CURRENCY_CODE.' '.$row->product_mrp;?></span>
                              <?php
                              
                            }
                          ?>
                          </div>
                          <div class="product-discription">
                            <?php 
                              if(strlen($row->product_desc) > 140){
                                  echo substr(stripslashes($row->product_desc), 0, 140);  
                              }else{
                                  echo $row->product_desc;
                              }
                            ?>
                          </div>
                          <div class="pro-action-2 mt-10">
                          <ul class="product-cart-area-list">
                            <li>
                              <?php 
                                if(!$ci->check_cart($row->id,$user_id)){
                                  ?>
                                  <a class="action-btn big btn_cart" data-id="<?=$row->id?>" data-maxunit="<?=$row->max_unit_buy?>" style="" href="javascript:void(0)" data-toggle="tooltip" title="Add to Cart">add to cart</a>
                                  <?php
                                }
                                else{

                                  $cart_id=$ci->get_single_info(array('product_id' => $row->id, 'user_id' => $user_id),'id','tbl_cart');

                                  ?>
                                  <a class="action-btn big btn_remove_cart" href="<?php echo site_url('remove-to-cart/'.$cart_id); ?>" data-toggle="tooltip" title="Remove to Cart">remove to cart</a>
                                  <?php
                                }
                              ?>
                            </li>



                            <li>
                              <?php 
                                if(check_user_login() && $ci->is_favorite($this->session->userdata('GSM_FUS_UserId'), $row->id)){
                                  ?>
                                  <a class="action-btn small btn_wishlist" href="" class="btn_wishlist" data-id="<?=$row->id?>" data-toggle="tooltip" title="Remove to Wishlist" style="background-color: #ff5252"><i class="ion-android-favorite-outline"></i></a>

                                  <?php
                                }
                                else if($ci->check_cart($row->id,$user_id)){
                                  ?>

                                  <a class="action-btn small" href="javascript:void(0)" data-toggle="tooltip" title="Already in Cart"><i class="ion-android-favorite-outline"></i></a>

                                  <?php
                                } 
                                else{
                                  ?>

                                  <a class="action-btn small btn_wishlist" href="" class="btn_wishlist" data-id="<?=$row->id?>" data-toggle="tooltip" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a>

                                  <?php
                                } 
                              ?>
                            </li>
                            <li>
                              <a class="action-btn small btn_quick_view" data-id="<?=$row->id?>" title="Quick View"><i class="ion-android-expand"></i></a>
                            </li>
                          </ul>
                          </div>
                        </div>
                        </div>
                      </div>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php 
          if(!empty($links)){
        ?>
        <div class="pagination pb-10">
          <?php 
              echo $links;  
          ?>
        </div>
        <?php } ?>
      </div>
      
    </div>
  </div>
</div>
