<?php 
  $this->load->view('site/layout/breadcrumb'); 
  $ci =& get_instance();

  // print_r($my_review);

?>

<div class="product-list-grid-view-area mt-20">
	  <div class="container">
	    <div class="row"> 
			<div class="col-lg-3 col-md-3 mb_40"> 
		        <?php $this->load->view('site/layout/sidebar_my_account'); ?>
			</div>
			<div class="col-lg-9 col-md-9">
				<div class="my_profile_area_detail">
					<div class="checkout-title">
					  <h3>My Reviews & Rating</h3>
					</div>
					<?php 
						if(!empty($my_review))
						{
					?>
					<div class="row">
						<?php 
							foreach ($my_review as $key => $value) {

								$img_file=$ci->_create_thumbnail('assets/images/products/',$ci->get_single_info(array('id' => $value->product_id),'product_slug','tbl_product'),$ci->get_single_info(array('id' => $value->product_id),'featured_image','tbl_product'),200,200);

						?>
						<div class="col-md-12 details_part_product_img my_review_area">
						  <div class="row">
							<div class="col-md-2 col-sm-2 col-xs-4">
							  <div class="product_img_part"> <a href="<?php echo site_url('product/'.$ci->get_single_info(array('id' => $value->product_id),'product_slug','tbl_product')); ?>" target="_blank"><img src="<?=base_url().$img_file?>" alt="" style="border: 2px solid #ddd;border-radius: 6px;"></a> </div>
							</div>
							<div class="col-md-8 col-sm-8 col-xs-8"> <a href="<?php echo site_url('product/'.$ci->get_single_info(array('id' => $value->product_id),'product_slug','tbl_product')); ?>" target="_blank" style="font-weight:500"><?php echo $ci->get_single_info(array('id' => $value->product_id),'product_title','tbl_product'); ?></a>
							  <div><?=stripslashes($value->rating_desc)?></div>
							  <div class="product-rating">
							  	<?php 
	                              for ($x = 0; $x < 5; $x++) { 
	                                if($x < $value->rating){
	                                  ?>
	                                  <i class="fa fa-star"></i>
	                                  <?php  
	                                }
	                                else{
	                                  ?>
	                                  <i class="fa fa-star on-color"></i>
	                                  <?php
	                                }
	                              }
	                            ?>
							  	<a class="review-link edit_review" href="" data-stuff='<?php echo stripslashes(json_encode($value)); ?>'>(Edit Reviews)</a> 
							  </div>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-12 text-right"> <a href="javascript:void(0)" class="form-button pull-right btn-danger btn_remove_review" data-id="<?=$value->id?>">Delete</a> </div>
						  </div>
						</div>
						<?php } ?>
					</div>
					<?php }else{
						?>
						<div class="col-md-12 text-center" style="padding: 1em 0px 1em 0px">
							<h3><strong>Sorry!</strong> you haven't reviewed any product..</h3>	
						</div>
						<?php
					} ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="edit_review" class="modal fade" role="dialog" style="z-index: 99999">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="modal-details">
					<h3 style="font-weight: 500">Edit Review</h3>
                  	<br/>
					<div class="ceckout-form" style="background: none;border:none;">
						<form action="" method="post" id="edit_review_form">
							<input type="hidden" name="review_id" value="">
							<input type="hidden" class="inp_rating" name="rating" value="">
							<label>Your Rating</label>
							<div class="rating"> 
								<div class='rating-stars'>
								    <ul id='stars'>
								      <li class='star selected' title='Poor' data-value='1'>
								        <i class='fa fa-star fa-fw'></i>
								      </li>
								      <li class='star' title='Fair' data-value='2'>
								        <i class='fa fa-star fa-fw'></i>
								      </li>
								      <li class='star' title='Good' data-value='3'>
								        <i class='fa fa-star fa-fw'></i>
								      </li>
								      <li class='star' title='Excellent' data-value='4'>
								        <i class='fa fa-star fa-fw'></i>
								      </li>
								      <li class='star' title='WOW!!!' data-value='5'>
								        <i class='fa fa-star fa-fw'></i>
								      </li>
								    </ul>
								  </div> 
							</div>

							<label>Review</label>
							<textarea placeholder="enter you review..." name="message" cols="40" rows="8"></textarea>
							
							  <div class="add-to-link" style="margin-top: 15px">
								<button class="form-button" type="submit" data-text="save">Save</button>
								<button class="form-button" type="button" data-dismiss="modal">close</button>
							  </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(".edit_review").click(function(e){
		e.preventDefault();

		$('#edit_review').modal({
	        backdrop: 'static',
	        keyboard: false
	    })

		var data=$(this).data('stuff');


		var onStar = parseInt(data['rating'], 10); // The star currently selected
		var stars = $('#stars li').parent().children('li.star');


		for (i = 0; i < stars.length; i++) {
			$(stars[i]).removeClass('selected');
		}

		for (i = 0; i < onStar; i++) {
			$(stars[i]).addClass('selected');
		}

		$('#edit_review').find(".inp_rating").val(parseInt(data['rating'], 10));
		$('#edit_review').find("input[name='review_id']").val(data['id']);
		$('#edit_review').find("textarea[name='message']").val(data['rating_desc']);
	});


	$("#edit_review_form").submit(function(e){
		e.preventDefault();

		$(".process_loader").show();

		var formData = new FormData($(this)[0]);

		var href = '<?=base_url()?>site/edit_review';

		$.ajax({
	        url: href,
	        processData: false,
	        contentType: false,
	        type: 'POST',
	        data: formData,
	        success: function(data){

	          var obj = $.parseJSON(atob(data));
	          $(".process_loader").hide();
	          $('#edit_review').modal("hide");

	          if(obj.success=='1'){
          		swal({ title: "Updated!", text: obj.message, type: "success" }, function(){ location.reload(); });
	          }
	          else{
	          	swal("Something gone wrong!", obj.message);
	          }

	        }
	    });


	});
</script>