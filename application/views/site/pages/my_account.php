<?php 
  	$this->load->view('site/layout/breadcrumb'); 
  	$ci =& get_instance();

  	// print_r($user_data);

  	define('IMG_PATH', base_url().'assets/images/users/');

  	if($user_data->user_image!='' && file_exists('assets/images/users/'.$user_data->user_image)){
		$user_img=IMG_PATH.$user_data->user_image;
	}
	else{
		$user_img=base_url('assets/images/photo.jpg');
	}

?>

<div class="product-list-grid-view-area mt-20">
	  <div class="container">
	    <div class="row"> 
			<div class="col-lg-3 col-md-3 mb_40"> 
		        <?php $this->load->view('site/layout/sidebar_my_account'); ?>
			</div>
			<div class="col-lg-9 col-md-9">
				<div class="my_profile_area_detail">
					<div class="checkout-title">
					  <h3>My Profile</h3>
					</div>
					<form action="" id="profile_form">
						<div class="row">
							<div class="form-fild col-md-6">
							  <p>
								<label>Name</label>
							  </p>
							  <input type="text" name="user_name" placeholder="Name" value="<?=$user_data->user_name?>" >
							</div>
							<div class="form-fild col-md-6">
							  <p>
								<label>Email Address</label>
							  </p>
							  <input type="text" name="user_email" placeholder="Email" value="<?=$user_data->user_email?>" readonly>
							</div>
							<div class="form-fild col-md-6">
							  <p>
								<label>Phone Number</label>
							  </p>
							  <input type="text" name="user_phone" placeholder="Phone Number" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="10" value="<?=$user_data->user_phone?>" >
							</div>
							<div class="form-fild col-md-4">
							  <p>
								<label>Profile Photo (200X200)</label>
							  </p>
							  <input type="file" name="file_name" value="" style="padding-top:8px">
							</div>
							<div class="form-fild col-md-2">
								<img class="fileupload_img" src="<?=$user_img?>" style="width:70px;height:70px;border-radius:50px;">
							</div>
							<div class="login-submit col-md-12">
							  <button type="submit" class="form-button">Save</button>			  
							</div>
						</div>			
					</form>
				</div>
				<div class="my_profile_area_detail">
					<div class="checkout-title">
					  <h3>Change Password</h3>
					</div>
					<form action="" id="change_password_form" method="post">
						<div class="row">
							<div class="form-fild col-md-12">
							  <p>
								<label>Old Password</label>
							  </p>
							  <input type="password" name="old_password" placeholder="Old Password" value=""  autocomplete="off">
							  <p class="err err_old_password"><i class="fa fa-exclamation-circle"></i> Old password is required !</p>
							</div>
							<div class="form-fild col-md-6">
							  <p>
								<label>New Password</label>
							  </p>
							  <input type="password" name="new_password" placeholder="New Password" value=""  autocomplete="off">
							  <p class="err err_new_password"><i class="fa fa-exclamation-circle"></i> New password is required !</p>
							</div>
							<div class="form-fild col-md-6">
							  <p>
								<label>Confirm New Password</label>
							  </p>
							  <input type="password" name="confirm_password" placeholder="Confirm New Password" value="" autocomplete="off">
							  <p class="err err_confirm_password"><i class="fa fa-exclamation-circle"></i> Confirm password is required !</p>
							</div>
							<div class="login-submit col-md-12">
							  <button type="submit" class="form-button">Change</button>			  
							</div>
						</div>			
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$(".fileupload_img").attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	$("input[name='file_name']").change(function() { 
		readURL(this);
	});

	$("#profile_form").submit(function(e){

		var inputs = $("#profile_form :input[type='text']");
		var counts=0;
		e.preventDefault();

		inputs.each(function(){
			if($(this).val()==''){
				$(this).css("border-color","#F00");
				counts++;
			}
			else{
				$(this).css("border-color","#E5E5E5");
			}
		});

		if(counts==0){
			
			$(".process_loader").show();

			var formData = new FormData($(this)[0]);

			var href = '<?=base_url()?>site/update_profile';

		    $.ajax({
		        url: href,
		        processData: false,
		        contentType: false,
		        type: 'POST',
		        data: formData,
		        success: function(data){

		          var obj = $.parseJSON(atob(data));
		          
		          $(".process_loader").hide();

		          if(obj.status==1){
	          		swal({ title: "Updated!", text: obj.msg, type: "success" }, function(){ location.reload(); });
		          }
		          else{
		          	swal("Something gone wrong!", obj.msg);
		          }

		        }
		     });

		}


	});

	$("#profile_form :input[type='text']").blur(function(e){
		if($(this).val()!=''){
			$(this).css("border-color","#E5E5E5");
		}
		else{
			$(this).css("border-color","#F00");
		}
	});

	$("#change_password_form").submit(function(e){

		var inputs = $("#change_password_form :input[type='password']");

		var counts=0;
		e.preventDefault();

		inputs.each(function(){
			if($(this).val()==''){

				$(this).css("border-color","#F00");

				$(this).next(".err").show();
				counts++;
			}
			else{
				$(this).next(".err").hide();
			}
		});

		if(counts==0){
			

			var formData = new FormData($(this)[0]);

			var href = '<?=base_url()?>site/change_password';

		    $.ajax({
		        url: href,
		        processData: false,
		        contentType: false,
		        type: 'POST',
		        data: formData,
		        success: function(data){

		          var obj = $.parseJSON(atob(data));
		          
		          if(obj.status==1){
	          		swal({ title: "Changed!", text: obj.msg, type: "success" }, function(){ location.reload(); });
		          }
		          else{
		          	$("#change_password_form").find("."+obj.class).show().html('<i class="fa fa-exclamation-circle"></i> '+obj.msg);
		          }
		          


		        }
		     });

		}

	});

	$("#change_password_form input").blur(function(e){
		if($(this).val()!=''){
			

			if($("input[name='confirm_password']").val()!=''){
				if($("input[name='confirm_password']").val()!=$("input[name='new_password']").val()){
					$("input[name='confirm_password']").next(".err").show().html('<i class="fa fa-exclamation-circle"></i> New and Confirm password must be match !');
				}
				else{
					$(this).next(".err").hide();
					$(this).css("border-color","#E5E5E5");
				}
			}
			else{
				$(this).next(".err").show().html('<i class="fa fa-exclamation-circle"></i> Confirm password is required !');
			}

			$(this).next("p").hide();

			$(this).css("border-color","#E5E5E5");

		}
		else{
			$(this).next("p").show(); 
		}
	});

	$("input[name='confirm_password']").keyup(function(e){
		if($(this).val()!=''){
			if($(this).val()!=$("input[name='new_password']").val()){
				$(this).css("border-color","#F00");
				$(this).next(".err").show().html('<i class="fa fa-exclamation-circle"></i> New and Confirm password must be match !');
			}
			else{
				$(this).next(".err").hide();
				$(this).css("border-color","#E5E5E5");
			}
		}
		else{

			$(this).css("border-color","#F00");

			$(this).next(".err").show().html('<i class="fa fa-exclamation-circle"></i> Confirm password is required !');
		}
	});

	$("input[name='confirm_password']").blur(function(e){
		if($(this).val()!=''){

			if($(this).val()!=$("input[name='new_password']").val()){

				$(this).css("border-color","#F00");

				$(this).next(".err").show().html('<i class="fa fa-exclamation-circle"></i> New and Confirm password must be match !');
			}
			else{
				$(this).next(".err").hide();
				$(this).css("border-color","#E5E5E5");
			}
		}
		else{
			$(this).css("border-color","#F00");
			$(this).next(".err").show().html('<i class="fa fa-exclamation-circle"></i> Confirm password is required !');
		}
	});

</script>