<?php 
  $this->load->view('site/layout/breadcrumb'); 
  // print_r($bank_details);
  $ci =& get_instance();
?>
<div class="wishlist-table-area mt-20 mb-50">
    <div class="container">
      <div class="row">
      	<?php 
          if(!empty($my_orders)){ 
      	?>
        <div class="col-md-12">
        	<?php 
        		foreach ($my_orders as $key => $value) {
        	?>
			<div class="product_oreder_part">
			  <div class="oreder_part_block">
				  <div class="order_detail_track">
					<div class="row">
						<div class="col-md-5 col-sm-4 col-xs-12">
						  <div class="order_track_btn">
							<a href="<?php echo site_url('my-orders/'.$value->order_unique_id); ?>">
								<div class="order_btn" style="text-transform: none;"><?=$value->order_unique_id?></div>
							</a>
						  </div>
						</div>
						<?php

							$status_arr=$ci->order_status($value->id,0);

							if($value->order_status!='4' && $value->order_status!='5'){
								?>
								<div class="col-md-4 col-sm-5 col-xs-12">
									Delivery Expected By <?=date("M d",$value->delivery_date)?>
									<br><?=$ci->get_single_info(array('order_id' => $value->id,'status_title' => $value->order_status),'status_desc','tbl_order_status')?>
								</div>
								<?php
							}
							else if($value->order_status=='5'){
								?>
								<div class="col-md-4 col-sm-4 col-xs-12">
									This Order Has Been Cancelled
								</div>
								<?php
							}
							else{
								?>
								<div class="col-md-4 col-sm-4 col-xs-12">
									Delivered on <?=date("M d",$value->delivery_date)?>
									<br><?=$ci->get_single_info(array('order_id' => $value->id,'status_title' => $value->order_status),'status_desc','tbl_order_status')?>
								</div>
								<?php
							}
						?>
						<div class="col-md-3 col-sm-3 col-xs-12 order_track_item">
						  <div class="order_cancle_btn_item">
							<a href="<?php echo site_url('my-orders/'.$value->order_unique_id); ?>">
								<div class="cancle_order_btn" data-id="<?=$value->order_unique_id;?>"><i class="fa fa-map-marker"></i> Track</div>
							</a>
						   </div>
						</div>
					</div>
				  </div>
				  <div class="track_order_details_part">
				  	<?php 
				  		$where= array('order_id' => $value->id);

                    	$row_items=$this->common_model->selectByids($where, 'tbl_order_items');

                    	foreach ($row_items as $key2 => $value2) {

                    		$img_file=$ci->_create_thumbnail('assets/images/products/',$ci->get_single_info(array('id' => $value2->product_id),'product_slug','tbl_product'),$ci->get_single_info(array('id' => $value2->product_id),'featured_image','tbl_product'),100,100);
				  	?>
					<div class="col-md-12 details_part_product_img slingle_item_address_part">
					  <div class="row">
						<div class="col-md-1 col-sm-2 col-xs-4">
							<div class="product_img_part">
								<a href="<?php echo site_url('product/'.$ci->get_single_info(array('id' => $value2->product_id),'product_slug','tbl_product')); ?>" target="_blank"><img src="<?=base_url().$img_file?>" alt=""></a>	
							</div>
							
						</div>

						<div class="col-md-4 col-sm-6 col-xs-8">
						  <a href="<?php echo site_url('product/'.$ci->get_single_info(array('id' => $value2->product_id),'product_slug','tbl_product')); ?>" target="_blank" title="<?=$value2->product_title?>">
						  	<?php 
			                    if(strlen($value2->product_title) > 40){
			                      echo substr(stripslashes($value2->product_title), 0, 40).'...';  
			                    }else{
			                      echo $value2->product_title;
			                    }
			                  ?>
						  </a>
						  <div>Price: <?=CURRENCY_CODE.' '.$value2->product_price?></div>
						  <div>Qty: <?=$value2->product_qty?></div>
						  <?php 
						  	if($value2->product_size!='')
						  	{
						  		echo '<div>Size: '.$value2->product_size.'</div>';
						  	}
						  ?>

						</div>

						<?php
							if($value2->pro_order_status!='4' && $value2->pro_order_status!='5'){
								?>
								<div class="col-md-2 col-sm-4 col-xs-12 col-md-offset-5 text-right">				 
									<a href="javascript:void(0)" class="form-button pull-right btn-danger product_cancel pull-right" data-order="<?=$value2->order_id?>" data-product="<?=$value2->product_id?>" data-unique="<?=$value->order_unique_id?>" data-gateway="<?=$ci->get_single_info(array('order_id' => $value2->order_id),'gateway','tbl_transaction')?>">Cancel</a>
								</div>
								<?php
							}
							else if($value2->pro_order_status=='5'){

								$cancelled_on=$ci->get_single_info(array('order_id' => $value2->order_id, 'product_id' => $value2->product_id),'created_at','tbl_refund');

								?>
								<div class="col-md-7 col-sm-4 col-xs-12">
									<span style="color: red;">This Product Has Been Cancelled on <?=date('d-m-Y h:i A',$cancelled_on)?></span>
									<br>
									<strong>Reason:</strong>
									<?php echo '<label style="">'.$ci->get_single_info(array('order_id' => $value2->order_id, 'product_id' => $value2->product_id),'refund_reason','tbl_refund').'</label>';?>
									<?php 
										if($ci->get_single_info(array('order_id' => $value2->order_id, 'product_id' => $value2->product_id),'gateway','tbl_refund')!='cod')
										{
											switch ($ci->get_single_info(array('order_id' => $value2->order_id, 'product_id' => $value2->product_id),'request_status','tbl_refund')) {
											  case '0':
												  $_lbl_title='Pending';
												  $_lbl_class='label-warning';
												  break;
											  case '2':
												  $_lbl_title='Process';
												  $_lbl_class='label-primary';
												  break;
											  case '1':
												  $_lbl_title='Completed';
												  $_lbl_class='label-success';
												  break;
											  case '-1':
												  $_lbl_title='Wating for claim';
												  $_lbl_class='btn-danger';
											}
											?>
											<br/>
											Refund Status: <label class="label <?=$_lbl_class?>"><?=$_lbl_title?></label>
											<?php 
											if($ci->get_single_info(array('order_id' => $value2->order_id, 'product_id' => $value2->product_id),'request_status','tbl_refund')=='-1')
											{
												echo '<button class="btn btn-warning btn-sm btn_claim" data-order="'.$value2->order_id.'" data-product="'.$value2->product_id.'" style="border-radius: 4px;outline: none">Claim Refund</button>';
											}
										}
									?>
								</div>
								<?php
							}
						?>

					  </div>
					</div>
					<?php } ?>
					<div class="row product_img_part_bottom">
					  <div class="col-md-6 col-sm-6 col-xs-12 product_item_date_item"><span>Ordered On </span><?=date("D, M jS 'y",$value->order_date)?></div>
					  <div class="col-md-6 col-sm-6 col-xs-12 price_item_right"><span>Order Total </span><span class="product_item_price_item"><?=CURRENCY_CODE.' '.$value->payable_amt?></span></div>
					</div>
				  </div>
				</div>
			</div>	
			<?php } ?>
        </div>
        <?php }else{ ?>
        <div class="col-md-12 text-center" style="padding: 1em 0px 3em 0px">
			<h3><strong>Sorry!</strong> no order found..</h3>	
		</div>
        <?php } ?>
      </div>
    </div>
</div>

<div id="orderCancel" class="modal" style="z-index: 9999999;background: rgba(0,0,0,0.5);overflow-y: auto;">
  <div class="modal-dialog modal-confirm">
    <div class="modal-content">
      <div class="modal-header">
        <img src="<?=base_url('assets/images/shopping-cancel-512.png')?>" style="width: 70px">
        <h4 class="modal-title cancelTitle">Are you sure to cancel selected product order ?</h4>
        <h5>ORDER ID: <span class="order_unique_id"></span></h5> 
      </div>
      <div class="modal-body" style="padding:0px;padding-top:20px;">
      	<div class="msg_holder">
      		
      	</div>
      	<form id="">
      		<input type="hidden" name="order_id" value="">
      		<input type="hidden" name="product_id" value="">
      		<input type="hidden" name="gateway" value="">

      		<div class="row">
      			<div class="col-md-12">
      				<div class="form-group">
						<label for="bank_name">Reason <span>*</span>:</label>
      					<textarea class="form-control" name="reason" rows="4" placeholder="Reason for cancellation"></textarea>	
      				</div>
      			</div>
      			<div class="col-md-12 bank_details" style="display: none">
      				<div class="address_details_block">
      					<?php 
      						foreach ($bank_details as $key => $row_bank) {
      					?>
	                    <div class="address_details_item">
				            <label class="container">
				              <input type="radio" name="bank_acc_id" class="address_radio" value="<?=$row_bank->id?>" <?php if($row_bank->is_default=='1'){ echo 'checked="checked"';} ?>>
				              <span class="checkmark"></span>
				            </label>
	            
				            <div class="address_list">
				              <span style="margin-bottom: 0px"><?=$row_bank->bank_name?> (Acc No. <?=$row_bank->account_no?>)</span>
				              <p style="margin-bottom: 0px">Account Type: <?=ucfirst($row_bank->account_type)?> | IFSC: <?=$row_bank->bank_ifsc?></p>
				              <p style="margin-bottom: 10px">Holder Name: <?=$row_bank->bank_holder_name?> | Mobile No.: <?=$row_bank->bank_holder_phone?></p>
				            </div>
			          	</div>
			          	<?php } ?>
	                    <div class="address_details_item">
				            <a href="" class="btn_new_account" style="font-size: 16px">
				              <div class="address_list" style="padding: 15px 5px">
				                <i class="fa fa-plus"></i> Add New Account
				              </div>
				            </a>
				        </div>
        			</div>
      			</div>
      		</div>
      	</form>

      	<form method="post" accept-charset="utf-8" action="<?php echo site_url('site/add_new_bank'); ?>" class="bank_form" style="display: none;">
      		<div class="row">
      			<div class="col-md-12">
      				<div class="form-group">
						<label for="bank_name">Bank Name <span>*</span>:</label>
						<input type="text" class="form-control" required="required" name="bank_name">
					</div>
      			</div>
      			<div class="col-md-12">
      				<div class="form-group">
						<label for="account_no">Bank Account No. <span>*</span>:</label>
						<input type="text" class="form-control" onkeypress="return isNumberKey(event)" required="required" name="account_no">
					</div>
      			</div>
      			<div class="col-md-6">
      				<div class="form-group">
						<label for="bank_name">Account Type <span>*</span>:</label>
						<select class="form-control" required="required" name="account_type">
							<option value="saving">Saving</option>
							<option value="current">Current</option>
						</select>
					</div>
      			</div>
      			<div class="col-md-6">
      				<div class="form-group">
						<label for="bank_ifsc">IFSC <span>*</span>:</label>
						<input type="text" class="form-control" required="required" name="bank_ifsc">
					</div>
      			</div>
      			<div class="col-md-12">
      				<div class="form-group">
						<label for="holder_name">Name <span>*</span>:</label>
						<p class="hint_lbl">(Name must match the Name linked to this Bank Account)</p>
						<input type="text" class="form-control" required="required" name="holder_name">
					</div>
      			</div>
      			<div class="col-md-12">
      				<div class="form-group">
						<label for="holder_name" style="margin-bottom: 0px">Mobile Number <span>*</span>:</label>
						<p class="hint_lbl">(Mobile Number must match the Mobile Number linked to this Bank Account)</p>
						<input type="text" class="form-control" onkeypress="return isNumberKey(event)" maxlength="10" required="required" name="holder_mobile">
					</div>
      			</div>
      			<div class="col-md-12">
      				<div class="form-group">
						<label for="holder_email">Email ID <span>*</span>:</label>
						<p class="hint_lbl">(Email must match the Email linked to this Bank Account)</p>
						<input type="text" class="form-control" name="holder_email">
					</div>
      			</div>
      			<div class="col-md-12">
      				<label class="container_checkbox">Set default for Refund Payment
					  <input type="checkbox" checked="checked" name="is_default">
					  <span class="checkmark"></span>
					</label>
      			</div>
      			<div class="col-md-12">
      				<br/>
      				<div class="form-group">
      					<button type="submit" class="btn btn-primary">Save</button>
      					<button type="button" class="btn btn-warning btn_cancel_form">Cancel</button>
      				</div>
      			</div>
      		</div>
      	</form>
      </div>
      <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
        <button class="btn btn-primary cancel_order">Cancel Order</button>
      </div>
    </div>
  </div>
</div>

<div id="claimRefund" class="modal" style="z-index: 9999999;background: rgba(0,0,0,0.5);overflow-y: auto;">
  <div class="modal-dialog modal-confirm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Choose you refund account</h4>
      </div>
      <div class="modal-body" style="padding-top: 0px">
      	<div class="msg_holder">
      		
      	</div>
      	<form id="">
      		<input type="hidden" name="order_id" value="">
      		<input type="hidden" name="product_id" value="">
      		<div class="row">
      			<div class="col-md-12 bank_details" style="display: none">
      				<div class="address_details_block">
      					<?php 
      						foreach ($bank_details as $key => $row_bank) {
      					?>
	                    <div class="address_details_item">
				            <label class="container">
				              <input type="radio" name="bank_acc_id" class="address_radio" value="<?=$row_bank->id?>" <?php if($row_bank->is_default=='1'){ echo 'checked="checked"';} ?>>
				              <span class="checkmark"></span>
				            </label>
	            
				            <div class="address_list">
				              <span style="margin-bottom: 0px"><?=$row_bank->bank_name?> (Acc No. <?=$row_bank->account_no?>)</span>
				              <p style="margin-bottom: 0px">Account Type: <?=ucfirst($row_bank->account_type)?> | IFSC: <?=$row_bank->bank_ifsc?></p>
				              <p style="margin-bottom: 10px">Holder Name: <?=$row_bank->bank_holder_name?> | Mobile No.: <?=$row_bank->bank_holder_phone?></p>
				            </div>
			          	</div>
			          	<?php } ?>
	                    <div class="address_details_item">
				            <a href="" class="btn_new_account" style="font-size: 16px">
				              <div class="address_list" style="padding: 15px 5px">
				                <i class="fa fa-plus"></i> Add New Account
				              </div>
				            </a>
				        </div>
        			</div>
      			</div>
      		</div>
      	</form>

      	<form method="post" accept-charset="utf-8" action="<?php echo site_url('site/add_new_bank'); ?>" class="bank_form" style="<?=(count($bank_details)!=0) ? 'display: none;' : 'display: block;'?>">
      		<div class="row">
      			<div class="col-md-12">
      				<div class="form-group">
						<label for="bank_name">Bank Name <span>*</span>:</label>
						<input type="text" class="form-control" required="required" id="bank_name" name="bank_name">
					</div>
      			</div>
      			<div class="col-md-12">
      				<div class="form-group">
						<label for="account_no">Bank Account No. <span>*</span>:</label>
						<input type="text" class="form-control" onkeypress="return isNumberKey(event)" autocomplete="off" required="required" id="account_no" name="account_no">
					</div>
      			</div>
      			<div class="col-md-6">
      				<div class="form-group">
						<label for="bank_name">Account Type <span>*</span>:</label>
						<select class="form-control" required="required" name="account_type">
							<option value="saving">Saving</option>
							<option value="current">Current</option>
						</select>
					</div>
      			</div>
      			<div class="col-md-6">
      				<div class="form-group">
						<label for="bank_ifsc">IFSC <span>*</span>:</label>
						<input type="text" class="form-control" required="required" id="bank_ifsc" name="bank_ifsc">
					</div>
      			</div>
      			<div class="col-md-12">
      				<div class="form-group">
						<label for="holder_name">Name <span>*</span>:</label>
						<p class="hint_lbl">(Name must match the Name linked to this Bank Account)</p>
						<input type="text" class="form-control" required="required" id="holder_name" name="holder_name">
					</div>
      			</div>
      			<div class="col-md-12">
      				<div class="form-group">
						<label for="holder_name" style="margin-bottom: 0px">Mobile Number <span>*</span>:</label>
						<p class="hint_lbl">(Mobile Number must match the Mobile Number linked to this Bank Account)</p>
						<input type="text" onkeypress="return isNumberKey(event)" maxlength="10" class="form-control" required="required" id="holder_mobile" name="holder_mobile">
					</div>
      			</div>
      			<div class="col-md-12">
      				<div class="form-group">
						<label for="holder_email">Email ID <span>*</span>:</label>
						<p class="hint_lbl">(Email must match the Email linked to this Bank Account)</p>
						<input type="text" class="form-control" id="holder_email" name="holder_email">
					</div>
      			</div>
      			<div class="col-md-12">
      				<label class="container_checkbox">Set default for Refund Payment
					  <input type="checkbox" checked="checked" name="is_default">
					  <span class="checkmark"></span>
					</label>
      			</div>
      			<div class="col-md-12">
      				<br/>
      				<div class="form-group">
      					<button type="submit" class="btn btn-primary">Save</button>
      					<button type="button" class="btn btn-warning btn_cancel_form">Cancel</button>
      				</div>
      			</div>
      		</div>
      	</form>
	  </div>
      <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
        <button class="btn btn-primary claim_refund">Claim Refund</button>
      </div>
    </div>
  </div>
</div>

<div id="orderConfirm" class="modal" style="z-index: 9999999;background: rgba(0,0,0,0.8);text-align:center;">
  <div class="modal-dialog modal-confirm" style="width: fit-content">
    <div class="modal-content">
      <div class="modal-header">
        <img src="<?=base_url('assets/images/success-icon.png')?>" style="width: 70px">
        <h4 class="modal-title">Order is placed!</h4> 
      </div>
      <div class="modal-body" style="padding-top: 0px">
        <p class="text-center" style="font-size: 18px;color: green;font-weight: 600;margin-bottom: 5px">Thank you for order...</p>
        <p style="margin-bottom: 5px;text-align: center;">Your order has been confirmed. Check your email for detials.</p>
        <p style="color: #000;margin-bottom: 5px;font-size: 16px;text-align: center;"><strong>Order No:</strong> <span class="ord_no_lbl"></span></p>
      </div>
      <div class="modal-footer" style="text-align: center;">
        <button class="btn btn-danger btn_track">Track Order</button>
        <button class="btn btn-primary btn_orders" onclick="location.href='<?=base_url('my-orders')?>'">My Orders</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">

	// Submit Bank Form
	$(".bank_form").submit(function(e){
		e.preventDefault();


		$(".msg_holder").html('');

		var _form=$(this);

		href=$(this).attr("action");

		$.ajax({
          type:'POST',
          url:href,
          data:$(this).serialize(),
          success:function(res){
            var obj = $.parseJSON(res);
            if(obj.success=='1'){

            	_form.find("input, textarea").val("");
            	$(".bank_form").hide();

            	swal({
					title: "Done !", 
					text: obj.message, 
					type: "success"
				},function() {
					location.reload();
				});
            }
            else{

            	$(".msg_holder").html('<div class="alert alert-danger alert-dismissible" role="alert"><span class="err_txt">'+obj.message+'</span><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true" style="color: red;margin-right: 15px">&times;</span></button></div>');	
            }
            
          }
        });

	});

	$(".btn_cancel_form").click(function(e){
		e.preventDefault();
		$(".bank_form").hide();
	});


	$('#orderCancel, #claimRefund').on('hidden.bs.modal', function () {
	  $("body").css("overflow-y","auto");
	  $(".bank_form").hide();
	  $(".bank_details").hide();
	  $(".msg_holder").html('');
	  $("textarea[name='reason']").css("border-color","#ccc");
	  $("textarea").val('');
	});

	$(".btn_claim").click(function(e){
		e.preventDefault();

		if($(this).data("gateway")!='cod'){
			$(".bank_details").show();
		}
		else{
			$(".bank_details").hide();	
		}

		$("#claimRefund").modal("show");

		var order_id=$(this).data("order");
	    var product_id=$(this).data("product");

	    $("#claimRefund input[name='order_id']").val(order_id);
	    $("#claimRefund input[name='product_id']").val(product_id);

		$("body").css("overflow-y","hidden");

	});

	$(".claim_refund").click(function(e){
		e.preventDefault();

		var _btn=$(this);

		_btn.attr("disabled", true);

		_btn.text("Please wait...");

		var _bank_id=$(this).parents("#claimRefund").find("input[name='bank_acc_id']:checked").val();

		var flag=false;

		$('.notifyjs-corner').empty();

		if(!flag){

			var order_id=$(this).parents("#claimRefund").find("input[name='order_id']").val();
	    	var product_id=$(this).parents("#claimRefund").find("input[name='product_id']").val();

			var href = '<?=base_url()?>site/claim_refund';
			
        	$.ajax({
				type:'POST',
				url:href,
				data:{"order_id":order_id, "product_id": product_id, 'bank_id':_bank_id},
				success:function(res){

					var obj = $.parseJSON(res);

					_btn.attr("disabled", false);
					_btn.text("Claim Refund");

					if(obj.success==1){

					 $("#claimRefund").modal("hide");
					 swal({
				          title: "Done !", 
				          text: obj.msg, 
				          type: "success"
				      },function() {
				          location.reload();
				      });
					}
				}
			});
		}
	});

</script>

<?php
  if($this->session->flashdata('payment_msg')) {
    $data = $this->session->flashdata('payment_msg');
    ?>
    <script type="text/javascript">
      $("#orderConfirm .ord_no_lbl").text('<?=$data['order_unique_id']?>');

      $("#orderConfirm .btn_track").click(function(e){
        window.location.href='<?=base_url().'my-orders/'.$data['order_unique_id']?>';
      });

      $("#orderConfirm").fadeIn();
    </script>
    <?php
  } 
?>
