<?php 
  $this->load->view('site/layout/breadcrumb'); 
?>

<div class="product-list-grid-view-area mt-20">
  	<div class="container">
    	<div class="row">
    		<div class="col-md-12 text-center" style="padding: 1em 0px 3em 0px">
    			<h3><strong>Sorry!</strong> no products available..</h3>	
    		</div> 
    	</div>
 	</div>
</div>