<?php  
  $this->load->view('site/layout/breadcrumb'); 

  // print_r($wishlist);

?>
<div class="wishlist-table-area mt-20 mb-50">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <?php 
            if(!empty($wishlist))
            {
          ?>
          <div class="wishlist-table table-responsive">

            <table>
              <thead>
                <tr>
                  <th class="product-remove"></th>
                  <th class="product-cart-img"> <span class="nobr">Product Image</span> </th>
                  <th class="product-name"> <span class="nobr">Product Name</span> </th>
                  <th class="product-price"> <span class="nobr"> Price </span> </th>
                  <!-- <th class="product-stock-stauts"> <span class="nobr"> Stock Status </span> </th> -->
                  <th class="product-add-to-cart">Add To Cart</th>
                </tr>
              </thead>
              <tbody>
              	<?php 
                  $ci =& get_instance();
                  foreach ($wishlist as $key => $row) {

                    $img_file=$ci->_create_thumbnail('assets/images/products/',$row->product_slug,$row->featured_image,50,50);
                ?>
                <tr>
                  <td class="product-remove"><a href="javascript:void(0)" class="btn_remove_wishlist" data-id="<?=$row->product_id?>">×</a></td>
                  <td class="product-cart-img"><a href="javascript:void(0)"><img src="<?=base_url().$img_file?>" alt="<?=$row->product_slug?>" style="width: 50px;height: 50px"></a></td>
                  <td class="product-name">
                    <a href="<?php echo site_url('product/'.$row->product_slug); ?>">
                      <?php 
                        if(strlen($row->product_title) > 30){
                          echo strtr(stripslashes($row->product_title), 0, 30).'...';  
                        }else{
                          echo $row->product_title;
                        }
                      ?>
                    </a>
                  </td>
                  <td class="product-price">
                    <span>
                      <?php 
                        if($row->you_save_amt!='0'){
                          ?>
                          <ins><?=CURRENCY_CODE.' '.$row->selling_price?></ins> 
                          <del><?=CURRENCY_CODE.' '.$row->product_mrp;?></del>
                          <?php
                        }
                        else{
                          ?>
                          <ins><?=CURRENCY_CODE.' '.$row->product_mrp;?></ins>
                          <?php
                          
                        }
                      ?>
                    </span>
                  </td>
                  <!-- <td class="product-stock-status"><span class="wishlist-in-stock">In Stock</span></td> -->
                  <td class="product-add-to-cart">
                    <a class="wishlist-btn btn_cart" data-id="<?=$row->product_id?>" data-maxunit="<?=$row->max_unit_buy?>" style="" href="javascript:void(0)">add to cart</a>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          <?php 
            }else{
            ?>
            <center>
              <img src="<?=base_url('assets/images/empty-wishlist.png')?>" style="opacity: .5">
              <br/>
              <a href="<?=base_url('/')?>"><img src="<?=base_url('assets/images/continue-shopping-button.png')?>"></a>
            </center>
            <?php
          } ?>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
  $(".btn_cart").click(function(e) {
    e.preventDefault();
    var btn = $(this);
    var _id=$(this).data("id");
    var _maxunit=$(this).data("maxunit");

    href = '<?=base_url()?>site/cart_action';

    $.ajax({
      url:href,
      data: {"product_id": _id},
      type:'post',
      success:function(res){
          if(res=='login_now'){
            window.location.href = "<?php echo site_url('login-register'); ?>";
          }
          else{

            var obj = $.parseJSON(res); 

            $("#cartModel .modal-body").html(obj.html_code);
            $("#cartModel").modal("show");

            $('.radio-group .radio_btn').click(function(){
                $(this).parent().find('.radio_btn').removeClass('selected');
                $(this).addClass('selected');
                var val = $(this).attr('data-value');
                $(this).parent().find('input').val(val);
            });

            $("input[name='product_qty']").blur(function(){
              if($(this).val() <= 0){
                $(this).val(1);
              }
              else if($(this).val() > _maxunit){
                alert("You cannot buy more than "+_maxunit+" items in single order !!!");
                $(this).val(_maxunit);
              }
            });
          }
        },
        error : function(res) {
            alert("error");
        }

    });
  });

  $(".btn_remove_wishlist").click(function(e) {
      e.preventDefault();

      var _id=$(this).data("id");

      swal({
          title: "Are you sure?",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger btn_edit",
          cancelButtonClass: "btn-warning btn_edit",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: false,
          showLoaderOnConfirm: true
        },
        function(isConfirm) {
          if (isConfirm) {
            var btn = $(this);
            
            href = '<?=base_url()?>site/wishlist_action';
            
            $.ajax({
              url:href,
              data: {product_id: _id},
              type:'post',
              success:function(res){
                  if(res=='login_now'){
                    window.location.href = "<?php echo site_url('login-register'); ?>";
                  }
                  else{
                    location.reload();
                  }
                },
                error : function(res) {
                    alert("error");
                }
            });
          }else{
            swal.close();
          }
      });

    });

</script>
