<?php 
  //echo "aBc123";
	//add_js(array('assets/site_assets/js/jquery.countdown.min.js'));
  $ci =& get_instance();
?>
<div class="page-content">
<!-- start slider container -->
<section class="slider-area mb-60">
  <div class="slider-wrapper theme-default"> 
    <div id="slider" class="nivoSlider"> 
      <?php 
        $i=0;
        foreach ($banner_list as $key => $row) 
        {
          $img_file=base_url().$ci->_create_thumbnail('assets/images/banner/',$row->banner_slug,$row->banner_image,1920,600);
      ?>
        <a href="<?=base_url('banners/'.$row->banner_slug)?>">
          <img src="<?=$img_file?>" class="banner_img" alt="" title="#<?=$row->id?>"/>
        </a>
      <?php 
        }
      ?>
    </div>
  </div>
</section>
<!-- end slider container -->

<!-- categories container -->
<section class="mb-0">
  <div class="container">
    <div class="row">
      <div class="col-md-12"> 
        <div class="section-title1-border">
          <div class="section-title1">
            <h3>Categories</h3>
          </div>
          <?php 
          if(count($category_list) > 6){
            echo '<div class="category_view_all" style="right: 100px"><a href="'.base_url('/category').'">View All</a></div>';
          }
        ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="category-slider mb-30 owl-carousel">
      <?php 
        $i=0;
        
        foreach ($category_list as $key => $row) 
        {
          $img_file=base_url().$ci->_create_thumbnail('assets/images/category/',$row->category_slug,$row->category_image,300,180);

          $counts=$ci->getCount('tbl_sub_category', array('category_id' => $row->id, 'status' => '1'));

          if($counts > 0)
          {
            $url=base_url('category/'.$row->category_slug);  
          }
          else{
            $url=base_url('category/products/'.$row->id);
          }
      ?>
      <div class="col-md-12 item-col">
        <div class="single-offer">
          <div class="all_categori_list img-full"> 
            <a href="<?=$url?>"> 
              <img src="<?=$img_file?>" alt="" style="height: auto">  
              <span>
                <?php 
                  if(strlen($row->category_name) > 30){
                    echo substr(stripslashes($row->category_name), 0, 30).'...';  
                  }else{
                    echo $row->category_name;
                  }
                ?>
              </span>
            </a>
          </div>
        </div>
      </div>   
      <?php } ?>  
      </div> 
    </div>
  </div>
</section>
<!-- end categories container -->

<!-- offer container -->
<div class="offer-area mb-30">
  <div class="container">
    <div class="row">
      <div class="col-md-12"> 
        <div class="section-title1-border">
          <div class="section-title1">
            <h3>Offers</h3>
          </div>
          <?php 
            if(count($offers_list) > 4){
              echo '<div class="category_view_all" style="right: 100px"><a href="'.base_url('/offers').'">View All</a></div>';
            }
          ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="product-offers mb-30 owl-carousel">
        <?php 
          $i=0;
          foreach ($offers_list as $key => $row) 
          {
            $img_offer=base_url().$ci->_create_thumbnail('assets/images/offers/',$row->offer_slug,$row->offer_image,370,210);
        ?>
        <div class="col-md-12 item-col">
          <div class="single-offer">
            <div class="offer-img img-full"> <a href="<?=base_url('offers/'.$row->offer_slug)?>"> <img src="<?=$img_offer?>" alt="" style="height: auto"> </a> </div>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>

<?php 
  if(!empty($todays_deal))
  {
?>
  <section class="hot-deal-product-of-the-day mb-60">
    <div class="container">
      <div class="row">
        <div class="col-md-12"> 
          <div class="section-title1-border">
            <div class="section-title1">
              <h3>Hot Deal of the day</h3>
            </div>
            <?php 
              if(count($todays_deal) > 2){
                echo '<div class="category_view_all" style="right: 100px"><a href="'.base_url('/todays-deals').'">View All</a></div>';
              }
            ?>
           
          </div>
        </div>
      </div>
      <div class="row">
        <div class="hot-deal-of-product owl-carousel">
          <?php 
            

            foreach ($todays_deal as $key => $row) {

              $db_date=date('Y-m-d',$row->today_deal_date);

              $img_file=$ci->_create_thumbnail('assets/images/products/',$row->product_slug,$row->featured_image,250,250);

              if($row->featured_image2=='')
              { 
                $img_file2=$img_file;
              }
              else{
                $img_file2=$ci->_create_thumbnail('assets/images/products/',$row->product_slug,$row->featured_image2,251,251);
              }

              $next_day=date('Y/m/d', strtotime($db_date .' +1 day'));

          ?> 
          <div class="col-md-12">
            <div class="single-product hot-deal-list">
              <div class="product-img"> <a href="<?php echo site_url('product/'.$row->product_slug); ?>" title="<?=$row->product_title?>" taget="_blank"> <img class="first-img" src="<?=base_url().$img_file?>" alt="" style="height: 250px;width: 250px"> <img class="hover-img" src="<?=base_url().$img_file2?>" alt="" style="height: 250px;width: 250px"> </a> </div>
              <div class="product-content">
                <h2 class="pro-title">
                  <a href="<?php echo site_url('product/'.$row->product_slug); ?>" title="<?=$row->product_title?>" taget="_blank">
                    <?php 
                      if(strlen($row->product_title) > 30){
                        echo substr(stripslashes($row->product_title), 0, 30).'...';  
                      }else{
                        echo $row->product_title;
                      }
                    ?>
                  </a>
                </h2>
                <div class="pro-rating-price">
                  <div class="rating">
                    <?php 
                      for ($x = 0; $x < 5; $x++) { 
                        if($x < $row->rate_avg){
                          ?>
                          <i class="fa fa-star" style="color: #F9BA48"></i>
                          <?php  
                        }
                        else{
                          ?>
                          <i class="fa fa-star"></i>
                          <?php
                        }
                        
                      }
                    ?>
                  </div>
                  <div class="product-price"> 
                    <?php 
                      if($row->you_save_amt!='0'){
                        ?>
                        <span class="new-price"><?=CURRENCY_CODE.' '.$row->selling_price?></span> 
                        <span class="old-price"><?=CURRENCY_CODE.' '.$row->product_mrp;?></span>
                        <?php
                      }
                      else{
                        ?>
                        <span class="new-price"><?=CURRENCY_CODE.' '.$row->product_mrp;?></span>
                        <?php
                        
                      }
                    ?>
                  </div>
                  <div class="hot-deal-product-des">
                    <p>
                      <?php
                        if(strlen($row->product_desc) > 100){
                            echo substr(stripslashes($row->product_desc), 0, 100);  
                        }else{
                            echo $row->product_desc;
                        }
                      ?>
                    </p>
                  </div>
                  <div class="count-down-box">
                    <div class="count-box">
                      <div class="pro-countdown" data-countdown="<?=$next_day?>"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </section>
<?php } ?>
<!--
<?php
  // for home page ads 
  //if($this->db->get_where('tbl_web_settings', array('id' => '1'))->row()->home_ad=='true')
  //{
?>
<div class="offer-area mb-60">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="single-offer">
          <div class="offer-img img-full">
            <?php 
            //  echo $this->db->get_where('tbl_web_settings', array('id' => '1'))->row()->home_banner_ad;
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php //} ?>
  -->
<?php
  // show home categories 
  if(!empty($home_categories))
  {
    foreach ($home_categories as $key1 => $row) {
      $sub_categories=$ci->get_sub_category($row->id);
      ?>
      <section class="bestseller-product mb-30">
        <div class="container">
          <div class="row">
            <div class="col-md-12"> 
              <div class="section-title1-border">
                <div class="section-title1">
                  <h3><?=$row->category_name?></h3>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12"> 
              <div class="product-tab-menu-area">
                <div class="product-tab">
                  <ul>
                    <?php
                      $is_active=true;
                      $limit=3;
                      $i=1;
                      foreach ($sub_categories as $key => $value) {

                        $counts=$ci->getCount('tbl_product', array('sub_category_id' => $value->id, 'status' => '1'));

                        if($counts <= 0)
                        {
                          continue;
                        }


                        if($i <= $limit){
                          if($is_active){
                              $is_active=false;
                              echo '<li class="active"><a data-toggle="tab" href="#'.$value->sub_category_slug.'">'.$value->sub_category_name.'</a></li>';
                          }
                          else{
                            echo '<li><a data-toggle="tab" href="#'.$value->sub_category_slug.'">'.$value->sub_category_name.'</a></li>';
                          }
                        }
                        else{
                          break;
                        }

                        $i++;
                      }

                    ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <div class="tab-content">
            <?php
              $is_active=true;
              $limit=10;

              $i=1;
              
              foreach ($sub_categories as $key => $value) {

                $counts=$ci->getCount('tbl_product', array('sub_category_id' => $value->id, 'status' => '1'));

                if($counts <= 0)
                {
                  continue;
                }

                $class='';
                if($i <= $limit){

                  if($is_active){
                    $is_active=false;
                    $class=' active in';
                  }

                  ?>
                  <div id="<?=$value->sub_category_slug?>" class="tab-pane fade<?=$class?>">
                    <div class="row">
                      <div class="bestseller-product3 mb-30 owl-carousel"> 

                        <?php 
                          $products=$ci->get_cat_sub_product($row->id, $value->id);
                        ?>
                        
                          <?php 
                            foreach ($products as $key => $product_row) {

                              $img_file=$ci->_create_thumbnail('assets/images/products/',$product_row->product_slug,$product_row->featured_image,210,210);

                              $img_file2=$ci->_create_thumbnail('assets/images/products/',$product_row->id,$product_row->featured_image2,210,210);

                          ?>
                            <div class="col-md-12 item-col">     
                            <div class="single-product3">
                              <div class="product-img"> <a href="<?php echo site_url('product/'.$product_row->product_slug); ?>" title="<?=$product_row->product_title?>" target="_blank"> <img class="first-img" src="<?=base_url().$img_file?>"> <img class="hover-img" src="<?=base_url().$img_file2?>"> </a>
                                <ul class="product-action">
                                  
                                  <?php 
                                    if( 1 == 2 /*isUserLoggedIn() && $ci->is_favorite($this->session->userdata('GSM_FUS_UserId'), $product_row->id)*/ ){
                                      ?>
                                      <li><a href="" class="btn_wishlist" data-id="<?=$product_row->id?>" data-toggle="tooltip" title="Remove to Wishlist" style="background-color: #ff5252"><i class="ion-android-favorite-outline"></i></a></li>
                                      <?php
                                    }
                                    else if($ci->check_cart($product_row->id,$this->session->userdata('GSM_FUS_UserId'))){
                                      ?>
                                      <li><a href="javascript:void(0)" data-toggle="tooltip" title="Already in Cart"><i class="ion-android-favorite-outline"></i></a></li>
                                      <?php
                                    } 
                                    else if(1 == 2){
                                      ?>
                                      <li><a href="" class="btn_wishlist" data-id="<?=$product_row->id?>" data-toggle="tooltip" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                      <?php
                                    } 
                                  ?>

                                  <li><a href="" class="btn_quick_view" data-id="<?=$product_row->id?>" title="Quick View"><i class="ion-android-expand"></i></a></li>

                                </ul>
                              </div>
                              <div class="product-content">
                                <h2>
                                  <a href="<?php echo site_url('product/'.$product_row->product_slug); ?>" target="_blank">
                                    <?php 
                                      if(strlen($product_row->product_title) > 20){
                                        echo substr(stripslashes($product_row->product_title), 0, 20).'...';  
                                      }else{
                                        echo $product_row->product_title;
                                      }
                                    ?>
                                  </a>
                                </h2>
                                <div class="rating"> 
                                  <?php 
                                    for ($x = 0; $x < 5; $x++) { 
                                      if($x < $product_row->rate_avg){
                                        ?>
                                        <i class="fa fa-star" style="color: #F9BA48"></i>
                                        <?php  
                                      }
                                      else{
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <?php
                                      }
                                    }
                                  ?>
                                </div>
                                <div class="product-price"> 
                                  <?php 
                                    if($product_row->you_save_amt!='0'){
                                      ?>
                                      <span class="new-price"><?=CURRENCY_CODE.' '.$product_row->selling_price?></span> 
                                      <span class="old-price"><?=CURRENCY_CODE.' '.$product_row->product_mrp;?></span>
                                      <?php
                                    }
                                    else{
                                      ?>
                                      <span class="new-price"><?=CURRENCY_CODE.' '.$product_row->product_mrp;?></span>
                                      <?php
                                      
                                    }
                                  ?>

                                  <?php
                                    $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';
                                    if(!$ci->check_cart($product_row->id,$user_id)){
                                      ?>
                                      <a class="button add-btn btn_cart" data-id="<?=$product_row->id?>" data-maxunit="<?=$product_row->max_unit_buy?>" style="" href="javascript:void(0)" data-toggle="tooltip" title="Add to Cart">add to cart</a>
                                      <?php
                                    }
                                    else{
                                      $cart_id=$ci->get_single_info(array('product_id' => $product_row->id, 'user_id' => $user_id),'id','tbl_cart');
                                      ?>
                                      <a class="button add-btn btn_remove_cart" style="" href="<?php echo site_url('remove-to-cart/'.$cart_id); ?>" data-toggle="tooltip" title="Remove to Cart">remove to cart</a>
                                      <?php
                                    }
                                  ?>
                                </div>
                              </div>
                            </div>
                            </div>
                          <?php } ?>
                      </div>
                    </div>
                  </div>
                  <?php
                }
                else{
                  break;
                }

                $i++;
              }
            ?>
          </div>
        </div>  
      </section> 
      <?php  
    }
  }
?>

<?php 
  // show recently viewed items
  if($this->session->userdata('product_id')!='')
  {
?>
<section class="bestseller-product mb-30">
  <div class="container">
    <div class="row">
      <div class="col-md-12"> 
        <div class="section-title1-border">
          <div class="section-title1">
            <h3>Recently Viewed Products</h3>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="recently-products mb-30 owl-carousel"> 

        <?php 

          $row=$ci->get_products();
          foreach ($row as $key => $product_row) {

            $img_file=$ci->_create_thumbnail('assets/images/products/',$product_row->product_slug,$product_row->featured_image,210,210);

            $img_file2=$ci->_create_thumbnail('assets/images/products/',$product_row->id,$product_row->featured_image2,210,210);

        ?>
        <div class="col-md-12 item-col">     
          <div class="single-product3">
            <div class="product-img"> <a href="<?php echo site_url('product/'.$product_row->product_slug); ?>" title="<?=$product_row->product_title?>" taget="_blank"> <img class="first-img" src="<?=base_url().$img_file?>"> <img class="hover-img" src="<?=base_url().$img_file2?>"> </a>
              <ul class="product-action">
                
                <?php 
                  if(check_user_login() && $ci->is_favorite($this->session->userdata('GSM_FUS_UserId'), $product_row->id)){
                    ?>
                    <li><a href="" class="btn_wishlist" data-id="<?=$product_row->id?>" data-toggle="tooltip" title="Remove to Wishlist" style="background-color: #ff5252"><i class="ion-android-favorite-outline"></i></a></li>
                    <?php
                  }
                  else if($ci->check_cart($product_row->id,$this->session->userdata('GSM_FUS_UserId'))){
                    ?>
                    <li><a href="javascript:void(0)" data-toggle="tooltip" title="Already in Cart"><i class="ion-android-favorite-outline"></i></a></li>
                    <?php
                  } 
                  else{
                    ?>
                    <li><a href="" class="btn_wishlist" data-id="<?=$product_row->id?>" data-toggle="tooltip" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                    <?php
                  } 
                ?>

                <li><a href="" class="btn_quick_view" data-id="<?=$product_row->id?>" title="Quick View"><i class="ion-android-expand"></i></a></li>

              </ul>
            </div>
            <div class="product-content">
              <h2>
                <a href="<?php echo site_url('product/'.$product_row->product_slug); ?>">
                  <?php 
                    if(strlen($product_row->product_title) > 20){
                      echo substr(stripslashes($product_row->product_title), 0, 20).'...';  
                    }else{
                      echo $product_row->product_title;
                    }
                  ?>
                </a>
              </h2>
              <div class="rating"> 
                <?php 
                  for ($x = 0; $x < 5; $x++) { 
                    if($x < $product_row->rate_avg){
                      ?>
                      <i class="fa fa-star" style="color: #F9BA48"></i>
                      <?php  
                    }
                    else{
                      ?>
                      <i class="fa fa-star"></i>
                      <?php
                    }
                  }
                ?>
              </div>
              <div class="product-price"> 
                <?php 
                  if($product_row->you_save_amt!='0'){
                    ?>
                    <span class="new-price"><?=CURRENCY_CODE.' '.$product_row->selling_price?></span> 
                    <span class="old-price"><?=CURRENCY_CODE.' '.$product_row->product_mrp;?></span>
                    <?php
                  }
                  else{
                    ?>
                    <span class="new-price"><?=CURRENCY_CODE.' '.$product_row->product_mrp;?></span>
                    <?php
                    
                  }
                ?>
                <?php
                  $user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';
                  if(!$ci->check_cart($product_row->id,$user_id)){
                    ?>
                    <a class="button add-btn btn_cart" data-id="<?=$product_row->id?>" data-maxunit="<?=$product_row->max_unit_buy?>" style="" href="javascript:void(0)" data-toggle="tooltip" title="Add to Cart">add to cart</a>
                    <?php
                  }
                  else{
                    $cart_id=$ci->get_single_info(array('product_id' => $product_row->id, 'user_id' => $user_id),'id','tbl_cart');
                    ?>
                    <a class="button add-btn btn_remove_cart" style="" href="<?php echo site_url('remove-to-cart/'.$cart_id); ?>" data-toggle="tooltip" title="Remove to Cart">remove to cart</a>
                    <?php
                  }
                ?>
              </div>
            </div>
          </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</section>
<?php } ?>
</div>
