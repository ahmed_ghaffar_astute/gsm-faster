<?php 
  	$this->load->view('site/layout/breadcrumb');

    if($this->session->flashdata('response_msg')) {
      $message = $this->session->flashdata('response_msg');
    }
    else{
      $message='';
    }
?>
<section class="my-account-area mt-20 mb-20">
<div class="container">
  <div class="row">
    <div class="col-md-6 col-sm-6">
      <div class="customer-login-register">
        <div class="form-login-title">
          <h2>Login</h2>
        </div>

        <div class="login-form">
          <form action="<?php echo site_url('site/login'); ?>" id="login_form" method="post">
          	<input type="hidden" name="preview_url" value="<?php if(isset($_SERVER['HTTP_REFERER']) && $this->session->userdata('single_pre_url')==''){ echo str_replace(base_url().'site/register','',$_SERVER['HTTP_REFERER']);}else { echo $this->session->userdata('single_pre_url');session_destroy(); }?>">
            <div class="form-fild">
              <p>
                <label>Email <span class="required">*</span></label>
              </p>
              <input type="email" name="email" value="" autocomplete="off" placeholder="example@example.com">
              <p style="color: red;display: none"><i class="fa fa-exclamation-circle"></i> <span>Email is required !!!</span></p>
            </div>
            <div class="form-fild">
              <p>
                <label>Password <span class="required">*</span></label>
              </p>
              <input type="password" name="password" value="" autocomplete="off" placeholder="************">
              <p style="color: red;display: none"><i class="fa fa-exclamation-circle"></i> <span>Password is required !!!</span></p>
            </div>
            <div class="login-submit">
              <!-- <label style="margin-left: 0px">
                <input class="checkbox" style="width: 13px;height: 13px" type="checkbox" name="rememberme" value="1">
                <span>Remember me</span> 
              </label> -->
              <div class="clearfix"></div>
              <button type="submit" class="form-button">Login</button>
              
            </div>
            <div class="lost-password"> <a href="" class="lost_password" data-toggle="modal" data-target="#lostPassword" data-backdrop="static" data-keyboard="false">Lost your password?</a> </div>
          </form>
        </div>
      </div>
    </div>

    <div class="col-md-6 col-sm-6">

      <div class="customer-login-register register-pt-0">
        <div class="form-register-title">
          <h2>Register</h2>
        </div>
        <div class="register-form">

          <form action="<?php echo site_url('site/register'); ?>" id="registerForm" method="post">
            <input type="hidden" name="preview_url" value="<?php if(isset($_SERVER['HTTP_REFERER'])){ echo $_SERVER['HTTP_REFERER']; }?>">
            <div class="step_1">
              <div class="form-fild">
                <p>
                  <label>Name <span class="required">*</span></label>
                </p>
                <input type="text" name="user_name" value="" placeholder="Enter your name">
                <p style="color: red;display: none"><i class="fa fa-exclamation-circle"></i> <span>Name is required !!!</span></p>
              </div>
              <div class="form-fild">
                <p>
                  <label>Email <span class="required">*</span></label>
                </p>
                <input type="email" name="user_email" value="" placeholder="Enter your email">
                <p style="color: red;display: none"><i class="fa fa-exclamation-circle"></i> <span>Email is required !!!</span></p>
              </div>
              <div class="form-fild">
                <p>
                  <label>Password <span class="required">*</span></label>
                </p>
                <input type="password" name="user_password" autocomplete="off" value="" placeholder="********">
                <p style="color: red;display: none"><i class="fa fa-exclamation-circle"></i> <span>Password is required !!!</span></p>
              </div>
              <div class="form-fild">
                <p>
                  <label>Confirm Password <span class="required">*</span></label>
                </p>
                <input type="password" name="c_password" autocomplete="off" value="" placeholder="********">
                <p style="color: red;display: none"><i class="fa fa-exclamation-circle"></i> <span>Confirm Password is required !!!</span></p>
              </div>
              <div class="form-fild">
                <p>
                  <label>Phone no. <span class="required">*</span></label>
                </p>
                <input type="text" name="user_phone" value="" placeholder="Enter your phone no." onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="10">
                <p style="color: red;display: none"><i class="fa fa-exclamation-circle"></i> <span>Phone no. is required !!!</span></p>
              </div>
              <div class="register-submit">
                <button type="button" class="form-button step_1_btn">Submit</button>
              </div>
            </div>
            <div class="step_2" style="display: none;">
              <div class="form-fild">
                <p class="text-center" style="color: red;font-size: 16px;font-weight: 400">We have send one time password on your email </p>
                <p>
                  <label>Enter Code <span class="required">*</span></label>
                </p>
                <input type="text" name="email_sent_code" value="" placeholder="Enter code">
                <p style="color: red;display: none"><i class="fa fa-exclamation-circle"></i> <span>Verify Code is invalid !!!</span></p>


              </div>
              <div class="register-submit">

                <button type="button" class="form-button btn_resend" disabled="true" style="background-color: #bbb">Resend</button><span>&nbsp;&nbsp;&nbsp;&nbsp;Wait <span id="countdown">60</span> Seconds</span>
                <div class="clearfix"></div>
                <br/>
                <button type="button" class="form-button btn_back">Back</button>
                
                <button type="submit" class="form-button step_2_btn" name="btn_register">Register</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</section>


<div id="lostPassword" class="modal fade" role="dialog" style="z-index: 9999999">
  <div class="modal-dialog modal-sm"> 
    <div class="modal-content">
      <div class="modal-header" style="padding: 15px 20px">
        <h3>Forgot Password</h3>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" style="padding-top: 10px">
        <div class="modal-details">
          <form action="<?=base_url('site/forgot_password')?>" method="post" id="lost_password_form">
            <p class="err_password" style="color: red;display: none"></p>
            <div class="form-fild">
              <p>
                <label>Registered Email <span class="required">*</span></label>
              </p>
              <input type="email" name="registered_email" value="" placeholder="example@example.com">
              <p style="color: red;display: none"><i class="fa fa-exclamation-circle"></i> <span>Email is required !!!</span></p>
            </div>
            <div class="login-submit">
              <button type="submit" class="form-button">Reset Now !</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  $('#lostPassword').on('hidden.bs.modal', function () {
    $("input[name='registered_email']").val('');
    $("input").next("p").fadeOut();
    $(".err_password").hide();
  })

	$("#login_form").submit(function (e) {

    e.preventDefault();
    $(".process_loader").show();

		var inputs = $("#login_form :input[type='email'],#login_form :input[type='password']");

		var counts=0;

		inputs.each(function(){
			if($(this).val()==''){
				$(this).next("p").fadeIn();
				counts++;
        $(".process_loader").hide();
			}
			else{
				$(this).next("p").fadeOut();
			}
		});

		if(counts==0){
      $.ajax({
        url:$(this).attr("action"),
        data: $(this).serialize(),
        type:'post',
        success:function(data){
          var obj = $.parseJSON(atob(data));
          if(obj.status=='1'){
            $(".process_loader").hide();
            window.location.href=obj.preview_url;
          }
          else{
            $(".process_loader").hide();
            swal(obj.message);
          }
          
        },
        error : function(data) {
          alert("error");
        }
      });
    }
	});

  function resendOTP() {
      var count = document.getElementById('countdown');
      timeoutfn = function(){

        if(parseInt(count.innerHTML) <= 0){
          clearInterval(this);

          $('.btn_resend').removeAttr("style");
          $('.btn_resend').attr("disabled", false);
          $("#countdown").parent("span").hide();

        }
        else{
          count.innerHTML = parseInt(count.innerHTML) - 1;
          setTimeout(timeoutfn, 1000);
        }
      };

      setTimeout(timeoutfn, 1000);
  }

  $(".btn_resend").click(function(e){
    e.preventDefault();

    $(this).attr("disabled", true);
    $(this).css("background-color", "#bbb");

    var _email=$("input[name='user_email']").val();

    href = '<?=base_url()?>site/sent_code';

    $(".process_loader").show();

    $.ajax({
      url:href,
      data: {"email": _email},
      type:'post',
      success:function(res){
        $(".process_loader").hide();
        swal("Verification code is successfully resent to your email...");
        $("#countdown").html("60");
        $("#countdown").parent("span").show();
        resendOTP();
      }

    });

  });


  $(".step_1_btn").click(function(e){

    var btn=$(this);

    var inputs = $(".step_1").find("input");

    var counts=0;

    inputs.each(function(){
      if($(this).val()==''){
        $(this).next("p").fadeIn();
        counts++;
      }
      else{
        $(this).next("p").fadeOut();
      }
    });

    var email=$(".step_1 :input[name='user_email']").val();
    var password=$(".step_1 :input[name='user_password']").val();
    var cpassword=$(".step_1 :input[name='c_password']").val();

    if(password!=cpassword){
        $(".step_1 :input[name='c_password']").next("p").find("span").text("Password and confirm password must be same !!!");
        $(".step_1 :input[name='c_password']").next("p").fadeIn();
        counts++;
    }

    if(counts==0)
    {

      $(".process_loader").show();

      href = '<?=base_url()?>site/check_email';
      $.ajax({
        url:href,
        data: {"email": email},
        type:'post',
        success:function(res){
          if($.trim(res)=="false"){

            $(".process_loader").hide();

            $(".step_1 :input[name='user_email']").next("p").find("span").text("Email is already registered !!!");
            $(".step_1 :input[name='user_email']").next("p").fadeIn();
          }
          else{

              btn.attr("disabled", true);

              href = '<?=base_url()?>site/sent_code';

              $.ajax({
                url:href,
                data: {"email": email},
                type:'post',
                success:function(res1){
                  $(".process_loader").hide();
                  swal("Verification code is successfully sent to your email...");
                  $(".step_1").slideUp();
                  $(".step_2").slideDown();

                  resendOTP();
                }

              });
          }
        },
        error : function(res) {
            alert("error");
        }
      });
    }

  });

  $(".btn_back").click(function(e){

    swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger btn_edit",
        cancelButtonClass: "btn-warning btn_edit",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: false,
        showLoaderOnConfirm: true
      },
      function(isConfirm) {
        if (isConfirm) {
            $(".step_2").slideUp();
            $(".step_1").slideDown();
        }else{
          swal.close();
        }
    });
    
  });

  $(".step_2_btn").click(function(e){
      e.preventDefault();

      var email=$(".step_1 :input[name='user_email']").val();
      var code=$(".step_2 :input[name='email_sent_code']").val();

      href = '<?=base_url()?>site/verify_code';

      $.ajax({
        url:href,
        data: {email: email,code: code},
        type:'post',
        success:function(res){
          if($.trim(res)=='true'){
            $("#registerForm").submit();
          }
          else{
            $(".step_2 :input[name='email_sent_code']").next("p").show();
          }
        },
        error : function(res) {
          alert("error");
        }
      });
  });



  $("#lost_password_form").submit(function(e){
    e.preventDefault();
    $(".err_password").hide();
    var _btn=$(this).find("button");

    _btn.text("Please wait...");

    _btn.attr("disabled", true);

    var formData = new FormData($(this)[0]);

    $.ajax({
        url:$(this).attr("action"),
        processData: false,
        contentType: false,
        type: 'POST',
        data: formData,
        success: function(data){
          var obj = $.parseJSON(data);
          if(obj.success=='1'){
            location.reload();
          }
          else if(obj.success=='0'){
            _btn.attr("disabled", false);
            _btn.text("Reset Now !");
            $(".err_password").html('<i class="fa fa-exclamation-circle"></i> '+obj.message+'<span></span>').fadeIn();
          }
          else{
            _btn.attr("disabled", false);
            _btn.text("Reset Now !");
            $(".err_password").html('<i class="fa fa-exclamation-circle"></i> Something going to wrong....<span></span>').fadeIn();
          }
      },
      error : function(res) {
        alert("error");
      }
    });


  });

</script>

<?php
  if($this->session->flashdata('response_msg')) {
    $message = $this->session->flashdata('response_msg');
    ?>
      <script type="text/javascript">
        var _msg='<?=$message['message']?>';
        var _class='<?=$message['class']?>';

        $('.notifyjs-corner').empty();
        $.notify(
          _msg, 
          { position:"top right",className: _class }
        ); 
      </script>
    <?php
  }
?>