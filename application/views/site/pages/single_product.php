<?php 
	
  	$this->load->view('site/layout/breadcrumb');

  	add_css(array('assets/site_assets/css/nivo-slider.css'));
  	add_js(array('assets/site_assets/js/jquery.nivo.slider.js'));

	$ci =& get_instance();

	$single_pre_url=current_url();

	$this->session->set_userdata(array('single_pre_url' => $single_pre_url));

	$user_id=$this->session->userdata('GSM_FUS_UserId') ? $this->session->userdata('GSM_FUS_UserId'):'0';

  	$img_file=$ci->_create_thumbnail('assets/images/products/',$product->product_slug,$product->featured_image,600,600);

  	$img_file_sm=$ci->_create_thumbnail('assets/images/products/',$product->product_slug,$product->featured_image,200,200);

  	$full_img='<div id="'.$product->product_slug.'" class="tab-pane fade in active">
	            	<div> <a href="'.base_url().$img_file.'" class="lightbox"> <img src="'.base_url().$img_file.'" alt=""> </a> </div>
	          	</div>';

	$thumb_img='<a data-toggle="tab" href="#'.$product->product_slug.'"><img src="'.base_url().$img_file_sm.'" alt=""></a> ';

	$where = array('parent_id' => $product->id,'type' => 'product');

	$row_img=$ci->common_model->selectByids($where,'tbl_product_images');

	foreach ($row_img as $key => $value) {
		$img_big=$ci->_create_thumbnail('assets/images/products/gallery/',$value->id,$value->image_file,600,600);

		$img_small=$ci->_create_thumbnail('assets/images/products/gallery/',$value->id,$value->image_file,200,200);

		$full_img.='<div id="'.$value->id.'" class="tab-pane fade">
	            		<div> <a href="'.base_url().$img_big.'" class="lightbox"> <img src="'.base_url().$img_big.'" alt=""> </a> </div>
	          		</div>';

		$thumb_img.='<a data-toggle="tab" href="#'.$value->id.'"><img src="'.base_url().$img_small.'" alt=""></a> ';
	}

	$size=$selected_size=$size_view='';
	if($product->product_size !=''){

        $i=1;
        foreach (explode(',', $product->product_size) as $key => $value) {

            $class='radio_btn';

            if($ci->check_cart($product->id,$this->session->userdata('GSM_FUS_UserId'))){

        		$cart_size=$ci->get_single_info(array('product_id' => $product->id, 'user_id' => $this->session->userdata('GSM_FUS_UserId')),'product_size','tbl_cart');

	        	
	        	if($cart_size==$value){
	        		$class='radio_btn selected';
	        	}
	        	else{
	        		$class='radio_btn';
	        	}
	        }
	        else{
	        	if($i==1){
	        		$class='radio_btn selected';
	        	}
	        	else{
	        		$class='radio_btn';
	        	}
	        }

            if($i==1){
                $selected_size=$value;
                $size.='<div class="'.$class.'" data-value="'.$value.'">'.$value.'</div>';
                $i=0;
            }
            else{
                $size.='<div class="'.$class.'" data-value="'.$value.'">'.$value.'</div>';
            }
        }

        $size_chart=($product->size_chart!='') ? base_url('assets/images/products/'.$product->size_chart) : "";

        
    	$size_view.='<p style="font-weight: 600">Select Size: </p>
                  <div class="radio-group" style="margin-bottom:10px">
                    '.$size.'
                    <br/>
                    <input type="hidden" id="radio-value" name="product_size" value="'.$selected_size.'" />
                    
                  </div><a href="" class="size_chart" data-img="'.$size_chart.'" style="font-size:14px;;font-weight:600"><img src="'.base_url('assets/images/size_chart.png').'" style="width:20px;height:20px;"> See Size Chart</a><br/><br/>';

    }

?>

<link rel="stylesheet" type="text/css" href="<?=base_url('assets/site_assets/js/baguettebox/baguetteBox.min.css')?>">

<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>

<style type="text/css">
.morecontent span {
	display: none;
}
.morelink {
	display: block;
}
#baguetteBox-overlay{
	background:rgba(0, 0, 0, 0.9) !important;
}
</style>

<section class="single-product-area mt-20">
	<div class="container"> 
	  <div class="row">
	    <div class="single-product-info mb-50"> 
	      <div class="col-md-5 col-sm-5 text-center tz-gallery"> 
	        <div class="single-product-tab-content tab-content" style="overflow: hidden;">
	          <?=$full_img?>
	        </div>
	        <div class="single-product-tab">
	          <div class="single-product-tab-menu owl-carousel"> 
	          	<?=$thumb_img?>
	          </div>
	        </div>
	      </div>
	      <div class="col-md-7 col-sm-7">
	        <div class="single-product-content"> 
	          <h1 class="product-title"><?=$product->product_title?></h1>
	          <div class="product-rating">
	          	<?php 
	                for ($x = 0; $x < 5; $x++) { 
	                  if($x < $product->rate_avg){
	                    ?>
	                    <i class="fa fa-star"></i>
	                    <?php  
	                  }
	                  else{
	                    ?>
	                    <i class="fa fa-star" style="color: #7a7a7a"></i>
	                    <?php
	                  }
	                }
	              ?>
	              <?php 
	              	if(count($product_rating) > 0){
	              		echo '<a class="review-link" href="#review">('.count($product_rating).' customer review)</a> ';
	              	}
	              ?>
	          	
	          </div>
	          <div class="single-product-price"> 
	          	<?php 
					if($product->you_save_amt!='0'){
						?>
						<span class="new-price"><?=CURRENCY_CODE.' '.$product->selling_price?></span> 
						<span class="old-price"><?=CURRENCY_CODE.' '.$product->product_mrp;?></span>
						<?php
					}
					else{
						?>
						<span class="new-price"><?=CURRENCY_CODE.' '.$product->product_mrp;?></span>
						<?php
					}
				?>
	          </div>
	          <div class="product-description">
	          	<span class="more">
	            	<?=$product->product_desc?>
	            </span>
	          </div>
	          <br/>
	          
	          <form action="<?=base_url('site/add_to_cart')?>" method="post" id="cartForm">
			 <?php 
			 	//echo $size_view;
			 ?>
			 <div class="single-product-quantity">
	              <div class="quantity">
	                <label>Quantity</label>
	                <input type="hidden" name="max_unit_buy" value="<?=$product->max_unit_buy ? $product->max_unit_buy: '1'?>" class="max_unit_buy">
	                <input type="hidden" name="preview_url" value="<?=current_url()?>">
                    <input type="hidden" name="product_id" value="<?=$product->id?>" />
	                <input class="input-text product_qty" name="product_qty" value="<?php if($ci->check_cart($product->id,$this->session->userdata('GSM_FUS_UserId'))){ echo $ci->get_single_info(array('product_id' => $product->id, 'user_id' => $this->session->userdata('GSM_FUS_UserId')),'product_qty','tbl_cart'); }else{ echo '1'; } ?>" type="number" min="1" max="<?=$product->max_unit_buy ? $product->max_unit_buy: '1'?>" onkeypress="return isNumberKey(event)">
	              </div>
	              <button class="quantity-button" type="submit"><?php if($ci->check_cart($product->id,$this->session->userdata('GSM_FUS_UserId'))){ echo 'Update to cart'; }else{ echo 'Add to Cart'; } ?></button>
	          </div>
	      	  </form>
	      	  <div class="single-product-quantity">
	          <?php 
	          	if($product->color!=''){
	          		$color_arr=explode('/', $product->color);
	                $color_name=$color_arr[0];
	                $color_code=$color_arr[1];

	                echo '<h4><strong>Colour:</strong> '.$color_name.'</h4>';	
	          	}

	          	if($product->other_color_product!=''){

	          		$img_color_sm=$ci->_create_thumbnail('assets/images/products/',$product->product_slug,$product->featured_image,80,80);

	          		?>
	          		<a href="<?php echo site_url('product/'.$product->product_slug); ?>" title="<?=$color_name?>" style="float:left;margin-right:10px;">
	          			<div class="text-center" style="width: 55px;margin-top: 10px">
	          				<div class="container-fluid" style="border: 3px solid #ff5252;padding: 0px;border-radius:40px;">
	          					<img src="<?=base_url().$img_color_sm?>" style="border-radius:40px;">		
	          				</div>
	          			</div>
          			</a>
	          		<?php

	          		$ids=explode(',', $product->other_color_product);

	          		foreach ($ids as $key => $value) {

	          			$product_slug=$ci->get_single_info(array('id' => $value),'product_slug','tbl_product');
	          			$featured_image=$ci->get_single_info(array('id' => $value),'featured_image','tbl_product');

	          			$img_color_sm=$ci->_create_thumbnail('assets/images/products/',$product_slug,$featured_image,80,80);

	          			$color_arr=explode('/', $ci->get_single_info(array('id' => $value),'color','tbl_product'));
		                $color_name=$color_arr[0];

	          			?>
	          			<a href="<?php echo site_url('product/'.$product_slug); ?>" title="<?=$color_name?>" style="float:left">
		          			<div class="text-center" style="width: 55px;margin-top: 10px">
		          				<div class="container-fluid" style="border: 3px solid #eee;padding: 0px;border-radius:40px;">
		          					<img src="<?=base_url().$img_color_sm?>" style="border-radius:40px;">		
		          				</div>
		          			</div>
	          			</a>
	          			<?php
	          		}
	          	}
	          ?>
	          	<div class="clearfix"></div>
	          </div>

	          <div class="single-product-sharing">
                <ul>
                	<li><h4><strong>Share:</strong></h4></li>
                  	<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?=current_url()?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                  	<li><a href="https://twitter.com/intent/tweet?text=<?=$page_title?>&amp;url=<?=current_url()?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                  	<li><a href="http://pinterest.com/pin/create/button/?url=<?=current_url()?>&media=<?=base_url().$img_file?>&description=<?=$page_title?>" target="_blank"><i class="fa fa-pinterest"></i></a></li>

                  	<li><a href="whatsapp://send?text=<?=current_url()?>" target="_blank" data-action="share/whatsapp/share"><i class="fa fa-whatsapp"></i></a></li>
                </ul>

              </div>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="row">
	    <div class="discription-tab">
	      <div class="col-md-12">
	        <div class="discription-review-contnet mb-40"> 
	          <div class="discription-tab-menu">
	            <ul>
	              <li class="active"><a data-toggle="tab" href="#description">Features</a></li>
	              <li><a data-toggle="tab" href="#review">Reviews (<?=count($product_rating)?>)</a></li>
	            </ul>
	          </div>
	          <div class="discription-tab-content tab-content">
	            <div id="description" class="tab-pane fade in active">
	              <div class="row">
	                <div class="col-md-12">
	                  <div class="description-content">
	                    <?=$product->product_features?>
	                  </div>
	                </div>
	              </div>
	            </div>
	            <div id="review" class="tab-pane fade">
	              <div class="row">
	                <div class="col-md-12">
	                  <div class="review-page-comment">
	                    <div class="review-comment">
	                      <?php 
	                      	if(!empty($product_rating)){
	                      	?>
	                      <h2><?=count($product_rating)?> Review for <?=$product->product_title?></h2>
	                      
	                      <ul>
	                      	<?php 
	                      		// print_r($product_rating);
	                      		define('IMG_PATH', base_url().'assets/images/users/');

	                      		$user_img='';

	                      		$i=1;
	                      		foreach ($product_rating as $key => $value) {

	                      			if($i > 1){
	                      				break;
	                      			}

	                      			$i++;

	                      			if($this->common_model->selectByidParam($value->user_id, 'tbl_users','user_image')!='' && file_exists('assets/images/users/'.$this->common_model->selectByidParam($value->user_id, 'tbl_users','user_image'))){
										$user_img=IMG_PATH.$this->common_model->selectByidParam($value->user_id, 'tbl_users','user_image');
									}
									else{
										$user_img=base_url('assets/images/2.png');
									}

									$row_review_img=$this->common_model->selectByids(array('parent_id' => $value->id, 'type' => 'review'), 'tbl_product_images');
	                      	?>
	                        <li>
	                          <div class="product-comment"> <img src="<?=$user_img?>" alt="" style="width: 60px;height: 60px;">
	                            <div class="product-comment-content">
	                              <p>
	                              	<strong><?=($value->user_id==$this->session->userdata('GSM_FUS_UserId')) ? 'You' : $value->user_name; ?></strong> - <span><?=date('M jS, Y',$value->created_at)?></span>
	                              	<span class="pro-comments-rating"> 
	                              		<?php 
			                              for ($x = 0; $x < 5; $x++) { 
			                                if($x < $value->rating){
			                                  ?>
			                                  <i class="fa fa-star" style="color: #F9BA48"></i>
			                                  <?php  
			                                }
			                                else{
			                                  ?>
			                                  <i class="fa fa-star"></i>
			                                  <?php
			                                }
			                              }
			                            ?>
			                       	</span> 
	                              </p>
	                              <div class="description">
	                                <p><?=stripslashes($value->rating_desc)?></p>
	                                <div class="tz-gallery">
        								<div class="row upload_img_part">
        									<?php 
        										foreach ($row_review_img as $key => $review_img) {

        									?>
        									<div class="user_upload_preview">
								                <a class="lightbox" href="<?=base_url('assets/images/review_images/').$review_img->image_file?>">
								                    <img src="<?=base_url('assets/images/review_images/').$review_img->image_file?>" alt="Review image">
								                </a>
								            </div>
								        	<?php } ?>
        								</div>
        							</div>
	                              </div>
	                            </div>
	                          </div>
	                        </li>
	                    	<?php } ?>
	                    	<?php 
	                    		if(count($product_rating) > 1){
	                    	?>
	                    	<li>
	                    		 <a href="<?=base_url('product-reviews/'.$product->product_slug)?>" style="float: right;">View All Reviews</a>
	                    	</li>
	                    	<?php } ?>
	                      </ul>
	                      <?php 
	                      	}
	                      	// print_r(check_user_login());

	                      	$ci->is_purchased($user_id, $product->id);

	                      	if(isUserLoggedIn()=='1' && $ci->is_purchased($user_id, $product->id)){
	                      ?>
	                      <div class="review-form-wrapper">
	                        <div class="review-form"> <span class="comment-reply-title" style="font-size: 18px;font-weight: 500">Add a Review </span>
	                          <form action="<?=base_url('site/product_review')?>" id="review-form">
	                            <p class="comment-notes"> <span id="email-notes">Your email address will not be published.</span> Required fields are marked <span class="required">*</span> </p>
	                            <div class="comment-form-rating">
	                              <label>Your Rating</label>
	                              <div class="rating"> 
									<div class='rating-stars'>
									    <ul id='stars'>
									      <li class='star selected' title='Poor' data-value='1'>
									        <i class='fa fa-star fa-fw'></i>
									      </li>
									      <li class='star <?php if(!empty($my_review) && $my_review[0]->rating >= 2){ echo 'selected';} ?>' title='Fair' data-value='2'>
									        <i class='fa fa-star fa-fw'></i>
									      </li>
									      <li class='star <?php if(!empty($my_review) && $my_review[0]->rating >= 3){ echo 'selected';} ?>' title='Good' data-value='3'>
									        <i class='fa fa-star fa-fw'></i>
									      </li>
									      <li class='star <?php if(!empty($my_review) && $my_review[0]->rating >= 4){ echo 'selected';} ?>' title='Excellent' data-value='4'>
									        <i class='fa fa-star fa-fw'></i>
									      </li>
									      <li class='star <?php if(!empty($my_review) && $my_review[0]->rating >= 5){ echo 'selected';} ?>' title='WOW!!!' data-value='5'>
									        <i class='fa fa-star fa-fw'></i>
									      </li>
									    </ul>
									  </div> 
								  </div>
	                            </div>
	                            <div class="row">
									<div class="input-element">
									  <input type="hidden" class="inp_rating" name="rating" value="<?php echo (!empty($my_review)) ? $my_review[0]->rating : '1';?>">
									  <input type="hidden" name="product_id" value="<?=$product->id?>">
									  <div class="review-comment-form-author col-md-6">
										<label>Name </label>
										<input placeholder="name" type="text" readonly="" value="<?=$this->session->userdata('user_name')?>">
									  </div>
									  <div class="review-comment-form-email col-md-6">
										<label>Email </label>
										<input placeholder="email" readonly="" type="text" value="<?=$this->session->userdata('user_email')?>">
									  </div>
									  <div class="comment-form-comment col-md-12">
										<label>Review</label>
										<textarea placeholder="enter you review..." name="message" cols="40" rows="8"><?php echo (!empty($my_review)) ? $my_review[0]->rating_desc : '';?></textarea>
									  </div>
									  <div class="comment-form-comment col-md-12 upload_img_part" style="margin-bottom: 10px">
										<label>Product Images (You can upload multiple image of products)</label>
										<input type="file" name="product_images[]" multiple="" style="outline: none !important;padding-left:0;">
										<?php 
											if(!empty($my_review))
											{
												?>
												<div class="row">
													<?php 
														foreach ($my_review[1] as $key => $value) {
													?>
													<div class="text-center review_img_holder">
														<img src="<?=base_url('assets/images/review_images/').$value->image_file?>" alt="">	
														<br/>
														<a href="" class="btn_remove_img" data-id="<?=$value->id?>" style="color: #F00">&times;</a>	
													</div>
													<?php } ?>
												</div>
												<?php
											}
										?>
										
									  </div>
									  <div class="comment-submit col-md-12 text-left" style="text-align: left">
										<button type="submit" class="form-button">Submit</button>
									  </div>
									</div>
								</div>
	                          </form>
	                        </div>
	                      </div>
	                  	  <?php }
	                  	  else if(isUserLoggedIn()=='1' && !$ci->is_purchased($user_id, $product->id)){
	                  	  	?>
	                  	  	<div class="review-form-wrapper">
		                  		<button class="quantity-button" onclick="javascript:alert('Sorry! you are not allowed to review this product..');">Add your Review</button>
		                  	</div>
	                  	  	<?php
	                  	  }
	                  	  else{ ?>
	                  	  <div class="review-form-wrapper">
	                  	  	<button class="quantity-button"  onclick="location.href='<?php echo site_url('login-register'); ?>'">Add your Review</button>
	                  	  </div>
	                  	  <?php } ?>
	                    </div>
	                  </div>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
</section>

<!-- Related products -->
<section class="related-products-area mb-85">
	<div class="container">
	  <div class="row">
	    <div class="col-md-12"> 
	      <div class="section-title1-border">
	        <div class="section-title1">
	          <h3>Related products</h3>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="row">
	    <div class="related-products owl-carousel">
	      <?php 

	      	foreach ($related_products as $key => $product_row) {

	      		$img_file=$ci->_create_thumbnail('assets/images/products/',$product_row->product_slug,$product_row->featured_image,210,210);

				$img_file2=$ci->_create_thumbnail('assets/images/products/',$product_row->id,$product_row->featured_image2,210,210);

	      ?> 
	      <div class="col-md-12">
	        <div class="single-product">
	          <div class="product-img"> <a href="<?php echo site_url('product/'.$product_row->product_slug); ?>" title="<?=$product_row->product_title?>"> <img class="first-img" src="<?=base_url().$img_file?>"> <img class="hover-img" src="<?=base_url().$img_file2?>"> </a>
	            <ul class="product-action">
                  <?php 
                    if(isUserLoggedIn() && $ci->is_favorite($this->session->userdata('GSM_FUS_UserId'), $product_row->id)){
                      ?>
                      <li><a href="" class="btn_wishlist" data-id="<?=$product_row->id?>" data-toggle="tooltip" title="Remove to Wishlist" style="background-color: #ff5252"><i class="ion-android-favorite-outline"></i></a></li>
                      <?php
                    }
                    else if($ci->check_cart($product_row->id,$this->session->userdata('GSM_FUS_UserId'))){
                      ?>
                      <li><a href="javascript:void(0)" data-toggle="tooltip" title="Already in Cart"><i class="ion-android-favorite-outline"></i></a></li>
                      <?php
                    } 
                    else{
                      ?>
                      <li><a href="" class="btn_wishlist" data-id="<?=$product_row->id?>" data-toggle="tooltip" title="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                      <?php
                    } 
                  ?>

                  <li><a href="" class="btn_quick_view" data-id="<?=$product_row->id?>" title="Quick View"><i class="ion-android-expand"></i></a></li>

                </ul>
	          </div>
	          <div class="product-content">
	            <h2>
	            	<a href="<?php echo site_url('product/'.$product_row->product_slug); ?>">
	                    <?php 
	                      if(strlen($product_row->product_title) > 20){
	                        echo substr(stripslashes($product_row->product_title), 0, 20).'...';  
	                      }else{
	                        echo $product_row->product_title;
	                      }
	                    ?>
	                </a>
	            </h2>
	            <div class="rating"> 
                  <?php 
                    for ($x = 0; $x < 5; $x++) { 
                      if($x < $product_row->rate_avg){
                        ?>
                        <i class="fa fa-star" style="color: #F9BA48"></i>
                        <?php  
                      }
                      else{
                        ?>
                        <i class="fa fa-star"></i>
                        <?php
                      }
                    }
                  ?>
                </div>
                <div class="product-price"> 
                  <?php 
                    if($product_row->you_save_amt!='0'){
                      ?>
                      <span class="new-price"><?=CURRENCY_CODE.' '.$product_row->selling_price?></span> 
                      <span class="old-price"><?=CURRENCY_CODE.' '.$product_row->product_mrp;?></span>
                      <?php
                    }
                    else{
                      ?>
                      <span class="new-price"><?=CURRENCY_CODE.' '.$product_row->product_mrp;?></span>
                      <?php
                      
                    }
                  ?>
	            <?php
                    if(!$ci->check_cart($product_row->id,$user_id)){
                      ?>
                      <a class="button add-btn btn_cart" data-id="<?=$product_row->id?>" data-maxunit="<?=$product_row->max_unit_buy?>" style="" href="javascript:void(0)" data-toggle="tooltip" title="Add to Cart">add to cart</a>
                      <?php
                    }
                    else{
                      ?>
                      <a class="button add-btn btn_remove_cart" style="" href="<?php echo site_url('remove-to-cart/'.$product_row->id); ?>" data-toggle="tooltip" title="Remove to Cart">remove to cart</a>
                      <?php
                    }
                  ?>
	        	</div>
	          </div>
	        </div>
	      </div>
	  	  <?php } ?>
	    </div>
	  </div>
	</div>
</section>

<div id="size_chart" class="modal" style="z-index: 9999999;background: rgba(0,0,0,0.5);overflow-y: auto;">
  <div class="modal-dialog modal-confirm">
    <div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title" style="font-weight: 600">Size Chart</h4>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		</div>
		<div class="modal-body" style="padding:0px;padding-top:15px;">
			<img src="" class="size_chart_img">
			<h3 class="no_data">No data available!</h3>
		</div>
  	</div>
  </div>
</div>

<script type="text/javascript" src="<?=base_url('assets/site_assets/js/baguettebox/baguetteBox.min.js')?>"></script>

<script type="text/javascript">
	baguetteBox.run('.tz-gallery');
</script>

<script type="text/javascript">
	// submit review form
	$("#review-form").submit(function(e){
		e.preventDefault();

		var formData = new FormData($(this)[0]);

		$.ajax({
	        url:$(this).attr("action"),
	        processData: false,
	        contentType: false,
	        type: 'POST',
	        data: formData,
	        success: function(data){

	        	var obj = $.parseJSON(data);
				if(obj.success=='1'){
					location.reload();
				}
				else{
					alert("Something going to wrong....");
				}
	        }

	    });

	});


	$('.radio-group .radio_btn').click(function(){
	    $(this).parent().find('.radio_btn').removeClass('selected');
	    $(this).addClass('selected');
	    var val = $(this).attr('data-value');
	    //alert(val);
	    $(this).parent().find('input').val(val);
	});

	$(".product_qty").blur(function(e){

		if(parseInt($(this).val()) <= 0){
			$(this).val(1);
		}
		else if(parseInt($(this).val()) > parseInt($(".max_unit_buy").val())){
			alert("You cannot buy more than "+$(".max_unit_buy").val()+" items in single order !!!");
			$(this).val($(".max_unit_buy").val());
		}
	});


	$(".btn_remove_img").click(function(e){
		e.preventDefault();

		var _ele=$(this).parent(".review_img_holder");

		href = '<?=base_url()?>site/remove_review_image';

		if(confirm("Are sure to remove image?")){
			$.ajax({
				url:href,
				data: {"id": $(this).data("id")},
				type:'post',
				success:function(res){
					var obj = $.parseJSON(res); 

					if(obj.success==1){
						_ele.remove();
						$('.notifyjs-corner').empty();
						$.notify(
							obj.message, 
							{ position:"top right",className: obj.class }
						);
					}
					else{
						alert("Something going wrong !!!");
					}
				}
			});
		}
	});

	$(document).ready(function() {
	      // Configure/customize these variables.
	      var showChar = 500;  // How many characters are shown by default
	      var ellipsestext = "...";
	      var moretext = "Show more >>";
	      var lesstext = "<< Show less";
	      

	      $('.more').each(function() {
	          var content = $.trim($(this).text());
	    
	          

	          if(content.length > showChar) {
	    
	              var c = content.substr(0, showChar);
	              var h = content.substr(showChar, content.length - showChar);
	    
	              var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span><a href="" class="morelink">' + moretext + '</a></span>';
	   
	              $(this).html(html);
	          }
	   
	      });
	   
	      $(".morelink").click(function(){
	          if($(this).hasClass("less")) {
	              $(this).removeClass("less");
	              $(this).html(moretext);
	          } else {
	              $(this).addClass("less");
	              $(this).html(lesstext);
	          }
	          $(this).parent().prev().toggle();
	          $(this).prev().toggle();
	          return false;
	      });


	      // for cart form
	      $("#cartForm").submit(function(event){

            event.preventDefault();
            $(".process_loader").show();

            var formData = $(this).serialize();
            var _form=$(this);

            $.ajax({
              type: 'POST',
              url: $(this).attr('action'),
              data: formData
            })
            .done(function(response) {

              var res = $.parseJSON(response);
              $(".process_loader").hide();

              if(res.success=='1'){
                swal({ title: "Done!", text: res.msg, type: "success" }, function(){ location.reload(); });
              }
              else if(res.success=='0'){
                window.location.href='<?=base_url()?>login-register';
              }
              
            })
            .fail(function(response) {
              $(".process_loader").hide();
              swal("Something gone wrong!", res);
            });

          });

	  });

	// for size chart modal open

	$(".size_chart").click(function(e){
		e.preventDefault();

		$(".size_chart_img").hide();
		$(".no_data").hide();

		if($(this).data("img")==''){
			$(".no_data").show();
		}
		else{
			$(".size_chart_img").show();
			$(".size_chart_img").attr("src",$(this).data("img"));
		}
		
		$("#size_chart").modal("show")

	});

</script>

<?php
  if($this->session->flashdata('cart_msg')) {
    $message = $this->session->flashdata('cart_msg');
    ?>
      <script type="text/javascript">
        var _msg='<?=$message['message']?>';
        var _class='<?=$message['class']?>';

        $('.notifyjs-corner').empty();
        $.notify(
          _msg, 
          { position:"top right",className: _class }
        ); 
      </script>
    <?php
  }
?>

<?php
  if($this->session->flashdata('response_msg')) {
    $message = $this->session->flashdata('response_msg');
    ?>
      <script type="text/javascript">
        var _msg='<?=$message['message']?>';
        var _class='<?=$message['class']?>';

        $('.notifyjs-corner').empty();
        $.notify(
          _msg, 
          { position:"top right",className: _class }
        ); 
      </script>
    <?php
  }
?>
