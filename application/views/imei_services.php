<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('CUST_CODE_HEADING'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-lg-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
                    <?php 
                    if($this->input->post_get('success')  && $this->input->post_get('success') == '1')
                    {
                        $myMsg = str_replace('LB', '<br />', $this->input->post_get('msge'));
                        $myMsg = str_replace('SP', ' ', $myMsg);
                        echo '<div id="msg1" class="row"><div class="col-lg-12"><div class="alert alert-success">'.$myMsg.'</div></div></div>';
                    }
                    ?>
                    <div class="form-group row">
						<div class="col-lg-12">
                                <?php if($message != '')
                                    echo '<div id="msg2" class="row"><div class="col-lg-12"><div class="alert alert-info">'.$message.'<br /><br /></div></div></div>';
                                if($message == '' && isset($msg) && $msg != '' && $this->input->post('isError') == '1')
                                    echo '<div id="msg3" class="row"><div class="col-lg-12"><div class="alert alert-danger">'.$msg.'</div></div></div>';
                                if(isset($errorMsg)  && trim($errorMsg) != '')
                                    echo "<div id='msg4' class='row'><div class='col-md-12'><div class='alert alert-danger'>ERRORS:<br />$errorMsg</div></div></div>";
                            ?>
                		</div>
					</div>
					

                    <?php echo form_open(base_url('page/imei_services') , 'name="frm" id="frm"');?>
                    <div class="form-group row">

                        <div class="col-lg-7">
                            <label><?php echo $this->lang->line('CUST_MENU_CODES_1'); ?>:</label>
                            <select class="form-control select2" id="packageId" name="packageId" style="width: 100%; color: inherit;" required>
                                <?php $prevCatId = 0; 
                                    foreach($rsPackages as $row) { 
                                        $changeBGColor = '';
                                        $myPackPrice = 0;
                                        if(isset($PACK_PRICES_USER[$row->PackageId]))
                                            $myPackPrice = $PACK_PRICES_USER[$row->PackageId];
                                        else if(isset($PACK_PRICES_PLAN[$row->PackageId]))
                                            $myPackPrice = $PACK_PRICES_PLAN[$row->PackageId];
                                        else
                                        {
                                            if (isset($PACK_PRICES_BASE[$row->PackageId]))
                                                $myPackPrice = $PACK_PRICES_BASE[$row->PackageId];
                                        }
                                        if($myPackPrice < $PACK_PRICES_BASE[$row->PackageId])
                                            $changeBGColor = 'class="discount_text"';
                                        if($prevCatId > 0)
                                        {
                                            if($row->CategoryId != $prevCatId)
                                                echo '</optgroup><optgroup label="'.stripslashes($row->Category).'"> ';
                                        }
                                        else
                                            echo '<option value="" selected>Select...</option><optgroup label="'.stripslashes($row->Category).'">';
                                ?>
                                <option <?php echo $changeBGColor; ?> value="<?php echo $row->PackageId?>" <?php if($ORDER_COMPLETED != '1' && $myNetworkId == $row->PackageId) echo 'selected';?> > <?php echo stripslashes($row->PackageTitle).' - '.$myPackPrice.' Credits'; ?> </option>
                                <?php $prevCatId = $row->CategoryId; }  
                                    echo '</optgroup>';
                                ?>
                            </select>
                            <div class="col-sm-6 dot-opacity-loader" align="center">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <?php if($settings->IMEIOrderDropDownType == '0' || $settings->IMEIOrderDropDownType  == '1') { ?>
                                <br />
                                <div class="col-sm-12" id="tblInfo" style="display:none; margin-top:10px">
                                    <div class="card m-b-30 card-info">
                                        <div class="card-body" style="background-color:#F5F5F5;">
                                            <blockquote class="card-bodyquote">
                                                <p id="dvSN" style="font-weight:bold; font-size:20px;">&nbsp;</p>
                                                <footer class="blockquote-footer" style="color:#E81516; font-size:20px; font-weight:bold;" id="lblCustPrice"></footer>
                                                <p id="dvSrvImg" style="display:none;"><img id="imgSrv" src="" /></p>
                                                <p style="color:green;"><strong>Delivery Time: </strong><span id="dvTT" style="color:#000000;"><?php echo isset($timeTaken); ?></span></p>
                                                <p id="dvFeatures" style="display:none;"></p>
                                                <div id="dvMR" <?php if($myNetworkId == 0) { echo 'style="display:none;"'; } ?>><?php echo $mustRead; ?></div>
                                                <p id="dvTOC" style="display:none;"><input type="checkbox" id="chkTOC" name="chkTOC" /> I agree with terms of services</p>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-lg-5">
                            <div class="card">
                                <div class="card-body">
                                    <div class="edit-cont mb-3">
                                        <div class="form-group" id="dvIMEIOptions" style="display:none;">
                                            <label><?php echo $this->lang->line('CUST_CODE_3'); ?>:</label>
                                            <div class="form-group row">
                                                <div class="col-sm-6 col-md-5 col-lg-4">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                        <input type="radio" name="rdIMEIType" id="rdIMEIType1" value="1" checked />
                                                        <?php echo $this->lang->line('CUST_LBL_6'); ?>
                                                        <i class="input-helper"></i></label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-5 col-lg-4">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                        <input type="radio" name="rdIMEIType" id="rdIMEIType2" value="2" />
                                                        <?php echo $this->lang->line('CUST_LBL_7');?>
                                                        <i class="input-helper"></i></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>	<!-- end col -->
                                        <div class="form-group" id="dvSIMEI" style="display:none;">
                                            <label><?php echo $this->lang->line('CUST_CODE_4'); ?>:*</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Enter IMEI #" maxlength="<?php echo $imeiMxLn; ?>" name="txtIMEI" id="txtIMEI" value="<?php echo $imei;?>">
                                                <div class="input-group-append">
                                                    <input type="text" class="form-control text-center" style="width:50px; padding: 0;" name="txtIMEILastDigit" id="txtIMEILastDigit" readonly value="<?php echo $lastDgt; ?>" placeholder="IMEI:" />
                                                </div>
                                               
                                            </div>
                                        </div>	<!-- end col -->
                                        <div class="form-group" id="dvMIMEI" style="display:none;">
                                            <label><?php echo $this->lang->line('CUST_CODE_4'); ?>:*</label>
                                            <textarea style="height:80px;" class="form-control" id="imei" name="imei" placeholder="Bulk IMEI:"><?php echo $multiIMEIs; ?></textarea>
                                            <div class="help-block alert alert-info mt-1">Last Digit will be calculated automatically!</div>
                                        </div>	<!-- end col -->
                                        <div class="form-group" id="dvCustomFld" style="display:none;">
                                        </div>	<!-- end col -->
                                        <div class="form-group" id="dvToolMobile" style="display:none;">
                                            <label><?php echo $this->lang->line('CUST_LBL_213'); ?>:*</label>
                                            <select name="toolMobileId" id="toolMobileId" class="form-control">
                                                <option value="0"><?php echo $this->lang->line('CUST_LBL_214').' '.$this->lang->line('CUST_LBL_213'); ?></option>
                                            </select>
                                        </div>	<!-- end col -->
                                        <div class="form-group" id="dvBrands" style="display:none;">
                                            <label><?php echo $this->lang->line('CUST_LBL_253'); ?>:*</label>
                                            <select name="brandId" id="brandId" class="form-control">
                                                <option value="0"><?php echo $this->lang->line('CUST_LBL_214').' '.$this->lang->line('CUST_LBL_253'); ?></option>
                                            </select>
                                            <span id="spnBrLoader" style="display:none;"><img src="<?php echo base_url('assets/img/loading.gif');?>" /></span>
                                        </div>	<!-- end col -->
                                        <div class="form-group" id="dvModels" style="display:none;">
                                            <label><?php echo $this->lang->line('CUST_LBL_254'); ?>:*</label>
                                            <select name="modelId" id="modelId" class="form-control select2me">
                                                <option value="0"><?php echo $this->lang->line('CUST_LBL_214').' '.$this->lang->line('CUST_LBL_254'); ?></option>
                                            </select>
                                        </div>	<!-- end col -->
                                        <input type="hidden" name="hdAPIId" id="hdAPIId" value="<?php echo $apiId; ?>" />
                                        <input type="hidden" name="chkSm" id="chkSm" value="<?php echo $settings->CheckSumIMEI; ?>" />
                                        <input type="hidden" name="extNtwrkId" id="extNtwrkId" value="0" />
                                        <input type="hidden" name="mdlVal" id="mdlVal" value="" />
                                        <div id="dvCstmFlds"></div>
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('CUST_CODE_10'); ?>:</label>
                                            <textarea rows="1" class="form-control" id="txtNotes" name="txtNotes" placeholder="Notes"><?php echo $notes; ?></textarea>
                                        </div>	<!-- end col -->
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('CUST_CODE_11'); ?>:</label>
                                            <textarea rows="1" class="form-control" id="txtComments" name="txtComments" placeholder="Comments"><?php echo $comments; ?></textarea>
                                        </div>	<!-- end col -->
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('CUST_CH_98'); ?>:</label>
                                            <input type="email" class="form-control" placeholder="Response E-mail:" name="txtAltEmail" maxlength="100" id="txtAltEmail" value="<?php echo($altEmail);?>">
                                          
                                        </div>	<!-- end col -->
                                        <?php if($userDetails->PinCode != '') { ?>
                                            <div class="form-group">
                                                <label><?php echo $this->lang->line('CUST_LBL_178'); ?>:*</label>
                                                <input type="text" autocomplete="off" class="form-control" placeholder="Enter your Pin Code" name="txtPinCode" maxlength="4" id="txtPinCode" value="" />
                                            </div>	<!-- end col -->
                                        <?php } ?>
                                        <div class="form-group">
                                            <button type="submit" class="clsBigBtn btn btn-primary" onClick="setValue('2');"><? echo $this->lang->line('CUST_MENU_CODES'); ?></button>
                                            <input type="hidden" id="cldFrm" name="cldFrm" value="<?php echo $cldFrm; ?>" />
                                            <input type="hidden" id="cldFm" name="cldFm" value="<?php echo $cldFm; ?>" />
                                            <input type="hidden" id="isError" name="isError" value="<?php echo $isError; ?>" />
                                            <input type="hidden" id="msg" name="msg" value="<?php echo isset($msg); ?>" />
                                            <input type="hidden" id="ddType" name="ddType" value="<?php echo $settings->IMEIOrderDropDownType; ?>" />
                                            <input type="hidden" id="hdPckTitle" name="hdPckTitle" value="<?php echo isset($pckTitle); ?>" />
                                            <input type="hidden" id="hdPreOrder" name="hdPreOrder" value="0" />
                                            <input type="hidden" id="imeiFType" name="imeiFType" value="0" />
                                            <input type="hidden" id="hdDupIMEI" name="hdDupIMEI" value="0" />
                                            <input type="hidden" id="hdSrvImg" name="hdSrvImg" value="0" />
                                            <input type="hidden" id="brandAPIId" name="brandAPIId" value="0" />
                                            <input type="hidden" id="usrCurrId" name="usrCurrId" value="<?php echo $userDetails->CurrencyId; ?>" />
                                            <input type="hidden" id="cnvrsnRt" name="cnvrsnRt" value="<?php echo $userDetails->ConversionRate; ?>" />
                                            <input type="hidden" id="currAbb" name="currAbb" value="<?php echo $userDetails->CurrencyAbb; ?>" />
                                            <input type="hidden" id="currSymbol" name="currSymbol" value="<?php echo $userDetails->CurrencySymbol; ?>" />
                                            <input type="hidden" id="brndVldtn" name="brndVldtn" value="0" />
                                            <input type="hidden" id="hdTOC" name="hdTOC" value="0" />
                                            <input type="hidden" id="customFldMaxLen" name="customFldMaxLen" value="0" />
                                            <input type="hidden" id="custFldRstrctn" name="custFldRstrctn" value="0" />
				                            <input type="hidden" id="customFldMinLen" name="customFldMinLen" value="0" />
                                        </div>
                                        <div class="form-group">
                                            <div class="alert alert-danger clsError" id="dvError" style="display:none;">&nbsp;</div>
                                        </div>
                                        <!--<div class="row" id="dvCstmFlds"></div>-->
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
$('document').ready(function(){
    $(".select2").select2();
});
</script>

<script language="javascript" src="<?php echo base_url('assets/js/functions.js');?>"></script>
<script language="javascript" src="<?php echo base_url('assets/js/imeiorder.js?v=1');?>"></script>
