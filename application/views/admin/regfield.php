<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_718'); ?></h4>
		</div>
		<div align="right">
			<a href="<?php echo base_url('admin/cms/') ?>regfields?frmId=<? echo $this->input->post_get('frmId'); ?>&fTypeId=<? echo $this->input->post_get('fTypeId') ?>"
			   class="btn btn-sm btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
		</div>
	</div>
</div>
<!-- /page header -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<?php echo form_open('', array('name' => 'frmSettings', 'id' => 'frmSettings', 'class' => 'form-horizontal')); ?>
						<div class="form-group row">
							<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PCK_17'); ?>:<span class="required_field">*</span></label>
							<div class="col-lg-4">
								<input type="text" class="form-control" placeholder="Enter Field Label" maxlength="100"
									   name="txtLbl" id="txtLbl" value="<?php echo($lbl); ?>">
								<input type="hidden" id="id" name="id" value="<? echo $id; ?>"/>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PCK_19'); ?>:<span class="required_field">*</span></label>
							<div class="col-lg-4">
								<select id="filedType" name="filedType" class="form-control select2me"
										data-placeholder="Select..." <? if ($id > 0) echo 'disabled'; ?>>
									<option value="0" selected><? echo $this->lang->line('BE_LBL_623'); ?></option>
									<option value="Text Box" <? if ($fType == 'Text Box') echo 'selected'; ?>>Text Box</option>
									<option value="Text Area" <? if ($fType == 'Text Area') echo 'selected'; ?>>Text Area
									</option>
									<option value="Drop Down" <? if ($fType == 'Drop Down') echo 'selected'; ?>>Drop Down
									</option>
									<option value="Radio Button" <? if ($fType == 'Radio Button') echo 'selected'; ?>>Radio
										Button
									</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_717'); ?>:<span class="required_field">*</span></label>
							<div class="col-lg-4">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="chkMandatory" id="chkMandatory" <?php echo $mandatory == 1 ? 'checked' : ''; ?> class="toggle"/><span><label for="chkMandatory"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_GNRL_1'); ?>:<span class="required_field">*</span></label>
							<div class="col-lg-4">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="chkDisable" id="chkDisable" <?php echo $disable == 1 ? 'checked' : ''; ?> class="toggle"/><span><label for="chkDisable"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-4"></div>
							<div class="col-lg-8">
								<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> id="btnSave"
										name="btnSave"
										class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
								<img style="display:none;" id="dvLoader"
									 src="<?php echo base_url('assets/images/loading.gif'); ?>" border="0"
									 alt="Please wait..."/>
							</div>
						</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url('assets/js/regfield.js') ?>"></script>
