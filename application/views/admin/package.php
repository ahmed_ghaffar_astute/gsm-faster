<div class="page-header border-bottom-0 pt-4">
	<div class="form-group row">
		<div class="col-lg-12">
			<?php include APPPATH . 'scripts/services_header.php'; ?>
		</div>
	</div>
</div>

<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4>
				<i class="icon-arrow-left52 mr-2"></i>
				<?php echo $fs == '0' ? $this->lang->line('BE_PCK_HD') : $this->lang->line('BE_PCK_25'); ?>
				<?php if ($updatedAt != '') { ?>
					<?php echo "(" . $this->lang->line('BE_GNRL_5') . ": $updatedAt)"; ?>
				<?php } ?>
			</h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-lg-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>
							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<?php echo form_open_multipart(base_url('admin/services/package'), 'id="frm" name="frm"'); ?>
							<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
							<input type="hidden" id="hdAPIIdForBrand" name="hdAPIIdForBrand"
								   value="<?php echo($apiIdForBrand); ?>">
							<input type="hidden" id="existingImage" name="existingImage"
								   value="<?php echo $existingImage; ?>">
							<input type="hidden" name="exAPIId" id="exAPIId" value="<?php echo $onlyAPIId; ?>">
							<input type="hidden" id="exAPIServiceId" name="exAPIServiceId"
								   value="<?php echo $extNetworkId; ?>">
							<input type="hidden" id="fs" name="fs" value="<?php echo($fs); ?>"/>
							<ul class="nav nav-tabs nav-tabs-solid rounded">
								<li class="nav-item">
									<a href="#tab_0" data-toggle="tab" class="nav-link active">
										<?php echo $this->lang->line('BE_LBL_380'); ?>
									</a>
								</li>
								<?php if ($count > 0 && $id > 0) { ?>
									<li class="nav-item">
										<a href="#tab_1" data-toggle="tab"
										   class="nav-link"><?php echo $this->lang->line('BE_LBL_528'); ?></a>
									</li>
								<?php } ?>
								<?php if ($id > 0) { ?>
									<li class="nav-item">
										<a href="#tab_2" data-toggle="tab"
										   class="nav-link"><?php echo $this->lang->line('BE_PCK_15'); ?></a>
									</li>
									<li class="nav-item">
										<a href="#tab_3" data-toggle="tab"
										   class="nav-link"><?php echo $this->lang->line('BE_LBL_377'); ?></a>
									</li>
									<li class="nav-item">
										<a href="#tab_5" data-toggle="tab"
										   class="nav-link"><?php echo $this->lang->line('BE_LBL_529'); ?></a>
									</li>
									<li class="nav-item">
										<a href="#tab_7" data-toggle="tab"
										   class="nav-link"><?php echo $this->lang->line('BE_LBL_653'); ?></a>
									</li class="nav-item">
									<li>
										<a href="#tab_8" data-toggle="tab"
										   class="nav-link"><?php echo $this->lang->line('BE_LBL_689'); ?></a>
									</li class="nav-item">
									<li>
										<a href="#tab_9" data-toggle="tab"
										   class="nav-link"><?php echo $this->lang->line('BE_LBL_694'); ?></a>
									</li>
									<li class="nav-item">
										<a href="#tab_10" id="tbDiscClients" data-toggle="tab"
										   class="nav-link"><?php echo $this->lang->line('BE_LBL_721'); ?></a>
									</li>
								<?php } ?>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="tab_0">
									<div class="row">
										<div class="col-lg-12">
											<div class="card">
												<div class="card-header header-elements-inline">
													<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_500'); ?></h5>
												</div>
											</div>
											<div class="card-body">
												<div class="form-group row">
													<label
														class="col-lg-3 control-label"><?php echo $this->lang->line('BE_MR_26'); ?>
														:*</label>
													<div class="col-lg-5">
														<input type="text" class="form-control"
															   placeholder="Enter Service" maxlength="255"
															   name="txtTitle" id="txtTitle"
															   value="<?php echo($packageTitle); ?>"/>
													</div>
												</div>
												<?php if ($id == 0) { ?>
													<div class="form-group row">
														<?php $packages_data = fetch_packages($fs); ?>
														<label class="col-lg-3 control-label">Duplicate:</label>
														<div class="col-lg-5">
															<select name="dupSrvcId" id="dupSrvcId"
																	class="form-control select2me">
																<option value="0">Select a Service</option>
																<?php FillCombo(0, $packages_data); ?>
															</select>
														</div>
													</div>
												<?php } ?>
												<div id="dvGeneral">
													<div class="form-group row">
														<?php $package_category = fetch_package_category($fs); ?>
														<label
															class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PCK_13'); ?>
															:*</label>
														<div class="col-lg-5">
															<select name="categoryId" id="categoryId"
																	class="form-control select2me"
																	data-placeholder="<?php echo $this->lang->line('BE_LBL_255'); ?>">
																<option
																	value="0"><?php echo $this->lang->line('BE_LBL_255'); ?></option>
																<?php FillCombo($categoryId, $package_category); ?>
															</select>
														</div>
													</div>
													<div class="form-group row">
														<label
															class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_201'); ?>
															:*</label>
														<div class="col-lg-5">
															<input type="text" class="form-control"
																   placeholder="Enter Price" name="txtPrice"
																   id="txtPrice" <?php if ($id == '0') echo 'onblur="calculatePrices();"'; ?> <?php if ($id > 0) { ?> onblur="if(document.getElementById('chkCnvrtPrices').checked) convertPrices();" <?php } ?>
																   maxlength="10"
																   value="<?php echo roundMe($price); ?>"/>
														</div>
													</div>
													<div class="form-group row">
														<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_651'); ?>:</label>
														<div class="col-lg-5">
															<input type="text" class="form-control"
																   placeholder="Enter Cost Price" name="txtCostPrice"
																   id="txtCostPrice" maxlength="10"
																   value="<?php echo roundMe($costPrice); ?>"/>
														</div>
													</div>
													<div class="form-group row ">
														<?php
															$PLANS = array();
															if ($onlyAPIId > 0 && $extNetworkId > 0) {
																$allowCP = false;
																if ($fs == '0')
																	$allowCP = true;
																else if ($fs == '1' && ($apiType == '4' || $apiType == '6'))
																	$allowCP = true;
																if ($allowCP) {
														?>
																<label style="margin-bottom:0;" class="col-lg-3 control-label d-flex align-items-center">Get Cost Price From API 
																	<img style="display:none;" id="imgCPLdr" src="<?php echo base_url(); ?>assets/images/loading.gif" border="0" alt="Please wait..."/>
																</label>
																<div class="col-lg-5 align-items-center d-flex">
																	<input type="checkbox" class="chkSelect" name="chkFetchCPr" id="chkFetchCPr" <?php if ($costPrFrmAPI == '1') echo 'checked'; ?> />
																	<?php
																		$rw = fetch_currency_data();

																		$DEFAULT_CURRENCY = $rw->CurrencyAbb;
																		$PRICES = array();
																		$rsPrices = fetch_service_api_pricing($fs, $id, $onlyAPIId);

																		foreach ($rsPrices as $row) {
																			$PRICES[$row->ServiceId][$row->GroupId] = $row->PriceMargin . '|' . $row->MarginType;
																		}
																		$rsGroups = fetch_price_plans();

																		foreach ($rsGroups as $row) {
																			$PLANS[$row->PricePlanId] = stripslashes($row->PricePlan);
																		}
																		$defaultPrice = '0';
																		$defaultMarginType = '';
																		if (isset($PRICES[$id][0]) && $PRICES[$id][0] != '') {
																			$val = $PRICES[$id][0];
																			$arr = explode('|', $val);
																			if (isset($arr[0]) && is_numeric($arr[0]))
																				$defaultPrice = roundMe($arr[0]);
																			if (isset($arr[1]) && is_numeric($arr[1])) {
																				$defaultMarginType = $arr[1] == '0' ? '' : '%';
																			}
																		}
																		$strPriceMargins = '';
																		$icon = 'upload';

																		if (sizeof($PLANS) > 0) {
																			foreach ($PLANS as $key => $value) {
																				$priceVal = '0';
																				$marginTypeVal = '';
																				if (isset($PRICES[$id][$key]) && $PRICES[$id][$key] != '') {
																					$val = $PRICES[$id][$key];
																					$arr = explode('|', $val);
																					if (isset($arr[0]) && is_numeric($arr[0]))
																						$priceVal = roundMe($arr[0]);
																					if (isset($arr[1]) && is_numeric($arr[1]))
																						$marginTypeVal = $arr[1] == '0' ? '' : '%';
																				}
																				$symbol = $priceVal >= 0 ? "+" : "-";
																				$strPriceMargins .= '<strong>' . $value . '</strong> ' . $symbol . ' ' . $priceVal . $marginTypeVal . ' ' . $DEFAULT_CURRENCY . '<br />';
																				if ($priceVal > 0 || $defaultPrice > 0)
																					$icon = 'chevron-down';
																			}
																		}
																	?>
																	<a data-target="#collapseList_<?php echo $id; ?>" class=" btn btn-xs pull-right" data-toggle="collapse" id="list_auto_update">
																		<i class="fa fa-<?php echo $icon; ?>"></i>
																	</a>
																	<div class="collapse" id="collapseList_<?php echo $id; ?>">
																		<div class="form-group row">
																			<div class="col-lg-6">
																				<strong>Default</strong> 
																				<?php echo $defaultPrice >= 0 ? '+' : '-' ?> <?php echo $defaultPrice . $defaultMarginType . ' ' . $DEFAULT_CURRENCY; ?>
																				<?php echo $strPriceMargins; ?>
																			</div>
																			<a href="<?php echo base_url('admin/services/package?')?>PMAPIId=<?php echo $onlyAPIId ?>&id=<?php echo $id; ?>&del=1" class="btn btn-danger btn-xs pull-right" title="Delete Prices" onclick="return confirm('Are you sure you want to delete these price margins?')">
																				<i class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i>
																			</a>
																			<a href="<?php echo base_url('admin/services/setpriceswithapi?srvcAPIId=' . $onlyAPIId . '&serviceId=' . $id . '&sType=' . $fs); ?>" class="fancybox fancybox.ajax btn btn-info btn-xs pull-right" title="Set Services Price Margins">
																				<i class="fa icon-pencil"></i>
																			</a>
																		</div>
																	</div>
																</div>
														<?php }
															} 
														?>
													</div>
													<div class="form-group row">
														<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_344'); ?>:</label>
														<div class="col-lg-5">
															<input type="text" class="form-control"
																   placeholder="Enter Supplier" maxlength="100"
																   name="txtSupplier"
																   value="<?php echo($supplier); ?>"/>
														</div>
													</div>
													<div class="form-group row">
														<label
															class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PCK_10'); ?>
															:</label>
														<div class="col-lg-5">
															<input type="text" class="form-control"
																   placeholder="Enter Delivery Time" name="txtTimeTaken"
																   id="txtTimeTaken" maxlength="100"
																   value="<?php echo($timeTaken); ?>"/>
														</div>
													</div>
													<div class="form-group row">
														<label
															class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_839'); ?>
															:</label>
														<div class="col-lg-5">
															<input type="text" class="form-control"
																   placeholder="Enter <?php echo $this->lang->line('BE_LBL_839'); ?>"
																   name="txtRedirection" maxlength="100"
																   value="<?php echo($redirect); ?>"/>

															<span class="form-text text-muted">
                                                                Sample URL: http://www.example.com/<br/>
                                                                Client will be sent to this URL on choosing this service.
                                                            </span>
														</div>
													</div>
													<div class="form-group row">
														<label class="col-lg-3 control-label">Notification
															Emails:</label>
														<div class="col-lg-5">
															<input type="text" class="form-control"
																   placeholder="Enter Emails separated by Commas"
																   name="txtEmails" maxlength="100"
																   value="<?php echo($emailIDs); ?>"/>

															<span class="form-text text-muted">
                                                                Email Addresses here to send New order emails for<br/>this service.
                                                            </span>
														</div>
													</div>
													<div class="form-group row">
														<label class="col-lg-3 control-label">IMEI Restriction
															Series:</label>
														<div class="col-lg-5">
															<?php $series_data = fetch_imei_restricted_series(); ?>
															<select name="seriesId" class="form-control select2me">
																<option value="0">Choose a Series</option>
																<?php FillCombo($seriesId, $series_data); ?>
															</select>
														</div>
													</div>
													<div class="form-group row">
														<label
															class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PCK_21'); ?>
															:</label>
														<div class="col-lg-7 radio-list">
															<div class="form-check form-check-inline mt-0">
															<label class="form-check-label">
																<input type="radio" class="form-check-input"
																	   name="rdServiceType" id="rdServiceType0"
																	   value="0"
																	   checked/><?php echo $this->lang->line('BE_LBL_832'); ?>
															</label>
															</div>
															<div class="form-check form-check-inline mt-0">
															<label class="form-check-label">
																<input type="radio" class="form-check-input"
																	   name="rdServiceType" id="rdServiceType1"
																	   value="1" <?php if ($srvType == '1') echo 'checked'; ?> /><?php echo $this->lang->line('BE_LBL_833'); ?>
															</label>
															</div>
														</div>
													</div>
													<div
														class="form-group row" <?php if ($srvType != '0') echo 'style="display:none;"'; ?>
														id="dvDelayTm">
														<label
															class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_835'); ?>
															:</label>
														<div class="col-lg-3">
															<input type="text" class="form-control"
																   placeholder="Enter <?php echo $this->lang->line('BE_LBL_835'); ?>"
																   name="txtResDelayTm" id="txtResDelayTm" maxlength="4"
																   value="<?php echo($responseDelayTm); ?>"/>
														</div>
														<span class="form-text text-muted">
                                                                In minutes
                                                            </span>
													</div>
													<?php if ($fs == '0') { ?>
														<div class="form-group row">
															<label
																class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_136'); ?>
																:</label>
															<div class="col-lg-7 radio-list">
																<div class="form-check form-check-inline mt-0">
																<label class="form-check-label">
																	<input type="radio" class="form-check-input"
																		   name="rdIMEIFType" id="rdIMEIFType1"
																		   value="0"
																		   checked/><?php echo $this->lang->line('BE_LBL_137'); ?>
																</label>
																</div>
																<div class="form-check form-check-inline mt-0">
																<label class="form-check-label">
																	<input type="radio" class="form-check-input"
																		   name="rdIMEIFType" id="rdIMEIFType2"
																		   value="1" <?php if ($imeiFType == '1') echo 'checked'; ?> /><?php echo $this->lang->line('BE_LBL_138'); ?>
																</label>
																</div>
																<div class="form-check form-check-inline mt-0">
																<label class="form-check-label">
																	<input type="radio" class="form-check-input"
																		   name="rdIMEIFType" value="2"
																		   id="rdIMEIFType3" <?php if ($imeiFType == '2') echo 'checked'; ?> /><?php echo $this->lang->line('BE_LBL_139'); ?>
																</label>
																</div>
																<div class="form-check form-check-inline mt-0">
																<label class="form-check-label">
																	<input type="radio" class="form-check-input"
																		   name="rdIMEIFType" value="3"
																		   id="rdIMEIFType4" <?php if ($imeiFType == '3') echo 'checked'; ?> /><?php echo $this->lang->line('BE_LBL_378'); ?>
																</label>
																</div>
															</div>
														</div>
													<?php } ?>
													<div class="form-group  row"
														 id="dvCustFld" <?php if ($imeiFType != '3') echo 'style="display:none;"'; ?>>
														<?php $custom_fields = fetch_custom_fields(); ?>
														<label class="col-lg-3 control-label">Custom Field:</label>
														<div class="col-lg-4">
															<select name="customFldId" id="customFldId"
																	class="form-control select2me">
																<?php FillCombo($customFldId, $custom_fields); ?>
															</select>
														</div>
													</div>
													<div class="form-group row">
														<label for="txtImage"
															   class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PCK_3'); ?>
															:</label>
														<div class="col-lg-4">
															<input type="file" id="txtImage" name="txtImage">
															<p class="form-text text-muted">
																.jpg, .gif, .png
															</p>
														</div>
													</div>
													<?php
													if ($existingImage != '') { ?>
														<div class="form-group row">
															<label
																class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_609'); ?>
																:</label>
															<div class="col-lg-4">
																<div class="switch-button switch-button-lg"
																	 data-on-label="YES" data-off-label="NO">
																	<input type="checkbox" name="chkDelImg"
																		   id="chkDelImg" class="toggle"/><span><label
																			for="chkDelImg"></label></span>
																</div>
															</div>
														</div>

														<?php
													}
													?>
													<div class="form-group row">
														<label
															class="control-label col-lg-3"><?php echo $this->lang->line('BE_LBL_105'); ?>
															:</label>
														<div class="col-lg-9">
															<textarea class="ckeditor form-control" name="txtMustRead"
																	  rows="6"><?php echo $mustRead; ?></textarea>
														</div>
													</div>
													<div class="form-group row">
														<label class="col-lg-3 control-label">Terms & Condtions:</label>
														<div class="col-lg-4">
															<div class="switch-button switch-button-lg"
																 data-on-label="YES" data-off-label="NO">
																<input type="checkbox" class="chkSelect" name="chkTOC"
																	   id="chkTOC" <?php echo $toc == 1 ? 'checked' : ''; ?>
																	   class="toggle"/><span><label
																		for="chkTOC"></label></span>
															</div>
														</div>
													</div>
													<?php if ($id > 0) { ?>
														<div class="form-group row">
															<label
																class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_389'); ?>
																:</label>
															<div class="col-lg-4">
																<div class="switch-button switch-button-lg"
																	 data-on-label="YES" data-off-label="NO">
																	<input type="checkbox" class="chkSelect"
																		   name="chkResetDiscounts"
																		   id="chkResetDiscounts" class="toggle"/><span><label
																			for="chkResetDiscounts"></label></span>
																</div>
															</div>
														</div>
													<?php } ?>
													<div class="form-group row">
														<label
															class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_630'); ?>
															:</label>
														<div class="col-lg-4">
															<div class="switch-button switch-button-lg"
																 data-on-label="YES" data-off-label="NO">
																<input type="checkbox" name="cancelOrders"
																	   id="cancelOrders" <?php echo $cancelOrders == 1 ? 'checked' : ''; ?>
																	   class="toggle"/><span><label
																		for="cancelOrders"></label></span>
															</div>
														</div>
													</div>
													<div class="form-group row">
														<label
															class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_736'); ?>
															:</label>
														<div class="col-lg-2">
															<input type="text" class="form-control" name="txtCancelTime"
																   maxlength="6" id="txtCancelTime"
																   value="<?php echo($cancelMins); ?>"/>
														</div>
														<span class="form-text text-muted">
                                                                Please enter time to verify an order in minutes!
                                                            </span>
													</div>
													<div class="form-group row">
														<label
															class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_675'); ?>
															:</label>
														<div class="col-lg-4">
															<div class="switch-button switch-button-lg"
																 data-on-label="YES" data-off-label="NO">
																<input type="checkbox" class="chkSelect"
																	   name="verifyOrders"
																	   id="verifyOrders" <?php echo $verifyOrders == 1 ? 'checked' : ''; ?>
																	   class="toggle"/><span><label
																		for="verifyOrders"></label></span>
															</div>
														</div>
													</div>
													<div class="form-group row">
														<label
															class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_737'); ?>
															:</label>
														<div class="col-lg-2">
															<input type="text" class="form-control" name="txtVerifyTime"
																   maxlength="6" id="txtVerifyTime"
																   value="<?php echo($verifyMins); ?>"/>
														</div>
														<span class="form-text text-muted">
                                                                Please enter time to verify an order in minutes!
                                                            </span>
													</div>
													<div class="form-group row">
														<label
															class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_180'); ?>
															:</label>
														<div class="col-lg-4">
															<div class="switch-button switch-button-lg"
																 data-on-label="YES" data-off-label="NO">
																<input type="checkbox" class="chkSelect" name="dupIMEI"
																	   id="dupIMEI" <?php echo $dupIMEI == 1 ? 'checked' : ''; ?>
																	   class="toggle"/><span><label
																		for="dupIMEI"></label></span>
															</div>
														</div>
													</div>
													<div class="form-group row">
														<label
															class="col-lg-3 control-label"><?php echo $this->lang->line('BE_GNRL_1'); ?>
															:</label>
														<div class="col-lg-4">
															<div class="switch-button switch-button-lg"
																 data-on-label="YES" data-off-label="NO">
																<input type="checkbox" class="chkSelect"
																	   name="chkDisablePackage"
																	   id="chkDisablePackage" <?php echo $disablePackage == 1 ? 'checked' : ''; ?>
																	   class="toggle"/><span><label
																		for="chkDisablePackage"></label></span>
															</div>
														</div>
													</div>
													<div class="form-group row mb-3">
														<label class="col-lg-3 control-label">Features:</label>
														<div class="col-lg-9">
															<?php
															foreach ($rsFtrs as $rwF) {
																$chckd = '';
																if ($rwF->Checked == '1')
																	$chckd = 'checked';
																?>
																<div class="form-check form-check-inline mt-0">
																	<label class="form-check-label">
																		<input type="checkbox" class="form-check-input"
																			   name="packFeatures[]"
																			   value="<?php echo $rwF->FeatureId; ?>" <?php echo $chckd; ?> /><?php echo $rwF->Feature; ?>
																	</label>
																</div>
																<?php
															}
															?>
														</div>
													</div>
												</div>
												<div class="form-group row">
													<div class="col-lg-3"></div><div class="col-lg-9">
														<button
															type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
															class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
													</div>
												</div>
												<div align="center">

												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab_1">
									<?php if ($count > 0) { ?>
										<div class="row">
											<div class="col-lg-12">
												<div class="card">
													<div class="card-header header-elements-inline">
														<h5 class="card-title">
															<?php echo $this->lang->line('BE_LBL_284'); ?>
														</h5>
													</div>
												</div>
												<div class="card-body">
													<input type="hidden" name="totalCurrencies"
														   value="<?php echo $count; ?>"/>
													<div class="form-body custom_packages">
														<?php if ($id > 0) { ?>
															<div class="form-group row">
																<label
																	class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_387'); ?>
																	:</label>
																<div class="col-lg-4">
																	<input type="checkbox" class="chkSelect"
																		   name="chkCnvrtPrices" id="chkCnvrtPrices"
																		   onClick="if(this.checked) convertPrices();"/>
																</div>
															</div>
														<?php }
														$index = 0;
														$strCurrHdrs = '';
														$PACK_PRICES_OTHER = array();
														$CURR_CONV_RATES = array();
														foreach ($rsCurrencies as $row) {
															$strCurrHdrs .= "<th>" . $row->CurrencyAbb . "</th>";
															if ($row->DefaultCurrency != '1') {
																$currencyPrice = '';
																if ($id > 0) {
																	if ($row->Price == '')// set as per conversion rate
																	{
																		$currencyPrice = number_format($price * $row->ConversionRate, 2, '.', '');
																	} else {
																		$currencyPrice = number_format($row->Price, 2, '.', '');
																	}
																	$PACK_PRICES_OTHER[$row->CurrencyId] = $currencyPrice;
																	$CURR_CONV_RATES[$row->CurrencyId] = $row->ConversionRate;
																}
																?>
																<div class="form-group row">
																	<label
																		class="col-lg-3 control-label"><?php echo $row->CurrencyAbb; ?>
																		:</label>
																	<div class="col-lg-2">
																		<input type="hidden"
																			   name="currencyId<?php echo $index; ?>"
																			   value="<?php echo $row->CurrencyId; ?>"/>
																		<input type="text" class="form-control"
																			   placeholder="Enter Price" maxlength="10"
																			   name="txtCurrPrice<?php echo $index; ?>"
																			   id="txtCurrPrice<?php echo $index; ?>"
																			   value="<?php echo $currencyPrice; ?>"/>
																		<input type="hidden"
																			   name="converstionRate<?php echo $index; ?>"
																			   id="converstionRate<?php echo $index; ?>"
																			   value="<?php echo $row->ConversionRate; ?>"/>
																	</div>
																</div>
																<?php
																$index++;
															} else {
																$DEFAULT_CURRENCY_ID = $row->CurrencyId;
															}
														}
														?>
														<input type="hidden" id="totalCurrencies" name="totalCurrencies"
															   value="<?php echo $index; ?>"/>

														<?php if ($strCurrHdrs != '') { ?>
															<div class="form-group row">
																<div class="col-lg-12">
																	<table cellspacing="1" cellpadding="1" width="100%"
																		   border="0">
																		<tr class="bg-primary">
																			<td width="2%">
																				<input type="checkbox" class="chkSelect"
																					   name="chkCnvrtPrice" id="chkCnvrtPrice"/><span><label
																						for="chkCnvrtPrice"></label></span>
																			</td>
																			<td width="98%" style="font-size:20px;">
																				<b><?php echo $this->lang->line('BE_LBL_406'); ?></b>
																			</td>
																		</tr>
																	</table>

																	<table
																		class="table table-striped table-bordered table-advance table-hover">
																		<thead>
																		<tr>
																			<th><?php echo $this->lang->line('BE_LBL_228'); ?></th>
																			<?php echo $strCurrHdrs; ?>
																		</tr>
																		</thead>
																		<tbody>
																		<?php
																		$rsGroups = fetch_price_plans();
																		$totalGroups = count($rsGroups);
																		$PLAN_PRICES_PER_PACK = getPlansPricesForService($id, $fs);
																		$CURRENCY_RATES = array();
																		$rsCurrRates = fetch_currency_by_id();

																		foreach ($rsCurrRates as $rw) {
																			$CURRENCY_RATES[$rw->CurrencyId] = $rw->ConversionRate;
																		}

																		$i = 0;
																		foreach ($rsGroups as $row) {
																			if (isset($PLAN_PRICES_PER_PACK[$row->PricePlanId][$DEFAULT_CURRENCY_ID])) {
																				$packPrice = $PLAN_PRICES_PER_PACK[$row->PricePlanId][$DEFAULT_CURRENCY_ID];
																			} else {
																				$packPrice = roundMe($price);
																			}
																			?>
																			<tr>
																				<td><?php echo stripslashes($row->PricePlan); ?></td>
																				<td>
																					<input type="hidden"
																						   name="planId<?php echo $i; ?>"
																						   value="<?php echo $row->PricePlanId; ?>"/>
																					<input type="text" onblur="savePrices();"
																						   id="txtPrice<?php echo $i; ?>_0"
																						   name="txtPrice<?php echo $i; ?>_0"
																						   value="<?php echo $packPrice; ?>"
																						   class="form-control">
																				</td>
																				<?
																				$z = 0;
																				foreach ($PACK_PRICES_OTHER as $key => $value) {
																					if (isset($PLAN_PRICES_PER_PACK[$row->PricePlanId][$key])) {
																						$packPrice = $PLAN_PRICES_PER_PACK[$row->PricePlanId][$key];
																					} else if (isset($PLAN_PRICES_PER_PACK[$row->PricePlanId][$DEFAULT_CURRENCY_ID])) {
																						$packPrice = roundMe($PLAN_PRICES_PER_PACK[$row->PricePlanId][$DEFAULT_CURRENCY_ID] * $CURRENCY_RATES[$key]);
																					} else if (isset($PACK_PRICES_OTHER[$key])) {
																						$packPrice = $PACK_PRICES_OTHER[$key];
																					}
																					?>
																					<td>
																						<input type="hidden"
																							   name="otherCurrencyId<?php echo $i; ?>_<?php echo $z + 1; ?>"
																							   value="<?php echo $key; ?>"/>
																						<input type="hidden"
																							   id="cRt<?php echo $i; ?>_<?php echo $z + 1; ?>"
																							   name="cRt<?php echo $i; ?>_<?php echo $z + 1; ?>"
																							   value="<?php echo $CURR_CONV_RATES[$key]; ?>"/>
																						<input type="text"
																							   onblur="savePrices();"
																							   name="txtOtherPrice<?php echo $i; ?>_<?php echo $z + 1; ?>"
																							   id="txtOtherPrice<?php echo $i; ?>_<?php echo $z + 1; ?>"
																							   value="<?php echo $packPrice; ?>"
																							   class="form-control">
																					</td>
																					<?
																					$z++;
																				}
																				?>
																			</tr>
																			<?php
																			$i++;
																		}
																		?>
																		</tbody>
																	</table>
																</div>
															</div>
															<input type="hidden" value="<?php echo $totalGroups; ?>"
																   name="totalGroups" id="totalGroups"/>
															<input type="hidden"
																   value="<?php echo count($PACK_PRICES_OTHER); ?>"
																   name="currencyCount" id="currencyCount"/>
															<input type="hidden"
																   value="<?php echo $DEFAULT_CURRENCY_ID; ?>"
																   name="defaultCurrId"/>
															<input type="hidden" value="0" name="hdSavePrices"
																   id="hdSavePrices"/>
														<?php } ?>
														<div class="form-group row">
															<div class="col-lg-12">
																<button
																	type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
																	class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
															</div>
														</div>

														<br/><br/>
														<?php if ($id > 0) { ?>
															<div class="form-group row">
																<label class="control-label col-lg-2">User
																	Notes:</label>
																<div class="col-lg-6">
																	<textarea class="form-control" id="txtUserNotes"
																			  name="txtUserNotes" rows="6"></textarea>
																</div>
															</div>
															<div class="form-group row">
																<?php $price_plan_data = fetch_price_plan_data(); ?>
																<label
																	class="control-label col-lg-2"><?php echo $this->lang->line('BE_LBL_18') ?>
																	:</label>
																<div class="col-lg-4">
																	<select name="uNplanId" id="uNplanId"
																			class="form-control select2me"
																			data-placeholder="Select...">
																		<option
																			value="0"><?php echo $this->lang->line('BE_LBL_407'); ?></option>
																		<?php FillCombo(0, $price_plan_data); ?>
																	</select>
																</div>
															</div>
															<div class="form-group row">
																<div class="col-lg-2"></div><div class="col-lg-9">
																	<button
																		type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
																		class="btn btn-primary" id="btnPriceEml"><i
																			class="fa fa-envelope"> </i> Send Service
																		Credit Information To Users
																	</button>&nbsp;
																	<img style="display:none;" id="imgEmlLdr"
																		 src="<?php echo base_url(); ?>assets/images/loading.gif"
																		 border="0" alt="Please wait..."/>
																</div>
															</div>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>
								<?php if ($id > 0) { ?>
								<div class="tab-pane" id="tab_2">
									<div class="row">
										<div class="col-lg-12">
											<div class="card">
												<div class="card-header header-elements-inline">
													<h5 class="card-title"> <?php echo $this->lang->line('BE_PCK_15'); ?></h5>
												</div>
											</div>
											<div class="card-body">

												<?php $api_data = fetch_api_data_by_concat(); ?>
												<div class="form-group row">
													<label
														class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PCK_6'); ?>
														:</label>
													<div class="col-lg-9">
														<select name="apiId" id="apiId"
																class="form-control select2me"
																data-placeholder="<?php echo $this->lang->line('BE_LBL_278'); ?>">
															<option
																value="-1~0~0"><?php echo $this->lang->line('BE_LBL_278'); ?></option>
															<?php FillCombo($apiId, $api_data); ?>
														</select>
														<img style="display:none;" id="imgSrvcLoader"
															 src="<?php echo base_url(); ?>/assets/images/loading.gif"
															 border="0" alt="Please wait..."/>
													</div>
												</div>
												<div class="form-group row" id="dvExtNetworkId">
													<?php $services_data = fetch_supplier_services_by_concat($onlyAPIId, $fs); ?>
													<label
														class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_293'); ?>
														:</label>
													<div class="col-lg-9">
														<select name="supplierPackId" id="supplierPackId"
																class="form-control select2me"
																data-placeholder="<?php echo $this->lang->line('BE_LBL_274'); ?>">
															<option
																value="0"><?php echo $this->lang->line('BE_LBL_274'); ?></option>
															<?php FillCombo($extNetworkId, $services_data); ?>
														</select>
														<img style="display:none;" id="imgSrvcToolLoader"
															 src="<?php echo base_url(); ?>/assets/images/loading.gif"
															 border="0" alt="Please wait..."/>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-lg-3 control-label">Service Type At API:</label>
													<div class="col-lg-7 radio-list">
														<div class="form-check form-check-inline mt-0">
															<label class="form-check-label"><input type="radio" class="form-check-input" name="rdSrvcAPIType" value="1" checked/>Instant</label>
														</div>
														<div class="form-check form-check-inline mt-0">
															<label class="form-check-label"><input type="radio" class="form-check-input" name="rdSrvcAPIType" value="2" <?php if ($cronDelayTm != '1') echo 'checked'; ?>/>Non
																Instant</label>
														</div>
													</div>
												</div>
												<div class="form-group row">
													<div class="col-lg-3"></div><div class="col-lg-8">
														<button
															type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
															class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
													</div>
												</div>
												<br/>
												<?php if ($onlyAPIId && $fs == 0) { ?>
													<div class="card">
														<div class="card-header header-elements-inline">
															<h2 class="card-title"> Alternate API</h2>
														</div>
													</div>
													<div class="form-group row">
														<div class="col-lg-12">
                                                                    <span class="form-text text-muted">
                                                                        If main API does not return result, provide here the list of APIs to be connected to in the order. Darg and drop to change the order of connections.
                                                                    </span>
														</div>
													</div>
													<div class="form-group row">
														<div class="col-lg-8">
															<a href="<?php echo base_url('admin/services/alternateapi?id=' . $id . '&sc=' . $fs . '&pn=' . str_replace(' ', 'S_P_', $packageTitle)); ?>"
															   class="fancybox fancybox.ajax btn btn-primary"
															   title="Apply an alternate API, if main API rejects an order then system will automatically apply alternate API with an order."><i
																	class="fa fa-plus"></i> Add</a>
														</div>
													</div>

													<?php if (isset($message) && $message != '') { ?>
														<div class="form-group row">
															<div class="col-lg-12">
																<div
																	class="alert alert-success"><?php echo $message; ?></div>
															</div>
														</div>
													<?php } ?>
													<?php if ($this->input->post_get('jd') && $this->input->post_get('jd') == '1') { ?>
														<div class="form-group row">
															<div class="col-lg-12">
																<div
																	class="alert alert-success"><?php echo $this->lang->line('BE_GNRL_11'); ?></div>
															</div>
														</div>
													<?php } ?>

													<div id="dvMsg" style="display:none;"
														 class="alert alert-success">&nbsp;
													</div>
													<table
														class="table table-striped table-bordered table-advance table-hover">
														<thead>
														<tr class="bg-primary">
															<th nowrap="nowrap" width="4%">Sr. #</th>
															<th><?php echo $this->lang->line('BE_MR_19'); ?></th>
															<th nowrap="nowrap"><?php echo $this->lang->line('BE_PCK_HD_4'); ?></th>
															<th><?php echo $this->lang->line('BE_PCK_14'); ?></th>
															<th></th>
															<th></th>
														</tr>
														</thead>
														<tbody id="sortableLst">
														<?php
														if ($rsAltAPIH) {
															$u = 0;
															foreach ($rsAltAPIH as $row) {
																?>
																<tr id="listItem_<?php echo $row->AltAPISrvcId; ?>">
																	<td nowrap="nowrap"><i class="fa fa-sort"></i>
																		&nbsp;&nbsp;<?php echo $u + 1; ?>.
																	</td>
																	<td><?php echo stripslashes($row->APITitle); ?></td>
																	<td><?php echo stripslashes($row->ServiceName); ?></td>
																	<td><?php echo roundMe($row->ServicePrice); ?></td>
																	<td style="text-align:center" valign="middle">
																		<a href="<?php echo base_url('admin/services/alternateapi?id=' . $id . '&sc=' . $fs . '&pn=' . str_replace(' ', 'S_P_', $packageTitle) . '&altId=' . $row->AltAPISrvcId); ?>"
																		   class="fancybox fancybox.ajax"><i
																				class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a></td>
																	<td style="text-align:center" valign="middle">
																		<a href="<?php echo base_url('admin/services/package?id=' . $id . '&fs=' . $fs . '&altId=' . $row->AltAPISrvcId . '&del=1#tab_2'); ?>"
																		   onclick="return confirm('<?php echo $this->lang->line('BE_LBL_32'); ?>')">
																			<i class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i>
																		</a>
																	</td>
																</tr>
																<?php
																$u++;
															}
														} else
															echo "<tr>
                                                                            <td colspan='7'>" . $this->lang->line('BE_GNRL_9') . "</td>
                                                                        </tr>";
														?>
														</tbody>
													</table>

												<?php } ?>
												<div class="form-group row">
												</div>
												<?php if ($id > 0) { ?>
													<div class="card">
														<div class="card-header header-elements-inline">
															<h5 class="card-title">API History</h5>
														</div>
													</div>

													<table
														class="table table-striped table-bordered table-advance table-hover">
														<thead>
														<tr class="bg-primary">
															<th><?php echo $this->lang->line('BE_MR_19'); ?></th>
															<th nowrap="nowrap"><?php echo $this->lang->line('BE_PCK_HD_4'); ?></th>
															<th><?php echo $this->lang->line('BE_PCK_14'); ?></th>
															<th></th>
														</tr>
														</thead>
														<tbody>
														<?php
														if ($rsAPIHistory) {
															foreach ($rsAPIHistory as $row) {
																?>
																<tr>
																	<td><?php echo stripslashes($row->APITitle); ?></td>
																	<td><?php echo stripslashes($row->ServiceName); ?></td>
																	<td><?php echo roundMe($row->ServicePrice); ?></td>
																	<td>
																		<a href="<?php echo base_url('admin/services/package?fs=' . $fs . '&id=' . $id . '&crAPIId=' . $row->APIId . '&crAPISrvId=' . $row->APIServiceId . '&applyAPI=1#tab_2'); ?>"
																		   onclick="return confirm('Are you sure to apply this API?')"
																		   class="btn btn-default btn-xs">
																			<i class="fa fa-cog"></i> Apply API
																		</a>
																	</td>
																</tr>
															<?php } ?>
														<?php } else { ?>
															<tr>
																<td colspan="5">
																	<strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong>
																</td>
															</tr>
														<?php } ?>
														</tbody>
													</table>

												<?php } ?>

											</div>
										</div>
									</div>


								</div>
								<div class="tab-pane" id="tab_3">
									<div class="row">
										<div class="col-lg-12">
											<div class="card">
												<div class="card-header header-elements-inline">
													<h5 class="card-title"> <?php echo $this->lang->line('BE_LBL_377'); ?></h5>
												</div>
											</div>
											<div class="card-body">
												<div class="form-group row">
													<label
														class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_819'); ?>
														:</label>
													<div class="col-lg-4">
														<select multiple="multiple" class="multi-select"
																id="my_multi_select2" name="customFlds[]">
															<?php FillSelectedList($fs, $id, 'FieldId'); ?>
														</select>
													</div>
												</div>
												<div class="form-group row">
													<div class="col-lg-3"></div><div class="col-lg-4">
														<button
															type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
															class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
								<div class="tab-pane" id="tab_5">
									<div class="row">
										<div class="col-lg-12">
											<div class="card">
												<div class="card-header header-elements-inline">
													<h5 class="card-title"> <?php echo $this->lang->line('BE_LBL_529'); ?></h5>
												</div>
											</div>
											<div class="card-body">
												<?php echo form_open(base_url('admin/services/package'), 'name="frm5" id="frm5" '); ?>
												<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">

												<div class="form-group row">
													<label
														class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_104'); ?>
														:</label>
													<div class="col-lg-5">
														<input type="text" class="form-control"
															   placeholder="Enter File Name" maxlength="100"
															   name="txtFileName" id="txtFileName"
															   value="<?php echo($fName); ?>"/>
													</div>
												</div>
												<div class="form-group row">
													<label
														class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_212'); ?>
														:</label>
													<div class="col-lg-5">
														<input type="text" class="form-control"
															   placeholder="Enter HTML Title" maxlength="100"
															   name="txtHTMLTitle" id="txtHTMLTitle"
															   value="<?php echo($htmlTitle); ?>"/>
													</div>
												</div>
												<div class="form-group row">
													<label
														class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_213'); ?>
														:</label>
													<div class="col-lg-5">
														<input type="text" class="form-control"
															   placeholder="Enter SEO Name" maxlength="100"
															   name="txtSEOName" id="txtSEOName"
															   value="<?php echo($seoName); ?>"/>
													</div>
												</div>
												<div class="form-group row">
													<label
														class="col-lg-3 control-label"><?php echo $this->lang->line('BE_MR_38'); ?>
														:</label>
													<div class="col-lg-5">
															<textarea class="form-control" rows="3" id="txtMetaTags"
																	  name="txtMetaTags"><?php echo $metaTags; ?></textarea>
													</div>
												</div>
												<div class="form-group row">
													<label
														class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_214'); ?>
														:</label>
													<div class="col-lg-5">
															<textarea class="form-control" rows="3" id="txtMetaKW"
																	  name="txtMetaKW"><?php echo $metaKW; ?></textarea>
													</div>
												</div>
												<div class="form-group row">
													<div class="col-lg-3"></div><div class="col-lg-5">
														<button
															type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
															class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
													</div>
												</div>
												<?php echo form_close(); ?>
											</div>
										</div>
									</div>

								</div>
								<div class="tab-pane" id="tab_7">
									<div class="row">
										<div class="col-lg-12">
											<div class="card">
												<div class="card-header header-elements-inline">
													<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_653'); ?></h5>
												</div>
											</div>
											<div class="card-body">

												<div class="form-group row">
													<label
														class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_653'); ?>
														:</label>
													<div class="col-lg-5">
															<textarea class="form-control" rows="16" id="txtPreCodes"
																	  name="txtPreCodes"><?php echo $preCodes; ?></textarea>
													</div>
												</div>
												<div class="form-group row">
													<label
														class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_654'); ?>
														:</label>
													<div class="col-lg-4">
														<div class="switch-button switch-button-lg"
															 data-on-label="YES" data-off-label="NO">
															<input type="checkbox" class="chkSelect"
																   name="chkPreCodes" id="chkPreCodes"
																   class="toggle" <?php if ($calPreCodes == '1') echo 'checked'; ?> />
															<span><label for="chkPreCodes"></label></span>
														</div>
													</div>
												</div>
												<div class="form-group row">
													<div class="col-lg-3"></div><div class="col-lg-4">
														<button
															type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
															class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab_8">
									<div class="row">
										<div class="col-lg-12">
											<div class="card">
												<div class="card-header header-elements-inline">
													<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_689'); ?></h5>
												</div>
											</div>
											<div class="card-body">

												<div class="form-group row">
													<?php $api_data = get_api_data(); ?>
													<label
														class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_691'); ?>
														:</label>
													<div class="col-lg-9">
														<select name="apiIdForBrand" id="apiIdForBrand"
																class="form-control select2me"
																data-placeholder="<?php echo $this->lang->line('BE_LBL_278'); ?>">
															<option
																value="0"><?php echo $this->lang->line('BE_LBL_278'); ?></option>
															<?php FillCombo($apiIdForBrand, $api_data); ?>
														</select><img style="display:none;" id="imgBrndLoader"
																	  src="<?php echo base_url('assets/images/loading.gif'); ?>"
																	  border="0" alt="Please wait..."/>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-lg-3 control-label">&nbsp;</label>
													<div class="col-lg-6">
														<table cellpadding="1" cellspacing="1" width="100%">
															<tr>
																<td width="50%" valign="top">
																	<button
																		type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
																		class="btn btn-primary"
																		id="btnModels" <?php if ($apiIdForBrand == 0) echo 'style="display:none"'; ?>>
																		Get Models
																	</button>
																	<img style="display:none;" id="imgMdlsLoader"
																		 src="<?php echo base_url('assets/images/loading.gif'); ?>"
																		 border="0" alt="Please wait..."/>
																	<div class="col-lg-9"
																		 id="dvBrands" <?php if ($apiIdForBrand == 0) echo 'style="display:none"'; ?>></div>
																	<input type="hidden" id="totalBrands"
																		   name="totalBrands" value="0"/>
																</td>
																<td width="50%" valign="top">
																	<div class="col-lg-9"
																		 id="dvModels" <?php if ($apiIdForBrand == 0) echo 'style="display:none"'; ?>></div>
																</td>
															</tr>
														</table>
													</div>
												</div>
												<div class="form-group row" id="dvBrandsMain">
												</div>
												<div class="form-group row"
													 id="dvModelsMain" <?php if ($apiIdForBrand == 0) echo 'style="display:none"'; ?>>
												</div>

											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab_9">
									<div class="row">
										<div class="col-lg-12">
											<div class="card">
												<div class="card-header header-elements-inline">
													<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_694'); ?></h5>
												</div>
											</div>
											<div class="card-body">
												<div class="form-group row">
													<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_695'); ?>:</label>
													<div class="col-lg-4">
														<div class="switch-button switch-button-lg data-on-label="YES" data-off-label="NO">
														<input type="checkbox" name="chkSMS" id="chkSMS" <?php echo $sendSMS == 1 ? 'checked' : ''; ?> class="toggle"/>
														<span>
																	<label for="chkSMS"></label>
																</span>
													</div>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-lg-3"></div><div class="col-lg-4">
													<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="tab_10">
								<div class="row">
									<div class="col-lg-12">
										<div class="card">
											<div class="card-header header-elements-inline">
												<h5 class="card-title"><?php echo $this->lang->line('BE_MENU_USRS'); ?></h5>
											</div>
										</div>
										<div class="card-body">
											<div class="form-group row">
												<img id="dvDiscClientsLdr" align="middle"
													 src="<?php echo base_url('assets/images/loading.gif'); ?>"
													 border="0" alt="Please wait..."/>
											</div>
											<div class="table-responsive" id="dvDiscClientsData"></div>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/plugins/ckeditor/ckeditor.js'); ?>"></script>
<script language="javascript" src="<?php echo base_url('assets/js/package.js'); ?>"></script>
