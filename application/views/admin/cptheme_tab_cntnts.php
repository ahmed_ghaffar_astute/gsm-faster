<?
$cnt1 = $clrsCnts[0];
$cnt2 = $clrsCnts[1];
$cnt3 = $clrsCnts[2];
$cnt4 = $clrsCnts[3];
$cnt5 = $clrsCnts[4];
$cnt6 = $clrsCnts[5];
$cnt7 = $clrsCnts[6];
$cnt8 = $clrsCnts[7];
$cnt9 = $clrsCnts[8];
$cnt10 = $clrsCnts[9];
$cnt11 = $clrsCnts[10];
$cnt12 = $clrsCnts[11];
$cnt13 = $clrsCnts[12];
$cnt14 = $clrsCnts[13];
$cnt15 = $clrsCnts[14];
$cnt16 = $clrsCnts[15];
?>
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_752'); ?></h5>
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label class="col-lg-4 control-label">Grid Header Background Color:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $cnt1; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="cnt1" value="<? echo $cnt1; ?>"
								   name="txtCnt1">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('cnt1').jscolor.show()"
									style="background-color: <? echo $cnt1; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label">Grid Header Link Color:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $cnt2; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="cnt2" value="<? echo $cnt2; ?>"
								   name="txtCnt2">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('cnt2').jscolor.show()"
									style="background-color: <? echo $cnt2; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label">Grid Header Heading Color:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $cnt3; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="cnt3" value="<? echo $cnt3; ?>"
								   name="txtCnt3">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('cnt3').jscolor.show()"
									style="background-color: <? echo $cnt3; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label">Table Header Row Background Color:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $cnt4; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="cnt4" value="<? echo $cnt4; ?>"
								   name="txtCnt4">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('cnt4').jscolor.show()"
									style="background-color: <? echo $cnt4; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label">Table Header Row Text Color:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $cnt5; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="cnt5" value="<? echo $cnt5; ?>"
								   name="txtCnt5">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('cnt5').jscolor.show()"
									style="background-color: <? echo $cnt5; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label">Page Title Background Color:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $cnt6; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="cnt6" value="<? echo $cnt6; ?>"
								   name="txtCnt6">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('cnt6').jscolor.show()"
									style="background-color: <? echo $cnt6; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label">Page Title Heading Color:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $cnt7; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="cnt7" value="<? echo $cnt7; ?>"
								   name="txtCnt7">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('cnt7').jscolor.show()"
									style="background-color: <? echo $cnt7; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label">Tabs Background Color:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $cnt8; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="cnt8" value="<? echo $cnt8; ?>"
								   name="txtCnt8">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('cnt8').jscolor.show()"
									style="background-color: <? echo $cnt8; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label">Selected Tab Background Color:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $cnt9; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="cnt9" value="<? echo $cnt9; ?>"
								   name="txtCnt9">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('cnt9').jscolor.show()"
									style="background-color: <? echo $cnt9; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>

				<div class="form-group row">
					<label class="col-lg-4 control-label">Selected Tab Text Color:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $cnt10; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="cnt10" value="<? echo $cnt10; ?>"
								   name="txtCnt10">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('cnt10').jscolor.show()"
									style="background-color: <? echo $cnt10; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label">Tab Heading Color:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $cnt11; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="cnt11" value="<? echo $cnt11; ?>"
								   name="txtCnt11">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('cnt11').jscolor.show()"
									style="background-color: <? echo $cnt11; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label">Small Buttons Background:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $cnt12; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="cnt12" value="<? echo $cnt12; ?>"
								   name="txtCnt12">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('cnt12').jscolor.show()"
									style="background-color: <? echo $cnt12; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label">Small Buttons Text:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $cnt13; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="cnt13" value="<? echo $cnt13; ?>"
								   name="txtCnt13">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('cnt13').jscolor.show()"
									style="background-color: <? echo $cnt13; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label">Page Sub Heading:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $cnt14; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="cnt14" value="<? echo $cnt14; ?>"
								   name="txtCnt14">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('cnt14').jscolor.show()"
									style="background-color: <? echo $cnt14; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label">Submit Buttons Background Color:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $cnt15; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="cnt15" value="<? echo $cnt15; ?>"
								   name="txtCnt15">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('cnt15').jscolor.show()"
									style="background-color: <? echo $cnt15; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label">Submit Buttons Text Color:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $cnt16; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="cnt16" value="<? echo $cnt16; ?>"
								   name="txtCnt16">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('cnt16').jscolor.show()"
									style="background-color: <? echo $cnt16; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
