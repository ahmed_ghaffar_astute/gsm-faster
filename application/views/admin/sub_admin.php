<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i>Sub Administrator</h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-6">
							<label class="control-label">Username</label>
							<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
							<input type="email" placeholder="Enter Username" maxlength="50" name="txtEmail"  id="txtEmail" value="<?php echo($email); ?>" class="form-control" autocomplete="off">
						</div>
						<div class="col-lg-6">
							<label class="control-label">Admin Role</label>
							<select name="roleId" id="roleId" class="form-control select2me"
									data-placeholder="Select Role">
								<option value="0">Please Select Admin Role</option>
								<?php FillCombo($roleId, $roles); ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label class="control-label">Password</label>
							<input type="password" autocomplete="off" placeholder="Password" name="txtPass" id="txtPass" maxlength="15" class="form-control" autocomplete="off"/>
						</div>
						<div class="col-lg-6">
							<label class="control-label">Confirm Password</label>
							<input type="password" autocomplete="off" placeholder="Confirm Password" name="txtCPass" id="txtCPass" maxlength="15" class="form-control"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label class="control-label">First Name</label>
							<input type="text" placeholder="Enter First Name" name="txtFName" id="txtFName"
								   maxlength="50" value="<?php echo($firstName); ?>" class="form-control">
						</div>
						<div class="col-lg-6">
							<label class="control-label">Last Name</label>
							<input type="email" placeholder="Enter Last Name" maxlength="50" name="txtLName"
								   id="txtLName" value="<?php echo($lastName); ?>" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label class="control-label">Phone</label>
							<input type="text" placeholder="Enter Phone" name="txtPhone" id="txtPhone"
								   maxlength="30" value="<?php echo($phone); ?>" class="form-control">
						</div>
						<div class="col-lg-6">
							<label class="control-label">Disable:</label><br/>
							<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
								<input type="checkbox" name="chkDisable"  id="chkDisable" <?php echo $disable == 1 ? 'checked' : ''; ?> class="toggle"/><span><label for="chkDisable"></label></span>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<div>
								<div class="switch-button switch-button-lg">
									<input type="checkbox" name="chkGAuth"
										   id="chkGAuth" <?php echo $gAuth == 1 ? 'checked' : ''; ?> />
									<span><label for="chkGAuth"></label></span>
								</div>
								<label class="control-label">Google Authenticator</label>
								<div id="imgGAuth" <?php if ($gAuth == '0') echo 'style="display:none;"'; ?>>
									<img src='<?php echo $qrCodeUrl; ?>'/>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<input type="hidden" id="gAuthSecret" name="gAuthSecret" value="<?php echo $secret; ?>"/>
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary"
									id="btnSave" name="btnSave"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
							<img style="display:none;" id="statusLoader"
								 src="<?php echo base_url('assets/images/loading.gif'); ?>" border="0"
								 alt="Please wait..."/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	<script language="javascript" src="<?php echo base_url('assets/js/subadmin.js'); ?>"></script>
