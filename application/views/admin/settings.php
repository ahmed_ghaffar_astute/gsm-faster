<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_MR_HD_8'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<div class="form-group row">
						<div class="col-lg-12">
							<ul class="nav nav-tabs nav-tabs-solid rounded">
								<li class="nav-item">
									<a href="#tab_0" data-toggle="tab" class="nav-link active"><?php echo $this->lang->line('BE_LBL_380'); ?></a>
								</li>
								<li class="nav-item">
									<a href="#tab_1" data-toggle="tab"
									   class="nav-link"><?php echo $this->lang->line('BE_PCK_HD_5'); ?></a>
								</li>
								<li class="nav-item">
									<a href="#tab_2" data-toggle="tab" class="nav-link"><?php echo $this->lang->line('BE_MENU_USRS'); ?></a>
								</li>
								<li class="nav-item">
									<a href="#tab_4" data-toggle="tab"
									   class="nav-link"><?php echo $this->lang->line('BE_LBL_596'); ?></a>
								</li>
								<li class="nav-item">
									<a href="#tab_5" data-toggle="tab"
									   class="nav-link"><?php echo $this->lang->line('BE_LBL_29'); ?></a>
								</li>
								<li class="nav-item">
									<a href="#tab_6" data-toggle="tab"
									   class="nav-link"><?php echo $this->lang->line('BE_LBL_678'); ?></a>
								</li>
								<li class="nav-item">
									<a href="#tab_7" data-toggle="tab"
									   class="nav-link"><?php echo $this->lang->line('BE_LBL_685'); ?></a>
								</li>
								<li class="nav-item">
									<a href="#tab_8" data-toggle="tab"
									   class="nav-link"><?php echo $this->lang->line('BE_LBL_694'); ?></a>
								</li>
								<li class="nav-item">
									<a href="#tab_10" data-toggle="tab"
									   class="nav-link"><?php echo $this->lang->line('BE_LBL_857'); ?></a>
								</li>
								<?
								if ($geniePay == '1')
									echo '<li class="nav-item"><a href="#tab_9" data-toggle="tab">' . $this->lang->line('BE_LBL_821') . '</a></li>';
								?>
							</ul>

							<?php echo form_open('', array('name' => 'frmSettings', 'id' => 'frmSettings', 'class' => 'form-horizontal')); ?>

							<div class="tab-content">
								<div class="tab-pane active" id="tab_0">
									<?php echo $settings_tab_general; ?>
								</div>
								<div class="tab-pane" id="tab_1">
									<?php echo $settings_tab_services; ?>
								</div>
								<div class="tab-pane" id="tab_2">
									<?php echo $settings_tab_user; ?>
								</div>
								<!-- <div class="tab-pane" id="tab_3">
										<?php //echo $settings_tab_autopmnts; ?>
									</div>-->
								<div class="tab-pane" id="tab_4">
									<?php echo $settings_tab_layout; ?>
								</div>
								<div class="tab-pane" id="tab_5">
									<?php echo $settings_tab_nl; ?>
								</div>
								<div class="tab-pane" id="tab_6">
									<?php echo $settings_tab_srvc_emails; ?>
								</div>
								<div class="tab-pane" id="tab_7">
									<?php echo $settings_tab_ip; ?>
								</div>
								<div class="tab-pane" id="tab_8">
									<?php echo $settings_tab_sms; ?>
								</div>
								<div class="tab-pane" id="tab_10">
									<?php echo $settings_tab_smtp; ?>
								</div>
								<?php if ($geniePay == '1') { ?>
									<div class="tab-pane" id="tab_9">
										<?php echo $settings_tab_geniepay; ?>
									</div>
								<?php } ?>
							</div>

							<?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script language="JavaScript" src="<?php echo base_url('assets/js/functions.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/emailsettings.js') ?>"></script>

