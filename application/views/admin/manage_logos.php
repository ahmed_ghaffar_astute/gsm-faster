<?php $THEME=$this->data['rsStngs']->Theme; ?>
<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_867'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<input type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="Add a Brand Logo" onclick="window.location.href='<?php echo base_url('admin/cms/') ?>addbrandlogo?frmId=<?php echo $this->input->post_get('frmId');?>&fTypeId=<?php echo $this->input->post_get('fTypeId')?>'" class="btn btn-primary" />
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">

	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<!-- Copy till this to paste to other pages -->
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-advance table-hover">
										<thead>
										<tr class="bg-primary">
											<th></th>
											<th></th>
											<th></th>
										</tr>
										</thead>
										<tbody>
										<?php
										if($rsData)
										{
											foreach($rsData as $row)
											{
												?>
												<tr>
													<td><img src="<?php echo base_url().'uplds'.$THEME;?>/<?php echo $row->BrandImage; ?>" /></td>
													<td style="text-align:center" valign="middle">
														<a href="<?php echo base_url('admin/cms/');?>addbrandlogo?id=<?php echo $row->Id;?>frmId=<?php echo $this->input->post_get('frmId');?>&fTypeId=<?php echo $this->input->post_get('fTypeId')?>" ><i class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> </a>
													</td>
													<td style="text-align:center" valign="middle">
														<a href="<?php echo base_url('admin/cms/') ?>brandlogos?del=1&id=<?php echo($row->Id); ?>&frmId=<?php echo $this->input->post_get('frmId');?>&fTypeId=<?php echo $this->input->post_get('fTypeId')?>" onclick="return confirm('<?php echo $this->lang->line('BE_LBL_32'); ?>')"><i class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></td>
												</tr>
											<?php }?>
										<?php }else{ ?>
											<tr><td colspan="7"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td></tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

