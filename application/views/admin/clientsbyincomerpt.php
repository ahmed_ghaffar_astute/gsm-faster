<?php
foreach ($rsUsers as $row) {
	$USERS_CURRENCY[$row->UserName] = $row->CurrencySymbol;
	$USERS_NAME[$row->UserName] = $row->Name;
	$USERS_EMAIL[$row->UserName] = $row->UserEmail;
}
?>
<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_817'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>

					<?php echo form_open(base_url() . 'admin/reports/clientsbyincomerpt?frmId=86&fTypeId=15', array('class' => "horizontal-form", 'method' => "post")); ?>
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label"><? echo $this->lang->line('BE_PM_3'); ?></label>
								<input type="text" name="txtUName" value="<? echo($uName); ?>" class="form-control">
							</div>
						</div>
						<div class="col-lg-6">
							<label class="control-label"><? echo $this->lang->line('BE_LBL_345'); ?></label>
							<input type="text" name="txtEmail" value="<? echo($email); ?>" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary"><? echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
							<input type="hidden" name="userId" id="userId" value="<? echo $userId; ?>"/>
						</div>
					</div>
					<?php echo form_close(); ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="page-title d-flex">
								<h4><i class="mr-2"></i> <?php echo $this->lang->line('BE_LBL_817'); ?></h4>
								<a href="#" class="header-elements-toggle text-default d-md-none"><i
										class="icon-more"></i></a>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th><? echo $this->lang->line('BE_USR_1'); ?></th>
										<th><? echo $this->lang->line('BE_USR_9'); ?></th>
										<th><? echo $this->lang->line('BE_LBL_345'); ?></th>
										<th style="text-align:right;"><? echo $this->lang->line('BE_PM_11'); ?></th>
									</tr>
									</thead>
									<tbody>
									<?
									if (count($USER_AMOUNTS) != 0) {
										arsort($USER_AMOUNTS);
										foreach ($USER_AMOUNTS as $key => $value) {
											?>
											<tr>
												<td><? echo stripslashes($key); ?></td>
												<td><? echo $USERS_NAME[stripslashes($key)]; ?></td>
												<td><? echo $USERS_EMAIL[stripslashes($key)]; ?></td>
												<td align="right"><? echo $USERS_CURRENCY[stripslashes($key)] . ' ' . roundMe($value); ?></td>
											</tr>
											<?

										}
									} else
										echo "<tr><td colspan='4'>" . $this->lang->line('BE_GNRL_9') . "</td></tr>";
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
