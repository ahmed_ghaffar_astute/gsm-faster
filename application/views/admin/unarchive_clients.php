<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i>Search Archived Client(s)</h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<?php echo form_open(base_url('admin/clients/unarchiveclients?frmId=301&fTypeId=5'), array('class' => 'form-horizontal', 'name' => 'frmSearch', 'id' => 'frmSearch')); ?>
					<div class="form-group row">
						<div class="col-lg-6">

							<label class="control-label"><?php echo $this->lang->line('BE_USR_4'); ?></label>
							<input type="text" placeholder="Enter First Name" name="txtFName"
								   value="<?php echo($fName); ?>" class="form-control">
						</div>
						<div class="col-lg-6">

							<label class="control-label"><?php echo $this->lang->line('BE_USR_5'); ?></label>
							<input type="text" name="txtLName" placeholder="Enter Last Name"
								   value="<?php echo($lName); ?>"
								   class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">

							<label class="control-label"><?php echo $this->lang->line('BE_USR_1'); ?></label>
							<input type="text" name="txtUName" placeholder="Enter Username"
								   value="<?php echo($uName); ?>"
								   class="form-control">
						</div>
						<div class="col-lg-6">

							<label class="control-label"><?php echo $this->lang->line('BE_LBL_345'); ?></label>
							<input type="text" name="txtEmail" placeholder="Enter User Email"
								   value="<?php echo($email); ?>"
								   class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<div>
								<button type="submit"
										name="btnSubmit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
										class="btn btn-primary"
										onclick="setValue('0');"><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
							</div>
						</div>
					</div>
					<?php echo form_close(); ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<?php if ($rsClients) { ?>
								<div class="form-group" align="right">
									<label class="control-label">&nbsp;</label><br/>
									<input type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
										   value="Un-Archive Client(s)" onclick="setValue('1');"
										   class="btn btn-primary"
										   name="btnSubmit"/>
								</div>
							<?php } ?>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th><?php echo $this->lang->line('BE_USR_1'); ?></th>
										<th><?php echo $this->lang->line('BE_LBL_345'); ?></th>
										<th><?php echo $this->lang->line('BE_USR_9'); ?></th>
										<th><?php echo $this->lang->line('BE_USR_10'); ?></th>
										<?php if ($rsClients) { ?>
											<th style="text-align:center;"><input type="checkbox" class="chkSelect"
																				  id="chkSelect"
																				  name="chkSelect"
																				  onClick="selectAllChxBxs('chkSelect', 'chkUsers', <?php echo $count; ?>);"
																				  value="true">
											</th>
										<?php } ?>
									</tr>
									</thead>
									<tbody>
									<?php
									if ($rsClients) {
										$i = 0;
										foreach ($rsClients as $row) {
											$key = crypt_key($row->UserId);
											$fontColor = '';
											$myCredits = decrypt($row->Credits, $key);
											if ($myCredits == '')
												$myCredits = '-';
											else
												$myCredits = $row->CurrencyAbb . ' ' . roundMe($myCredits);
											?>
											<tr>
												<td class="highlight">
													<div class="success"></div>
													<a href="<?php echo base_url('admin/clients/'); ?>user?id=<?php echo $row->UserId; ?>"><?php echo stripslashes($row->UserName); ?></a>
												</td>
												<td><?php echo($row->UserEmail); ?></td>
												<td><?php echo stripslashes($row->Name); ?></td>
												<td><?php echo $myCredits; ?></td>
												<td align="center">
													<input type="checkbox" class="chkSelect"
														   id="chkUsers<?php echo $i; ?>"
														   name="chkUsers[]" value="<?php echo $row->UserId; ?>"/>
												</td>
											</tr>
											<?php
											$i++;
										}
									} else
										echo "<tr><td colspan='4'>" . $this->lang->line('BE_GNRL_9') . "</td></tr>";
									?>
									<input type="hidden" value="0" name="cldFrm" id="cldFrm"/>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function setValue(i) {
		document.getElementById('cldFrm').value = i;
	}
</script>
