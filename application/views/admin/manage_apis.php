<div class="row">
	<div class="col-md-10">
		<? if($message != '')
			echo '<br /><br /><div class="alert alert-success">'.$message.'</div>'; ?>
		<h3 class="page-title"><?php echo $this->lang->line('MR_HD_5'); ?></h3>
	</div>
	<div class="col-md-2" style="text-align:right;">
		<a href="apis.php?frmId=<? echo $this->input->post_get('frmId');?>&fTypeId=<? echo $this->input->post_get('fTypeId')?>" class="btn btn default"><span class="fa fa-angle-double-left"></span> Back To List</a>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="tabbable tabbable-custom boxless">
			<input type="hidden" id="id" name="id" value="<? echo($id); ?>">
			<input type="hidden" id="url" name="url" value="<? echo($url); ?>">
			<input type="hidden" id="apiKey" name="apiKey" value="<? echo($apiKey); ?>">
			<input type="hidden" id="userName" name="userName" value="<? echo($accountId); ?>">
			<ul class="nav nav-tabs nav-tabs-solid border-0">
				<li class="active">
					<a href="#tab_0" data-toggle="tab"><? echo $this->lang->line('LBL_380'); ?></a>
				</li>
				<? if($id > 0 && $id != '206' && $IMEI_API == '1') { ?>
					<li>
						<a href="#tab_1" data-toggle="tab"><? echo $this->lang->line('LBL_506').' - ' .$this->lang->line('MENU_PCKGS'); ?></a>
					</li>
				<? }
				if($id > 0 && $FILE_API == '1')
				{
					echo '<li><a href="#tab_3" data-toggle="tab">Synchronize - FILE Services</a></li>';
				}
				if($id > 0 && $SERVER_API == '1')
				{
					echo '<li><a href="#tab_4" data-toggle="tab">Synchronize - SERVER Services</a></li>';
				}
				if($id > 0) { ?>
					<li>
						<a href="#tab_2" data-toggle="tab"><? echo $this->lang->line('LBL_619'); ?></a>
					</li>
				<? } ?>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab_0">
					<? include 'api_tab_general.php'; ?>
				</div>
				<? if($id > 0 && $id != '206' && $IMEI_API == '1') { ?>
					<div class="tab-pane" id="tab_1">
						<? include 'api_tab_sync.php'; ?>
					</div>
				<? } ?>
				<? if($id > 0 && $FILE_API == '1') { ?>
					<div class="tab-pane" id="tab_3">
						<? include 'api_file_sync.php'; ?>
					</div>
				<? } ?>
				<? if($id > 0 && $SERVER_API == '1') { ?>
					<div class="tab-pane" id="tab_4">
						<? include 'api_server_sync.php'; ?>
					</div>
				<? } ?>
				<? if($id > 0) { ?>
					<div class="tab-pane" id="tab_2">
						<? include 'api_tab_crons.php'; ?>
					</div>
				<? } ?>
			</div>
		</div>
	</div>
</div>

<script language="javascript" src="<?php echo base_url()?>js/api.js"></script>
