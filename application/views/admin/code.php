<div class="page-content">
    <div class="row">
        <div class="col-md-4">
            <h3 class="page-title"><?php echo $this->lang->line('BE_LBL_530'); ?> - #<?php  echo $id?></h3>
        </div>
        <div class="col-md-8"  style="text-align:right;">
            <a href="<?php echo base_url('admin/services/codes?bulk=1&searchType=3&frmId=99&fTypeId=16'); ?>" class="btn btn-primary">
                <span class="fa fa-angle-double-left"></span> 
                Back To List
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block-web full">
                <?php if($message != '')
                    echo '<div class="alert alert-success">'.$message.'</div>'; 
                ?>
                <ul class="nav nav-tabs nav-justified">
                    <li class="active">
                        <a href="#tab_0" data-toggle="tab">Order</a>
                    </li>
                    <li>
                        <a href="#tab_1" data-toggle="tab">Send Custom Email</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_0">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-reorder"></i>
                                    <h3>
                                        <?php echo $this->lang->line('BE_LBL_530'); ?>
                                    </h3>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <form class="form-horizontal" name="frm" method="post" id="frm">
                                <?php echo form_open(base_url('admin/services/code') , 'name="frm" id="frm" ') ?>
                                    <input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
                                    <input type="hidden" id="userId" name="userId" value="<?php echo(isset($userId)); ?>">
                                    <input type="hidden" id="codeCr" name="codeCr" value="<?php echo(isset($credits)); ?>">
                                    <input type="hidden" id="imei" name="imei" value="<?php echo(isset($imeiNo)); ?>">
                                    <input type="hidden" id="packageId" name="packageId" value="<?php echo(isset($packageId)); ?>">
                                    <input type="hidden" name="pckTitle" value="<?php echo isset($pckTitle); ?>" />
                                    <input type="hidden" id="SMS_Pack" name="SMS_Pack" value="<?php echo(isset($SMS_Pack)); ?>">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_PCK_1'); ?>:</label>
                                            <div class="col-md-6">
                                                <p class="form-control-static"><?php echo isset($pckTitle); ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_INDEX_USRNAME'); ?>:</label>
                                            <div class="col-md-6">
                                                <p class="form-control-static"><?php echo isset($userName); ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_CODE_1'); ?>:</label>
                                            <div class="col-md-6">
                                                <p class="form-control-static"><?php echo isset($imeiNo); ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php  echo $this->lang->line('BE_LBL_163'); ?>:</label>
                                            <div class="col-md-6">
                                                <p class="form-control-static"><?php  echo isset($orderDt); ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_USR_10'); ?>:</label>
                                            <div class="col-md-6">
                                                <p class="form-control-static"><?php  echo isset($credits); ?></p>
                                            </div>
                                        </div>
                                        <?php
                                        $i = 1;
                                        foreach($rsFields as $rowFld)
                                        {
                                            $fldColName = $rowFld->FieldColName;
                                            if(isset($ROW_IMEI_ORDER->$fldColName) && $ROW_IMEI_ORDER->$fldColName != '')
                                            {
                                        ?>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label"><?php  echo $rowFld->FieldLabel; ?>:<?php  if($rowFld->Mandatory == '1') echo '*';?></label>
                                                    <div class="col-md-4 <?php  if($rowFld->FieldType == 'Radio Button') echo 'radio-list'; ?>">
                                                        <?php  if($rowFld->FieldType == 'Text Box') { ?>
                                                            <input type='text' value="<?php  echo $ROW_IMEI_ORDER->$fldColName; ?>" placeholder="Enter <?php  echo $rowFld->FieldLabel; ?>" name="fld<?php  echo $i; ?>" maxlength="100" class="form-control" />
                                                        <?php  } else if($rowFld->FieldType == 'Text Area') { ?>
                                                            <textarea name="fld<?php  echo $i; ?>" class="form-control"><?php  echo $ROW_IMEI_ORDER->$fldColName; ?></textarea>
                                                        <?php  } else if($rowFld->FieldType == 'Drop Down') { ?>
                                                            <select name="fld<?php  echo $i; ?>" class="form-control select2me" data-placeholder="<?php  echo $rowFld->FieldLabel; ?>">
                                                            <?php
                                                                $rsValues = get_reg_field_values($rowFld->FieldId);
                                                            
                                                                foreach($rsValues as $rw)
                                                                {
                                                                    $slctd = '';
                                                                    if($ROW_IMEI_ORDER->$fldColName == $rw->RegValue)
                                                                        $slctd = 'selected';
                                                                    echo '<option value="'.$rw->RegValue.'" '.$slctd.'>'.$rw->RegValue.'</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        <?php  } else if($rowFld->FieldType == 'Radio Button') { ?>
                                                            <?php
                                                                $rsValues = get_reg_field_values($rowFld->FieldId);
                                                                foreach($rsValues as $rw)
                                                                {
                                                                    echo '<label class="radio-inline"><input type="radio" class="form-control" name="fld'.$i.'" checked="checked" value="'.$rw->RegValue.'">'.$rw->RegValue.'</label>';
                                                                }
                                                            }
                                                echo '</div></div>';
                                                echo '<input type="hidden" name="colNm'.$i.'" value="'.$rowFld->FieldColName.'" />';
                                                echo '<input type="hidden" name="lbl'.$i.'" value="'.$rowFld->FieldLabel.'" />';
                                                $i++;
                                            }
                                        } ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php  echo $this->lang->line('BE_LS_3'); ?>:*</label>
                                            <?php $code_data=get_code_status(); ?>
                                            <div class="col-md-4">
                                                <select id="codeStatusId" name="codeStatusId" class="form-control select2me" data-placeholder="Choose...">
                                                    <?php  FillCombo($codeStatusId, $code_data); ?>
                                                    <option value="5">Resubmit the Order</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_LBL_163'); ?>:</label>
                                            <div class="col-md-6">
                                                <p class="form-control-static"><?php echo isset($orderDt); ?></p>
                                            </div>
                                        </div>
                                        <?php if(isset($replyDtTm) &&   $replyDtTm != '') { ?>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_LBL_601'); ?>:</label>
                                                <div class="col-md-6">
                                                    <p class="form-control-static"><?php echo isset($replyDtTm) ?: ''; ?></p>
                                                </div>
                                            </div>
                                        <?php  } ?>
                                        <?php  if(isset($other) && $other != '') { ?>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"><?php  echo $this->lang->line('BE_LBL_531'); ?>:</label>
                                                <div class="col-md-6">
                                                    <p class="form-control-static"><?php  echo isset($other) ?: '' ; ?></p>
                                                </div>
                                            </div>
                                        <?php  } ?>
                                        <?php  if(isset($prd) && $prd != '') { ?>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_LBL_141'); ?>:</label>
                                                <div class="col-md-6">
                                                    <p class="form-control-static"><?php echo isset($prd) ?: '' ; ?></p>
                                                </div>
                                            </div>
                                        <?php  } ?>
                                        <?php  if(isset($serialNo) && $serialNo != '') { ?>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"><?php  echo $this->lang->line('BE_CODE_10'); ?>:</label>
                                                <div class="col-md-6">
                                                    <p class="form-control-static"><?php  echo $serialNo; ?></p>
                                                </div>
                                            </div>
                                        <?php  } if(isset($modelNo) && $modelNo != '') { ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php  echo $this->lang->line('BE_CODE_9'); ?>:</label>
                                            <div class="col-md-6">
                                                <p class="form-control-static"><?php  echo $modelNo; ?></p>
                                            </div>
                                        </div>
                                        <?php  } ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php  echo $this->lang->line('BE_LBL_147'); ?>:</label>
                                            <div class="col-md-6">
                                                <p class="form-control-static"><?php echo isset($sent2OtherSrvr) && $sent2OtherSrvr  == 1 ? 'Yes' : 'No'; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">API:</label>
                                            <div class="col-md-6">
                                                <p class="form-control-static"><?php  echo isset($apiName) && $apiName != '' ? $apiName : '-'; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php  echo $this->lang->line('BE_CD_3'); ?>:</label>
                                            <div class="col-md-6">
                                                <p class="form-control-static"><?php  echo isset($orderIdFrmServer) && $orderIdFrmServer  != '' ? $orderIdFrmServer : '-'; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php  echo $this->lang->line('BE_LBL_146'); ?>:</label>
                                            <div class="col-md-6">
                                                <p class="form-control-static"><?php  echo isset($msg4mSrvr) && $msg4mSrvr  != '' ? $msg4mSrvr : '-'; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Last Updated By:</label>
                                            <div class="col-md-6">
                                                <p class="form-control-static"><?php  echo isset($updatedBy) && $updatedBy != '' ? $updatedBy : '-'; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php  echo $this->lang->line('BE_CODE_6'); ?>:</label>
                                            <div class="col-md-6">
                                                <textarea class="form-control" rows="3" id="txtCode" name="txtCode"><?php  echo replaceBRTag(isset($code)); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_LBL_362'); ?>:</label>
                                            <div class="col-md-6">
                                                <textarea class="form-control" rows="3" id="txtNotes" name="txtNotes"><?php  echo isset($notes) ?: ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_GNRL'); ?>:</label>
                                            <div class="col-md-6">
                                                <textarea class="form-control" rows="3" id="txtComments" name="txtComments"><?php  echo isset($comments) ?: ''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-3"></div><div class="col-lg-9">
                                            <input type="hidden" name="totalCustomFields" value="<?php  echo $i-1; ?>" />
                                            <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><?php  echo $this->lang->line('BE_LBL_72'); ?></button>
                                        </div>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_1">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-reorder"></i>
                                    <h3>
                                        Newsletter Email
                                    </h3>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-3" style="text-align:right;"><?php echo $this->lang->line('BE_LBL_30'); ?>:</label>
                                        <div class="col-md-9">
                                            <select name="ltrId" id="ltrId" class="form-control select2me" data-placeholder="Select...">
                                                <?php
                                                    $rsNLs = fetch_newsltrs_cat_letter();
                                                   
                                                    $totalNLs = count($rsNLs);
                                                    if($totalNLs > 0)
                                                    {
                                                        $prevCatId = 0;
                                                        foreach($rsNLs as $row)
                                                        {
                                                            if($prevCatId > 0)
                                                            {
                                                                if($row->CategoryId != $prevCatId)
                                                                    echo '</optgroup><optgroup label="'.$row->Category.'"> ';
                                                            }
                                                            else
                                                                echo '<option value="0" selected>Choose a Newsletter</option><optgroup label="'.$row->Category.'">';
                                                        ?>
                                                            <option value="<?php echo $row->NewsLtrId?>"><?php echo $row->NewsLtrTitle; ?></option>
                                                        <?php
                                                            $prevCatId = $row->CategoryId;
                                                        }
                                                        echo '</optgroup>';
                                                    }
                                                    else
                                                        echo '<option class="clsOption" value="0" selected>Choose a Newsletter</option>';
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-3"></div><div class="col-lg-9">
                                        <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" id="btnSendEml">Send Newsletter</button>
                                        <img style="display:none;" id="statusLoader97" src="<?php echo base_url();?>/assets/img/loading.gif" border="0" alt="Please wait..." />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-reorder"></i>
                                    <h2>
                                        Custom Email
                                    </h2>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" style="text-align:right;">Subject:</label>
                                        <div class="col-md-6">
                                            <input type='text' placeholder="Enter Subject" name="txtCEmailSubj" id="txtCEmailSubj" required="required" maxlength="100" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" style="text-align:right;">Message:</label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" rows="3" name="txtCEmailMsg" id="txtCEmailMsg" placeholder="Enter Message" required="required"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br /><br /><br />
                                <div class="form-group row">
                                    <div class="col-md-3"></div><div class="col-lg-9">
                                        <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" id="btnSendCEml">Send Custom Email</button>
                                        <img style="display:none;" id="statusLoader99" src="<?php echo base_url();?>assets/img/loading.gif" border="0" alt="Please wait..." />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
