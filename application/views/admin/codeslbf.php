<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_532'); ?> - #<? echo $id?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<a href="<?php echo base_url('admin/services/')?>codesslbf?searchType=3&frmId=105&fTypeId=17" class="btn btn-primary btn-default"><span class="fa fa-angle-double-left"></span> Back To List</a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-lg-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<div class="form-group row">
						<div class="col-md-12">
							<? if($message != '')
								echo '<div class="alert alert-success">'.$message.'</div>'; ?>
							<div class="tabbable tabbable-custom tabbable-custom-profile">
								<ul class="nav nav-tabs nav-tabs-solid border-0">
									<li class="nav-item ">
										<a href="#tab_0" data-toggle="tab" class="nav-link active">Order</a>
									</li>
									<li class="nav-item">
										<a href="#tab_1" data-toggle="tab" class="nav-link">Send Custom Email</a>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="tab_0">
										<div class="portlet box blue">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-reorder"></i><? echo $this->lang->line('BE_LBL_532'); ?>
												</div>
											</div>
											<div class="portlet-body form">
												<!-- BEGIN FORM-->
												<?php echo form_open('',array('class'=>"form-horizontal",'name'=>"frm",'method'=>"post",'id'=>"frm",'enctype'=>"multipart/form-data")) ?>
													<input type="hidden" id="id" name="id" value="<? echo($id); ?>">
													<input type="hidden" id="existingFile" name="existingFile" value="<? echo $file; ?>">
													<input type="hidden" id="userId" name="userId" value="<? echo($userId); ?>">
													<input type="hidden" id="codeCr" name="codeCr" value="<? echo($credits); ?>">
													<input type="hidden" id="packageId" name="packageId" value="<? echo($packageId); ?>">
													<input type="hidden" id="imei" name="imei" value="<? echo($imeiNo); ?>">
													<input type="hidden" name="pckTitle" value="<? echo $pckTitle; ?>" />
													<input type="hidden" id="SMS_Pack" name="SMS_Pack" value="<? echo($SMS_Pack); ?>">
													<div class="form-body">
														<div class="form-group row">
															<label class="col-lg-4 control-label">Client Group:*</label>
															<div class="col-lg-6">
																<input type="text" class="form-control" placeholder="Enter Client Group" maxlength="100"
																	   name="txtBx" id="txtBx" value="<?php echo $plan; ?>"/>
															</div>
														</div>
														<div class="form-group row">
															<label class="col-lg-4 control-label"><? echo $this->lang->line('BE_LBL_373'); ?></label>
															<div class="col-lg-6">
																<p class="form-control-static"><? echo $pckTitle; ?></p>
															</div>
														</div>
														<div class="form-group row">
															<label class="col-md-4 control-label"><? echo $this->lang->line('BE_INDEX_USRNAME'); ?>:</label>
															<div class="col-md-6">
																<p class="form-control-static"><? echo $userName; ?></p>
															</div>
														</div>
														<div class="form-group row">
															<label class="col-md-4 control-label"><? echo $this->lang->line('BE_CODE_1'); ?>:</label>
															<div class="col-md-6">
																<input type="text" class="form-control" placeholder="Enter IMEI #" name="txtIMEI" id="txtIMEI" maxlength="15" value="<? echo($imeiNo);?>" />
															</div>
														</div>
														<div class="form-group row">
															<label class="col-md-4 control-label"><? echo $this->lang->line('BE_LBL_158'); ?>:</label>
															<div class="col-md-6">
																<textarea class="form-control" rows="3" id="txtHash" name="txtHash"><? echo $hash; ?></textarea>
															</div>
														</div>
														<? if($file != '') { ?>
															<div class="form-group row">
																<label class="col-md-4 control-label"><? echo $this->lang->line('BE_CODE_14'); ?></label>
																<div class="col-md-6">
																	<p class="form-control-static"><a href="downloadlbffiles.php?id=<? echo $id;?>" class="hyperlink"><b><? echo $this->lang->line('BE_LBL_96'); ?></b></a></p>
																</div>
															</div>
														<? } ?>
														<? if($replyFile != '') { ?>
															<div class="form-group row">
																<label class="col-md-4 control-label">Reply File</label>
																<div class="col-md-6">
																	<p class="form-control-static"><a href="downloadsl3replyfile.php?id=<? echo $id;?>" class="hyperlink"><i class="fa fa-download"></i></a></p>
																</div>
															</div>
														<? } ?>
														<div class="form-group row">
															<label class="col-md-4 control-label"><? echo $this->lang->line('BE_LBL_163'); ?>:</label>
															<div class="col-md-6">
																<p class="form-control-static"><? echo $orderDt; ?></p>
															</div>
														</div>
														<? if($replyDtTm != '') { ?>
															<div class="form-group row">
																<label class="col-md-4 control-label"><? echo $this->lang->line('BE_LBL_601'); ?>:</label>
																<div class="col-md-6">
																	<p class="form-control-static"><? echo $replyDtTm; ?></p>
																</div>
															</div>
														<? } ?>
														<div class="form-group row">
															<label class="col-md-4 control-label"><? echo $this->lang->line('BE_USR_10'); ?></label>
															<div class="col-md-6">
																<p class="form-control-static"><? echo $credits; ?></p>
															</div>
														</div>
														<div class="form-group row">
															<label class="col-md-4 control-label"><? echo $this->lang->line('BE_LS_3'); ?>:*</label>
															<div class="col-md-6">
																<select id="codeStatusId" name="codeStatusId" class="form-control select2me" data-placeholder="Choose...">
																	<? FillCombo($codeStatusId, 'SELECT CodeStatusId AS Id, CodeStatus AS Value FROM tbl_gf_code_status WHERE DisableCodeStatus = 0 ORDER BY CodeStatus', $objDBCD14); ?>
																</select>
															</div>
														</div>
														<div class="form-group row">
															<label class="col-md-4 control-label"><? echo $this->lang->line('BE_LBL_147'); ?>:</label>
															<div class="col-md-6">
																<p class="form-control-static"><? echo $sent2OtherSrvr == 1 ? 'Yes' : 'No'; ?></p>
															</div>
														</div>
														<div class="form-group row">
															<label class="col-md-4 control-label">API:</label>
															<div class="col-md-6">
																<p class="form-control-static"><? echo $apiName != '' ? $apiName : '-'; ?></p>
															</div>
														</div>
														<div class="form-group row">
															<label class="col-md-4 control-label"><? echo $this->lang->line('BE_CD_3'); ?>:</label>
															<div class="col-md-6">
																<p class="form-control-static"><? echo $orderIdFrmServer != '' ? $orderIdFrmServer : '-'; ?></p>
															</div>
														</div>
														<div class="form-group row">
															<label class="col-md-4 control-label"><? echo $this->lang->line('BE_LBL_146'); ?>:</label>
															<div class="col-md-6">
																<p class="form-control-static"><? echo $msg4mSrvr != '' ? $msg4mSrvr : '-'; ?></p>
															</div>
														</div>
														<div class="form-group row">
															<label class="col-md-4 control-label">Last Updated By:</label>
															<div class="col-md-6">
																<p class="form-control-static"><? echo $updatedBy != '' ? $updatedBy : '-'; ?></p>
															</div>
														</div>
														<div class="form-group row">
															<label for="txtImage" class="col-md-4 control-label">Answer File:</label>
															<div class="col-md-6">
																<input type="file" id="txtFile" name="txtFile">
															</div>
														</div>
														<div class="form-group row">
															<label class="col-md-4 control-label"><? echo $this->lang->line('BE_CODE_6'); ?>:</label>
															<div class="col-md-6">
																<textarea class="form-control" rows="3" id="txtCode" name="txtCode"><? echo replaceBRTag($code); ?></textarea>
															</div>
														</div>
														<div class="form-group row">
															<label class="col-md-4 control-label"><? echo $this->lang->line('BE_LBL_362'); ?>:</label>
															<div class="col-md-6">
																<textarea class="form-control" rows="3" id="txtNotes" name="txtNotes"><? echo $notes; ?></textarea>
															</div>
														</div>
														<div class="form-group row">
															<label class="col-md-4 control-label"><? echo $this->lang->line('BE_GNRL'); ?>:</label>
															<div class="col-md-6">
																<textarea class="form-control" rows="3" id="txtComments" name="txtComments"><? echo $comments; ?></textarea>
															</div>
														</div>
													</div>
													<div class="form-actions fluid">
														<div class="col-md-offset-3 col-md-9">
															<button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-success"><? echo $this->lang->line('BE_LBL_72'); ?></button>
														</div>
													</div>
												<?php echo form_close();?>
												<!-- END FORM-->
											</div>
										</div>
									</div>
									<div class="tab-pane" id="tab_1">
										<?include 'order_tab2.php'; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
    var base_url = '<?php echo base_url();?>' ;
</script>
<script language="javascript" src="<?php echo base_url();?>assets/js/overview.js?"></script>
