</div>
    <?php $this->load->view('layouts/notifications'); ?>
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/accordion.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/common-script.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.nicescroll.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/gallery/jquery.magnific-popup.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/data-tables/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/data-tables/DT_bootstrap.js');?>"></script>
    <script src="<?php echo base_url('assets/js/chosen.jquery.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-multi-select/js/jquery.multi-select.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-multi-select/js/jquery.quicksearch.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js');?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');?>" type="text/javascript"></script> 
    <script src="<?php echo base_url('assets/js/jquery.sparkline.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/sparkline-chart.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/graph.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/edit-graph.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/kalendar/kalendar.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/kalendar/edit-kalendar.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/knob/jquery.knob.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-validation/dist/jquery.validate.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-validation/dist/additional-methods.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');?>" type="text/javascript" ></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.input-ip-address-control-1.0.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js');?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/fuelux/js/spinner.min.js');?>" type="text/javascript" ></script>
    <script src="<?php echo base_url('assets/js/jquery.fancybox.js?v=2.1.5')?>"></script>
    <script src="<?php echo base_url('/assets/js/jquery.easy-pie-chart.js');?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/app.js');?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/charts.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap-toastr/toastr.min.js');?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/ui-toastr.js')?>"></script>
    <script src="<?php echo base_url('assets/js/form-components.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jscolor.js'); ?>"></script>

    <script>
        $(document).ready(function() {       
            FormComponents.init();
        });
    </script>
    <script>
        function update(jscolor) {
            // 'jscolor' instance can be used as a string
            document.getElementById('color-picker-1').style.backgroundColor = '#' + jscolor
        }
    </script>
    <script>
        $(document).ready(function() {
            if ($.fn.magnificPopup) {
                $('.image-zoom').magnificPopup({
                    type: 'image',
                    closeOnContentClick: false,
                    closeBtnInside: true,
                    fixedContentPos: true,
                    mainClass: 'mfp-no-margins mfp-with-zoom',
                    image: {
                        verticalFit: true,
                        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
                    }
                });

            }
/*
            App.init();
            if($('#datatable')) {
				$('#datatable').dataTable();
			}
            App.init();
            UIToastr.init();
*/
        });

        function showStickyErrorToast(message, title, type) {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            
            var $toast = toastr[type](message, title); // Wire up an event handler to a button in the toast, if it exists
            $toastlast = $toast;
        }

        
    </script>

    <?php if ($this->data['CURRENT_PAGE'] != 'dashboard') { ?>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.fancybox').fancybox();
            });
        </script>

        <style type="text/css">
            .fancybox-custom .fancybox-skin {
                box-shadow: 0 0 50px #222;
            }
        </style>
    <?php } ?>

</body>



</html>
