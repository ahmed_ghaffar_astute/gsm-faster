<!DOCTYPE html>
<html lang="en" class="no-js">

<head>
    <meta charset="utf-8" />
    <title><?php echo stripslashes($this->data['rsStngs']->AdminTitle); ?></title>
    <?php switchlanguage(); ?>
    <?php if ($this->session->userdata('AdminFavIcon') != '') { ?>
        <link rel="shortcut icon" href="<?php echo $this->session->userdata('AdminFavIcon'); ?>" type="image/x-icon" />
    <?php } ?>
    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/custom.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/style-responsive.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/kalendar/kalendar.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/jquerysctipttop.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/plugins/gallery/magnific-popup.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/data-tables/DT_bootstrap.css'); ?>" rel="stylesheet" >
    <link href="<?php echo base_url('assets/plugins/jquery-multi-select/css/multi-select.css');?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/jquery.fancybox.css?v=2.1.5') ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/chosen.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/jquery.easy-pie-chart.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/plugins.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('assets/css/paging.css');?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/bootstrap-toastr/toastr.min.css')?>" rel="stylesheet" type="text/css" >
    <link href="<?php echo base_url('assets/css/profile.css') ?>" rel="stylesheet" type="text/css"/>
    
    
    <script src="<?php echo base_url('assets/js/jquery-2.0.2.min.js'); ?>"></script>
    <script language="JavaScript" src="<?php echo base_url('assets/js/functions.js'); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<!-- Sweetalert popup -->
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/sweetalert/sweetalert.css')?>">
	<script type="text/javascript" src="<?=base_url('assets/sweetalert/sweetalert.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/js/vendor.js')?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.js"></script>
	<script>
		$(document).ready(function() {

			$(".select2me").select2();

			$(".js-select2-multi").select2();

			$(".large").select2({
				dropdownCssClass: "big-drop",
				scrollAfterSelect: true
			});

		});
	</script>

    <script>
        $(document).ready(function(){
            $.ajaxSetup({
                beforeSend:function(jqXHR, Obj){
                    var value = "; " + document.cookie;
                    var parts = value.split("; csrf_cookie_name=");
                    if(parts.length == 2)   
                    Obj.data += '&csrf_test_name='+parts.pop().split(";").shift();
                }
            });
        });
    </script>
    <style type="text/css">
        .fix-li li {
            float: none;
            display: block;
            position: static !important;
            border: none !important;
        }

        .fix-li-w-border li {
            float: none;
            display: block;
            position: static !important;
        }

        .caption{
            font-size : 22px;
        }

        h2 , h3{
            display : unset
        }
		.required_fields{
			color: red;
		}
		.btn_edit, .btn_delete, .btn_cust{
			padding: 5px 10px !important;
		}
		.sa-button-container .btn{
			font-size: 16px;padding: 8px 20px;
		}
		.select2-container--default .select2-selection--multiple .select2-selection__rendered{
			padding: 4px 5px !important;
		}
		.select2-container--default .select2-selection--multiple .select2-selection__choice{
			padding: 3px 3px !important;
		}
		.hint_lbl{
			color: red !important;
			margin-bottom: 2px;
		}

    </style>
</head>

<body class="light-theme">
    <div class="header navbar navbar-inverse box-shadow navbar-fixed-top">
        <div class="navbar-inner">
            <div class="header-seperation">
                <ul class="nav navbar-nav">
                    <li class="sidebar-toggle-box"> <a href="#"><i class="fa fa-bars"></i></a> </li>
                    <li>
                        <a href="<?php echo base_url(); ?>dashboard?frmId=1">
                            <img src="<?php echo base_url() . $this->session->userdata('AdminLogo'); ?>" alt="logo" class="img-responsive" width="97" height="14" />
                        </a>
                    </li>
                    <li class="hidden-xs">
                        <form method="post" action="" class="searchform" id="searchform">
                            <input type="text" placeholder="<?php echo $this->lang->line('BE_GNRL_BTN_1'); ?>..." style="width:230px;" class="form-control" id="inputString" onkeyup="lookup(this.value);">
                            <div id="suggestions"></div>
                        </form>
                    </li>
                    <?php echo $strTopMenu; ?>
                    <li class="">
                        <div class="hov">
                            <div class="btn-group">
                                <a data-toggle="dropdown" href="" class="con">
                                    <span class="fa fa-bell"></span>
                                    <span class="label label-danger">
                                        <?php echo isset($TICKETS->NEW) ? $TICKETS->NEW : 0; ?>
                                    </span>
                                </a>
                                <ul role="menu" class="dropdown-menu fix-li-w-border pull-right dropdown-alerts">
                                    <li class="title"><span class="icon icon-bell"></span>&nbsp;&nbsp;You have <?php echo $totalTickets; ?> total tickets</li>
                                    <li class="alert" onclick="window.location.href = '<?php echo base_url(); ?>admin/tickets/tickets?sId=1&frmId=52&fTypeId=12'">
                                        <div class="alert-icon alt-default"><span class="fa fa-bullhorn"></span></div>
                                        <div class="alert-content">New Tickets:</div>
                                        <div class="alert-time"><?php echo isset($TICKETS->NEW) ? $TICKETS->NEW : 0; ?></div>
                                    </li>
                                    <li class="alert" onclick="window.location.href = '<?php echo base_url(); ?>admin/tickets/tickets?sId=5&frmId=52&fTypeId=12'">
                                        <div class="alert-icon alt-primary"><span class="fa fa-plus"></span></div>
                                        <div class="alert-content">Waiting:</div>
                                        <div class="alert-time"><?php echo isset($TICKETS->WAITING) ? $TICKETS->WAITING : 0; ?></div>
                                    </li>
                                    <li class="alert" onclick="window.location.href = '<?php echo base_url(); ?>admin/tickets/tickets?sId=3&frmId=52&fTypeId=12'">
                                        <div class="alert-icon alt-warning"><span class="fa fa-plus"></span></div>
                                        <div class="alert-content">Answered By Admin:</div>
                                        <div class="alert-time"><?php echo isset($TICKETS->ANSWERED) ? $TICKETS->ANSWERED : 0; ?></div>
                                    </li>
                                    <li class="alert" onclick="window.location.href = '<?php echo base_url(); ?>admin/tickets/tickets?sId=4&frmId=52&fTypeId=12'" style="position:relative;">
                                        <div class="alert-icon alt-danger"><span class="fa fa-bolt"></span></div>
                                        <div class="alert-content">Replied By Customers:</div>
                                        <div class="alert-time"><?php echo isset($TICKETS->CUSTOMERREPLIED) ? $TICKETS->CUSTOMERREPLIED : 0; ?></div>
                                    </li>
                                    <li class="alert" onclick="window.location.href = '<?php echo base_url(); ?>admin/tickets/tickets?sId=2&frmId=52&fTypeId=12'" style="position:relative;">
                                        <div class="alert-icon alt-danger"><span class="fa fa-bell-o"></span></div>
                                        <div class="alert-content">Tickets Closed:</div>
                                        <div class="alert-time"><?php echo isset($TICKETS->CLOSED) ? $TICKETS->CLOSED : 0; ?></div>
                                    </li>
                                    <li class="alert" onclick="window.location.href = '<?php echo base_url(); ?>admin/tickets/tickets?frmId=52&fTypeId=12'" style="position:relative;">
                                        <div class="alert-content">See all tickets:</div>
                                        <div class="alert-icon"><span class="fa fa-arrow-right"></span></div>
                                    </li>

                                </ul>
                            </div>
                            <div class="btn-group">
                                <a data-toggle="dropdown" href="" class="con">
                                    <span class="fa fa-user"></span>
                                    <span class="label label-primary"><?php echo $onlineUsers; ?></span>
                                </a>
                                <ul role="menu" class="dropdown-menu fix-li-w-border pull-right dropdown-profile">
                                    <li class="title" style="height:auto"><span>You have <?php echo $onlineUsers; ?> customers online now!</span></li>
                                    <li><a href="<?php echo base_url('admin/dashboard/onlinecustomers'); ?>"><span class="fa fa-bell-o"></span> Online Customers: <?php echo $onlineUsers; ?></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li style="right: 68px; position: absolute;" class="dropdown language hidden-sm hidden-xs">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img alt="" src="<?php echo base_url('assets/images/flags/us.png'); ?>">
                            <span class="username">
                                US
                            </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu fix-li dropdown-menu-right">
                            <li>
                                <a href="<?php echo base_url('admin/dashboard?ln=en&frmId=1'); ?>">
                                <img alt="" src="<?php echo base_url('assets/images/flags/us.png'); ?>"> English</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/dashboard?ln=sp&frmId=1'); ?>"><img alt="" src="<?php echo base_url('assets/images/flags/es.png'); ?>"> Spanish</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/dashboard?ln=ru&frmId=1'); ?>"><img alt="" src="<?php echo base_url('assets/images/flags/ru.png'); ?>"> Russian</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/dashboard?ln=fr&frmId=1"'); ?>"><img alt="" src="<?php echo base_url('assets/images/flags/fr.png'); ?>"> French</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/dashboard?ln=hu&frmId=1'); ?>"><img alt="" src="<?php echo base_url('assets/images/flags/hu.png'); ?>"> Hungarian</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/dashboard?ln=de&frmId=1'); ?>"><img alt="" src="<?php echo base_url('assets/images/flags/de.png'); ?>"> German</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/dashboard?ln=pl&frmId=1'); ?>"><img alt="" src="<?php echo base_url('assets/images/flags/pl.png'); ?>"> Polish</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/dashboard?ln=pt&frmId=1'); ?>"><img alt="" src="<?php echo base_url('assets/images/flags/pt.png'); ?>"> Portuguese</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/dashboard?ln=cns&frmId=1'); ?>"><img alt="" src="<?php echo base_url('assets/images/flags/cn.png'); ?>"> Chinese (Simplified)</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/dashboard?ln=ro&frmId=1'); ?>"><img alt="" src="<?php echo base_url('assets/images/flags/ro.png'); ?>"> Romanian</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/dashboard?ln=it&frmId=1'); ?>"><img alt="" src="<?php echo base_url('assets/images/flags/it.png'); ?>"> Italian</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/dashboard?ln=vn&frmId=1'); ?>"><img alt="" src="<?php echo base_url('assets/images/flags/vn.png'); ?>" /> Vietnamese</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/dashboard?ln=ir&frmId=1'); ?>"><img alt="" src="<?php echo base_url('assets/images/flags/ir.png'); ?>" /> Farsi</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown user">
                        <a style="padding-bottom: 8px; padding-top: 8px;" href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img alt="" src="<?php echo base_url(); ?>assets/images/img/photo.jpg" />
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu fix-li dropdown-menu-right">
                            <li>
                                <a href="<?php echo base_url('admin/dashboard/changepassword'); ?>"><i class="fa fa-user"></i> <?php echo $this->lang->line('BE_PWD_HEADING'); ?></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/dashboard/refreshapiorders'); ?>"><i class="fa fa-refresh"></i> Refresh API Orders</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('dashboard'); ?>" target="_blank"><i class="fa fa-windows"></i> <?php echo $this->lang->line('BE_LBL_587'); ?></a>
                            </li>
                            <li>
                                <a href="javascript:;" id="trigger_fullscreen"><i class="fa fa-move"></i> <?php echo $this->lang->line('BE_LBL_588'); ?></a>
                            </li>
                            <?
                            if ($showSettingsInHdr == true) {
                                echo '<li><a href="' . base_url('admin/settings/settings?frmId=57&fTypeId=13') . '"><i class="fa fa-lock"></i>' . $this->lang->line('BE_MR_HD_8') . '</a></li>';
                            }
                            ?>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo base_url('admin/dashboard/signout'); ?>"><i class="fa fa-key"></i> <?php echo $this->lang->line('BE_LBL_66'); ?></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END NAVBAR -->
    <div class="page-container">
        <div class="nav-collapse top-margin fixed box-shadow2 hidden-xs" id="sidebar">
            <div class="leftside-navigation">
                <ul id="nav-accordion" class="sidebar-menu" style="border-radius:5px 0 5px 5px">
                    <li>
                        <a class="<?php echo $CURRENT_PAGE == 'dashboard' ? 'active' : ''; ?>" href="<?php echo base_url('admin/dashboard?frmId=1'); ?>">
                            <i class="fa fa-home"></i>
                            <span class="title">
                                <?php echo $this->lang->line('BE_LBL_301'); ?>
                            </span>
                            <span class="selected">
                            </span>
                        </a>
                    </li>
                    <?php echo $strLeftMenu; ?>
                </ul>
            </div>
        </div>
		<script>
			var base_url = '<?php echo base_url(); ?>';
		</script>
