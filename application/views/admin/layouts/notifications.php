<?php if($this->session->flashdata('message')) { ?>
    <div class="alert notification alert-success"><?php echo $this->session->flashdata('message'); ?></div>
<?php } ?>
<?php if($this->session->flashdata('error_message')) { ?>
    <div class="alert notification alert-danger"><?php echo $this->session->flashdata('error_message'); ?></div>
<?php } ?>