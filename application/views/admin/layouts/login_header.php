<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $adminTitle == '' ? 'GSM Tool | Backend Login' : $adminTitle; ?></title>

    <?php if($this->session->userdata('AdminFavIcon') != '') { ?>
	    <link rel="shortcut icon" href="<?php echo $this->session->userdata('AdminFavIcon'); ?>" type="image/x-icon" />
    <?php } ?>

    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/custom.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/style-responsive.css'); ?>" rel="stylesheet">
   
    <?php if($this->input->server('HTTP_HOST') != 'localhost' && $LOGIN_CAPTCHA == '1')
	        echo "<script src='https://www.google.com/recaptcha/api.js'></script>";
    ?>

</head>

<body>
<?php $this->load->view('layouts/notifications'); ?>
    <div class="login-container">
