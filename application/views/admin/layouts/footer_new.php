<!-- Footer -->
<?php $this->load->view('layouts/notifications'); ?>
<div class="navbar navbar-expand-lg navbar-light">
	<div class="text-center d-lg-none w-100">
		<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
			<i class="icon-unfold mr-2"></i>
			Footer
		</button>
	</div>

	<div class="navbar-collapse collapse" id="navbar-footer">
		<span class="navbar-text">
			&copy; 2020 - 2021. <a href="https://gsmtool.net/">GSM Tools</a>
		</span>
	</div>
</div>
<!-- /footer -->

</div>
<!-- /main content -->

</div>
<!-- /page content -->

<script src="<?php echo base_url('assets/global_assets/js/app.js') ?>"></script>

<!-- Theme JS files -->

<script src="<?php echo base_url('assets/global_assets/js/plugins/forms/styling/switchery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/global_assets/js/plugins/ui/moment/moment.min.js') ?>"></script>
<script src="<?php echo base_url('assets/global_assets/js/plugins/pickers/daterangepicker.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');?>" type="text/javascript" ></script>
<script src="<?php echo base_url('assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js');?>"></script>
<script src="<?php echo base_url('assets/global_assets/js/demo_pages/dashboard.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-multi-select/js/jquery.multi-select.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-multi-select/js/jquery.quicksearch.js')?>"></script>
<script src="<?php echo base_url('assets/global_assets/js/uniform.min.js') ?>"></script>
<script src="<?php echo base_url('assets/global_assets/js/form_layouts.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/plugins/bootstrap-toastr/toastr.min.js');?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.input-ip-address-control-1.0.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/ui-toastr.js')?>"></script>
<script src="<?php echo base_url('assets/js/form-components.js');?>"></script>
<script src="<?php echo base_url('assets/js/jscolor.js'); ?>"></script>
<script>
	$(document).ready(function() {
		FormComponents.init();
	});

	function update(jscolor) {
		// 'jscolor' instance can be used as a string
		document.getElementById('color-picker-1').style.backgroundColor = '#' + jscolor
	}

	$(document).ready(function() {
		if ($.fn.magnificPopup) {
			$('.image-zoom').magnificPopup({
				type: 'image',
				closeOnContentClick: false,
				closeBtnInside: true,
				fixedContentPos: true,
				mainClass: 'mfp-no-margins mfp-with-zoom',
				image: {
					verticalFit: true,
					tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
				}
			});

		}
	
	});

	function showStickyErrorToast(message, title, type) {
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-top-right",
			"onclick": null,
			"showDuration": "1000",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		};

		var $toast = toastr[type](message, title); // Wire up an event handler to a button in the toast, if it exists
		$toastlast = $toast;
	}


</script>

</body>
</html>
