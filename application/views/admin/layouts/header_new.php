<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?php echo stripslashes($this->data['rsStngs']->AdminTitle); ?></title>
	<?php switchlanguage(); ?>
	<?php if ($this->session->userdata('AdminFavIcon') != '') { ?>
		<link rel="shortcut icon" href="<?php echo $this->session->userdata('AdminFavIcon'); ?>" type="image/x-icon" />
	<?php } ?>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/AstuteSol.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/global_assets/css/icons/icomoon/styles.min.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/global_assets/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/global_assets/css/colors.min.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/global_assets/css/bootstrap_limitless.min.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/global_assets/css/layout.min.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/global_assets/css/components.min.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/plugins/jquery-multi-select/css/multi-select.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('assets/css/jquery.easy-pie-chart.css') ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('assets/plugins/bootstrap-toastr/toastr.min.css')?>" rel="stylesheet" type="text/css" >
	<link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'); ?>" rel="stylesheet">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="<?php echo base_url('assets/global_assets/js/main/jquery.min.js') ?>"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
	<script src="<?php echo base_url('assets/global_assets/js/main/bootstrap.bundle.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/global_assets/js/plugins/loaders/blockui.min.js') ?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.js"></script>
	<!-- /core JS files -->

	<script>
		var base_url = '<?php echo base_url(); ?>';
		var baseurl = '<?php echo base_url(); ?>';
		$(document).ready(function() {

			$(".select2me").select2();

			$(".js-select2-multi").select2();

			$(".large").select2({
				dropdownCssClass: "big-drop",
				scrollAfterSelect: true
			});
		});
	</script>
	<script>
        $(document).ready(function(){
			$.ajaxSetup({
              beforeSend:function(jqXHR, Obj){
                  var value = "; " + document.cookie;
                  var parts = value.split("; csrf_cookie_name=");
                  if(parts.length == 2)   
                  Obj.data += '&csrf_test_name='+parts.pop().split(";").shift();
              },
              complete:function(){
                var value = "; " + document.cookie;
                  var parts = value.split("; csrf_cookie_name=");
                  if(parts.length == 2){
                      $('form [name="csrf_test_name"]').val(parts.pop().split(";").shift());
                  }
              }
          	});
		});
    </script>
</head>
<body>
<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-dark bg-body navbar-static">

	<!-- Header with logos -->
	<div class="navbar-header sidebar-dark bg-indigo-400 d-none d-md-flex align-items-md-center" style="background-color: #324148;">
		<div class="navbar-brand navbar-brand-md">
			<a href="<?php echo base_url('admin/dashboard') ?>" class="d-inline-block">
				<img src="<?php echo base_url('assets/images/logos') ?>/1.png" alt="">
			</a>
		</div>

		<div class="navbar-brand navbar-brand-xs">
			<a href="#" class="d-inline-block">
				
			</a>
		</div>
	</div>
	<!-- /header with logos -->


	<!-- Mobile controls -->
	<div class="d-flex flex-1 d-md-none">
		<div class="navbar-brand mr-auto">
			<a href="index.html" class="d-inline-block">
				<img src="<?php echo base_url('assets/global_assets/images') ?>/logo_light.png" alt="">
			</a>
		</div>

		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
			<i class="icon-tree5"></i>
		</button>

		<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
			<i class="icon-paragraph-justify3"></i>
		</button>
	</div>
	<!-- /mobile controls -->


	<!-- Navbar content -->
	<div class="collapse navbar-collapse justify-content-end" id="navbar-mobile">
		<div align="right">
			<ul class="navbar-nav">
				<li class="nav-item dropdown dropdown-user">
					<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown" >
						<img alt="" src="<?php echo base_url('assets/global_assets/images') ?>/placeholders/placeholder.jpg" class="rounded-circle mr-2" height="34" />
						Welcome
					</a>

					<div class="dropdown-menu dropdown-menu-right">
						<a href="<?php echo base_url('admin/dashboard/logout'); ?>" class="dropdown-item"><i class="icon-switch2"></i> <?php echo $this->lang->line('BE_LBL_66'); ?></a>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!-- /navbar content -->

</div>
<!-- /main navbar -->
<!-- Page content -->
<div class="page-content">
	<!-- Main sidebar -->
	<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

		<!-- Sidebar mobile toggler -->
		<div class="sidebar-mobile-toggler text-center">
			<a href="#" class="sidebar-mobile-main-toggle">
				<i class="icon-arrow-left8"></i>
			</a>
			Navigation
			<a href="#" class="sidebar-mobile-expand">
				<i class="icon-screen-full"></i>
				<i class="icon-screen-normal"></i>
			</a>
		</div>
		<!-- /sidebar mobile toggler -->


		<!-- Sidebar content -->
		<div class="sidebar-content">
			<!-- Main navigation -->
			<div class="card card-sidebar-mobile">
				<ul class="nav nav-sidebar" data-nav-type="accordion">
					<?php echo $strLeftMenu; ?>
				</ul>
			</div>
			<!-- /main navigation -->
		</div>
		<!-- /sidebar content -->
	</div>
	<!-- /main sidebar -->
	<!-- Main content -->
	<div class="content-wrapper">
