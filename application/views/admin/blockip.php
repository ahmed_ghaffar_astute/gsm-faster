<div class="page-content">

    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title"><? echo $this->lang->line('BE_LBL_391'); ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
           <div class="block-web">
               <? if($message != '')
                   echo '<div class="alert alert-success">'.$message.'</div>'; ?>
               <div class="portlet box blue">
                   <div class="portlet-title">
                       <div class="caption">
                           <i class="fa fa-reorder"></i><? echo $this->lang->line('BE_LBL_391'); ?>
                       </div>
                   </div>
                   <div class="portlet-body form">
                       <?php echo form_open('admin/reports/blockip',array('class'=>"form-horizontal",'name'=>"frm",'method'=>"post",'id'=>"frm"))?>
                           <input type="hidden" id="id" name="id" value="<? echo($id); ?>">
                           <div class="form-body">
                               <div class="form-group">
                                   <label class="control-label col-md-3">IP(s):*</label>
                                   <div class="col-md-4">
                                       <textarea class="form-control" name="txtIPs" rows="6"></textarea>
                                       <span class="form-text text-muted">Seperated By Commas</span>
                                   </div>
                               </div>
                               <div class="form-group">
                                   <label class="control-label col-md-3"><? echo $this->lang->line('BE_GNRL'); ?>:</label>
                                   <div class="col-md-4">
                                       <textarea class="form-control" name="txtComments" rows="3"></textarea>
                                   </div>
                               </div>
                           </div>
                           <div class="form-group row">
                               <div class="col-md-offset-3 col-md-4">
                                   <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn bnt-success"><? echo $this->lang->line('BE_LBL_72'); ?></button>
                               </div>
                           </div>
                      <?php echo form_close();?>
                   </div>
               </div>
           </div>
        </div>
    </div>

</div>
