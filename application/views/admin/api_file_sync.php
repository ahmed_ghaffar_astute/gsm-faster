<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_512'); ?></h5>
			</div>
			<div class="card-body">
				<form action="#" class="form-horizontal" id="frmSync" name="frmSync" method="post">
					<div class="form-group row">
						<div class="col-lg-12" align="center">
							<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary" id="btnFileSync" name="btnFileSync">Synchronize File
								Services
							</button>
							<img style="display:none;" id="syncFlLdr"
								 src="<?php echo base_url('assets/images/loading.gif'); ?>" border="0"
								 alt="Please wait..."/>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>`
