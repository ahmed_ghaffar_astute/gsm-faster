<?php echo form_open(base_url("admin/services/serverservice?tab=3&id=") . $id, 'class="form-horizontal well" id="frm2" name="frm2"'); ?>
<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
<input type="hidden" name="exAPIId" id="exAPIId" value="<?php echo $onlyAPIId; ?>">
<input type="hidden" id="exAPIServiceId" name="exAPIServiceId" value="<?php echo $extNetworkId; ?>">

<div class="form-group row">
	<div class="col-lg-12">
		<h4><i class="mr-2"></i>
			<?php echo $this->lang->line('BE_PCK_15'); ?>
		</h4>
	</div>
</div>
<?php $api_data = fetch_api_data_by_concat() ?>
<div class="form-group row">
	<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PCK_6'); ?>:</label>
	<div class="col-lg-8">
		<select name="apiId" id="apiId" class="form-control select2me"
				data-placeholder="<?php echo $this->lang->line('BE_LBL_278'); ?>">
			<option value="-1~0~0"><?php echo $this->lang->line('BE_LBL_278'); ?></option>
			<?php FillCombo($apiId, $api_data); ?>
		</select>
		<img style="display:none;" id="imgSrvcLoader" src="<?php echo base_url('assets/images/loading.gif'); ?>"
			 border="0"
			 alt="Please wait..."/>
	</div>
</div>
<div class="form-group row" id="dvExtNetworkId">
	<label class="col-lg-3 control-label">
		<?php echo $this->lang->line('BE_LBL_293'); ?>:
	</label>
	<div class="col-lg-8">
		<select name="supplierPackId" id="supplierPackId" class="form-control select2me"
				data-placeholder="<?php echo $this->lang->line('BE_LBL_274'); ?>">
			<option value="0"><?php echo $this->lang->line('BE_LBL_274'); ?></option>
			<?php
			$rsAPISrvcs = fetch_supplier_services_by_api_id($onlyAPIId);
			foreach ($rsAPISrvcs as $rwAPIS) {
				$selected = '';
				if ($rwAPIS->ServiceId == $extNetworkId)
					$selected = 'selected';
				$apiSrvc = '';
				$apiSrvc = stripslashes($rwAPIS->ServiceName);
				if ($rwAPIS->ServicePrice != '')
					$apiSrvc .= ' - ' . stripslashes($rwAPIS->ServicePrice) . ' Credits';

				if ($rwAPIS->RequiredParams != '')
					$apiSrvc .= ' - Required Paramters: ' . stripslashes($rwAPIS->RequiredParams);
				?>
				<option <?php echo $selected; ?>
					value="<?php echo $rwAPIS->ServiceId; ?>"><?php echo $apiSrvc; ?></option>
			<?php } ?>

		</select>
		<img style="display:none;" id="imgSrvcTypeLoader" src="<?php echo base_url('assets/images/loading.gif'); ?>"
			 border="0" alt="Please wait..."/>
	</div>
</div>
<div class="form-group row" id="dvAPIServiceType" <?php if ($apiSTypeId == 0) echo 'style="display:none;"'; ?>>
	<?php $services_types = fetch_service_service_types_by_apiid($onlyAPIId); ?>
	<label class="col-lg-3 control-label">Service Type:</label>
	<div class="col-lg-9">
		<select name="apiServiceTypeId" id="apiServiceTypeId" class="form-control select2me">
			<option value="0">Choose Service Type</option>
			<?php FillCombo($apiSTypeId, $services_types); ?>
		</select>
	</div>
</div>
<div class="form-group row">
	<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_836'); ?>:</label>
	<div class="col-lg-3">
		<input type="text" class="form-control" placeholder="Enter <?php echo $this->lang->line('BE_LBL_836'); ?>"
			   name="txtCronTiming" id="txtCronTiming" maxlength="4" value="<?php echo $cronDelayTm; ?>"/>
	</div>
	<span class="form-text text-muted">
                            In minutes
                        </span>
</div>
<div class="form-group row">
	<div class="col-lg-3"></div>
	<div class="col-lg-9">
	<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
			class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
	</div>
</div>
<?php if ($onlyAPIId) { ?>
	<div class="form-group row">
		<div class="col-lg-12">
			<h4><i class="mr-2"></i>
				Alternate API
		</div>
	</div>
	<div class="form-group row">
		<div class="col-lg-12">
			<span class="form-text text-muted">
				If main API does not return result, provide here the list of APIs to be connected to in the order. Drag and drop to change the order of connections.
			</span>
		</div>
	</div>
	<div class="form-group row">
		<div class="col-lg-8">
			<a href="<?php echo base_url('admin/services/alternateapi?id=' . $id . '&sc=2&pn=' . str_replace(' ', 'S_P_', $packageTitle)); ?>"
			   class="fancybox fancybox.ajax btn btn-primary"
			   title="Apply an alternate API, if main API rejects an order then system will automatically apply alternate API with an order."><i
					class="fa fa-plus"></i> Add</a>
			<a <?php if ($altAPIId == 0 || $altAPIId == '') echo 'style="display:none;"'; ?>
				class="btn btn-warning" title="Remove Alternate API" id="lnkRmvAltAPI"><i
					class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i> Remove Alternative API</a>
			<img style="display:none;" id="rmvAltAPILdr"
				 src="<?php echo base_url('assets/images/loading.gif'); ?>" border="0"
				 alt="Please wait..."/>
		</div>
	</div>
	<?php
}
if ($id > 0) {
	$rsAPIHistory = fetch_api_srv_history_supplier_services($id);
	?>
	<div class="form-group row">
		<div class="col-lg-12">
			<h4><i class="mr-2"></i>API History
		</div>
	</div>

	<div class="table-responsive">
		<table class="table table-striped table-bordered table-advance table-hover">
			<thead>
			<tr class="bg-primary">
				<th><?php echo $this->lang->line('BE_MR_19'); ?></th>
				<th nowrap="nowrap"><?php echo $this->lang->line('BE_PCK_HD_4'); ?></th>
				<th><?php echo $this->lang->line('BE_PCK_14'); ?></th>
				<th></th>
			</tr>
			</thead>
			<tbody>
			<?php
			if ($rsAPIHistory) {
				foreach ($rsAPIHistory as $row) {
					?>
					<tr>
						<td><?php echo stripslashes($row->APITitle); ?></td>
						<td><?php echo stripslashes($row->ServiceName); ?></td>
						<td><?php echo roundMe($row->ServicePrice); ?></td>
						<td>
							<a href="<?php echo base_url('admin/services/serverservice?id=' . $id . '&crAPIId=' . $row->APIId . '&crAPISrvId=' . $row->APIServiceId . '&applyAPI=1#tab_2'); ?>"
							   onclick="return confirm('Are you sure to apply this API?')"
							   class="btn default btn-xs purple"><i class="fa icon-cog"></i> Apply API</a></td>
					</tr>
				<?php } ?>
			<?php } else { ?>
				<tr>
					<td colspan="5"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
<?php } ?>
<?php echo form_close(); ?>

