<?php
if ($totalRecords != 0) {
	$row = fetch_precode_count($id, $strPrCdWhr);

	$totalRows = $row->TotalRecs;
	if ($totalRows > $limit) {
		$txtlqry .= '#tab_8';
		doPages($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next);
	}
}
?>
<?php echo form_open(base_url("admin/services/serverservice?tab=8&id=") . $id, 'class="form-horizontal well" id="frm8" name="frm8"'); ?>
<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
<div class="form-group row">
	<div class="col-lg-5">
		<h4><i class="mr-2"></i><?php echo $this->lang->line('BE_LBL_653'); ?>
		</h4>
	</div>
	<div class="col-lg-3">
		<div class="form-group">
			<input class="form-control" placeholder="Pre Code" type="text" name="txtPrCde"
				   value="<?php echo($prdCde); ?>"/>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="form-group">
			<select name="prCdSt" class="form-control select2me" data-placeholder="Select...">
				<option value="-1" selected>Select Status</option>
				<option value="1" <?php if ($prCdSt == '1') echo 'selected'; ?>>Assigned</option>
				<option value="0" <?php if ($prCdSt == '0') echo 'selected'; ?>>Un Assigned</option>
			</select>
		</div>
	</div>
	<div class="col-lg-1">
		<div class="form-group">
			<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> class="btn btn-primary"
					onclick="document.getElementById('cldFrm').value='3'"><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
		</div>
	</div>
</div>
<div class="form-group row">
	<div class="col-lg-9"></div>
	<div class="col-lg-3">
		<div class="form-group" align="right" style="padding-right:16px;">
			<input type="hidden" name="cldFrm" id="cldFrm" value="0"/>
			<input type="hidden" name="hdStock" value="0"/>			<input type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> value="Assign"
				   class="btn btn-primary" onclick="document.getElementById('cldFrm').value='2'"/>
			<input type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> value="Delete"
				   class="btn btn-danger" onclick="document.getElementById('cldFrm').value='1'"/>
		</div>
	</div>
</div>
<div class="form-group row">
	<div class="col-lg-12">

		<table class="table table-striped table-bordered table-advance table-hover">
			<thead>
			<tr class="bg-primary">
				<th nowrap="nowrap" width="3%">Sr. #</th>
				<th>ID</th>
				<th>Pre Code</th>
				<th>Order #</th>
				<th>User Name</th>
				<th>Date Assigned</th>
				<th style="text-align:center;">Status</th>
				<th style="text-align:center;">Action</th>
				<th>
					<?php if ($totalStock > 0) { ?>
						<input type="checkbox" class="chkSelect" id="chkSelect" name="chkSelect"
							   onClick="selectAllChxBxs('chkSelect', 'chkPreCodes', <?php echo $totalStock; ?>);"
							   value="true">
					<?php } ?>
				</th>
			</tr>
			</thead>
			<tbody>
			<?php
			if ($totalRecords != 0) {
				$iCnt = 0;
				$iStock = 0;
				foreach ($rsRpt as $row) {
					?>
					<tr>
						<td><?php echo $iCnt + 1; ?>.</td>
						<td><?php echo stripslashes($row->PreCodeId); ?></td>
						<td><?php echo stripslashes($row->PreCode); ?></td>
						<td>
							<?php
							if ($row->OrderId != '0')
								echo '<a href="' . base_url('admin/services/logrequest?id=' . $row->OrderId) . '">#' . $row->OrderId . '</a>';
							else
								echo 'N/A';
							?>
						</td>
						<td>
							<?php
							if ($row->UserId != '0')
								echo '<a href="' . base_url('admin/services/overview?id=' . $row->UserId) . '">' . $row->UserName . '</a>';
							else
								echo 'N/A';
							?>
						</td>
						<td><?php echo $row->Assigned == '1' ? $row->AssignedDtTm : '-'; ?></td>
						<td style="text-align:center;"><span
								class="badge badge-<?php echo $row->Assigned == '1' ? 'default' : 'success'; ?>"><?php echo $row->Assigned == '1' ? 'Assigned' : 'Available'; ?></span>
						</td>
						<td style="text-align:center;">
							<?php
							if ($row->Assigned == '0') {
								?>
								<a href="<?php echo base_url('admin/services/serverservice?id=' . $id . '&asgn=1&rId=' . $row->PreCodeId); ?>"
								   class="btn default btn-xs btn-primary"><i class="fa fa-refresh"></i>
									Assign</a>&nbsp;&nbsp;&nbsp;

								<a href="<?php echo base_url('admin/services/serverservice?id=' . $id . '&asgn=1&rId=' . $row->PreCodeId); ?>"
								   onclick="return confirm('<?php echo $this->lang->line('BE_LBL_32'); ?>')"><i
										class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>
								<?php
							} else
								echo '-';
							?>
						</td>
						<td align="center">
							<?php if ($row->Assigned == '0') { ?>
								<input type="checkbox" id="chkPreCodes<?php echo $iStock; ?>" name="chkPreCodes[]"
									   value="<?php echo $row->PreCodeId; ?>"/>
								<?php
								$iStock++;
							} else echo '-'; ?>
						</td>
					</tr>
					<?php
					$iCnt++;
				}
			} else
				echo "<tr><td colspan='9'>" . $this->lang->line('BE_GNRL_9') . "</td></tr>";
			?>
			</tbody>
		</table>
	</div>
</div>
<?php echo form_close(); ?>
