<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_516'); ?></h4>
		</div>
		<div align="right">
			<a href="<?php echo base_url('admin/cms/') ?>reviews?frmId=<? echo $_REQUEST['frmId']; ?>&fTypeId=<? echo $_REQUEST['fTypeId'] ?>&type=<? echo $_REQUEST['type'] ?>"
			   class="btn btn-sm btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
			<div class="card-body">
				<?php if (isset($message) && $message != '') { ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="alert alert-success"><?php echo $message; ?></div>

						</div>
					</div>
				<?php } ?>
				<?php echo form_open(base_url('admin/cms/') . 'review?frmId=40&fTypeId=8', array('class' => "form-horizontal", 'name' => "frm", 'method' => "post", 'id' => "frm")) ?>
				<!--                        <form action="#" >-->
				<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
				<input type="hidden" name="type" value="<? echo $type; ?>"/>
				<div class="form-group row">
					<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_USR_9'); ?>:<span class="required_field">*</span> </label>
					<div class="col-lg-4">
						<input type="text" class="form-control" placeholder="Enter Name" maxlength="100" name="txtName"
							   id="txtName" value="<?php echo($name); ?>"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_516'); ?>:</label>
					<div class="col-lg-4">
						<textarea class="form-control" rows="3" id="txtReview"
								  name="txtReview"><?php echo $review; ?></textarea>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 control-label"><? echo $this->lang->line('BE_LBL_518'); ?></label>
					<div class="col-lg-4">
						<div class="rating">
							<input type="radio" id="star1" name="rating" value="1"  <?php if ($rating == '1') echo 'checked'; ?>/>
							<label class="star" for="star1" title="Bad" aria-hidden="true"></label>
							<input type="radio" id="star2" name="rating" value="2" <?php if ($rating == '2') echo 'checked'; ?>/>
							<label class="star" for="star2" title="Good" aria-hidden="true"></label>
							<input type="radio" id="star3" name="rating" value="3" <?php if ($rating == '3') echo 'checked'; ?>/>
							<label class="star" for="star3" title="Very good" aria-hidden="true"></label>
							<input type="radio" id="star4" name="rating" value="4" <?php if ($rating == '4') echo 'checked'; ?>/>
							<label class="star" for="star4" title="Great" aria-hidden="true"></label>
							<input type="radio" id="star5" name="rating" value="5" <?php if ($rating == '5') echo 'checked'; ?>/>
							<label class="star" for="star5" title="Awesome" aria-hidden="true"></label>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_517'); ?>:</label>
					<div class="col-lg-4">
						<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
							<input type="checkbox" name="chkEnable" id="chkEnable" <?php echo $enabled == 1 ? 'checked' : ''; ?> class="toggle"/><span><label for="chkEnable"></label></span>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-lg-3"></div>
					<div class="col-lg-9">
						<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
								class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
					</div>

				</div>
				<?php echo form_close(); ?>
				<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
</div>


<script language="javascript" src="<?php echo base_url('assets/') ?>js/review.js"></script>
