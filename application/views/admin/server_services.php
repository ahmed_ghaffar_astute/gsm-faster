<!-- Page header -->
<div class="page-header border-bottom-0 pt-4 pl-3">
	<div class="form-group row">
		<div class="col-lg-12">
			<?php include APPPATH . 'scripts/serverservices_header.php'; ?>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Page header -->
<div class="page-header border-bottom-0">
    <div class="page-header-content header-elements-md-inline">
        <div style="padding-top: 10px !important;" class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i>
                <?php echo $this->lang->line('BE_PCK_HD_7'); ?>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

    </div>
</div>
<!-- /page header -->
<div class="content pt-0">
    <div class="card">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card-body">
                    <?php if (isset($message) && $message != '') { ?>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <div class="alert alert-success"><?php echo $message; ?></div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php echo form_open('' , 'class="horizontal-form" id="frm" name="frm"');?>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <?php $cat_data = fetch_log_package_category_by_arc_cat(); ?>
                                <label class="control-label"><?php echo $this->lang->line('BE_PCK_HD_7'); ?></label>
                                <select name="categoryId" id="categoryId" class="form-control select2me" data-placeholder="<?php echo $this->lang->line('BE_LBL_256'); ?>">
                                    <option value="0"><?php echo $this->lang->line('BE_LBL_255'); ?></option>
                                    <?php FillCombo($categoryId, $cat_data); ?>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <?php $api_data=fetch_api_data_by_concat(); ?>
                                <label class="control-label"><?php echo $this->lang->line('BE_PCK_15'); ?></label>
                                <select name="apiId" id="apiId" class="form-control select2me">
                                    <option value="0"><?php echo $this->lang->line('BE_LBL_278'); ?></option>
                                    <?php FillCombo($apiId, $api_data); ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label class="control-label"><?php echo $this->lang->line('BE_CODE_12'); ?></label>
                                <select name="packageId" id="packageId" class="form-control select2me" data-placeholder="Select...">
                                    <?php
                                    
                                    
                                    $rsPackages = fetch_log_package_category_packages();
                                    $prevCatId = 0;
                                    foreach($rsPackages as $row )
                                    {
                                        if($prevCatId > 0)
                                        {
                                        if($row->CategoryId != $prevCatId)
                                        echo '</optgroup><optgroup label="'.$row->Category.'"> ';
                                        }
                                        else
                                        echo '<option value="0" selected>'. $this->lang->line('BE_LBL_276') .'</option><optgroup label="'.$row->Category.'">';
                                    ?>
                                        <option value="<?php echo $row->PackageId?>" <?php if($myNetworkId == $row->PackageId) echo 'selected';?> >
                                            <?php echo $row->LogPackageTitle?>
                                        </option>
                                    <?php
                                        $prevCatId = $row->CategoryId;
                                    }
                                    echo '</optgroup>';
                                    ?>
                                </select>

                            </div>
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label class="control-label"><?php echo $this->lang->line('BE_CODE_5'); ?></label>
                                    <select name="srvStatus" class="form-control select2me">
                                        <option value="0" <?php if($srvStatus == '0') echo 'selected';?>>Active</option>
                                        <option value="1" <?php if($srvStatus == '1') echo 'selected';?>>In Active</option>
                                        <option value="2" <?php if($srvStatus == '2') echo 'selected';?>>Both</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-8">
                                <label class="control-label"></label>
                                <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" onclick="setValue('0');document.getElementById('frm').submit();"><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
                            </div>
                            <div class="col-lg-4" align="right">
                                    <input type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="<?php echo $this->lang->line('BE_LBL_102');?>" onClick="exportServices();" class="btn btn-warning Margin-Left5" />
                                    <input type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="<?php echo $this->lang->line('BE_LBL_72'); ?>" onclick="setValue('1');" class="btn btn-primary Margin-Left5" name="btnSubmit" />
                            </div>
                        </div>
                        
                        <div class="form-group row">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                    <tr class="bg-primary">
                                        <th><?php echo $this->lang->line('BE_PCK_HD_4'); ?></th>
                                        <th><?php echo $this->lang->line('BE_PCK_14'); ?></th>
                                        <th><?php echo $this->lang->line('BE_PCK_15'); ?></th>
                                        <th style="text-align:center;">Status</th>
                                        <th width="10%" style="text-align:center;" nowrap="nowrap">Pre Codes</th>
                                        <th width="5%"></th>
                                        <?php if (array_key_exists('324', $ARR_ADMIN_FORMS)) { ?>
                                            <th width="5%"></th>
                                        <?php } ?>
                                        <th style="text-align:center;" width="15%">Enable/Disable
                                            <input type="checkbox" id="chkSelect" class="chkSelect" name="chkSelect" onClick="selectAllChxBxs('chkSelect', 'chkPacks', <?php echo $count; ?>);" value="true">
                                        </th>
                                    </tr>
                                    </thead>
                                <tbody>
                                <?
                                $strCurrPacks = 0;
                                if($count != 0)
                                {
                                    $i = 0;
                                    foreach($rsPacks as $row)
                                    {
                                        $strCurrPacks .= ', '.$row->PackageId;
                                        $strPCs = '-';
                                        $packStatus = $row->DisablePackage == '0' ? 'Active' : 'In Active';
                                        if(isset($arrPCs[$row->PackageId]) && $arrPCs[$row->PackageId] > 0)
                                        {
                                            $strPCs = $arrPCs[$row->PackageId];
                                        }
                                    ?>
                                        <tr>
                                            <!--<td><?php echo stripslashes($row->Category);?></td>-->
                                            <td><?php echo stripslashes($row->PackageTitle);?></td>
                                            <td><input type="text" name="packagePrice[<?php echo $row->PackageId; ?>][]" id="<?php echo $row->PackageId; ?>" value="<?php echo roundMe($row->PackagePrice);?>" style="width:75px;" class="form-control" /></td>
                                            <td><?php echo $row->API == '' ? '-' : stripslashes($row->API);?></td>
                                            <td style="text-align:center" valign="middle">
                                                <a href="JavaScript:void(0);" class="btn default btn-xs <?php echo $row->DisablePackage == '0' ? 'green' : 'red' ?>"> <?php echo $packStatus; ?>
                                            </td>
                                            <td style="text-align:center" valign="middle">
                                                <?
                                                if($strPCs == '-')
                                                    echo $strPCs;
                                                else
                                                { ?>
                                                    <a href="<?php echo base_url('admin/services/serverservice?id='.$row->PackageId);?>#tab_8"><?php echo $strPCs; ?></a>
                                                <?
                                                }
                                                ?>
                                            </td>
                                            <td style="text-align:center" valign="middle">
                                                <a href="<?php echo base_url('admin/services/serverservice?id='.$row->PackageId);?>" ><i class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
                                            </td>
                                            <?php if (array_key_exists('324', $ARR_ADMIN_FORMS)) { ?>
                                                <td style="text-align:center" valign="middle">
                                                    <a href="<?php echo base_url('admin/services/serverservices?del=1&packId='.$row->PackageId.'&categoryId='.$categoryId.'&packageId='.$packageId);?>" onclick="return confirm('Are you sure you want to archive this service?')"><i class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>
                                                </td>
                                            <?php } ?>
                                            <td align="center">
                                                <input type="checkbox" class="chkSelect" id="chkPacks<?php echo $i; ?>" name="chkPacks[]" value="<?php print $row->PackageId; ?>" <?php if($row->DisablePackage == '1') echo 'checked'; ?> />
                                            </td>
                                        </tr>
                                <?
                                        $i++;
                                    }
                                }
                                else
                                    echo "<tr><td colspan='8'>".$this->lang->line('BE_GNRL_9')."</td></tr>";
                                ?>
                                    <input type="hidden" value="0" name="cldFrm" id="cldFrm" />
                                    <input type="hidden" value="<?php echo $dis?>" name="dis" id="dis" />
                                    <input type="hidden" value="<?php echo $strCurrPacks; ?>" name="currPcks" />
                                </tbody>
                                </table>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-2">
                                <div class="form-group row">
                                    <?php $NO_OF_RECORDS = array("10"=>10, "50"=>50, "100"=>100, "200"=>200, "300"=>300, "400"=>400, "500"=>500, "1000"=>1000) ;?>
                                    <label class="control-label"><strong>Records Per Page</strong></label>
                                    <select name="records" class="form-control select2me" data-placeholder="Select..." onchange="setValue('0');document.getElementById('frm').submit();">
                                        <?php
                                            foreach ($NO_OF_RECORDS as $key => $value)
                                            {
                                                $selected = '';
                                                if($limit == $key)
                                                    $selected = 'selected';
                                                echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-10">
                                    <div class="form-group row">
                                        <?php
                                        if($count != 0)
                                        {
                                            
                                            $totalRows = fetch_package_category_count($strWhere);
                                            if($totalRows > $limit)
                                                doPages($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next);
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<script>
function setValue(i)
{
	document.getElementById('cldFrm').value = i;
}
function exportServices()
{
	var strIds = getServiceIds();
	window.location.href = base_url+'admin/services/exportservices?type=1&ids='+strIds;
}
function getServiceIds()
{
	var totalSrvcs = <? echo $count; ?>;
	var strIds = '0';
	for(var i=0; i<totalSrvcs; i++)
	{
		if(document.getElementById('chkPacks'+i).checked)
		{
			strIds += ',' + document.getElementById('chkPacks'+i).value;
		}
	}
	return strIds;
}
</script>
