<div class="page-content">
    <div class="row">
        <div class="col-md-10">
            <h3 class="page-title"><?php echo $this->lang->line('BE_LBL_533'); ?> - #<?php echo $id?></h3>
        </div>
        <div class="col-md-2" style="text-align:right;">
            <a href="<?php echo base_url('admin/services/logrequests?searchType=3&frmId=110&fTypeId=18');?>" class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php if($message != '')
                    echo '<div class="alert alert-success">'.$message.'</div>'; ?>
            <div class="tabbable tabbable-custom boxless">
                <ul class="nav nav-tabs nav-tabs-solid border-0">
                    <li class="active">
                        <a href="#tab_0" data-toggle="tab">Order</a>
                    </li>
                    <li>
                        <a href="#tab_1" data-toggle="tab">Send Custom Email</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_0">
                        <?php ?>
                    </div>
                    <div class="tab-pane" id="tab_1">
                        <?php  ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
