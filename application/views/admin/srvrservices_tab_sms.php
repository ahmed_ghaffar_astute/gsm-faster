<?php echo form_open(base_url("admin/services/serverservice?tab=7&id=") . $id, 'class="form-horizontal well"  id="frm7" name="frm7"'); ?>
<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
<div class="form-group row">
	<div class="col-lg-12">
		<h4><i class="mr-2"></i>
			<?php echo $this->lang->line('BE_LBL_694'); ?>
		</h4>
	</div>
</div>
<div class="form-group row">
	<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_695'); ?>:</label>
	<div class="col-lg-4">
		<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
			<input type="checkbox" name="chkSMS" id="chkSMS" <?php echo $sendSMS == 1 ? 'checked' : ''; ?>
				   class="toggle"/><span><label for="chkSMS"></label></span>
		</div>
	</div>
</div>
<div class="form-group row">
	<div class="col-lg-3"></div>
	<div class="col-lg-4">
		<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> class="btn btn-primary"
				name="btnSMS"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
	</div>
</div>
<?php echo form_close(); ?>
