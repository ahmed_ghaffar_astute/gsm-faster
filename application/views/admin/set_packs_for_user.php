<!-- BEGIN PAGE HEADER-->
<div class="form-group row">
	<div class="col-lg-12">
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">
			<? echo $this->lang->line('BE_LBL_539'); ?><br/>
		</h3>
		<!-- END PAGE TITLE & BREADCRUMB-->
	</div>
</div>
<!-- END PAGE HEADER-->
<? if ($message != '')
	echo '<div class="alert alert-success">' . $message . '</div>'; ?>
<?
$USER_ID_BTNS = $userId;
if ($USER_ID_BTNS > 0)
	include APPPATH . 'scripts/userbtns.php';

?>
<!-- BEGIN PAGE CONTENT-->
<?php echo form_open('admin/services/set_packs_for_user?userId=' . $userId . "&sc=" . $sc, array('name' => 'frm', 'id' => 'frm', 'class' => 'form-horizontal')); ?>
<input type="hidden" name="sc" value="<? echo $sc; ?>"/>
<div class="form-group row">
	<div class="col-lg-6">
		<div class="form-group">
			<label class="control-label"><? echo $this->lang->line('BE_LBL_219'); ?></label>
			<select name="categoryId" id="categoryId" class="form-control select2me"
					data-placeholder="<? echo $this->lang->line('BE_LBL_256'); ?>"
					onChange="document.getElementById('frm').submit();">
				<option value="0"><? echo $this->lang->line('BE_LBL_255'); ?></option>
				<? FillCombo($categoryId, $rsCategories); ?>
			</select>
		</div>
	</div>
	<!--/span-->
	<div class="col-lg-6">&nbsp;</div>
	<!--/span-->
</div>
<!--/row-->
<?php echo form_close(); ?>
<div class="form-group row">
	<div class="col-lg-12">
		<?php echo form_open('admin/services/set_packs_for_user?userId=' . $userId . "&sc=" . $sc, array('name' => 'frm1', 'id' => 'frm1', 'class' => 'form-horizontal')); ?>
		<div class="page-title d-flex">
			<h3><i class="mr-2"></i> <?php echo $this->lang->line('BE_LBL_539'); ?></h3>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<div class="form-group row">
	<div class="col-lg-12">
		<div class="table-responsive">
			<? if ($count > 0) { ?>
				<div class="form-group row">
					<div class="col-lg-6">&nbsp;</div>
					<div class="col-lg-6">
						<div class="form-group" align="right">
							<input type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
								   value="<? echo $this->lang->line('BE_LBL_539'); ?>" class="btn btn-primary"
								   name="btnSave"/>
						</div>
					</div>
					<!--/span-->
				</div>
				<!--/row-->
			<? } ?>
			<table class="table table-striped table-bordered table-advance table-hover">
				<thead>
				<tr class="bg-primary">
					<th><? echo $this->lang->line('BE_PCK_HD_4'); ?></th>
					<th style="text-align:center" nowrap="nowrap"><input type="checkbox" id="chkSelect" name="chkSelect"
																		 onClick="selectAllChxBxs('chkSelect', 'chkPacks', <? echo $count; ?>);"
																		 value="true"></th>
				</tr>
				</thead>
				<tbody>
				<?
				if ($count != 0) {
					$i = 0;
					$strCatBasedSrvcs = '0';
					foreach ($rsPacks as $row) {
						$checked = '';
						if (isset($REMOVED_PACKS[$row->PackageId]))
							$checked = "checked";
						$strCatBasedSrvcs .= ', ' . $row->PackageId;
						?>
						<tr>
							<td class="highlight">
								<div class="success"></div>
								<a href="javascript:void(0);"><? echo stripslashes($row->PackageTitle); ?></a></td>
							<td style="text-align:center">
								<input type="checkbox" id="chkPacks<? echo $i; ?>" name="chkPacks[]"
									   value="<? echo $row->PackageId; ?>" <? echo $checked; ?> />
							</td>
						</tr>
						<?
						$i++;
					}
				} else
					echo "<tr><td colspan='7'>" . $this->lang->line('BE_GNRL_9') . "</td></tr>";
				?>
				<input type="hidden" value="<? echo $userId; ?>" id="userId" name="userId"/>
				<input type="hidden" name="categoryId" value="<? echo $categoryId; ?>"/>
				<input type="hidden" value="<? echo $strCatBasedSrvcs; ?>" id="catBasedSrvcs" name="catBasedSrvcs"/>
				<input type="hidden" name="sc" value="<? echo $sc; ?>"/>
				</tbody>
			</table>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>

<!-- END PAGE CONTENT-->
