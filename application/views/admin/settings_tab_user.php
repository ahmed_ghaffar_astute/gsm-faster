<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_502'); ?></h5>
			</div>

			<div class="card-body">
				
            <div class="form-group row">
                <label class="col-md-4 control-label">Admin Login Captcha:</label>
                <div class="col-md-6">
					<div class="form-check form-check-inline mt-0">
                    <label class="form-check-label mr-3"><input type="radio" name="rdAdminCaptcha" id="rdAdminCaptcha1" value="1" checked  class="form-check-input" />ON </label>
					<label class="form-check-label"><input type="radio" name="rdAdminCaptcha" id="rdAdminCaptcha0" value="0" <? if($adminCaptcha == '0') echo 'checked'; ?>  class="form-check-input" />OFF </label>
					</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 control-label">Client Login Captcha:</label>
                <div class="col-md-6">
					<div class="form-check form-check-inline mt-0">
                    <label class="form-check-label mr-3">
                    <input type="radio" name="rdUserCaptcha" id="rdUserCaptcha1" value="1" checked  class="form-check-input" />ON </label>
                    <label class="form-check-label">
                    <input type="radio" name="rdUserCaptcha" id="rdUserCaptcha0" value="0" <? if($userCaptcha == '0') echo 'checked'; ?>  class="form-check-input" />OFF </label>
					</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 control-label">Captcha At Registration Page:</label>
                <div class="col-md-6">
					<div class="form-check form-check-inline mt-0">
					<label class="form-check-label mr-3">
                    <input type="radio" name="rdRgstrCaptcha" id="rdRgstrCaptcha1" value="1" checked  class="form-check-input" />ON </label>
                    <label class="form-check-label">
                    <input type="radio" name="rdRgstrCaptcha" id="rdRgstrCaptcha0" value="0" <? if($rgstrCaptcha == '0') echo 'checked'; ?>  class="form-check-input" />OFF </label>
					</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 control-label">Captcha At Contact Page:</label>
                <div class="col-md-8">
					<div class="form-check form-check-inline mt-0">
					<label class="form-check-label mr-3">
                    <input type="radio" name="rdCntctCaptcha" id="rdCntctCaptcha1" value="1" checked  class="form-check-input" />ON </label>
                    <label class="form-check-label">
                    <input type="radio" name="rdCntctCaptcha" id="rdCntctCaptcha0" value="0" <? if($cntctCaptcha == '0') echo 'checked'; ?>  class="form-check-input" />OFF </label>
					</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 control-label"><? echo $this->lang->line('BE_LBL_604'); ?>:</label>
                <div class="col-md-6">
					<div class="form-check form-check-inline mt-0">
					<label class="form-check-label mr-3">
                    <input type="radio" name="rdNewClients" id="rdNewClients0" value="0" checked  class="form-check-input" /><? echo $this->lang->line('BE_LBL_606'); ?> </label>
                    <label class="form-check-label">
                    <input type="radio" name="rdNewClients" id="rdNewClients1" value="1" <? if($newClients == '1') echo 'checked'; ?>  class="form-check-input" /><? echo $this->lang->line('BE_LBL_605'); ?> </label>
					</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 control-label">Transfer Credits Automatically For New Customers:</label>
                <div class="col-md-4">
                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                        <input type="checkbox" name="chkAutoCredits" id="chkAutoCredits" <?php echo $AUTO_CREDITS == 1 ? 'checked' : '';?> class="toggle" /><span><label for="chkAutoCredits"></label></span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 control-label">New Customers Can Buy Credits:</label>
                <div class="col-md-2">
                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                        <input type="checkbox" name="chkCanBuyCr" id="chkCanBuyCr" <? echo $ADD_CREDITS_DEFAULT_VAL == 1 ? 'checked' : ''; ?> class="toggle" /><span><label for="chkCanBuyCr"></label></span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 control-label"><? echo $this->lang->line('BE_LBL_840'); ?>:</label>
                <div class="col-md-2">
                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                        <input type="checkbox" name="chkSendEmails" id="chkSendEmails" <? echo $priceEmails == 1 ? 'checked' : ''; ?> class="toggle"/><span><label for="chkSendEmails"></label></span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 control-label"><? echo $this->lang->line('BE_LBL_576'); ?>:</label>
                <div class="col-md-2">
					<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                        <input type="checkbox" name="chkPinCode" id="chkPinCode" <? echo $pinCode == 1 ? 'checked' : '';?> class="toggle"/><span><label for="chkPinCode"></label></span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 control-label">Unpaid Admin Invoices Notification:</label>
                <div class="col-md-4">
                    <select id="invsNotify" name="invsNotify" class="form-control select2me">
                    	<option value="0" selected="selected">Do Not Apply</option>
                        <?
                            for($z = 1; $z <= 7; $z++)
                            {
                                $selected = '';
                                if($invsNotify == $z)
                                    $selected = 'selected';
                                echo '<option value="'.$z.'" '.$selected.'>Every '.$z.' day(s)</option>';
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 control-label">Rush Payment Notification:</label>
                <div class="col-md-4">
                    <select id="rushPmnt" name="rushPmnt" class="form-control select2me">
                    	<option value="0" selected="selected">Do Not Apply</option>
                        <?
                            for($z = 2; $z <= 10; $z++)
                            {
                                $selected = '';
                                if($rushPmnt == $z)
                                    $selected = 'selected';
                                echo '<option value="'.$z.'" '.$selected.'>'.$z.'</option>';
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 control-label"><? echo $this->lang->line('BE_LBL_608'); ?>:</label>
                <div class="col-md-3">
                    <select id="ffPwdChDays" name="ffPwdChDays" class="form-control select2me" data-placeholder="Select...">
                        <?
                            for($z = 0; $z <= 90; $z++)
                            {
                                $selected = '';
                                if($ffPwdChDays == $z)
                                    $selected = 'selected';
								if($z == 0)
    	                            echo '<option value="0" '.$selected.'>'.$this->lang->line('BE_LBL_613').'</option>';
								else
	                                echo '<option value="'.$z.'" '.$selected.'>'.$z.'</option>';
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 control-label"><? echo $this->lang->line('BE_LBL_383'); ?>:<span class="required_field">*</span></label>
                <div class="col-md-2">
                <input type="text" class="form-control" placeholder="Enter text" name="txtLoginAttempts" maxlength="100" id="txtLoginAttempts" value="<? echo($loginAttempts);?>"  >
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 control-label"><? echo $this->lang->line('BE_LBL_385'); ?>:<span class="required_field">*</span></label>
                <div class="col-md-2">
                <input type="text" class="form-control" placeholder="Enter text" name="txtMinCredits" maxlength="8" id="txtMinCredits" value="<? echo($minCredits);?>" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 control-label"><? echo $this->lang->line('BE_LBL_386'); ?>:<span class="required_field">*</span></label>
                <div class="col-md-2">
                <input type="text" class="form-control" placeholder="Enter text" name="txtMaxCredits" maxlength="8" id="txtMaxCredits" value="<? echo($maxCredits);?>" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 control-label"><? echo $this->lang->line('BE_LBL_823'); ?>:<span class="required_field">*</span></label>
                <div class="col-md-8">
                <input type="text" class="form-control" placeholder="Enter Percentage" name="txtCrdtsTrsfrFee" maxlength="5" id="txtCrdtsTrsfrFee" value="<? echo $crdtsTrnsfrFee;?>" />
					<span <span class="form-text text-muted">
    	                % - If you want to set a fee while transferring credits into user accounts!
	                </span>
                </div>
            </div>
			<div class="form-group row">
				<div class="col-md-offset-4 col-md-8">
					<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> id="btnClients" name="btnClients" class="btn btn-primary"><? echo $this->lang->line('BE_LBL_72'); ?></button>
					<div style="display:none;" id="dvLoader2"><img src="<?php echo base_url('assets/images/loading.gif');?>" border="0" alt="Please wait..." /></div>
				</div>
			</div>
		</div>
    </div>
</div>
</div>
