<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_694'); ?></h5>
			</div>
			<div class="card-body">
		<!-- BEGIN FORM-->
		<form action="#" class="form-horizontal" name="frm3" method="post" id="frm3">
				<div class="form-group row">
					<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_617'); ?>:</label>
					<div class="col-lg-8">
						<p style="font-size:13px;" class="form-control-static"><strong>/usr/bin/wget -O - -q -t 1 <?php echo base_url() . $CRON_FOLDER; ?>/sendimeiorders.php?apiId=<?php echo $id; ?></strong></p>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_618'); ?>:</label>
					<div class="col-lg-8">
						<p style="font-size:13px;" class="form-control-static"><strong>/usr/bin/wget -O - -q -t 1 <?php echo base_url() . $CRON_FOLDER; ?>/getimeiorders.php?apiId=<?php echo $id; ?></strong></p>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_620'); ?>:</label>
					<div class="col-lg-8">
						<p style="font-size:13px;" class="form-control-static"><strong>/usr/bin/wget -O - -q -t 1 <?php echo base_url() . $CRON_FOLDER; ?>/sendfileorders.php?apiId=<?php echo $id; ?></strong></p>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_621'); ?>:</label>
					<div class="col-lg-8">
						<p style="font-size:13px;" class="form-control-static"><strong>/usr/bin/wget -O - -q -t 1 <?php echo base_url() . $CRON_FOLDER; ?>/getfileorders.php?apiId=<?php echo $id; ?></strong></p>
					</div>
				</div>
			</div>
			<!-- END FORM-->
	</div>
</div>
