<!-- Page header -->
<div class="page-header border-bottom-0 pt-4">
	<div class="form-group row">
		<div class="col-lg-12">
			<?php
			if ($iFrm == '1')
				include APPPATH . 'scripts/serverservices_header.php';
			else if ($iFrm == '0')
				include APPPATH . 'scripts/services_header.php';
			?>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div style="padding-top: 10px !important;" class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i>
				<?php echo $this->lang->line('BE_CAT_1'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>
							</div>
						</div>
					<?php } ?>
					<?php echo form_open('', 'class="horizontal-form" id="frm" name="frm"'); ?>
					<input type="hidden" id="id" name="id" value="<?php echo($id); ?>"/>
					<input type="hidden" id="fs" name="fs" value="<?php echo($fs); ?>"/>
					<input type="hidden" id="iFrm" name="iFrm" value="<?php echo($iFrm); ?>"/>
					<div class="form-group row">

						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_CAT_1'); ?>
							:*</label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter Category" maxlength="50"
								   name="txtCategory" id="txtCategory" value="<?php echo($category); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_212'); ?>
							:</label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter HTML Title"
								   maxlength="100" name="txtHTMLTitle" id="txtHTMLTitle"
								   value="<?php echo($htmlTitle); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_213'); ?>
							:</label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter SEO Name" maxlength="100"
								   name="txtSEOName" id="txtSEOName" value="<?php echo($seoName); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_MR_38'); ?>
							:</label>
						<div class="col-lg-4">
									<textarea class="form-control" rows="3" id="txtMetaTags"
											  name="txtMetaTags"><?php echo $metaDesc; ?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_214'); ?>
							:</label>
						<div class="col-lg-4">
									<textarea class="form-control" rows="3" id="txtMetaKW"
											  name="txtMetaKW"><?php echo $metaKW; ?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-3"><?php echo $this->lang->line('BE_LBL_105'); ?>
							:</label>
						<div class="col-lg-9">
									<textarea class="ckeditor form-control" name="txtDesc"
											  rows="6"><?php echo $desc; ?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_GNRL_1'); ?>
							:</label>
						<div class="col-lg-4">
							<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
								<input type="checkbox" name="disable"
									   id="disable" <?php echo $disable == 1 ? 'checked' : ''; ?>
									   class="toggle"/><span><label for="disable"></label></span>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3"></div>
						<div class="col-lg-9">
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
						</div>
					</div>
					<?php echo form_close(); ?> <!-- END FORM-->
				</div>
			</div>
		</div>
	</div>
</div>
<script language="javascript" src="<?php echo base_url('assets/js/category.js'); ?>"></script>
