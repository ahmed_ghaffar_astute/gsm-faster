<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_124'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<input type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="<?php echo $this->lang->line('BE_GNRL_14').' a '.$this->lang->line('BE_PM_5'); ?>" onclick="window.location.href='<?php echo base_url('admin/sales/');?>paymentmethod?frmId=<?php echo $this->input->post_get('frmId');?>&fTypeId=<?php echo $this->input->post_get('fTypeId')?>'" class="btn btn-primary" />
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">

					<div class="table-responsive">
						<table class="table table-striped table-bordered table-advance table-hover">
							<thead>
							<tr class="bg-primary">
								<th>Payment Method</th>
								<th>Type</th>
								<th>Wholesale Fee</th>
								<th>Retail Fee</th>
								<th><?php echo $this->lang->line('BE_GNRL_3'); ?></th>
								<th></th>
							</tr>
							</thead>
							<tbody id="sortableLst">
							<?php
							if ($rsPMs) {
								foreach ($rsPMs as $row) {
									?>
									<tr id="listItem_<?php echo $row->PaymentMethodId; ?>">
										<td><i class="fa fa-sort"></i> &nbsp;&nbsp;<a
												href="<?php echo base_url('admin/sales/'); ?>paymentmethod?id=<?php echo $row->PaymentMethodId; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId'); ?>"><?php echo stripslashes($row->PaymentMethod); ?></a>
										</td>
										<td><?php echo stripslashes($row->PayMethodType); ?></td>
										<td><?php echo $row->Fee; ?></td>
										<td><?php echo $row->RetailFee; ?></td>
										<td><?php echo $row->DisablePaymentMethod == '1' ? 'Yes' : 'No'; ?></td>
										<td style="text-align:center" valign="middle">
											<a href="<?php echo base_url('admin/sales/'); ?>paymentmethod?id=<?php echo $row->PaymentMethodId; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><i
													class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> </a>
										</td>
									</tr>
									<?php
								}
							} else
								echo "<tr><td colspan='6'>" . $this->lang->line('BE_GNRL_9') . "</td></tr>";
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="form-group row">
			<div class="col-lg-12">
				<div class="page-title d-flex">
					<h4><i class="mr-2"></i> <?php echo 'PayPal Receiver Details'; ?></h4>
					<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
				</div>
				<div align="right">
				<input type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> value="Add"
					   onclick="window.location.href='<?php echo base_url('admin/sales/'); ?>pprecvrdet?frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId'); ?>'"
					   class="btn btn-primary"/>
				</div>
			</div>
			</div>
					<div class="form-group row">
						<div class="col-lg-12">

					<div class="table-responsive">
						<table class="table table-striped table-bordered table-advance table-hover">
							<thead>
							<tr class="bg-primary">
								<th>PayPal ID</th>
								<th><?php echo $this->lang->line('BE_GNRL_3'); ?></th>
								<th></th>
								<th></th>
							</tr>
							</thead>
							<tbody>
							<?php
							if ($rsPPR) {
								foreach ($rsPPR as $row) {
									?>
									<tr>
										<td>&nbsp;&nbsp;<a
												href="<?php echo base_url('admin/sales/'); ?>pprecvrdet?id=<?php echo $row->ReceiverId; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><?php echo stripslashes($row->Username); ?></a>
										</td>
										<td><?php echo $row->DisablePP == '1' ? 'Yes' : 'No'; ?></td>
										<td style="text-align:center" valign="middle">
											<a href="<?php echo base_url('admin/sales/'); ?>pprecvrdet?id=<?php echo $row->ReceiverId; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><i
													class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> </a>
										<td style="text-align:center" valign="middle">
											<a href="<?php echo base_url('admin/sales/'); ?>paymentmethods?del=1&id=<?php echo($row->ReceiverId); ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"
											   onclick="return confirm('<?php echo $this->lang->line('BE_LBL_32'); ?>')"><i
													class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></td>
										</td>
									</tr>
									<?php
								}
							} else
								echo "<tr><td colspan='4'>" . $this->lang->line('BE_GNRL_9') . "</td></tr>";
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<script language="javascript" src="<?php echo base_url('assets/js/pmethods.js') ?>"></script>
