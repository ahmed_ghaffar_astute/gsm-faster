<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title"><?php  echo $this->lang->line('BE_DB_WELCOME'); ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <?php
                    if($rSet)
                    {
                    
                        if($totalRows > $limit)
                            doPages($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next);
                    }
                ?>
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-bars"></i><?php echo $this->lang->line('BE_DB_WELCOME'); ?>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th><?php echo $this->lang->line('BE_INDEX_USRNAME'); ?></th>
                                <th><?php echo $this->lang->line('BE_USR_9'); ?></th>
                                <th><?php echo $this->lang->line('BE_LBL_345'); ?></th>
                                <th>IP</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if($rSet)
                            {
                                foreach($rSet as $row)
                                {
                                ?>
                                    <tr>
                                        <td class="highlight"><div class="success"></div><a href="javascript:void(0);"><?php echo($row->UserName);?></a></td>
                                        <td><?php echo stripslashes($row->UName);?></td>
                                        <td><?php echo $row->Email;?></td>
                                        <td><?php echo $row->IP;?></td>
                                    </tr>
                            <?php
                                }
                            }
                            else
                                echo "<tr><td colspan='5'>".$this->lang->line('BE_GNRL_9')."</td></tr>";
                            ?>
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>