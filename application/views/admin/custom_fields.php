<!-- Page header -->
<div class="page-header border-bottom-0">
    <div class="page-header-content header-elements-md-inline">
        <div style="padding-top: 10px !important;" class="page-title d-flex">
            <h4>
                <i class="icon-arrow-left52 mr-2"></i>
                <?php echo $this->lang->line('BE_LBL_819'); ?> - <?php echo $hdr; ?>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none">
                <i class="icon-more"></i>
            </a>
        </div>
		<div align="right">
			<input type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="<?php echo $this->lang->line('BE_GNRL_14').' '.$this->lang->line('BE_LBL_819'); ?> - <?php echo $hdr; ?>" onclick="window.location.href='<?php echo base_url('admin/services/customfield?sc'.$serviceType.'&frmId='.$this->input->post_get('frmId').'&fTypeId='.$this->input->post_get('fTypeId')) ;?>'" class="btn btn-primary" />
		</div>
    </div>
</div>
<!-- /page header -->
<div class="content pt-0">
    <div class="card">
        <div class="row">
            <div class="col-md-12">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div id="dvMsg" style="display:none;" class="alert alert-success">&nbsp;</div>
                        </div>
					</div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-advance table-hover">
                                <thead>
                                    <tr class="bg-primary">
                                        <th><?php echo $this->lang->line('BE_PCK_17'); ?></th>
                                        <th><?php echo $this->lang->line('BE_PCK_19'); ?></th>
                                        <th><?php echo $this->lang->line('BE_LBL_717'); ?></th>
                                        <th><?php echo $this->lang->line('BE_GNRL_3'); ?></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="sortableLst">
                                    <?php
                                    if($rsFields)
                                    {
                                        foreach($rsFields as $row )
                                        {
                                        ?>
                                            <tr id="listItem_<?php echo $row->FieldId;?>">
                                                <td><i class="fa fa-sort"></i> &nbsp;&nbsp;<?php echo stripslashes($row->FieldLabel);?></td>
                                                <td><?php echo $row->FieldType;?></td>
                                                <td><?php echo $row->Mandatory == '1' ? 'Yes' : 'No';?></td>
                                                <td><?php echo $row->DisableField == '1' ? 'Yes' : 'No';?></td>
                                                <td style="text-align:center" valign="middle">
                                                    <a href="<?php echo base_url('admin/services/customfield?id='.$row->FieldId.'&sc='.$serviceType.'&frmId='.$this->input->post_get('frmId').'&fTypeId='.$this->input->post_get('fTypeId'));?>" ><i class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> </a>
                                                </td>
                                                <td style="text-align:center" valign="middle">
                                                <?php if($row->FieldType == 'Drop Down' || $row->FieldType == 'Radio Button') { ?>
                                                    <a href="<?php echo base_url('admin/services/customfieldvalues?id='.$row->FieldId.'&sc='.$serviceType.'&frmId='.$this->input->post_get('frmId').'&fTypeId='.$this->input->post_get('fTypeId'));?>"><i class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> <?php echo $this->lang->line('BE_LBL_719'); ?></a>
                                                <?php } else echo '-';?>
                                                </td>
                                            </tr>
                                    <?php }?>
                                    <?php }else{ ?>
                                        <tr><td colspan="6"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td></tr>
                                    <?php } ?>
                                        <input type="hidden" name="sc" id="sc" value="<?php echo $serviceType;?>" />
                                </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui-1.7.1.custom.min.js');?>"></script>
<script language="javascript" src="<?php echo base_url('assets/js/customfields.js');?>"></script>
