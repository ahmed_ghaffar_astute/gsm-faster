<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_857'); ?></h5>
			</div>
			<div class="card-body">
				<div class="form-group row">
				<label class="col-lg-4 col-form-label"><?php echo $this->lang->line('BE_LBL_858'); ?>:</label>
                <div class="col-md-4">
                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                        <input type="checkbox" name="chkSMTP" id="chkSMTP" <? echo $smtp == 1 ? 'checked' : '';?> class="toggle"/><span><label for="chkSMTP"></label></span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-4 col-form-label"><?php echo $this->lang->line('BE_LBL_859'); ?>:</label>
                <div class="col-md-4">
                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                        <input type="checkbox" name="chkSMTPAuth" id="chkSMTPAuth" <? echo $smtpAuth == 1 ? 'checked' : '';?> class="toggle"/><span><label for="chkSMTPAuth"></label></span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-4 col-form-label"><?php echo $this->lang->line('BE_LBL_860'); ?>:</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" placeholder="Enter SMTP Host" maxlength="100" name="txtSMTPHost" id="txtSMTPHost" value="<? echo($smtpHost);?>" >
                    <input type="hidden" id="customDsgn" name="customDsgn" value="<? echo $customDesign; ?>" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-4 col-form-label"><?php echo $this->lang->line('BE_LBL_861'); ?>:</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" placeholder="Enter SMTP Port" maxlength="10" name="txtSMTPPort" id="txtSMTPPort" value="<? echo($smtpPort);?>">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-4 col-form-label"><? echo $this->lang->line('BE_INDEX_USRNAME'); ?>:</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" placeholder="Enter <? echo $this->lang->line('BE_INDEX_USRNAME'); ?>" name="txtSMTPUN" maxlength="100" id="txtSMTPUN" value="<? echo($smtpUN);?>">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-4 col-form-label"><? echo $this->lang->line('BE_INDEX_PWD'); ?>:</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" placeholder="Enter <? echo $this->lang->line('BE_INDEX_PWD'); ?>" name="txtSMTPPwd" maxlength="150" id="txtSMTPPwd" value="<? echo($smtpPwd);?>" />
                </div>
            </div>
			<div class="form-group row">
				<div class="col-md-offset-4 col-md-8">
					<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> id="btnSMTP" name="btnSMTP" class="btn btn-primary"><? echo $this->lang->line('BE_LBL_72'); ?></button>
					<img id="imgSMTP" style="display:none;" src="<?php echo base_url('assets/images/loading.gif');?>" border="0" alt="Please wait..." />
				</div>
			</div>

        </div>
    </div>
</div>
