<!-- Page header -->
<div class="page-header border-bottom-0 pt-4">
	<div class="form-group row">
		<div class="col-lg-12">
			<?php include APPPATH . 'scripts/services_header.php'; ?>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Page header -->
<div class="page-header border-bottom-0">
    <div class="page-header-content header-elements-md-inline">
        <div style="padding-top: 10px !important;" class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i>
                <?php echo $fs == '0' ? $this->lang->line('BE_PCK_HD_1') : $this->lang->line('BE_PCK_25'); ?></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
    <div class="card">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card-body">
                    <!-- Our Working Area Start -->
                    <?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>
							</div>
						</div>
                    <?php } ?>
                    <?php echo form_open(base_url('admin/services/services?frmId=5&fTypeId=2'), ' id="frm" name="frm"'); ?>
						<div class="form-group row">
							<div class="col-lg-6">
								<?php $category_data = package_category($strWhereCat); ?>
								<label class="control-label"><?php echo $this->lang->line('BE_LBL_219'); ?></label>
								<select name="categoryId" id="categoryId" class="js-select2 form-control select2me"
									data-placeholder="<?php echo $this->lang->line('BE_LBL_256'); ?>">
									<option value="0"><?php echo $this->lang->line('BE_LBL_255'); ?></option>
									<?php FillCombo($categoryId, $category_data); ?>
								</select>
							</div>
							<div class="col-lg-6">
								<?php $api_data = fetch_api_data_by_concat(); ?>
								<label class="control-label"><?php echo $this->lang->line('BE_PCK_15'); ?></label>
								<select name="apiId" id="apiId" class="js-select2 form-control select2me">
									<option value="0"><?php echo $this->lang->line('BE_LBL_278'); ?></option>
									<?php FillCombo($apiId, $api_data); ?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<label class="control-label"><?php echo $this->lang->line('BE_PCK_HD'); ?></label>
								<select name="packageId" id="packageId" class="js-select2 form-control select2me"
									data-placeholder="Select...">
									<?php
							$PCK_TITLE = 'PackageTitle';
							$CATEGORY = 'Category';
							include APPPATH . 'scripts/packagesdropdown.php';
							?>
								</select>
							</div>
							<div class="col-lg-6">
								<label class="control-label"><?php echo $this->lang->line('BE_CODE_5'); ?></label>
								<select name="srvStatus" class="js-select2 form-control select2me">
									<option value="0" <?php if ($srvStatus == '0') echo 'selected'; ?>>Active</option>
									<option value="1" <?php if ($srvStatus == '1') echo 'selected'; ?>>In Active</option>
									<option value="2" <?php if ($srvStatus == '2') echo 'selected'; ?>>Both</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-4">
								<div class="form-group LeftPadding125">
									<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
										class="btn btn-primary"
										onclick="setValue('0');document.getElementById('frm').submit();"><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
								</div>
							</div>
						</div>

						<div class="form-group row">
							<div class="col-lg-3">

								<?php $price_plan = fetch_price_plan_data(); ?>
								<!--                                        <label class="control-label">&nbsp;</label><br />-->
								<select name="groupId" id="groupId" class="js-select2 form-control select2me">
									<option value="0">Choose Group</option>
									<?php FillCombo(0, $price_plan); ?>
								</select>
							</div>
							<div class="col-lg-9">
								<div align="right">
									<!--											<label class="control-label">&nbsp;</label><br />-->
									<input type="submit" value="Show For Group"
										onclick="setValue('5');return confirm('Are you sure you want to allow selected services for the group?');"
										class="btn btn-primary Margin-Left5" />
									<input type="submit" value="Hide For Group"
										onclick="setValue('4');return confirm('Are you sure you want to hide selected services for the group?');"
										class="btn btn-danger Margin-Left5" />
									<input type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
										value="<?php echo $this->lang->line('BE_LBL_102'); ?>" onClick="exportServices();"
										class="btn btn-warning Margin-Left5" />
									<input type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
										value="Show For All Users"
										onclick="setValue('2');return confirm('Are you sure you want to allow selected services for all users?');"
										class="btn btn-primary Margin-Left5" name="btnSubmit" />
									<input type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
										value="Hide For All Users"
										onclick="setValue('3');return confirm('Are you sure you want to hide selected services for all users?');"
										class="btn btn-danger Margin-Left5" name="btnSubmit1" />
									<input type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
										value="<?php echo $this->lang->line('BE_LBL_72'); ?>" onclick="setValue('1');"
										class="btn btn-primary Margin-Left5" name="btnSubmit2" />
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-12">
								<table class="table table-striped table-bordered table-advance table-hover table-responsive">
									<thead>
									<tr class="bg-primary">
										<th><?php echo $this->lang->line('BE_PCK_13'); ?></th>
										<th><?php echo $this->lang->line('BE_PCK_HD_4'); ?></th>
										<th><?php echo $this->lang->line('BE_PCK_14'); ?></th>
										<th><?php echo $this->lang->line('BE_PCK_15'); ?></th>
										<th style="text-align:center;">Service Status</th>
										<th style="text-align:right;"><?php echo $this->lang->line('BE_LBL_52'); ?> </th>
										<?php if (array_key_exists($deleteKey, $ARR_ADMIN_FORMS)) { ?>
											<?php if (!$IS_DEMO) { ?>
												<th></th>
											<?php } ?>
										<?php } ?>
										<th style="text-align: center;"><input type="checkbox" class="chkSelect" id="chkSelect" name="chkSelect"  onClick="selectAllChxBxs('chkSelect', 'chkPacks', <?php echo $count; ?>);" value="true">
										</th>
									</tr>
									</thead>
									<tbody>
									<?php
									$strCurrPacks = 0;
									if ($count != 0) {
										$i = 0;
										foreach ($rsPacks as $row) {
											$strCurrPacks .= ', ' . $row->PackageId;
											$packStatus = $row->DisablePackage == '0' ? 'Active' : 'In Active';
											?>
											<tr>
												<td class="highlight">
													<div class="success"></div>
													<a
														href="<?php echo base_url('admin/services/package?fs=' . $fs . '&id=' . $row->PackageId); ?>"><?php echo stripslashes($row->Category); ?></a>
												</td>
												<td><?php echo stripslashes($row->PackageTitle); ?></td>
												<td>
													<input type="text" name="packagePrice[<?php echo $row->PackageId; ?>][]" id="<?php echo $row->PackageId; ?>" value="<?php echo roundMe($row->PackagePrice); ?>" style="width:75px;" class="form-control" />
												</td>
												<td><?php echo $row->API == '' ? '-' : stripslashes($row->API); ?></td>
												<td style="text-align:center" valign="middle">
													<a href="JavaScript:void(0);" class="btn default btn-xs <?php echo $row->DisablePackage == '0' ? 'green' : 'red' ?>">
														<?php echo $packStatus; ?>
													</a>
												</td>
												<td style="text-align:center" valign="middle" width="5%">
													<a href="<?php echo base_url('admin/services/package?fs=' . $fs . '&id=' . $row->PackageId); ?>">
														<i class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i>
													</a>
												</td>
												<?php if (array_key_exists($deleteKey, $ARR_ADMIN_FORMS)) { ?>
													<?php if (!$IS_DEMO) { ?>
														<td style="text-align:center" valign="middle" width="5%">
															<a href="<?php echo base_url('admin/services/services?del=1&fs=' . $fs . '&packId=' . $row->PackageId . '&packageId=' . $packageId . '&categoryId=' . $categoryId); ?>"  onclick="return confirm('Are you sure you want to archive this service?')">
																<i class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i>
															</a>
														</td>
													<?php } ?>
												<?php } ?>
												<td align="center">
													<input type="checkbox" class="chkSelect" id="chkPacks<?php echo $i; ?>" name="chkPacks[]" value="<?php print $row->PackageId; ?>" <?php if ($row->DisablePackage == '1') echo 'checked'; ?> />
												</td>
											</tr>
											<?php
											$i++;
										}
									} else
										echo "<tr><td colspan='8'>" . $this->lang->line('BE_GNRL_9') . "</td></tr>";
									?>
									<input type="hidden" value="0" name="cldFrm" id="cldFrm" />
									<input type="hidden" value="<?php echo $dis ?>" name="dis" id="dis" />
									<input type="hidden" value="<?php echo $fs ?>" name="fs" />
									<input type="hidden" value="<?php echo $strCurrPacks; ?>" name="currPcks" />
									</tbody>
								</table>
							</div>
						</div>
						<div class="form-group row">
							<div style="display:flex;align-items: center;" class="col-lg-2">
								<?php $NO_OF_RECORDS = array("10" => 10, "50" => 50, "100" => 100, "200" => 200, "300" => 300, "400" => 400, "500" => 500, "1000" => 1000) ?>
								<label class="control-label"><strong>Records Per Page</strong></label>
							</div>	
							<div class="col-lg-4">
								<select style="height: auto !important;" name="records" class="form-control select2me"
									data-placeholder="Select..."
									onchange="setValue('0');document.getElementById('frm').submit();">
									<?php
										foreach ($NO_OF_RECORDS as $key => $value) {
											$selected = '';
											if ($limit == $key)
												$selected = 'selected';
											echo '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
										}
										?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<?php
									if ($count != 0) {
										$rsPackspager = fetch_category_packages_api($strWhere);

										$totalRows = count($rsPackspager);
										if ($totalRows > $limit)
											doPages($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next);
									}
								?>
							</div>
						</div>
							
                    	
					<?php echo form_close(); ?>
                </div>
            </div>
        </div>

    </div>
</div>


<script>
function setValue(i) {
    document.getElementById('cldFrm').value = i;
}

function exportServices() {
    var strIds = getServiceIds();
    window.location.href = '<?php echo base_url('admin/services/exportservices?ids='); ?>' + strIds;
}

function getServiceIds() {
    var totalSrvcs = '<?php echo $count; ?>';
    var strIds = '0';
    for (var i = 0; i < totalSrvcs; i++) {
        if (document.getElementById('chkPacks' + i).checked) {
            strIds += ',' + document.getElementById('chkPacks' + i).value;
        }
    }
    return strIds;
}
</script>
