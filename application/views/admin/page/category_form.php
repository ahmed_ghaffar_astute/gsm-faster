<div class="page-content">
	<div class="row" style="padding-left: 30px;padding-right: 30px">
  <div class="col-md-12">
	  <div class="Heading-Bg">
		  <h3 class="page-title">
			  <i class="fa fa-th-large" aria-hidden="true"></i>
			  <?php echo $this->lang->line('BE_LBL_855'); ?>
		  </h3>
	  </div>
	  <div class="block-web">
		<div class="card">
			<div class="row PaddingBtm20">
				<div class="col-md-12">
					<?php
						define('IMG_PATH', base_url().'assets/images/category/');

						if($this->session->flashdata('response_msg')) {
						  $message = $this->session->flashdata('response_msg');
						  ?>
							<div class="<?=$message['class']?> alert-dismissible" role="alert" style="margin-left: 30px;margin-right: 30px">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							  <?=$message['message']?>
							</div>
						  <?php
						}
					?>
					<div class="actions">
						<a href="<?php echo base_url('admin/category/');?>" class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
					</div>
				</div>
		  	</div>
			<div class="row">
				<div class="col-md-12">
					<div class="card-body mrg_bottom">
						<?php
						$formAction = "";
						if(!isset($category))
						{
							$formAction = site_url('admin/category/addForm');
						}
						else
						{
							$formAction = site_url('admin/category/editForm/'.$category[0]->id);
						}
						echo form_open($formAction, array('name' => 'categoryForm', 'id'=> 'categoryForm', 'class' => 'form-horizontal', 'enctype' => "multipart/form-data"));?>
						<div class="section-body">
							<div class="row">
								<div class="form-group">
									<div class="col-md-4">
										<label class="control-label">Category Name :</label>
									</div>
									<div class="col-md-6">
										<input type="text" placeholder="Category Title" id="title" name="title" class="form-control" value="<?php if(isset($category)){ echo $category[0]->category_name;} ?>">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<div class="col-md-4">
										<label class="control-label">Select Image :
											<p class="control-label-help hint_lbl">(Recommended resolution: 400x240)</p>
											<p class="control-label-help hint_lbl">(Accept only png, jpg, jpeg, gif image files)</p>
										</label>
									</div>
									<div class="col-md-6">
										<div class="fileupload_block">
											<div class="row" style="display: flex; align-items: center;">
												<div class="col-md-6">
													<input type="file" name="file_name" value="fileupload" id="fileupload" accept=".gif, .jpg, .png, jpeg">
												</div>
												<div class="col-md-6 text-center">
													<div class="fileupload_img"><img type="image" src="<?php
														if(!isset($category)){ echo base_url('assets/images/no-image-1.jpg'); }else{  echo IMG_PATH.$category[0]->category_image; }
														?>" alt="category image" style="width: 150px;height: 90px" />
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<hr/>
							<p style="font-weight: 500;font-size: 16px">Product Features Category Wise:</p>
							<hr/>
							<?php
							if(isset($category)){
								$db_features=explode(',', $category[0]->product_features);
							}
							?>
							<div class="row">
								<div class="col-md-3">
									<p style="font-weight: 500;font-size: 15px">Color <a href="javascript:void(0)" data-trigger="focus" data-toggle="popover" data-content="If your product is available in multiple colors"><i class="fa fa-exclamation-circle"></i></a></p>
									<input type="checkbox" name="product_features[]" <?php if(isset($category) && in_array('color',$db_features)){ echo 'checked'; } ?> value="color" id="color_cbx" class="cbx hidden">
									<label for="color_cbx" class="lbl"></label>
								</div>
								<div class="col-md-3">
									<p style="font-weight: 500;font-size: 15px">Size <a href="javascript:void(0)" data-trigger="focus" data-toggle="popover" data-content="If your product is available in different size"><i class="fa fa-exclamation-circle"></i></a></p>
									<input type="checkbox" name="product_features[]" <?php if(isset($category) && in_array('size',$db_features)){ echo 'checked'; } ?> value="size" id="size_cbx" class="cbx hidden">
									<label for="size_cbx" class="lbl"></label>
								</div>
							</div>
							<hr/>
							<div class="row">
								<div class="form-group">
									<div class="col-md-12">
										<button type="submit" name="btn_submit" class="btn btn-primary">Save</button>
									</div>
								</div>
							</div>

						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	  </div>
  </div>
</div>
</div>

<script type="text/javascript">

  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $("input[name='file_name']").next(".fileupload_img").find("img").attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("input[name='file_name']").change(function() { 
    readURL(this);
  });

  $(function () {
    $('[data-toggle="popover"]').popover()
  })

</script> 
