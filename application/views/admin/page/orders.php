<style type="text/css">
  .dataTables_wrapper{
    overflow: unset !important;
  }
  .btn-success{
    background-color: #398439 !important;
    border-color: #398439 !important;
  }
  .label-success-cust{
    background: green !important;
  }
</style>
<div class="row" style="padding-left:30px;padding-right: 30px">
  <div class="col-xs-12">
    <div class="card mrg_bottom">

      <div class="col-md-12 mrg-top">
          <div style="float: left;font-size: 21px;font-weight: 500;padding-top: 20px;"><?php echo $this->lang->line('BE_LBL_856'); ?></div>
            <div class="col-md-12" style="margin-top: 1.5em">
              <div class="col-md-3 col-xs-12 text-right" style="float: right;">
                <form method="post" action="">
                    <div class="checkbox" style="width: 100px;margin-top: 5px;margin-left: 10px;float: left;right: 110px;position: absolute;">
                      <input type="checkbox" id="checkall">
                      <label for="checkall">
                          Select All
                      </label>
                    </div>
                    <div class="dropdown" style="float:right">
                      <button class="btn btn-primary dropdown-toggle btn_cust" type="button" data-toggle="dropdown">Action
                      <span class="caret"></span></button>
                      <ul class="dropdown-menu" style="right:0;left:auto;">
                        <li><a href="" class="actions" data-action="delete">Delete !</a></li>
                      </ul>
                    </div>
              </div>
            </div>

            <table class="datatable table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th></th>
                  <th>Order ID</th>						 
                  <th>User Name</th>
        				  <th>User Phone</th>
        				  <th nowrap="">Ordered On</th>
        				  <th>Status</th>	 
                  <th class="cat_action_list">Action</th>
                </tr>
              </thead>
              <tbody>
              	<?php 
  		            $i=1;
                  $ci =& get_instance();
  		            foreach ($order_list as $key => $row) 
  		            {
  		          ?>
                <tr class="item_holder">
                  <td>  
                    <div>
                        <div class="checkbox">
                          <input type="checkbox" name="post_ids[]" id="checkbox<?php echo $i;?>" value="<?php echo $row->id; ?>" class="post_ids">
                          <label for="checkbox<?php echo $i;?>">
                          </label>
                        </div>
                    </div>
                  </td>
                  <td><a href="<?php echo site_url("admin/orders/".$row->order_unique_id);?>" target="_blank"><?php echo $row->order_unique_id;?></a></td>
		              <td><?php echo $this->common_model->selectByidParam($row->order_address, 'tbl_addresses','name');?></td>   
                  <td><?php echo $this->common_model->selectByidParam($row->order_address, 'tbl_addresses','mobile_no');?></td>
		              <td><?php echo date('d-m-Y h:i A',$row->order_date);?></td>
		              <td>

                    <?php 
                      
                      $_bnt_class='label-primary';
                      $_btn_title=$ci->get_status_title($row->order_status);

                      switch ($row->order_status) {
                          case '1':
                              $_bnt_class='label-default';
                              break;
                          case '2':
                              $_bnt_class='label-warning';
                              break;
                          case '3':
                              $_bnt_class='label-success';
                              break;

                          case '4':
                              $_bnt_class='label-success-cust';
                              break;
                          
                          default:
                              $_bnt_class='label-danger';
                              break;
                      }

                    ?>

                    <span class="label <?=$_bnt_class?>"><?=$_btn_title?></span>

              		</td>
                  <td nowrap="">

                    <a href="" class="btn btn-warning btn_edit btn_status" data-toggle="tooltip" data-id="<?=$row->id?>" data-tooltip="Order Status"><i class="fa fa-wrench"></i></a>


                    <a href="<?php echo ($row->order_status!=5) ? site_url("admin/orders/print/".$row->order_unique_id) : 'javascript:void(0)';?>" class="btn btn-primary btn_edit" data-toggle="View" data-tooltip="Print" <?php echo ($row->order_status!=5) ? 'target="_blank"' : 'disabled' ?>><i class="fa fa-print"></i></a>

                    <a href="" class="btn btn-danger btn_delete" data-toggle="tooltip" data-id="<?=$row->id?>" data-tooltip="<?=$this->lang->line('delete_lbl')?>"><i class="fa fa-trash"></i></a>
                    
                  </td>
                </tr>
               <?php		
      					 $i++;
      			 	   }
      			   ?>
              </tbody>
            </table>
          </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>

<div id="orderStatus" class="modal fade" role="dialog" style="">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Order Status</h4>
        </div>
        <div class="modal-body" style="padding-top: 0px">
          
        </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  $(".btn_status").click(function(e){  

        e.preventDefault();

      $("#orderStatus").modal("show");

      var _id=$(this).data("id");

      var href='<?=base_url()?>admin/order/order_status_form/'+_id;

      $("#orderStatus .modal-body").load(href);

  });
</script>

<script type="text/javascript">


  // for multiple action

  $(".actions").click(function(e){
    e.preventDefault();

    var _table='tbl_order_details';

    var href='<?=base_url()?>admin/pages/perform_multipe';

    var _ids = $.map($('.post_ids:checked'), function(c){return c.value; });
    var _action=$(this).data("action");

    if(_ids!='')
    {
      swal({
        title: "Do you really want to perform?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger btn_edit",
        cancelButtonClass: "btn-warning btn_edit",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: false,
        showLoaderOnConfirm: true
      },
      function(isConfirm) {
        if (isConfirm) {

          $.ajax({
              type:'post',
              url:href,
              dataType:'json',
              data:{ids:_ids,for_action:_action,table:_table},
              success:function(res){
                  console.log(res);
                  if(res.status=='1'){
                    swal({
                        title: "Successfully", 
                        text: "You have successfully done", 
                        type: "success"
                    },function() {
                        location.reload();
                    });
                  }
                }
            });

        }else{
          swal.close();
        }
      });
    }
    else{
        $('.notifyjs-corner').empty();
        $.notify(
          'No record selected !', 
          { position:"top center",className: 'danger' }
        );
    }

  });


  $("#checkall").click(function () {
    $('.post_ids').not(this).prop('checked', this.checked);
  });


  // for delete order
  $(".btn_delete").click(function(e){
      e.preventDefault();
      var _id=$(this).data("id");

      e.preventDefault(); 

      var href='<?=base_url()?>admin/order/delete/'+_id;

      var btn = this;

      swal({
        title: "Are you sure to delete this order ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger btn_edit",
        cancelButtonClass: "btn-warning btn_edit",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: false,
        showLoaderOnConfirm: true
      },
      function(isConfirm) {
        if (isConfirm) {

          $.ajax({
            type:'GET',
            url:href,
            success:function(res){
                if($.trim(res)=='success'){
                  swal({
                      title: "Deleted", 
                      text: "Your order has been deleted", 
                      type: "success"
                  },function() {
                      $(btn).closest('.item_holder').fadeOut("200");
                  });
                }
                else
                {
                  //alert("Error");
                }

              }
          });
          
        }else{
          swal.close();
        }
      });
  });
</script>
