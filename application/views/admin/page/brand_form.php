<div class="row" style="padding-left: 30px;padding-right: 30px">
  <div class="col-md-12">
    <div class="card">
      <div class="page_title_block">
        <div class="col-md-5 col-xs-12">
          <div class="page_title"><?=$current_page?></div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row mrg-top">
        <div class="col-md-12">
            <?php
                define('IMG_PATH', base_url().'assets/images/brand/');

                if($this->session->flashdata('response_msg')) {
                  $message = $this->session->flashdata('response_msg');
                  ?>
                    <div class="<?=$message['class']?> alert-dismissible" role="alert" style="margin-left: 30px;margin-right: 30px">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                      <?=$message['message']?>
                    </div>
                  <?php
                }
            ?>

        </div>
      </div>
      <div class="card-body mrg_bottom"> 
        <form action="<?php if(!isset($brand)){ echo site_url('admin/brand/addForm'); }else{  echo site_url('admin/brand/editForm/'.$brand[0]->id); } ?>" method="post" id="categoryForm" class="form form-horizontal" enctype="multipart/form-data">
		<?php
			$formAction = "";
		if (!isset($brand)) {
			$formAction = site_url('admin/brand/addForm');
		} else {
			$formAction = site_url('admin/brand/editForm/' . $brand[0]->id);
		}
		echo form_open($formAction, array('name' => 'categoryForm', 'id'=> 'categoryForm', 'class' => 'form-horizontal', 'enctype' => "multipart/form-data"));?>
          <div class="section">
            <div class="section-body">
              <div class="form-group">
                <?php 
                  if(isset($brand)){
                    $cat_id=explode(',', $brand[0]->category_id);
                  }
                ?>
                <label class="col-md-3 control-label">Select Categories :-</label>
                <div class="col-md-6">
                  <select name="category_id[]" class="select2" multiple="">
                      <?php 
                          foreach ($category_list as $key => $value)
                          {
                            ?>
                            <option value="<?=$value->id?>" <?php if(isset($brand) && in_array($value->id,$cat_id)){ echo 'selected'; } ?>><?=$value->category_name?></option>
                            <?php
                          }
                      ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Brand Name :-
                </label>
                <div class="col-md-6">
                  <input type="text" placeholder="Brand Title" id="title" name="title" class="form-control" value="<?php if(isset($brand)){ echo $brand[0]->brand_name;} ?>">
                </div>
              </div>
              <div class="form-group">
                  <label class="col-md-3 control-label">Select Image :-
                    <p class="control-label-help hint_lbl">(Recommended resolution: 300x300,400x400 or Square Image)</p>
                    <p class="control-label-help hint_lbl">(Accept only png, jpg, jpeg, gif image files)</p>
                  </label>
                  <div class="col-md-6">
                    <div class="fileupload_block">
                      <input type="file" name="file_name" value="fileupload" id="fileupload" accept=".gif, .jpg, .png, jpeg">
                      
                      <div class="fileupload_img"><img type="image" src="<?php 
                        if(!isset($brand)){ echo base_url('assets/images/no-image-1.jpg'); }else{  echo IMG_PATH.$brand[0]->brand_image; } 
                      ?>" alt="category image" style="width: 90px;height: 90px" /></div>
                         
                    </div>
                  </div>
                </div>
              <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                  <button type="submit" name="btn_submit" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $("input[name='file_name']").next(".fileupload_img").find("img").attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("input[name='file_name']").change(function() { 
    readURL(this);
  });

</script> 
