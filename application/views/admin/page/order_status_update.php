<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/flat-admin.css')?>">

<style type="text/css">
  ul.timeline {
      list-style-type: none;
      position: relative;
  }
  ul.timeline:before {
      content: ' ';
      background: #d4d9df;
      display: inline-block;
      position: absolute;
      left: 29px;
      width: 2px;
      height: 100%;
      z-index: 400;
  }
  ul.timeline > li {
      margin: 20px 0;
      padding-left: 20px;
  }
  ul.timeline > li:before {
      content: ' ';
      background: white;
      display: inline-block;
      position: absolute;
      border-radius: 50%;
      border: 3px solid #22c0e8;
      left: 20px;
      width: 20px;
      height: 20px;
      z-index: 400;
  }
  ul.timeline p{
    font-size: 13px !important;
  }

  .checkbox label::before, .checkbox label::after, .radio label::before, .radio label::after{
    width: 23px;
    height: 23px;
  }

</style>
<h4>Expected Delivery Date: <input type="text" name="delivery_date" class="datepicker" value="<?=date('d-m-Y',$delivery_date)?>" name="" style="padding: 10px 10px;font-size: 16px;">&nbsp;&nbsp;<button class="btn btn-primary btn_update_date" style="padding: 10px 10px;font-size: 12px;margin-bottom: 0px"><i class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> Update</button></h4>



<hr/>
<ul class="timeline">
  <?php 
    $ci = &get_instance();

    $max_index=max(array_keys($order_data));

    foreach ($order_data as $key => $value) {
  ?>
  <li>
    <a href="javascript:void(0)" style="color: #000"><?=$ci->get_status_title($value->status_title);?></a>
    <a href="javascript:void(0)" style="float: right;color: #000"><?=date('d-m-Y',$value->created_at)?></a>
    <p>
      <?=$value->status_desc?>
    </p>
    <?php 
      if($max_index==$key){
    ?>
    <hr style="margin-top: 10px;margin-bottom: 10px" />
    <a href="" class="btn_delete" data-ord="<?=$value->order_id?>" data-status="<?=$value->status_title?>">
      <span class="label label-danger">Delete</span>
    </a>
    <?php } ?>
  </li>
  <?php } ?>
</ul>
<hr/>
	
  <form action="<?php echo base_url('admin/order/update_product_status') ?>" name=status_form id=status_form class='form-horizontal' >
  
  <input type="hidden" name="order_id" value="<?=$order_data[0]->order_id?>">
  <input type="hidden" name="user_id" value="<?=$order_data[0]->user_id?>">
  <input type="hidden" name="product_id" value="<?=$product_id?>">

  <div class="section">
    <div class="section-body">
      <div class="form-group">
        <label class="col-md-3 control-label">Status :-</label>
        <div class="col-md-9">
          <select name="order_status" class="select2" required="">
              <option value="">---Select---</option>
              <?php 
                foreach ($status_titles as $key => $value) {
                    ?>
                    <option value="<?=$value->id?>" <?php if($value->id <= $order_data[$max_index]->status_title){ echo 'disabled';} ?>><?=$value->title?></option>
                    <?php    
                }
              ?>
              <!-- <option value="other_status">Other</option> -->
          </select>

        </div>
      </div>
       <div class="form-group titleInput" style="display: none;">
        <label class="col-md-3 control-label">Status Title :-</label>
        <div class="col-md-9">
          <input type="text" placeholder="Enter Title" id="order_status_title" name="order_status_title" class="form-control">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-3 control-label">Sort Description :-</label>
        <div class="col-md-9">
          <textarea name="status_desc" id="status_desc" class="form-control" required=""></textarea>
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-9">
          <div class="checkbox checkbox-inline" style="padding-top: 7px;">
            <input type="checkbox" id="cbk_mail" name="send_mail">
            <label for="cbk_mail">
              Send email to user for their order status update.
            </label>
          </div>
        </div>
      </div>
      <br/>

      <div class="form-group">
        <div class="col-md-9 col-md-offset-3">
          <button type="submit" name="btn_submit" class="btn btn-primary btn_save">Save</button>
        </div>
      </div>
    </div>
  </div>
  </form>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>




<script type="text/javascript">

  $(document).ready(function(e){
    $(".datepicker").datepicker({ minDate: 0,dateFormat: 'dd-mm-yy' });
  });


  $("select[name='order_status']").change(function(e){
    if($(this).val()=='other_status'){
      $(".titleInput").show();
    }
    else{
      $(".titleInput").hide(); 
    }
  });


  $(".btn_update_date").click(function(e){
    e.preventDefault();
    var _delivery_date=$("input[name='delivery_date']").val();
    var _order_id=$("input[name='order_id']").val();

    var href='<?=base_url()?>admin/order/update_delivery_date';



    $.ajax({
      url:href,
      data:{'delivery_date':_delivery_date,'order_id':_order_id},
      type:'post',
      success:function(res){
          res=$.trim(res);
          if(res=='success'){
            alert('Expected delivery date is updated...');
          }
          else{
            alert("Oops ! there is error in updating status...");
          }
        },
        error : function(res) {
          alert("error");
        }

    });


  });


  $("#status_form").submit(function (e) {

    e.preventDefault();

    var _btn=$(this).find(".btn_save");

    _btn.attr("disabled", true);

    href = $(this).attr("action");

    $(this).find("button[name='btn_submit']").text("Please wait...");

    var data = $(this).serialize();//new FormData($(this)[0]);

    $.ajax({
      url:href,
      data: data,
      method:'POST',
      success:function(res){
          //alert(res);
          _btn.attr("disabled", false);
          if($.trim(res)=='true'){
            alert('Order status is successfully updated...');
            location.reload();
          }
          else{
            alert("Oops ! there is error in updating status...");
          }
          
        },
        error : function(res) {
          alert("error");
        }

    });

  });

  $(".btn_delete").click(function(e){
    e.preventDefault();

    if(confirm("Are you sure to delete order status?"))
    {
      var order_id=$(this).data("ord");
      var status_id=$(this).data("status");

      var ele=$(this).parent("li");  

      var href='<?=base_url()?>admin/order/delete_ord_status';

      $.ajax({
        url:href,
        data:{'order_id':order_id,'status_id':status_id},
        type:'POST',
        success:function(res){
            if($.trim(res)=='success'){
              ele.remove();
              alert('Order status is successfully remove...');
              location.reload();
            }
            else{
              alert("Oops ! there is error in updating status...");
            }
          },
          error : function(res) {
            alert("error");
          }

      });
    }


  });
</script> 
