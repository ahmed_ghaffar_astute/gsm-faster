<div class="page-content">
	<div class="row" style="padding-left: 30px;padding-right: 30px">
		<div class="Heading-Bg">
			<h3 class="page-title">
				<i class="fa fa-th-large" aria-hidden="true"></i>
				Edit Sub Category
			</h3>
		</div>
		<div class="block-web">
			<div class="card">
				<div class="row PaddingBtm20">
					<div class="col-md-12">
						<?php
						define('IMG_PATH', base_url().'assets/images/sub_category/');

						if($this->session->flashdata('response_msg')) {
							$message = $this->session->flashdata('response_msg');
							?>
							<div class="<?=$message['class']?> alert-dismissible" role="alert" style="margin-left: 30px;margin-right: 30px">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
								<?=$message['message']?>
							</div>
							<?php
						}
						?>
						<form action="<?php if(!isset($sub_category)){ echo site_url('admin/SubCategory/addForm'); }else{  echo site_url('admin/SubCategory/editForm/'.$sub_category[0]->id); } ?>" method="post" id="categoryForm" class="form form-horizontal" enctype="multipart/form-data">
							<?php
							$formAction = "";
							if(!isset($sub_category))
							{
								$formAction = site_url('admin/SubCategory/addForm');
							}
							else
							{
								$formAction = site_url('admin/SubCategory/editForm/'.$sub_category[0]->id);
							}

							echo form_open($formAction, array('name' => 'categoryForm', 'id'=> 'categoryForm', 'class' => 'form-horizontal', 'enctype' => "multipart/form-data"));?>
							<div class="section">
								<div class="section-body">
									<div class="row">
										<div class="form-group">
											<div class="col-md-4">
												<label class="control-label">Category Name :-</label>
											</div>
											<div class="col-md-6">
												<select name="category_id" class="select2">
													<option value="" selected>--Select Category--</option>
													<?php
													foreach ($category_list as $key => $value)
													{
														?>
														<option value="<?=$value->id?>" <?php if(isset($sub_category) && $sub_category[0]->category_id==$value->id){ echo 'selected';} ?>><?=$value->category_name?></option>
														<?php
													}
													?>
												</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<div class="col-md-4">
												<label class="control-label">Sub-Category Name :-</label>
											</div>
											<div class="col-md-6">
												<input type="text" placeholder="Sub-Category Title" id="title" name="title" class="form-control" value="<?php if(isset($sub_category)){ echo $sub_category[0]->sub_category_name;} ?>">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<div class="col-md-4">
												<label class="control-label">Select Image :-
													<p class="control-label-help hint_lbl">(Recommended resolution: 400x240)</p>
													<p class="control-label-help hint_lbl">(Accept only png, jpg, jpeg, gif image files)</p>
												</label>
											</div>
											<div class="col-md-6">
												<div class="fileupload_block">
													<div class="row" style="display: flex; align-items: center;">
														<div class="col-md-6">
															<input type="file" name="file_name" value="fileupload" id="fileupload" accept=".gif, .jpg, .png, jpeg">
														</div>
														<div class="col-md-6 text-center">
															<div class="fileupload_img"><img type="image" src="<?php
																if(!isset($sub_category)){ echo base_url('assets/images/no-image-1.jpg'); }else{  echo IMG_PATH.$sub_category[0]->sub_category_image; }
																?>" alt="sub_category image" style="width: 150px;height: 90px" />
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<div class="col-md-4">
												<label class="control-label">Show Product's List in Sub Category List <a href="<?=base_url('assets/images/product_list_hint.jpg')?>" target="_blank"><i class="fa fa-exclamation-circle"></i></a>:-
												</label>
											</div>
											<div class="col-md-6" style="margin-top: 15px">
												<input type="checkbox" name="show_on_off" <?php if(isset($sub_category) && $sub_category[0]->show_on_off=='1'){ echo 'checked'; } ?> value="1" id="color_cbx" class="cbx hidden">
												<label for="color_cbx" class="lbl"></label>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<div class="col-md-8 col-md-offset-4">
												<button type="submit" name="btn_submit" class="btn btn-primary">Save</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					</div>
					</div>
		</div>
	</div>
</div>



<script type="text/javascript">

	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$("input[name='file_name']").next(".fileupload_img").find("img").attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}
	$("input[name='file_name']").change(function() {
		readURL(this);
	});

</script>

