<style type="text/css">
  .select2{
    padding: 0px;
  }
  .select2-selection{
    min-height: auto !important;
  }
</style>

<div class="row" style="padding-left: 30px;padding-right: 30px">
  <div class="col-md-12">
    <div class="card">
      <div class="page_title_block">
        <div class="col-md-5 col-xs-12">
          <div class="caption"><?php echo $this->lang->line('BE_LBL_XXX'); ?></div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row mrg-top">
        <div class="col-md-12">
            <?php
                define('IMG_PATH', base_url().'assets/images/coupons/');

                if($this->session->flashdata('response_msg')) {
                  $message = $this->session->flashdata('response_msg');
                  ?>
                    <div class="<?=$message['class']?> alert-dismissible" role="alert" style="margin-left: 30px;margin-right: 30px">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                      <?=$message['message']?>
                    </div>
                  <?php
                }
            ?>

        </div>
      </div>
      <div class="card-body mrg_bottom"> 
        <form action="<?php if(!isset($coupon)){ echo site_url('admin/coupon/addForm'); }else{  echo site_url('admin/coupon/editForm/'.$coupon[0]->id); } ?>" method="post" class="form form-horizontal" enctype="multipart/form-data">
          <div class="section">
            <div class="section-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Coupon Description :-</label>
                <div class="col-md-6">
                  <textarea name="coupon_desc" id="coupon_desc" class="form-control"><?php if(isset($coupon)){ echo $coupon[0]->coupon_desc;} ?></textarea>
                </div>
              </div>
              <br/>
              <div class="form-group">
                <label class="col-md-3 control-label">Coupon Code :-
                </label>
                <div class="col-md-6">
                  <input type="text" name="coupon_code" id="coupon_code" value="<?php if(isset($coupon)){ echo $coupon[0]->coupon_code;} ?>" class="form-control"  required>
                </div>
              </div>
              <hr/>
              <div class="form-group">
                <label class="col-md-3 control-label">Discount in % :-
                  <p class="control-label-help">(You no need to add <strong>%</strong> in field)</p>
                </label>
                <div class="col-md-6">
                  <input type="text" name="coupon_per" id="coupon_per" maxlength="3" min="0" max="100" value="<?php if(isset($coupon)){ echo $coupon[0]->coupon_per;} ?>" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                </div>
              </div>
              <div class="or_link_item">
              <h2>OR</h2>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Discount in Amount :-
                </label>
                <div class="col-md-6">
                  <input type="text" name="coupon_amt" id="coupon_amt" min="0" max="100" value="<?php if(isset($coupon)){ echo $coupon[0]->coupon_amt;} ?>" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" >
                </div>
              </div>
              <hr/>
              <div class="form-group max_discount">
                <label class="col-md-3 control-label">Maximum Discount Status:-
                </label>
                <div class="col-md-6">
                  <select class="select2" name="max_amt_status" required="">
                    <option value="true" <?php if(isset($coupon) && $coupon[0]->max_amt_status=='true'){ echo 'selected';} ?>>True</option>
                    <option value="false" <?php if(isset($coupon) && $coupon[0]->max_amt_status=='false'){ echo 'selected';} ?>>False</option>
                  </select>
                </div>
              </div>
              <div class="form-group max_discount">
                <label class="col-md-3 control-label">Maximum Discount (Amount) :-
                </label>
                <div class="col-md-6">
                  <input type="text" name="coupon_max_amt" id="coupon_max_amt" min="0" max="100" value="<?php if(isset($coupon)){ echo $coupon[0]->coupon_max_amt;} ?>" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" >
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Minimum Amount in Cart Status:-
                </label>
                <div class="col-md-6">
                  <select class="select2" name="cart_status" required="">
                    <option value="true" <?php if(isset($coupon) && $coupon[0]->cart_status=='true'){ echo 'selected';} ?>>True</option>
                    <option value="false" <?php if(isset($coupon) && $coupon[0]->cart_status=='false'){ echo 'selected';} ?>>False</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Minimum Amount in Cart:-
                </label>
                <div class="col-md-6">
                  <input type="text" name="coupon_cart_min" id="coupon_cart_min" min="0" value="<?php if(isset($coupon)){ echo $coupon[0]->coupon_cart_min;} ?>" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" >
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Maximum Coupon Use Per User:-
                </label>
                <div class="col-md-6">
                  <input type="text" name="coupon_limit_use" id="coupon_limit_use" min="0" value="<?php if(isset($coupon)){ echo $coupon[0]->coupon_limit_use;} ?>" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" >
                </div>
              </div>
              <hr/>
              <div class="form-group">
                  <label class="col-md-3 control-label">Select Image :-
                    <p class="control-label-help">(Recommended resolution: 300X130,400X173, or Width greater than height)</p>
                  </label>
                  <div class="col-md-6">
                    <div class="fileupload_block">
                      <input type="file" name="file_name" value="fileupload" id="fileupload">
                      
                      <div class="fileupload_img"><img type="image" src="<?php 
                        if(!isset($coupon)){ echo base_url('assets/images/no-image-1.jpg'); }else{  echo IMG_PATH.$coupon[0]->coupon_image; } 
                      ?>" alt="coupon image" style="width: 150px;height: 90px" /></div>
                         
                    </div>
                  </div>
              </div>

              <br/>
              <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                  <button type="submit" name="btn_submit" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  CKEDITOR.replace( 'coupon_desc', {
      removePlugins: 'link,about', 
      removeButtons:'Subscript,Superscript,Image',
  } );
</script> 

<script type="text/javascript">

  $(".select2").each(function(index){
    var _ele=$(this).val();
    if(_ele=='true'){
      $(this).parents(".form-group").next(".form-group").fadeIn(100);
    }
    else{
      $(this).parents(".form-group").next(".form-group").fadeOut(100);
    }
  });

  $(".select2").on("change",function(e){
    var _ele=$(this).val();
    if(_ele=='true'){
      $(this).parents(".form-group").next(".form-group").fadeIn(100);
    }
    else{
      $(this).parents(".form-group").next(".form-group").fadeOut(100);
    }
  });

  //perform operations

  $("#coupon_per").keyup(function(e){
    var _per=$(this).val();
    if(_per!=''){
      $("#coupon_amt").val('');
      $(".max_discount").show();
    }
  });

  $("#coupon_amt").keyup(function(e){
    var _amt=$(this).val();
    if(_amt!=''){
      $("#coupon_per").val('');
      $(".max_discount").hide();
    }
  });

  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $("input[name='file_name']").next(".fileupload_img").find("img").attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("input[name='file_name']").change(function() { 
    readURL(this);
  });

</script> 
