<div class="page-content">
	<div class="row" style="padding-left:30px;padding-right: 30px">
  <div class="col-xs-12">
    <div class="card mrg_bottom">
      <div class="page_title_block">
        <div class="col-md-5 col-xs-12">
          <div class="caption"><?php echo $this->lang->line('BE_LBL_XXX'); ?></div>
        </div>
        <div class="col-md-6 col-md-offset-1 col-xs-12">
          <div class="col-sm-12">
            <div class="search_list">
              <div class="search_block">
				  <?php echo form_open("", array('name' => 'frm', 'id'=> 'frm', 'class' => 'form-horizontal'));?>
                <input class="form-control input-sm" placeholder="<?=$this->lang->line('search_lbl')?>" aria-controls="DataTables_Table_0" type="search" name="search_value" required value="<?php if(isset($_POST['search_value'])){ echo $_POST['search_value']; }?>">
                      <button type="submit" name="data_search" class="btn-search"><i class="fa fa-search"></i></button>
                <?php echo form_close(); ?>
              </div>
              <div class="add_btn_primary"> <a href="<?php echo site_url("admin/products/add");?>"><?=$this->lang->line('add_new_lbl')?></a> </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="col-md-3 col-xs-12 text-right" style="float: right;">
            <form method="post" action="">
                <div class="checkbox" style="width: 100px;margin-top: 5px;margin-left: 10px;float: left;right: 110px;position: absolute;">
                  <input type="checkbox" id="checkall">
                  <label for="checkall">
                      Select All
                  </label>
                </div>
                <div class="dropdown" style="float:right">
                  <button class="btn btn-primary dropdown-toggle btn_cust" type="button" data-toggle="dropdown">Action
                  <span class="caret"></span></button>
                  <ul class="dropdown-menu" style="right:0;left:auto;">
                    <li><a href="" class="actions" data-action="enable">Enable</a></li>
                    <li><a href="" class="actions" data-action="disable">Disable</a></li>
                    <li><a href="" class="actions" data-action="delete">Delete !</a></li>
                    <li><a href="" class="actions" data-action="set_today_deal">Set to todays Deal</a></li>
                    <li><a href="" class="actions" data-action="remove_today_deal">Remove to todays Deal</a></li>
                  </ul>
                </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-12 mrg-top">
        <?php 
          if($this->session->flashdata('response_msg')) {
            $message = $this->session->flashdata('response_msg');
            ?>
              <div class="<?=$message['class']?> alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <?=$message['message']?>
              </div>
            <?php
          }
        ?>
        <?php 
          if(!empty($products)){ 
        ?>
        <div class="row">
          <?php 
            $i=0;
            define('IMG_PATH', base_url().'assets/images/products/');

            $CI=&get_instance();

            // print_r($products);

            foreach ($products as $key => $row) 
            {

          ?>
          <div class="col-lg-4 col-sm-6 col-xs-12 item_holder">
            <div class="block_wallpaper add_wall_category" style="box-shadow:0px 3px 8px rgba(0, 0, 0, 0.3)">  
              <div class="wall_category_block">
                <h2 style="font-size: 16px">
                  <?php 
                    if(strlen($row->category_name) > 23){
                      echo substr(stripslashes($row->category_name), 0, 23).'...';  
                    }else{
                      echo $row->category_name;
                    }
                  ?>
                </h2>
                <?php 
                  $curr_date=date('d-m-Y');

                  if($row->today_deal_date!='' && $curr_date==date('d-m-Y',$row->today_deal_date)){
                    ?>
                    <a href="" data-id="<?php echo $row->id;?>" class="btn_today" data-action="deactive" data-toggle="tooltip" data-tooltip="Today's deal !" style="width: 30px;height: 30px;z-index: 1"><div style="color:green;"><i class="fa fa-check-circle"></i></div></a>
                    <?php
                  }
                  else{

                    $CI->deactive_today($row->id,true);

                    ?>
                    <a href="" data-id="<?php echo $row->id;?>" class="btn_today" data-id="active" data-toggle="tooltip" data-tooltip="Today's deal !" style="width: 30px;height: 30px;z-index: 1"><div><i class="fa fa-circle"></i></div></a>
                    <?php
                  }

                ?>

                <a href="<?php echo site_url("admin/products/duplicate-product/".$row->product_slug);?>" data-toggle="tooltip" data-tooltip="Make Duplicate" style="width: 30px;height: 30px;z-index: 1;margin-right: 5px"><div><i class="fa fa-clone"></i></div></a>

                <div class="checkbox" style="float: right;z-index: 1">
                  <input type="checkbox" name="post_ids[]" id="checkbox<?php echo $i;?>" value="<?php echo $row->id; ?>" class="post_ids">
                  <label for="checkbox<?php echo $i;?>">
                  </label>
                </div>
                 
              </div>        
              <div class="wall_image_title">
                <h2>
                  <a href="<?php echo site_url("admin/products/edit/".$row->id);?>" style="text-shadow: 1px 1px 1px #000;font-size: 16px" title="<?=$row->product_title?>">
                  <?php 
                    if(strlen($row->product_title) > 33){
                      echo substr(stripslashes($row->product_title), 0, 33).'...';  
                    }else{
                      echo $row->product_title;
                    }
                  ?>
                  </a>
                </h2>
                <p style="margin-bottom: 0px;font-size: 14px">
                  <?php echo $CI->get_sub_category_info($row->sub_category_id, 'sub_category_name')?$CI->get_sub_category_info($row->sub_category_id, 'sub_category_name'):'-';?>
                </p>
                <p style="margin-bottom: 0px;font-size: 14px">
                  <?php echo $CI->get_brand_info($row->brand_id, 'brand_name')?$CI->get_brand_info($row->brand_id, 'brand_name'):'-';?>
                </p>
                <ul>                
                  <li><a href="javascript:void(0)" data-toggle="tooltip" data-tooltip="<?=$row->total_views?> Views"><i class="fa fa-eye"></i></a></li>
                  <li><a href="<?php echo site_url("admin/products/edit/".$row->id);?>" data-toggle="tooltip" data-tooltip="<?=$this->lang->line('edit_lbl')?>"><i class="fa fa-edit"></i></a></li>               
                  <li><a href="" data-toggle="tooltip" class="btn_delete_a" data-id="<?=$row->id?>" data-tooltip="<?=$this->lang->line('delete_lbl')?>"><i class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></li>
                  <li>
                    <div class="row toggle_btn">
                      <input type="checkbox" id="enable_disable_check_<?=$i?>" data-id="<?=$row->id?>" class="cbx hidden enable_disable" <?php if($row->status==1){ echo 'checked';} ?>>
                      <label for="enable_disable_check_<?=$i?>" class="lbl"></label>
                    </div>
                  </li>
                  <?php 
                    
                    if($row->offer_id!=0){

                  ?>
                  <li>
                    <div class="container-fluid">
                      <span class="label label-danger"><?=$CI->get_single_info(array('id' => $row->offer_id), 'offer_percentage', 'tbl_offers');?>% OFF</span>
                    </div>
                  </li>
                  <?php } ?>
                </ul>
              </div>
              <span>
                <?php 
                  if(file_exists(IMG_PATH.$row->featured_image) || $row->featured_image==''){
                    ?>
                    <img src="https://via.placeholder.com/300x300?text=No image" style="height: 300px !important">
                    <?php
                  }else{
                    ?>
                    <img src="<?=IMG_PATH.$row->featured_image?>" style="height: 300px !important"/>
                    <?php
                  }
                ?>
              </span>
            </div>
          </div>  
        <?php 
            $i++;
          }
        ?>
        </div>
        <?php }else{ ?>
          <div class="col-lg-12 col-sm-12 col-xs-12" style="">
            <h3 class="text-muted" style="font-weight: 400">Sorry! no records found...</h3>
          </div>
        <?php } ?>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-12 col-xs-12">
          <div class="pagination_item_block">
            <nav>
              <?php 
                  if(!empty($links)){
                    echo $links;  
                  } 
              ?>
            </nav>
          </div>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">

  // for multiple action

  $(".actions").click(function(e){
    e.preventDefault();

    var _table='tbl_product';

    var href='<?=base_url()?>admin/product/perform_multipe';

    var _ids = $.map($('.post_ids:checked'), function(c){return c.value; });
    var _action=$(this).data("action");

    if(_ids!='')
    {
      swal({
        title: "Do you really want to perform?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger btn_edit",
        cancelButtonClass: "btn-warning btn_edit",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: false,
        showLoaderOnConfirm: true
      },
      function(isConfirm) {
        if (isConfirm) {

          $.ajax({
              type:'post',
              url:href,
              dataType:'json',
              data:{ids:_ids,for_action:_action,table:_table},
              success:function(res){
                  console.log(res);
                  if(res.status=='1'){
                    swal({
                        title: "Successfully", 
                        text: "You have successfully done", 
                        type: "success"
                    },function() {
                        location.reload();
                    });
                  }
                }
            });

        }else{
          swal.close();
        }
      });
    }
    else{
        $('.notifyjs-corner').empty();
        $.notify(
          'No record selected !', 
          { position:"top center",className: 'danger' }
        );
    }

  });


  $("#checkall").click(function () {
    $('.post_ids').not(this).prop('checked', this.checked);
  });


  $(".btn_delete_a").click(function(e){
      e.preventDefault();
      var _id=$(this).data("id");

      e.preventDefault(); 
      var href='<?=base_url()?>admin/product/delete/'+_id;
      var btn = this;

      swal({
        title: "Are you sure ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger btn_edit",
        cancelButtonClass: "btn-warning btn_edit",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: false,
        showLoaderOnConfirm: true
      },
      function(isConfirm) {
        if (isConfirm) {

          $.ajax({
            type:'GET',
            url:href,
            success:function(res){
                if($.trim(res)=='success'){
                  swal({
                      title: "Deleted", 
                      text: "Your product has been deleted", 
                      type: "success"
                  },function() {
                      $(btn).closest('.item_holder').fadeOut("200");
                  });
                }

              }
          });
          
        }else{
          swal.close();
        }
      });
  });


  $(".enable_disable").on("click",function(e){

    var href;
    var btn = this;
    var _id=$(this).data("id");

    var _for=$(this).prop("checked");
    if(_for==false){
      href='<?=base_url()?>admin/product/deactive/'+_id;
    }else{
      href='<?=base_url()?>admin/product/active/'+_id;
    }

    $.ajax({
      type:'GET',
      url:href,
      success:function(res){
          $('.notifyjs-corner').empty();
          $.notify(
            res, 
            { position:"top center",className: 'success' }
          );
        }
    });

  });

  $(".btn_today").on("click",function(e){
    e.preventDefault();
    var href;
    var btn = this;
    var _id=$(this).data("id");

    var _for=$(this).data("action");
    if(_for=='deactive'){
      href='<?=base_url()?>admin/product/deactive_today/'+_id;
    }else{
      href='<?=base_url()?>admin/product/active_today/'+_id;
    }

    $.ajax({
      type:'GET',
      url:href,
      success:function(res){
          location.reload();
        }
    });

  });

</script>
