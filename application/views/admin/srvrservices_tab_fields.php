<?php echo form_open(base_url('admin/services/serverservice?tab=4&id=' . $id), 'class="form-horizontal well" id="frm3" name="frm3"') ?>
<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
<div class="form-group row">
	<div class="col-lg-12">
		<h4><i class="mr-2"></i><?php echo $this->lang->line('BE_LBL_377'); ?>
		</h4>
	</div>
</div>
<div class="form-group row">
	<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_819'); ?>:</label>
	<div class="col-lg-4">
		<select multiple="multiple" class="multi-select" id="my_multi_select2" name="customFlds[]">
			<?php FillSelectedList(2, $id, 'FieldId'); ?>
		</select>
	</div>
</div>
<div class="form-group row">
	<div class="col-lg-3">
	</div>
	<div class="col-lg-8">
	<input type="hidden" name="fieldsForm"/>
	<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
			class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
	</div>
</div>
<?php echo form_close(); ?>
