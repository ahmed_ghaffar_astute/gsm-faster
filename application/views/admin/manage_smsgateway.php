<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_692'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th><?php echo $this->lang->line('BE_LBL_693'); ?></th>
										<th><?php echo $this->lang->line('BE_PM_3'); ?></th>
										<th><?php echo $this->lang->line('BE_GNRL_3'); ?></th>
										<th></th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ($rsPMs) {
										foreach ($rsPMs as $row) {
											?>
											<tr>
												<td class="highlight">
													<div class="success"></div>
													<a href="<?php base_url('admin/smsgateway/') ?>smsgateway?id=<?php echo $row->SMSGatewayId; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><?php echo stripslashes($row->SMSGateway); ?></a>
												</td>
												<td><?php echo $row->Username; ?></td>
												<td><?php echo $row->DisableSMSGateway == '1' ? 'Yes' : 'No'; ?></td>
												<td style="text-align:center" valign="middle">
													<a href="<?php base_url('admin/smsgateway/') ?>smsgateway?id=<?php echo $row->SMSGatewayId; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId'); ?>"><i
															class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> </a>
												</td>
											</tr>
											<?php
										}
									} else {
										?>
										<tr>
											<td colspan='5'><?php echo $this->lang->line('BE_GNRL_9'); ?></td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

