<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i>Website Newsletters Subscriptions</h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th>Email</th>
										<th nowrap="nowrap">Subscription Type</th>
										<th></th>
									</tr>
									</thead>
									<tbody>
									<?
									if ($rsData) {
										foreach ($rsData as $row) {
											?>
											<tr>
												<td><? echo stripslashes($row->Email); ?></td>
												<td><? echo $row->EmailType == '0' ? 'Unlocking' : 'Ecommerce'; ?></td>
												<td style="text-align:center" valign="middle">
													<a href="<?php ?>nlsubscriptions?del=1&id=<? echo($row->Id); ?>&frmId=<? echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"
													   onclick="return confirm('<? echo $this->lang->line('BE_LBL_32'); ?>')"><i
															class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></td>
											</tr>
										<? } ?>
									<? } else { ?>
										<tr>
											<td colspan="3">
												<strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td>
										</tr>
									<? } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

