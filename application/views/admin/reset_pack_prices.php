<!-- BEGIN PAGE HEADER-->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title"><? echo  $this->lang->line('BE_LBL_389'); ?></h3>
		<!-- END PAGE TITLE & BREADCRUMB-->
	</div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<?
$USER_ID_BTNS = $userId;
if($USER_ID_BTNS > 0)
	include APPPATH . 'scripts/userbtns.php';
?>
<div class="row">
	<div class="col-md-12">
		<? if($message != '')
			echo '<div class="alert alert-success">'.$message.'</div>'; ?>
	</div>
</div>
<!-- END PAGE CONTENT-->
</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
