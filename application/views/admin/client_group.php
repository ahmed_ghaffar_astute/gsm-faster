<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo 'Price Group'; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<a href="JavaScript:void(0);" onClick="window.history.back();" class="btn btn-primary"><span
					class="fa fa-angle-double-left"></span> Back To List</a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php }
					echo form_open(base_url(''), 'class="form-horizontal" name="frmChangePass" id="frmChangePass"'); ?>
					<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
						<div class="form-group row">
							<label class="col-lg-4 control-label">Client Group:<span class="required_field">*</span> </label>
							<div class="col-lg-4">
								<input type="text" class="form-control" placeholder="Enter Client Group" maxlength="100"
									   name="txtBx" id="txtBx" value="<?php echo $plan; ?>"/>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4 control-label">Show IMEI Prices At Web:</label>
							<div class="col-lg-4">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="iPrices"
										   id="iPrices" <?php echo $iPrices == 1 ? 'checked' : ''; ?>
										   class="toggle"/><span><label for="iPrices"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4 control-label">Show File Prices At Web:</label>
							<div class="col-lg-4">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="fPrices"
										   id="fPrices" <?php echo $fPrices == 1 ? 'checked' : ''; ?>
										   class="toggle"/><span><label for="fPrices"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4 control-label">Show Server Prices At Web:</label>
							<div class="col-lg-4">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="sPrices"
										   id="sPrices" <?php echo $sPrices == 1 ? 'checked' : ''; ?>
										   class="toggle"/><span><label for="sPrices"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_GNRL_1'); ?>:</label>
							<div class="col-lg-4">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="disable"
										   id="disable" <?php echo $disable == 1 ? 'checked' : ''; ?>
										   class="toggle"/><span><label for="disable"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-4"></div><div class="col-lg-8">
								<button
									type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary" id="btnSave" name="btnSave">Submit
								</button>
								<div style="display:none;" id="statusLoader"><img
										src="<?php echo base_url('assets/images/loading.gif'); ?>" border="0"
										alt="Please wait..."/></div>
							</div>
						</div>

					<?php echo form_close(); ?>
					<!-- END FORM-->
				</div>
			</div>
		</div>
	</div>
</div>
<script language="javascript" src="<?php echo base_url('assets/js/languages/english.js'); ?>"></script>
<script language="javascript" src="<?php echo base_url('assets/js/clientgroup.js'); ?>"></script>
