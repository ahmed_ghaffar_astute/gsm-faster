<?
$ftr1 = $clrsFtr[0];
$ftr2 = $clrsFtr[1];
$ftr3 = $clrsFtr[2];
$ftr4 = $clrsFtr[3];
?>
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_753'); ?></h5>
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_763'); ?>:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $ftr1; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="ftr4" value="<? echo $ftr1; ?>"
								   name="txtFtr1">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('ftr4').jscolor.show()"
									style="background-color: <? echo $ftr1; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_764'); ?>:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $ftr2; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="ftr1" value="<? echo $ftr2; ?>"
								   name="txtFtr2">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('ftr1').jscolor.show()"
									style="background-color: <? echo $ftr2; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_765'); ?>:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $ftr3; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="ftr2" value="<? echo $ftr3; ?>"
								   name="txtFtr3">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('ftr2').jscolor.show()"
									style="background-color: <? echo $ftr3; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_766'); ?>:<span class="required_field">*</span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $ftr4; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="ftr3" value="<? echo $ftr4; ?>"
								   name="txtFtr4">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('ftr3').jscolor.show()"
									style="background-color: <? echo $ftr4; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
