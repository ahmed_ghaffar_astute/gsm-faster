<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_512'); ?></h5>
			</div>
			<form action="#" class="form-horizontal" id="frmSync" name="frmSync" method="post">
				<div class="card-body">
					<? if ($apiType == '6' || $apiType == '7') { ?>
						<div class="form-group row" style="display:none;">
							<label class="col-lg-7 control-label"><? echo $this->lang->line('BE_LBL_508'); ?>:</label>
							<div class="col-lg-4">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="chkIntAll" value="1" disabled checked id="chkIntAll_S"
										   class="form-input-styled"/>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4 control-label">Services:</label>
							<div class="col-lg-6 radio-list">
								<label class="radio">
									<input type="radio" name="rdSServices" id="rdSServices1" value="0" checked/>All
									Services </label>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4 control-label">Synchronize Type:</label>
							<div class="col-lg-8 radio-list">
								<label class="radio">
									<input type="radio" name="rdSSyncType" id="rdSSyncType1" value="0" checked/>Normal
									Synchronization to connect your services with API service</label>
								<label class="radio">
									<input type="radio" name="rdSSyncType" id="rdSSyncType2" value="1"/>DELETE Existing
									Groups & Services and ADD new services from <b><? echo $apiTitle; ?></b></label>
							</div>
							<div id="dvSPrices" style="display:none;">
								<br/><br/>
								<hr>
								<h3 style="padding-left:10px;">Server Services Synchronization Details</h3>
								<div class="form-group row">
									<label class="col-lg-4 control-label">CONNECT API services with My Services:</label>
									<div class="col-lg-4" style="padding-left:40px;">
										<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
											<input type="checkbox" name="chkAttachSSrv" checked id="chkAttachSSrv"
												   class="form-input-styled"/>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-4 control-label">Price Type:</label>
									<div class="col-lg-6 radio-list" style="padding-left:40px;">
										<label class="radio-inline">
											<input type="radio" name="rdSPrType" id="rdSPrType0" value="0"
												   checked/><? echo $this->lang->line('BE_LBL_825'); ?> </label>
										<label class="radio-inline">
											<input type="radio" name="rdSPrType" id="rdSPrType1"
												   value="1"/><? echo $this->lang->line('BE_LBL_826'); ?>
											<label class="radio-inline">
												<input type="radio" name="rdSPrType" id="rdSPrType2" value="2"/>Keep
												Same
											</label>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_PCK_14'); ?>
										:*</label>
									<div class="col-lg-4" style="padding-left:40px;">
										<input type="text" class="form-control" placeholder="Enter Price" maxlength="5"
											   name="txtSPrice" id="txtSPrice"/>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-4 control-label">Pricing:</label>
									<div class="col-lg-4 radio-list" style="padding-left:40px;">
										<label class="radio-inline">
											<input type="radio" name="rdSPricing" id="rdSPricing0" value="0" checked/>Fixed</label>
										<label class="radio-inline">
											<input type="radio" name="rdSPricing" id="rdSPricing1" value="1"/>Percentage</label>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-4 control-label">Update Prices Of Other Currencies:</label>
									<div class="col-lg-4" style="padding-left:40px;">
										<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
											<input type="checkbox" name="chkOtherCurrSPrices" checked
												   id="chkOtherCurrSPrices" class="form-input-styled"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					<? } else { ?>
						<div class="form-group row">
							<div class="col-lg-12" align="center">
								<br/>
								<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
										class="btn btn-primary" id="btnServerSync1" name="btnServerSync1">Synchronize
									Server Services
								</button>
								<img style="display:none;" id="syncSLdr1"
									 src="<?php echo base_url('assets/images/loading.gif'); ?>" border="0"
									 alt="Please wait..."/>
							</div>
						</div>
					<? } ?>
				</div>
				<? if ($apiType == '6' || $apiType == '7') { ?>
					<div class="form-group row">
						<div class="col-lg-12" align="center">
							<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary" id="btnServerSync" name="btnServerSync">Synchronize Server
								Services
							</button>
							<img style="display:none;" id="syncSLdr"
								 src="<?php echo base_url('assets/images/loading.gif'); ?>" border="0"
								 alt="Please wait..."/>
						</div>
					</div>
				<? } ?>
			</form>
		</div>
	</div>
</div>
