<div class="page-header border-bottom-0 pt-4"></div>
<div class="content pt-0">
	<div class="form-group row">
		<div class="col-lg-12">
			<?php $USER_ID_BTNS = $uId;
			if($USER_ID_BTNS){
				include APPPATH.'scripts/userbtns.php';
			} else{ ?>
				<div class="btn-toolbar">
					<div style="padding: 0;" class="btn">
						<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><?php echo $this->lang->line('BE_LBL_285'); ?></button>
						<ul class="dropdown-menu" role="menu" style="text-align:left;">
							<li class="dropdown-item"><a class="Menu-Links" href="<?php echo base_url('admin/services/codesslbf?codeStatusId=1&searchType=1&uId='.$uId);?>"><?php echo $this->lang->line('BE_LBL_286'); ?></a></li>
							<li class="dropdown-item"><a class="Menu-Links" href="<?php echo base_url('admin/services/newfileorders?frmId=103&fTypeId=17');?>"><?php echo $this->lang->line('BE_LBL_287'); ?></a></li>
						</ul>
					</div>
					<div style="padding: 0;" class="btn">
						<button type="button"<?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><?php echo $this->lang->line('BE_LBL_288'); ?></button>
						<ul class="dropdown-menu" role="menu" style="text-align:left;">
							<li class="dropdown-item"><a class="Menu-Links" href="<?php echo base_url('admin/services/codesslbf?codeStatusId=4&searchType=3&uId='.$uId);?>"><?php echo $this->lang->line('BE_LBL_288'); ?></a></li>
						</ul>
					</div>
					<div style="padding: 0;" class="btn">
						<button type="button"<?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><?php echo $this->lang->line('BE_LBL_291'); ?></button>
						<ul class="dropdown-menu" role="menu" style="text-align:left;">
							<li class="dropdown-item"><a class="Menu-Links" href="<?php echo base_url('admin/services/codesslbf?uId='.$uId);?>"><?php echo $this->lang->line('BE_LBL_291'); ?></a></li>
						</ul>
					</div>
					<div style="padding: 0;" class="btn">
						<button type="button"<?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn default btn-primary dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><?php echo $this->lang->line('BE_LBL_292'); ?></button>
						<ul class="dropdown-menu" role="menu" style="text-align:left;">
							<li class="dropdown-item"><a class="Menu-Links" href="<?php echo base_url('admin/services/verifyfileorders?uId='.$uId);?>"><?php echo $this->lang->line('BE_LBL_292'); ?></a></li>
						</ul>
					</div>
				</div>
			<?php }  ?>
		</div>
	</div>
    <div class="card">
        <div class="form-group row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card-body">
                    <!-- Our Working Area Start -->
                    <?php if (isset($message) && $message != '') { ?>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <div class="alert alert-success"><?php echo $message; ?></div>
                            </div>
                        </div>
                    <?php } ?>
                   
                    <?php if($this->input->post('hdsbmt') && $cldFrm == '4')
                    { 
                        $strIMEIs = showAcceptedFileOrders($ORDER_IDS);
                    ?>
                        <div class="page-header border-bottom-0">
                            <div class="page-header-content header-elements-md-inline">
                                <div style="padding-top: 10px !important;" class="page-title d-flex">
                                    <h4>
                                        <i class="icon-arrow-left52 mr-2"></i>
                                        <?php echo $this->lang->line('BE_LBL_712'); ?>
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <?php
                            if($LINKED_IMEIS != '')
                                echo '<div class="alert alert-danger">The following IMEIs already exist and marked as linked '.$LINKED_IMEIS.'</div>';
                        ?>
                        <div class="from-group row">
                            <div class="col-lg-12">
                                <?php echo $strIMEIs;?>
                            </div>
                        </div>                 
                        <div class="from-group row">
                            <div class="col-lg-2">
                                <div class="btn-group">
                                    <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" onclick="downloadOrders('<?php echo $ORDER_IDS;?>');"><i class="fa fa-download"></i> Download Orders</button>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="txtEmlAddrss" name="txtEmlAddrss" placeholder="Email Address">
                                    <input type="hidden" name="acceptedIds" id="acceptedIds" value="<?php echo $ORDER_IDS; ?>" />
                                    <span class="input-group-btn">
                                        <button class="btn dark" type="button" id="btnSendEmail" name="btnSendEmail"><i class="fa fa-envelope"></i> Send Email</button>
                                        <img style="display:none;" id="imgSendEmail" src="<?php echo base_url('assets/img/loading.gif');?>" border="0" alt="Please wait..." />
                                    </span>
                                </div>
                            </div>
                        </div>   
                    <?php } ?>
                    <?php if($searchType != '1') { ?>
                            <?php echo form_open(base_url('admin/services/codesslbf') , 'name="frm" , id="frm"');?>
                                <input type="hidden" name="records" value="<?php echo $limit; ?>" />
                                <input type="hidden" name="searchType" id="searchType" value="<?php echo $searchType; ?>" />
                                <input type="hidden" name="uId" value="<?php echo $uId;?>" />
                                <input type="hidden" name="planId" value="<?php echo $planId;?>" />
                                <input type="hidden" name="start" id="start" value="0" />
                                <input type="hidden" name="applyAPI" value="<?php echo $applyAPI; ?>" />                            
                                <input type="hidden" name="packId" value="<?php echo $packId;?>" />
                                <?php include APPPATH.'scripts/admincodessearchsection.php'; ?>
                            <?php echo form_close();?>
                    <?php } ?>
                    <?php echo form_open(base_url('admin/services/codesslbf') , 'onsubmit="return validate(-1)"') ?>
                        <?php if($applyAPI == '1') { 
                        
                            $row = get_api_srv_history($packId , $sc);
                            if(isset($row->APIId) && $row->APIId != '')
                            {
                                $lastAPIId = $row->APIId;
                                $lastAPIServiceId = $row->APIServiceId;
                            }
                        ?>
                            <div class="page-header border-bottom-0">
                                <div class="page-header-content header-elements-md-inline">
                                    <div style="padding-top: 10px !important;" class="page-title d-flex">
                                        <h4>
                                            <i class="icon-arrow-left52 mr-2"></i>
                                            Apply API
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <div class="from-group row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label">Choose API</label>
                                            <?php $api_data=get_api_data() ;?>
                                            <select name="apiId" id="apiId" class="form-control select2me" data-placeholder="<?php echo $this->lang->line('BE_LBL_278'); ?>">
                                                <option value="0"><?php echo $this->lang->line('BE_LBL_278'); ?></option>
                                                <?php FillCombo($lastAPIId , $api_data); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <label class="control-label">Choose Supplier Service</label>
                                        <select name="supplierPackId" id="supplierPackId" class="form-control select2me" data-placeholder="<?php echo $this->lang->line('BE_LBL_274'); ?>">
                                            <option value="0"><?php echo $this->lang->line('BE_LBL_274'); ?></option>
                                            <?php 
                                                if($lastAPIId > 0)
                                                {
                                                    $rsSrvcs = fetch_supplier_services($lastAPIId ,$sc);
                                                    foreach($rsSrvcs as $row)
                                                    {
                                                        $selected = '';
                                                        if($row->Id == $lastAPIServiceId)
                                                            $selected = 'selected';
                                                        $optionVal = stripslashes($row->ServiceName);
                                                        if($row->ServicePrice != '')
                                                            $optionVal .= ' - '.stripslashes($row->ServicePrice).' Credits';
                                                        if($row->ServiceTime != '')
                                                            $optionVal .= ' - '.stripslashes($row->ServiceTime);
                                                        echo "<option value='".$row->Id."' $selected>".$optionVal."</option>";
                                                    }
                                                }
                                            ?>
                                        </select>
                                        <img style="display:none;" id="imgSrvcLoader" src="<?php echo base_url('assets/img/loading.gif');?>" border="0" alt="Please wait..." />
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                       
                        <div class="page-header border-bottom-0">
                            <div class="page-header-content header-elements-md-inline">
                                <div style="padding-top: 10px !important;" class="page-title d-flex">
                                    <h4>
                                        <i class="icon-arrow-left52 mr-2"></i>
                                        <?php echo $this->lang->line('BE_LBL_1'); ?>
                                    </h4>
                                </div>
                            </div>
                        </div>
                        
                        <div class="table-responsive">
                            <input type="hidden" name="hdsbmt" />
                            <?php if($searchType == '3') { ?>
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Enter code data to enter against all selected jobs</strong></label>
                                            <input type="text" class="form-control" placeholder="Enter code data to enter against all jobs" name="txtBulkCodeVal" />
                                        </div>
                                    </div>
                                    <div class="col-lg-1" style="padding-top:26px;">
                                        <input type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="<?php echo $this->lang->line('BE_LBL_194'); ?>" class="btn btn-primary" onClick="setValue('6');" />
                                    </div>
                                </div>
                            <?php } ?>
                            <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                                <tr>
                                    <th nowrap="nowrap" width="3%">Sr. #</th>
                                    <?php if($searchType == '3') { ?>
                                        <th>
                                            &nbsp;<?php echo $this->lang->line('BE_LBL_194'); ?>&nbsp;<input type="checkbox" class="chkSelect" id="chkSelect1" name="chkSelect1" onClick="selectAllChxBxs('chkSelect1', 'chkReply', <?php echo $count; ?>);" value="true">
                                        </th>
                                    <?php } ?>
                                    <th><?php echo $this->lang->line('BE_LBL_589'); ?></th>
                                    <th><?php echo $this->lang->line('BE_PCK_HD_4'); ?></th>
                                    <th><?php echo $this->lang->line('BE_INDEX_USRNAME'); ?></th>
                                    <th><?php echo $this->lang->line('BE_CODE_1'); ?></th>
                                    <th width="20%">Date</th>
                                    <?php
                                        if($searchType == '3')	
                                            echo '<th>'.$this->lang->line('BE_CODE_6').'</th>';
                                        if($searchType != '0')
                                        {
                                            echo '<th style="text-align:center">';
                                            if($searchType == '3') 
                                                echo $this->lang->line('BE_LBL_308').'<br />Download';
                                    ?>
                                        <input type="checkbox" class="chkSelect" id="chkSelect" name="chkSelect" onClick="selectAllChxBxs('chkSelect', 'chkCodes', <?php echo $count; ?>);" value="true">

                                    </th>
                                    <?php } ?>
                                    <th><?php echo $this->lang->line('BE_CODE_14'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if($count != 0)
                                    {
                                        $i = 0;
                                        foreach($rsCodes as $row)
                                        {
                                            $strAPI = '';
                                            $apiError = '';
                                            $strAPIError = 'Order Did not go through API - ';
                                            $strAPIError .= $row->MessageFromServer == '' ? 'No Error reported' : stripslashes($row->MessageFromServer);
                                            if($row->API != '-')
                                                $strAPI = '<br />Replied By: <font style="color:#ABABAB;">'.stripslashes($row->API).'</font>';
                                            if($row->OrderAPIId > 0 && $row->CodeStatusId =='1' && $row->CodeSentToOtherServer == '0')
                                                $apiError = '<img src="'.base_url().'/images/red-flags-icon-png-7.png" width="20" height="20" title="'.$strAPIError.'" />';
                                ?>
                                        <tr>
                                            <td><?php echo $i+1; ?>.</td>
                                            <?php if($searchType == '3') { ?>
                                                <td align="center">
                                                    <input type="checkbox" class="chkSelect" id="chkReply<?php echo $i; ?>" name="chkReply[]" value="<?php echo $row->CodeId.'|'.$row->UserId.'|'.$row->Credits.'|'.$row->IMEINo.'|'.$i.'|'.$row->PackageId.'|'.stripslashes($row->PackageTitle).'|'.$row->OrderType; ?>">
                                                </td>
                                            <?php } ?>
                                            <td><a href="<?php echo base_url('admin/services/codeslbf?id='.$row->CodeId);?>">#<?php echo $row->CodeId;?></a> <?php echo $apiError;?></td>
                                            <td style="width:200px;"><?php echo stripslashes($row->PackageTitle);?><br /><b><?php echo roundMe($row->Credits).'</b> '.$row->CurrencyAbb;?>
                                            <br />
                                            <?php if($row->OrderCostPrice > 0) { ?>
                                                <span id="spnPrft_<?php echo $row->CodeId;?>" class="badge badge-<?php echo $row->CodeStatusId == '3' ? 'danger' : 'info'; ?>">
                                                    <?php echo roundMe($row->Profit);?>
                                                </span>
                                                <br />
                                            <?php } echo stripslashes($row->TimeTaken);?></td>
                                            <td><a href="<?php echo base_url('admin/services/overview?id='.$row->UserId);?>"><?php echo $row->UserName;?></a><br /><?php echo $row->IP;?></td>
                                            <td><?php echo stripslashes($row->IMEINo);?>
                                                <br />
                                                <font <?php echo getOrderBGColor($row->CodeStatusId); ?>>&nbsp;&nbsp;&nbsp;
                                                <?php echo stripslashes($row->CodeStatus); ?>&nbsp;&nbsp;&nbsp;</font>
                                                <?php if($row->Profit != 0) { ?>
                                                    &nbsp;<a title="Remove profit for this order" class="btn btn-xs" style="background-color:RED; color:#FFFFFF" href="JavaScript:void(0);" onclick="removeProfit('<?php echo $row->CodeId?>', '1');"><i class="fa fa-minus"></i></a>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                Ordered:<br /><font style="color:#ABABAB;"><?php echo $row->RequestedAt; ?></font><br />
                                                Replied:<br /><font style="color:#ABABAB;"><?php echo $row->ReplyDtTm; ?></font><?php echo $strAPI; ?></td>
                                                <?php if($searchType == '3') { ?>
                                                    <td>
                                                        <?php echo stripslashes($row->Code); ?>
                                                    </td>
                                                <?php }
                                                if($searchType != '0') { ?>
                                                    <td align="center">
                                                        <input type="checkbox" class="chkSelect" id="chkCodes<?php echo $i; ?>" name="chkCodes[]" value="<?php echo $row->CodeId.'|'.$row->UserId.'|'.$row->Credits.'|'.$row->IMEINo.'|'.$i.'|'.$row->PackageId.'|'.stripslashes($row->PackageTitle).'|'.$row->OrderType; ?>">
                                                        <?php
                                                        if($row->LinkedOrderId != 0) 
                                                            echo '<span class="badge badge-warning">Linked To: '.$row->LinkedOrderId.'</span>';
                                                        ?>
                                                    </td>
                                                <?php } ?>
                                            <td>
                                                <a href="<?php echo base_url('admin/services/downloadlbffiles?id='.$row->CodeId);?>" class="btn btn-primary"><i class="fa fa-download"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                        </tr>
                                <?php
                                    $i++;
                                    }
                                }
                                    else
                                        echo '<tr><td colspan="9">'.$this->lang->line('BE_GNRL_9').'</td></tr>';
                                ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-12">
                                <?php if($searchType == '1' || $searchType == '2') { ?>
                                    <div class="btn-toolbar margin-bottom-10">
                                        <div class="btn">
                                            <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" onClick="setValue('4');"><i class="fa fa-check"></i> <?php echo $this->lang->line('BE_LBL_304'); ?></button>
                                            <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" onClick="document.getElementById('chkSelect').checked = true;selectAllChxBxs('chkSelect', 'chkCodes', <?php echo $count; ?>);setValue('4');"><i class="fa fa-check"></i> <?php echo $this->lang->line('BE_LBL_305'); ?></button>
                                            <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-danger" onClick="setValue('5');"><i class="fa fa-ban"></i> <?php echo $this->lang->line('BE_LBL_306'); ?></button>
                                            
                                            <?php if($applyAPI == '1') { ?>
                                                <input type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="Apply API" class="btn dark" onClick="setValue('7');" />
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="btn">
                                        <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" onClick="downloadFileOrders();"><i class="fa fa-download"></i> <?php echo $this->lang->line('BE_LBL_40'); ?></button>
                                        <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" onClick="setValue('2');"><i class="fa fa-check"></i> <?php echo $this->lang->line('BE_LBL_49'); ?></button>
                                    </div>
                                <?php } ?>
                                <input type="hidden" name="applyAPI" value="<?php echo $applyAPI; ?>" />                            
                                <input type="hidden" name="packId" value="<?php echo $packId;?>" />
                                <input type="hidden" value="0" name="cldFrm" id="cldFrm" />
                                <input type="hidden" name="uId" value="<?php echo $uId;?>" />
                                <input type="hidden" name="planId" value="<?php echo $planId;?>" />
                                <input type="hidden" name="txtBulkIMEIs" value="<?php echo @$this->input->post('txtBulkIMEIs'); ?>" />
                                <input type="hidden" name="start" value="<?php echo $start; ?>" />
                                <input type="hidden" name="hdsbmt" />
                                <input type="hidden" name="txtFromDt" id="txtFromDt" value="<?php echo $dtFrom; ?>" />
                                <input type="hidden" name="searchType" id="searchType" value="<?php echo $searchType; ?>" />
                                <input type="hidden" name="codeStatusId" id="codeStatusId" value="<?php echo $codeStatusId; ?>" />
                                <input type="hidden" name="sc" id="sc" value="<?php echo $sc; ?>" />
                            </div>
                        </div>
                    <?php echo form_close();?>
                    <?php echo form_open(base_url('admin/services/codesslbf') , 'name="frmPaging" , id="frmPaging"');?>
                        <input type="hidden" name="searchType" id="searchType" value="<?php echo $searchType; ?>" />
                        <input type="hidden" name="uId" value="<?php echo $uId;?>" />
                        <input type="hidden" name="planId" value="<?php echo $planId;?>" />
                        <input type="hidden" name="start" id="start" value="0" />
                        <input type="hidden" name="applyAPI" value="<?php echo $applyAPI; ?>" />                            
                        <input type="hidden" name="packId" value="<?php echo $packId;?>" />
                        <input type="hidden" name="packageId" value="<?php echo $packageId; ?>" />
                        <input type="hidden" name="codeStatusId" value="<?php echo $codeStatusId; ?>" />
                        <input type="hidden" name="txtFromDt" value="<?php echo $dtFrom; ?>" />
                        <input type="hidden" name="txtToDt" value="<?php echo $dtTo; ?>" />
                        <input type="hidden" name="txtOrderNo" value="<?php echo $orderNo; ?>" />
                        <input type="hidden" name="txtUName" value="<?php echo $uName; ?>" />
                        <input type="hidden" name="txtBulkIMEIs" value="<?php echo $this->input->post_get('txtBulkIMEIs'); ?>" />
                        <div class="from-group row">
                            <div class="col-lg-12">                              
                                <label class="control-label"><strong>Records Per Page</strong></label>
                                <table>
                                    <tr>
                                        <td>
                                            <input type="text" class="form-control" placeholder="Enter Custom Page Size" style="width:180px;" name="records" value="<?php echo $limit?>" />
                                        </td>
                                        <td>
                                            <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" onClick="setValue('5');"><i class="fa fa-check"></i> Set</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-lg-10">
                                <?php
                                    if($count != 0)
                                    {
                                        $row = fetch_codes_slbf_users($strWhere); 
                                    
                                        $totalRows = $row->TotalRecs;
                                        if($totalRows > $limit)
                                        {
                                            $whichFrm = $searchType != '1' ? 'frm' : 'frmPaging';
                                            doPages_DropDown($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next, $whichFrm);
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                    <?php echo form_close();?>
                    <?php echo form_open(base_url('admin/services/codesslbf?searchType=3&frmId=105&fTypeId=17') , 'name="frmIds" , id="frmIds"');?>
                            <input type="hidden" name="hdnOrdrIds" id="hdnOrdrIds" value="<?php echo $arcIds;?>" />
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<script>
function downloadFileOrders()
{
	var strIds = validate('0');
	if(strIds != false)
		window.location.href = '<?php echo base_url();?>admin/services/downloadfileorders?ids='+strIds;
}
function downloadOrders(ids)
{
	window.location.href = '<?php echo base_url();?>admin/services/downloadfileorders?ids='+ids;
}
function validate(cldFrm)
{
	var totalCodes = <?php echo $count; ?>;
	var searchType = <?php echo $searchType; ?>;
	var strIds = '0';
	var ctrlName = 'chkCodes';
	if(document.getElementById('cldFrm').value == '6')
		ctrlName = 'chkReply';
	for(var i=0; i<totalCodes; i++)
	{
		if(document.getElementById(ctrlName+i).checked)
		{
			arr = document.getElementById(ctrlName+i).value.split('|');
			strIds += ',' + arr[0];
		}
	}
	if(strIds == '0')
	{
		alert('<?php echo $this->lang->line('BE_LBL_53'); ?>');
		return false;
	}
	if(cldFrm == '-1' && document.getElementById('cldFrm').value == '3')
	{
		if(document.getElementById('blkCodeStatusId').value == '0')
		{
			alert('<?php echo $this->lang->line('BE_LBL_69'); ?>');
			document.getElementById('blkCodeStatusId').focus();
			return false;
		}
	}
	if(cldFrm == 0)
		return strIds;
	else
		return true;
}
function setValue(i)
{
	document.getElementById('cldFrm').value = i;
}
function backToList()
{
	document.getElementById('frmIds').submit();	
}

<script language="javascript" src="<?php echo base_url('assets/js/pendingorders.js');?>"></script>
