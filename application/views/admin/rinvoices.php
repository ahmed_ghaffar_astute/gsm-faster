<!-- Page header -->
<?php $NO_OF_RECORDS = array("10"=>10, "50"=>50, "100"=>100, "200"=>200, "300"=>300, "400"=>400, "500"=>500, "1000"=>1000); ?>
<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <? echo $this->lang->line('BE_LBL_582').' '.$this->lang->line('BE_PM_HD_1'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>

<div class="form-group row">
	<div class="col-lg-12">
		<?php /*$USER_ID_BTNS = $userId;
		if($USER_ID_BTNS){
			include APPPATH.'scripts/userbtns.php';
		}
		*/?>
	</div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php echo form_open(base_url('admin/sales/rinvoices?frmId=231&fTypeId=7'), 'class="horizontal-form"  id="frmSrch" name="frmSrch"'); ?>
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label"><? echo $this->lang->line('BE_PM_1'); ?></label>
								<input type="text" name="txtName" value="<? echo($name);?>" class="form-control">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label"><? echo $this->lang->line('BE_PM_2'); ?></label>
								<input type="text" name="txtEmail" value="<? echo($email);?>" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label"><? echo $this->lang->line('BE_PM_4'); ?></label>
								<input type="text" name="txtPaymentId" value="<? echo($paymentId);?>" class="form-control">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label"><? echo $this->lang->line('BE_LBL_60'); ?></label>
								<select name="records" class="form-control select2me" data-placeholder="Select...">
									<?php

									foreach ($NO_OF_RECORDS as $key => $value)
									{
										$selected = '';
										if($limit == $key)
											$selected = 'selected';
										echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
									}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<?php $payment_methods = fetch_payment_methods(); ?>
							<div class="form-group">
								<label class="control-label"><?php echo $this->lang->line('BE_PM_5'); ?></label>
								<select name="pMethodId" class="form-control select2me"
										data-placeholder="<?php echo $this->lang->line('BE_LBL_256'); ?>">
									<option value="0"><?php echo $this->lang->line('BE_LBL_256'); ?></option>
									<?php FillCombo($pMethodId, $payment_methods); ?>
								</select>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<?php $payment_status = fetch_payment_status_by_id($strWherePmnts); ?>
								<label class="control-label"><?php echo $this->lang->line('BE_PM_6'); ?></label>
								<select name="pStatusId" class="form-control select2me"
										data-placeholder="<?php echo $this->lang->line('BE_LBL_257'); ?>">
									<option value="0"><?php echo $this->lang->line('BE_LBL_257'); ?></option>
									<?php FillCombo($pStatusId, $payment_status); ?>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group">
								<label
									class="control-label"><?php echo $this->lang->line('BE_PM_7'); ?></label><br/>
								<input class="form-control form-control-inline input-largest date-picker"
									   type="text" name="txtFromDt" value="<?php echo($dtFrom); ?>"/>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label
									class="control-label"><?php echo $this->lang->line('BE_PM_8'); ?></label><br/>
								<input class="form-control form-control-inline input-largest date-picker"
									   type="text" name="txtToDt" value="<?php echo($dtTo); ?>"/>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary"><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
							<input type="hidden" name="cldFrm" id="cldFrm" value="0"/>
						</div>
					</div>

					<?php
					if($count != 0)
					{
						$row = fetch_user_retail_payments($strWhere);
						$totalRows = $row->TotalRecs;
						if($totalRows > $limit)
							doPages_DropDown($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next,'frmSrch');
					}


					?>
					<div class="form-group row">
						<div class="col-lg-12">
							<p align="right">
								<input type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									   value="<?php echo $this->lang->line('BE_LBL_102'); ?>"
									   onClick="window.location.href='<?php base_url('admin/clients/exportpayments'); ?>';"
									   name="btnSubmit" class="btn btn-warning"/>
								<?php if (array_key_exists("319", $ARR_ADMIN_FORMS)) { ?>
									<input type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
										   value="<?php echo $this->lang->line('BE_LBL_303'); ?>"
										   onClick="return validate(1);" class="btn btn-danger" name="btnSubmit"/>
								<?php } ?>
							</p>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">

							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th><? echo $this->lang->line('BE_LBL_101'); ?></th>
										<th><? echo $this->lang->line('BE_LBL_345'); ?></th>
										<th><? echo $this->lang->line('BE_PM_9'); ?></th>
										<th><? echo $this->lang->line('BE_LBL_657'); ?></th>
										<th><? echo $this->lang->line('BE_LBL_658'); ?></th>
										<th><? echo $this->lang->line('BE_LBL_100'); ?></th>
										<th><? echo $this->lang->line('BE_CODE_5'); ?></th>
										<th></th>
										<th style="text-align:center">
											<input type="checkbox" id="chkSelect" name="chkSelect" onClick="selectAllChxBxs('chkSelect', 'chkPayments', <? echo $count; ?>);" value="true">
										</th>
									</tr>
									</thead>
									<tbody>
									<?
									if($count != 0)
									{
										$i = 0;
									foreach ($rsPayments as $row) {
											$dtTm = explode(" ", $row->PaymentDtTm);
											if($row->PaymentStatusId == '1') //Unpaid
												$badgeColor = 'warning';
											else if($row->PaymentStatusId == '2') //Paid
												$badgeColor = 'success';
											else if($row->PaymentStatusId == '3') //Cancel
												$badgeColor = 'danger';
											else if($row->PaymentStatusId == '4') //Refunded
												$badgeColor = 'warning';
											else// if($row->PaymentStatusId == '5') //Review
												$badgeColor = 'info';
											?>
											<tr>
												<td class="highlight"><div class="success"></div><a href="<?php echo base_url('admin/Sales/') ?>rinvoice?id=<?php echo $row->RetailPaymentId;?>&frmId=<? echo $this->input->post_get('frmId');?>&fTypeId=<? echo $this->input->post_get('fTypeId')?>"><? echo($row->RetailPaymentId);?></a></td>
												<td><? echo($row->Email);?></td>
												<td><? echo stripslashes($row->Name);?></td>
												<td><? echo $row->Currency.' '.roundMe($row->OrderPrice); ?></td>
												<td><? echo $row->Currency.' '.roundMe($row->Amount);?></td>
												<td><? echo($row->PaymentMethod == '' ? '-' : $row->PaymentMethod);?></td>
												<td>
                                                    <span class="label label-<? echo $badgeColor;?>">
                                                        <? echo($row->PaymentStatus);?>
                                                    </span>
												</td>
												<td style="text-align:center" valign="middle">
													<a href="<?php echo base_url('admin/Sales/') ?>rinvoice?id=<?php echo $row->RetailPaymentId;?>&frmId=<? echo $this->input->post_get('frmId');?>&fTypeId=<? echo $this->input->post_get('fTypeId')?>" ><i class="fa fa-pencil"></i> </a>
													<a title="View Order" href="<?php echo base_url('admin/Sales/') ?><? echo $row->RetailOrderType == 0 ? 'retailorder' : 'srvrrtlorder';?>?id=<?php echo $row->RetailOrderId;?>" ><i class="fa fa-shopping-cart"></i> </a>
												</td>
												<td align="center">
													<input type="checkbox" id="chkPayments<? echo $i; ?>" name="chkPayments[]" value="<? echo $row->RetailPaymentId; ?>">
												</td>
											</tr>
											<tr>
												<td colspan="2"><b style="color:#906666;"><? echo $this->lang->line('BE_PM_12'); ?>:</b> <? echo($row->TransactionId);?></td>
												<td colspan="2"><b style="color:#906666;"><? echo $this->lang->line('BE_LBL_594'); ?>:</b> <? echo($row->PayerEmail);?></td>
												<td><b style="color:#906666;"><? echo $this->lang->line('BE_PM_13'); ?>:</b> <? echo convertDate($row->PaymentDtTm);?></td>
												<td colspan="4"><b style="color:#906666;"><?php echo $this->lang->line('BE_LBL_595'); ?>:</b> <? echo stripslashes($row->ShippingAddress);?></td>
											</tr>
											<?
											$i++;
										}
									}
									else{?>
										<tr><td colspan='9'><?php echo $this->lang->line('BE_GNRL_9')?></td></tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
    function validate()
    {
        var totalPayments = <? echo $count; ?>;
        var strIds = '0';
        for(var i=0; i<totalPayments; i++)
        {
            if(document.getElementById('chkPayments'+i).checked)
            {
                arr = document.getElementById('chkPayments'+i).value.split('|');
                strIds += ',' + arr[0];
            }
        }
        if(strIds == '0')
        {
            alert('<? echo $this->lang->line('BE_LBL_53'); ?>');
            return false;
        }
        if(confirm('Are your sure you want to delete the payment(s)?'))
        {
            document.getElementById('cldFrm').value = '1';
            return true;
        }
        else
            return false;
    }
</script>


