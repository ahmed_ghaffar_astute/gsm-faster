<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <?php if($message != '')
                        echo '<div class="alert alert-success">'.$message.'</div>'; 
                ?>
                <?php echo form_open(base_url('admin/services/apiorders') , 'name="frm" id="frm"');?>
                    <?php include APPPATH.'scripts/admincodessearchsection.php';?>
                <?php echo form_close(); ?>
                <?php 
                if($count != 0)
					{
						$row = fetch_users_codes_count_by_status($strWhere);
						$totalRows = $row->TotalRecs;
						if($totalRows > $limit)
							doPages($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next);
					}
                ?>
                <?php echo form_open(base_url('admin/services/apiorders') , 'id="myFrm" onSubmit="return validate();" ');?>
                    <p>
                        <div class="btn-group btn-group-sm btn-group-solid">
                            <input type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="<?php echo $this->lang->line('BE_LBL_749'); ?>" class="btn btn-primary" name="btnSubmit" />
                        </div>
                    </p>
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>
                                <h2>
                                    <?php echo $this->lang->line('BE_LBL_748'); ?>
                                </h2>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-responsive">
                                <input type="hidden" name="hdsbmt" id="hdsbmt" />
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                            <th style="text-align:center">
                                                <input type="checkbox" id="chkSelect1" name="chkSelect1" onClick="selectAllChxBxs('chkSelect1', 'chkOrders', <?php echo $count; ?>);" value="true">
                                            </th>
                                            <th nowrap="nowrap"><?php echo $this->lang->line('BE_LBL_589'); ?></th>
                                            <th><?php echo $this->lang->line('BE_CODE_2'); ?></th>
                                            <th><?php echo $this->lang->line('BE_INDEX_USRNAME'); ?></th>
                                            <th><?php echo $this->lang->line('BE_CODE_1'); ?></th>
                                            <th><?php echo $this->lang->line('BE_CODE_7'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if($count != 0)
                                    {
                                        $i = 0;
                                        foreach($rsCodes as $row)
                                        {
                                    ?>
                                            <tr>
                                                <td align="center">
                                                    <input type="checkbox" id="chkOrders<?php echo $i; ?>" name="chkOrders[]" value="<?php echo $row->CodeId; ?>">
                                                </td>
                                                <td class="highlight"><div class="success"></div>&nbsp;<a href="<?php echo base_url('admin/services/code?id='.$row->CodeId);?>"><?php echo $row->CodeId;?></a></td>
                                                <td><?php echo stripslashes($row->PackageTitle); ?></td>
                                                <td><?php echo $row->UserName; ?></td>
                                                <td><?php echo $row->IMEINo;?></td>
                                                <td><?php echo $row->RequestedAt; ?></td>
                                            </tr>
                                    <?php
                                            $i++;
                                        }
                                    }
                                    else
                                        echo '<tr><td colspan="9">'.$this->lang->line('BE_GNRL_9').'</td></tr>';
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                <?php echo form_close() ;?>
            </div>
        </div>
    </div>
</div>
