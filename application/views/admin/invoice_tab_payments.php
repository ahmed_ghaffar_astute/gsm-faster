<div class="form-group row">
	<div class="col-lg-12">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_227'); ?></h4>
        </div>
    </div>
</div>
<div class="form-group row">
	<div class="col-lg-12">
		<div class="form-body">
			<table class="table table-striped table-bordered table-advance table-hover">
				<thead>
				<tr class="bg-primary">
					<th><?php echo $this->lang->line('BE_PM_11'); ?></th>
					<th><?php echo $this->lang->line('BE_PM_5'); ?></th>
					<th><?php echo $this->lang->line('BE_PM_12'); ?></th>
					<th><?php echo $this->lang->line('BE_CR_3'); ?></th>
					<th><?php echo $this->lang->line('BE_GNRL'); ?></th>
				</tr>
				</thead>
				<tbody>
				<?php
				if($rsPayments)
				{
					foreach($rsPayments as $row )
					{
					?>
						<tr>
							<td class="highlight"><? echo $currency.' '.$row->InvAmount;?></a></td>
							<td><?php echo stripslashes($row->PaymentMethod);?></td>
							<td><?php echo stripslashes($row->InvTransactionId);?></td>
							<td><?php echo stripslashes($row->InvDtTm);?></td>
							<td><?php echo stripslashes($row->InvComments);?></td>
						</tr>
				<?php }?>
				<? }else{ ?>
					<tr><td colspan="5"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td></tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
                <thead>
					<tr class="bg-primary">
						<th><?php echo $this->lang->line('BE_PM_11'); ?></th>
						<th><?php echo $this->lang->line('BE_PM_5'); ?></th>
						<th><?php echo $this->lang->line('BE_PM_12'); ?></th>
						<th><?php echo $this->lang->line('BE_CR_3'); ?></th>
						<th><?php echo $this->lang->line('BE_GNRL'); ?></th>
					</tr>
                </thead>
                <tbody>
                <?php
                if($rsPayments)
                {
                    foreach($rsPayments as $row )
                    {
                    ?>
                        <tr>
                            <td class="highlight"><? echo $currency.' '.$row->InvAmount;?></td>
                            <td><?php echo stripslashes($row->PaymentMethod);?></td>
                            <td><?php echo stripslashes($row->InvTransactionId);?></td>
                            <td><?php echo stripslashes($row->InvDtTm);?></td>
                            <td><?php echo stripslashes($row->InvComments);?></td>
                        </tr>
                <?php }?>
                <? }else{ ?>
                    <tr><td colspan="5"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td></tr>
                <?php } ?>			    			
                </tbody>
            </table>
        </div>
    </div>
</div>


