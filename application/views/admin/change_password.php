<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title"><?php echo $this->lang->line('BE_PWD_HEADING'); ?></h3>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <?php if($message != '')
                        echo '<div class="alert '.$class.'">'.$message.'</div>'; ?>
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-bars"></i><?php echo $this->lang->line('BE_PWD_HEADING'); ?>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php echo form_open('' , 'class="form-horizontal" name="frmChangePass"  id="frmChangePass"'); ?>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_PWD_1'); ?>:*</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <input type="password" autocomplete="off" class="form-control" placeholder="Password" maxlength="20" name="txtOldPass" id="txtOldPass" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_PWD_2'); ?>:*</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <input type="password" autocomplete="off" class="form-control" placeholder="Password" maxlength="20" name="txtNewPass" id="txtNewPass">
                                            <span class="input-group-addon">
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_USR_3'); ?>:*</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <input type="password" autocomplete="off" class="form-control" placeholder="Password" maxlength="20" name="txtConfirmPass" id="txtConfirmPass">
                                            <span class="input-group-addon">
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group fluid">
                                <div class="col-md-3"></div><div class="col-lg-9">
                                    <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" id="btnChangePass" name="btnChangePass" ><?php echo $this->lang->line('BE_LBL_72'); ?></button>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script language="javascript" src="<?php echo base_url('assets/js/languages/english.js');?>"></script>
<script language="javascript" src="<?php echo base_url('assets/js/user.js');?>"></script>
