<div class="block-web">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-bars"></i><?php echo $this->lang->line('BE_LBL_533'); ?>
            </div>
        </div>
        <div class="portlet-body form">
            <form class="form-horizontal" name="frm" method="post" id="frm">
                <input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
                <input type="hidden" id="userId" name="userId" value="<?php echo($userId); ?>">
                <input type="hidden" id="codeCr" name="codeCr" value="<?php echo($credits); ?>">
                <input type="hidden" name="pckTitle" value="<?php echo $pckTitle; ?>" />
                <input type="hidden" id="SMS_Pack" name="SMS_Pack" value="<?php echo($SMS_Pack); ?>">
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_PCK_HD_6'); ?>:</label>
                        <div class="col-md-4">
                            <p class="form-control-static"><?php echo $this->lang->line('pckTitle'); ?></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_INDEX_USRNAME'); ?>:</label>
                        <div class="col-md-6">
                            <p class="form-control-static"><?php echo $userName; ?></p>
                        </div>
                    </div>
                    <?php
                    $i = 1;
                    foreach($rsFields as $rowFld )
                    {
                        $fldColName = $rowFld->FieldColName;
                        if(isset($ROW_SERVER_ORDER->$fldColName) && $ROW_SERVER_ORDER->$fldColName != '')
                        {
                    ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $rowFld->FieldLabel; ?>:<?php if($rowFld->Mandatory == '1') echo '*';?></label>
                                <div class="col-md-4 <?php if($rowFld->FieldType == 'Radio Button') echo 'radio-list'; ?>">
                                    <?php if($rowFld->FieldType == 'Text Box') { ?>
                                        <input type='text' value="<?php echo $ROW_SERVER_ORDER->$fldColName; ?>" placeholder="Enter <?php echo $rowFld->FieldLabel; ?>" name="fld<?php echo $i; ?>" maxlength="100" class="form-control" />
                                    <?php } else if($rowFld->FieldType == 'Text Area') { ?>
                                        <textarea name="fld<?php echo $i; ?>" class="form-control"></textarea>
                                    <?php } else if($rowFld->FieldType == 'Drop Down') { ?>
                                        <select name="fld<?php echo $i; ?>" class="form-control select2me" data-placeholder="<?php echo $rowFld->FieldLabel; ?>">
                                        <?php
                                        $rsValues = fetch_custom_field_values($rowFld->FieldId);
                                        
                                        foreach($rsValues as $rw )
                                        {
                                            $SELECTED_VAL = '';
                                            if($ROW_SERVER_ORDER->$fldColName == $rw->RegValue)
                                                $SELECTED_VAL = 'selected';
                                            echo '<option value="'.$rw->RegValue.'" '.$SELECTED_VAL.'>'.$rw->RegValue.'</option>';
                                        }
                                        ?>
                                        </select>
                                    <?php } else if($rowFld->FieldType == 'Radio Button') { ?>
                                        <?php
                                        $rsValues = fetch_custom_field_values($rowFld->FieldId);
                                        foreach($rsValues as $rw )
                                        {
                                            $chckd = '';
                                            if($rw->RegValue == $ROW_SERVER_ORDER->$fldColName)
                                                $chckd = 'checked';
                                            echo '<label class="radio-inline"><input type="radio" name="fld'.$i.'" '.$chckd.' value="'.$rw->RegValue.'">'.$rw->RegValue.'</label>';
                                        }
                                    }
                            echo '</div></div>';
                            echo '<input type="hidden" name="colNm'.$i.'" value="'.$rowFld->FieldColName.'" />';
                            echo '<input type="hidden" name="lbl'.$i.'" value="'.$rowFld->FieldLabel.'" />';
                            $i++;
                        }
                    } ?>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_LBL_163'); ?>:</label>
                        <div class="col-md-6">
                            <p class="form-control-static"><?php echo $orderDt; ?></p>
                        </div>
                    </div>
                    <?php if($replyDtTm != '') { ?>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_LBL_601'); ?>:</label>
                            <div class="col-md-6">
                                <p class="form-control-static"><?php echo $replyDtTm; ?></p>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_USR_10'); ?></label>
                        <div class="col-md-4">
                            <p class="form-control-static"><?php echo $credits; ?></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <?php $code_status=get_code_status(); ?>
                        <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_LR_1'); ?>:*</label>
                        <div class="col-md-4">
                            <select id="statusId" name="statusId" class="form-control select2me" data-placeholder="Choose...">
                                <?php FillCombo($statusId, $code_status); ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_LBL_147'); ?>:</label>
                        <div class="col-md-6">
                            <p class="form-control-static"><?php echo $sent2OtherSrvr == 1 ? 'Yes' : 'No'; ?></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">API:</label>
                        <div class="col-md-6">
                            <p class="form-control-static"><?php echo $apiName != '' ? $apiName : '-'; ?></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_CD_3'); ?>:</label>
                        <div class="col-md-6">
                            <p class="form-control-static"><?php echo $orderIdFrmServer != '' ? $orderIdFrmServer : '-'; ?></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_LBL_146'); ?>:</label>
                        <div class="col-md-6">
                            <p class="form-control-static"><?php echo $msg4mSrvr != '' ? $msg4mSrvr : '-'; ?></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Last Updated By:</label>
                        <div class="col-md-6">
                            <p class="form-control-static"><?php echo $updatedBy != '' ? $updatedBy : '-'; ?></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_CODE_6'); ?>:</label>
                        <div class="col-md-4">
                            <textarea class="form-control" rows="3" id="txtCode" name="txtCode"><?php echo replaceBRTag($code); ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_LBL_362'); ?>:</label>
                        <div class="col-md-4">
                            <textarea class="form-control" rows="3" id="txtNotes" name="txtNotes"><?php echo $notes; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_GNRL'); ?>:</label>
                        <div class="col-md-4">
                            <textarea class="form-control" rows="3" id="txtComments" name="txtComments"><?php echo $comments; ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3"></div><div class="col-lg-9">
                        <input type="hidden" name="totalCustomFields" value="<?php echo $i-1; ?>" />
                        <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
