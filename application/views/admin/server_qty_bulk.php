<?php echo form_open(base_url('admin/services/serverservice?id=' . $id), 'class="form-horizontal well"  id="frmBPrices" name="frmBPrices"'); ?>
<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4>
				<i class="icon-arrow-left52 mr-2"></i> <?php echo "Add Quantity Range";//$this->lang->line('BE_LBL_514'); ?>
			</h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> class="btn btn-primary" id="btnAdd"
					name="btnAdd">Add New Record
			</button>
			<div style="display:none;" id="statusLoader"><img src="<?php echo base_url('assets/images/loading.gif'); ?>"
															  border="0" alt="Please wait..."/></div>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">

							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover"
									   id="tblDisc">
									<thead>
									<tr class="bg-primary">
										<th nowrap="nowrap">Min Quantity</th>
										<th nowrap="nowrap">Max Quantity</th>
										<th>Price</th>
										<?php echo $strGroupHdrs; ?>
										<th></th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ($rsBulkPrices) {
										foreach ($rsBulkPrices as $row) {
											?>
											<tr>
												<td><?php echo $row->MinQty; ?></td>
												<td><input class="form-control" placeholder="Maximum Quantity"
														   maxlength="4" value="<?php echo $row->MaxQty; ?>" type="text"
														   name="txtMaxQty[]"/></td>
												<td><input type="text" placeholder="Price" class="form-control"
														   maxlength="10" name="txtPrice[]"
														   value="<?php echo($row->Price); ?>"/></td>
												<?
												$i = 1;
												foreach ($arrGroups as $value) {
													$nm = 'txtGPrice' . $i . '[]';
													$idNm = 'hdGrpId' . $i . '[]';
													$index = $row->MinQty . '-' . $row->MaxQty . '-' . $value[0];
													$val = '';
													if (isset($GROUP_PRICES[$index]) && $GROUP_PRICES[$index] != '')
														$val = $GROUP_PRICES[$index];
													?>
													<td>
														<input type="text" placeholder="Price" class="form-control"
															   maxlength="10" name="<?php echo $nm; ?>"
															   value="<?php echo $val; ?>"/>
														<input type="hidden" name="<?php echo $idNm; ?>"
															   value="<?php echo $value[0]; ?>"/>
													</td>
													<?
													$i++;
												}
												?>
												<td style="text-align:center"><a href="javascript:void(0);"
																				 class="remCF"><i
															class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></td>
											</tr>
										<?php }
									} else {
										$colSpan = 4 + $totalGroups;
										echo '<tr id="trNR"><td colspan="' . $colSpan . '"><strong>No Bulk Prices Found!</strong></td></tr>';
									}
									$newRow = '<td style="text-align:center;">-</td><td><input class="form-control" placeholder="Max Quantity" maxlength="4" type="text" name="txtMaxQty[]" /></td><td><input type="text" class="form-control" maxlength="10" name="txtPrice[]" placeholder="Price" value="' . $price . '" /></td>' . $strHGroupFlds . '<td style="text-align:center"><a href="javascript:void(0);" class="remCF"><i class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></td>';
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<input type="hidden" id="id" name="id" value="<?php echo $id; ?>"/>
							<input type="hidden" name="ttlGroups" value="<?php echo $totalGroups; ?>"/>
							<input type="hidden" id="newRow" name="newRow" value='<tr><?php echo $newRow; ?></tr>'/>
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary" name="btnBulkPrices">Submit
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
					<?php echo form_close(); ?>
