<?php if ($count > 0) { ?>
	<?php echo form_open(base_url("admin/services/serverservice?tab=2&id=") . $id, ' class="form-horizontal well" id="frm1" name="frm1"'); ?>
	<input type="hidden" id="id" name="id" value="<?php echo($id); ?>"/>
	<input type="hidden" name="hdPackPrice" value="<?php echo($price); ?>"/>
	<input type="hidden" name="hdPackTitle" value="<?php echo($packageTitle); ?>"/>
	<div class="form-group row">
		<div class="col-lg-3">
			<h4><i class="mr-2"></i><?php echo $this->lang->line('BE_LBL_284'); ?></h4>
		</div>
		<div class="col-lg-9" align="right">
			<a href="<?php echo base_url('admin/services/server_qty_bulk?id=' . $id); ?>"
			   class="fancybox fancybox.ajax btn btn-primary" title="Add Quantity Range">Add Quantity Range</a>
		</div>
	</div>
	<input type="hidden" name="totalCurrencies" value="<?php echo $count; ?>"/>
	<?php if ($id > 0) { ?>
		<div class="form-group row">
			<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_387'); ?>:</label>
			<div class="col-lg-4">
				<div class="switch-button switch-button-lg">
					<input type="checkbox" class="chkSelect" name="chkCnvrtPrices" id="chkCnvrtPrices"
						   onClick="if(this.checked) convertPrices();"/><span><label for="chkCnvrtPrices"></label></span>
				</div>
			</div>
		</div>
	<?php }
	$index = 0;
	$strCurrHdrs = '';
	$PACK_PRICES_OTHER = array();
	$CURR_CONV_RATES = array();
	foreach ($rsCurrencies as $row) {
		$strCurrHdrs .= "<th>" . $row->CurrencyAbb . "</th>";
		if ($row->DefaultCurrency != '1') {
			$currencyPrice = '';
			if ($id > 0) {
				if ($row->Price == '')// set as per conversion rate
				{
					$currencyPrice = number_format($price * $row->ConversionRate, 2, '.', '');
				} else {
					$currencyPrice = number_format($row->Price, 2, '.', '');
				}
				$PACK_PRICES_OTHER[$row->CurrencyId] = $currencyPrice;
				$CURR_CONV_RATES[$row->CurrencyId] = $row->ConversionRate;
			}
			?>
			<div class="form-group row">
				<label class="col-lg-3 control-label"><?php echo $row->CurrencyAbb; ?>:</label>
				<div class="col-lg-2">
					<input type="hidden" name="currencyId<?php echo $index; ?>"
						   value="<?php echo $row->CurrencyId; ?>"/>
					<input type="text" class="form-control" placeholder="Enter Price" maxlength="10"
						   name="txtCurrPrice<?php echo $index; ?>" id="txtCurrPrice<?php echo $index; ?>"
						   value="<?php echo $currencyPrice; ?>"/>
					<input type="hidden" name="converstionRate<?php echo $index; ?>"
						   id="converstionRate<?php echo $index; ?>"
						   value="<?php echo $row->ConversionRate; ?>"/>
				</div>
			</div>
			<?php
			$index++;
		} else {
			$DEFAULT_CURRENCY_ID = $row->CurrencyId;
		}
	} ?>
	<input type="hidden" id="totalCurrencies" name="totalCurrencies" value="<?php echo $index; ?>"/>

	<?php if ($strCurrHdrs != '') { ?>
<div class="form-group row">
	<div class="col-lg-12">
		<table cellspacing="1" cellpadding="1" width="100%" border="0">
			<tr>
				<td width="2%">
					<input type="checkbox" class="chkSelect" name="chkCnvrtPrice"
						   id="chkCnvrtPrice"/><span><label for="chkCnvrtPrice"></label></span>
				</td>
				<td width="98%" style="font-size:20px;">
					<b><?php echo $this->lang->line('BE_LBL_406'); ?></b>
				</td>
			</tr>
		</table>
	</div>
</div>
<div class="form-group row">
	<div class="col-lg-12">
		<table class="table table-striped table-bordered table-advance table-hover">
			<thead>
			<tr class="bg-primary">
				<th><?php echo $this->lang->line('BE_LBL_228'); ?></th>
				<?php echo $strCurrHdrs; ?>
			</tr>
			</thead>
			<tbody>
			<?php
			$rsGroups = fetch_price_plans();
			$totalGroups = count($rsGroups);
			$PLAN_PRICES_PER_PACK = getPlansPricesForService($id, 2);
			$CURRENCY_RATES = array();
			$rsCurrRates = fetch_currency_by_id();

			foreach ($rsCurrRates as $rw) {
				$CURRENCY_RATES[$rw->CurrencyId] = $rw->ConversionRate;
			}

			$i = 0;
			foreach ($rsGroups as $row) {
				if (isset($PLAN_PRICES_PER_PACK[$row->PricePlanId][$DEFAULT_CURRENCY_ID])) {
					$packPrice = $PLAN_PRICES_PER_PACK[$row->PricePlanId][$DEFAULT_CURRENCY_ID];
				} else {
					$packPrice = roundMe($price);
				}
				?>
				<tr>
					<td><?php echo stripslashes($row->PricePlan); ?></td>
					<td>
						<input type="hidden" name="planId<?php echo $i; ?>"
							   value="<?php echo $row->PricePlanId; ?>"/>
						<input type="text" onblur="savePrices();" id="txtPrice<?php echo $i; ?>_0"
							   name="txtPrice<?php echo $i; ?>_0" value="<?php echo $packPrice; ?>"
							   class="form-control">
					</td>
					<?
					$z = 0;
					foreach ($PACK_PRICES_OTHER as $key => $value) {
						if (isset($PLAN_PRICES_PER_PACK[$row->PricePlanId][$key])) {
							$packPrice = $PLAN_PRICES_PER_PACK[$row->PricePlanId][$key];
						} else if (isset($PLAN_PRICES_PER_PACK[$row->PricePlanId][$DEFAULT_CURRENCY_ID])) {
							$packPrice = roundMe($PLAN_PRICES_PER_PACK[$row->PricePlanId][$DEFAULT_CURRENCY_ID] * $CURRENCY_RATES[$key]);
						} else if (isset($PACK_PRICES_OTHER[$key])) {
							$packPrice = $PACK_PRICES_OTHER[$key];
						}
						?>
						<td>
							<input type="hidden" name="otherCurrencyId<?php echo $i; ?>_<?php echo $z + 1; ?>"
								   value="<?php echo $key; ?>"/>
							<input type="hidden" id="cRt<?php echo $i; ?>_<?php echo $z + 1; ?>"
								   name="cRt<?php echo $i; ?>_<?php echo $z + 1; ?>"
								   value="<?php echo $CURR_CONV_RATES[$key]; ?>"/>
							<input type="text" onblur="savePrices();"
								   name="txtOtherPrice<?php echo $i; ?>_<?php echo $z + 1; ?>"
								   id="txtOtherPrice<?php echo $i; ?>_<?php echo $z + 1; ?>"
								   value="<?php echo $packPrice; ?>" class="form-control">
						</td>
						<?
						$z++;
					}
					?>
				</tr>
				<?php
				$i++;
			}
			?>
			</tbody>
		</table>
	</div>
</div>
		<input type="hidden" value="<?php echo $totalGroups; ?>" name="totalGroups" id="totalGroups"/>
		<input type="hidden" value="<?php echo count($PACK_PRICES_OTHER); ?>" name="currencyCount"
			   id="currencyCount"/>
		<input type="hidden" value="<?php echo $DEFAULT_CURRENCY_ID; ?>" name="defaultCurrId"/>
		<input type="hidden" value="0" name="hdSavePrices" id="hdSavePrices"/>
	<?php } ?>
	<div class="form-group row">
		<div class="col-lg-12">
		<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
				class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
		</div>
	</div>
	<br/><br/>
	<?php if ($id > 0) { ?>
		<div class="form-group row">
			<label class="control-label col-lg-2">User Notes:</label>
			<div class="col-lg-6">
				<textarea class="form-control" id="txtUserNotes" name="txtUserNotes" rows="6"></textarea>
			</div>
		</div>
		<div class="form-group row">
			<?php $price_data = fetch_price_plan_data(); ?>
			<label class="control-label col-lg-2"><?php echo $this->lang->line('BE_LBL_18') ?>:</label>
			<div class="col-lg-4">
				<select name="uNplanId" id="uNplanId" class="form-control select2me"
						data-placeholder="Select...">
					<option value="0"><?php echo $this->lang->line('BE_LBL_407'); ?></option>
					<?php FillCombo(0, $price_data); ?>
				</select>
			</div>
		</div>
		<div class="form-group row">
			<label class="control-label col-lg-2"></label>
			<div class="col-lg-4">
				<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> class="btn btn-primary"
						id="btnPriceEml"><i class="fa fa-envelope"> </i> Send Service Credit Information To
					Users
				</button>&nbsp;
				<img style="display:none;" id="imgEmlLdr"
					 src="<?php echo base_url('assets/images/loading.gif'); ?>" border="0"
					 alt="Please wait..."/>
			</div>
		</div>
	<?php } ?>
	<?php echo form_close(); ?>
<?php } ?>
