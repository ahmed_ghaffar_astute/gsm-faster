<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_92'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>

<div class="form-group row">
	<div class="col-lg-12">
		<?php $USER_ID_BTNS = $userId;
		if($USER_ID_BTNS){
				include APPPATH . 'scripts/userbtns.php'; 
		}
		?>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<?php echo form_open(base_url('admin/clients/userapi'), 'class="form-horizontal" name="frm" id="frm"'); ?>
						<input type="hidden" id="userId" name="userId" value="<?php echo($userId); ?>"/>
						<div class="form-group row">
								<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_571'); ?>
									:</label>
								<div class="col-lg-4">
									<div class="switch-button switch-button-lg" data-on-label="YES"
										data-off-label="NO">
										<input type="checkbox" id="rdAPIAllow"
											name="rdAPIAllow" <?php if ($apiAllow == '1') echo 'checked'; ?>
											class="toggle"/><span><label for="rdAPIAllow"></label></span>
									</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_MR_20'); ?>
								:</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" readonly="readonly" name="txtAPIKey"
										id="txtAPIKey" value="<?php echo($apiKey); ?>"/>
								<?php if (!$IS_DEMO) { ?>
									<a href="<?php echo base_url('admin/clients/'); ?>userapi?gnrtKey=1&userId=<?php echo $userId; ?>"
										style="text-decoration:underline;"><?php echo $this->lang->line('BE_LBL_41'); ?></a>
								<?php } ?>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_535'); ?>
								:</label>
							<div class="col-lg-4">
								<textarea class="form-control" rows="3" id="txtIPs"
											name="txtIPs"><?php echo @$ips; ?></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_536'); ?>
								:</label>
							<div class="col-lg-4">
								<div class="switch-button switch-button-lg" data-on-label="YES"
										data-off-label="NO">
									<input type="checkbox" name="chkEmail" id="chkEmail" checked="checked"
											class="toggle chkSelect"/><span><label for="chkEmail"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-4"></div><div class="col-lg-4">
								<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
										id="btnSave" name="btnSave"
										class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
							</div>
						</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
