<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="looked the project coding and stuff">
                <h5 class="card-title"><?php echo $this->lang->line('BE_LBL_229'); ?></h5>
            </div>
        </div>
        <div class="card-body">
                <div class="form-group row">
                    <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_179'); ?>:<span class="required_field">*</span> </label>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" placeholder="Enter Subject" maxlength="100" name="txtSubject" id="txtSubject" value="<?php echo($subject);?>" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_726'); ?>:<span class="required_field">*</span></label>
                    <div class="col-lg-4">
                        <select id="dId" name="dId" class="form-control select2me" data-placeholder="Select...">
                            <?php FillCombo($dId, $dept_data); ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_CAT_1'); ?>:<span class="required_field">*</span></label>
                    <div class="col-lg-4">
                        <select id="cId" name="cId" class="form-control select2me" data-placeholder="Select...">
                            <?php FillCombo($cId, $dept_cat); ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 control-label">Status:<span class="required_field">*</span></label>
                    <div class="col-lg-4">
                        <select id="stId" name="stId" class="form-control select2me" data-placeholder="Select...">
                            <?php FillCombo($stId, $tckt_status, $objDBCD14); ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_189'); ?>:<span class="required_field">*</span></label>
                    <div class="col-lg-4">
                        <select id="pId" name="pId" class="form-control select2me" data-placeholder="Select...">
                            <?php FillCombo($pId, $tckt_priority); ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_362'); ?>:</label>
                    <div class="col-lg-4">
                        <textarea class="form-control" rows="3" name="txtNotes" id="txtNotes"><? echo $notes;  ?></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-8">
                        <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> id="btnStngs" name="btnStngs" class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
                        <img style="display:none;" id="imgStngs" src="<?php echo base_url('assets/images/loading.gif');?>" border="0" alt="Please wait..." />
                    </div>
                </div>
            </div>
           
    
    </div>
    
</div>
