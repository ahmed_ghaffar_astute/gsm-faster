<?php
$USER_ID_BTNS = $userId;
?>
<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div style="padding-top: 10px !important;" class="page-title d-flex">
			<h4>
				<i class="icon-arrow-left52 mr-2"></i>
				<?php echo $this->lang->line('BE_MENU_USRS_3'); ?>
			</h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<div class="form-group row">
	<div class="col-lg-12">
		<?php
			$USER_ID_BTNS = $userId;
			if($USER_ID_BTNS){
				include APPPATH . 'scripts/userbtns.php'; 
			}
		?> 
	</div>
</div>
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<?php if ($message != '')
						echo '<div class="alert alert-success">' . $message . '</div>'; 
					?>
					<div class="form-group row">
						<div class="col-lg-12">
							<h4 class="page-title">
								<b><?php echo $this->lang->line('BE_LBL_18'); ?>:</b> <?php echo $pricePlan == '' ? '-' : $pricePlan; ?><br/>
								<b><?php echo $this->lang->line('BE_LS_9'); ?>:</b><?php echo $userCurrency == '' ? '-' : $userCurrency; ?>
							</h4>
						</div>
					</div>
					<?php echo form_open(base_url('admin/services/userpackageprices?userId=' . $userId . "&sc=" . $sc), 'id="frm" name="frm"'); ?>
						<input type="hidden" name="planId" value="<?php echo $planId; ?>"/>
						<input type="hidden" name="sc" value="<?php echo $sc; ?>"/>
						<input type="hidden" name="plan" value="<?php echo $plan; ?>"/>
						<div class="form-group row">
							<div class="col-lg-6">
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('BE_LBL_219'); ?></label>
									<select name="categoryId" id="categoryId" class="form-control select2me"
											data-placeholder="<?php echo $this->lang->line('BE_LBL_256'); ?>"
											onChange="document.getElementById('frm').submit();">
										<option value="0"><?php echo $this->lang->line('BE_LBL_255'); ?></option>
										<?php FillCombo($categoryId, $categoriesRecords); ?>
									</select>
								</div>
							</div>
							<!--/span-->
							<div class="col-lg-6">&nbsp;</div>
							<!--/span-->
						</div>

					<?php echo form_close(); ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<?php echo form_open(base_url('admin/services/userpackageprices?userId=' . $userId . "&sc=" . $sc), 'id="frm" name="frm"'); ?>
							<div class="page-title d-flex">
								<h4>
									<i class="mr-2"></i>
									<?php echo $this->lang->line('BE_MENU_USRS_3'); ?>
								</h4>
							</div>
							<div class="table-responsive">
								<?php if ($NoData == 0 && $count > 0) { ?>
									<div class="form-group row">
										<div class="col-lg-6">&nbsp;</div>
										<div class="col-lg-6">
											<div class="form-group" align="right">
												<input type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
													value="<?php echo $this->lang->line('BE_LBL_181'); ?>" class="btn btn-primary"
													name="btnSubmit"/>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->
								<?php } ?>
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th>
											<input type="checkbox" id="chkSelect" name="chkSelect"
												onClick="selectAllChxBxs('chkSelect', 'chkPrice', <?php echo $count; ?>);"/>
										</th>
										<th><?php echo $this->lang->line('BE_PCK_HD_4'); ?></th>
										<th><?php echo $this->lang->line('BE_LBL_538'); ?></th>
										<th><?php echo $this->lang->line('BE_PCK_14'); ?></th>
									</tr>
									</thead>
									<tbody>
									<input type="hidden" name="categoryId" value="<?php echo $categoryId; ?>"/>
									<input type="hidden" name="sc" value="<?php echo $sc; ?>"/>
									<?
									if ($NoData == 0 && $count != 0) {
										$i = 0;
										foreach ($rsUserSrvcs as $row) {
											$currencyPrice = 0;
											$pckTitle = stripslashes($row->PackageTitle);
											if (isset($PACK_PRICES_USER[$row->PackageId])) {
												$currencyPrice = $PACK_PRICES_USER[$row->PackageId];
											} else {
												if (isset($PACK_PRICES_PLAN[$row->PackageId])) {
													$currencyPrice = $PACK_PRICES_PLAN[$row->PackageId];
												} else {
													if ($defaultCurrency == '1')
														$currencyPrice = $row->PackagePrice;
													else {
														if (isset($PACK_PRICES_PLAN_DEFAULT[$row->PackageId])) {
															$currencyPrice = roundMe($PACK_PRICES_PLAN_DEFAULT[$row->PackageId] * $conversionRate);
														} else {
															if (isset($PACK_ORIGINAL_PRICES[$row->PackageId]))
																$currencyPrice = $PACK_ORIGINAL_PRICES[$row->PackageId];
															else
																$currencyPrice = $row->PackagePrice * $conversionRate;
														}
													}
												}
											}

											if (isset($PACK_ORIGINAL_PRICES[$row->PackageId])) {
												$packOriginalPrice = $PACK_ORIGINAL_PRICES[$row->PackageId];
											} else {
												$packOriginalPrice = $row->PackagePrice * $conversionRate;
											}

											?>
											<tr>
												<td align="center">
													<input type="checkbox" id="chkPrice<?php echo $i; ?>" name="chkPrice[]"
														value="<?php echo $row->PackageId . '|' . $i; ?>">
												</td>
												<td><?php echo $pckTitle; ?></td>
												<td><?php echo roundMe($packOriginalPrice); ?></td>
												<td>
													<input type="hidden" name="packageId<?php echo $i; ?>"
														value="<?php echo $row->PackageId; ?>"/>
													<input type="text" name="txtPrice<?php echo $i; ?>"
														value="<?php echo roundMe($currencyPrice); ?>" class="form-control">
												</td>
											</tr>
											<?
											$i++;
										}
									} else
										echo "<tr><td colspan='7'>" . $this->lang->line('BE_GNRL_9') . "</td></tr>";
									?>
									<input type="hidden" value="<?php echo $count; ?>" name="totalPackages"/>
									<input type="hidden" value="<?php echo $userId; ?>" id="userId" name="userId"/>
									</tbody>
								</table>
							</div>
							<?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

