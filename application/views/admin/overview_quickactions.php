<div class="tab-pane" id="tab_1_3">
    <div class="tab-container tab-left">
        <div class="col-md-3">
            <ul class="nav nav-tabs flat-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#tab_1-1">
                        <i class="fa icon-cog"></i>
                        <?php echo $this->lang->line('BE_LBL_328'); ?> 
                    </a>
                    <span class="after"></span>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab_1-5"><i class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> <?php echo $this->lang->line('BE_LBL_310'); ?></a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab_1-2"><i class="fa fa-lock"></i> <?php echo $this->lang->line('BE_PWD_HEADING'); ?></a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab_1-3"><i class="fa icon-eye"></i> <?php echo $this->lang->line('BE_LBL_391'); ?></a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab_1-4"><i class="fa fa-envelope"></i> Send Custom Email</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab_1-6"><i class="fa fa-dollar"></i> Remove Payment Methods</a>
                </li>
            </ul>
        </div>
        <div class="col-md-9">
            <div class="tab-content">
                <div id="tab_1-1" class="tab-pane active">
                    <div class="margiv-top-10">
						<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> name="btnChPwd" id="btnChPwd" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_331'); ?></button>
                        <a href="<?php echo base_url('admin/clients');?>lgn_a_s_clint?uId=<?php echo $USER_ID_BTNS; ?>" target="_blank" class="btn btn-primary" name="btnLoginAsClient" id="btnLoginAsClient"><?php echo $this->lang->line('BE_LBL_384'); ?></a>
						<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> name="btnChPin" id="btnChPin" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn default"><?php echo $this->lang->line('BE_LBL_650'); ?></button>
                        <input type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="<?php echo $this->lang->line('BE_LBL_321'); ?>" class="btn btn-danger" onClick="deleteClient();" />
                        <img style="display:none;" id="statusLoader" src="<?php echo base_url();?>assets/images/loading.gif" border="0" alt="Please wait..." />
                        <input type="hidden" name="id" id="id" value="<?php echo $USER_ID_BTNS; ?>" />
                    </div>
                </div>
                <div id="tab_1-2" class="tab-pane ">
                    <div class="form-group">
                        <div class="form-group">
                            <div class="col-md-4">
                                <input type="text" class="form-control" placeholder="Password" maxlength="20" name="txtPass" id="txtPass" />
                            </div>
							<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> id="btnChangePwd" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary">Change Password and Notify Client</button>
	                        <img style="display:none;" id="statusLoader98" src="<?php echo base_url();?>assets/img/loading.gif" border="0" alt="Please wait..." />
                        </div>
                    </div>
                </div>
                <div id="tab_1-3" class="tab-pane ">
                    <form method="post">
                        <div class="form-group">
                            <label class="control-label">IP:*</label>
                            <textarea class="form-control" rows="3" name="txtIP"></textarea>
                            <span class="form-text text-muted" style="font-size:11px;">&nbsp; Seperated By Commas</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><?php echo $this->lang->line('BE_GNRL'); ?></label>
                            <textarea class="form-control" name="txtComments" rows="3"></textarea>
                        </div>
                        <div class="margin-top-10">
                        	<button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
                            <input type="hidden" name="id" id="id" value="<?php echo $USER_ID_BTNS; ?>" />
                        </div>
                    </form>
                </div>
                <div id="tab_1-5" class="tab-pane ">
                    <form action="#" class="">
                    <?php echo form_open('');?>
                        <table class="table table-bordered table-striped">
                            <tr>
                                <td>
                                    <?php echo $this->lang->line('BE_LBL_329'); ?>
                                </td>
                                <td>
                                    <label class="uniform-inline">
                                    <input type="radio" class="chkSelect" name="rdAllowCrdtTransfr" id="rdAllowCrdtTransfr1" <?php if($transferCredits == '1') echo 'checked'?> />
                                    Yes </label>
                                    <label class="uniform-inline">
                                    <input type="radio" class="chkSelect" name="rdAllowCrdtTransfr" id="rdAllowCrdtTransfr2" <?php if($transferCredits == '0') echo 'checked'?> />
                                    No </label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Can Buy Credits
                                </td>
                                <td>
                                    <label class="uniform-inline">
                                    <input type="radio" name="rdBuyCr" id="rdBuyCr1" <?php if($buyCr == '1') echo 'checked'?> />Yes </label>
                                    <label class="uniform-inline">
                                    <input type="radio" name="rdBuyCr" id="rdBuyCr2" <?php if($buyCr == '0') echo 'checked'?> />No </label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Auto Fill Credits
                                </td>
                                <td>
                                    <label class="uniform-inline">
                                    <input type="radio" name="rdAutoFillCrdts" id="rdAutoFillCrdts1" <?php if($autoFillCredits == '1') echo 'checked'?> />
                                    Yes </label>
                                    <label class="uniform-inline">
                                    <input type="radio" name="rdAutoFillCrdts" id="rdAutoFillCrdts2" <?php if($autoFillCredits == '0') echo 'checked'?> />
                                    No </label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Remove All Services From Client's Account
                                </td>
                                <td>
                                    <label class="uniform-inline">
                                    <input type="radio" name="rdRmvSrvcs" id="rdRmvSrvcs1" <?php if($rmvAllSrvcs == '1') echo 'checked'?> />
                                    Yes </label>
                                    <label class="uniform-inline">
                                    <input type="radio" name="rdRmvSrvcs" id="rdRmvSrvcs2" <?php if($rmvAllSrvcs == '0') echo 'checked'?> />
                                    No </label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo $this->lang->line('BE_LBL_571'); ?>
                                </td>
                                <td>
                                    <label class="uniform-inline">
                                    <input type="radio" class="chkSelect" name="rdAPIAllowed" id="rdAPIAllowed1" <?php if($API_ALLOWED == '1') echo 'checked'?> />
                                    Yes </label>
                                    <label class="uniform-inline">
                                    <input type="radio"  class="chkSelect" name="rdAPIAllowed" id="rdAPIAllowed2" <?php if($API_ALLOWED == '0') echo 'checked'?> />
                                    No </label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Activate/Deactivate Client Account
                                </td>
                                <td>
                                    <label class="uniform-inline">
                                    <input type="radio" class="chkSelect" name="rdActivate" id="rdUserStatus1" onclick="showReason();" <?php if($status == '0') echo 'checked'?> />
                                    Activate </label>
                                    <label class="uniform-inline">
                                    <input type="radio" class="chkSelect" name="rdActivate" id="rdActivate2" onclick="showReason();" <?php if($status == '1') echo 'checked'?> />
                                    Deactivate </label>
                                </td>
                            </tr>
                            <tr id="rwReason" <?php if($status != '1') echo 'style="display:none;"'?>>
                                <td>
                                    Deactivation Reason
                                </td>
                                <td>
                                    <textarea class="form-control" rows="3" id="txtReason" name="txtReason"><?php echo $reason ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Client Group
                                </td>
                                <td>
                                    <?php $price_data=fetch_price_plan_data();?>
                                    <label class="uniform-inline">
                                    <select name="planId" id="planId" class="form-control select2me" style="width:200px;">
                                        <option value="0"><?php echo $this->lang->line('BE_LBL_623').'...';?></option>
                                        <?php FillCombo($planId, $price_data); ?>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <div class="margin-top-10">
							<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> name="btnSave" id="btnSave" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
	                        <img style="display:none;" id="statusLoader99" src="<?php echo base_url();?>assets/images/loading.gif" border="0" alt="Please wait..." />
                        </div>
                    <?php echo form_close();?>
                </div>
                <div id="tab_1-4" class="tab-pane">
                    <div class="form-group">
                        <label class="control-label">Newsletter:*</label>
                        <select name="ltrId" id="ltrId" class="form-control select2me" data-placeholder="Select...">
                        	<?php include APPPATH.'scripts/newsltrsdropdown.php';?>
                        </select>
                    </div>
                    <div class="margin-top-10">
                        <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> name="btnSendEml" id="btnSendEml" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary">Send Email</button>
                        <img style="display:none;" id="statusLoader97" src="<?php echo base_url();?>assets/images/loading.gif" border="0" alt="Please wait..." />
                    </div>
                </div>
				<div id="tab_1-6" class="tab-pane">
                    <h3><b>Remove Payment Methods</b></h3>
                    <?php echo form_open();?>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo $this->lang->line('BE_LBL_124'); ?>:</label>
                            <div class="col-md-4">
                                <select multiple="multiple" class="multi-select" id="my_multi_select1" name="pMethodIds[]">
                                    <?php FillSelectedList_payments($USER_ID_BTNS, 'PaymentMethodId'); ?>
                                </select>
                            </div>
                        </div>
                        <div align="center" class="margin-top-10">
                        	<button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
                            <input type="hidden" name="id" id="id" value="<?php echo $USER_ID_BTNS; ?>" />
                            <input type="hidden" name="hdRmvPMs" />
                        </div>
                    <?php echo form_close(); ?>
                </div>                
            </div>      
        </div>
        <!--end col-md-9-->
    </div>
</div>
<!--end tab-pane-->
<script>
function deleteClient()
{
	var id = document.getElementById('id').value;
	if(id != '')
	{
		if(confirm('<?php echo $this->lang->line('BE_LBL_150'); ?>'))
			window.location.href = '<?php echo base_url('admin/clients/');?>users.php?id='+id+'&del=1';
	}
	else
	{
		alert('Invalid Client');	
	}
}
</script>
