				<!-- Page header -->
				<div class="page-header border-bottom-0">
					<div class="page-header-content header-elements-md-inline">
						<div class="page-title d-flex">
							<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_837'); ?></h4>
							<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
						</div>
						<div align="right">
							<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> class="btn btn-primary"
									onclick="javascript:if(confirm('<? echo $this->lang->line('BE_LBL_854'); ?>')) window.location.href='<?php echo base_url('admin/settings/cpaneltheme?pr=1'); ?>';"><? echo $this->lang->line('BE_LBL_838'); ?></button>
						</div>
					</div>
				</div>
				<!-- /page header -->
				<!-- Content area -->
				<div class="content pt-0">
					<div class="card">
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
								<div class="card-body">
									<!-- Our Working Area Start -->
									<?php if (isset($message) && $message != '') { ?>
										<div class="form-group row">
											<div class="col-lg-12">
												<div class="alert alert-success"><?php echo $message; ?></div>

											</div>
										</div>
									<?php } ?>				<? echo form_open(base_url('admin/settings/cpaneltheme'), array('name' => 'frm', 'id' => 'frm', 'class' => 'form-horizontal'));
				/*if ($message != '') {*/ ?>
					<!--<div class="form-group row">
						<div class="col-lg-12">
							<div class="alert alert-success"><?php /*echo $message; */?></div>
						</div>
					</div>-->
				<?php /*} */?>
				<div class="form-group row">
					<div class="col-lg-12">

						<div class="tabbable tabbable-custom boxless">
							<ul class="nav nav-tabs nav-tabs-solid rounded">
								<li class="nav-item">
									<a href="#tab_0" class="nav-link active"
									   data-toggle="tab"><? echo $this->lang->line('BE_LBL_751'); ?></a>
								</li>
								<li class="nav-item">
									<a href="#tab_1" class="nav-link"
									   data-toggle="tab"><? echo $this->lang->line('BE_LBL_752'); ?></a>
								</li>
								<li class="nav-item">
									<a href="#tab_2" class="nav-link"
									   data-toggle="tab"><? echo $this->lang->line('BE_LBL_753'); ?></a>
								</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="tab_0">
									<? include 'cptheme_tab_hdr.php'; ?>
								</div>
								<div class="tab-pane" id="tab_1">
									<? include 'cptheme_tab_cntnts.php'; ?>
								</div>
								<div class="tab-pane" id="tab_2">
									<? include 'cptheme_tab_ftr.php'; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group row">
						<div class="col-lg-5"></div>
						<div class="col-lg-7">
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary"><? echo $this->lang->line('BE_LBL_72'); ?></button>
						</div>

				</div>
				</form>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>
<script language="javascript"
		src="include/js/modules/emailsettings.js?v=<?= filemtime('include/js/modules/emailsettings.js'); ?>"></script>
