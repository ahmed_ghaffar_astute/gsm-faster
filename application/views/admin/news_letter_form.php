<?php if ($existingImage != '') { ?>
	<p align="center" valign="middle"><img src="../<?php echo $existingImage ?>"/></p>
<?php } ?>
<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_30'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<a href="newsletters?frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"
			   class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<!-- BEGIN FORM-->
					<?php echo form_open(base_url() . 'admin/Miscellaneous/insertNewsletters', array('class' => "form-horizontal", 'name' => "frm", 'method' => "post", 'id' => "frm", 'enctype' => "multipart/form-data")); ?>
					<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_CAT_1'); ?>:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<select name="categoryId" id="categoryId" class="form-control select2me">
								<option value="select">Select...</option>
								<?php $newsltrcategory = getNewsCategories(); ?>
								<?php FillCombo($categoryId, $newsltrcategory); ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_31'); ?>:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter Title" maxlength="100"
								   name="txtTitle" id="txtTitle" value="<?php echo($newsLtrTitle); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-3"><?php echo $this->lang->line('BE_LBL_30'); ?>:</label>
						<div class="col-lg-9">
							<textarea class="ckeditor form-control" name="txtNewsLtr"
									  rows="6"><? echo $newsLtr; ?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_GNRL_1'); ?>:</label>
						<div class="col-lg-4">
							<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
								<input type="checkbox" name="chkDisableNewsLtr" id="chkDisableNewsLtr" <?php echo $disableNewsLtr == 1 ? 'checked' : ''; ?> class="toggle"/><span><label for="chkDisableNewsLtr"></label></span>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3"></div>
							<div class="col-lg-9">
								<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
										class="btn btn-primary"><? echo $this->lang->line('BE_LBL_72'); ?></button>
							</div>
						</div>

					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>


<script type="text/javascript" src="<?php echo base_url('assets/plugins/ckeditor/ckeditor.js'); ?>"></script>
<script language="javascript" src="<?php echo base_url('assets/js/newsletter.js'); ?>"></script>
