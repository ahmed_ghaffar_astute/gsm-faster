<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_685'); ?></h5>
			</div>
			<div class="card-body">

				<div class="form-group row">
					<label class="col-lg-4 col-form-label"><?php echo $this->lang->line('BE_LBL_686'); ?>:</label>
					<div class="col-lg-4">
						<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
							<input type="checkbox" name="chkIPRestriction" id="chkIPRestriction" <?php echo $ipRestriction == 1 ? 'checked' : ''; ?> class="form-input-styled"/>
							<span><label for="chkIPRestriction"></label></span>
						</div>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-lg-4 col-form-label">Apply IP Restrictions at API:</label>
					<div class="col-lg-4">
						<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
							<input type="checkbox" name="chkAPIIPRstrct"id="chkAPIIPRstrct" <?php echo $apiIPRstrct == 1 ? 'checked' : ''; ?> class="form-input-styled"/>
							<span><label for="chkAPIIPRstrct"></label></span>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-offset-4 col-md-8">
						<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> id="btnIPRestriction" name="btnIPRestriction" class="btn btn-primary">
							<?php echo $this->lang->line('BE_LBL_72'); ?>
						</button>
						<div style="display:none;" id="dvLoaderIP">
							<img src="<?php echo base_url('assets/images/loading.gif'); ?>" border="0" alt="Please wait..."/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
