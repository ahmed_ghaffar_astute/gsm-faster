<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">Refresh API Orders</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" align="center">
            <div class="block-web">
            <br /><br /><br /><div style="font-size:18px;font-weight:bold;" class="alert alert-success">API Fetching orders list has been refreshed and reset.</div>
            </div>
        </div>
    </div>
</div>