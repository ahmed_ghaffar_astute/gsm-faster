<!-- Page header -->
<div class="page-header border-bottom-0">
    <div class="page-header-content header-elements-md-inline">
        <div style="padding-top: 10px !important;" class="page-title d-flex">
            <h4>
                <i class="icon-arrow-left52 mr-2"></i>
                <?php echo $this->lang->line('BE_LBL_537'); ?>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

    </div>
</div>

<div class="form-group row">
    <div class="col-lg-12">
        <?php $USER_ID_BTNS = $userId;
        if($USER_ID_BTNS){
            include APPPATH.'scripts/userbtns.php';
        }
        ?>
    </div>
</div>

<!-- /page header -->

<div class="content pt-0">
    <div class="card">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card-body">
                    <?php if (isset($message) && $message != '') { ?>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <div class="alert alert-success"><?php echo $message; ?></div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php echo form_open(base_url('admin/clients/credithistory?userId' . $userId) , 'class="horizontal-form" id="frmSrch" name="frmSrch"');?>
                        <h2>
                            <?php echo $this->lang->line('BE_GNRL_BTN_1'); ?>
                        </h2>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <div class="form-group ">
                                    <label class="control-label"><?php echo $this->lang->line('BE_PM_7'); ?></label><br />
                                    <input class="form-control form-control-inline input-largest date-picker" type="text" name="txtFromDt" value="<?php  echo($dtFrom);?>" />
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $this->lang->line('BE_PM_8'); ?></label><br />
                                    <input class="form-control form-control-inline input-largest date-picker" type="text" name="txtToDt" value="<?php echo($dtTo);?>" />
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                
                        <div class="form-group row">
							<div class="col-lg-6">
                            <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><?php  echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
                            <input type="hidden" name="userId" id="userId" value="<?php echo $userId; ?>" />
                            <input type="hidden" name="start" id="start" value="0" />
							</div>
                        </div>
                    <?php echo form_close(); ?>
                    <?php
                    if($rsHistory)
                    {
                        $row = fetch_credit_history_by_userid($userId , $strWhere);
                        
                        $totalRows = $row->TotalRecs;
                        if($totalRows > $limit)
                            doPages_DropDown($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next, 'frmSrch');
                            
                    }
                    ?>
                   
                    <h2>
                        <?php echo $this->lang->line('BE_LBL_70'); ?>
                    </h2>    
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr class="bg-primary">
                                <th><?php echo $this->lang->line('BE_CR_2'); ?></th>
                                <th>Order Data</th>
                                <th><?php echo $this->lang->line('BE_USR_10'); ?></th>
                                <th nowrap><?php echo $this->lang->line('BE_CR_1'); ?></th>
                                <th><?php echo $this->lang->line('BE_CODE_7'); ?></th>
                                <th>IP</th>
                                <th>Fee</th>
                                <th><?php echo $this->lang->line('BE_GNRL'); ?></th>
                                
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if($rsHistory)
                            {
                                foreach($rsHistory as $row)
                                {
                                    if($row->CreditsLeft != '')
                                        $creditsLeft = decrypt($row->CreditsLeft , $key);
                                    else
                                        $creditsLeft = '-'; 
                                ?>
                                    <tr>
                                        <td><div class="success"></div>
                                            <?php  echo stripslashes($row->Description);?>
                                            <?php if(is_numeric($row->LogRequestId) && $row->LogRequestId > 0)
                                            {
                                                if($row->OType == '0')
                                                    $lnk = base_url('admin/services/code?id='.$row->LogRequestId);
                                                else if($row->OType == '1')
                                                    $lnk = base_url('admin/services/')."codeslbf?id=".$row->LogRequestId;
                                                else if($row->OType == '2')
                                                    $lnk = base_url('admin/services/')."logrequest?id=".$row->LogRequestId;
                                            ?>
                                                - <a style="text-decoration:underline;" href="<?php  echo $lnk;?>">#<?php  echo $row->LogRequestId;?></a>
                                            <?php  }?>
                                        </td>
                                        <td><?php echo $row->IMEINo;?></td>
                                        <td><?php echo $row->Credits >= 0 ? number_format($row->Credits, 2, '.', '') : $row->Credits;?></td>
                                        <td><?php echo $creditsLeft != '-' ? number_format($creditsLeft, 2, '.', '') : $creditsLeft;?></td>
                                        <td><?php echo convertDate($row->HistoryDtTm);?></td>
                                        <td><?php echo $row->IP;?></td>
                                        <td><?php echo $row->FeePercentage;?></td>
                                        <td><?php echo stripslashes($row->Comments);?></td>
                                    </tr>
                            <?php
                                }
                            }
                            else
                                echo "<tr><td colspan='8'>".$this->lang->line('BE_GNRL_9')."</td></tr>";
                            ?>
                            </tbody>
                        </table>
                    </div>       
                </div>
            </div>
        </div>
    </div>
