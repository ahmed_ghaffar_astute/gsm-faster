<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_867'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<a href="<?php echo base_url('admin/cms/');?>brandlogos?frmId=<? echo $this->input->post_get('frmId');?>&fTypeId=<? echo $this->input->post_get('fTypeId')?>" class="btn btn-sm btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">

	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">
					<? if($existingImage != '') { ?>
						<p align="center" valign="middle"><img src="<?php echo base_url().$existingImage ?>" /></p>
					<? } ?>
						</div>
					</div>
					<div class="form-group row">
							<div class="col-lg-12 form">
								<?php echo form_open(base_url('admin/cms/addbrandlogo?id='.$id),array('class'=>"form-horizontal",'name'=>"frm",'method'=>"post",'id'=>"frm",'enctype'=>"multipart/form-data"));?>
								<!--<form action="addbrandlogo.php" class="form-horizontal" name="frm" method="post" id="frm" enctype="multipart/form-data">-->
								<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
								<input type="hidden" id="existingImage" name="existingImage" value="<?php echo $existingImage; ?>">
								<div class="form-body">
									<div class="form-group">
										<label for="txtImage" class="col-md-3 control-label"><? echo $this->lang->line('BE_N_3'); ?>:</label>
										<div class="col-lg-9">
											<input type="file" id="txtImage" name="txtImage">
											<span class="form-text text-muted">
												.jpg, .gif, .png <strong>Ideal Image size is Width: 390px height: 73px</strong>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-lg-3"></div><div class="col-lg-9">
										<button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><? echo $this->lang->line('BE_LBL_72'); ?></button>
									</div>
								</div>
								<?php echo form_close();?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

