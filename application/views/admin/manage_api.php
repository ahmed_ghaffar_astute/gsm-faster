				<!-- Page header -->
				<div class="page-header border-bottom-0">
					<div class="page-header-content header-elements-md-inline">
						<div class="page-title d-flex">
							<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_MR_HD_5'); ?></h4>
							<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
						</div>
						<div align="right">
							<a href="apis?frmId=<? echo $this->input->post_get('frmId');?>&fTypeId=<? echo $this->input->post_get('fTypeId')?>" class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
						</div>
					</div>
				</div>
				<!-- /page header -->
				<!-- Content area -->
				<div class="content pt-0">

					<div class="card">
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
								<div class="card-body">
									<!-- Our Working Area Start -->
									<?php if (isset($message) && $message != '') { ?>
										<div class="form-group row">
											<div class="col-lg-12">
												<div class="alert alert-success"><?php echo $message; ?></div>

											</div>
										</div>
									<?php } ?>
				<div class="form-group row">
					<div class="col-lg-12">

                        <input type="hidden" id="id" name="id" value="<? echo($id); ?>">
                        <input type="hidden" id="url" name="url" value="<? echo($url); ?>">
                        <input type="hidden" id="apiKey" name="apiKey" value="<? echo($apiKey); ?>">
                        <input type="hidden" id="userName" name="userName" value="<? echo($accountId); ?>">
                        <ul class="nav nav-tabs nav-tabs-solid rounded">
                            <li class="nav-item active">
                                <a href="#tab_0" data-toggle="tab" class="nav-link active"><? echo $this->lang->line('BE_LBL_380'); ?></a>
                            </li>
                            <? if($id > 0 && $id != '206' && $IMEI_API == '1') { ?>
                                <li class="nav-item">
                                    <a href="#tab_1" data-toggle="tab" class="nav-link"><? echo $this->lang->line('BE_LBL_506').' - ' .$this->lang->line('BE_MENU_PCKGS'); ?></a>
                                </li>
							<? }
                            if($id > 0 && $FILE_API == '1')
							{
                                echo '<li class="nav-item"><a href="#tab_3" data-toggle="tab" class="nav-link">' . $this->lang->line('BE_LBL_862') . '</a></li>';
							}
                            if($id > 0 && $SERVER_API == '1')
							{
                                echo '<li class="nav-item"><a href="#tab_4" data-toggle="tab" class="nav-link">' . $this->lang->line('BE_LBL_863') . '</a></li>';
							}
							if($id > 0) { ?>
                                <li class="nav-item">
                                    <a href="#tab_2" data-toggle="tab" class="nav-link"><? echo $this->lang->line('BE_LBL_619'); ?></a>
                                </li>
							<? } ?>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_0">
                                <? echo $api_tab_general;  ?>
                            </div>
							<? if($id > 0 && $id != '206' && $IMEI_API == '1') { ?>
                                <div class="tab-pane" id="tab_1">
                                    <? echo $api_tab_sync; ?>
                                </div>
							<? } ?>
							<? if($id > 0 && $FILE_API == '1') { ?>
                                <div class="tab-pane" id="tab_3">
                                    <? echo $api_file_sync; ?>
                                </div>
							<? } ?>
							<? if($id > 0 && $SERVER_API == '1') { ?>
                                <div class="tab-pane" id="tab_4">
                                    <? echo $api_server_sync; ?>
                                </div>
							<? } ?>
							<? if($id > 0) { ?>
                                <div class="tab-pane" id="tab_2">
	                            	<? echo $api_tab_crons; ?>
                                </div>
                            <? } ?>
                        </div>
					</div>
				</div>
</div>
</div>
<script language="javascript" src="<?php base_url('assets/js/api.js'); ?>"></script>
	</div>
</div>
