<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_500'); ?></h5>
			</div>
			<div class="card-body">
				<!-- BEGIN FORM-->
				<?php echo form_open(base_url('admin/settings/manageapi'), array('name' => 'frm', 'id' => 'frm', 'class' => 'form-horizontal')); ?>
				<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
				<input type="hidden" name="hdAPIType" value="<?php echo($apiType); ?>">
				<div class="form-group row">
					<label class="col-lg-4 col-form-label"><?php echo $this->lang->line('BE_LBL_135'); ?>:<span class="required_field">*</span></label>
					<div class="col-lg-5">
						<select class="form-control select2me" data-placeholder="Select..." name="apiType"
								id="apiType" <? if ($systemAPI == '1') echo 'disabled="disabled"'; ?>>
							<option value="6" selected>GSM Tool</option>
							<option value="4" <?php if ($apiType == '4') echo 'selected'; ?>>GSM Fusion</option>
							<option value="2" <?php if ($apiType == '2') echo 'selected'; ?>>DHRU Fusion</option>
							<option value="7" <?php if ($apiType == '7') echo 'selected'; ?>>GSM HUB</option>
							<option value="1" <?php if ($apiType == '1') echo 'selected'; ?>>Hot Fusion</option>
							<option value="3" <?php if ($apiType == '3') echo 'selected'; ?>>CodeskPro Server</option>
							<option value="5" <?php if ($apiType == '5') echo 'selected'; ?>>Naksh Soft</option>
							<option value="0" <?php if ($apiType == '0') echo 'selected'; ?>>Other</option>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 col-form-label"><?php echo $this->lang->line('BE_MR_19'); ?>:<span class="required_field">*</span></label>
					<div class="col-lg-5">
						<input type="text" class="form-control" placeholder="Enter Title" maxlength="100"
							   name="txtTitle" id="txtTitle"
							   value="<?php echo($apiTitle); ?>" <? if ($systemAPI == '1') echo 'readonly="readonly"'; ?> />
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 col-form-label"><?php echo $this->lang->line('BE_LBL_38'); ?>:<span class="required_field">*</span></label>
					<div class="col-lg-5">
						<input type="text" class="form-control" placeholder="Enter Supplier's URL" name="txtURL"
							   id="txtURL"
							   value="<?php echo($url); ?>" <? if ($systemAPI == '1') echo 'readonly="readonly"'; ?> />
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 col-form-label"><?php echo $this->lang->line('BE_MR_22'); ?>:</label>
					<div class="col-lg-5">
						<input type="text" class="form-control" placeholder="Enter Username" maxlength="100"
							   name="txtAccountId" id="txtAccountId" value="<?php echo($accountId); ?>"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 col-form-label"><?php echo $this->lang->line('BE_MR_20'); ?>:<span class="required_field">*</span></label>
					<div class="col-lg-5">
						<input type="text" class="form-control" placeholder="XXXX-XXXX-XXXX-XXXX-XXXX" name="txtAPIKey"
							   id="txtAPIKey" value="" title="Enter New API Key ONLY"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 col-form-label"><?php echo $this->lang->line('BE_LBL_597'); ?>:</label>
					<div class="col-lg-5">
						<input type="text" class="form-control" placeholder="Enter API Key-2" name="txtAPIKey2"
							   value="<?php echo($apiKey2); ?>"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 col-form-label"><?php echo $this->lang->line('BE_LBL_598'); ?>:</label>
					<div class="col-lg-5">
						<input type="text" class="form-control" placeholder="Enter API Password" name="txtAPIPass"
							   value="<?php echo($password); ?>"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 col-form-label">Credit Balance Threshold Limit:</label>
					<div class="col-lg-5">
						<input type="text" class="form-control" placeholder="Enter Credit Balance Threshold Limit"
							   id="txtThresholdLimit" name="txtThresholdLimit"
							   value="<? echo $threshold == 0 ? '' : $threshold; ?>"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 col-form-label">Notification Emails (Separated By Comma):</label>
					<div class="col-lg-5">
						<input type="text" class="form-control" placeholder="Emails to be notified for threshold amount"
							   name="txtNEmails" value="<?php echo($emails); ?>"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 col-form-label"><?php echo $this->lang->line('BE_GNRL_1'); ?>:</label>
					<div class="col-lg-5">
						<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
							<input type="checkbox" name="chkDisable"
								   id="chkDisable" <?php echo $disableAPI == 1 ? 'checked' : ''; ?>
								   class="form-input-styled"/><span><label for="chkDisable"></label></span>
						</div>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-lg-4 col-form-label"></div>
					<div class="col-lg-5">
						<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
								class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
						<?php if ($id > 0) { ?>
							<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary"
									id="btnVC"><?php echo $this->lang->line('BE_LBL_76'); ?></button>
							<img style="display:none;" id="vcLoader"
								 src="<?php echo base_url('assets/images/loading.gif'); ?>" border="0"
								 alt="Please wait..."/>
						<?php } ?>

					</div>
				</div>
				<?php echo form_close(); ?>
				<!-- END FORM-->
			</div>
		</div>
	</div>
</div>

