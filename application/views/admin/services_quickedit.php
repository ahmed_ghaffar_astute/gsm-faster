<!-- Page header -->
<div class="page-header border-bottom-0 pt-4 pl-3">
	<div class="form-group row">
		<div class="col-lg-12">
			<?php
			if ($iFrm == '1')
				include APPPATH . 'scripts/serverservices_header.php';
			else if ($iFrm == '0')
				include APPPATH . 'scripts/services_header.php';
			?>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i
					class="icon-arrow-left52 mr-2"></i><?php echo $fs == '0' ? $this->lang->line('BE_PCK_HD_1') : $this->lang->line('BE_PCK_25'); ?>
			</h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<?php echo form_open(base_url('admin/services/services_quickedit'), 'id="frm" name="frm"'); ?>
					<div class="form-group row">
						<div class="col-lg-6">
							<?php $package_category_data = fetch_log_package_category($tblName, $strWhereCat); ?>
							<label class="control-label"><?php echo $this->lang->line('BE_LBL_219'); ?></label>
							<select name="categoryId" id="categoryId" class="form-control select2me"
									data-placeholder="<?php echo $this->lang->line('BE_LBL_256'); ?>">
								<option value="0"><?php echo $this->lang->line('BE_LBL_255'); ?></option>
								<?php FillCombo($categoryId, $package_category_data); ?>
							</select>
						</div>
						<div class="col-lg-6">
							<?php $custom_data = custom_query_services_quickedit($colId, $colTitle, $tblPckName, $disableCol, $strWhereSrv, $categoryId); ?>
							<label
								class="control-label"><?php echo $this->lang->line('BE_PCK_HD_4'); ?></label>
							<select id="serviceId" name="serviceId" class="form-control select2me"
									data-placeholder="Select...">
								<option value="0">Please Choose Service...</option>
								<?php FillCombo($serviceId, $custom_data); ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">

							<label class="control-label"></label>
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary purple"
									onclick="setValue('0');document.getElementById('frm').submit();"><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
							<img style="display:none;" id="imgSrvcLoader"
								 src="<?php echo base_url('assets/images/loading.gif'); ?>" border="0"
								 alt="Please wait..."/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="form-group" align="right">
								<br/>
								<input type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									   value="<?php echo $this->lang->line('BE_LBL_72'); ?>"
									   onclick="setValue('1');" class="btn btn-primary" name="btnSubmit"/>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th nowrap="nowrap" width="4%">Sr. #</th>
										<th nowrap="nowrap" width="2%"></th>
										<th width="40%"><?php echo $this->lang->line('BE_PCK_HD_4'); ?>
										</th>
										<?php if ($fs == '0') echo '<th width="11%" nowrap>Instant</th>'; ?>
										<th width="16%" nowrap="nowrap">
											<?php echo $this->lang->line('BE_PCK_10'); ?></th>
										<th width="8%" nowrap="nowrap">
											<?php echo $this->lang->line('BE_LBL_651'); ?></th>
										<th width="8%"><?php echo $this->lang->line('BE_PCK_14'); ?>
										</th>
										<?php
										$PLANS = array();
										if ($totalGroups > 0) {
											foreach ($rsGroups as $row) {
												echo '<th width="8%">' . stripslashes($row->PricePlan) . '</th>';
												$PLANS[$row->PricePlanId] = stripslashes($row->PricePlan);
											}

										} ?>
										<th style="text-align: center"><input type="checkbox"
																			  id="chkSelect" name="chkSelect"
																			  class="chkSelect"
																			  onClick="selectAllChxBxs('chkSelect', 'chkPacks', <?php echo $count; ?>);"
																			  value="true">
										</th>
									</tr>
									</thead>
									<input type="hidden" value="0" name="cldFrm" id="cldFrm"/>
									<input type="hidden" value="<?php echo $sc ?>" id="sc" name="sc"/>
									<input type="hidden" value="<?php echo $sc ?>" name="fs"/>
									<input type="hidden" value="<?php echo $totalGroups ?>"
										   name="totalPlans"/>
									<input type="hidden"
										   value="<?php echo $this->input->post_get('frmId'); ?>"
										   name="frmId"/>
									<input type="hidden"
										   value="<?php echo $this->input->post_get('fTypeId'); ?>"
										   name="fTypeId"/>
									<tbody id="sortableLst">
									<?php
									$strCurrPacks = 0;
									if ($count != 0) {
										$i = 0;
										foreach ($rsPacks as $row) {
											$priceColStyle = '';
											$strCurrPacks .= ', ' . $row->$colId;
											$packStatus = $row->$disableCol == '0' ? 'Active' : 'In Active';
											if ($row->$colPrice < $row->CostPrice)
												$priceColStyle = ' style="color:red; font-weight:bold;"';
											if ($sc == '2')
												$editLnk = base_url('admin/services/serverservice' . $row->$colId);
											else
												$editLnk = base_url('admin/services/package?fs=$sc&id=' . $row->$colId);
											?>
											<tr id="listItem_<?php echo $row->$colId; ?>">
												<td nowrap="nowrap"><i class="fa fa-sort"></i>
													&nbsp;&nbsp;<?php echo $i + 1; ?>.
												</td>
												<td nowrap="nowrap" style="padding-top:10px;"><a
														title="Edit Service" href="<?php echo $editLnk; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
												<td nowrap="nowrap"><input type="text" maxlength="255"
																		   class="form-control"
																		   name="txtPack<?php echo $i; ?>"
																		   value="<?php echo stripslashes($row->$colTitle); ?>">
												</td>
												<?php if ($fs == '0') { ?>
													<td nowrap="nowrap">
														<div class="form-group mb-0">
															<div class="form-check form-check-inline">
																<label class="form-check-label"><input type="radio" class="form-check-input" name="rdSrvcAPIType<?php echo $i; ?>" value="1" checked/>Yes</label>
															</div>
															<div class="form-check form-check-inline">
																<label class="form-check-label"><input type="radio" class="form-check-input" name="rdSrvcAPIType<?php echo $i; ?>" value="2" <?php if ($row->CronDelayTm != '1') echo 'checked'; ?> />No</label>
															</div>
														</div>
													</td>
												<?php } ?>
												<td><input type="text" maxlength="100" class="form-control"
														   name="txtTime<?php echo $i; ?>"
														   value="<?php echo stripslashes($row->$colTime); ?>"></td>
												<td><input type="text" maxlength="10" class="form-control"
														   name="txtCostPrice<?php echo $i; ?>"
														   value="<?php echo roundMe($row->CostPrice); ?>"></td>
												<td><input type="text" <?php echo $priceColStyle; ?>
														   maxlength="10" class="form-control"
														   name="txtPrice<?php echo $i; ?>"
														   value="<?php echo roundMe($row->$colPrice); ?>" size="5">
												</td>
												<?
												$pCount = 0;
												if (sizeof($PLANS) > 0) {
													foreach ($PLANS as $key => $value) {
														$strStyle = '';
														if (isset($DEFAULT_PLAN_PRICES[$key][$row->$colId])) {
															$planPrice = roundMe($DEFAULT_PLAN_PRICES[$key][$row->$colId]);
														} else {
															$planPrice = roundMe($row->$colPrice);
														}
														if ($planPrice < $row->CostPrice)
															$strStyle = ' style="color:red; font-weight:bold;"';
														?>
														<td>
															<input type="text" <?php echo $strStyle; ?> maxlength="10"
																   class="form-control"
																   name="txtPlanPrice<?php echo $pCount; ?>_<?php echo $i; ?>"
																   value="<?php echo roundMe($planPrice); ?>" size="5">
															<input type="hidden" value="<?php echo $key ?>"
																   name="hdPlanId<?php echo $pCount; ?>_<?php echo $i; ?>"/>
														</td>
														<?
														$pCount++;
													}
												}
												?>
												<td align="center"><input type="checkbox" class="chkSelect"
																		  id="chkPacks<?php echo $i; ?>"
																		  name="chkPacks[]"
																		  value="<?php echo $row->$colId . '|' . $i; ?>"/>
												</td>
											</tr>
											<?php
											$i++;
										}
									} else
										echo "<tr><td colspan='8'>" . $this->lang->line('BE_GNRL_9') . "</td></tr>";
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
						<div class="form-group row">
							<div class="col-lg-2">
									<label class="control-label"><strong>Records Per Page</strong></label>
									<select name="records" class="form-control select2me"
											data-placeholder="Select..."
											onchange="setValue('0');document.getElementById('frm').submit();">
										<?php
										$NO_OF_RECORDS = array("10" => 10, "50" => 50, "100" => 100, "200" => 200, "300" => 300, "400" => 400, "500" => 500, "1000" => 1000);
										foreach ($NO_OF_RECORDS as $key => $value) {
											$selected = '';
											if ($limit == $key)
												$selected = 'selected';
											echo '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
										}
										?>
									</select>
							</div>
							<div class="col-lg-9">
								<div class="form-group row">
									<?php
									if ($count != 0) {
										$rsPackspager = fetch_packageId_custom($colId, $tblPckName, $disableCol, $strWhereSrv, $strWhere, $colTitle);

										$totalRows = count($rsPackspager);
										if ($totalRows > $limit)
											doPages($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next);
									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
</div>
