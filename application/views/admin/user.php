<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_USR_HD'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-12">
		<?php
			$USER_ID_BTNS = $id;
			if ($USER_ID_BTNS > 0)
				include APPPATH . 'scripts/userbtns.php';
			?>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<div class="form-group row">
						<div class="col-lg-12">
							<?php if (isset($message) && $message != '') { ?>
								<div class="alert alert-success"><?php echo $message; ?></div>
							<?php } ?>
						</div>
					</div>

					<?php echo form_open(base_url("admin/clients/user?frmId=25&fTypeId=5"), 'name="frm" id="frm"'); ?>
						<div class="form-group row">
							<div class="col-lg-6">
								<div class="form-group">
									<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
									<label class="control-label"><?php echo $this->lang->line('BE_USR_1'); ?></label>
									<input type="text" placeholder="Enter Username" maxlength="50" name="txtUName"
										id="txtUName" value="<?php echo($uName); ?>" class="form-control">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('BE_LBL_345'); ?></label>
									<input type="email" placeholder="Enter Email" maxlength="50" name="txtEmail"
										id="txtEmail" value="<?php echo($email); ?>" class="form-control">
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('BE_USR_2'); ?></label>
									<input type="password" autocomplete="off" placeholder="Password" name="txtPass"
										id="txtPass" maxlength="15" class="form-control">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('BE_USR_3'); ?></label>
									<input type="password" autocomplete="off" placeholder="Confirm Password"
										name="txtCPass" id="txtCPass" maxlength="15" class="form-control">
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('BE_USR_4'); ?></label>
									<input type="text" placeholder="Enter First Name" name="txtFName" id="txtFName"
										maxlength="50" value="<?php echo($firstName); ?>" class="form-control">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('BE_USR_5'); ?></label>
									<input type="text" placeholder="Enter Last Name" maxlength="50" name="txtLName"
										id="txtLName" value="<?php echo($lastName); ?>" class="form-control">
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('BE_USR_6'); ?></label>
									<input type="text" placeholder="Enter Phone" name="txtPhone" id="txtPhone"
										maxlength="30" value="<?php echo($phone); ?>" class="form-control">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="control-label">IP</label>
									<p class="form-control-static"><?php echo $ip == '' ? '-' : $ip; ?></p>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<div class="form-group">
									<?php $currency = fetch_currency_by_disablecurrency(); ?>
									<label class="control-label"><?php echo $this->lang->line('BE_LS_9'); ?></label>
									<select name="currencyId" id="currencyId" class="form-control select2me"
											data-placeholder="<?php echo $this->lang->line('BE_LBL_271'); ?>">
										<?php FillCombo($currencyId, $currency); ?>
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<?php $price_plan = fetch_price_plan_data(); ?>
									<label class="control-label"><?php echo $this->lang->line('BE_LBL_18'); ?></label>
									<select name="planId" id="planId" class="form-control select2me"
											data-placeholder="<?php echo $this->lang->line('BE_LBL_272'); ?>">
										<option value="-1"><?php echo $this->lang->line('BE_LBL_272'); ?></option>
										<?php FillCombo($planId, $price_plan); ?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<div class="form-group">
									<?php $country = get_country_data(); ?>
									<label class="control-label"><?php echo $this->lang->line('BE_USR_7'); ?></label>
									<select name="countryId" id="countryId" class="form-control select2me"
											data-placeholder="<?php echo $this->lang->line('BE_LBL_273'); ?>">
										<?php FillCombo($countryId, $country); ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-lg-6">
									<label class="control-label">Login Countries</label>
									<select multiple="multiple" class="multi-select" id="my_multi_select2"
											name="loginCountries[]">
										<?php FillSelectedList2($id, 'ISO'); ?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('BE_USR_8'); ?></label><br/>
									<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
										<input type="checkbox" class="chkSelect" name="chkAutoFill" id="chkAutoFill" <?php echo $autoFill == 1 ? 'checked' : ''; ?> class="toggle"/><span><label for="chkAutoFill"></label></span>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label
										class="control-label"><?php echo $this->lang->line('BE_LBL_574'); ?></label><br/>
									<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
										<input type="checkbox" class="chkSelect" name="chkCanLoginFrmOther"
											id="chkCanLoginFrmOther" <?php echo $chkCanLoginFrmOther == 1 ? 'checked' : ''; ?>
											class="toggle"/><span><label for="chkCanLoginFrmOther"></label></span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('BE_LBL_625'); ?>
										:</label><br/>
									<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
										<input type="checkbox" class="chkSelect" name="chkSubscribe"
											id="chkSubscribe" <?php echo $subscribed == 1 ? 'checked' : ''; ?>
											class="toggle"/><span><label for="chkSubscribe"></label></span>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('BE_LBL_695'); ?>
										:</label><br/>
									<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
										<input type="checkbox" class="chkSelect" name="chkSMS"
											id="chkSMS" <?php echo $sendSMS == 1 ? 'checked' : ''; ?>
											class="toggle"/><span><label for="chkSMS"></label></span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('BE_LBL_329'); ?>
										:</label><br/>
									<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
										<input type="checkbox" class="chkSelect" name="chkTransfrCrdts"
											id="chkTransfrCrdts" <?php if ($transferCredits == '1') echo 'checked'; ?>
											class="toggle"/><span><label for="chkTransfrCrdts"></label></span>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="control-label">Two Step Login Verification:</label><br/>
									<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
										<input type="checkbox" class="chkSelect" name="chkLoginVerification"
											id="chkLoginVerification" <?php if ($loginVerification == '1') echo 'checked'; ?>
											class="toggle"/><span><label for="chkLoginVerification"></label></span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<div class="form-group">
									<label
										class="control-label"><?php echo $this->lang->line('BE_LBL_575'); ?></label><br/>
									<div>
										<input type="text" placeholder="Enter Pin Code" maxlength="50" name="txtPinCode"
											id="txtPinCode" value="<?php echo($pinCode); ?>" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label
										class="control-label"><?php echo $this->lang->line('BE_GNRL_1'); ?></label><br/>
									<div class="switch-button switch-button-lg">
										<input type="checkbox" name="chkDisable"
											id="chkDisable" <?php echo $disableUser == 1 ? 'checked' : ''; ?> />
										<span><label for="chkDisable"></label></span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('BE_LBL_362'); ?></label>
									<textarea class="form-control" rows="3"
											name="txtNotes"><?php echo $notes; ?></textarea>
								</div>
							</div>
							<div class="col-lg-6"
								id="dvReason" <?php if ($disableUser == '0') echo 'style="display:none;"'; ?>>
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('BE_LBL_393'); ?></label>
									<textarea class="form-control" rows="3" id="txtReason"
											name="txtReason"><?php echo $reason ?></textarea>
								</div>
							</div>
						</div>
						<?php
						$i = 1;
						foreach ($rsFields as $row) {
						   if ($i % 2 == 1){ ?>
						<div class="form-group row">
							<?php } ?>
							<div class="col-lg-6">
								<label class="control-label"><?php echo $row->FieldLabel; ?>
									:<?php if ($row->Mandatory == '1') echo '*'; ?></label><br/>
								<?php if ($row->FieldType == 'Text Box') { ?>
									<input type="text" id="fld<?php echo $i; ?>" name="fld<?php echo $i; ?>"
										value="<?php echo $USER_VALUES[$row->FieldColName]; ?>"
										placeholder="Enter <?php echo $row->FieldLabel; ?>" maxlength="50"
										value="" class="form-control">
								<?php } else if ($row->FieldType == 'Text Area') { ?>
									<textarea class="form-control" rows="3" id="fld<?php echo $i; ?>"
											name="fld<?php echo $i; ?>"><?php echo $USER_VALUES[$row->FieldColName]; ?></textarea>
								<?php } else if ($row->FieldType == 'Drop Down') { ?>
									<select name="fld<?php echo $i; ?>" id="fld<?php echo $i; ?>"
											class="form-control select2me">
										<?php
										$rsValues = get_reg_field_values($row->FieldId);

										foreach ($rsValues as $rw) {
											$slctd = '';
											if ($rw->RegValue == $USER_VALUES[$row->FieldColName])
												$slctd = 'selected';
											echo '<option value="' . $rw->RegValue . '" ' . $slctd . '>' . $rw->RegValue . '</option>';
										}
										?>
									</select>
								<?php } else if ($row->FieldType == 'Radio Button') { ?>
									<?php
									$rsValues = get_reg_field_values($row->FieldId);
									echo '<div class="col-lg-4 radio-list">';
									foreach ($rsValues as $rw) {
										$chckd = '';
										if ($rw->RegValue == $USER_VALUES[$row->FieldColName])
											$chckd = 'checked';
										?>
										<label class="radio-inline">
											<input type="radio" name="fld'.$i.'" id="fld'.$i.'"
												value="<?php echo $rw->RegValue; ?>" <?php echo $chckd; ?> /><?php echo $rw->RegValue; ?>
										</label>
										<?php
									}
									echo '</div>';
									?>
								<?php } ?>
								<?php
								echo '<input type="hidden" name="colNm' . $i . '" value="' . $row->FieldColName . '" />';
								echo '<input type="hidden" name="mndtry' . $i . '" value="' . $row->Mandatory . '" />';
								echo '<input type="hidden" name="lbl' . $i . '" value="' . $row->FieldLabel . '" />';

								echo '
															</div>';
								if ($i % 2 == 0) {
									echo '</div>';
								}
								$i++;
								}
								if ($i % 2 == 0) {
									echo '</div>';
								}
								?>
								<input type="hidden" name="totalRegFields" value="<?php echo $totalFields; ?>"/>
								<div class="form-group row">
									<div class="col-lg-12">
										<button
											type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
											class="btn btn-primary" id="btnSave"
											name="btnSave"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
										<img style="display:none;" id="statusLoader"
											src="<?php echo base_url('assets/img/loading.gif'); ?>" border="0"
											alt="Please wait..."/>
									</div>
								</div>
							</div>
						</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>


<script src="<?php echo base_url('assets/js/user.js') ?>" language="javascript"></script>
