			<!-- Page header -->
			<div class="page-header border-bottom-0">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_804'); ?></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>
					<div align="right">
						<a href="knbasecontents?frmId=<?php echo $this->input->post_get('frmId');?>&fTypeId=<? echo $this->input->post_get('fTypeId')?>" class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
					</div>

				</div>
			</div>
			<!-- /page header -->
			<!-- Content area -->
			<div class="content pt-0">
				<div class="card">
					<div class="row">
						<div class="col-12 col-sm-12 col-lg-12 col-lg-10 col-xl-10">
							<div class="card-body">
								<!-- Our Working Area Start -->
								<?php if (isset($message) && $message != '') { ?>
									<div class="form-group row">
										<div class="col-lg-12">
											<div class="alert alert-success"><?php echo $message; ?></div>

										</div>
									</div>
								<?php } ?>

                        <?php echo form_open(base_url().'admin/Miscellaneous/insertKnBaseContents?frmId=69&fTypeId=14',array("class"=>"form-horizontal","name"=>"frm","method"=>"post","id"=>"frm")); ?>
                            <input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
                                <div class="form-group row">
                                    <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_CAT_1'); ?>:<span class="required_field">*</span></label>
                                    <div class="col-lg-4">
                                        <select name="categoryId" id="categoryId" class="form-control select2me">
                                            <?php $knbasecatdata = getKnowledegeBaseCategories(); ?>
                                            <? FillCombo($categoryId, $knbasecatdata); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 control-label"><? echo $this->lang->line('BE_MR_26'); ?>:<span class="required_field">*</span></label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" placeholder="Enter Title" maxlength="100" name="txtTitle" id="txtTitle" value="<? echo($title);?>"  />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-lg-3"><? echo $this->lang->line('BE_LBL_105'); ?>:</label>
                                    <div class="col-lg-9">
                                        <textarea class="ckeditor form-control" name="txtContents" rows="6"><? echo $contents; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 control-label"><? echo $this->lang->line('BE_GNRL_1'); ?>:</label>
                                    <div class="col-lg-4">
                                        <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                                            <input type="checkbox" name="chkDisableKB" id="chkDisableKB" <? echo $disable == 1 ? 'checked' : '';?> class="toggle"/><span><label for="chkDisableKB"></label></span>
                                        </div>
                                    </div>
                                </div>
								<div class="form-group row">
									<div class="col-lg-12">

                            <div class="form-group row">
                                <div class="col-lg-3"></div><div class="col-lg-g-9">
                                    <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary green"><? echo $this->lang->line('BE_LBL_72'); ?></button>
                                </div>
                            </div>
									</div>
								</div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


<script type="text/javascript" src="<?php echo base_url('assets/plugins/ckeditor/ckeditor.js');?>"></script>
<script language="javascript" src="<?php base_url('assets/js/knowledgebase.js');?>"></script>
