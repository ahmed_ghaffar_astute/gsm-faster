<?php
    $rwLockdAmnt = fetch_codes_sum($USER_ID_BTNS); 
    $lockedAmount_IMEI = $rwLockdAmnt->LockedAmount;
    
    $rwLockdAmnt = fetch_codes_slbf($USER_ID_BTNS);
    $lockedAmount_File = $rwLockdAmnt->LockedAmount;
    
    $rwLockdAmnt = fetch_log_requests($USER_ID_BTNS);
    $lockedAmount = $lockedAmount_IMEI + $lockedAmount_File + $rwLockdAmnt->LockedAmount;

    $rwUsedAmnt = fetch_codes_by_CodeStatusId($USER_ID_BTNS);
    $usedAmntIMEI = $rwUsedAmnt->UsedAmount;
   
    $rwUsedAmnt = fetch_codes_slbf_by_CodeStatusId($USER_ID_BTNS);
    $usedAmntFile = $rwUsedAmnt->UsedAmount;
    
    $rwUsedAmnt = fetch_log_requests_by_statusid($USER_ID_BTNS);
    $usedAmount = $usedAmntIMEI + $usedAmntFile + $rwUsedAmnt->UsedAmount;
    
    $rwPartialPaid = fetch_payment_details_sum($USER_ID_BTNS);
   
    $partiallyPaidAmnt = $rwPartialPaid->Amount == '' ? 0 : $rwPartialPaid->Amount;
?>
<style>
	ul.xyz123{
		list-style-type: none;
		padding: 0;
	}
	ul.xyz123 li{
		border-bottom: 1px solid lightgray;
		padding: 5px 0;
	}
</style>
<!-- Page header -->
<div class="page-header border-bottom-0">
    <div class="page-header-content header-elements-md-inline">
        <div style="padding-top: 10px !important;" class="page-title d-flex">
            <h4>
                <i class="icon-arrow-left52 mr-2"></i>
                User Profile
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

    </div>
</div>

	<div class="form-group row">
		<div class="col-lg-12">
			<?php include APPPATH.'scripts/userbtns.php';; ?>
		</div>
	</div>
<!-- /page header -->
<!-- Page header -->

<!-- /page header -->
<div class="content pt-0">
    <div class="card">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card-body">
                    <!-- Our Working Area Start -->
                    <?php if (isset($message) && $message != '') { ?>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <div class="alert alert-success"><?php echo $message; ?></div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="ttabbable tabbable-custom boxless">
                        <ul class="nav nav-tabs nav-tabs-solid border-0">
                            <li class="active">
                                <a href="#tab_1_1" class="nav-link"  data-toggle="tab"><?php echo $this->lang->line('BE_LBL_324'); ?></a>
                            </li>
                            <li>
                                <a href="#tab_1_3" class="nav-link"   data-toggle="tab"><?php echo $this->lang->line('BE_LBL_230'); ?></a>
                            </li>
                            <li>
                                <a href="#tab_1_4" class="nav-link"  data-toggle="tab"><?php echo $this->lang->line('BE_LBL_315'); ?></a>
                            </li>
                            <li>
                                <a href="#tab_1_6" class="nav-link"  data-toggle="tab"><?php echo $this->lang->line('BE_LBL_390'); ?></a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1_1">
                                <div class="form-group row">
                                    <div class="col-lg-3">
                                        <ul class="navbar-nav">
                                            <li class="nav-item">
                                                <img src="<?php echo base_url('assets/images/profile-img.png');?>" class="img-fluid" alt=""/>
                                            </li>
                                            <li class="nav-item">
                                                <a href="JavaScript:void(0);"><?php echo $name;?></a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="JavaScript:void(0);"><?php echo $userName; ?>
                                                <span><?php echo $status == '1' ? 'In-Active' : 'Active'; ?></span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="mailto:<?php echo $userEmail; ?>"><?php echo $userEmail; ?></a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="JavaScript:void(0);"><?php echo $country; ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="from-group row">
                                            <div class="col-lg-8">
                                                <h1><?php echo $name;?></h1>
                                                <p>
                                                    <?php echo "User name of $name is <b>$userName</b>. Email address of this particular user is <b>$userEmail</b>";?>
                                                    <?
                                                    if($phone != '')
                                                        echo "Contact # is <b>$phone</b>";?>
                                                </p>
                                                <ul class="list-inline">
                                                    <li>
                                                        <i class="fa fa-map-marker"></i><?php echo $country != '' ? $country : '-'; ?>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-calendar"></i> <?php echo convertDate($dt); ?>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-dollar"></i> <?php echo $currencyAbb; ?>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-star"></i>
                                                        <?php
                                                            if($status == '1')
                                                            {
                                                                echo "<font style='color:red; font-size:16px; font-weight:bold;'>In-Active<br />
                                                                ".$reason."</font>";
                                                            }
                                                            else
                                                                echo "<font style='color:green; font-size:16px; font-weight:bold;'>Active</font>";
                                                        ?>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-star"></i>
                                                        <?php
                                                            if($API_ALLOWED == '1')
                                                                echo "<font style='color:Purple; font-size:16px; font-weight:bold;'>API Allowed<br />".$reason."</font>";
                                                            else
                                                                echo "<font style='color:red; font-size:16px; font-weight:bold;'>API Not Allowed</font>";
                                                        ?>
                                                        
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="card border-0">
                                                    <div class="caption">
                                                        <h2>Sales/Credits Report</h2>
                                                    </div>
                                                    <div class="card-body p-0">
                                                        <ul class="xyz123">
                                                            <li>
                                                                <span class="sale-info">
                                                                    Current Credits <i class="fa fa-img-up"></i>
                                                                </span>
                                                                <span class="sale-num">
                                                                    <?php echo $currency.roundMe($customerCredits); ?>
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="sale-info">
                                                                    Credits Used
                                                                </span>
                                                                <span class="sale-num">
                                                                    <?php echo $currency.roundMe($usedAmount); ?>
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="sale-info">
                                                                    Inprocess Credits
                                                                </span>
                                                                <span class="sale-num">
                                                                    <?php echo $currency.roundMe($lockedAmount); ?>
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div>

													<div class="caption">
														<h2><?php echo $this->lang->line('BE_LBL_553'); ?></h2>
													</div>
                                                    <div class="card-body p-0">
                                                        <ul class="xyz123">
                                                            <li>
                                                                <span class="sale-info">
                                                                    <?php echo $this->lang->line('BE_LBL_342'); ?> <i class="fa fa-img-down"></i>
                                                                </span>
                                                                <span class="sale-num">
                                                                    <?php echo $currency.roundMe($totalReceipts); ?>
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="sale-info">
                                                                    <?php echo $this->lang->line('BE_LBL_335'); ?>
                                                                </span>
                                                                <span class="sale-num">
                                                                    <?php echo $currency.roundMe($paidInvoices_user + $paidInvoices_admin + $partiallyPaidAmnt); ?>
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="sale-info">
                                                                    <?php echo $this->lang->line('BE_LBL_336'); ?>
                                                                </span>
                                                                <span class="sale-num">
                                                                    <?php echo $currency.roundMe($unpaidInvoices_admin - $partiallyPaidAmnt); ?>
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="sale-info">
                                                                    <?php echo $this->lang->line('BE_LBL_341'); ?>
                                                                </span>
                                                                <span class="sale-num">
                                                                    <?php echo $currency.roundMe($minCredits); ?>
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <!--end row-->
                                        <?php echo $overview_topservices; ?>
                                    </div>
                                </div>
                            </div>
                            <!--tab_1_2-->
                            <?php echo $overview_quickactions;?>
                            <div class="tab-pane" id="tab_1_4">
                                <?php echo $overview_invoices ;?>
                            </div>
                            <!--end tab-pane-->
                            <div class="tab-pane" id="tab_1_6">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <?php echo  $overview_blockedips ?>												
                                    </div>
                                </div>
                            </div>
                            <!--end tab-pane-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
   var base_url = '<?php echo base_url();?>' ;
</script>
<script language="javascript" src="<?php echo base_url();?>assets/js/overview.js?"></script>

