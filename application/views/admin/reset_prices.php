<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4>
				<i class="icon-arrow-left52 mr-2"></i> 
				<?php echo $heading; ?>
			</h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<div class="content pt-0">
	<div class="card">
		<div class="row">
            <div class="col-12 col-sm-12 col-lg-12 col-lg-10 col-xl-10">
                <div class="card-body">
                    <!-- BEGIN FORM-->
                    <?php echo form_open('#' , 'class="form-horizontal" name="frmChangePass" id="frmChangePass"');?>
                        <fieldset>
                            <div class="form-group row">
                                <?php $data = fetch_tblcat($tblCat , $strWhereCat); ?>
                                <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_219'); ?></label>
                                <div class="col-lg-4">
                                    <select name="categoryId" id="categoryId" class="form-control select2me" data-placeholder="<?php echo $this->lang->line('BE_LBL_256'); ?>">
                                        <option value="0"><?php echo $this->lang->line('BE_LBL_255'); ?></option>
                                        <?php FillCombo(0 , $data); ?>
                                    </select>
                                </div>
                                <div class="col-lg-5">
                                    <span class="form-text text-muted" style="color:white;">
                                        Choosing a category is optional. If you'll choose a category then system will only reset the prices against the selected category. If you'll not choose any category then system will reset the prices of all services
                                    </span>                                        
                                </div>
                            </div>
                            <div class="form-group row" style="display:none;" id="dvServices">
                                <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PCK_HD_4'); ?></label>
                                <div class="col-lg-4">
                                    <select id="serviceId" name="serviceId" class="form-control select2me" data-placeholder="Select...">
                                        <option value="0">Select...</option>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <span class="form-text text-muted" style="color:red;">
                                        <b>CHOOSING A SERVICE IS OPTIONAL. IF YOU'LL CHOOSE A SERVICE THEN SYSTEM WILL ONLY RESET THE PRICES AGAINST THE SELECTED SERVICE. IF YOU'LL NOT CHOOSE ANY SERVICE THEN SYSTEM WILL RESET THE PRICES OF ALL SERVICES.</b>
                                    </span>                                        
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 control-label">Reset Group Prices AS WELL:</label>
                                <div class="col-lg-4">
                                    <div class="switch-button switch-button-lg">
                                        <input type="checkbox" class="chkSelect" name="chkGroupPrices" id="chkGroupPrices" /><span><label for="chkGroupPrices"></label></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" style="display:none;" id="dvGroups">
                                <?php $price_plan=fetch_price_plan_data(); ?>
                                <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_228'); ?></label>
                                <div class="col-lg-4">
                                    <select id="groupId" name="groupId" class="form-control select2me" data-placeholder="Select...">
                                        <option value="0">Select...</option>
                                        <?php FillCombo(0, $price_plan); ?>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <span class="form-text text-muted" style="color:red;">
                                        <b>CHOOSING A PRICE GROUP IS OPTIONAL. IF YOU'LL CHOOSE A PRICE GROUP THEN SYSTEM WILL ONLY RESET THE PRICES AGAINST THE SELECTED PRICE GROUP. IF YOU'LL NOT CHOOSE ANY PRICE GROUP THEN SYSTEM WILL RESET THE PRICES OF ALL GROUPS.</b>
                                    </span>                                        
                                </div>
                            </div>
                        <fieldset>
                        <div class="form-group row">
                            <div class="col-lg-9">
                                <div class="text-right">
                                    <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" id="btnSave" name="btnSave"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
                                </div>
                                <input type="hidden" name="srvc" id="srvc" value="<?php echo $srvc; ?>" />
                                <img style="display:none;" id="statusLoader" src="<?php echo base_url('assets/images/loading.gif');?>" border="0" alt="Please wait..." />
                            </div>
                        </div>
                    <?php form_close();?>
                    <!-- END FORM-->
                </div>
    </div>
</div>



<script language="javascript" src="<?php echo base_url('assets/js/resetprices.js');?>"></script>
