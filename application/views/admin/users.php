<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_71'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>

<div class="form-group row">
	<div class="col-lg-12">
		<?php
			$USER_ID_BTNS = $userId;
			if ($USER_ID_BTNS > 0)
				include APPPATH . 'scripts/userbtns.php';
		?>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<?php echo form_open(base_url('admin/clients/?frmId=26&fTypeId=5'), 'class="horizontal-form" id="frm" name="frm"'); ?>
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group">
								<label
									class="control-label"><?php echo $this->lang->line('BE_USR_4'); ?></label>
								<input type="text" placeholder="Enter First Name" name="txtFName"
									   value="<?php echo($fName); ?>" class="form-control">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label
									class="control-label"><?php echo $this->lang->line('BE_USR_5'); ?></label>
								<input type="text" name="txtLName" placeholder="Enter Last Name"
									   value="<?php echo($lName); ?>" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group">
								<label
									class="control-label"><?php echo $this->lang->line('BE_USR_1'); ?></label>
								<input type="text" name="txtUName" placeholder="Enter Username"
									   value="<?php echo($uName); ?>" class="form-control">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label
									class="control-label"><?php echo $this->lang->line('BE_LBL_345'); ?></label>
								<input type="text" name="txtEmail" placeholder="Enter User Email"
									   value="<?php echo($email); ?>" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group">
								<?php $price_plans = fetch_price_plans_2(); ?>
								<label
									class="control-label"><?php echo $this->lang->line('BE_LBL_18'); ?></label>
								<select name="planId" id="planId" class="form-control select2me"
										data-placeholder="<?php echo $this->lang->line('BE_LBL_272'); ?>">
									<option
										value="0"><?php echo $this->lang->line('BE_LBL_272'); ?></option>
									<?php FillCombo($planId, $price_plans); ?>
								</select>
							</div>
						</div>
						<div class="col-lg-6"></div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<div>
								<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
										class="btn btn-primary"
										onclick="setValue('0');"><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
							</div>
						</div>
					</div>
				</div>

				<?php
				if ($count != 0) {
					$row = $users_row;
					$totalRows = $row->TotalRecs;
					if ($totalRows > $limit)
						doPages_DropDown($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next, 'frm');
				}
				?>
				<div class="card-body">
					<div class="form-group row">
						<div class="col-lg-12">
							<div align="right">
								<div class="btn-group">
									<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> class="btn btn-primary dropdown-toggle" data-toggle="dropdown"data-hover="dropdown" data-delay="1000" data-close-others="true"> <?php echo $this->lang->line('BE_LBL_664'); ?></button>
									<ul class="dropdown-menu" role="menu" style="text-align:left;">
										<?php
										if ($count > 0) {
											foreach ($rsGroups as $row) {
												echo '<li class="dropdown-item"><a href="' . base_url() . 'admin/clients/downloadcustomers?gId=' . $row->PricePlanId . '">' . stripslashes($row->PricePlan) . '</a></li>';
											}
										}
										?>
										<li class="dropdown-item"><a href="JavaScript:void(0);" onClick="downloadUsers();">Selected
												Clients</a></li>
										<li class="dropdown-item"><a href="<?php echo base_url('admin/clients/downloadcustomers'); ?>">All
												Clients</a></li>
									</ul>
								</div>
								<input type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									   value="<?php echo $this->lang->line('BE_LBL_309'); ?>"
									   onClick="setValue('1');" class="btn btn-primary" name="btnSubmit"/>
								<?php if (array_key_exists('326', $ARR_ADMIN_FORMS)) { ?>
									<input type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
										   value="Archive Client(s)" class="btn btn-warning"
										   onClick="deleteUsers();"/>
								<?php } ?>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">

							<div class="table-responsive">

								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th><?php echo $this->lang->line('BE_USR_1'); ?></th>
										<th><?php echo $this->lang->line('BE_LBL_345'); ?></th>
										<th><?php echo $this->lang->line('BE_USR_9'); ?></th>
										<th><?php echo $this->lang->line('BE_USR_10'); ?></th>
										<th><?php echo $this->lang->line('BE_LBL_18'); ?></th>
										<th style="text-align:center">Action</th>
										<th style="text-align:center"><input type="checkbox" class="chkSelect"
																			 id="chkSelect" name="chkSelect"
																			 onClick="selectAllChxBxs('chkSelect', 'chkUsers', <?php echo $count; ?>);">
										</th>
									</tr>
									</thead>
									<tbody>
									<?php
									$strUserIds = '0';
									if ($count != 0) {
										$i = 0;

										foreach ($rsCats as $row) {
											$strUserIds .= ',' . $row->UserId;
											$keys = crypt_key($row->UserId);
											$fontColor = '';
											$myCredits = decrypt($row->Credits, $keys);
											if ($myCredits == '')
												$myCredits = '-';
											else
												$myCredits = $row->CurrencyAbb . ' ' . roundMe($myCredits);
											if ($row->DisableUser == '1')
												$fontColor = ' style="color:grey;"';
											?>
											<tr>
												<td><strong><?php echo stripslashes($row->UserName); ?></strong></td>
												<td><?php echo($row->UserEmail); ?></td>
												<td><?php echo stripslashes($row->Name); ?></td>
												<td><?php echo $myCredits; ?></td>
												<td><?php echo $row->PricePlan == '' ? '-' : stripslashes($row->PricePlan); ?></td>
												<td style="text-align:center" valign="middle" nowrap="nowrap">
													<?php if (array_key_exists('328', $ARR_ADMIN_FORMS)) { ?>
														<a title="Login to customer account"
														   href="<?php echo base_url('admin/clients/'); ?>lgn_a_s_clint?uId=<?php echo($row->UserId); ?>"
														   target="_blank" class="btn btn-xs"
														   style="background-color:#F75462; color:#FFFFFF"><i class="fa fa-user" aria-hidden="true"></i></a>
													<?php } ?>
													<?php if (array_key_exists('327', $ARR_ADMIN_FORMS)) { ?>
														<a title="Overview"
														   href="<?php echo base_url('admin/services/'); ?>overview?id=<?php echo($row->UserId); ?>"
														   style="background-color:#AAA8A9; color:#FFFFFF"
														   class="btn btn-xs"><i class="fa icon-eye"></i></a>
													<?php } ?>
													<?php if (array_key_exists('346', $ARR_ADMIN_FORMS)) { ?>
														<a title="Edit Customer Details"
														   href="<?php echo base_url('admin/clients/'); ?>user?id=<?php echo($row->UserId); ?>"
														   class="btn btn-xs"
														   style="background-color:#89C4F5; color:#FFFFFF"><i
																class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
													<?php } ?>
													<?php if (array_key_exists('329', $ARR_ADMIN_FORMS)) { ?>
														<a title="Credit History"
														   href="<?php echo base_url('admin/clients/'); ?>credithistory?userId=<?php echo($row->UserId); ?>"
														   style="background: rgba(198, 195, 202, 0.87)" class="btn btn-xs btn-default"><i class="fa fa-usd"></i></a>
													<?php } ?>
													<?php if (array_key_exists('330', $ARR_ADMIN_FORMS)) { ?>
														<a title="Add/Rebate Credits"
														   href="<?php echo base_url('admin/clients/'); ?>addcredits?userId=<?php echo($row->UserId); ?>"
														   class="btn btn-xs btn-primary"><i class="fa fa-money"></i></a>
													<?php } ?>
													<?php if (array_key_exists('331', $ARR_ADMIN_FORMS)) { ?>
														<a title="Customer API"
														   href="<?php echo base_url('admin/clients/'); ?>userapi?userId=<?php echo($row->UserId); ?>"
														   class="btn btn-xs"
														   style="background-color:#8775A8; color:#FFFFFF"><i
																class="fa icon-cog"></i></a>
													<?php } ?>
												</td>
												<td align="center">
													<input type="checkbox" class="chkSelect"
														   id="chkUsers<?php echo $i; ?>" name="chkUsers[]"
														   value="<?php echo $row->UserId; ?>" <?php if ($row->DisableUser == '1') echo 'checked'; ?> />
												</td>
											</tr>
											<?php
											$i++;
										}
									} else
										echo "<tr><td colspan='8'>" . $this->lang->line('BE_GNRL_9') . "</td></tr>";
									if ($this->input->post_get('du') && $this->input->post_get('du') == '1')
										echo '<input type="hidden" name="du" value="1" />';
									?>
									<input type="hidden" value="0" name="cldFrm" id="cldFrm"/>
									<input type="hidden" value="<?php echo $strUserIds; ?>" name="strUserIds"/>
									<input type="hidden" id="records" name="records" value="<?php echo $limit; ?>"/>
									<input type="hidden" name="start" id="start" value="<?php echo $start; ?>"/>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function setValue(i) {
		document.getElementById('cldFrm').value = i;
	}

	function deleteUsers() {
		var strIds = validate();
		if (strIds != false) {
			if (confirm('Are you sure you want to archive the selected client(s)?'))
				window.location.href = '<?php echo base_url('admin/clients/');?>users?id=' + strIds + '&del=1&start=<?php echo $start . $txtlqry; ?>';
		}
	}

	function downloadUsers() {
		var strIds = validate();
		if (strIds != false)
			window.location.href = '<?php echo base_url('admin/clients/');?>downloadcustomers?ids=' + strIds;
	}

	function validate(cldFrm) {
		var totalUsers = <? echo $count; ?>;
		var strIds = '0';
		for (var i = 0; i < totalUsers; i++) {
			if (document.getElementById('chkUsers' + i).checked) {
				strIds += ',' + document.getElementById('chkUsers' + i).value;
			}
		}
		if (strIds == '0') {
			alert('<? echo $this->lang->line('BE_LBL_53'); ?>');
			return false;
		} else
			return strIds;
	}
</script>
