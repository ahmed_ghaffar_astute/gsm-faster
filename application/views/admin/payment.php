<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_DB_ICON4');
				if ($id > 0) echo " # $id"; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<a href="<?php echo base_url('admin/clients/'); ?>payments?frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"
			   class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>
							</div>
						</div>
					<?php } ?>
					<div class="tabbable tabbable-custom boxless">
						<ul class="nav nav-tabs nav-tabs-solid border-0">
							<li class="nav-item active">
								<a href="#tab_0" data-toggle="tab"
								   class="nav-link active"><?php echo $this->lang->line('BE_LBL_380'); ?></a>
							</li>
							<?php if ($pStatusId == '1' && $byAdmin == '1' || 1==1) { ?>
								<li class="nav-item">
									<a href="#tab_1" data-toggle="tab"
									   class="nav-link"><?php echo $this->lang->line('BE_LBL_626'); ?></a>
								</li>
							<?php } ?>
							<li class="nav-item">
								<a href="#tab_2" data-toggle="tab"
								   class="nav-link"><?php echo $this->lang->line('BE_LBL_227'); ?></a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_0">
								<?php echo $invoice_tab_general; ?>
							</div>
							<?php if ($pStatusId == '1' && $byAdmin == '1' || 1==1) { ?>
							<div class="tab-pane" id="tab_1">
								<?php echo $invoice_tab_addpmnt; ?>
							</div>
							<?php } ?>
							<div class="tab-pane" id="tab_2">
								<?php echo $invoice_tab_payments; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		<script language="javascript" src="<?php echo base_url('assets/js/languages/english.js') ?>"></script>
		<script language="javascript" src="<?php echo base_url('assets/js/payment.js') ?>"></script>
