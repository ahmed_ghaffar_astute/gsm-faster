<!-- Page header -->
<div class="page-header border-bottom-0 pt-4">
	<div class="form-group row">
		<div class="col-lg-12">
			<?php
			if ($iFrm == '1')
				include APPPATH . 'scripts/serverservices_header.php';
			else if ($iFrm == '0')
				include APPPATH . 'scripts/services_header.php';
			?>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div style="padding-top: 10px !important;" class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i>
				<?php echo $this->lang->line('BE_CAT_HD_1'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<input type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>  value="<?php echo $this->lang->line('BE_GNRL_14') . ' a ' . $this->lang->line('BE_CAT_1'); ?>"  onclick="window.location.href='<?php echo base_url('admin/services/category?id=0&' . $QUERY_STRING); ?>'" class="btn btn-primary"/>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>
							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">

							<div class="table-responsive">
								<input type="hidden" name="hw" id="hw" value="<?php echo $iFrm; ?>"/>
								<input type="hidden" name="fs" id="fs" value="<?php echo $fs; ?>"/>
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th><?php echo $this->lang->line('BE_CAT_1'); ?></th>
										<?php
										if ($iFrm == '3')
											echo '<th>&nbsp;</th>';
										?>
										<th><?php echo $this->lang->line('BE_GNRL_3'); ?></th>
										<th>Edit</th>
									</tr>
									</thead>
									<tbody id="sortableLst">
									<?php
									if ($count != 0) {
										$i = 1;
										foreach ($rsCats as $row) {
											?>
											<tr id="listItem_<?php echo $row->CategoryId; ?>">
												<td><i class="fa fa-sort"></i> &nbsp;&nbsp;<a href="<?php echo base_url('admin/services/category?id=' . $row->CategoryId . '&' . $QUERY_STRING); ?>"><?php echo stripslashes($row->Category); ?></a>
												</td>
												<?php
													if ($iFrm == '3') {
														?>
														<td style="text-align:center" valign="middle">
															<a style="text-decoration:underline" href="<?php echo base_url('admin/services/assignproducts?categoryId' . $row->CategoryId . '&category=' . $row->Category . '&' . $QUERY_STRING); ?>">Assign
																Products
															</a>
														</td>
														<?php
													}
												?>
												<td><?php echo $row->DisableCategory == '1' ? 'Yes' : 'No'; ?></td>
												<td style="text-align:center" valign="middle">
													<a href="<?php echo base_url('admin/services/category?id=' . $row->CategoryId . '&' . $QUERY_STRING); ?>">
														<i class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i>
													</a>
												</td>
											</tr>
											<?php
											$i++;
										}
									} else
										echo "<tr><td colspan='4'>" . $this->lang->line('BE_GNRL_9') . "</td></tr>";
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui-1.7.1.custom.min.js'); ?>"></script>
<script language="text/javascript" src="<?php echo base_url('assets/js/categories.js'); ?>"></script>
