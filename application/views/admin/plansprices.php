<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_18'); ?>:</b> <font ><?php echo $plan; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div class="actions">

		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>


					<?php echo form_open('' , 'name="frm" id="frm"');?>
					<input type="hidden" name="planId" value="<?php echo $planId; ?>" />
					<input type="hidden" name="sc" value="<?php echo $sc; ?>" />
					<input type="hidden" name="plan" value="<?php echo $plan; ?>" />
					<div class="form-group row">
						<div class="col-md-6">
							<div class="form-group">
								<?php $cat_data = fetch_cat_by_tblname($tblName , $strWhereCat); ?>
								<label class="control-label"><?php echo $this->lang->line('BE_LBL_219'); ?></label>
								<select name="categoryId" id="categoryId" class="form-control select2me" data-placeholder="<?php echo $this->lang->line('BE_LBL_256'); ?>" onChange="document.getElementById('frm').submit();">
									<option value="0"><?php echo $this->lang->line('BE_LBL_255'); ?></option>
									<?php FillCombo($categoryId, $cat_data); ?>
								</select>
							</div>
						</div>
						<div class="col-md-6">&nbsp;</div>
					</div>
					<?php echo form_close();?>
					<div class="orm-group row">
						<div class="col-md-12">
							<div class="block-web">
								<?php echo form_open('' ,  'class="horizontal-form" id="frm1" name="frm1"') ?>
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-bars"></i><?php echo $this->lang->line('BE_MENU_USRS_3'); ?>
										</div>
									</div>
									<div class="portlet-body">
										<div class="table-responsive">
											<?php if($count > 0) { ?>
												<table cellspacing="1" cellpadding="1" width="100%" border="0">
													<tr>
														<td width="2%">
															<input type="checkbox" class="chkSelect" name="chkCnvrtPrice" id="chkCnvrtPrice" /><span><label for="chkCnvrtPrice"></label></span>
														</td>
														<td width="50%" style="font-size:20px;">
															<b><?php echo $this->lang->line('BE_LBL_406'); ?></b>
														</td>
														<td align="right" width="48%">
															<input type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="<?php echo $this->lang->line('BE_LBL_72'); ?>" class="btn btn-primary" name="btnSubmitPrices" />
														</td>
													</tr>
												</table>
											<?php } ?>
											<table class="table table-striped table-bordered table-advance table-hover">
												<thead>
												<tr>
													<th>
														<input type="checkbox" class="chkSelect" id="chkSelect" name="chkSelect" onClick="selectAllChxBxs('chkSelect', 'chkPrice', <?php echo $count; ?>);" />
													</th>
													<th><?php echo $this->lang->line('BE_PCK_HD_4'); ?></th>
													<th><?php echo $defaultCurrencyTitle; ?></th>
													<?
													foreach ($CURR_ABV as $value)
													{
														echo "<th>$value</th>";
													}
													?>
												</tr>
												</thead>
												<tbody>
												<?
												if($count != 0)
												{
													$i = 0;
													foreach($rSet as $row )
													{
														$pckTitle = stripslashes($row->PackageTitle);
														?>
														<tr>
															<td align="center">
																<input type="checkbox" class="chkSelect" id="chkPrice<?php echo $i; ?>" name="chkPrice[]" value="<?php echo $row->PackageId.'|'.$i; ?>">
															</td>
															<td><?php echo $pckTitle ;?></td>
															<td>
																<input type="hidden" name="packageId<?php echo $i; ?>" value="<?php echo $row->PackageId; ?>" />
																<b><?php echo roundMe($row->OriginalPrice); ?></b><br />
																<input type="text" id="txtPrice<?php echo $i; ?>_0" name="txtPrice<?php echo $i; ?>_0" value="<?php echo roundMe($row->Price); ?>" class="form-control">
															</td>
															<?
															for($z = 0; $z < $currencyCount-1; $z++)
															{
																if(isset($PACK_PRICES[$row->PackageId][$CURR_IDS[$z]]))
																{
																	$currencyPrice = $PACK_PRICES[$row->PackageId][$CURR_IDS[$z]];
																}
																else
																{
																	$currencyPrice = roundMe($row->Price * $CURR_RT[$z]);
																}
																?>
																<td>
																	<b><?php echo roundMe($row->OriginalPrice * $CURR_RT[$z]); ?></b><br />
																	<input type="hidden" name="otherCurrencyId<?php echo $i; ?>_<?php echo $z+1; ?>" value="<?php echo $CURR_IDS[$z]; ?>" />
																	<input type="hidden" id="cRt<?php echo $i; ?>_<?php echo $z+1; ?>" name="cRt<?php echo $i; ?>_<?php echo $z+1; ?>" value="<?php echo $CURR_RT[$z]; ?>" />
																	<input type="text" name="txtOtherPrice<?php echo $i;?>_<?php echo $z+1; ?>" id="txtOtherPrice<?php echo $i;?>_<?php echo $z+1; ?>" value="<?php echo $currencyPrice; ?>" class="form-control">
																</td>
																<?
															}
															?>
														</tr>
														<?
														$i++;
													}
												}
												else
													echo "<tr><td colspan='7'>".$this->lang->line('BE_GNRL_9')."</td></tr>";
												?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<input type="hidden" name="categoryId" value="<?php echo $categoryId; ?>" />
								<input type="hidden" name="sc" value="<?php echo $sc; ?>" />
								<input type="hidden" value="<?php echo $count; ?>" name="totalPackages" id="totalPackages" />
								<input type="hidden" value="<?php echo $currencyCount; ?>" name="currencyCount" id="currencyCount" />
								<input type="hidden" value="<?php echo $planId; ?>" id="planId" name="planId" />
								<input type="hidden" value="<?php echo $plan; ?>" id="plan" name="plan" />
								<?php echo form_close(); ?>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>

