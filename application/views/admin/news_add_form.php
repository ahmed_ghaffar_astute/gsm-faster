<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_MENU_NEWS'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<a href="<?php echo base_url() ?>admin/Miscellaneous/viewnews?frmId=<? echo $this->input->get_post('frmId'); ?>&fTypeId=<? echo $this->input->get_post('fTypeId') ?>&type=<? echo $type; ?>"
			   class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>

					<?php echo form_open(base_url('admin/Miscellaneous/addNewsdata'), array("class" => "form-horizontal", "name" => "frm", "method" => "post", "id" => "frm", "enctype" => "multipart/form-data")); ?>
					<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
					<input type="hidden" name="type" value="<? echo $type; ?>"/>
					<input type="hidden" id="existingImage" name="existingImage" value="<?php echo $existingImage; ?>">
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_CAT_1'); ?>:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<select name="categoryId" id="categoryId" class="form-control select2me">
								<option value="0">Select...</option>
								<?php $categories = getNewsCategories(); ?>
								<? FillCombo($categoryId, $categories); ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><? echo $this->lang->line('BE_N_1'); ?>:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter Title" maxlength="100"
								   name="txtTitle" id="txtTitle" value="<? echo($newsTitle); ?>"/>
						</div>
					</div>
					<? if ($type == '1') { ?>
						<div class="form-group row">
							<label class="col-lg-3 control-label">Show News At HomePage:</label>
							<div class="col-lg-9 radio-list">
								<label class="radio-inline">
									<input type="radio" name="chkWebsite" value="2" checked/>Left Side </label>
								<label class="radio-inline">
									<input type="radio" name="chkWebsite"
										   value="3" <?php if ($atWebsite == '3') echo 'checked'; ?> />Slider
								</label>
								<label class="radio-inline">
									<input type="radio" name="chkWebsite"
										   value="4" <?php if ($atWebsite == '4') echo 'checked'; ?> />Both </label>
							</div>
						</div>
					<? } ?>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><? echo $this->lang->line('BE_N_2'); ?>:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter Short Description"
								   maxlength="255" name="txtShortDesc" id="txtShortDesc"
								   value="<? echo($shortDesc); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><? echo $this->lang->line('BE_LBL_104'); ?>:</label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter File Name" maxlength="100"
								   name="txtFileName" id="txtFileName" value="<? echo($fileName); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><? echo $this->lang->line('BE_LBL_212'); ?>:</label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter HTML Title" maxlength="100"
								   name="txtHTMLTitle" id="txtHTMLTitle" value="<? echo($htmlTitle); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><? echo $this->lang->line('BE_LBL_213'); ?>:</label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter SEO Name" maxlength="100"
								   name="txtSEOName" id="txtSEOName" value="<? echo($seoName); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label for="txtImage" class="col-lg-3 control-label"><? echo $this->lang->line('BE_N_3'); ?>
							:</label>
						<div class="col-lg-9">
							<input type="file" id="txtImage" name="txtImage">
							<p class="form-text text-muted">
								.jpg, .gif, .png
								<? if ($type == '1') echo '<br />IDEAL Size: Width = 300, Height = 200'; ?>
							</p>
							<? if ($existingImage != '') { ?>
								<p align="center" valign="middle"><img src="<? echo $existingImage ?>"/></p>
							<? } ?>

						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><? echo $this->lang->line('BE_N_4'); ?>:</label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter Image URL" maxlength="100"
								   name="txtURL" id="txtURL" value="<? echo($url); ?>"/>
							<span
								class="form-text text-muted"><? echo $this->lang->line('BE_N_6'); ?>&nbsp;http://www.anylink.com</span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><? echo $this->lang->line('BE_MR_38'); ?>:</label>
						<div class="col-lg-4">
								<textarea class="form-control" rows="3" id="txtMetaTags"
										  name="txtMetaTags"><? echo $metaTags; ?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><? echo $this->lang->line('BE_LBL_214'); ?>:</label>
						<div class="col-lg-4">
								<textarea class="form-control" rows="3" id="txtMetaKW"
										  name="txtMetaKW"><? echo $metaKW; ?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-3"><? echo $this->lang->line('BE_N_5'); ?>:</label>
						<div class="col-lg-9">
								<textarea class="ckeditor form-control" name="txtNews"
										  rows="6"><? echo $news; ?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><? echo $this->lang->line('BE_GNRL_1'); ?>:</label>
						<div class="col-lg-4">
							<div class="make-switch switch-small">
								<input type="checkbox" name="chkDisableNews" id="chkDisableNews" <? echo $disableNews == 1 ? 'checked' : ''; ?> class="toggle"/><span><label for="chkDisableNews"></label></span>
							</div>
						</div>
					</div>
					<? if ($type == '0') { ?>
						<div class="form-group row">
							<label class="col-lg-3 control-label">Show At Website:</label>
							<div class="col-lg-4">
								<div class="make-switch switch-small">
									<input type="checkbox" name="chkWebsite" id="chkWebsite" <? echo $atWebsite == 1 ? 'checked' : ''; ?> class="toggle"/><span><label for="chkWebsite"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-3 control-label">Show At IMEI Order Page:</label>
							<div class="col-lg-4">
								<div class="make-switch switch-small">
									<input type="checkbox" name="chkIMEI" id="chkIMEI" <? echo $atIMEI == 1 ? 'checked' : ''; ?> class="toggle"/><span><label for="chkIMEI"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-3 control-label">Show At File Order Page:</label>
							<div class="col-lg-4">
								<div class="make-switch switch-small">
									<input type="checkbox" name="chkFile" id="chkFile" <? echo $atFile == 1 ? 'checked' : ''; ?> class="toggle"/><span><label for="chkFile"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-3 control-label">Show At Server Order Page:</label>
							<div class="col-lg-4">
								<div class="make-switch switch-small">
									<input type="checkbox" name="chkServer" id="chkServer" <? echo $atServer == 1 ? 'checked' : ''; ?> class="toggle"/><span><label for="chkServer"></label></span>
								</div>
							</div>
						</div>
					<? } ?>
					<div class="form-group row">
						<div class="col-lg-3"></div>
						<div class="col-lg-9">
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary green"><? echo $this->lang->line('BE_LBL_72'); ?></button>
						</div>
					</div>
					<?php echo form_close(); ?>
					<!-- END FORM-->
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript" src="<?php echo base_url('assets/plugins/ckeditor/ckeditor.js'); ?>"></script>
<script language="javascript" src="<?php echo base_url('assets/js/news.js'); ?>"></script>
