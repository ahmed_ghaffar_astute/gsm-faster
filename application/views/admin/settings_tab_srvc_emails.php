<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_678'); ?></h5>
			</div>
			<div class="card-body">
	        <h3><?php echo $this->lang->line('BE_LBL_246'); ?></h3>
            <div class="form-group row">
                <label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_640'); ?>:</label>
                <div class="col-lg-4">
                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                        <input type="checkbox" name="chkIMEIOrderEmail" id="chkIMEIOrderEmail" value="1" <?php if($IMEI_ORDER_EMAIL == '1') echo 'checked';?>  class="form-input-styled"/>
                        <span><label for="chkIMEIOrderEmail"></label></span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_679'); ?>:</label>
                <div class="col-lg-4">
                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                        <input type="checkbox" name="chkIMEIScsOrderEml" id="chkIMEIScsOrderEml" value="1" <?php if($IMEI_SUCCESS_ORDER_EMAIL == '1') echo 'checked';?>  class="form-input-styled"/>
                        <span><label for="chkIMEIScsOrderEml"></label></span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_682'); ?>:</label>
                <div class="col-lg-4">
                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                        <input type="checkbox" name="chkIMEIRejOrderEml" id="chkIMEIRejOrderEml" value="1" <?php if($IMEI_FAILURE_ORDER_EMAIL == '1') echo 'checked';?>  class="form-input-styled"/>
                        <span><label for="chkIMEIRejOrderEml"></label></span>
                    </div>
                </div>
            </div>

	        <h3><?php echo $this->lang->line('BE_LBL_249'); ?></h3>
            <div class="form-group row">
                <label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_641'); ?>:</label>
                <div class="col-lg-4">
                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                        <input type="checkbox" name="chkFileOrderEmail" id="chkFileOrderEmail" value="1" <?php if($FILE_ORDER_EMAIL == '1') echo 'checked';?>  class="form-input-styled"/>
                        <span><label for="chkFileOrderEmail"></label></span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_680'); ?>:</label>
                <div class="col-lg-4">
                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                        <input type="checkbox" name="chkFileScsOrderEml" id="chkFileScsOrderEml" value="1" <?php if($FILE_SUCCESS_ORDER_EMAIL == '1') echo 'checked';?>  class="form-input-styled"/>
                        <span><label for="chkFileScsOrderEml"></label></span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_683'); ?>:</label>
                <div class="col-lg-4">
                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                        <input type="checkbox" name="chkFileRejOrderEml" id="chkFileRejOrderEml" value="1" <?php if($FILE_FAILURE_ORDER_EMAIL == '1') echo 'checked';?>  class="form-input-styled"/>
                        <span><label for="chkFileRejOrderEml"></label></span>
                    </div>
                </div>
            </div>

	        <h3><?php echo $this->lang->line('BE_LBL_252'); ?></h3>
            <div class="form-group row">
                <label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_642'); ?>:</label>
                <div class="col-lg-4">
                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                        <input type="checkbox" name="chkSrvrOrderEmail" id="chkSrvrOrderEmail" value="1" <?php if($SRVR_ORDER_EMAIL == '1') echo 'checked';?>  class="form-input-styled"/>
                        <span><label for="chkSrvrOrderEmail"></label></span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_681'); ?>:</label>
                <div class="col-lg-4">
                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                        <input type="checkbox" name="chkSrvrScsOrderEml" id="chkSrvrScsOrderEml" value="1" <?php if($SRVR_SUCCESS_ORDER_EMAIL == '1') echo 'checked';?>  class="form-input-styled"/>
                        <span><label for="chkSrvrScsOrderEml"></label></span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_684'); ?>:</label>
                <div class="col-lg-4">
                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                        <input type="checkbox" name="chkSrvrRejOrderEml" id="chkSrvrRejOrderEml" value="1" <?php if($SRVR_FAILURE_ORDER_EMAIL == '1') echo 'checked';?>  class="form-input-styled"/>
                        <span><label for="chkSrvrRejOrderEml"></label></span>
                    </div>
                </div>
            </div>
			<div class="form-group row">
				<div class="col-md-offset-4 col-md-8">
					<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> id="btnSrvcEmls" name="btnSrvcEmls" class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
					<div style="display:none;" id="dvLrdSrvEml"><img src="<?php echo base_url('assets/images/loading.gif');?>" border="0" alt="Please wait..." /></div>
				</div>
			</div>
		</div>
    </div>
</div>
</div>
