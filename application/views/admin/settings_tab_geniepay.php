<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-reorder"></i><?php echo $BE_LBL_821; ?>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-4 control-label"><?php echo $BE_LBL_822; ?>:*</label>
                <div class="col-md-2">
                <input type="text" class="form-control" placeholder="Enter Percentage" name="txtPPTrsfrFee" maxlength="5" id="txtPPTrsfrFee" value="<? echo $ppTrnsfrFee;?>" />
                </div>
                <span class="form-text text-muted">
                    % - If it will be 0 then system will pick default paypal fee, if it will not be 0 then system will pick this fee
                </span>
            </div>
			<div class="form-group">
				<div class="col-md-offset-4 col-md-8">
					<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> id="btnGeniePay" name="btnGeniePay" class="btn btn-primary"><?php echo $BE_LBL_72; ?></button>
					<div style="display:none;" id="dvLoaderGP"><img src="<?php echo base_url('assets/images/loading.gif');?>" border="0" alt="Please wait..." /></div>
				</div>
			</div>
		</div>
    </div>
</div>
