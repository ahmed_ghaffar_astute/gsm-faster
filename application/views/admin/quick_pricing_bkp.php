<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $heading; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
						<?php echo form_open(base_url('admin/services/quickpricing?frmId=6&fTypeId=2'),  'name="frmChangePass" id="frmChangePass" class="form-horizontal" ') ?>
						<div class="form-group row">
							<?php $package_data = fetch_custom_log_package_category($tblCat, $strWhereCat); ?>
							<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_219'); ?></label>
							<div class="col-lg-4">
								<select name="categoryId" id="categoryId" class="js-select2 form-control select2me"
										data-placeholder="<?php echo $this->lang->line('BE_LBL_256'); ?>">
									<option value="0"><?php echo $this->lang->line('BE_LBL_255'); ?></option>
									<?php FillCombo(0, $package_data); ?>
								</select>
							</div>
							<div class="col-lg-5">
                                        <span class="form-text text-muted" style="color:red;font-style: italic;">
                                            Selecting a category is optional. If you'll choose a category then system will only update the services prices against the selected category. If you'll not select any category then system will update all services prices.
                                        </span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_192'); ?>
								:</label>
							<div class="col-lg-4">
								<div class="form-check form-check-inline mt-0">
								<label class="form-check-label">
									<input type="radio" class="form-check-input" name="rdPrType" id="rdPrType0" value="0"
										   checked/><?php echo $this->lang->line('BE_LBL_825'); ?> </label>
								</div>
								<div class="form-check form-check-inline mt-0">
								<label class="form-check-label">
									<input type="radio" class="form-check-input" name="rdPrType" id="rdPrType1"
										   value="1"/><?php echo $this->lang->line('BE_LBL_826'); ?>
								</label>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PCK_14'); ?>
								:<span class="required_field">*</span></label>
							<div class="col-lg-4">
								<input type="text" class="form-control" placeholder="Enter Price" maxlength="50"
									   name="txtPrice" id="txtPrice"/>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-3 control-label">Pricing:</label>
							<div class="col-lg-4 radio-list">
								<div class="form-check form-check-inline mt-0">
									<label class="form-check-label"><input type="radio" class="form-check-input" name="rdPricing" id="rdPricing0" value="0" checked/>Fixed</label>
								</div>
								<div class="form-check form-check-inline mt-0">
									<label class="form-check-label"><input type="radio" class="form-check-input" name="rdPricing" id="rdPricing1" value="1"/>Percentage</label>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-3 control-label">Update Prices Of Other Currencies:</label>
							<div class="col-lg-4">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="chkOtherCurrPrices" checked id="chkOtherCurrPrices"
										   class="toggle"/><span><label for="chkOtherCurrPrices"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-3"></div>
							<div class="col-lg-9">
								<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
										class="btn btn-primary Width15" id="btnSave"
										name="btnSave"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
								<input type="hidden" name="srvc" id="srvc" value="<?php echo $srvc; ?>"/>
								<img style="display:none;" id="statusLoader"
									 src="<?php echo base_url('assets/images/loading.gif'); ?>" border="0"
									 alt="Please wait..."/>
							</div>
						</div>
				</div>

				<?php echo form_close(); ?>
			</div>
		</div>
	</div>

</div>
<script>
	var base_url = '<?php echo base_url();?>';
</script>
<script language="javascript" src="<?php echo base_url('assets/js/functions.js');?>"></script>
<script language="javascript" src="<?php echo base_url('assets/js/pricing.js'); ?>"></script>
