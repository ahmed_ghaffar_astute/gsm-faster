<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4>
                <i class="icon-arrow-left52 mr-2"></i> 
                <?php echo $heading; ?>
            </h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
                    <?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
                    <?php echo form_open('' , 'class="horizontal-form" id="frm" name="frm"');?>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <?php $data = fetch_service_category($tblName , $strWhereCat); ?>
                                    <label class="control-label"><?php echo $this->lang->line('BE_LBL_219'); ?></label>
                                    <select name="categoryId" id="categoryId" class="form-control select2me" data-placeholder="<?php  echo $this->lang->line('BE_LBL_256'); ?>" onchange="setValue('0');document.getElementById('frm').submit();">
                                        <option value="0"><?php echo $this->lang->line('BE_LBL_255'); ?></option>
                                        <?php  FillCombo($categoryId, $data); ?>
                                    </select>
                                </div>
                            </div>
                            <?php  if($count > 0) { ?>
                                <div class="col-md-12">
                                    <div class="form-group" align="right">
                                        <label class="control-label">&nbsp;</label><br />
                                        <input type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="Hide Services" onclick="setValue('1');" class="btn btn-primary" name="btnSubmit" />
                                    </div>
                                </div>
                            <?php  } ?>
                        </div>
                        <div class="form-group row">
                            <table class="table table-striped table-bordered table-advance table-hover table-responsive DisplayInlineTab">
                                <thead>
                                <tr class="bg-primary">
                                    <th><?php echo $this->lang->line('BE_PCK_13'); ?></th>
                                    <th><?php echo $this->lang->line('BE_PCK_HD_4'); ?></th>
                                    <?php  if($count > 0) { ?>
                                        <th style="text-align:center;"><input type="checkbox" class="chkSelect" id="chkSelect" name="chkSelect" onClick="selectAllChxBxs('chkSelect', 'chkPacks', <?php  echo $count; ?>);" value="true">
                                        </th>
                                    <?php  } ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $strCurrPacks = 0;
                                if($rsPacks)
                                {
                                    $i = 0;
                                    foreach($rsPacks as $row)
                                    {
                                        $strCurrPacks .= ', '.$row->PackageId;
                                    ?>
                                        <tr>
                                            <td class="highlight"><div class="success"></div>&nbsp;&nbsp;&nbsp;<?php  echo stripslashes($row->Category);?></td>
                                            <td><?php  echo stripslashes($row->PackageTitle);?></td>
                                            <td align="center">
                                                <input type="checkbox" class="chkSelect" id="chkPacks<?php  echo $i; ?>" name="chkPacks[]" value="<?php  print $row->PackageId; ?>" <?php  if($row->HideServiceAtWeb == '1') echo 'checked'; ?> />
                                            </td>
                                        </tr>
                                <?php
                                        $i++;
                                    }
                                }
                                else
                                    echo "<tr><td colspan='2'>".$this->lang->line('BE_GNRL_9')."</td></tr>";
                                ?>
                                    <input type="hidden" value="0" name="cldFrm" id="cldFrm" />
                                    <input type="hidden" value="<?php  echo $sc?>" name="sc" />
                                    <input type="hidden" value="<?php  echo $strCurrPacks; ?>" name="currPcks" />
                                </tbody>
                            </table>
                        </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function setValue(i)
    {
        document.getElementById('cldFrm').value = i;
    }
</script>
