<div class="form-group row">
	<div class="col-lg-12">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_626'); ?></h4>
		</div>
	</div>
</div>
	<?php echo form_open('', 'class="form-horizontal" name="frm1" id="frm1" '); ?>
	<div class="form-body">
			<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
			<input type="hidden" id="invAmnt" name="invAmnt" value="<?php echo($amountPayable); ?>">
				<div class="form-group row">
					<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PM_11'); ?>:</label>
					<div class="col-lg-4">
						<input type="text" class="form-control" placeholder="Enter Amount" name="txtAmount1"
							   id="txtAmount1"
							   maxlength="50" value="<?php echo($amount1); ?>"/>
					</div>
				</div>
				<div class="form-group row">
					<?php $payment_methods = fetch_payment_methods(); ?>
					<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PM_5'); ?>:<span class="required_field">*</span></label>
					<div class="col-lg-4">
						<select id="pMethodId1" name="pMethodId1" class="form-control select2me"
								data-placeholder="Select...">
							<?php FillCombo($pMethodId, $payment_methods); ?>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PM_12'); ?>:</label>
					<div class="col-lg-4">
						<input type="text" class="form-control" placeholder="Enter Transaction #"
							   name="txtTransactionId1" id="txtTransactionId1"
							   maxlength="50" value="<?php echo($transactionId1); ?>"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_GNRL'); ?>:</label>
					<div class="col-lg-4">
						<textarea class="form-control" rows="3" id="txtComments1"
								  name="txtComments1"><?php echo $comments1; ?></textarea>
					</div>
				</div>
				<div class="form-group row">
		<div class="col-lg-3"></div>
		<div class="col-lg-4">
			<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> id="btnAddPmnt" name="btnAddPmnt"
					class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
			<div style="display:none;" id="dvLoader1"><img src="<?php echo base_url('assets/images/loading.gif'); ?>"
														   border="0" alt="Please wait..."/></div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>
