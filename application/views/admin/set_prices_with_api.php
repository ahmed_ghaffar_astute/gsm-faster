<?php echo form_open('' , 'class="form-horizontal well" id="frmBPrices" name="frmBPrices"') ?>
    <div class="table-responsive big-table" style="width:800px;">
        <div style="font-size:30px;color:#2B85D8;">
            Auto Update Price - Set Additional Cost
        </div>
        <div class="portlet-body">
            <div class="table-responsive">
            	<br />
                <table class="table table-striped table-bordered table-advance table-hover" id="tblDisc">
                <tbody>
                    <tr>
                        <td><strong>Default</strong></td>
                        <td><input class="form-control" placeholder="Price Margin" maxlength="10" value="<? echo $defaultPrice;?>" type="text" id="txtPrice0" name="txtPrice0" /></td>
                        <td>
                            <select id="marginType0" name="marginType0" class="form-control select2me">
                                <option value="0" <? if($defaultMarginType == '0') echo 'selected';?> >Flat</option>
                                <option value="1" <? if($defaultMarginType == '1') echo 'selected';?> >Percentage</option>
                            </select>
                        </td>
                    </tr>
                    <?php
					$z = 1;
					foreach($rsGroups as $row)
					{
						$val = '';
						$priceVal = '';
						$marginTypeVal = '';
						if(isset($PRICES[$serviceId][$row->PricePlanId]) && $PRICES[$serviceId][$row->PricePlanId] != '')
						{
							$val = $PRICES[$serviceId][$row->PricePlanId];
							$arr = explode('|', $val);
							if(isset($arr[0]) && is_numeric($arr[0]))
								$priceVal = roundMe($arr[0]);
							if(isset($arr[1]) && is_numeric($arr[1]))
								$marginTypeVal = $arr[1];
						}						
					?>
						<tr>
							<td><strong><?php echo stripslashes($row->PricePlan);?></strong></td>
							<td>
                            	<input class="form-control" placeholder="Price Margin" maxlength="10" value="<? echo $priceVal;?>" type="text" name="txtPrice<? echo $z;?>" id="txtPrice<? echo $z;?>" />
				                <input type="hidden" name="groupId<? echo $z;?>" id="groupId<? echo $z;?>" value="<? echo $row->PricePlanId; ?>" />
                            </td>
							<td>
                                <select id="marginType<? echo $z;?>" name="marginType<? echo $z;?>" class="form-control select2me">
                                    <option value="0" <? if($marginTypeVal == '0') echo 'selected';?> >Flat</option>
                                    <option value="1" <? if($marginTypeVal == '1') echo 'selected';?> >Percentage</option>
                                </select>
							</td>
						</tr>
				<?php 	
						$z++;
					}
                    ?>			    			
                </tbody>
                </table>
            </div>
            <div align="center">
                <input type="hidden" id="serviceId" name="serviceId" value="<? echo $serviceId; ?>" />
                <input type="hidden" id="srvcAPIId" name="srvcAPIId" value="<? echo $srvcAPIId; ?>" />
                <input type="hidden" id="sType" name="sType" value="<? echo $sType; ?>" />
                <input type="hidden" id="ttlGroups" name="ttlGroups" value="<? echo $totalGroups; ?>" />
                <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" id="btnSave" name="btnSave">Submit</button>
                <div style="display:none;" id="statusLoader"><img src="<?php echo base_url('assets/images/loading.gif');?>" border="0" alt="Please wait..." /></div>
            </div>
        </div>
    </div>
<?php echo form_close() ?>


<script language="javascript" src="<?php echo base_url('assets/js/functions.js') ?>"></script>
<script language="javascript" src="<?php echo base_url('assets/js/setpriceswithapi.js') ?>"></script>
