
				<!-- Page header -->
				<div class="page-header border-bottom-0">
					<div class="page-header-content header-elements-md-inline">
						<div class="page-title d-flex">
							<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_715'); ?></h4>
						</div>
						<div align="right">
							<input type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
								   value="<?php echo $this->lang->line('BE_LBL_716'); ?>"
								   onclick="window.location.href='regfield?&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>'"
								   class="btn btn-primary"/>
						</div>						</div>
				</div>
				<!-- /page header -->
				<!-- Content area -->
				<div class="content pt-0">
					<div class="card">
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
								<div class="card-body">
									<!-- Our Working Area Start -->
									<?php if (isset($message) && $message != '') { ?>
										<div class="form-group row">
											<div class="col-lg-12">
												<div class="alert alert-success"><?php echo $message; ?></div>

											</div>
										</div>
									<?php } ?>				<div class="form-group row">
					<table class="table table-striped table-bordered table-advance table-hover">
						<thead>
						<tr class="bg-primary">
							<th><?php echo $this->lang->line('BE_PCK_17'); ?></th>
							<th><?php echo $this->lang->line('BE_PCK_19'); ?></th>
							<th><?php echo $this->lang->line('BE_LBL_717'); ?></th>
							<th><?php echo $this->lang->line('BE_GNRL_3'); ?></th>
							<th></th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						<?php
						if ($rsFields) {
							foreach ($rsFields as $row) {
								?>
								<tr>
									<td class="highlight">
										<div class="success"></div>
										<a target="_blank"
										   href="regfield?id=<?php echo $row->FieldId; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><?php echo stripslashes($row->FieldLabel); ?></a>
									</td>
									<td><?php echo $row->FieldType; ?></td>
									<td><?php echo $row->Mandatory == '1' ? 'Yes' : 'No'; ?></td>
									<td><?php echo $row->DisableField == '1' ? 'Yes' : 'No'; ?></td>
									<td style="text-align:center" valign="middle">
										<a href="regfield?id=<?php echo $row->FieldId; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><i
												class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> </a>
									</td>
									<td style="text-align:center" valign="middle">
										<?php if ($row->FieldType == 'Drop Down' || $row->FieldType == 'Radio Button') { ?>
											<a href="regfieldvalues?id=<?php echo $row->FieldId; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><i
													class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> <?php echo $this->lang->line('BE_LBL_719'); ?>
											</a>
										<?php } else echo '-'; ?>
									</td>
								</tr>
							<?php } ?>
						<?php } else { ?>
							<tr>
								<td colspan="6"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

