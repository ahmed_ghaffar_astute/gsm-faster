<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_812'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<?php echo form_open_multipart(base_url("admin/reports/creditsrpt?frmId=85&fTypeId=15")) ?>
					<div class="form-group row">
						<div class="col-lg-6">
								<label class="control-label"><? echo $this->lang->line('BE_PM_3'); ?></label>
								<input type="text" name="txtUName" value="<?php echo $userName; ?>"
									   class="form-control">
						</div>
						<div class="col-lg-6">
								<label class="control-label"><? echo $this->lang->line('BE_LBL_345'); ?></label>
								<input type="text" name="txtEmail" value="<? echo($email); ?>" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
								<label class="control-label"><? echo $this->lang->line('BE_LBL_385'); ?></label>
								<input type="text" name="txtMinCr" value="<? echo($minCr); ?>" class="form-control">
						</div>
						<div class="col-lg-6">
								<label class="control-label"><? echo $this->lang->line('BE_LBL_386'); ?></label>
								<input type="text" name="txtMaxCr" value="<? echo($maxCr); ?>" class="form-control">
						</div>
					</div>
				<div class="form-group row">
					<div class="col-lg-12">
					<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
							class="btn btn-primary"><? echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
					<input type="hidden" name="userId" id="userId" value="<? echo $userId; ?>"/>
					<input type="hidden" name="start" id="start" value="0"/>
				</div>
			</div>
<?php echo form_close(); ?>
		<div class="form-group row">
		<div class="col-lg-12">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-advance table-hover">
					<thead>
					<tr class="bg-primary">
						<th>Available Credits</th>
						<th>Credits Used</th>
						<th>Inprocess Credits</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td><? echo strip_tags($strAvCredits); ?></td>
						<td><? echo strip_tags($strCreditsUsed); ?></td>
						<td><? echo strip_tags($strIPCredits)	; ?></td>
					</tr>
					</tbody>
				</table>
				</div>
		</div>
		</div>
		<?
		if ($this->input->post_get('txtEmail') || $userId > 0) { ?>
			<div class="form-group row">
				<div class="col-lg-12">

				<div class="page-title d-flex">
						<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_812'); ?></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-lg-12">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-advance table-hover">
							<thead>
							<tr class="bg-primary">
								<th><?php echo $this->lang->line('BE_USR_1'); ?></th>
								<th><?php echo $this->lang->line('BE_USR_9'); ?></th>
								<th><? echo $this->lang->line('BE_LBL_345'); ?></th>
								<th>Available Credits</th>
								<th>Credits Used</th>
								<th>Inprocess Credits</th>
							</tr>
							</thead>
							<tbody>
							<?
							if ($rsUsers) {
								$i = 0;
								foreach ($rsUsers as $row) {
									$displayRow = false;


									crypt_key($row->UserId);
									$myCredits = decrypt($row->Credits);
									if ($myCredits == '')
										$myCredits = '0';
									else
										$myCredits = roundMe($myCredits);


									$inProcessCredits = $INPROCESS_IMEI_ARR[$row->UserId] + $INPROCESS_FILE_ARR[$row->UserId] + $INPROCESS_SRVR_ARR[$row->UserId];
									$creditsUsed = $COMPLETED_IMEI_ARR[$row->UserId] + $COMPLETED_FILE_ARR[$row->UserId] + $COMPLETED_SRVR_ARR[$row->UserId];
									if ($minCr != '' && $maxCr != '') {
										if (is_numeric($minCr) && is_numeric($maxCr)) {
											if ($myCredits >= $minCr && $myCredits <= $maxCr)
												$displayRow = true;
										}
									} else if ($minCr != '' || $maxCr != '') {
										if ($minCr != '' && is_numeric($minCr)) {
											if ($myCredits >= $minCr)
												$displayRow = true;
										}
										if ($maxCr != '' && is_numeric($maxCr)) {
											if ($myCredits <= $maxCr)
												$displayRow = true;
										}
									} else if ($minCr == '' && $maxCr == '')
										$displayRow = true;
									if ($displayRow) {
										?>
										<tr>
											<td class="highlight">
												<div class="success"></div>
												<a href="<?php echo base_url("admin/clients/users") ?>?id=<? echo $row->UserId; ?>"><? echo stripslashes($row->UserName); ?></a>
											</td>
											<td><? echo stripslashes($row->Name); ?></td>
											<td><? echo($row->UserEmail); ?></td>
											<td><? echo $row->CurrencyAbb . ' ' . $myCredits; ?></td>
											<td><? echo $row->CurrencyAbb . ' ' . roundMe($creditsUsed); ?></td>
											<td><? echo $row->CurrencyAbb . ' ' . roundMe($inProcessCredits); ?></td>
										</tr>
										<?
										$i++;
									}
								}
							} else
								echo "<tr><td colspan='8'>".$this->lang->line('BE_GNRL_9')."</td></tr>";
							if ($this->input->post_get('du') && $this->input->post_get('du') == '1')
								echo '<input type="hidden" name="du" value="1" />';
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?
		}
		?>
	</div>
</div>
</div>
</div>
</div>
