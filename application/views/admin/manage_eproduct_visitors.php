<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo 'Product Visitors'; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>

					<?php echo form_open(base_url('admin/reports/eproductvisitors?frmId=311&fTypeId=15'), array('class' => "horizontal-form", 'method' => "post")) ?>
					<!--<form action="#" class="horizontal-form" method="post">-->
					<div class="form-group row">
						<div class="col-lg-4">
							<label class="control-label">IP</label>
							<input type="text" name="txtIP" value="<?php echo($ip); ?>" class="form-control">
						</div>
						<div class="col-lg-4">
							<label class="control-label"><?php echo $this->lang->line('BE_PM_7'); ?></label><br/>
							<input class="form-control form-control-inline input-largest date-picker" type="text"
								   name="txtFromDt" value="<?php echo($dtFrom); ?>"/>
						</div>
						<div class="col-lg-4">
							<label class="control-label"><?php echo $this->lang->line('BE_PM_8'); ?></label><br/>
							<input class="form-control form-control-inline input-largest date-picker" type="text"
								   name="txtToDt" value="<?php echo($dtTo); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary"><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
						</div>
					</div>
					<?php echo form_close(); ?>
					<?
					if ($count != 0) {

						if ($totalRows > $limit)
							doPages($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next);
					}
					?>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="page-title d-flex">
								<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo 'Product Visitors'; ?></h4>
								<a href="#" class="header-elements-toggle text-default d-md-none"><i
										class="icon-more"></i></a>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th>Product Name</th>
										<th style="text-align:center" valign="middle">Total Visits</th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ($count != 0) {
										foreach ($rsIpRpt as $row) {
											?>
											<tr>
												<td><?php echo stripslashes($row->ProductName); ?></td>
												<td style="text-align:center" valign="middle">
													<a target="_blank" style="text-decoration:underline"
													   href="<?php echo base_url(); ?>admin/reports/eprodvisitdtls?pId=<?php echo($row->ProductId); ?>&pn=<?php echo stripslashes($row->ProductName); ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><?php echo $row->TotalVisits; ?></a>
												</td>
											</tr>
											<?
										}
									} else ?>
										<tr>
											<td colspan='3'>
												<?php echo $this->lang->line('BE_GNRL_9') ?>
											</td>
										</tr>;
									
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php /* include 'jscomponents.php'; */ ?>
