			<!-- Page header -->
			<div class="page-header border-bottom-0">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo 'PayPal Receiver Details'; ?></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>
					<div class="actions">
						<a href="<?php echo base_url('admin/sales/');?>paymentmethods?frmId=<?php echo $this->input->post_get('frmId');?>&fTypeId=<?php echo $this->input->post_get('fTypeId')?>" class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
					</div>

				</div>
			</div>
			<!-- /page header -->
			<!-- Content area -->
			<div class="content pt-0">
				<div class="card">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
							<div class="card-body">
								<!-- Our Working Area Start -->
								<?php if (isset($message) && $message != '') { ?>
									<div class="form-group row">
										<div class="col-lg-12">
											<div class="alert alert-success"><?php echo $message; ?></div>

										</div>
									</div>
								<?php } ?>

                        <?php echo form_open('' , 'class="form-horizontal" name="frm"  id="frm"'); ?>
                            <input type="hidden" id="id" name="id" value="<?php echo($id); ?>" />
                                <div class="form-group row">
                                    <label class="col-lg-3 control-label">PayPal ID:*</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" placeholder="Enter Mass PayPal ID" maxlength="100" name="txtUName" id="txtUName" value="<?php echo($userName);?>" required="required" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 control-label">Mass Payment Fee Type:*</label>
                                    <div class="col-lg-6 radio-list">
                                        <label class="radio-inline">
                                        <input type="radio" class="chkSelect" name="rdFeeType" value="0" checked />Fee Applied By PayPal </label>
                                        <label class="radio-inline">
                                        <input type="radio" class="chkSelect" name="rdFeeType" value="1" <?php if($feeType == '1') echo 'checked'; ?> />Fee Set In Backend </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_125'); ?>:*</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" placeholder="Enter Payment Fee" required="required" maxlength="5" name="txtFee" id="txtFee" value="<?php echo($fee);?>" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 control-label">Mass Payment Expiry:*</label>
                                    <div class="col-lg-4">
                                        <select id="massPayDays" name="massPayDays" class="form-control select2me" data-placeholder="Select...">
                                            <option value="0" <?php if($massPayDays == '0') echo 'selected';?>>Do Not Apply</option>
                                            <option value="1" <?php if($massPayDays == '1') echo 'selected';?>>1 Day</option>
                                            <option value="2" <?php if($massPayDays == '2') echo 'selected';?>>2 Days</option>
                                            <option value="3" <?php if($massPayDays == '3') echo 'selected';?>>3 Days</option>
                                            <option value="4" <?php if($massPayDays == '4') echo 'selected';?>>4 Days</option>
                                            <option value="5" <?php if($massPayDays == '5') echo 'selected';?>>5 Days</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_730'); ?>:*</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" placeholder="Enter <?php echo $this->lang->line('BE_LBL_730'); ?>" maxlength="100" name="txtAPIUN" value="<?php echo($apiUN);?>" required="required" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_731'); ?>:*</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" placeholder="Enter <?php echo $this->lang->line('BE_LBL_731'); ?>" maxlength="100" name="txtAPIPwd" value="<?php echo($apiPwd);?>" required="required" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_732'); ?>:*</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" placeholder="Enter <?php echo $this->lang->line('BE_LBL_732'); ?>" maxlength="100" name="txtAPISign" value="<?php echo($apiSign);?>" required="required" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_GNRL_1'); ?>:</label>
                                    <div class="col-lg-4">
                                        <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                                            <input type="checkbox"  name="chkDisable" id="chkDisable" <?php echo $disable == 1 ? 'checked' : '';?> class="toggle chkSelect"/><span><label for="chkDisable"></label></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-3"></div><div class="col-lg-9">
                                        <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
                                    </div>
                                </div>
                            </div>
                            
                        
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
