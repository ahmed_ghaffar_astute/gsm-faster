<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LS_9'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div class="right">
			<a href="currencies?frmId=<? echo $this->input->post_get('frmId'); ?>&fTypeId=<? echo $this->input->post_get('fTypeId') ?>"
			   class="btn btn-primary default"><span class="fa fa-angle-double-left"></span> Back To List</a>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>                <?php echo form_open(base_url(''), array('name' => 'frmChangePass', 'id' => 'frmChangePass', 'class' => 'form-horizontal')); ?>
					<div class="form-group row">
						<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LS_9'); ?>:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter Currency " maxlength="50"
								   name="txtCurrency" id="txtCurrency" value="<?php echo($currency); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_121'); ?>:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter Currency Abbreviation"
								   maxlength="15" name="txtAbb" id="txtAbb" value="<?php echo($abbr); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_MR_17'); ?>:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter Currency Symbol" maxlength="15"
								   name="txtSymbol" id="txtSymbol" value="<?php echo($symbol); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_123'); ?>:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter Conversion Rate" name="txtRate"
								   maxlength="10" id="txtRate" value="<?php echo($cRate); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_122'); ?>:</label>
						<div class="col-lg-4">

							<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
								<input type="checkbox" name="defaultCur" id="defaultCur" <?php echo $defaultCur == 1 ? 'checked' : ''; ?> class="toggle"/><span><label for="defaultCur"></label></span>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_652'); ?>:</label>
						<div class="col-lg-4">

							<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
								<input type="checkbox" name="chkRetailCurr" id="chkRetailCurr" <?php echo $retailCurr == 1 ? 'checked' : ''; ?> class="toggle"/><span><label for="chkRetailCurr"></label></span>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_GNRL_1'); ?>:</label>
						<div class="col-lg-4">
							<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
								<input type="checkbox" name="disable" id="disable" <?php echo $disable == 1 ? 'checked' : ''; ?> class="toggle"/><span><label for="disable"></label></span>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3"></div>
						<div class="col-lg-9">
							<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary" id="btnSave"
									name="btnSave"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
							<? if ($defaultCur == '0' && $id > 0) { ?>
								<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
										class="btn btn-primary" id="btnRefreshPrices" name="btnRefreshPrices">Refresh
									Currency Prices As Per New Rate
								</button>
							<? } ?>
							<img style="display:none;" id="statusLoader"
								 src="<?php echo base_url('assets/images/loading.gif'); ?>" border="0"
								 alt="Please wait..."/>
						</div>
					</div>

				</div>
			</div>
			<?php echo form_close(); ?>

		</div>
	</div>
</div>
<script src="<?php echo base_url('assets/js/currency.js') ?>"></script>
