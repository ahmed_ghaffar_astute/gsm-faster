<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                                <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_285'); ?></button>
                                <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><i class="fa fa-angle-down"></i></button>
                                <ul class="dropdown-menu" role="menu" style="text-align:left;">
                                    <li><a href="<?php echo base_url('admin/services/logrequests?codeStatusId=1&searchType=1');?>"><?php echo $this->lang->line('BE_LBL_286'); ?></a></li>
                                    <li><a href="<?php echo base_url('admin/services/logrequests?codeStatusId=1&searchType=2');?>"><?php echo $this->lang->line('BE_LBL_287'); ?></a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_288'); ?></button>
                                <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><i class="fa fa-angle-down"></i></button>
                                <ul class="dropdown-menu" role="menu" style="text-align:left;">
                                    <li><a href="<?php echo base_url('admin/services/logrequests?codeStatusId=4&searchType=3'); ?>"><?php echo $this->lang->line('BE_LBL_288'); ?></a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_291'); ?></button>
                                <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><i class="fa fa-angle-down"></i></button>
                                <ul class="dropdown-menu" role="menu" style="text-align:left;">
                                    <li><a href="<?php echo base_url('admin/services/logrequests');?>"><?php echo $this->lang->line('BE_LBL_291'); ?></a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_292'); ?></button>
                                <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><i class="fa fa-angle-down"></i></button>
                                <ul class="dropdown-menu" role="menu" style="text-align:left;">
                                    <li><a href="<?php echo base_url('admin/services/verifyserverorders');?>"><?php echo $this->lang->line('BE_LBL_292'); ?></a></li>
                                </ul>
                            </div>
                        </div>
                        <br />
                    </div>
                    <div class="col-md-12">
                        <?php if($message != '')
                            echo '<div class="alert alert-success">'.$message.'</div>'; 
                        ?>
                        <?php echo form_open(base_url('admin/services/verifyserverorders') , 'name="frm" id="frm"');?>
                            <?php include APPPATH.'scripts/serverservicessearch.php';?>
                        <?php echo form_close(); ?>
                        <?php
                        if($count != 0)
                        {
                            $row = fetch_count_log_requests_by_verify($strWhere);
                            $totalRows = $row->TotalRecs;
                            if($totalRows > $limit)
                                doPages($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next);
                        }
                        ?>
                        <?php echo form_open(base_url('admin/services/verifyserverorders') , 'id="myFrm" onSubmit="return validate();" ');?>
                            <p>
                                <div class="btn-group btn-group-sm btn-group-solid">
                                    <input type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="<?php echo $this->lang->line('BE_LBL_194'); ?>" class="btn btn-primary" name="btnSubmit" />
                                </div>
                            </p>
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-reorder"></i>
                                            <h2>
                                                <?php echo $this->lang->line('BE_LBL_183'); ?>
                                            </h2>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <input type="hidden" name="hdsbmt" id="hdsbmt" />
                                        <input type="hidden" name="uId" value="<?php echo $uId;?>" />
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        &nbsp;<?php echo $this->lang->line('BE_LBL_194'); ?>&nbsp;<input type="checkbox"  class="chkSelect" id="chkSelect1" name="chkSelect1" onClick="selectAllChxBxs('chkSelect1', 'chkReply', <?php echo $count; ?>);" value="true">
                                                    </th>
                                                        <th><?php echo $this->lang->line('BE_CODE_12'); ?></th>
                                                        <th><?php echo $this->lang->line('BE_INDEX_USRNAME'); ?></th>
                                                        <th><?php echo $this->lang->line('BE_INDEX_USRNAME'); ?></th>
                                                        <th><?php echo $this->lang->line('BE_LBL_154'); ?></th>
                                                        <th><?php echo $this->lang->line('BE_LBL_155'); ?></th>
                                                        <th><?php echo $this->lang->line('BE_CODE_7'); ?></th>
                                                        <th>Code</th>
                                                        <th>IP</th>
                                                        <th style="text-align:center"><?php echo $this->lang->line('BE_GNRL_6'); ?></th>
                                                        <th style="text-align:center">&nbsp;<?php echo $this->lang->line('BE_LBL_308'); ?>&nbsp;
                                                        <input type="checkbox"  class="chkSelect" id="chkSelect" name="chkSelect" onClick="selectAllChxBxs('chkSelect', 'chkCodes', <?php echo $count; ?>);" value="true">
                                                        </th>
                                                    </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if($count != 0)
                                            {
                                                $i = 0;
                                                foreach($rsCodes as $row)
                                                {
                                                    $dt= '-';
                                                    if($row->RequestedAt != '')
                                                        $arrDt = explode(' ', $row->RequestedAt);
                                                    if(isset($arrDt[0]))
                                                        $dt = $arrDt[0];
                                            ?>
                                                    <tr>
                                                        <td align="center">
                                                            <input type="checkbox" class="chkSelect" id="chkReply<?php echo $i; ?>" name="chkReply[]" value="<?php echo $row->LogRequestId.'|'.$row->UserId.'|'.$row->Credits.'|0|'.$i.'|'.$row->LogPackageId; ?>">
                                                        </td>
                                                            <td><?php echo stripslashes($row->LogPackageTitle); ?></td>
                                                            <td><?php echo $row->UserName; ?></td>
                                                            <td><?php echo $row->SUsername; ?></td>
                                                            <td><?php echo $row->SerialNo; ?></td>
                                                            <td><?php echo $row->BoxUserName; ?></td>
                                                            <td><?php echo $dt; ?></td>
                                                            <td>
                                                                <input type="text" class="form-control" placeholder="Enter Data" style="width:150px;" name="txtCode<?php echo $i; ?>" value="<?php echo $row->Code; ?>" />
                                                            </td>
                                                            <td><?php echo $row->VerifyIP;?></td>
                                                            <td style="text-align:center" valign="middle">
                                                                <a href="logrequest.php?id=<?php echo($row->LogRequestId);?>" ><i class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> </a>
                                                            </td>
                                                            <td align="center">
                                                                <input type="checkbox" id="chkCodes<?php echo $i; ?>" name="chkCodes[]" value="<?php echo $row->LogRequestId.'|'.$row->UserId.'|'.$row->Credits.'|0|'.$i.'|'.$row->LogPackageId; ?>">
                                                            </td>
                                                    </tr>
                                            <?php
                                                    $i++;
                                                }
                                            }
                                            else
                                                echo '<tr><td colspan="10">'.$this->lang->line('BE_GNRL_9').'</td></tr>';
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
