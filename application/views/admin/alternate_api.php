<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo 'Alternate API - '.$heading.' Service - '.$pn; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-lg-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<?php echo form_open(base_url("admin/services/alternateapi?tab=1&id=") . $id . "&sc=" . $sc . "&pn=" . $pn, 'class="form-horizontal well"  id="frm" name="frm"'); ?>
					<div class="form-group row">
						<?php $api_data = get_api_data(); ?>
						<label class="col-lg-3 control-label">Alternate API:</label>
						<div class="col-lg-8">
							<select name="altAPIId" id="altAPIId" class="form-control select2me"
									data-placeholder="<?php echo $this->lang->line('BE_LBL_278'); ?>">
								<option value="0"><?php echo $this->lang->line('BE_LBL_278'); ?></option>
								<?php FillCombo($altAPIId, $api_data); ?>
							</select><img style="display:none;" id="altImgLdr"
										  src="<?php echo base_url('assets/img/loading.gif') ?>" border="0"
										  alt="Please wait..."/>
						</div>
					</div>
					<div class="form-group row">
						<?php $supplier_services = fetch_supplier_services_by_concat($altAPIId, $sc); ?>
						<label class="col-lg-3 control-label">API Service:</label>
						<div class="col-lg-8">
							<select name="altSrvcId" id="altSrvcId" class="form-control select2me"
									data-placeholder="<?php echo $this->lang->line('BE_LBL_274'); ?>">
								<option value="0">Please choose a Service</option>
								<?php FillCombo($altServiceId, $supplier_services); ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-lg-3"></div>
					<div class="col-lg-8">
						&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> id="btnSave"
								name="btnSave"
								class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
						<input type="hidden" name="myServiceId" id="myServiceId" value="<?php echo $MY_SERVICE_ID; ?>"/>
						<input type="hidden" name="altAPISrvcId" id="altAPISrvcId"
							   value="<?php echo $altAPISrvcId; ?>"/>
						<input type="hidden" name="sc" id="sc" value="<?php echo $sc; ?>"/>
						<img style="display:none;" id="dvLoader" src="<?php echo base_url('assets/img/loading.gif') ?>"
							 border="0" alt="Please wait..."/>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

