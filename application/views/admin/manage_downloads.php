<?php $THEME=$this->data['rsStngs']->Theme; ?>
<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i>Downloads</h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div class="form-group" align="right">
			<input type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="Add New Download" onclick="window.location.href='<?php echo base_url();?>admin/Miscellaneous/download?frmId=<?php echo $this->input->post_get('frmId');?>&fTypeId=<?php echo $this->input->post_get('fTypeId');?>'" class="btn btn-primary" />
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-advance table-hover">
								<thead>
								<tr class="bg-primary">
									<th>Title</th>
									<th>File</th>
									<th><?php echo $this->lang->line('BE_GNRL_3'); ?></th>
									<th></th>
									<th></th>
								</tr>
								</thead>
								<tbody>
								<?php
								if($rsDwnlds)
								{
									foreach($rsDwnlds as $row)
									{
										?>
										<tr>
											<td><i class="fa fa-sort"></i> &nbsp;&nbsp;<a href="<?php echo base_url('admin/Miscellaneous/');?>download?id=<?php echo $row->DownloadId;?>&frmId=<?php echo $this->input->post_get('frmId');?>&fTypeId=<?php echo $this->input->post_get('fTypeId');?>"><?php echo stripslashes($row->Title);?></a></td>
											<td>
												<?php if($row->FileURL != '') { ?>
													<a href="<?php echo base_url().'uplds'.$THEME."/".$row->FileURL;?>">Download File</a>
												<?php } else echo '-'; ?>
											</td>
											<td><?php echo $row->DisableDownload == '1' ? 'Yes' : 'No';?></td>
											<td style="text-align:center" valign="middle">
												<a href="<?php echo base_url();?>admin/Miscellaneous/download?id=<?php echo $row->DownloadId;?>&frmId=<?php echo $this->input->post_get('frmId');?>&fTypeId=<?php echo $this->input->post_get('fTypeId');?>" ><i class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> </a>
											</td>
											<td style="text-align:center" valign="middle">
												<a href="<?php echo base_url();?>admin/Miscellaneous/downloads?del=1&id=<?php echo($row->DownloadId); ?>&frmId=<?php echo $this->input->post_get('frmId');?>&fTypeId=<?php echo $this->input->post_get('fTypeId')?>" onclick="return confirm('<?php echo $this->lang->line('BE_LBL_32'); ?>')"><i class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></td>
										</tr>
								<?php
									}
								}
								else
									echo "<tr><td colspan='5'>$this->lang->line('BE_GNRL_9')<td></tr>";
								?>
								</tbody>
							</table>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
