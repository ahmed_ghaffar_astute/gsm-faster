<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo 'Client Groups'; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<input type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> value="Add a Client Group"
				   onclick="window.location.href='<?php echo base_url('admin/clients/clientgroup'); ?>'"
				   class="btn btn-primary"/>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">

							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th>Group</th>
										<th>Show IMEI Prices At Web</th>
										<th>Show File Prices At Web</th>
										<th>Show Server Prices At Web</th>
										<th>Disabled</th>
										<th>Prices</th>
										<th>Orders</th>
										<th></th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ($count != 0) {
										foreach ($rsGroups as $row) {
											?>
											<tr>
												<td class="highlight">
													<div class="success"></div>
													<a style="text-decoration:underline;"
													   href="<?php echo base_url('admin/clients/'); ?>clientgroup?id=<?php echo $row->PricePlanId; ?>"><?php echo stripslashes($row->PricePlan); ?></a>
												</td>
												<td><?php echo $row->IMEIPricesAtWeb == '1' ? 'Yes' : 'No'; ?></td>
												<td><?php echo $row->FilePricesAtWeb == '1' ? 'Yes' : 'No'; ?></td>
												<td><?php echo $row->ServerPricesAtWeb == '1' ? 'Yes' : 'No'; ?></td>
												<td><?php echo $row->DisablePricePlan == '1' ? 'Yes' : 'No'; ?></td>
												<td>
													<a title="IMEI Services Prices"
													   href="<?php echo base_url('admin/clients/'); ?>plansprices?planId=<?php echo $row->PricePlanId; ?>&plan=<?php echo $row->PricePlan; ?>"
													   target="_blank" class="btn btn-xs btn-primary" ;
													   style="color:#FFFFFF"><i class="fa fa-dollar"></i></a>
													<a title="File Services Prices"
													   href="<?php echo base_url('admin/clients/'); ?>plansprices?sc=1&planId=<?php echo $row->PricePlanId; ?>&plan=<?php echo $row->PricePlan; ?>"
													   target="_blank" class="btn btn-xs"
													   style="background-color:#AAA8A9; color:#FFFFFF"><i
															class="fa fa-dollar"></i></a>
													<a title="Server Services Prices"
													   href="<?php echo base_url('admin/clients/'); ?>plansprices?sc=2&planId=<?php echo $row->PricePlanId; ?>&plan=<?php echo $row->PricePlan; ?>"
													   target="_blank" class="btn btn-xs"
													   style="background-color:#89C4F5; color:#FFFFFF"><i
															class="fa fa-dollar"></i></a>
													<a title="Product Prices"
													   href="<?php echo base_url('admin/clients/'); ?>plansprices?sc=3&planId=<?php echo $row->PricePlanId; ?>&plan=<?php echo $row->PricePlan; ?>"
													   target="_blank" class="btn btn-xs"
													   style="background-color:#8775A8; color:#FFFFFF"><i
															class="fa fa-dollar"></i></a>
												</td>
												<td>
													<a title="IMEI Orders"
													   href="<?php echo base_url('admin/services/'); ?>codes?searchType=3&bulk=1&planId=<?php echo $row->PricePlanId; ?>"
													   style="color:#FFFFFF" class="btn btn-xs btn-primary"><i
															class="fa fa-shopping-cart"></i></a>
													<a title="File Orders"
													   href="<?php echo base_url('admin/services/'); ?>codesslbf?planId=<?php echo $row->PricePlanId; ?>"
													   class="btn btn-xs"
													   style="background-color:#AAA8A9; color:#FFFFFF"><i
															class="fa fa-shopping-cart"></i></a>
													<a title="Server Orders"
													   href="<?php echo base_url('admin/services/'); ?>logrequests?planId=<?php echo $row->PricePlanId; ?>"
													   class="btn btn-xs"
													   style="background-color:#8775A8; color:#FFFFFF;"><i
															class="fa fa-shopping-cart"></i></a>
												</td>
												<td style="text-align:center" valign="middle">
													<a href="<?php echo base_url('admin/clients/'); ?>clientgroup?id=<?php echo $row->PricePlanId; ?>"><i
															class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
													<?php if (array_key_exists('325', $ARR_ADMIN_FORMS)) { ?>
														<a href="<?php echo base_url('admin/clients/'); ?>clientgroups?del=1&iFrm=<?php echo $iFrm; ?>&id=<?php echo $row->PricePlanId; ?>"
														   onclick="return confirm('<?php echo $this->lang->line('BE_LBL_32'); ?>')"><i
																class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>
													<?php } ?>
												</td>
											</tr>
										<?php } ?>
									<?php } else { ?>
										<tr>
											<td colspan="8">
												<strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
