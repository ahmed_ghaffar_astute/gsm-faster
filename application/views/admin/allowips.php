<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_866'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
				<?php echo form_open("", array('name' => 'frm', 'id' => 'frm', 'class' => 'form-horizontal')); ?>
				<div class="form-group row">
					<label class="col-md-3 control-label"><?php echo $this->lang->line('BE_LBL_152'); ?>:</label>
					<div class="col-md-4">
						<input type="text" class="form-control" placeholder="Enter IP to whitelist" maxlength="20"
							   name="txtIP" required="required"/>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-3"></div>
					<div class="col-md-9">
						<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
								class="btn btn-primary"><? echo $this->lang->line('BE_LBL_72'); ?></button>
					</div>
				</div>
				<?php echo form_close(); ?>
				<div class="form-group row">
					<div class="col-md-12">

						<table class="table table-striped table-bordered table-advance table-hover">
							<thead>
							<tr class="bg-primary">
								<th><? echo $this->lang->line('CUST_LBL_24'); ?></th>
								<th></th>
							</tr>
							</thead>
							<tbody>
							<?
							if ($rsIPs) {
								if (!file_exists(".htaccess")) fopen(".htaccess", "w+");
								$strIPs = '';
								foreach ($rsIPs as $row) {
									$ip = stripslashes(theString_Decrypt($row->IP, 'WLIP', 10));
									?>
									<tr>
										<td><? echo $ip; ?></td>
										<td style="text-align:center" valign="middle">
											<a href="allowips?del=1&id=<? echo($row->Id); ?>"
											   onclick="return confirm('<? echo $this->lang->line('BE_LBL_32'); ?>')"><i
													class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></td>
									</tr>
									<?
									/****************************************CHECK IP***********************************************/
									if ($_SERVER['HTTP_HOST'] != 'localhost') {
										createIPHtaccess("111");
									}
									/****************************************CHECK IP***********************************************/
								}
								?>
							<? } else { ?>
								<tr>
									<td colspan="2"><strong><? echo $this->lang->line('BE_GNRL_9'); ?></strong></td>
								</tr>
							<? } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
</div>

