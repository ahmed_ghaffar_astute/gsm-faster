<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title"><?php echo $this->lang->line('BE_LBL_400').' '.$this->lang->line('BE_DB_ICON2'); ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <?php if($message != '')
                        echo '<div class="alert alert-success">'.$message.'</div>'; 
                ?>
                <?php
                    if($count != 0)
                    {
                        if($totalRows > $limit)
                            doPages($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next);

                        $row = fetch_api_srv_history_by_id($packId ,$sc);
                        if(isset($row->APIId) && $row->APIId != '')
                        {
                            $lastAPIId = $row->APIId;
                            $lastAPIServiceId = $row->APIServiceId;
                        }
                    }
                ?>
               
                <?php echo form_open(base_url('admin/services/pendingorders') , 'id="myFrm"');?>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?php $api_data=get_api_data(); ?>
                                <label class="control-label"><?php echo $this->lang->line('BE_PCK_6'); ?></label>
                                <select name="apiId" id="apiId" class="form-control select2me" data-placeholder="<?php echo $this->lang->line('BE_LBL_278'); ?>">
                                    <option value="0"><?php echo $this->lang->line('BE_LBL_278'); ?></option>
                                    <?php FillCombo($lastAPIId , $api_data); ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line('BE_LBL_293'); ?></label>
                                <select name="supplierPackId" id="supplierPackId" class="form-control select2me" data-placeholder="<?php echo $this->lang->line('BE_LBL_274'); ?>">
                                    <option value="0"><?php echo $this->lang->line('BE_LBL_274'); ?></option>
                                    <?php 
                                        if($lastAPIId > 0)
                                        {
                                            $rsSrvcs = fetch_supplier_services($lastAPIId ,$sc);
                                            foreach($rsSrvcs as $row)
                                            {
                                                $selected = '';
                                                if($row->Id == $lastAPIServiceId)
                                                    $selected = 'selected';
                                                $optionVal = stripslashes($row->ServiceName);
                                                if($row->ServicePrice != '')
                                                    $optionVal .= ' - '.stripslashes($row->ServicePrice).' Credits';
                                                if($row->ServiceTime != '')
                                                    $optionVal .= ' - '.stripslashes($row->ServiceTime);
                                                echo "<option value='".$row->Id."' $selected>".$optionVal."</option>";
                                            }
                                        }
                                    ?>
                                </select><img style="display:none;" id="imgSrvcLoader" src="<?php echo base_url();?>/assets/img/loading.gif" border="0" alt="Please wait..." />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-secondary" name="btnSubmit">Apply API</button>
                        </div>
                    </div>
                    <br />
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i><h3>
                                    <?php echo $this->lang->line('BE_LBL_400').' '.$this->lang->line('BE_DB_ICON2'); ?>
                                </h3>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-responsive">
                                <input type="hidden" name="hdsbmt" id="hdsbmt" />
                                <input type="hidden" name="sc" value="<?php echo $sc; ?>" />
                                <table class="table table-striped table-bordered table-advance table-hover">
                                <thead>
                                <tr>
                                    <th style="text-align:center">
                                        <input type="checkbox" class="chkSelect" id="chkSelect1" name="chkSelect1" onClick="selectAllChxBxs('chkSelect1', 'chkOrders', <?php echo $count; ?>);" value="true">
                                    </th>
                                    <th nowrap="nowrap"><?php echo $this->lang->line('BE_LBL_589'); ?></th>
                                    <th><?php echo $this->lang->line('BE_PCK_HD_4'); ?></th>
                                    <th><?php echo $this->lang->line('BE_INDEX_USRNAME'); ?></th>
                                    <th>Job Data</th>
                                    <th><?php echo $this->lang->line('BE_USR_10'); ?></th>
                                    <th><?php echo $this->lang->line('BE_CODE_7'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($count != 0)
                                {
                                    $i = 0;
                                    foreach($rsCodes as $row)
                                    {
                                        $strCustVals = '';
                                        for($z = 1; $z < $colIndex; $z++)
                                        {
                                            $colName = 'Col'.$z;
                                            if(isset($row->$colName) && trim($row->$colName) != '')
                                            {
                                                if(isset($CUST_COL_LBL[$colName]) && $CUST_COL_LBL[$colName] != '')
                                                {
                                                    if($strCustVals != '')
                                                        $strCustVals .= '<br />';
                                                    $strCustVals .= '<b>'.$CUST_COL_LBL[$colName].':</b>';
                                                }
                                                $strCustVals .= ' '.$row->$colName;
                                            }
                                        }
                                        $lnk = base_url()."admin/services/code?id=".$row->CodeId;
                                        if($sc == '2')
                                            $lnk = base_url()."admin/services/logrequest?id=".$row->CodeId;
                                        else
                                        {
                                            $strCustVals = 'IMEI: '.isset($row->IMEINo).'<br />'.$strCustVals;
                                        }
                                        $packTitle = $row->PackageTitle;
                                    ?>
                                        <tr>
                                            <td align="center">
                                                <input type="checkbox" class="chkSelect" id="chkOrders<?php echo $i; ?>" name="chkOrders[]" value="<?php echo $row->CodeId; ?>">
                                            </td>
                                            <td class="highlight"><div class="success"></div>&nbsp;<a href="<?php echo $lnk; ?>"><?php echo $row->CodeId;?></a></td>
                                            <td><?php echo stripslashes($row->PackageTitle); ?></td>
                                            <td><a href="<?php echo base_url('admin/services/overview?id='.isset($row->UserId));?>"><?php echo $row->UserName;?></a></td>
                                            <td><?php echo $strCustVals == '' ? '-' : $strCustVals; ?></td>
                                            <td><?php echo $row->Credits;?></td>
                                            <td><?php echo $row->RequestedAt; ?></td>
                                        </tr>
                                <?php
                                        $i++;
                                    }
                                }
                                    else
                                        echo '<tr><td colspan="9">'.$this->lang->line('BE_GNRL_9').'</td></tr>';
                                ?>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>


<script language="javascript" src="<?php echo base_url('assets/js/pendingorders.js');?>"></script>