<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_596'); ?></h5>
			</div>

			<div class="card-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Home page:</label>
                    <div class="col-md-6"><div class="form-check form-check-inline">
						<div class="form-check form-check-inline mt-0">
							<label class="form-check-label mr-3"><input type="radio" name="rdHomePage" id="rdHomePage0" value="0" checked  class="form-check-input" />Unlocking </label>
						</div>
						<div class="form-check form-check-inline mt-0">
							<label class="form-check-label mr-3"><input type="radio" name="rdHomePage" id="rdHomePage1" value="1" <? if($homePage == '1') echo 'checked'; ?>  class="form-check-input" />Ecommerce </label>
						</div>
						<div class="form-check form-check-inline mt-0">
							 <label class="form-check-label"><input type="radio" name="rdHomePage" id="rdHomePage2" value="2" <? if($homePage == '2') echo 'checked'; ?>  class="form-check-input" />Retail </label>
						</div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label"><? echo $this->lang->line('BE_LBL_600'); ?>:</label>
                    <div class="col-md-4">
                        <select id="language" name="language" class="form-control select2me" data-placeholder="Select...">
                            <?
                                foreach ($LANGUAGES_ARR as $key => $value)
                                {
                                    $selected = '';
                                    if($lang == $key)
                                        $selected = 'selected';
                                    echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Google Translator At Unlock:</label>
                    <div class="col-md-4">
                        <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                            <input type="checkbox" name="chkGTU" id="chkGTU" <?php echo $gTransU == 1 ? 'checked' : '';?> class="form-input-styled" />
                            <span><label for="chkGTU"></label></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Google Translator At Shop:</label>
                    <div class="col-md-4">
                        <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                            <input type="checkbox" name="chkGTS" id="chkGTS" <?php echo $gTransS == 1 ? 'checked' : '';?> class="form-input-styled" />
                            <span><label for="chkGTS"></label></span>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="visibility:hidden;">
                    <label class="col-lg-3 col-form-label">Theme:</label>
                    <div class="col-md-4"><div class="form-check form-check-inline">
                        <label class="form-check-label">
                        <input type="radio" name="rdTheme" id="rdTheme1" value="1" checked  class="form-input-styled" /><? echo $this->lang->line('BE_LBL_107'); ?> </label>
                        <label class="form-check-label">
                        <input type="radio" name="rdTheme" id="rdTheme2" value="2" <? if($theme == '2') echo 'checked'; ?>  class="form-input-styled" /><? echo $this->lang->line('BE_LBL_108'); ?> </label>
                        <label class="form-check-label">
                        <input type="radio" name="rdTheme" id="rdTheme3" value="3" <? if($theme == '3') echo 'checked'; ?>  class="form-input-styled" /><? echo $this->lang->line('BE_LBL_109'); ?> </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> id="btnLayout" name="btnLayout" class="btn btn-primary"><? echo $this->lang->line('BE_LBL_72'); ?></button>
                        <div style="display:none;" id="dvLdLayOut"><img src="<?php echo base_url('assets/images/loading.gif');?>" border="0" alt="Please wait..." /></div>
                    </div>
                </div>
		    </div>
        </div>
    </div>
</div>
