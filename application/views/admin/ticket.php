<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo "[#$tcktNo] $subject"; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<a href="<?php echo base_url('admin/tickets/'); ?>tickets?frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"
			   class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>

					<?php echo form_open('', 'class="form-horizontal" id="frmReply" name="frmReply"'); ?>
						<div class="tabbable tabbable-custom boxless">
							<ul class="nav nav-tabs nav-tabs-solid border-0">
								<li class="nav-item active">
									<a href="#tab_0" data-toggle="tab" class="nav-link active"><?php echo $this->lang->line('BE_LBL_194'); ?></a>
								</li>
								<li class="nav-item">
									<a href="#tab_1" data-toggle="tab" class="nav-link"><?php echo $this->lang->line('BE_LBL_229'); ?></a>
								</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="tab_0">
									<?php echo $tckt_tab_reply; ?>
								</div>
								<div class="tab-pane" id="tab_1">
									<?php echo $tckt_tab_settings; ?>
								</div>
							</div>
						</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
					<div class="card-header header-elements-inline">
						<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_191'); ?></h5>
					</div>
				
					<div id="chats">
						<div class="scroller"  style="height: 435px;" data-always-visible="1" data-rail-visible1="1">
							<ul class="chats">
								<?php
								foreach ($rsTckts as $row) {
									$name = $row->Name;
									if ($name == '')
										$name = $row->ClientName;
									$class = 'out';
									if ($row->ByAdmin == '1') {
										$class = 'in';
										$name = $row->DeptName;
									}
									$dtTm = explode(' ', $row->DtTm);
									?>
									<li class="<?php echo $class; ?>">
										<img class="avatar img-responsive" alt=""
											src="<?php echo base_url(); ?>/assets/images/avatar.png"/>
										<div class="message">
									<span class="arrow">
									</span>
											<a href="#" class="name"><?php echo $name; ?></a>
											<span class="datetime">
										at <?php echo convertDate($dtTm[0]) . ' ' . $dtTm[1]; ?>
									</span>
											<span class="body">
											<?php echo stripslashes($row->Message); ?>
									</span>
										</div>
									</li>
								<?php } ?>
							</ul>
						</div>
					</div>
				
			</div>
			<!-- END PORTLET-->
		</div>
	</div>

	<script language="javascript" src="<?php echo base_url('assets/js/languages/english.js'); ?>"></script>
	<script language="javascript" src="<?php echo base_url('assets/js/ticket.js'); ?>"></script>
