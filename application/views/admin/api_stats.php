<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> API Stats<?php echo $this->lang->line('MR_HD_4'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">

							<table class="table table-striped table-bordered table-advance table-hover">
								<thead>
								<tr class="bg-primary">
									<th nowrap><? echo $this->lang->line('MR_19'); ?></th>
									<th>URL</th>
									<th nowrap>IMEI Services</th>
									<th nowrap>File Services</th>
									<th nowrap>Server Services</th>
									<th nowrap>Credits Available</th>
								</tr>
								</thead>
								<tbody>
								<?
								if ($rsAPIs) {
									foreach ($rsAPIs as $row) {
										$imeiSrvcs = 0;
										$fileSrvcs = 0;
										$srvrSrvcs = 0;
										$strStyle = '';
										$strFStyle = '';
										$strSStyle = '';

										$apiData .= ',' . $row->APIId . '|' . $row->AccountId ./*'|'.decryptAPIKey($row->APIKey, $this->data['rsStngs']->EncKeyPwd, $this->data['rsStngs']->EncKeyLen).hammad*/
											'|' . $row->APIType . '|' . $row->ServerURL . '|' . $row->APIKey2 . '|' . $row->APIPassword;
										if (isset($ARR_SRVCS[$row->APIId]) && $ARR_SRVCS[$row->APIId] != '')
											$imeiSrvcs = $ARR_SRVCS[$row->APIId];
										if (isset($ARR_F_SRVCS[$row->APIId]) && $ARR_F_SRVCS[$row->APIId] != '')
											$fileSrvcs = $ARR_F_SRVCS[$row->APIId];
										if (isset($ARR_S_SRVCS[$row->APIId]) && $ARR_S_SRVCS[$row->APIId] != '')
											$srvrSrvcs = $srvrSrvcs + $ARR_S_SRVCS[$row->APIId];

										//====================== COMPARE DEFAULT PRICES WITH COST PRICE ============================//
										if (isset($SERVICE_PRICES[$row->APIId])) {
											foreach ($SERVICE_PRICES[$row->APIId] as $key => $value) {
												if ($SERVICE_PRICES[$row->APIId][$key]['PRICE'] < $SERVICE_PRICES[$row->APIId][$key]['COST_PRICE'])
													$strStyle = ' color:red; font-weight:bold;';
											}
										}
										if (isset($FILE_SERVICE_PRICES[$row->APIId])) {
											foreach ($FILE_SERVICE_PRICES[$row->APIId] as $key => $value) {
												if ($FILE_SERVICE_PRICES[$row->APIId][$key]['PRICE'] < $FILE_SERVICE_PRICES[$row->APIId][$key]['COST_PRICE'])
													$strFStyle = ' color:red; font-weight:bold;';
											}
										}
										if (isset($SRVR_SRVC_PRICES[$row->APIId])) {
											foreach ($SRVR_SRVC_PRICES[$row->APIId] as $key => $value) {
												if ($SRVR_SRVC_PRICES[$row->APIId][$key]['PRICE'] < $SRVR_SRVC_PRICES[$row->APIId][$key]['COST_PRICE'])
													$strSStyle = ' color:red; font-weight:bold;';
											}
										}
										//====================== COMPARE DEFAULT PRICES WITH COST PRICE ============================//

										//====================== COMPARE GROUP PRICES WITH COST PRICE ============================//
										if (isset($IMEI_PLAN_PRICES[$row->APIId])) {
											foreach ($IMEI_PLAN_PRICES[$row->APIId] as $key => $value) {
												foreach ($IMEI_PLAN_PRICES[$row->APIId] as $key1 => $value1) {
													if ($IMEI_PLAN_PRICES[$row->APIId][$key][$key1]['PRICE'] < $IMEI_PLAN_PRICES[$row->APIId][$key][$key1]['COST_PRICE'])
														$strStyle = ' color:red; font-weight:bold;';
												}
											}
										}
										if (isset($FILE_PLAN_PRICES[$row->APIId])) {
											foreach ($FILE_PLAN_PRICES[$row->APIId] as $key => $value) {
												foreach ($FILE_PLAN_PRICES[$row->APIId] as $key1 => $value1) {
													if ($FILE_PLAN_PRICES[$row->APIId][$key][$key1]['PRICE'] < $FILE_PLAN_PRICES[$row->APIId][$key][$key1]['COST_PRICE'])
														$strFStyle = ' color:red; font-weight:bold;';
												}
											}
										}
										if (isset($SRVR_PLAN_PRICES[$row->APIId])) {
											foreach ($SRVR_PLAN_PRICES[$row->APIId] as $key => $value) {
												foreach ($SRVR_PLAN_PRICES[$row->APIId] as $key1 => $value1) {
													if ($SRVR_PLAN_PRICES[$row->APIId][$key][$key1]['PRICE'] < $SRVR_PLAN_PRICES[$row->APIId][$key][$key1]['COST_PRICE'])
														$strSStyle = ' color:red; font-weight:bold;';
												}
											}
										}
										//====================== COMPARE GROUP PRICES WITH COST PRICE ============================//

										?>
										<tr>
											<td class="highlight">
												<div class="success"></div>
												<a href="<?php echo base_url() ?>admin/settings/manageapi?id=<? echo $row->APIId; ?>"><? echo stripslashes($row->APITitle); ?></a>
											</td>
											<td><? echo $row->ServerURL; ?></td>
											<td align="center">
												<?
												if ($imeiSrvcs > 0)
													echo '<a href="' . base_url() . 'admin/settings/apiservices?id=' . $row->APIId . '&an=' . stripslashes($row->APITitle) . '" style="text-decoration:underline;' . $strStyle . '.">' . $imeiSrvcs . '</a>';
												else
													echo $imeiSrvcs;
												?>
											</td>
											<td align="center">
												<?
												if ($fileSrvcs > 0)
													echo '<a href="' . base_url() . 'admin/settings/apiservices?sc=1&id=' . $row->APIId . '&an=' . stripslashes($row->APITitle) . '" style="text-decoration:underline;' . $strFStyle . '.">' . $fileSrvcs . '</a>';
												else
													echo $fileSrvcs;
												?>
											</td>
											<td align="center">
												<?
												if ($srvrSrvcs > 0)
													echo '<a href="' . base_url() . 'admin/settings/apiservices?sc=2&id=' . $row->APIId . '&an=' . stripslashes($row->APITitle) . '" style="text-decoration:underline;' . $strSStyle . '.">' . $srvrSrvcs . '</a>';
												else
													echo $srvrSrvcs;
												?>
											</td>
											<td id="td_<? echo $row->APIId; ?>"></td>
										</tr>
										<?
									}
								} else { ?>
									<tr>
										<td colspan="6"><strong><? echo $this->lang->line('GNRL_9'); ?></strong></td>
									</tr>
								<? } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	var API_IDS = '<? echo $apiData;?>';
</script>
<script language="javascript" src="<?php base_url() ?>assets/js/apisrpt.js"></script>
