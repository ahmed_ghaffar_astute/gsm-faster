<?php echo form_open(base_url("admin/services/serverservice?tab=6&id=") . $id, 'class="form-horizontal well" id="frm6" name="frm6"'); ?>
<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
<div class="form-group row">
	<div class="col-lg-12">
		<h4><i class="mr-2"></i><?php echo $this->lang->line('BE_LBL_653'); ?>
		</h4>
	</div>
</div>
<div class="form-group row">
	<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_653'); ?>:</label>
	<div class="col-lg-5">
		<textarea class="form-control" rows="16" id="txtPreCodes" name="txtPreCodes"></textarea>
	</div>
</div>
<div class="form-group row">
	<label class="col-lg-3 control-label">&nbsp;</label>
	<div class="col-lg-5">
                            <span class="form-text text-muted">
                                <strong>SERIAL KEY SEPARATED BY NEW LINE.</strong>
                            </span>
	</div>
</div>
<div class="form-group row">
	<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_654'); ?>:</label>
	<div class="col-lg-4">
		<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
			<input type="checkbox" class="chkSelect" name="chkPreCodes" id="chkPreCodes"
				   class="toggle" <?php if ($calPreCodes == '1') echo 'checked'; ?> />
		</div>
	</div>
</div>
<div class="form-group row">
	<div class="col-lg-3"></div>
	<div class="col-lg-4">
		<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
				class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
	</div>
</div>
<?php echo form_close(); ?>

