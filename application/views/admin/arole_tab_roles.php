    <?php echo form_open(base_url('admin/system/adminrole?mn=10#tab_1') , ' name="frm" id="frm"') ?>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <? $form_types = fetch_form_type();?>
                    <label class="control-label"><?php echo $this->lang->line('BE_LBL_827'); ?></label>
                    <select name="formTypeId" class="form-control select2me" data-placeholder="<?php echo $this->lang->line('BE_LBL_256'); ?>" onChange="document.getElementById('frm').submit();">
                        <option value="0"><?php echo $this->lang->line('BE_LBL_623').' '.$this->lang->line('BE_LBL_827'); ?></option>
                        <?php FillCombo($formTypeId, $form_types); ?>
                    </select>
                </div>
            </div>
            <div class="col-md-6">&nbsp;</div>
            <input type="hidden" value="<?php echo $id; ?>" name="id" />
        </div>
        <!--/row-->
    <?php echo form_close(); ?>
    <div class="row">
        <div class="col-md-12">
			<?php echo form_open(base_url('admin/system/adminrole?mn=10#tab_1') , 'class="horizontal-form" name="frm1" id="frm1"') ?>
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-bars"></i><?php echo $this->lang->line('BE_LBL_828'); ?>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <?php if($count > 0) { ?>
                                <div class="row">
                                    <div class="col-md-6">&nbsp;</div>
                                    <div class="col-md-6">
                                        <div class="form-group" align="right">
                                            <input type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="<?php echo $this->lang->line('BE_LBL_72'); ?>" class="btn btn-primary" name="btnSave" />
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            <?php } ?>
                            <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr class="bg-primary">
                                <th>Form Type</th>
                                <th>Form</th>
                                <th style="text-align:center" nowrap="nowrap"><input type="checkbox" class="chkSelect" id="chkSelect" name="chkSelect" onClick="selectAllChxBxs('chkSelect', 'chkForms', <?php echo $count; ?>);" value="true"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
							if($count != 0)
							{
								$i = 0;
								foreach($rsForms as $row)
								{
									$checked = '';
									if(isset($ARR_ASGND_FORMS[$row->FormId]))
										$checked = "checked";
									$strTypeBsdFrms .= ', '.$row->FormId;
								?>
									<tr>
										<td class="highlight"><div class="success"></div><a href="javascript:void(0);"><?php echo stripslashes($row->FormType);?></a></td>
										<td><?php echo stripslashes($row->Form);?></td>
										<td style="text-align:center">
										    <input type="checkbox" class="chkSelect" id="chkForms<?php echo $i; ?>" name="chkForms[]" value="<?php echo $row->FormId; ?>" <?php echo $checked; ?> />
										</td>
									</tr>
							<?php
									$i++;
								}
							}
							else
								echo "<tr><td colspan='3'>".$this->lang->line('BE_GNRL_9')."</td></tr>";
                            ?>
                                <input type="hidden" value="<?php echo $count; ?>" name="totalForms" />
                                <input type="hidden" value="<?php echo $id; ?>" name="id" />
	                            <input type="hidden" name="formTypeId" value="<?php echo $formTypeId; ?>" />
                                <input type="hidden" value="<?php echo $strTypeBsdFrms; ?>" id="typeBsdFrms" name="typeBsdFrms" />
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <?php echo form_close(); ?>
        </div>
    </div>   
