<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $heading; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>

					<?php echo form_open(base_url('admin/reports/creditseatenrpt?frmId=89&fTypeId=15'), array('class' => "horizontal-form", 'method' => "post")); ?>
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label"><?php echo $this->lang->line('BE_CAT_1'); ?>
									&nbsp;
									<img style="display:none;" id="imgSrvcLoader"
										 src="../assets/img/loading.gif" border="0" alt="Please wait..."/>
								</label>
								<select id="categoryId" name="categoryId" class="form-control select2me"
										data-placeholder="Select...">
									<option value="0">Select...</option>
									<?php FillCombo($categoryId, $FillCombo2); ?>
								</select>

							</div>
						</div>
						<!--/span-->
						<div class="col-lg-6">
							<div class="form-group">
								<label
									class="control-label"><?php echo $this->lang->line('BE_PCK_HD_4'); ?></label>
								<select id="serviceId" name="serviceId" class="form-control select2me"
										data-placeholder="Select...">
									<option value="0">Select...</option>
									<?php FillCombo($serviceId, $FillCombo1); ?>
								</select>
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group">
								<label
									class="control-label"><?php echo $this->lang->line('BE_PM_7'); ?></label><br/>
								<input class="form-control form-control-inline input-largest date-picker"
									   type="text" name="txtFromDt" value="<?php echo($dtFrom); ?>"/>
							</div>
						</div>
						<!--/span-->
						<div class="col-lg-6">
							<div class="form-group">
								<label
									class="control-label"><?php echo $this->lang->line('BE_PM_8'); ?></label><br/>
								<input class="form-control form-control-inline input-largest date-picker"
									   type="text" name="txtToDt" value="<?php echo($dtTo); ?>"/>
							</div>
						</div>
						<!--/span-->
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary"><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
							<input type="hidden" name="sc" value="<?php echo $sc; ?>"/>
						</div>
					</div>
					<?php echo form_close(); ?>
					<?php
					if ($rsCreditsEaten) {

						$totalRows = $row->TotalRecs;
						if ($totalRows > $limit)
							doPages($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next);
					}
					?>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="page-title d-flex">
								<h4><i class="mr-2"></i> <?php echo $heading; ?></h4>
								<a href="#" class="header-elements-toggle text-default d-md-none"><i
										class="icon-more"></i></a>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th><?php echo $this->lang->line('BE_USR_1'); ?></th>
										<th><?php echo $this->lang->line('BE_USR_9'); ?></th>
										<th><?php echo $this->lang->line('BE_PCK_HD_4'); ?></th>
										<th><?php echo $this->lang->line('BE_CODE_1'); ?>
										<th><?php echo $this->lang->line('BE_USR_10'); ?></th>
										<th><?php echo $this->lang->line('BE_CODE_7'); ?></th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ($rsCreditsEaten) {
										foreach ($rsCreditsEaten as $row) {
											$dt = '-';
											if ($row->RequestedAt != '')
												$arrDt = explode(' ', $row->RequestedAt);
											if (isset($arrDt[0]))
												$dt = $arrDt[0];
											?>
											<tr>
												<td class="highlight">
													<div class="success"></div>
													<a href="javascript:void(0);"><?php echo($row->UserName); ?></a>
												</td>
												<td><?php echo $row->Name; ?></td>
												<td><?php echo stripslashes($row->$colTitle); ?></td>
												<td><?php echo $row->$displayCol; ?></td>
												<td><?php echo $row->Credits; ?></td>
												<td><?php echo $dt; ?></td>
											</tr>
											<?php
										}
									} else { ?>
										<tr>
											<td colspan='6'><?php echo $this->lang->line('BE_GNRL_9'); ?></td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<? /* include 'jscomponents.php'; */ ?>
	<script language="javascript" src="<?php echo base_url('assets/') ?>js/general.js"></script>
