<?php echo form_open(base_url("admin/services/serverservice?tab=5&id=") . $id, 'class="form-horizontal well" id="frm5" name="frm5"'); ?>
<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
<div class="form-group row">
	<div class="col-lg-12">
		<h4><i class="mr-2"></i>
			<?php echo $this->lang->line('BE_LBL_529'); ?>
		</h4>
	</div>
</div>
<div class="form-group row">
	<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_104'); ?>:</label>
	<div class="col-lg-5">
		<input type="text" class="form-control" placeholder="Enter File Name" maxlength="100" name="txtFileName"
			   id="txtFileName" value="<?php echo($fName); ?>"/>
	</div>
</div>
<div class="form-group row">
	<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_212'); ?>:</label>
	<div class="col-lg-5">
		<input type="text" class="form-control" placeholder="Enter HTML Title" maxlength="100" name="txtHTMLTitle"
			   id="txtHTMLTitle" value="<?php echo($htmlTitle); ?>"/>
	</div>
</div>
<div class="form-group row">
	<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_213'); ?>:</label>
	<div class="col-lg-5">
		<input type="text" class="form-control" placeholder="Enter SEO Name" maxlength="100" name="txtSEOName"
			   id="txtSEOName" value="<?php echo($seoName); ?>"/>
	</div>
</div>
<div class="form-group row">
	<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_MR_38'); ?>:</label>
	<div class="col-lg-5">
		<textarea class="form-control" rows="3" id="txtMetaTags" name="txtMetaTags"><?php echo $metaTags; ?></textarea>
	</div>
</div>
<div class="form-group row">
	<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_214'); ?>:</label>
	<div class="col-lg-5">
		<textarea class="form-control" rows="3" id="txtMetaKW" name="txtMetaKW"><?php echo $metaKW; ?></textarea>
	</div>
</div>
<div class="form-group row">
	<div class="col-lg-3 control-label"></div>
	<div class="col-lg-5">
		<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
				class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
	</div>
</div>
<?php echo form_close(); ?>
