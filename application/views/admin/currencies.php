<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_MENU_LS_8'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<input type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
				   value="<?php echo $this->lang->line('BE_GNRL_14') . ' ' . $this->lang->line('BE_LS_9'); ?>"
				   onclick="window.location.href='currency?&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>'"
				   class="btn btn-primary"/>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<table class="table table-striped table-bordered table-advance table-hover">
							<thead>
							<tr class="bg-primary">
								<th><?php echo $this->lang->line('BE_LS_9'); ?></th>
								<th><?php echo $this->lang->line('BE_LBL_121'); ?></th>
								<th><?php echo $this->lang->line('BE_MR_17'); ?></th>
								<th><?php echo $this->lang->line('BE_LBL_123'); ?></th>
								<th><?php echo $this->lang->line('BE_LBL_122'); ?></th>
								<th><?php echo $this->lang->line('BE_GNRL_3'); ?></th>
								<th></th>
							</tr>
							</thead>
							<tbody>
							<?php
							if ($rsCurrencies) {
								foreach ($rsCurrencies as $row) {
									?>
									<tr>
										<td class="highlight">
											<div class="success"></div>
											<a href="currency?id=<?php echo $row->CurrencyId; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><?php echo stripslashes($row->Currency); ?></a>
										</td>
										<td><?php echo $row->CurrencyAbb; ?></td>
										<td><?php echo $row->CurrencySymbol; ?></td>
										<td><?php echo $row->ConversionRate; ?></td>
										<td><?php echo $row->DefaultCurrency == '1' ? 'Yes' : 'No'; ?></td>
										<td><?php echo $row->DisableCurrency == '1' ? 'Yes' : 'No'; ?></td>
										<td style="text-align:center" valign="middle">
											<a href="currency?id=<?php echo $row->CurrencyId; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><i
													class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> </a>
										</td>
									</tr>
									<?php
								}
							} else
								echo "<tr><td colspan='7'>" . $this->lang->line('BE_GNRL_9') . "</td></tr>";
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
