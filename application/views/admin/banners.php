<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_57'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<input type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="<?php echo $this->lang->line('BE_GNRL_14').' a '.$this->lang->line('BE_LBL_58'); ?>" onclick="window.location.href='<?php echo base_url('admin/cms/')?>banner?type=<? echo $type;?>&frmId=<? echo $_REQUEST['frmId'];?>&fTypeId=<? echo $_REQUEST['fTypeId']?>'" class="btn btn-warning" />
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">

	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<!-- Copy till this to paste to other pages -->
					<div class="form-group row">
						<div class="col-lg-12">
							<table class="table table-striped table-bordered table-advance table-hover">
								<thead>
								<tr>
									<th><?php echo $this->lang->line('BE_MR_26'); ?></th>
									<th><?php echo $this->lang->line('BE_GNRL_3'); ?></th>
									<th></th>
									<th></th>
								</tr>
								</thead>
								<tbody id="sortableLst">
								<?php
								if($rsBanners)
								{
									foreach($rsBanners as $row)
									{
										?>
										<tr id="listItem_<? echo $row->BannerId; ?>">
											<td><i class="fa fa-sort"></i> &nbsp;&nbsp;<a href="<?php echo base_url('admin/cms/');?>banner?id=<?php echo $row->BannerId;?>&type=<? echo $type;?>&frmId=<? echo $this->input->post_get('frmId');?>&fTypeId=<? echo $this->input->post_get('fTypeId')?>"><?php echo stripslashes($row->BannerTitle);?></a></td>
											<td><?php echo $row->DisableBanner == '1' ? 'Yes' : 'No';?></td>
											<td style="text-align:center" valign="middle">
												<a href="<?php echo base_url('admin/cms/');?>banner?id=<?php echo $row->BannerId;?>&type=<? echo $type;?>&frmId=<? echo $this->input->post_get('frmId');?>&fTypeId=<? echo $this->input->post_get('fTypeId')?>" ><i class="icon-pencil5"></i> </a>
											</td>
											<td style="text-align:center" valign="middle">
												<a href="<?php echo base_url('admin/Cms/')?>banners?type=<? echo $type;?>&del=1&id=<?php echo($row->BannerId); ?>&type=<? echo $type;?>&frmId=<? echo $this->input->post_get('frmId');?>&fTypeId=<? echo $this->input->post_get('fTypeId')?>" onclick="return confirm('<?php echo $this->lang->line('BE_LBL_32'); ?>')"><i class="icon-trash-alt"></i></a></td>
										</tr>
									<? }?>
								<? }else{ ?>
									<tr><td colspan="4"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td></tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/')?>js/jquery-ui-1.7.1.custom.min.js"></script>
<script language="javascript" src="<?php echo base_url('assets/')?>js/banner.js?v=<?=rand_string(10);?>"></script>
