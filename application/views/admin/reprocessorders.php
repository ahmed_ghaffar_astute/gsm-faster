
<?php echo form_open(base_url('admin/services/reprocessorders') , 'id="frmAPIOrders" name="frmAPIOrders" class="form-horizontal well"') ?>
    <div class="table-responsive big-table" style="width:800px;">
        <div style="font-size:30px;color:#2B85D8;">
            <?php echo $heading.' Service - '.$pn; ?>
        </div>
        <div class="portlet-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-advance table-hover" id="tblDisc">
                <thead>
                    <tr>
                        <th nowrap="nowrap">Order #</th>
                        <th nowrap="nowrap">API</th>
                        <th>Error</th>
                        <th><input type="checkbox" class="chkSelect" id="chkSelect" name="chkSelect" onClick="selectAllChxBxs('chkSelect', 'chkOrders', <?php echo $totalOrders; ?>);" value="true"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($totalOrders != 0)
                    {
						$i = 0;
						foreach($rsOrders as $row)
						{
                    ?>
                        <tr>
                            <td>#<?php echo $row->OrderId;?></td>
                            <td><?php echo $row->OrderAPIName != '' ? stripslashes($row->OrderAPIName) : '-'; ?></td>
                            <td><?php echo $row->MessageFromServer != '' ? stripslashes($row->MessageFromServer) : '-'; ?></td>
                            <td><input type="checkbox" id="chkOrders<?php echo $i; ?>" name="chkOrders[]" value="<?php echo($row->OrderId);?>" /></td>
                        </tr>
                    <?php
                    		$i++;
						}
                    }
                    else
                    {
                        echo '<tr><td colspan="4"><strong>No Orders Found!</strong></td></tr>';
                    }
                    ?>			    			
                </tbody>
                </table>
            </div>
            <div align="center">
                <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" name="btnReprocessing">ReProcess Orders</button>
                <div style="display:none;" id="statusLoader"><img src="<?php echo base_url();?>assets/img/loading.gif" border="0" alt="Please wait..." /></div>
            </div>
        </div>
    </div>
<?php echo form_close(); ?>

<script>
$(document).ready(function()
{
	function validate()
	{
		var totalCodes = <?php echo $totalOrders; ?>;
		var strIds = '0';
		var ctrlName = 'chkOrders';
		for(var i=0; i<totalCodes; i++)
		{
			if(document.getElementById(ctrlName+i).checked)
			{
				strIds += ',' + document.getElementById(ctrlName+i).value;
			}
		}
		if(strIds == '0')
		{
			alert('Plesae select at least one order.');
			return false;
		}
		return true;
	}

	$("#frmAPIOrders").submit(function(){
		return validate();
	});
	
});
</script>
