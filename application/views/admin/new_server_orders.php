<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <h2>New Server Orders</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i>New Server Orders
                        </div>
                        <div class="actions">
                            <a href="<?php echo base_url('admin/services/newserverorders?frmId=107&fTypeId=18');?>" class="btn btn-primary"><span class="fa fa-refresh"></span> Refresh</a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-advance table-hover">
                                <thead>
                                    <tr>
                                        <th><?php echo $this->lang->line('BE_PCK_HD_4'); ?></th>
                                        <th><?php echo $this->lang->line('BE_PCK_15'); ?></th>
                                        <th><?php echo $this->lang->line('BE_MR_4'); ?></th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            <tbody>
                                <?php
                                    if($count != 0){
                                        foreach($rsNewOrders as $row)
                                        {
                                            $apiError = '';
                                            if(isset($ARR_API_ERRORS[$row->LogPackageId]) && $ARR_API_ERRORS[$row->LogPackageId] != '')
                                                $apiError = '<a href="'.base_url('admin/services/newserverorders?rp=1&pkId='.$row->LogPackageId).'"><img src="'.base_url().'assets/images/red-flags-icon-png-7.png" width="20" height="20" title="Order Did not go through API - '.$ARR_API_ERRORS[$row->LogPackageId].'. You can click it to resubmit all orders to API." /></a>';
                                ?>
                                            <tr>
                                                <td><?php echo stripslashes($row->LogPackageTitle);?> <?php echo $apiError;?></td>
                                                <td><?php echo $row->APITitle == '' ? '-' : stripslashes($row->APITitle); ?></td>
                                                <td><?php echo $row->TotalOrders;?></td>
                                                <td valign="middle" nowrap="nowrap">
                                                    <a title="Take Action" href="<?php echo base_url('admin/services/logrequests?packId='.$row->LogPackageId.'&codeStatusId=1&searchType=1&txtFromDt='.$dt.'&oB=ASC');?>"><i class="fa fa-search-plus" style="font-size:16px;"></i></a>
                                                    <?php if($row->APITitle == '') { ?>
                                                        &nbsp;&nbsp;<a title="Apply API" href="<?php echo base_url('admin/services/logrequests?packId='.$row->LogPackageId.'&applyAPI=1&codeStatusId=1&searchType=1&txtFromDt='.$dt.'&oB=ASC');?>"><i class="fa icon-cog" style="font-size:16px;"></i></a>
                                                    <?php } ?>
                                                    <?php if(isset($ARR_API_ERRORS[$row->LogPackageId]) && sizeof($ARR_API_ERRORS[$row->LogPackageId]) > 0 && $row->APITitle != '') { ?>
                                                        &nbsp;&nbsp;<a title="Check API Errors and Reprocess Orders" href="<?php echo base_url('admin/services/reprocessorders?id='.$row->LogPackageId.'&sc=2&pn='.str_replace(' ', 'S_P_', stripslashes($row->LogPackageTitle)));?>" class="fancybox fancybox.ajax"><i class="fa fa-refresh" style="font-size:16px;"></i></a>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php }?>
                                    <?php }else{ ?>
                                        <tr><td colspan="4"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td></tr>
                                    <?php } ?>			    			
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
