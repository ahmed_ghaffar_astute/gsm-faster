<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_390'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-sm-12 col-lg-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="page-title d-flex">
								<h4><i class="mr-2"></i> <?php echo $this->lang->line('BE_LBL_391'); ?></h4>
								<a href="#" class="header-elements-toggle text-default d-md-none"><i
										class="icon-more"></i></a>
							</div>
						</div>
					</div>
					<?php echo form_open(base_url('admin/reports/blockedipsrpt?frmId=95&fTypeId=15'), array('class' => "horizontal-form", 'method' => "post")); ?>
					<input type="hidden" id="id" name="id" value="<? echo($id); ?>">
					<div class="form-group row">
						<label class="control-label col-lg-3">IP(s):<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<textarea class="form-control" name="txtIPs" rows="6"></textarea>
							<span class="form-text text-muted">Seperated By Commas</span>
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-lg-3"><? echo $this->lang->line('BE_GNRL'); ?>:</label>
						<div class="col-lg-4">
							<textarea class="form-control" name="txtComments" rows="3"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3"></div>
						<div class="col-lg-4">
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary"><? echo $this->lang->line('BE_LBL_72'); ?></button>
						</div>
					</div>
					<?php echo form_close(); ?>
					<?php echo form_open(base_url('admin/reports/blockedipsrpt?frmId=95&fTypeId=15'), array('class' => "horizontal-form", 'method' => "post")); ?>
					<div class="form-group row">
						<div class="col-lg-6">
							<label class="control-label"><? echo $this->lang->line('BE_PM_3'); ?></label>
							<input type="text" name="txtUName" value="<? echo($uName); ?>"
								   class="form-control">
						</div>
						<!--/span-->
						<div class="col-lg-6">
							<label class="control-label">IP</label>
							<input type="text" name="txtIP" value="<? echo($ip); ?>" class="form-control">
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="form-group row">
						<div class="col-lg-6">
							<label
								class="control-label"><? echo $this->lang->line('BE_PM_7'); ?></label><br/>
							<input class="form-control form-control-inline input-largest date-picker"
								   type="text" name="txtFromDt" value="<? echo($dtFrom); ?>"/>
						</div>
						<!--/span-->
						<div class="col-lg-6">
							<label
								class="control-label"><? echo $this->lang->line('BE_PM_8'); ?></label><br/>
							<input class="form-control form-control-inline input-largest date-picker"
								   type="text" name="txtToDt" value="<? echo($dtTo); ?>"/>
						</div>
						<!--/span-->
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary"><? echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
							<input type="hidden" name="cldFrm" id="cldFrm" value="0"/>
							<input type="hidden" name="byAdmin" id="byAdmin" value="<? echo $byAdmin; ?>"/>
							<input type="hidden" name="userId" id="userId" value="<? echo $userId; ?>"/>
						</div>
					</div>

					<?php echo form_close(); ?>
					<?
					if ($count != 0) {

						//$totalRows = $row->TotalRecs;
						$totalRows = 0;
						if ($totalRows > $limit)
							doPages($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next);
					}
					?>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="page-title d-flex">
								<h3><i class="mr-2"></i> <?php echo $this->lang->line('BE_LBL_390'); ?></h3>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th><?php echo $this->lang->line('BE_LBL_43'); ?></th>
										<th><?php echo $this->lang->line('BE_INDEX_USRNAME'); ?></th>
										<th><?php echo $this->lang->line('BE_LBL_345'); ?></th>
										<th><?php echo $this->lang->line('BE_USR_9'); ?></th>
										<th><?php echo $this->lang->line('BE_CR_3'); ?></th>
										<th><?php echo $this->lang->line('BE_GNRL'); ?></th>
										<th></th>
									</tr>
									</thead>
									<tbody>
									<?
									if ($count != 0) {
										$i = 0;
										foreach ($rsCats as $row) {
											?>
											<tr>
												<td class="highlight">
													<div class="success"></div>
													<a href="javascript:void(0);"><? echo($row->IP); ?></a></td>
												<td><?php echo $row->UserName == '' ? '-' : $row->UserName; ?></td>
												<td><?php echo $row->UserEmail == '' ? '-' : $row->UserEmail; ?></td>
												<td><?php echo $row->Name == '' ? '-' : $row->Name; ?></td>
												<td><?php echo $row->DtTm; ?></td>
												<td><?php echo stripslashes($row->Comments); ?></td>

												<td style="text-align:center" valign="middle">
													<a href="<?php echo base_url('admin/reports/') ?>blockedipsrpt?del=1&uId=<?php echo($row->UserId); ?>&id=<?php echo ($row->Id) . $txtlqry; ?>"
													   onclick="return confirm('<?php echo $this->lang->line('BE_LBL_32'); ?>')"><i
															class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>
												</td>
											</tr>
											<?
											$i++;
										}
									} else {
										?>
										<tr>
											<td colspan='7'><?php echo $this->lang->line('BE_GNRL_9'); ?></td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
