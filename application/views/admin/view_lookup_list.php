<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $strLabel; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<?php if($iFrm != '1' && $iFrm != '2' && $iFrm != '3' && $iFrm != '4' && $iFrm != '7' && $iFrm != '8' && $iFrm != '9' && $iFrm != '12' && $iFrm != '14') { ?>
				<input type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="<? echo $this->lang->line('BE_GNRL_14').' a '.$strLabel; ?>" onclick="window.location.href=baseurl+'admin/Settings/lookuplist?iFrm=<? echo $iFrm; ?>&service=<? echo $service; ?>&servtbl=<? echo $tbl; ?>'" class="btn btn-primary" />
			<?php } ?>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-md-12">
							<?php if ($message != '')
								echo '<div class="alert alert-success">' . $message . '</div>'; ?>
							<table class="table table-striped table-bordered table-advance table-hover">
								<thead>
								<tr class="bg-primary">
									<?php
									echo "<th>$strLabel</th>";
									echo "<th>" . $this->lang->line('BE_GNRL_3') . "</th>";
									if ($iFrm == '12')
										echo "<th>" . $this->lang->line('BE_LBL_117') . "</th>";
									?>
								</tr>
								</thead>
								<tbody>
								<?php
								if ($rsLists) {
									foreach ($rsLists as $row) {
										?>
										<tr>
											<td class="highlight">
												<div class="success"></div>
												<a style="text-decoration:underline;"
												   href="lookuplist?iFrm=<? echo $iFrm; ?>&service=<? echo $service; ?>&id=<? echo $row->$idCol; ?>&servtbl=<? echo $tbl; ?>"><? echo stripslashes($row->$textCol); ?></a>
											</td>
											<td><?php echo $row->$disableCol == '1' ? 'Yes' : 'No'; ?></td>
											<?
											if ($iFrm == '12')
												echo '<td style="text-align:center" valign="middle"><a href="planspricesoffers?planId=' . $row->$idCol . '&plan=' . $row->$textCol . '" ><i class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> ' . $this->lang->line('BE_GNRL_6') . '</a></td>';
											?>
										</tr>
									<?php } ?>
								<?php } else { ?>
									<tr>
										<td colspan="4"><strong><? echo $this->lang->line('BE_GNRL_9'); ?></strong></td>
									</tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>

	<!-- END CONTENT -->
	<!-- END CONTAINER -->
