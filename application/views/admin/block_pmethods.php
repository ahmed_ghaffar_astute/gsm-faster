<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo 'Block Payment Methods'; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<?php echo form_open(base_url('admin/sales/blockpmethods?frmId=9&fTypeId=7'), 'class="form-horizontal" name="frm" id="frm"'); ?>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_USR_7'); ?>:<span class="required_field">*</span> </label>
						<div class="col-lg-6">
							<?php $country_data = get_country_data(); ?>
							<select name="countryId" id="countryId" class="form-control select2me" required>
								<option value="" selected="selected">Choose Country</option>
								<?php FillCombo($id, $country_data); ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label">Payment Methods:<span class="required_field">*</span></label>
						<div class="col-lg-6">
							<select multiple="multiple" class="multi-select" id="my_multi_select2"
									name="payMethods[]">
								<?php FillListFromString($strIds); ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3"></div>
						<div class="col-lg-9">
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary" id="btnSave"
									name="btnSave"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
							<input type="hidden" id="cldFrm" name="cldFrm"/>
						</div>
					</div>
					<?php echo form_close(); ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th>Country</th>
										<th nowrap="nowrap">Blocked Payment Methods</th>
										<th></th>
										<th></th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ($count != 0) {

										foreach ($rsPMs as $rw) {
											$arrPMs[$rw->PaymentMethodId] = stripslashes($rw->PaymentMethod);
										}


										foreach ($rsData as $row) {
											$strBPMs = '';
											if ($row->BlockedPMethodIds != '') {
												$arrIds = array();
												$arrIds = explode(',', $row->BlockedPMethodIds);
											}
											foreach ($arrIds as $key => $value) {
												if (isset($arrPMs[$value])) {
													$strBPMs .= $arrPMs[$value] . '<br />';
												}
											}
											?>
											<tr>
												<td><?php echo stripslashes($row->Country); ?></td>
												<td><?php echo $strBPMs; ?></td>
												<td style="text-align:center" valign="middle">
													<a href="<?php echo base_url('admin/sales/'); ?>blockpmethods?countryId=<?php echo $row->CountryId; ?>"><i
															class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> </a>
												</td>
												<td style="text-align:center" valign="middle">
													<a href="<?php echo base_url('admin/sales/'); ?>blockpmethods?countryId=<?php echo $row->CountryId; ?>&del=1"
													   onclick="return confirm('<?php echo $this->lang->line('BE_LBL_32'); ?>')">
														<i class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i>
													</a>
												</td>
											</tr>
										<?php } ?>
									<?php } else { ?>
										<tr>
											<td colspan="4">
												<strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function () {
		$("#countryId").change(function () {
			$("#cldFrm").val('0');
			$("#frm").submit();
		});
		$("#btnSave").click(function () {
			$("#cldFrm").val('1');
		});
	});
</script>
