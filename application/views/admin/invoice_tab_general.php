<div class="form-group row">
	<div class="col-lg-12">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_DB_ICON4'); ?></h4>
		</div>
	</div>
</div>
	<div class="form-body">
			<!-- BEGIN FORM-->
			<?php echo form_open('', 'class="form-horizontal" name="frm" id="frm"'); ?>
				<input type="hidden" id="id" name="id" value="<?php echo($id); ?>"/>
				<div class="form-group row">
					<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_INDEX_USRNAME'); ?>:</label>
					<div class="col-lg-4">
						<p class="form-control-static"><strong><a
									href="<?php echo base_url('admin/services/'); ?>overview?id=<?php echo $userId; ?>"
									style="text-decoration:underline;"><?php echo $userName; ?></a></strong></p>
					</div>
				</div>
				<div class="form-group row">
					<?php $payment_methods = fetch_payment_methods(); ?>
					<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PM_5'); ?>:<span class="required_field">*</span></label>
					<div class="col-lg-4">
						<select id="pMethodId" name="pMethodId" class="form-control select2me"
								data-placeholder="Select...">
							<?php FillCombo($pMethodId, $payment_methods); ?>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<?php $payment_status = fetch_payment_status(); ?>
					<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LS_7'); ?>:<span class="required_field">*</span></label>
					<div class="col-lg-4">
						<select name="pStatusId" id="pStatusId" class="form-control select2me"
								data-placeholder="Select...">
							<?php FillCombo($pStatusId, $payment_status); ?>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PM_12'); ?>:<span class="required_field">*</span></label>
					<div class="col-lg-4">
						<input type="text" class="form-control" placeholder="Enter Transaction #"
							   name="txtTransactionId" id="txtTransactionId" maxlength="50"
							   value="<?php echo($transactionId); ?>"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_170'); ?>:<span class="required_field">*</span></label>
					<div class="col-lg-4">
						<input type="text" class="form-control"
							   placeholder="Enter <?php echo $this->lang->line('BE_LBL_170'); ?>" id="txtPEmail"
							   name="txtPEmail" maxlength="100" value="<?php echo($pEmail); ?>"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PM_10'); ?>:</label>
					<div class="col-lg-4">
						<p class="form-control-static"><?php echo $myCredits; ?></p>
					</div>
				</div>
				<?php if ($amountPayable != '') { ?>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><b><?php echo $this->lang->line('BE_LBL_628'); ?>
								:</b></label>
						<div class="col-lg-4">
							<p class="form-control-static"><b><?php echo $amountPayable; ?></b></p>
						</div>
					</div>
				<?php } ?>
				<?php if ($paidDtTm != '') { ?>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><b><?php echo $this->lang->line('BE_LBL_629'); ?>
								:</b></label>
						<div class="col-lg-4">
							<p class="form-control-static"><b><?php echo $paidDtTm; ?></b></p>
						</div>
					</div>
				<?php } ?>
				<?php if ($creditsTransferred == 0) { ?>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PM_17'); ?>:</label>
						<div class="col-lg-4">
							<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
								<input type="checkbox" name="chkTransferCredits" id="chkTransferCredits"
									   class="toggle chkSelect"/><span><label
										for="chkTransferCredits"></label></span>
							</div>
						</div>
					</div>
				<?php } else { ?>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_145'); ?>:</label>
						<div class="col-lg-4">
							<p class="form-control-static"><?php echo $this->lang->line('BE_LBL_524'); ?></p>
						</div>
					</div>
				<?php } ?>
				<div class="form-group row">
					<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_525'); ?>:</label>
					<div class="col-lg-4">
						<div class="switch-button switch-button-lg">
						<input type="checkbox" class="chkSelect" name="chkNotify" id="chkNotify"/><span><label
								for="chkNotify"></label></span>
						</div>
					</div>
				</div>
				<div class="form-group" style="display:none;" id="dvNType">
					<label class="col-lg-3 control-label">&nbsp;</label>
					<div class="col-lg-9 radio-list">
						<label class="radio-inline">
							<input type="radio" class="chkSelect" name="chkNotificationType" id="chkNotificationType1"
								   checked><?php echo $this->lang->line('BE_LBL_526'); ?></label>
						<label class="radio-inline">
							<input type="radio" class="chkSelect" name="chkNotificationType"
								   id="chkNotificationType2"><?php echo $this->lang->line('BE_LBL_527'); ?></label>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_GNRL'); ?>:</label>
					<div class="col-lg-4">
						<textarea class="form-control" rows="3" id="txtComments"
								  name="txtComments"><?php echo $comments; ?></textarea>
					</div>
				</div>
				<div class="form-group row">
				<div class="col-lg-3"></div>
				<div class="col-lg-9">
			<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> id="btnSave" name="btnSave"
					class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
			<img style="display:none;" id="statusLoader" src="<?php echo base_url('assets/images/loading.gif'); ?>"
				 border="0" alt="Please wait..."/>
		</div>
				</div>
		<?php echo form_close(); ?>
		<!-- END FORM-->
	</div>
