<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_387'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>				<?php echo form_open(base_url('admin/settings/convertprices'), array('name' => 'frm', 'id' => 'frm', 'class' => 'form-horizontal')); ?>
				<div class="form-group row">
					<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LS_9'); ?>:<span class="required_field">*</span></label>
					<div class="col-lg-4">
						<select name="currencyId" id="currencyId" class="form-control select2me col-4"
								data-placeholder="Select...">
							<?php FillCombo(0, $rsResults); ?>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-lg-3"></div>
					<div class="col-lg-9">
						<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> class="btn btn-primary"
								id="btnSave" name="btnSave"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
						<img style="display:none;" id="statusLoader"
							 src=""<?php echo base_url('assets/images/loading.gif'); ?>"" border="0" alt="Please
						wait..." />
					</div>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
	<div id="baseurl" style="display: none">
		<?php echo base_url(); ?>
	</div>
<script src="<?php echo base_url('assets/js/cnvrtprices.js') ?>"></script>
