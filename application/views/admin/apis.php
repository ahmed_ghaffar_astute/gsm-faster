
				<!-- Page header -->
				<div class="page-header border-bottom-0">
					<div class="page-header-content header-elements-md-inline">
						<div class="page-title d-flex">
							<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_MR_HD_4'); ?></h4>
							<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
						</div>
						<?php if ($rsStngs->CanAddNewAPI == '1') { ?>
							<div align="right">
								<input type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									   value="<?php echo $this->lang->line('BE_LBL_182'); ?>"
									   onclick="window.location.href='<?php echo base_url();?>admin/Settings/manageapi?&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>'"
									   class="btn btn-primary"/>
							</div>
						<?php } ?>
					</div>
				</div>
				<!-- /page header -->
				<!-- Content area -->
				<div class="content pt-0">

					<div class="card">
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
								<div class="card-body">
									<!-- Our Working Area Start -->
									<?php if (isset($message) && $message != '') { ?>
										<div class="form-group row">
											<div class="col-lg-12">
												<div class="alert alert-success"><?php echo $message; ?></div>

											</div>
										</div>
									<?php } ?>
									<div class="table-responsive">
							<div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
								<img src="../assets/img/ajax-modal-loading.gif" alt="" class="loading">
							</div>
							<table class="table table-striped table-bordered table-advance table-hover">
								<thead>
								<tr class="bg-primary">
									<th><?php echo $this->lang->line('BE_MR_19'); ?></th>
									<th><?php echo $this->lang->line('BE_INDEX_USRNAME'); ?></th>
									<th><?php echo $this->lang->line('BE_LBL_38'); ?></th>
									<th><?php echo $this->lang->line('BE_GNRL_3'); ?></th>
									<th></th>
									<th></th>
								</tr>
								</thead>
								<tbody>
								<?php
								if ($rsAPIs) {
									foreach	($rsAPIs as $row) {
										?>
										<tr>
											<td>
												<a data-target="#ajax" data-toggle="modal" href="JavaScript:void(0);"
												   onclick="syncIMEIServices('<?php echo $row->APIId; ?>');"
												   title="Sync IMEI Services"><span class="fa fa-refresh"></span></a>&nbsp;&nbsp;&nbsp;
												<a href="<?php echo base_url()?>admin/Settings/manageapi?id=<?php echo $row->APIId; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $_REQUEST['fTypeId'] ?>"><?php echo stripslashes($row->APITitle); ?></a>
												<img style="display:none;" id="syncLoader_<?php echo $row->APIId; ?>"
													 src="<?php echo base_url()?>assets/images/loading.gif" border="0" alt="Please wait..."/>
											</td>
											<td><?php echo $row->AccountId; ?></td>
											<td><?php echo $row->ServerURL; ?></td>
											<td><?php echo $row->DisableAPI == '1' ? 'Yes' : 'No'; ?></td>
											<td style="text-align:center" valign="middle">
												<a href="<?php echo base_url()?>admin/Settings/manageapi?id=<?php echo $row->APIId; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><i
														class="icon-pencil7"></i> </a>
											</td>
											<td style="text-align:center" valign="middle">
												<?php if ($row->SystemAPI == '0') { ?>
													<a href="<?php echo base_url()?>admin/Settings/apis?del=1&id=<?php echo($row->APIId); ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"
													   onclick="return confirm('<?php echo $this->lang->line('BE_LBL_32'); ?>')"><i
															class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i> </a>
												<?php } else echo '-'; ?>
											</td>
										</tr>
									<?php } ?>
								<?php } else { ?>
									<tr>
										<td colspan="6"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td>
									</tr>
								<?php } ?>
								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>
		</div>
