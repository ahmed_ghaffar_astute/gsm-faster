           <!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_MENU_MR_7'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div class="form-group" align="right">
			<input type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
				   value="<?php echo $this->lang->line('BE_GNRL_14') . ' a ' . $this->lang->line('BE_MR_HD_10'); ?>"
				   onclick="window.location.href='faqs?frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>'"
				   class="btn btn-primary yellow"/>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th><?php echo $this->lang->line('BE_MR_32'); ?></th>
										<th><?php echo $this->lang->line('BE_GNRL_3'); ?></th>
										<th></th>
										<th></th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ($rsFAQs) {
										foreach ($rsFAQs as $row) {
											?>
											<tr>
												<td nowrap="nowrap" class="highlight" valign="top">
													<div class="success"></div>
													<a href="<?php echo base_url() ?>admin/Miscellaneous/faqs?id=<?php echo $row->FAQId; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><?php echo substr(stripslashes($row->Question), 0, 25); ?>
														...</a></td>
												<td><?php echo $row->DisableFAQ == '1' ? 'Yes' : 'No'; ?></td>
												<td style="text-align:center" valign="middle">
													<a href="<?php echo base_url() ?>admin/Miscellaneous/faqs?id=<?php echo $row->FAQId; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><i
															class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> </a>
												</td>
												<td style="text-align:center" valign="middle">
													<a href="<?php echo base_url() ?>admin/Miscellaneous/viewfaqs?del=1&id=<?php echo($row->FAQId); ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"
													   onclick="return confirm('<?php echo $this->lang->line('BE_LBL_32'); ?>')"><i
															class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></td>
											</tr>
										<?php } ?>
									<?php } else { ?>
										<tr>
											<td colspan="4">
												<strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
