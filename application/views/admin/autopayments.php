<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_71'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php }
					echo form_open(base_url('admin/clients/autopayments'), 'class="horizontal-form" id="frm" name="frm"'); ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<label
								class="control-label"><?php echo $this->lang->line('BE_USR_1'); ?></label>
							<input type="text" name="txtUName" placeholder="Enter Username"
								   value="<?php echo($uName); ?>" class="form-control">
						</div>
						<div class="col-lg-6">
							<label
								class="control-label"><?php echo $this->lang->line('BE_LBL_345'); ?></label>
							<input type="text" name="txtEmail" placeholder="Enter User Email"
								   value="<?php echo($email); ?>" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<?php $users_data = fetch_distinct_users_countries(); ?>
							<label
								class="control-label"><?php echo $this->lang->line('BE_LS_6'); ?></label>
							<select name="countryId" class="form-control select2me"
									data-placeholder="<?php echo $this->lang->line('BE_LBL_272'); ?>">
								<option value="0">Choose a Country</option>
								<?php FillCombo($countryId, $users_data); ?>
							</select>
						</div>
						<div class="col-lg-6">
							<label class="control-label">IP</label>
							<input type="text" name="txtIP" placeholder="Enter IP"
								   value="<?php echo($ip); ?>" class="form-control"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label
								class="control-label"><?php echo $this->lang->line('BE_LBL_612'); ?></label>
							<select name="autoPmnts" class="form-control select2me">
								<option value="-1" selected="selected">All</option>
								<option value="1" <?php if ($autoPmnts == '1') echo 'selected'; ?>>
									Allowed
								</option>
								<option value="0" <?php if ($autoPmnts == '0') echo 'selected'; ?>>Not
									Allowed
								</option>
							</select>
						</div>
						<div class="col-lg-6">
							<label
								class="control-label"><?php echo $this->lang->line('BE_MENU_USRS_5'); ?></label>
							<select name="canAddCrdts" class="form-control select2me">
								<option value="-1" selected="selected">All</option>
								<option value="1" <?php if ($canAddCrdts == '1') echo 'selected'; ?> >
									Allowed
								</option>
								<option value="0" <?php if ($canAddCrdts == '0') echo 'selected'; ?>>Not
									Allowed
								</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">

							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary"><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
						</div>
					</div>
					<?php
					if ($count != 0) {

						$row = fetch_users_count($strWhere);
						$totalRows = $row->TotalRecs;
						if ($totalRows > $limit)
							doPages_DropDown($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next, 'frm');
					}
					?>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="page-header-content header-elements-md-inline">
								<div class="page-title d-flex">
									<h4>
										<i class="mr-2"></i> <?php echo $this->lang->line('BE_MENU_USRS'); ?>
									</h4>
								</div>
								<div align="right">
									<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
											class="btn btn-primary" onclick="setValue('1');"><i
											class="fa fa-check"> </i> Allow Payments
									</button>
									<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
											class="btn btn-danger" onclick="setValue('2');"><i class="fa fa-ban"> </i>
										Block Payments
									</button>
									<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
											class="btn btn-primary" onclick="setValue('3');"><i
											class="fa fa-check"> </i> Allow Credits
									</button>
									<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
											class="btn btn-danger" onclick="setValue('4');"><i class="fa fa-ban"> </i>
										Block Credits
									</button>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">

							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th><?php echo $this->lang->line('BE_USR_1'); ?></th>
										<th><?php echo $this->lang->line('BE_LBL_345'); ?></th>
										<th><?php echo $this->lang->line('BE_USR_9'); ?></th>
										<th><?php echo $this->lang->line('BE_GNRL_4'); ?></th>
										<th style="text-align:center"><?php echo $this->lang->line('BE_LBL_612'); ?></th>
										<th style="text-align:center"><?php echo $this->lang->line('BE_MENU_USRS_5'); ?></th>
										<th style="text-align:center"><input type="checkbox" class="chkSelect"
																			 id="chkSelect" name="chkSelect"
																			 onClick="selectAllChxBxs('chkSelect', 'chkUsers', <?php echo $count; ?>);">
										</th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ($count != 0) {
										$i = 0;
										foreach ($rsClnts as $row) {
											$clsAuto = ' red';
											$txtAuto = 'Not Allowed';
											$clsCr = ' red';
											$txtCr = 'Not Allowed';
											if ($row->AutoFillCredits == '1') {
												$clsAuto = ' green';
												$txtAuto = 'Allowed';
											}
											if ($row->CanAddCredits == '1') {
												$clsCr = ' green';
												$txtCr = 'Allowed';
											}
											?>
											<tr>
												<td>
													<a href="<?php echo base_url('admin/clients/'); ?>user?id=<?php echo $row->UserId; ?>"><?php echo stripslashes($row->UserName); ?></a><br/>
													<?php echo $row->Country; ?><br/>
													IP: <?php echo($row->IP == '' ? 'N-A' : $row->IP); ?>
												</td>
												<td><?php echo($row->UserEmail); ?></td>
												<td><?php echo stripslashes($row->Name); ?></td>
												<td><?php echo $row->AddedAt; ?></td>
												<td style="text-align:center" valign="middle" nowrap="nowrap">
													<a style="width:90px;" class="btn btn-xs <?php echo $clsAuto; ?>"
													   href="<?php echo base_url('admin/clients'); ?>autopayments?ap=<?php echo $row->AutoFillCredits == '1' ? '0' : '1'; ?>&start=<?php echo $start; ?>&uId=<?php echo ($row->UserId) . $txtlqry; ?>&frmId=27&fTypeId=5"
													   onclick="return confirm('Are you sure you want to <?php echo $row->AutoFillCredits == '1' ? 'block' : 'allow'; ?> auto payment?')"> <?php echo $txtAuto; ?> </a>
												</td>
												<td style="text-align:center" valign="middle" nowrap="nowrap">
													<a style="width:90px;" class="btn btn-xs <?php echo $clsCr; ?>"
													   href="<?php echo base_url('admin/clients'); ?>autopayments?ac=<?php echo $row->CanAddCredits == '1' ? '0' : '1'; ?>&start=<?php echo $start; ?>&uId=<?php echo ($row->UserId) . $txtlqry; ?>&frmId=27&fTypeId=5"
													   onclick="return confirm('Are you sure you want to <?php echo $row->CanAddCredits == '1' ? 'block' : 'allow'; ?> add credits?')"> <?php echo $txtCr; ?> </a>
												</td>
												<td align="center">
													<input type="checkbox" id="chkUsers<?php echo $i; ?>"
														   name="chkUsers[]" value="<?php echo $row->UserId; ?>"/>
												</td>
											</tr>
											<?php
											$i++;
										}
									} else
										echo "<tr><td colspan='7'>" . $this->lang->line('BE_GNRL_9') . "</td></tr>";
									?>
									<input type="hidden" id="records" name="records" value="<?php echo $limit; ?>"/>
									<input type="hidden" name="start" id="start" value="<?php echo $start; ?>"/>
									<input type="hidden" name="cldFrm" id="cldFrm" value="0"/>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function setValue(i) {
		document.getElementById('cldFrm').value = i;
	}

	function validate() {
		if ($("#cldFrm").val() > 0) {
			var totalUsers = <?php echo $count; ?>;
			var strIds = '0';
			for (var i = 0; i < totalUsers; i++) {
				if (document.getElementById('chkUsers' + i).checked) {
					strIds += ',' + document.getElementById('chkUsers' + i).value;
				}
			}
			if (strIds == '0') {
				alert('<?php echo $this->lang->line('BE_LBL_53'); ?>');
				return false;
			} else
				return true;
		} else
			return true;
	}

	$(document).ready(function () {
		$("#frm").submit(function () {
			return validate();
		});
	});
</script>
