<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                                <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn default"><? echo $this->lang->line('BE_LBL_285'); ?></button>
                                <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><i class="fa fa-angle-down"></i></button>
                                <ul class="dropdown-menu" role="menu" style="text-align:left;">
                                    <li><a href="<?php echo base_url('admin/services/codes?codeStatusId=1&searchType=1');?>"><?php echo $this->lang->line('BE_LBL_286'); ?></a></li>
                                    <li><a href="<?php echo base_url('admin/services/codes?codeStatusId=1&searchType=2');?>"><?php echo $this->lang->line('BE_LBL_287'); ?></a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn default"><? echo $this->lang->line('BE_LBL_288'); ?></button>
                                <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><i class="fa fa-angle-down"></i></button>
                                <ul class="dropdown-menu" role="menu" style="text-align:left;">
                                    <li><a href="<?php echo base_url('admin/services/codes?codeStatusId=4&searchType=3');?>"><?php echo $this->lang->line('BE_LBL_289'); ?></a></li>
                                    <li><a href="<?php echo base_url('admin/services/codes?bulk=1&codeStatusId=4&searchType=3')?>"><?php echo $this->lang->line('BE_LBL_290'); ?></a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn default"><? echo $this->lang->line('BE_LBL_291'); ?></button>
                                <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><i class="fa fa-angle-down"></i></button>
                                <ul class="dropdown-menu" role="menu" style="text-align:left;">
                                    <li><a href="<?php echo base_url('admin/services/codes')?>"><?php echo $this->lang->line('BE_LBL_291'); ?></a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn default"><? echo $this->lang->line('BE_LBL_292'); ?></button>
                                <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><i class="fa fa-angle-down"></i></button>
                                <ul class="dropdown-menu" role="menu" style="text-align:left;">
                                    <li><a href="<?php echo base_url('admin/services/verifyorders')?>"><?php echo $this->lang->line('BE_LBL_292'); ?></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="col-md-12">
                        <?php if($message != '')
                            echo '<div class="alert alert-success">'.$message.'</div>'; 
                        ?>
                        <?php echo form_open(base_url('admin/services/verifyorders'), 'name="frm" id="frm"') ?>
                            <?php include APPPATH.'scripts/admincodessearchsection.php';?>
                        <?php echo form_close(); ?>
                        <?php
                            if($count != 0)
                            {
                                $row = fetch_users_codes_count($strWhere);
                                $totalRows = $row->TotalRecs;
                                if($totalRows > $limit)
                                    doPages($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next);
                            }
                        ?>
                        <?php echo form_open(base_url('admin/services/verifyorders'), 'id="myFrm" onsubmit="return validate(-1)"');?>
                            <p>
                                <div class="btn-group btn-group-sm btn-group-solid">
                                    <input type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="<?php echo $this->lang->line('BE_LBL_194'); ?>" class="btn btn-primary" name="btnSubmit" />
                                </div>
                                <div class="btn-group btn-group-sm btn-group-solid" align="right">
                                    <input type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="<?php echo $this->lang->line('BE_LBL_40'); ?>" class="btn btn-primary" onClick="downloadIMEIs();" />
                                </div>
                            </p>
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-reorder"></i><?php echo $this->lang->line('BE_LBL_183'); ?>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <input type="hidden" name="hdsbmt" id="hdsbmt" />
                                        <input type="hidden" name="uId" value="<?php echo $uId;?>" />
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th nowrap="nowrap" width="3%">Sr. #</th>
                                                    <th>
                                                    &nbsp;<?php echo $this->lang->line('BE_LBL_194'); ?>&nbsp;<input type="checkbox" class="chkSelect" id="chkSelect1" name="chkSelect1" onClick="selectAllChxBxs('chkSelect1', 'chkReply', <?php echo $count; ?>);" value="true">
                                                    </th>
                                                    <th nowrap="nowrap"><?php echo $this->lang->line('BE_LBL_589'); ?></th>
                                                    <th><?php echo $this->lang->line('BE_CODE_2'); ?></th>
                                                    <th><?php echo $this->lang->line('BE_INDEX_USRNAME'); ?></th>
                                                    <th><?php echo $this->lang->line('BE_CODE_1'); ?>/Data</th>
                                                    <th><?php echo $this->lang->line('BE_CODE_7'); ?></th>
                                                    <th><?php echo $this->lang->line('BE_CODE_6'); ?></th>
                                                    <th style="text-align:center">&nbsp;<?php echo $this->lang->line('BE_LBL_308').' / Download';?>&nbsp;
                                                    <input type="checkbox" id="chkSelect" name="chkSelect" onClick="selectAllChxBxs('chkSelect', 'chkCodes', <?php echo $count; ?>);" value="true">
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if($count != 0){
                                                        $i = 0;
                                                        foreach($rsCodes as $row)
                                                        {
                                                            $dt= '-';
                                                            if($row->RequestedAt != '')
                                                                $arrDt = explode(' ', $row->RequestedAt);
                                                            if(isset($arrDt[0]))
                                                                $dt = $arrDt[0];
                                                            $imeiInChkBx = $row->IMEINo == '' ? '.' : $row->IMEINo;
                                                ?>
                                                        <tr>
                                                            <td><?php echo $i+1; ?>.</td>
                                                            <td align="center">
                                                                <input type="checkbox" id="chkReply<?php echo $i; ?>" name="chkReply[]" value="<?php echo $row->CodeId.'|'.$row->UserId.'|'.$row->Credits.'|'.$imeiInChkBx.'|'.$i.'|'.$row->PackageId; ?>">
                                                            </td>
                                                            <td><? echo $apiError;?> <a href="code.php?id=<? echo($row->CodeId);?>">#<? echo $row->CodeId;?></a></td>
                                                            <td><?php echo stripslashes($row->PackageTitle); ?></td>
                                                            <td><?php echo $row->UserName; ?></td>
                                                            <td>
                                                                <?php echo $row->IMEINo;?><br /><br />
                                                                <?php echo stripslashes($row->VerifyData); ?>
                                                            </td>
                                                            <td nowrap="nowrap"><?php echo $dt; ?></td>
                                                            <td>
                                                                <input type="text" class="form-control" placeholder="Enter Data" style="width:150px;" name="txtCode<?php echo $i; ?>" value="<?php echo $row->Code; ?>" />
                                                            </td>
                                                            <td align="center">
                                                            <input type="checkbox" id="chkCodes<?php echo $i; ?>" name="chkCodes[]" value="<?php echo $row->CodeId.'|'.$row->UserId.'|'.$row->Credits.'|'.$imeiInChkBx.'|'.$i.'|'.$row->PackageId; ?>">
                                                            </td>
                                                        </tr>
                                                <?php
                                                    $i++;
                                                        }
                                                }
                                                else
                                                    echo '<tr><td colspan="9">'.$this->lang->line('BE_GNRL_9').'</td></tr>';
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>      
            </div>
        </div>
    </div>

<script>
    function downloadIMEIs()
    {
        var strIds = validate('0');
        if(strIds != false)
            window.location.href = 'downloadIMEIs.php?ids='+strIds;
    }
    function validate(cldFrm)
    {
        if(cldFrm == 0)
        {
            var totalCodes = <? echo $count; ?>;
            var strIds = '0';
            for(var i=0; i<totalCodes; i++)
            {
                if(document.getElementById('chkCodes'+i).checked)
                {
                    arr = document.getElementById('chkCodes'+i).value.split('|');
                    strIds += ',' + arr[0];
                }
            }
            if(strIds == '0')
            {
                alert('<? echo $BE_LBL_53; ?>');
                return false;
            }
            if(cldFrm == 0)
                return strIds;
            else
                return true;
        }
        else
            return true;
    }
</script>
