<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_653'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?
					$USER_ID_BTNS = $userId;
					if ($USER_ID_BTNS > 0) { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<?php include APPPATH . 'scripts/userbtns.php'; ?>
							</div>
						</div>
					<?php }
					if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>

					<?php echo form_open(base_url() . 'admin/reports/precodeshistory?frmId=88&fTypeId=15', array('class' => "horizontal-form", 'method' => "post")); ?>
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label"><?php echo $this->lang->line('BE_PM_3'); ?></label>
								<input type="text" name="txtUName" value="<?php echo($userName); ?>"
									   class="form-control">
							</div>
						</div>
						<!--/span-->
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label"><?php echo $this->lang->line('BE_PCK_HD_4'); ?></label>
								<input type="text" name="txtService" value="<?php echo($service); ?>"
									   class="form-control">
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label"><?php echo $this->lang->line('BE_PM_7'); ?></label><br/>
								<input class="form-control form-control-inline input-largest date-picker" type="text"
									   name="txtFromDt" value="<?php echo($dtFrom); ?>"/>
							</div>
						</div>
						<!--/span-->
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label"><?php echo $this->lang->line('BE_PM_8'); ?></label><br/>
								<input class="form-control form-control-inline input-largest date-picker" type="text"
									   name="txtToDt" value="<?php echo($dtTo); ?>"/>
							</div>
						</div>
						<!--/span-->
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary"><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
							<input type="hidden" name="cldFrm" id="cldFrm" value="0"/>
						</div>
					</div>
					<?php echo form_close(); ?>
					<?
					if ($rsRpt) {
						$totalRows = $row->TotalRecs;
						if ($totalRows > $limit)
							doPages($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next);
					}
					?>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="page-title d-flex">
								<h4>
									<i class="mr-2"></i> <?php echo $this->lang->line('BE_LBL_653'); ?>
								</h4>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th><?php echo $this->lang->line('BE_INDEX_USRNAME'); ?></th>
										<th><?php echo $this->lang->line('BE_PCK_HD_4'); ?></th>
										<th><?php echo $this->lang->line('BE_CODE_6'); ?></th>
										<th><?php echo $this->lang->line('BE_CR_3'); ?></th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ($rsRpt) {
										foreach ($rsRpt as $row) {
											?>
											<tr>
												<td class="highlight">
													<div class="success"></div>
													<a href="javascript:void(0);"><?php echo($row->UserName); ?></a>
												</td>
												<td><?php echo stripslashes($row->Service); ?></td>
												<td><?php echo stripslashes($row->Code); ?></td>
												<td><?php echo $row->HistoryDtTm; ?></td>
											</tr>
											<?php
										}
									} else {
										?>
										<tr>
											<td colspan='4'><?php echo $this->lang->line('BE_GNRL_9'); ?></td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<? /* include 'jscomponents.php'; */ ?>
