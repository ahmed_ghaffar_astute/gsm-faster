<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $prodName; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="page-title d-flex">
								<h4><i class="mr-2"></i> <?php echo 'Visitors Details'; ?></h4>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">

							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th><?php echo $this->lang->line('BE_LBL_43'); ?></th>
										<th>Date Time</th>
										<th>Referer</th>
										<th></th>
									</tr>
									</thead>
									<tbody>
									<?
									if ($count != 0) {
										$i = 0;
										foreach ($rsIpRpt as $row) {
											?>
											<tr>
												<td><? echo $row->IP; ?></td>
												<td><? echo $row->VisitDtTm; ?></td>
												<td><? echo $row->Referer == '' ? '-' : stripslashes($row->Referer); ?></td>
												<td style="text-align:center" valign="middle">
													<a href="<?php echo base_url(); ?>admin/reports/eprodvisitdtls?del=1&id=<? echo($row->Id); ?>&pId=<? echo $productId; ?>&pn=<? echo($prodName); ?>&frmId=<? echo $this->input->post_get('frmId'); ?>&fTypeId=<? echo $this->input->post_get('fTypeId') ?>"
													   onclick="return confirm('<? echo $this->lang->line('BE_LBL_32'); ?>')"><i
															class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></td>
											</tr>
											<?
											$i++;
										}
									} else
										echo "<tr><td colspan='4'>" . $this->lang->line('BE_GNRL_9') . "</td></tr>";
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<? /* include 'jscomponents.php'; */ ?>
