<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4>
				<i class="icon-arrow-left52 mr-2"></i> 
				<?php echo $this->lang->line('BE_LBL_819'); ?> - <?php echo $hdr; ?>
			</h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-lg-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<?php echo form_open('#',  'name="frmSettings" id="frmSettings" class="form-horizontal" ') ?>
						<fieldset>
							<div class="form-group row">
								<label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_PCK_17'); ?>:<span class="required_field">*</span> </label>
								<div class="col-lg-9">
									<input type="text" class="form-control" placeholder="Enter Field Label" <?php if($systemFld == '1') echo 'disabled'; ?> maxlength="100" name="txtLbl" id="txtLbl" value="<?php echo($lbl);?>" >
									<input type="hidden" id="id" name="id" value="<?php echo $id; ?>" />
									<span class="form-text text-muted">
										<strong>ONLY USE CHARACTERS FROM A-Z OR DIGITS OR SPACES.</strong>
									</span>
								</div>	
							</div>
							<div class="form-group row">
								<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PCK_19'); ?>:<span class="required_field">*</span></label>
								<div class="col-lg-9">
									<select id="fieldType" name="fieldType" class="form-control select2me" data-placeholder="Select..." <?php if($id > 0) echo 'disabled'; ?>>
										<option value="0" selected><?php echo $this->lang->line('BE_LBL_623'); ?></option>
										<option value="Text Box" <?php if($fType == 'Text Box') echo 'selected'; ?>>Text Box</option>
										<option value="Text Area" <?php if($fType == 'Text Area') echo 'selected'; ?>>Text Area</option>
										<option value="Drop Down" <?php if($fType == 'Drop Down') echo 'selected'; ?>>Drop Down</option>
										<option value="Radio Button" <?php if($fType == 'Radio Button') echo 'selected'; ?>>Radio Button</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-lg-3 control-label">Restriction:<span class="required_field">*</span></label>
								<div class="col-lg-9">
									<select id="restriction" name="restriction" class="form-control select2me">
										<option value="0" selected>No Restriction</option>
										<option value="1" <?php if($restriction == '1') echo 'selected'; ?>>Digits Only</option>
										<option value="2" <?php if($restriction == '2') echo 'selected'; ?>>Characters Only</option>
										<option value="3" <?php if($restriction == '3') echo 'selected'; ?>>Digits and Characters Only</option>
									</select>
								</div>
							</div>
							<div class="form-group row" id="dvMinLen" <?php if($fType != 'Text Box') echo 'style="display:none;"';?>>
								<label class="col-lg-3 control-label">Min Length:</label>
								<div class="col-lg-9">
									<input type="text" class="form-control" placeholder="Enter Min Length" maxlength="3" name="txtMinLen" id="txtMinLen" value="<?php echo($minLen);?>" >
								</div>
							</div>
							<div class="form-group row" id="dvMaxLen" <?php if($fType != 'Text Box') echo 'style="display:none;"';?>>
								<label class="col-lg-3 control-label">Max Length:</label>
								<div class="col-lg-9">
									<input type="text" class="form-control" placeholder="Enter Max Length" maxlength="3" name="txtMaxLen" id="txtMaxLen" value="<?php echo($maxLen);?>" >
								</div>
							</div>
							<div class="form-group row">
								<label class="col-lg-3 control-label">Instructions:</label>
								<div class="col-lg-9">
									<textarea class="form-control" rows="5" id="txtInst" name="txtInst"><?php echo replaceBRTag($instructions); ?></textarea>
									<span class="form-text text-muted">
										These instructions will be displayed at order's page against this custom field.
									</span>
								</div>
								
							</div>
							<div class="form-group row">
								<label class="col-lg-3 control-label">Use it As Quantity in Pricing:</label>
								<div class="col-lg-9">
									<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
										<input type="checkbox" name="chkAsQty" id="chkAsQty" <?php echo $useAsQty == 1 ? 'checked' : '';?> class="toggle"/><span><label for="chkAsQty"></label></span>
									</div>
									<span class="form-text text-muted">
										To calculate prices as per quantity.
									</span>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_717'); ?>:</label>
								<div class="col-lg-9">
									<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
										<input type="checkbox" class="chkSelect" name="chkMandatory" id="chkMandatory" <?php echo $mandatory == 1 ? 'checked' : '';?> class="toggle"/><span><label for="chkMandatory"></label></span>
									</div>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_GNRL_1'); ?>:</label>
								<div class="col-lg-9">
									<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
										<input type="checkbox" name="chkDisable" class="chkSelect" id="chkDisable" <?php echo $disable == 1 ? 'checked' : '';?> class="toggle"/><span><label for="chkDisable"></label></span>
									</div>
								</div>
							</div>
						</fieldset>
					<?php echo form_close(); ?>
						
						<div class="form-group row">
							<div class="col-lg-3"></div>
							<div class="col-lg-9">
									<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> id="btnSave" name="btnSave" class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
								<input type="hidden" name="srvType" id="srvType" value="<?php echo $serviceType; ?>" />
								<img style="display:none;" id="dvLoader" src="<?php echo base_url('assets/images/loading.gif') ?>" border="0" alt="Please wait..." />
							</div>
						</div>
					
        		</div>
    		</div>
		</div>
	</div>
</div>


<script language="javascript" src="<?php echo base_url('assets/js/functions.js');?>" ></script>
<script language="javascript" src="<?php echo base_url('assets/js/customfield.js');?>"></script>
