<div class="tabbable tabbable-custom tabbable-custom-profile">
    <ul class="nav nav-tabs nav-tabs-solid border-0">
        <li class="active">
            <a href="#tab_1_11" class="nav-link" data-toggle="tab"><?php echo $this->lang->line('BE_LBL_554'); ?></a>
        </li>
        <li>
            <a href="#tab_1_22" class="nav-link" data-toggle="tab"><?php echo $this->lang->line('BE_LBL_555'); ?></a>
        </li>
        <li>
            <a href="#tab_1_33" class="nav-link" data-toggle="tab"><?php echo $this->lang->line('BE_LBL_556'); ?></a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1_11">
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-advance table-hover">
                    <thead>
                    <tr class="bg-primary">
                        <th>
                            <i class="fa fa-briefcase"></i> <?php echo $this->lang->line('BE_PCK_HD'); ?>
                        </th>
                        <th>
                            <i class="fa fa-bookmark"></i> <?php echo $this->lang->line('BE_LBL_333'); ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($rsIMEIOrders as $row)
                        {
                            echo "<tr><td><a href='JavaScript:void(0);'>".stripslashes($row->PackageTitle)."</a></td>";
                            echo "<td><a target='_blank' href='".base_url('admin/services/')."codes?uId=".$USER_ID_BTNS."&packId=".$row->PackageId."'>".$row->TotalOrders."</a></td></tr>";
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!--tab-pane-->
        <div class="tab-pane" id="tab_1_22">
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-advance table-hover">
                    <thead>
                    <tr class="bg-primary">
                        <th>
                            <i class="fa fa-briefcase"></i> <?php echo $this->lang->line('BE_LBL_373'); ?>
                        </th>
                        <th>
                            <i class="fa fa-bookmark"></i> <?php echo $this->lang->line('BE_LBL_333'); ?>
                        </th>
                        <th>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($rsFileOrders as $row )
                        {
                            echo "<tr><td><a href='JavaScript:void(0);'>".stripslashes($row->PackageTitle)."</a></td>";
                            echo "<td><a target='_blank' href='".base_url('admin/services/')."codesslbf?uId=".$USER_ID_BTNS."&packageId=".$row->PackageId."'>".$row->TotalOrders."</a></td></tr>";
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane" id="tab_1_33">
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-advance table-hover">
                    <thead>
                    <tr class="bg-primary">
                        <th>
                            <i class="fa fa-briefcase"></i> <?php echo $this->lang->line('BE_PCK_HD_6'); ?>
                        </th>
                        <th>
                            <i class="fa fa-bookmark"></i> <?php echo $this->lang->line('BE_LBL_333'); ?>
                        </th>
                        <th>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($rsServerOrders as $row)
                        {
                            echo "<tr><td><a href='JavaScript:void(0);'>".stripslashes($row->PackageTitle)."</a></td>";
                            echo "<td><a target='_blank' href='".base_url('admin/services/')."logrequests?uId=".$USER_ID_BTNS."&logPackageId=".$row->LogPackageId."'>".$row->TotalOrders."</a></td></tr>";
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>                                                
    </div>
</div>
