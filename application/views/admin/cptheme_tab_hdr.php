<?
$hdr1 = $clrsHdr[0];
$hdr2 = $clrsHdr[1];
$hdr3 = $clrsHdr[2];
$hdr4 = $clrsHdr[3];
$hdr5 = $clrsHdr[4];
$hdr6 = $clrsHdr[5];
$hdr7 = $clrsHdr[6];
$hdr8 = $clrsHdr[7];
$hdr9 = $clrsHdr[8];
$hdr10 = $clrsHdr[9];
?>
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_751'); ?></h5>
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_754'); ?>:<span class="required_field"><span class="required_field">*</span></span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $hdr1; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="hdr1" value="<? echo $hdr1; ?>"
								   name="txtHdr1">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('hdr1').jscolor.show()"
									style="background-color: <? echo $hdr1; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_755'); ?>:<span class="required_field"><span class="required_field">*</span></span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $hdr2; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="hdr2" value="<? echo $hdr2; ?>"
								   name="txtHdr2">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('hdr2').jscolor.show()"
									style="background-color: <? echo $hdr2; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_756'); ?>:<span class="required_field"><span class="required_field">*</span></span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $hdr3; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="hdr3" value="<? echo $hdr3; ?>"
								   name="txtHdr3">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('hdr3').jscolor.show()"
									style="background-color: <? echo $hdr3; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_759'); ?>:<span class="required_field"><span class="required_field">*</span></span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $hdr6; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="hdr4" value="<? echo $hdr6; ?>"
								   name="txtHdr6">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('hdr4').jscolor.show()"
									style="background-color: <? echo $hdr6; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_760'); ?>:<span class="required_field"><span class="required_field">*</span></span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $hdr7; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="hdr5" value="<? echo $hdr7; ?>"
								   name="txtHdr7">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('hdr5').jscolor.show()"
									style="background-color: <? echo $hdr7; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_761'); ?>:<span class="required_field"><span class="required_field">*</span></span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $hdr8; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="hdr6" value="<? echo $hdr8; ?>"
								   name="txtHdr8">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('hdr6').jscolor.show()"
									style="background-color: <? echo $hdr8; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_762'); ?>:<span class="required_field"><span class="required_field">*</span></span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $hdr9; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="hdr7" value="<? echo $hdr9; ?>"
								   name="txtHdr9">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('hdr7').jscolor.show()"
									style="background-color: <? echo $hdr9; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label">2nd Nav Bar Background Color:<span class="required_field"><span class="required_field">*</span></span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $hdr10; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="hdr8" value="<? echo $hdr10; ?>"
								   name="txtHdr10">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('hdr8').jscolor.show()"
									style="background-color: <? echo $hdr10; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label">Drop Down Menu Links Color:<span class="required_field"><span class="required_field">*</span></span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $hdr4; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="hdr9" value="<? echo $hdr4; ?>"
								   name="txtHdr4">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('hdr9').jscolor.show()"
									style="background-color: <? echo $hdr4; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 control-label">Drop Down Menu Background Color:<span class="required_field"><span class="required_field">*</span></span></label>
					<div class="col-lg-8">
						<div class="input-group color colorpicker-default col-4" data-color="<? echo $hdr5; ?>"
							 data-color-format="rgba">
							<input type="text" class="form-control jscolor rounded-round" id="hdr10" value="<? echo $hdr5; ?>"
								   name="txtHdr5">
							<span class="input-group-btn">
                            <button class="btn default" type="button"><i
									onclick="document.getElementById('hdr10').jscolor.show()"
									style="background-color: <? echo $hdr5; ?>;"></i>&nbsp;</button>
                        </span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
