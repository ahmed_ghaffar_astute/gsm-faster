
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4>
				<i class="icon-arrow-left52 mr-2"></i> 
				<?php echo $this->lang->line('BE_LBL_696').' - '.$header; ?>
			</h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<div class="content pt-0">
	<div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12 col-lg-10 col-xl-10">
                    <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> name="btnSaveG" id="btnSaveG" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_697').' '.$header; ?></button>
                    <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> name="btnSaveR" id="btnSaveR" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-danger"><?php echo $this->lang->line('BE_LBL_698').' '.$btnRHdr; ?></button>
                    <input type="hidden" name="type" id="type" value="<?php echo $type; ?>" />
                    <div style="display:none;" id="statusLoader">
                        <img src="<?php  echo base_url('assets/images/loading.gif');?>" border="0" alt="Please wait..." />
                    </div>	                     
                </div>
            </div>
        </div>
    </div>
</div>

<script language="javascript" src="<?php echo base_url('assets/js/smsintegration.js');?>"></script>
