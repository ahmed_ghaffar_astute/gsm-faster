<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                In Process Server Orders
            </h3>
        </div>
    </div>
    <div class="row">
		<div class="col-md-12">
            <div class="block-web">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i>
                            <h3>
                                In Process Server Orders
                            </h3>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-advance table-hover">
                                <thead>
                                    <tr>
                                        <th><?php echo $this->lang->line('BE_PCK_HD_4'); ?></th>
                                        <th><?php echo $this->lang->line('BE_PCK_15'); ?></th>
                                        <th><?php echo $this->lang->line('BE_MR_4'); ?></th>
                                        <th style="text-align:center">Action</th>
                                    </tr>
                                </thead>
                            <tbody>
                                <?php
                                if($count != 0)
                                {
                                    foreach($rsNewOrders as $row)
                                    {
                                    ?>
                                        <tr>
                                            <td class="highlight"><div class="success"></div><a href="#"><?php  echo stripslashes($row->LogPackageTitle);?></a></td>
                                            <td><?php  echo $row->APITitle == '' ? '-' : stripslashes($row->APITitle); ?></td>
                                            <td><?php  echo $row->TotalOrders;?></td>
                                            <td style="text-align:center" valign="middle">
                                                <a title="Take Action" href="<?php echo base_url('admin/services/logrequests?packId='.$row->LogPackageId.'&codeStatusId=4&searchType=3&txtFromDt='. $dt.'&oB=ASC');?>"><i class="fa fa-search-plus" style="font-size:16px;"></i></a>
                                            </td>
                                        </tr>
                                <?php  }?>
                                <?php  }else{ ?>
                                    <tr><td colspan="4"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td></tr>
                                <?php  } ?>			    			
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>