<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo 'Email Integrations'; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>
							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th>Email Provider Name</th>
										<th>API Key</th>
										<th>Group ID</th>
										<th>Edit</th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ($rsPMs) {
										foreach ($rsPMs as $row) {
											?>
											<tr>
												<td class="highlight">
													<div class="success"></div>
													<a href="<?php echo base_url('admin/clients/'); ?>emailintegration?id=<?php echo $row->Id; ?>"><?php echo stripslashes($row->APIName); ?></a>
												</td>
												<td><?php echo $row->APIKey; ?></td>
												<td><?php echo $row->GroupId; ?></td>
												<td style="text-align:center" valign="middle">
													<a href="<?php echo base_url('admin/clients/'); ?>emailintegration?id=<?php echo $row->Id; ?>">
														<i class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> 
													</a>
												</td>
											</tr>
											<?php
										}
									} else
										echo "<tr><td colspan='5'>" . $this->lang->line('BE_GNRL_9') . "</td></tr>";
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
