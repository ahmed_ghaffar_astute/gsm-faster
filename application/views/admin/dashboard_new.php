<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold"></i> <?php echo $this->lang->line('BE_LBL_4'); ?> <small><?php echo $this->lang->line('BE_LBL_541'); ?></small></span></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

		<div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
			<a href="<?php echo base_url('admin/dashboard');?>" class="btn btn-primary"><span class="fa fa-refresh"></span> Refresh Data</a>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">

	<!-- Main charts -->
	<div class="row">
		<div class="col-xl-7">

			<!-- Annually orders chart -->
			<div class="card">
				<div class="card-header header-elements-inline">
					<h6 class="card-title">Annually Completed Orders</h6>
					<div class="header-elements">
						<div class="form-check form-check-right form-check-switchery form-check-switchery-sm">
							<label class="form-check-label">

							</label>
						</div>
					</div>
				</div>

				<div class="card-body py-0">
					<div class="row">
						<div class="col-sm-12">
							<div class="d-flex align-items-center justify-content-center mb-2">
								<!--chart-->
								<?php $this->load->view('admin/chartscript') ?>
								<div id="chart_div" style="width: 760px; height: 300px"></div>
							</div>
						</div>
					</div>
				</div>

				<div class="chart position-relative" id="traffic-sources"></div>
			</div>
			<!-- /traffic sources -->

		</div>

		<div class="col-xl-5">

			<!-- Sales stats -->
			<div class="card">
				<div class="card-header header-elements-inline">
					<h6 class="card-title">Income</h6>
					<div class="header-elements">
						<h3 align="center" id="pdInvTD" style="color:#A6A6A6;"><strong><?php echo $DEFAULT_CURR_SYM.' '.roundMe($TD_INV_CREDITS); ?></strong></h3>
						<h3 align="center" id="pdInvMN" style="color:#A6A6A6; display:none;"><strong><?php echo $DEFAULT_CURR_SYM.' '.roundMe($MN_INV_CREDITS); ?></strong></h3>
						<h3 align="center" id="pdInvYR" style="color:#A6A6A6; display:none;"><strong><?php echo $DEFAULT_CURR_SYM.' '.roundMe($YR_INV_CREDITS); ?></strong></h3>
					</div>
				</div>

				<div class="card-body py-0">
					<div class="row text-center">
						<div class="col-12">
							<div class="mb-3">
								<h5 align="center"><?php echo $DEFAULT_CURR_ABB?></h5>
								<h5 align="center">Today</h5>
							</div>
						</div>
						<div class="col-4">
							<div class="mb-3">
								<a class="btn btn-info" href="Javascript:void(0);" id="lnkPITd">Today's<br />Income</a>
							</div>
						</div>

						<div class="col-4">
							<div class="mb-3">
								<a class="btn btn-primary" href="Javascript:void(0);" id="lnkPIMn">This<br />Month</a>
							</div>
						</div>

						<div class="col-4">
							<div class="mb-3">
								<a class="btn btn-primary" href="Javascript:void(0);" id="lnkPIYr">&nbsp;This&nbsp;<br />&nbsp;Year&nbsp;</a>
							</div>
						</div>
					</div>
				</div>

				<div class="chart mb-2" id="app_sales"></div>
				<div class="chart" id="monthly-sales-stats"></div>
			</div>
			<!-- /sales stats -->

		</div>
	</div>
	<!-- /main charts -->


	<!-- Dashboard content -->
	<div class="row">
		<div class="col-xl-7">
			<!-- Quick stats boxes -->
			<div class="card">
				<div class="card-header header-elements-sm-inline">
					<h6 class="card-title"><strong><?php echo $this->lang->line('BE_LBL_738'); ?></strong></h6>
					<div class="header-elements">

					</div>
				</div>
				<div class="card-body d-md-flex align-items-md-center justify-content-md-between flex-md-wrap">
					<?php if($rsStngs->IMEIServices == '1') { ?>
						<div class="col-lg-4">

							<!-- Members online -->
							<div class="card bg-teal-400">
								<div class="card-body">
									<div class="d-flex">
										<h3 class="font-weight-semibold mb-0"><?php echo $TOTAL_IMEI_ORDERS_P_IP == '' ? 0 : $TOTAL_IMEI_ORDERS_P_IP; ?></h3>
										<div class="list-icons ml-auto">
											<div class="dropdown">
												<a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"></a>
												<div class="dropdown-menu dropdown-menu-right">
													<a class="dropdown-item" href="JavaScript:void(0);" style="text-align:left"><strong><?php echo $this->lang->line('BE_MR_1'); ?></strong>
														<span class="pull-right" style="padding-left:10px;"><strong><?php  echo isset($TOTAL_IMEI_ORDERS_TODAY) ?: '0' ; ?></strong></span>
													</a>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/newimeiorders?dt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_PCK_12'); ?>
														<span class="pull-right" style="padding-left:10px;"><strong><?php echo isset($IMEI_ORDERS_COUNT_TODAY[1]) == '' ? 0 : $IMEI_ORDERS_COUNT_TODAY[1];?></strong></span>
													</a>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/codes?codeStatusId=4&searchType=3&txtFromDt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_LBL_739'); ?>
														<span class="pull-right" style="padding-left:10px;"><strong><?php echo isset($IMEI_ORDERS_COUNT_TODAY[4]) == '' ? 0 : $IMEI_ORDERS_COUNT_TODAY[4];?></strong></span>
													</a>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/codes?codeStatusId=2&searchType=3&txtFromDt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_LBL_401'); ?>
														<span class="pull-right" style="padding-left:10px;"><strong><?php echo isset($IMEI_ORDERS_COUNT_TODAY[2]) == '' ? 0 : $IMEI_ORDERS_COUNT_TODAY[2];?></strong></span>
													</a>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/codes?codeStatusId=3&searchType=3&txtFromDt='.$currDtOnly); ?>" style="text-align:left"><?php echo $this->lang->line('BE_LBL_740'); ?>
														<span class="pull-right" style="padding-left:10px;"><strong><?php echo isset($IMEI_ORDERS_COUNT_TODAY[3]) == '' ? 0 : $IMEI_ORDERS_COUNT_TODAY[3];?></strong></span>
													</a>
													<hr>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/codes?codeStatusId=2&searchType=3&txtFromDt='.$thisMonth);?>" style="text-align:left"><strong><?php echo $this->lang->line('BE_LBL_741'); ?></strong>
														<span class="pull-right" style="padding-left:10px;"><strong><?php echo $TOTAL_IMEI_ORDERS_M; ?></strong></span>
													</a>
													<hr>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/codes?codeStatusId=2&searchType=3&txtFromDt='.$thisYear);?>" style="text-align:left"><strong><?php echo $this->lang->line('BE_LBL_742'); ?></strong>
														<span class="pull-right" style="padding-left:10px;">
                                                            <strong><?php echo $TOTAL_IMEI_ORDERS_Y; ?></strong>
                                                        </span>
													</a>
													<hr>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/verifyorders?frmId=100&fTypeId=16');?>" style="text-align:left"><strong><?php echo $this->lang->line('BE_LBL_743'); ?></strong>
														<span class="pull-right" style="padding-left:10px;">
                                                            <strong><?php echo $IMEI_ORDERS_2_B_VERIFIED; ?></strong>
                                                        </span>
													</a>
												</div>
											</div>
										</div>
									</div>

									<div>
										<?php echo $this->lang->line('BE_LBL_246'); ?>
									</div>
								</div>
							</div>
							<!-- /members online -->

						</div>
					<?php } ?>
					<?php if($rsStngs->FileServices == '1') { ?>
						<div class="col-lg-4">

							<!-- Current server load -->
							<div class="card bg-pink-400">
								<div class="card-body">
									<div class="d-flex">
										<h3 class="font-weight-semibold mb-0"><?php echo $TOTAL_FILE_ORDERS_P_IP == '' ? 0 : $TOTAL_FILE_ORDERS_P_IP; ?></h3>
										<div class="list-icons ml-auto">
											<div class="dropdown">
												<a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"></a>
												<div class="dropdown-menu dropdown-menu-right">
													<a class="dropdown-item" href="JavaScript:void(0);" style="text-align:left"><strong><?php echo $this->lang->line('BE_MR_1'); ?></strong>
														<span class="pull-right" style="padding-left:10px;"><strong><?php echo isset($TOTAL_FILE_ORDERS_TODAY); ?></strong></span>
													</a>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/codesslbf?codeStatusId=1&searchType=1&txtFromDt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_PCK_12'); ?>
														<span class="pull-right" style="padding-left:10px;"><strong><?php echo isset($FILE_ORDERS_COUNT_TODAY[1]) == '' ? 0 : $FILE_ORDERS_COUNT_TODAY[1];?></strong></span>
													</a>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/codesslbf?codeStatusId=4&searchType=3&txtFromDt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_LBL_739'); ?>
														<span class="pull-right" style="padding-left:10px;"><strong><?php echo isset($FILE_ORDERS_COUNT_TODAY[4]) == '' ? 0 : $FILE_ORDERS_COUNT_TODAY[4];?></strong></span>
													</a>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/codesslbf?codeStatusId=2&searchType=3&txtFromDt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_LBL_401'); ?>
														<span class="pull-right" style="padding-left:10px;"><strong><?php echo isset($FILE_ORDERS_COUNT_TODAY[2]) == '' ? 0 : $FILE_ORDERS_COUNT_TODAY[2];?></strong></span>
													</a>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/codesslbf?codeStatusId=3&searchType=3&txtFromDt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_LBL_740'); ?>
														<span class="pull-right" style="padding-left:10px;"><strong><?php echo isset($FILE_ORDERS_COUNT_TODAY[3]) == '' ? 0 : $FILE_ORDERS_COUNT_TODAY[3];?></strong></span>
													</a>
													<hr>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/codesslbf?codeStatusId=2&searchType=3&txtFromDt='.$thisMonth);?>" style="text-align:left"><strong><?php echo $this->lang->line('BE_LBL_741'); ?></strong>
														<span class="pull-right" style="padding-left:10px;">
                                                            <strong><?php echo $TOTAL_FILE_ORDERS_M; ?></strong>
                                                        </span>
													</a>
													<hr>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/codesslbf?codeStatusId=2&searchType=3&txtFromDt='.$thisMonth);?>" style="text-align:left"><strong><?php echo $this->lang->line('BE_LBL_742'); ?></strong>
														<span class="pull-right" style="padding-left:10px;">
                                                            <strong><?php echo $TOTAL_FILE_ORDERS_Y; ?></strong>
                                                        </span>
													</a>
													<hr>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/verifyfileorders');?>" style="text-align:left"><strong><?php echo $this->lang->line('BE_LBL_743'); ?></strong>
														<span class="pull-right" style="padding-left:10px;">
                                                            <strong><?php echo $FILE_ORDERS_2_B_VERIFIED; ?></strong>
                                                        </span>
													</a>
												</div>
											</div>
										</div>
									</div>

									<div>
										<?php echo $this->lang->line('BE_LBL_249');?>
									</div>
								</div>

								<div id="server-load"></div>
							</div>
							<!-- /current server load -->

						</div>
					<?php } ?>
					<?php if($rsStngs->ServerServices == '1') { ?>
						<div class="col-lg-4">

							<!-- Today's revenue -->
							<div class="card bg-blue-400">
								<div class="card-body">
									<div class="d-flex">
										<h3 class="font-weight-semibold mb-0"><?php echo $TOTAL_SRVR_ORDERS_P_IP == '' ? 0 : $TOTAL_SRVR_ORDERS_P_IP; ?></h3>
										<div class="list-icons ml-auto">
											<div class="dropdown">
												<a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"></a>
												<div class="dropdown-menu dropdown-menu-right">
													<a class="dropdown-item" href="JavaScript:void(0);" style="text-align:left"><strong><?php echo $this->lang->line('BE_MR_1'); ?></strong>
														<span class="pull-right" style="padding-left:10px;"><strong><?php echo isset($TOTAL_SRVR_ORDERS_TODAY) ?: ''; ?></strong></span>
													</a>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/logrequests?codeStatusId=1&searchType=1&txtFromDt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_PCK_12'); ?>
														<span class="pull-right" style="padding-left:10px;"><strong><?php echo isset($SRVR_ORDERS_COUNT_TODAY[1]) == '' ? 0 : $SRVR_ORDERS_COUNT_TODAY[1];?></strong></span>
													</a>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/logrequests?codeStatusId=4&searchType=3&txtFromDt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_LBL_739'); ?>
														<span class="pull-right" style="padding-left:10px;"><strong><?php echo isset($SRVR_ORDERS_COUNT_TODAY[4]) == '' ? 0 : $SRVR_ORDERS_COUNT_TODAY[4];?></strong></span>
													</a>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/logrequests?codeStatusId=2&searchType=3&txtFromDt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_LBL_401'); ?>
														<span class="pull-right" style="padding-left:10px;"><strong><?php echo isset($SRVR_ORDERS_COUNT_TODAY[2]) == '' ? 0 : $SRVR_ORDERS_COUNT_TODAY[2];?></strong></span>
													</a>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/logrequests?codeStatusId=3&searchType=3&txtFromDt='.$currDtOnly);?> " style="text-align:left"><?php echo $this->lang->line('BE_LBL_740'); ?>
														<span class="pull-right" style="padding-left:10px;"><strong><?php echo isset($SRVR_ORDERS_COUNT_TODAY[3]) == '' ? 0 : $SRVR_ORDERS_COUNT_TODAY[3];?></strong></span>
													</a>
													<hr>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/logrequests?codeStatusId=2&searchType=3&txtFromDt='.$thisMonth);?>" style="text-align:left"><strong><?php echo $this->lang->line('BE_LBL_741'); ?></strong>
														<span class="pull-right" style="padding-left:10px;">
                                                            <strong><?php echo isset($TOTAL_SRVR_ORDERS_M) ?: ''; ?></strong>
                                                        </span>
													</a>
													<hr>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/logrequests?codeStatusId=2&searchType=3&txtFromDt='.$thisYear);?>" style="text-align:left"><strong><?php echo $this->lang->line('BE_LBL_742'); ?></strong>
														<span class="pull-right" style="padding-left:10px;">
                                                            <strong><?php echo $TOTAL_SRVR_ORDERS_Y; ?></strong>
                                                        </span>
													</a>
													<hr>
													<a class="dropdown-item" href="<?php echo base_url('admin/services/verifyserverorders');?>" style="text-align:left"><strong><?php echo $this->lang->line('BE_LBL_743'); ?></strong>
														<span class="pull-right" style="padding-left:10px;">
                                                            <strong><?php echo $SERVER_ORDERS_2_B_VERIFIED; ?></strong>
                                                        </span>
													</a>
												</div>
											</div>
										</div>
									</div>

									<div>
										<?php echo $this->lang->line('BE_LBL_252'); ?>

									</div>
								</div>

								<div id="today-revenue"></div>
							</div>
							<!-- /today's revenue -->

						</div>
					<?php } ?>
				</div>
			</div>
			<!-- Tabs -->
			<div class="card">
				<div class="card-header header-elements-inline">
					<h6 class="card-title"></h6>
					<div class="header-elements">
					</div>
				</div>

				<!-- Area chart -->
				<div id="messages-stats"></div>
				<!-- /area chart -->


				<!-- Tabs -->
				<ul class="nav nav-tabs nav-tabs-bottom nav-justified mb-0">
					<li class="nav-item">
						<a href="#tab_1_1" class="nav-link font-size-sm text-uppercase active" data-toggle="tab">
							<?php echo $this->lang->line('BE_LBL_549'); ?>
						</a>
					</li>

					<li class="nav-item">
						<a href="#tab_1_2" class="nav-link font-size-sm text-uppercase" data-toggle="tab">
							<?php echo $this->lang->line('BE_LBL_550'); ?>
						</a>
					</li>

					<li class="nav-item">
						<a href="#tab_1_3" class="nav-link font-size-sm text-uppercase" data-toggle="tab">
							<?php echo $this->lang->line('BE_LBL_551'); ?>
						</a>
					</li>
					<li class="nav-item">
						<a href="#tab_1_4" class="nav-link font-size-sm text-uppercase" data-toggle="tab">
							PreCode Stock
						</a>
					</li>
				</ul>
				<!-- /tabs -->


				<!-- Tabs content -->
				<div class="tab-content card-body">
					<div class="tab-pane active fade show" id="tab_1_1">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-advance table-hover">
								<thead>
								<tr>
									<th nowrap="nowrap" width="87%"><b><?php echo $this->lang->line('BE_PCK_20'); ?></b></th>
									<th style="text-align:center;" width="13%" nowrap="nowrap"><b><?php echo $this->lang->line('BE_MR_4'); ?></b></th>
								</tr>
								</thead>
								<tbody>
								<?php if($rsIMEIReqs)
								{
									foreach($rsIMEIReqs as $row)
									{
										?>
										<tr>
											<td><?php echo stripslashes($row->PackageTitle); ?></td>
											<td align="center"><?php echo $row->TotalRequests; ?></td>
										</tr>
									<?php }?>
									<?php
								}else{ ?>
									<tr><td colspan="2"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td></tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>

					<div class="tab-pane fade" id="tab_1_2">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-advance table-hover">
								<thead>
								<tr>
									<th nowrap="nowrap" width="87%"><b><?php echo $this->lang->line('BE_PCK_20'); ?></b></th>
									<th style="text-align:center;" width="13%" nowrap="nowrap"><b><?php echo $this->lang->line('BE_MR_4'); ?></b></th>
								</tr>
								</thead>
								<tbody>
								<?php if($rsFileReqs)
								{
									foreach($rsFileReqs as $row)
									{
										?>
										<tr>
											<td nowrap="nowrap"><?php echo stripslashes($row->PackageTitle); ?></td>
											<td align="center"><?php echo $row->TotalRequests; ?></td>
										</tr>
									<?php }?>
								<?php }else{ ?>
									<tr><td colspan="2"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td></tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>

					<div class="tab-pane fade" id="tab_1_3">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-advance table-hover">
								<thead>
								<tr>
									<th nowrap="nowrap" width="87%"><b><?php echo $this->lang->line('BE_PCK_20'); ?></b></th>
									<th style="text-align:center;" width="13%" nowrap="nowrap"><b><?php echo $this->lang->line('BE_MR_4'); ?></b></th>
								</tr>
								</thead>
								<tbody>
								<?php if($rsServerReqs)
								{
									foreach($rsServerReqs as $row)
									{
										?>
										<tr>
											<td nowrap="nowrap"><?php echo stripslashes($row->PackageTitle); ?></td>
											<td align="center"><?php echo $row->TotalRequests; ?></td>
										</tr>
									<?php }?>
								<?php }else{ ?>
									<tr><td colspan="2"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td></tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>

					<div class="tab-pane fade" id="tab_1_4">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-advance table-hover">
								<thead>
								<tr>
									<th nowrap="nowrap" width="87%"><b><?php echo $this->lang->line('BE_PCK_20'); ?></b></th>
									<th style="text-align:center;" width="13%" nowrap="nowrap"><b>Total Codes</b></th>
								</tr>
								</thead>
								<tbody>
								<?php if($rsPCs){
									foreach($rsPCs as $row)
									{
										?>
										<tr>
											<td nowrap="nowrap"><?php echo stripslashes($row->LogPackageTitle); ?></td>
											<td align="center"><?php echo $row->TotalPCs; ?></td>
										</tr>
									<?php }?>
								<?php }else{ ?>
									<tr><td colspan="2"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td></tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>

				</div>
				<!-- /tabs content -->

			</div>
			<!-- /Tabs -->

		</div>

		<div class="col-xl-5">

			<div class="card">
				<div class="card-header header-elements-inline">
					<h6 class="card-title">Frequently Used</h6>
					<div class="header-elements">
					</div>
				</div>

				<div class="card-body">
					<ul class="media-list">
						<li class="media">
							<div class="mr-3 position-relative">
								<a href="<?php echo base_url('admin/clients/users?frmId=26&fTypeId=5');?>" style="color:#3D3D3D; text-decoration:none;"><i class="fa fa-hand-o-right"></i></a>
							</div>

							<div class="media-body">
								<div class="d-flex justify-content-between">
									<a href="<?php echo base_url('admin/clients/users?frmId=26&fTypeId=5');?>">Clients</a>
								</div>
							</div>
						</li>

						<li class="media">
							<div class="mr-3 position-relative">
								<a href="<?php echo base_url('admin/reports/apisrpt?frmId=81&fTypeId=15');?>" style="color:#3D3D3D; text-decoration:none;"><i class="fa fa-hand-o-right"></i></a>
							</div>

							<div class="media-body">
								<div class="d-flex justify-content-between">
									<a href="<?php echo base_url('admin/reports/apisrpt?frmId=81&fTypeId=15');?>">API Stats</a>
								</div>
							</div>
						</li>

						<li class="media">
							<div class="mr-3">
								<a href="<?php echo base_url('admin/reports/profitlossrpt?frmId=82&fTypeId=15');?>" style="color:#3D3D3D; text-decoration:none;"><i class="fa fa-hand-o-right"></i></a>
							</div>

							<div class="media-body">
								<div class="d-flex justify-content-between">
									<a href="<?php echo base_url('admin/reports/profitlossrpt?frmId=82&fTypeId=15');?>">Profit Report</a>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>

			<!-- Daily sales
			<div class="card">
				<div class="card-header header-elements-inline">
					<h6 class="card-title">Daily sales stats</h6>
					<div class="header-elements">
						<span class="font-weight-bold ml-2">$4,378</span>
						<div class="list-icons ml-3">
							<div class="dropdown">
								<a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i></a>
								<div class="dropdown-menu dropdown-menu-right">
									<a href="#" class="dropdown-item"><i class="icon-sync"></i> Update data</a>
									<a href="#" class="dropdown-item"><i class="icon-list-unordered"></i> Detailed log</a>
									<a href="#" class="dropdown-item"><i class="icon-pie5"></i> Statistics</a>
									<div class="dropdown-divider"></div>
									<a href="#" class="dropdown-item"><i class="icon-cross3"></i> Clear list</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card-body">
					<div class="chart" id="sales-heatmap"></div>
				</div>

				<div class="table-responsive">
					<table class="table text-nowrap">
						<thead>
						<tr>
							<th class="w-100">Application</th>
							<th>Time</th>
							<th>Price</th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								<div class="d-flex align-items-center">
									<div class="mr-3">
										<a href="#" class="btn bg-primary-400 rounded-round btn-icon btn-sm">
											<span class="letter-icon"></span>
										</a>
									</div>
									<div>
										<a href="#" class="text-default font-weight-semibold letter-icon-title">Sigma application</a>
										<div class="text-muted font-size-sm"><i class="icon-checkmark3 font-size-sm mr-1"></i> New order</div>
									</div>
								</div>
							</td>
							<td>
								<span class="text-muted font-size-sm">06:28 pm</span>
							</td>
							<td>
								<h6 class="font-weight-semibold mb-0">$49.90</h6>
							</td>
						</tr>

						<tr>
							<td>
								<div class="d-flex align-items-center">
									<div class="mr-3">
										<a href="#" class="btn bg-danger-400 rounded-round btn-icon btn-sm">
											<span class="letter-icon"></span>
										</a>
									</div>
									<div>
										<a href="#" class="text-default font-weight-semibold letter-icon-title">Alpha application</a>
										<div class="text-muted font-size-sm"><i class="icon-spinner11 font-size-sm mr-1"></i> Renewal</div>
									</div>
								</div>
							</td>
							<td>
								<span class="text-muted font-size-sm">04:52 pm</span>
							</td>
							<td>
								<h6 class="font-weight-semibold mb-0">$90.50</h6>
							</td>
						</tr>

						<tr>
							<td>
								<div class="d-flex align-items-center">
									<div class="mr-3">
										<a href="#" class="btn bg-indigo-400 rounded-round btn-icon btn-sm">
											<span class="letter-icon"></span>
										</a>
									</div>
									<div>
										<a href="#" class="text-default font-weight-semibold letter-icon-title">Delta application</a>
										<div class="text-muted font-size-sm"><i class="icon-lifebuoy font-size-sm mr-1"></i> Support</div>
									</div>
								</div>
							</td>
							<td>
								<span class="text-muted font-size-sm">01:26 pm</span>
							</td>
							<td>
								<h6 class="font-weight-semibold mb-0">$60.00</h6>
							</td>
						</tr>

						<tr>
							<td>
								<div class="d-flex align-items-center">
									<div class="mr-3">
										<a href="#" class="btn bg-success-400 rounded-round btn-icon btn-sm">
											<span class="letter-icon"></span>
										</a>
									</div>
									<div>
										<a href="#" class="text-default font-weight-semibold letter-icon-title">Omega application</a>
										<div class="text-muted font-size-sm"><i class="icon-lifebuoy font-size-sm mr-1"></i> Support</div>
									</div>
								</div>
							</td>
							<td>
								<span class="text-muted font-size-sm">11:46 am</span>
							</td>
							<td>
								<h6 class="font-weight-semibold mb-0">$55.00</h6>
							</td>
						</tr>

						<tr>
							<td>
								<div class="d-flex align-items-center">
									<div class="mr-3">
										<a href="#" class="btn bg-danger-400 rounded-round btn-icon btn-sm">
											<span class="letter-icon"></span>
										</a>
									</div>
									<div>
										<a href="#" class="text-default font-weight-semibold letter-icon-title">Alpha application</a>
										<div class="text-muted font-size-sm"><i class="icon-spinner11 font-size-sm mr-2"></i> Renewal</div>
									</div>
								</div>
							</td>
							<td>
								<span class="text-muted font-size-sm">10:29 am</span>
							</td>
							<td>
								<h6 class="font-weight-semibold mb-0">$90.50</h6>
							</td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
			/daily sales -->
		</div>
	</div>
	<!-- /dashboard content -->

</div>
<!-- /content area -->
