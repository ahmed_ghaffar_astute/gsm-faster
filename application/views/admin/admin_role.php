<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_636'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>

					<div class="tabbable tabbable-custom boxless">
						<ul class="nav nav-tabs nav-tabs-solid rounded">
							<li class="nav-item">
								<a href="#tab_0" data-toggle="tab" class="nav-link active"><?php echo $this->lang->line('BE_LBL_380'); ?></a>
							</li>
							<?php if ($id > 0) { ?>
								<li class="nav-item">
									<a href="#tab_1" data-toggle="tab" class="nav-link"><?php echo $this->lang->line('BE_LBL_639'); ?></a>
								</li>
							<?php } ?>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_0">
								<?php echo $arole_tab_general; ?>
							</div>
							<?php if ($id > 0) { ?>
								<div class="tab-pane" id="tab_1">
									<?php echo $arole_tab_roles; ?>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

