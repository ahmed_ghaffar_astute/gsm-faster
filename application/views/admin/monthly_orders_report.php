<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo "Monthly Report"; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn <?php echo $sc != '0' ? 'purple' : 'btn-primary'; ?>"
									onclick="window.location.href='<?php echo base_url(); ?>admin/reports/monthlyordersrpt'"><?php echo $this->lang->line('BE_LBL_246'); ?></button>
							<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn <?php echo $sc != '1' ? 'purple' : 'btn-primary'; ?>"
									onclick="window.location.href='<?php echo base_url(); ?>admin/reports/monthlyordersrpt?sc=1'"><?php echo $this->lang->line('BE_LBL_249'); ?></button>
							<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn <?php echo $sc != '2' ? 'purple' : 'btn-primary'; ?>"
									onclick="window.location.href='<?php echo base_url(); ?>admin/reports/monthlyordersrpt?sc=2'"><?php echo $this->lang->line('BE_LBL_252'); ?></button>
							<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn <?php echo $sc != '3' ? 'purple' : 'btn-primary'; ?>"
									onclick="window.location.href='<?php echo base_url(); ?>admin/reports/monthlyordersrpt?sc=3'"><?php echo $this->lang->line('BE_LBL_590'); ?></button>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="page-title d-flex">
								<h4><i class="mr-2"></i> <?php echo $heading; ?></h4>
								<a href="#" class="header-elements-toggle text-default d-md-none"><i
										class="icon-more"></i></a>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">

							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th><b><?php echo $this->lang->line('BE_LBL_830'); ?></b></th>
										<th><b><?php echo $this->lang->line('BE_LBL_400'); ?></b></th>
										<th><b><?php echo $this->lang->line('BE_MR_7'); ?></b></th>
										<th><b><?php echo $this->lang->line('BE_LBL_401'); ?></b></th>
										<th><b><?php echo $this->lang->line('BE_LBL_740'); ?></b></th>
										<th><b><?php echo $this->lang->line('BE_MR_4'); ?></b></th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ($rsRpt) {
										foreach ($rsRpt as $row) {
											$totalOrders = $row->P + $row->IP + $row->CA + $row->NA;
											?>
											<tr>
												<td><?php echo $row->OrdersMonth . ', ' . $row->OrdersYear; ?></td>
												<td><?php echo $row->P; ?></td>
												<td><?php echo $row->IP; ?></td>
												<td><?php echo $row->CA; ?></td>
												<td><?php echo $row->NA; ?></td>
												<td><b><?php echo $totalOrders; ?></b></td>
											</tr>
											<?php
										}
									} else {
										?>
										<tr>
											<td colspan='6'><?php echo $this->lang->line('BE_GNRL_9') ?></td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
