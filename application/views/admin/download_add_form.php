<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i>Download</h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<a href="<?php echo base_url('admin/Miscellaneous/'); ?>downloads?frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"
			   class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<?php echo form_open('', array('class' => "form-horizontal", 'name' => "frm", 'method' => "post", 'id' => "frm_download", 'enctype' => "multipart/form-data")) ?>
					<!--<form class="form-horizontal" name="frm" method="post" id="frm" enctype="multipart/form-data">-->
					<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">

					<div class="form-group row">
						<label class="col-lg-3 control-label">Title:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<input type="text" class="form-control" id="title" placeholder="Enter Title" maxlength="100"
								   name="txtTitle" value="<? echo($title); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label for="txtImage" class="col-lg-3 control-label"><?php echo $this->lang->line('BE_N_3'); ?>
							:</label>
						<div class="col-lg-9">
							<input type="file" id="txtImage" name="txtImage">
							<p class="form-text text-muted">
								.jpg, .gif, .png, .zip
							</p>
						</div>
					</div>
						<div class="form-group row">
							<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_GNRL_1'); ?>:</label>
							<div class="col-lg-4">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="chkDisable"
										   id="chkDisable" <?php echo $disable == 1 ? 'checked' : ''; ?>
										   class="toggle"/><span><label for="chkDisable"></label></span>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3"></div>
						<div class="col-lg-9">
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary"><? echo $this->lang->line('BE_LBL_72'); ?></button>
						</div>
					</div>
					<?php echo form_close(); ?>
					<!--</form>-->
				</div>
			</div>
		</div>
	</div>

<script>
    $(document).ready(function()
    {
        function validate()
        {
            var errors = '';
            if ($("#title").val()=='')
                errors += "- Please Write title<br />";
            if (errors=='')
                return true;
            else
            {
                showStickyErrorToast(errors, BE_ERR, 'error');
                return false;
            }
        }
        $("#frm_download").submit(function(){
            return validate();
        });
    });
</script>
