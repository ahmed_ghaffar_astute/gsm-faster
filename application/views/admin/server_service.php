<!-- Page header -->
<div class="page-header border-bottom-0 pt-4 pl-3">
	<div class="form-group row">
		<div class="col-lg-12">
            <?php include APPPATH.'scripts/serverservices_header.php';?>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Page header -->
<div class="page-header border-bottom-0">
    <div class="page-header-content header-elements-md-inline">
        <div style="padding-top: 10px !important;" class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i>
                <?php echo $this->lang->line('BE_PCK_HD_6'); ?>
                <?php if($updatedAt != ''){
                    echo '('.$this->lang->line('BE_GNRL_5').':'.$updatedAt.')';
                }  ?>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

    </div>
</div>
<!-- /page header -->
<div class="content pt-0">
    
    <div class="card">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card-body">
                    <?php if (isset($message) && $message != '') { ?>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <div class="alert alert-success"><?php echo $message; ?></div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if (isset($strIgnoredPC) && $strIgnoredPC != '') { ?>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <div class="alert alert-success"> Few Pre Codes are duplicated and ignored:<br /><?php echo $strIgnoredPC; ?></div>
                            </div>
                        </div>
                    <?php } ?>
                
                    <div class="tabbable tabbable-custom boxless">
                        <ul class="nav nav-tabs nav-tabs-solid border-0">
                            <li class="<?php  echo $cldFrm == '0' ? 'active' : '' ?>" >
                                <a href="#tab_0" data-toggle="tab" class="nav-link <?php if($tab==1)echo ' active'; ?>"><?php echo $this->lang->line('BE_LBL_380'); ?></a>
                            </li>
                            <?php if($count > 0 && $id > 0) { ?>
                                <li>
                                    <a href="#tab_1" data-toggle="tab" class="nav-link <?php if($tab==2)echo ' active'; ?>"><?php echo $this->lang->line('BE_LBL_528'); ?></a>
                                </li>
                            <?php } ?>
                            <?php if($id > 0) { ?>
                                <li>
                                    <a href="#tab_2" data-toggle="tab" class="nav-link <?php if($tab==3)echo ' active'; ?>"><?php echo $this->lang->line('BE_PCK_15'); ?></a>
                                </li>
                                <li>
                                    <a href="#tab_6" data-toggle="tab" class="nav-link <?php if($tab==4)echo ' active'; ?>"><?php echo $this->lang->line('BE_LBL_377'); ?></a>
                                </li>
                            
                                <li>
                                    <a href="#tab_7" data-toggle="tab" class="nav-link <?php if($tab==5)echo ' active'; ?>"><?php echo $this->lang->line('BE_LBL_529'); ?></a>
                                </li>
                                <li>
                                    <a href="#tab_4" data-toggle="tab" class="nav-link <?php if($tab==6)echo ' active'; ?>"><?php echo $this->lang->line('BE_LBL_653'); ?></a>
                                </li>
                                <li>
                                    <a href="#tab_5" data-toggle="tab" class="nav-link <?php if($tab==7)echo ' active'; ?>"><?php echo $this->lang->line('BE_LBL_694'); ?></a>
                                </li>
                                <li class="<?php  echo $cldFrm != '0' ? 'active' : '' ?>" >
                                    <a href="#tab_8" data-toggle="tab" class="nav-link <?php if($tab==8) echo ' active'; ?>">Stock Manage</a>
                                </li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane<?php if($tab==1) echo ' active'; ?>" id="tab_0">
                                <?php echo $srvrservices_tab_general; ?>
                            </div>
                            <?php if($count > 0 && $id > 0) { ?>
                                <div class="tab-pane <?php if($tab==2) echo ' active'; ?>" id="tab_1">
                                    <?php echo $srvrservices_tab_prices; ?>
                                </div>
                            <?php } ?>
                            <?php if($id > 0) { ?>
                                <div class="tab-pane<?php if($tab==3) echo ' active'; ?>" id="tab_2">
                                    <?php echo $srvrservices_tab_api; ?>
                                </div>
                                <div class="tab-pane<?php if($tab==4) echo ' active'; ?>" id="tab_6">
                                    <?php echo $srvrservices_tab_fields; ?>
                                </div>
                                <div class="tab-pane<?php if($tab==5) echo ' active'; ?>" id="tab_7">
                                    <?php echo $services_tab_seo; ?>
                                </div>
                                <div class="tab-pane<?php if($tab==6) echo ' active'; ?>" id="tab_4">
                                    <?php echo $srvrservices_tab_precodes; ?>
                                </div>
                                <div class="tab-pane<?php if($tab==7) echo ' active'; ?>" id="tab_5">
                                    <?php echo $srvrservices_tab_sms;  ?>
                                </div>
                                <div class="tab-pane<?php if($tab==8) echo ' active'; ?>" id="tab_8">
                                    <?php echo $srvrsrvcs_precodes; ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
var base_url = '<?php echo base_url();?>' ;
</script>
<script language="javascript" src="<?php echo base_url('assets/js/logpackage.js');?>"></script>
