<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawVisualization);
    function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
        ['Year', 'IMEI', 'File', 'Server'],
        ['Jan',  <? echo isset($arrIMEIMO['1']) ? $arrIMEIMO['1'] : 0;?>, <? echo isset($arrFileMO['1']) ? $arrFileMO['1'] : 0;?>, <? echo isset($arrSrvrMO['1']) ? $arrSrvrMO['1'] : 0;?>],
        ['Feb',  <? echo isset($arrIMEIMO['2']) ? $arrIMEIMO['2'] : 0;?>, <? echo isset($arrFileMO['2']) ? $arrFileMO['2'] : 0;?>, <? echo isset($arrSrvrMO['2']) ? $arrSrvrMO['2'] : 0;?>],
        ['Mar',  <? echo isset($arrIMEIMO['3']) ? $arrIMEIMO['3'] : 0;?>, <? echo isset($arrFileMO['3']) ? $arrFileMO['3'] : 0;?>, <? echo isset($arrSrvrMO['3']) ? $arrSrvrMO['3'] : 0;?>],
        ['Apr',  <? echo isset($arrIMEIMO['4']) ? $arrIMEIMO['4'] : 0;?>, <? echo isset($arrFileMO['4']) ? $arrFileMO['4'] : 0;?>, <? echo isset($arrSrvrMO['4']) ? $arrSrvrMO['4'] : 0;?>],
        ['May',  <? echo isset($arrIMEIMO['5']) ? $arrIMEIMO['5'] : 0;?>, <? echo isset($arrFileMO['5']) ? $arrFileMO['5'] : 0;?>, <? echo isset($arrSrvrMO['5']) ? $arrSrvrMO['5'] : 0;?>],
        ['Jun',  <? echo isset($arrIMEIMO['6']) ? $arrIMEIMO['6'] : 0;?>, <? echo isset($arrFileMO['6']) ? $arrFileMO['6'] : 0;?>, <? echo isset($arrSrvrMO['6']) ? $arrSrvrMO['6'] : 0;?>],
        ['Jul',  <? echo isset($arrIMEIMO['7']) ? $arrIMEIMO['7'] : 0;?>, <? echo isset($arrFileMO['7']) ? $arrFileMO['7'] : 0;?>, <? echo isset($arrSrvrMO['7']) ? $arrSrvrMO['7'] : 0;?>],
        ['Aug',  <? echo isset($arrIMEIMO['8']) ? $arrIMEIMO['8'] : 0;?>,<? echo isset($arrFileMO['8']) ? $arrFileMO['8'] : 0;?>, <? echo isset($arrSrvrMO['8']) ? $arrSrvrMO['8'] : 0;?>],
        ['Sep',  <? echo isset($arrIMEIMO['9']) ? $arrIMEIMO['9'] : 0;?>, <? echo isset($arrFileMO['9']) ? $arrFileMO['9'] : 0;?>, <? echo isset($arrSrvrMO['9']) ? $arrSrvrMO['9'] : 0;?>],
        ['Oct',  <? echo isset($arrIMEIMO['10']) ? $arrIMEIMO['10'] : 0;?>, <? echo isset($arrFileMO['10']) ? $arrFileMO['10'] : 0;?>, <? echo isset($arrSrvrMO['10']) ? $arrSrvrMO['10'] : 0;?>],
        ['Nov',  <? echo isset($arrIMEIMO['11']) ? $arrIMEIMO['11'] : 0;?>, <? echo isset($arrFileMO['11']) ? $arrFileMO['11'] : 0;?>, <? echo isset($arrSrvrMO['11']) ? $arrSrvrMO['11'] : 0;?>],
        ['Dec',  <? echo isset($arrIMEIMO['12']) ? $arrIMEIMO['12'] : 0;?>, <? echo isset($arrFileMO['12']) ? $arrFileMO['12'] : 0;?>, <? echo isset($arrSrvrMO['12']) ? $arrSrvrMO['12'] : 0;?>]
        ]);
        
        var options = {
            legend: 'none',
            hAxis: { minValue: 0, maxValue: 30},
            pointSize: 7,
            series: {
                0: { pointShape: 'circle' }
            },
            curveType: 'function',
            legend: { position: 'bottom' },
            colors: ['#7AB5EF','#444348', '#90EC7D']
        };
        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }
</script>
