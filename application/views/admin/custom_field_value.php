<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_75'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<?php echo form_open('', 'class="form-horizontal"') ?>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_75'); ?>
							:*</label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter Value" maxlength="100"
								   name="txtVal" id="txtVal" value="<?php echo($val); ?>">
							<input type="hidden" id="id" name="id" value="<?php echo $id; ?>"/>
							<input type="hidden" id="fId" name="fId" value="<?php echo $fId; ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_GNRL_1'); ?>
							:*</label>
						<div class="col-lg-4">
							<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
								<input type="checkbox" name="chkDisable"
									   id="chkDisable" <?php echo $disable == 1 ? 'checked' : ''; ?>
									   class="toggle"/><span><label for="chkDisable"></label></span>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-4"></div>
						<div class="col-lg-8">
							<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									id="btnSave" name="btnSave"
									class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
							<img style="display:none;" id="dvLoader"
								 src="<?php echo base_url('assets/images/loading.gif'); ?>" border="0"
								 alt="Please wait..."/>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>

			</div>
		</div>
	</div>
</div>

<script>
	var base_url = '<?php echo base_url();?>';
</script>
<script language="javascript" src="<?php echo base_url('assets/js/customfieldval.js'); ?>"></script>
