<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_501'); ?></h5>
			</div>

			<div class="card-body">
				<div class="form-group row">
                    <label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_136'); ?>:</label>
                    <div class="col-lg-9">
						<div class="form-check form-check-inline mt-0">
						<label class="form-check-label mr-3">
                        <input type="radio" name="rdIMEIFType" id="rdIMEIFType1" value="0" checked  class="form-check-input"/><?php echo $this->lang->line('BE_LBL_137'); ?> </label>
						</div>
						<div class="form-check form-check-inline mt-0">
						<label class="form-check-label mr-3">
                        <input type="radio" name="rdIMEIFType" id="rdIMEIFType2" value="1" <?php if($imeiFType == '1') echo 'checked'; ?> class="form-check-input"/><?php echo $this->lang->line('BE_LBL_138'); ?> </label>
						</div>
						<div class="form-check form-check-inline mt-0">
						<label class="form-check-label">
                        <input type="radio" name="rdIMEIFType" value="2" id="rdIMEIFType3" <?php if($imeiFType == '2') echo 'checked'; ?> class="form-check-input"/><?php echo $this->lang->line('BE_LBL_139'); ?> </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_95'); ?>:</label>
                    <div class="col-lg-9">
                        <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                            <input type="checkbox" name="chkSm" id="chkSm" <?php echo $chkSm == 1 ? 'checked' : '';?> class="toggle"/><span><label for="chkSm"></label></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_260'); ?>:</label>
                    <div class="col-lg-9">
						<div class="form-check form-check-inline mt-0">
						<label class="form-check-label mr-3">
                        <input type="radio" name="rdIMEIOrderDD" id="rdIMEIOrderDD0" value="0" <? if($imeiDD == '0') echo 'checked'; ?> checked class="form-check-input"/><? echo $this->lang->line('BE_LBL_322'); ?> </label>
						</div>
						<div class="form-check form-check-inline mt-0">
						<label class="form-check-label">
                        <input type="radio" name="rdIMEIOrderDD" id="rdIMEIOrderDD1" value="1" <?php if($imeiDD == '1') echo 'checked'; ?> class="form-check-input"/><?php echo $this->lang->line('BE_LBL_323'); ?> </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_261'); ?>:</label>
                    <div class="col-lg-9">
						<div class="form-check form-check-inline mt-0">
							<label class="form-check-label mr-3">
                        <input type="radio"name="rdFileOrderDD" id="rdFileOrderDD0" value="0" <?php if($fileDD == '0') echo 'checked'; ?> checked class="form-check-input"/><?php echo $this->lang->line('BE_LBL_322'); ?> </label>
						</div>
						<div class="form-check form-check-inline mt-0">
							<label class="form-check-label mr-3">
                        <input type="radio" name="rdFileOrderDD" id="rdFileOrderDD1" value="1" <?php if($fileDD == '1') echo 'checked'; ?> class="form-check-input" /><?php echo $this->lang->line('BE_LBL_323'); ?> </label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_262'); ?>:</label>
                    <div class="col-lg-9">
                        <div class="form-check form-check-inline">
							<div class="form-check form-check-inline mt-0">
							<label class="form-check-label mr-3">
                            <input type="radio" name="rdServerOrderDD" id="rdServerOrderDD0" value="0" <?php if($serverDD == '0') echo 'checked'; ?> checked class="form-check-input"/><?php echo $this->lang->line('BE_LBL_322'); ?> </label>
							</div>
							<div class="form-check form-check-inline mt-0">
							<label class="form-check-label mr-3">
                            <input type="radio" name="rdServerOrderDD" id="rdServerOrderDD1" value="1" <?php if($serverDD == '1') echo 'checked'; ?> class="form-check-input"/><?php echo $this->lang->line('BE_LBL_323'); ?> </label>
							</div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_65').' '.$this->lang->line('BE_MENU_PCKGS'); ?>:</label>
                    <div class="col-lg-9">
                        <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                            <input type="checkbox" name="rdIMEI" id="rdIMEI" <? if($SHOW_IMEI_SERVICES == '1') echo 'checked';?> class="form-input-styled"/>
                            <span><label for="rdIMEI"></label></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_65').' '.$this->lang->line('BE_PCK_25'); ?>:</label>
                    <div class="col-lg-9">
                        <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                            <input type="checkbox" name="rdFile" id="rdFile" <? if($SHOW_FILE_SERVICES == '1') echo 'checked';?> class="form-input-styled"/>
                            <span><label for="rdFile"></label></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_65').' '.$this->lang->line('BE_PCK_HD_7'); ?>:</label>
                    <div class="col-lg-9">
                        <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                            <input type="checkbox" name="rdServer" id="rdServer" value="1" <? if($SHOW_SERVER_SERVICES == '1') echo 'checked';?>  class="form-input-styled"/>
                            <span><label for="rdServer"></label></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_603'); ?>:</label>
                    <div class="col-lg-9">
                        <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                            <input type="checkbox" name="chkShowPrices" id="chkShowPrices" <? if($SHOW_PRICES == '1') echo 'checked';?> class="form-input-styled"/>
                            <span><label for="chkShowPrices"></label></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Show Ecommerce Product Prices Without Login:</label>
                    <div class="col-lg-9">
                        <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                            <input type="checkbox" name="chkShowEPrices" id="chkShowEPrices" <? if($SHOW_EPRICES == '1') echo 'checked';?> class="form-input-styled"/>
                            <span><label for="chkShowEPrices"></label></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-9">
                        <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> id="btnSrvcs" name="btnSrvcs" class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
                        <img style="display:none;" id="dvLoader1" src="<?php echo base_url('assets/images/loading.gif');?>" border="0" alt="Please wait..." />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
