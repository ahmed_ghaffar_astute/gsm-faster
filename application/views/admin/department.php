			<!-- Page header -->
			<div class="page-header border-bottom-0">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_726'); ?></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>
					<div align="right">
						<a href="<?php echo base_url('admin/tickets/');?>departments?frmId=<?php echo $this->input->post_get('frmId');?>&fTypeId=<?php echo $this->input->post_get('fTypeId')?>" class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
					</div>

				</div>
			</div>
			<!-- /page header -->
			<!-- Content area -->
			<div class="content pt-0">
				<div class="card">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
							<div class="card-body">
								<!-- Our Working Area Start -->
								<?php if (isset($message) && $message != '') { ?>
									<div class="form-group row">
										<div class="col-lg-12">
											<div class="alert alert-success"><?php echo $message; ?></div>

										</div>
									</div>
								<?php } ?>
								<?php echo form_open('' , 'class="form-horizontal" name="frmChangePass"  id="frmChangePass"'); ?>
                       
                            <input type="hidden" id="id" name="id" value="<?php echo($id); ?>">			 
                            
                                <div class="form-group row">
                                    <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_726'); ?>:<span class="required_field">*</span></label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" placeholder="Enter <?php echo $this->lang->line('BE_LBL_726'); ?>" maxlength="100" name="txtDept" id="txtDept" value="<?php echo($dept);?>" />
                                    </div>
                                </div>
                                <div class="form-group row form-group-feedback form-group-feedback-left">
                                    <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_345'); ?>:<span class="required_field">*</span></label>
                                    <div class="col-lg-4">
										<input type="email" class="form-control" placeholder="Email Address" name="txtEmail" id="txtEmail" maxlength="50" value="<?php echo($email);?>">
										<div class="form-control-feedback form-control-feedback-lg">
											<i class="fa fa-envelope"></i>
										</div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_CR_2'); ?>:</label>
                                    <div class="col-lg-4">
                                        <textarea class="form-control" rows="3" id="txtDesc" name="txtDesc"><?php echo $desc; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_GNRL_1'); ?>:</label>
                                    <div class="col-lg-4">
                                        <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                                            <input type="checkbox" name="disable" id="disable" <?php echo $disable == 1 ? 'checked' : '';?> class="toggle chkSelect"/><span><label for="disable"></label></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-3"></div><div class="col-lg-9">
                                        <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" id="btnSave" name="btnSave"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
                                        <img style="display:none;" id="statusLoader" src="<?php echo base_url('assets/images/loading.gif');?>" border="0" alt="Please wait..." />
                                    </div>
                                </div>
                        <?php echo form_close(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>

<script language="javascript" src="<?php echo base_url('assets/js/languages/english.js');?>"></script>
<script language="javascript" src="<?php echo base_url('assets/js/department.js');?>"></script>
