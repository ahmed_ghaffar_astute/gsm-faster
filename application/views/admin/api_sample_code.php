<?php
$dir = base_url("assets/api_files/");
$file = $file_name;

	switch(strtolower(end(explode(".", $file)))) {
		case "zip": $type = "application/zip"; break;
		case "rar": $type = "application/x-rar-compressed"; break;
		default: $type = "application/octet-stream";
	}
	header("Content-Type: $type");
	header("Content-Disposition: attachment; filename=\"$file\"");
	readfile($dir.$file);
	exit; // you should exit here to prevent the file from becoming corrupted if anything else gets echo'd after this function was called.

?>
