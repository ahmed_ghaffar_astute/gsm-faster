<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_PM_5'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<a href="<?php echo base_url('admin/sales/'); ?>paymentmethods?frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"
			   class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<?php if (isset($errmessage) && $errmessage != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-danger"><?php echo $errmessage; ?></div>

							</div>
						</div>
					<?php } ?>

					<?php echo form_open_multipart(base_url('admin/sales/paymentmethod'), 'class="form-horizontal" name="frm" method="post" id="frm"') ?>
					<input type="hidden" id="id" name="id" value="<?php echo($id); ?>"/>
					<input type="hidden" id="existingImage" name="existingImage" value="<?php echo $existingImage; ?>">

					<div class="form-group row">
						<?php $pay_method = fetch_pay_method_types(); ?>
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_365'); ?>:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<select name="typeId" id="typeId" class="form-control select2me">
								<option value="0">Select...</option>
								<?php FillCombo($typeId, $pay_method); ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<?php $currency_data = fetch_currency_info(); ?>
						<label class="col-lg-3 control-label">Restricted Currency:</label>
						<div class="col-lg-4">
							<select name="rstCurrId" class="form-control select2me">
								<option value="0">Select...</option>
								<?php FillCombo($rstCurrId, $currency_data); ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PM_5'); ?>:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter Payment Method" maxlength="100"
								   name="txtPM" id="txtPM" value="<?php echo($pMethod); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label for="txtImage" class="col-lg-3 control-label"><?php echo $this->lang->line('BE_N_3'); ?>
							:</label>
						<div class="col-lg-9">
							<input type="file" id="txtImage" name="txtImage">
							<p class="form-text text-muted">
								.jpg, .gif, .png
								<br/>IDEAL Size: Width = 120, Height = Around 80
							</p>
							<?php if ($existingImage != '') { ?>
								<p align="center" valign="middle"><img src="<?php echo $existingImage ?>"/></p>
							<?php } ?>

						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PM_3'); ?>:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter Username" maxlength="255"
								   name="txtUName" id="txtUName" value="<?php echo($userName); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label">Mass Payment Fee Type:</label>
						<div class="col-lg-6 radio-list">
							<div class="form-check form-check-inline mt-0">
							<label class="form-check-label">
								<input type="radio" class="form-check-input" name="rdFeeType" value="0" checked/>Fee Applied By
								PayPal </label>
							</div>
							<div class="form-check form-check-inline mt-0">
							<label class="form-check-label">
								<input type="radio" class="form-check-input" name="rdFeeType"
									   value="1" <?php if ($feeType == '1') echo 'checked'; ?> />Fee Set In Backend
							</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_125'); ?>:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter Payment Fee" maxlength="5"
								   name="txtFee" id="txtFee" value="<?php echo($fee); ?>" required="required"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label">VAT (If any):</label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter VAT" maxlength="5" name="txtVat"
								   value="<?php echo($vat); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label">Retail Payment Fee:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter Retail Fee" maxlength="5"
								   name="txtRFee" id="txtRFee" value="<?php echo($rFee); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label">Shop Payment Fee:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter Shop Payment Fee" maxlength="5"
								   name="txtSFee" id="txtSFee" value="<?php echo($sFee); ?>"/>
						</div>
					</div>
					<div class="form-group" <?php if ($typeId != '2') echo ' style="display:none;"'; ?> id="dvMP">
						<label class="col-lg-3 control-label">Mass Payment Expiry:</label>
						<div class="col-lg-4">
							<select id="massPayDays" name="massPayDays" class="form-control select2me"
									data-placeholder="Select...">
								<option value="0" <?php if ($massPayDays == '0') echo 'selected'; ?>>Do Not Apply
								</option>
								<option value="1" <?php if ($massPayDays == '1') echo 'selected'; ?>>1 Day</option>
								<option value="2" <?php if ($massPayDays == '2') echo 'selected'; ?>>2 Days</option>
								<option value="3" <?php if ($massPayDays == '3') echo 'selected'; ?>>3 Days</option>
								<option value="4" <?php if ($massPayDays == '4') echo 'selected'; ?>>4 Days</option>
								<option value="5" <?php if ($massPayDays == '5') echo 'selected'; ?>>5 Days</option>
							</select>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_730'); ?>:</label>
						<div class="col-lg-4">
							<input type="text" class="form-control"
								   placeholder="Enter <?php echo $this->lang->line('BE_LBL_730'); ?>" maxlength="100"
								   name="txtAPIUN" value="<?php echo($apiUN); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_731'); ?>:</label>
						<div class="col-lg-4">
							<input type="text" class="form-control"
								   placeholder="Enter <?php echo $this->lang->line('BE_LBL_731'); ?>" maxlength="100"
								   name="txtAPIPwd" value="<?php echo($apiPwd); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_732'); ?>:</label>
						<div class="col-lg-4">
							<input type="text" class="form-control"
								   placeholder="Enter <?php echo $this->lang->line('BE_LBL_732'); ?>" maxlength="100"
								   name="txtAPISign" value="<?php echo($apiSign); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-3"><?php echo $this->lang->line('BE_CR_2'); ?>:</label>
						<div class="col-lg-9">
							<textarea class="ckeditor form-control" name="txtDesc"
									  rows="6"><?php echo $desc; ?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label">Enable in WholeSale:</label>
						<div class="col-lg-4">
							<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
								<input type="checkbox" name="chkWS"
									   id="chkWS" <?php echo $wholeSale == 1 ? 'checked' : ''; ?>
									   class="toggle chkSelect"/><span><label for="chkWS"></label></span>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label">Enable in Retail:</label>
						<div class="col-lg-4">
							<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
								<input type="checkbox" name="chkRetail"
									   id="chkRetail" <?php echo $retail == 1 ? 'checked' : ''; ?>
									   class="toggle chkSelect"/><span><label for="chkRetail"></label></span>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label">Enable in Shop:</label>
						<div class="col-lg-4">
							<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
								<input type="checkbox" name="chkShop"
									   id="chkShop" <?php echo $shop == 1 ? 'checked' : ''; ?> class="toggle chkSelect"/><span><label for="chkShop"></label></span>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_GNRL_1'); ?>:</label>
						<div class="col-lg-4">
							<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
								<input type="checkbox" name="chkDisable"
									   id="chkDisable" <?php echo $disable == 1 ? 'checked' : ''; ?>
									   class="toggle chkSelect"/><span><label for="chkDisable"></label></span>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3"></div>
						<div class="col-lg-9">
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
						</div>
					</div>


					<?php echo form_close(); ?>
					<!-- END FORM-->
				</div>
			</div>
		</div>
	</div>
</div>

