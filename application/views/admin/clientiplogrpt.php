<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_56'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<?
					$USER_ID_BTNS = $userId;
					if ($USER_ID_BTNS > 0) { ?>
						<div class="portlet box blue">
							<div class="portlet-title">
								<?php include APPPATH . 'scripts/userbtns.php'; ?>
							</div>
						</div>
					<?php } ?>
					<?php echo form_open(base_url('admin/reports/clientiplogrpt?frmId=93&fTypeId=15'), array('class' => "horizontal-form", 'method' => "post")); ?>
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group">
								<label
									class="control-label"><? echo $this->lang->line('BE_PM_3'); ?></label>
								<input type="text" name="txtUName" value="<? echo($uName); ?>"
									   class="form-control">
							</div>
						</div>
						<!--/span-->
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label">IP</label>
								<input type="text" name="txtIP" value="<? echo($ip); ?>"
									   class="form-control">
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group">
								<label
									class="control-label"><? echo $this->lang->line('BE_PM_7'); ?></label><br/>
								<input class="form-control form-control-inline input-largest date-picker"
									   type="text" name="txtFromDt" value="<? echo($dtFrom); ?>"/>
							</div>
						</div>
						<!--/span-->
						<div class="col-lg-6">
							<div class="form-group">
								<label
									class="control-label"><? echo $this->lang->line('BE_PM_8'); ?></label><br/>
								<input class="form-control form-control-inline input-largest date-picker"
									   type="text" name="txtToDt" value="<? echo($dtTo); ?>"/>
							</div>
						</div>
						<!--/span-->
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary"><? echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
							<input type="hidden" name="cldFrm" id="cldFrm" value="0"/>
							<input type="hidden" name="byAdmin" id="byAdmin" value="<? echo $byAdmin; ?>"/>
							<input type="hidden" name="userId" id="userId" value="<? echo $userId; ?>"/>
						</div>
					</div>

					<?php echo form_close(); ?>
					<?php
					if (!$rsIpRpt) {

						//$totalRows = $row->TotalRecs;
						$totalRows = 5;
						if ($totalRows > $limit)
							doPages($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next);
					}
					?>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="page-title d-flex">
								<h4><i class="mr-2"></i> <?php echo $this->lang->line('BE_LBL_56'); ?>
								</h4>
								<a href="#" class="header-elements-toggle text-default d-md-none"><i
										class="icon-more"></i></a>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th><?php echo $this->lang->line('BE_INDEX_USRNAME'); ?></th>
										<th><?php echo $this->lang->line('BE_LBL_43'); ?></th>
										<th><?php echo $this->lang->line('BE_LBL_44'); ?></th>
										<th><?php echo $this->lang->line('BE_LBL_45'); ?></th>
									</tr>
									</thead>
									<tbody>
									<?
									if ($rsIpRpt) {
										$i = 0;
										foreach ($rsIpRpt as $row) {
											?>
											<tr>
												<td class="highlight">
													<div class="success"></div>
													<a style="text-decoration:underline;"
													   href="<? echo base_url("admin/clients/users?id=") . $row->UserId ?>"><? echo($row->UserName); ?></a>
												</td>
												<td><? echo $row->IP; ?></td>
												<td><? echo $row->LoginTime; ?></td>
												<td><? echo $row->LogoutTime; ?></td>
											</tr>
											<?
											$i++;
										}
									} else {
										?>
										<tr>
											<td colspan='4'><?php echo $this->lang->line('BE_GNRL_9'); ?></td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

