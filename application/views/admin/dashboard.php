<div class="page-content">
    <div class="row PaddingBtm20">
        <div class="col-md-12">
            <h2><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('BE_LBL_4'); ?> <small><?php echo $this->lang->line('BE_LBL_541'); ?></small></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">  
                <div class="row">
                    <div class="col-md-8 ">
                        <div class="portlet-body">
                            <?php $this->load->view('admin/chartscript') ?>
                            <div id="chart_div" style="width: 760px; height: 300px"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div align="right" style="padding-right:55px;"><a href="<?php echo base_url('admin/dashboard');?>" class="btn btn-sm green"><span class="fa fa-refresh"></span> Refresh Data</a></div>
                        <br /><br /><br />
                        <h4 class="alert-heading" align="center"><strong>Income</strong></h4>
                        <h3 align="center" id="pdInvTD" style="color:#A6A6A6;"><strong><?php echo $DEFAULT_CURR_SYM.' '.roundMe($TD_INV_CREDITS); ?></strong></h3>
                        <h3 align="center" id="pdInvMN" style="color:#A6A6A6; display:none;"><strong><?php echo $DEFAULT_CURR_SYM.' '.roundMe($MN_INV_CREDITS); ?></strong></h3>
                        <h3 align="center" id="pdInvYR" style="color:#A6A6A6; display:none;"><strong><?php echo $DEFAULT_CURR_SYM.' '.roundMe($YR_INV_CREDITS); ?></strong></h3>
                        <h5 align="center"><?php echo $DEFAULT_CURR_ABB?></h5>
                        <h5 align="center">Today</h5>
                        <p align="center">
                            <a class="btn btn-info" href="Javascript:void(0);" id="lnkPITd">Today's<br />Income</a>
                            <a class="btn btn-primary" href="Javascript:void(0);" id="lnkPIMn">This<br />Month</a>
                            <a class="btn btn-primary" href="Javascript:void(0);" id="lnkPIYr">&nbsp;This&nbsp;<br />&nbsp;Year&nbsp;</a>
                        </p>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-7" style="padding-left:120px;">
                        <h4 align="center"><strong><?php echo $this->lang->line('BE_LBL_738'); ?></strong></h4>
                        <br />
                        <div class="portlet-body">
                            <div class="row">
                                <?php if($rsStngs->IMEIServices == '1') { ?>
                                    <div class="col-md-4">
                                        <div class="easy-pie-chart">
                                            <div class="number transactions" data-percent="<?php echo $TOTAL_IMEI_ORDERS_P_IP == '' ? 0 : $TOTAL_IMEI_ORDERS_P_IP; ?>">
                                                <span>
                                                    <?php echo $TOTAL_IMEI_ORDERS_P_IP == '' ? 0 : $TOTAL_IMEI_ORDERS_P_IP; ?>
                                                </span>
                                            </div>
                                            <br />
                                            <div class="btn-group">
                                                <button class="btn default dropdown-toggle" data-toggle="dropdown"><?php echo $this->lang->line('BE_LBL_246'); ?> <i class="fa fa-angle-down"></i></button>
                                                <ul class="dropdown-menu" role="menu" style="width:250px;">
                                                    <li role="presentation">
                                                        <a role="menuitem" href="JavaScript:void(0);" style="text-align:left"><strong><?php echo $this->lang->line('BE_MR_1'); ?></strong>
                                                        <span class="pull-right" style="padding-right:10px;"><strong><?php  echo isset($TOTAL_IMEI_ORDERS_TODAY) ?: '' ; ?></strong></span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/newimeiorders?dt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_PCK_12'); ?>
                                                            <span class="pull-right" style="padding-right:10px;"><strong><?php echo isset($IMEI_ORDERS_COUNT_TODAY[1]) == '' ? 0 : $IMEI_ORDERS_COUNT_TODAY[1];?></strong></span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/codes?codeStatusId=4&searchType=3&txtFromDt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_LBL_739'); ?>
                                                            <span class="pull-right" style="padding-right:10px;"><strong><?php echo isset($IMEI_ORDERS_COUNT_TODAY[4]) == '' ? 0 : $IMEI_ORDERS_COUNT_TODAY[4];?></strong></span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/codes?codeStatusId=2&searchType=3&txtFromDt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_LBL_401'); ?>
                                                            <span class="pull-right" style="padding-right:10px;"><strong><?php echo isset($IMEI_ORDERS_COUNT_TODAY[2]) == '' ? 0 : $IMEI_ORDERS_COUNT_TODAY[2];?></strong></span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/codes?codeStatusId=3&searchType=3&txtFromDt='.$currDtOnly); ?>" style="text-align:left"><?php echo $this->lang->line('BE_LBL_740'); ?>
                                                            <span class="pull-right" style="padding-right:10px;"><strong><?php echo isset($IMEI_ORDERS_COUNT_TODAY[3]) == '' ? 0 : $IMEI_ORDERS_COUNT_TODAY[3];?></strong></span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation" class="divider"></li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/codes?codeStatusId=2&searchType=3&txtFromDt='.$thisMonth);?>" style="text-align:left"><strong><?php echo $this->lang->line('BE_LBL_741'); ?></strong>
                                                        <span class="pull-right" style="padding-right:10px;"><strong><?php echo $TOTAL_IMEI_ORDERS_M; ?></strong></span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation" class="divider"></li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/codes?codeStatusId=2&searchType=3&txtFromDt='.$thisYear);?>" style="text-align:left"><strong><?php echo $this->lang->line('BE_LBL_742'); ?></strong>
                                                        <span class="pull-right" style="padding-right:10px;">
                                                            <strong><?php echo $TOTAL_IMEI_ORDERS_Y; ?></strong>
                                                        </span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation" class="divider"></li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/verifyorders?frmId=100&fTypeId=16');?>" style="text-align:left"><strong><?php echo $this->lang->line('BE_LBL_743'); ?></strong>
                                                        <span class="pull-right" style="padding-right:10px;">
                                                            <strong><?php echo $IMEI_ORDERS_2_B_VERIFIED; ?></strong>
                                                        </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="margin-bottom-10 visible-sm">
                                    </div>
                                <?php } ?>
                                
                                <?php if($rsStngs->FileServices == '1') { ?>
                                    <div class="col-md-4">
                                        <div class="easy-pie-chart">
                                            <div class="number transactions easyPieChart" data-percent="<?php echo $TOTAL_FILE_ORDERS_P_IP == '' ? 0 : $TOTAL_FILE_ORDERS_P_IP; ?>">
                                                <span>
                                                    <?php echo $TOTAL_FILE_ORDERS_P_IP == '' ? 0 : $TOTAL_FILE_ORDERS_P_IP; ?>
                                                </span>
                                            </div>
                                            <br />
                                            <div class="btn-group">
                                                <button class="btn default dropdown-toggle" data-toggle="dropdown"><?php echo $this->lang->line('BE_LBL_249');?> 
                                                    <i class="fa fa-angle-down"></i>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" style="width:250px;">
                                                    <li role="presentation">
                                                        <a role="menuitem" href="JavaScript:void(0);" style="text-align:left"><strong><?php echo $this->lang->line('BE_MR_1'); ?></strong>
                                                        <span class="pull-right" style="padding-right:10px;"><strong><?php echo isset($TOTAL_FILE_ORDERS_TODAY); ?></strong></span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/codesslbf?codeStatusId=1&searchType=1&txtFromDt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_PCK_12'); ?>
                                                            <span class="pull-right" style="padding-right:10px;"><strong><?php echo isset($FILE_ORDERS_COUNT_TODAY[1]) == '' ? 0 : $FILE_ORDERS_COUNT_TODAY[1];?></strong></span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/codesslbf?codeStatusId=4&searchType=3&txtFromDt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_LBL_739'); ?>
                                                            <span class="pull-right" style="padding-right:10px;"><strong><?php echo isset($FILE_ORDERS_COUNT_TODAY[4]) == '' ? 0 : $FILE_ORDERS_COUNT_TODAY[4];?></strong></span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/codesslbf?codeStatusId=2&searchType=3&txtFromDt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_LBL_401'); ?>
                                                            <span class="pull-right" style="padding-right:10px;"><strong><?php echo isset($FILE_ORDERS_COUNT_TODAY[2]) == '' ? 0 : $FILE_ORDERS_COUNT_TODAY[2];?></strong></span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/codesslbf?codeStatusId=3&searchType=3&txtFromDt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_LBL_740'); ?>
                                                            <span class="pull-right" style="padding-right:10px;"><strong><?php echo isset($FILE_ORDERS_COUNT_TODAY[3]) == '' ? 0 : $FILE_ORDERS_COUNT_TODAY[3];?></strong></span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation" class="divider"></li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/codesslbf?codeStatusId=2&searchType=3&txtFromDt='.$thisMonth);?>" style="text-align:left"><strong><?php echo $this->lang->line('BE_LBL_741'); ?></strong>
                                                        <span class="pull-right" style="padding-right:10px;">
                                                            <strong><?php echo $TOTAL_FILE_ORDERS_M; ?></strong>
                                                        </span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation" class="divider"></li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/codesslbf?codeStatusId=2&searchType=3&txtFromDt='.$thisMonth);?>" style="text-align:left"><strong><?php echo $this->lang->line('BE_LBL_742'); ?></strong>
                                                        <span class="pull-right" style="padding-right:10px;">
                                                            <strong><?php echo $TOTAL_FILE_ORDERS_Y; ?></strong>
                                                        </span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation" class="divider"></li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/verifyfileorders');?>" style="text-align:left"><strong><?php echo $this->lang->line('BE_LBL_743'); ?></strong>
                                                        <span class="pull-right" style="padding-right:10px;">
                                                            <strong><?php echo $FILE_ORDERS_2_B_VERIFIED; ?></strong>
                                                        </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                
                                        </div>
                                    </div>
                                    <div class="margin-bottom-10 visible-sm">
                                    </div>
                                <?php } ?>
                                <?php if($rsStngs->ServerServices == '1') { ?>
                                    <div class="col-md-4">
                                        <div class="easy-pie-chart">
                                            <div class="number transactions" data-percent="<?php echo $TOTAL_SRVR_ORDERS_P_IP == '' ? 0 : $TOTAL_SRVR_ORDERS_P_IP; ?>">
                                                <span>
                                                    <?php echo $TOTAL_SRVR_ORDERS_P_IP == '' ? 0 : $TOTAL_SRVR_ORDERS_P_IP; ?>
                                                </span>
                                            </div>
                
                                            <br />
                                            <div class="btn-group ">
                                                <button class="btn default dropdown-toggle" data-toggle="dropdown"><?php echo $this->lang->line('BE_LBL_252'); ?> <i class="fa fa-angle-down"></i>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" style="width:250px;">
                                                    <li role="presentation">
                                                        <a role="menuitem" href="JavaScript:void(0);" style="text-align:left"><strong><?php echo $this->lang->line('BE_MR_1'); ?></strong>
                                                        <span class="pull-right" style="padding-right:10px;"><strong><?php echo isset($TOTAL_SRVR_ORDERS_TODAY) ?: ''; ?></strong></span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/logrequests?codeStatusId=1&searchType=1&txtFromDt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_PCK_12'); ?>
                                                            <span class="pull-right" style="padding-right:10px;"><strong><?php echo isset($SRVR_ORDERS_COUNT_TODAY[1]) == '' ? 0 : $SRVR_ORDERS_COUNT_TODAY[1];?></strong></span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/logrequests?codeStatusId=4&searchType=3&txtFromDt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_LBL_739'); ?>
                                                            <span class="pull-right" style="padding-right:10px;"><strong><?php echo isset($SRVR_ORDERS_COUNT_TODAY[4]) == '' ? 0 : $SRVR_ORDERS_COUNT_TODAY[4];?></strong></span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/logrequests?codeStatusId=2&searchType=3&txtFromDt='.$currDtOnly);?>" style="text-align:left"><?php echo $this->lang->line('BE_LBL_401'); ?>
                                                            <span class="pull-right" style="padding-right:10px;"><strong><?php echo isset($SRVR_ORDERS_COUNT_TODAY[2]) == '' ? 0 : $SRVR_ORDERS_COUNT_TODAY[2];?></strong></span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/logrequests?codeStatusId=3&searchType=3&txtFromDt='.$currDtOnly);?> " style="text-align:left"><?php echo $this->lang->line('BE_LBL_740'); ?>
                                                            <span class="pull-right" style="padding-right:10px;"><strong><?php echo isset($SRVR_ORDERS_COUNT_TODAY[3]) == '' ? 0 : $SRVR_ORDERS_COUNT_TODAY[3];?></strong></span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation" class="divider"></li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/logrequests?codeStatusId=2&searchType=3&txtFromDt='.$thisMonth);?>" style="text-align:left"><strong><?php echo $this->lang->line('BE_LBL_741'); ?></strong>
                                                        <span class="pull-right" style="padding-right:10px;">
                                                            <strong><?php echo isset($TOTAL_SRVR_ORDERS_M) ?: ''; ?></strong>
                                                        </span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation" class="divider"></li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/logrequests?codeStatusId=2&searchType=3&txtFromDt='.$thisYear);?>" style="text-align:left"><strong><?php echo $this->lang->line('BE_LBL_742'); ?></strong>
                                                        <span class="pull-right" style="padding-right:10px;">
                                                            <strong><?php echo $TOTAL_SRVR_ORDERS_Y; ?></strong>
                                                        </span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation" class="divider"></li>
                                                    <li role="presentation">
                                                        <a role="menuitem" href="<?php echo base_url('admin/services/verifyserverorders');?>" style="text-align:left"><strong><?php echo $this->lang->line('BE_LBL_743'); ?></strong>
                                                        <span class="pull-right" style="padding-right:10px;">
                                                            <strong><?php echo $SERVER_ORDERS_2_B_VERIFIED; ?></strong>
                                                        </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                
                                        </div>
                                    </div>
                                    <div class="margin-bottom-10 visible-sm">
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">&nbsp;</div>
                    <div class="col-md-4" style="padding-left:55px;">
                        <h4 class="alert-heading"><strong>Frequently Used</strong></h4>
                        <br />
                        <table class="" width="100%">
                            <tbody>
                                <tr height="35px;">
                                    <td>
                                        <a href="<?php echo base_url('admin/clients/users?frmId=26&fTypeId=5');?>" style="color:#3D3D3D; text-decoration:none;"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Clients</a>
                                    </td>
                                </tr>
                                <tr height="35px;">
                                    <td>
                                        <a href="<?php echo base_url('admin/reports/apisrpt?frmId=81&fTypeId=15');?>" style="color:#3D3D3D; text-decoration:none;"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;API Stats</a>
                                    </td>
                                </tr>
                                <tr height="35px;">
                                    <td>
                                        <a href="<?php echo base_url('admin/reports/profitlossrpt?frmId=82&fTypeId=15');?>" style="color:#3D3D3D; text-decoration:none;"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Profit Report</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <br /><br /><br />
                        <div class="portlet-body">
                            <div class="tabbable tabbable-custom">
                                <ul class="nav nav-tabs nav-tabs-solid border-0">
                                    <li class="active">
                                        <a href="#tab_1_1" data-toggle="tab"><?php echo $this->lang->line('BE_LBL_549'); ?></a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_2" data-toggle="tab"><?php echo $this->lang->line('BE_LBL_550'); ?></a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_3" data-toggle="tab"><?php echo $this->lang->line('BE_LBL_551'); ?></a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_4" data-toggle="tab">PreCode Stock</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1_1">
                                        <div class="scroller" style="height: 430px;" data-always-visible="1" data-rail-visible="0">
                                            <div class="portlet-body">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-advance table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th nowrap="nowrap" width="87%"><b><?php echo $this->lang->line('BE_PCK_20'); ?></b></th>
                                                            <th style="text-align:center;" width="13%" nowrap="nowrap"><b><?php echo $this->lang->line('BE_MR_4'); ?></b></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php if($rsIMEIReqs)
                                                            {
                                                                foreach($rsIMEIReqs as $row)
                                                                {
                                                                ?>
                                                                    <tr>
                                                                        <td><?php echo stripslashes($row->PackageTitle); ?></td>
                                                                        <td align="center"><?php echo $row->TotalRequests; ?></td>
                                                                    </tr>
                                                                <?php }?>
                                                            <?php 
                                                            }else{ ?>
                                                                <tr><td colspan="2"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td></tr>
                                                            <?php } ?>			    			
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_1_2">
                                        <div class="scroller" style="height: 430px;" data-always-visible="1" data-rail-visible1="1">
                                            <div class="portlet-body">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-advance table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th nowrap="nowrap" width="87%"><b><?php echo $this->lang->line('BE_PCK_20'); ?></b></th>
                                                                <th style="text-align:center;" width="13%" nowrap="nowrap"><b><?php echo $this->lang->line('BE_MR_4'); ?></b></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php if($rsFileReqs)
                                                        {
                                                            foreach($rsFileReqs as $row)
                                                            {
                                                            ?>
                                                                <tr>
                                                                    <td nowrap="nowrap"><?php echo stripslashes($row->PackageTitle); ?></td>
                                                                    <td align="center"><?php echo $row->TotalRequests; ?></td>
                                                                </tr>
                                                            <?php }?>
                                                        <?php }else{ ?>
                                                            <tr><td colspan="2"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td></tr>
                                                        <?php } ?>			    			
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_1_3">
                                        <div class="scroller" style="height: 430px;" data-always-visible="1" data-rail-visible1="1">
                                            <div class="portlet-body">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-advance table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th nowrap="nowrap" width="87%"><b><?php echo $this->lang->line('BE_PCK_20'); ?></b></th>
                                                                <th style="text-align:center;" width="13%" nowrap="nowrap"><b><?php echo $this->lang->line('BE_MR_4'); ?></b></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php if($rsServerReqs)
                                                        {
                                                            foreach($rsServerReqs as $row)
                                                            {
                                                            ?>
                                                                <tr>
                                                                    <td nowrap="nowrap"><?php echo stripslashes($row->PackageTitle); ?></td>
                                                                    <td align="center"><?php echo $row->TotalRequests; ?></td>
                                                                </tr>
                                                            <?php }?>
                                                        <?php }else{ ?>
                                                            <tr><td colspan="2"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td></tr>
                                                        <?php } ?>			    			
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_1_4">
                                        <div class="scroller" style="height: 430px;" data-always-visible="1" data-rail-visible1="1">

                                            <div class="portlet-body">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-advance table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th nowrap="nowrap" width="87%"><b><?php echo $this->lang->line('BE_PCK_20'); ?></b></th>
                                                                <th style="text-align:center;" width="13%" nowrap="nowrap"><b>Total Codes</b></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php if($rsPCs){
                                                            foreach($rsPCs as $row)
                                                            {
                                                            ?>
                                                                <tr>
                                                                    <td nowrap="nowrap"><?php echo stripslashes($row->LogPackageTitle); ?></td>
                                                                    <td align="center"><?php echo $row->TotalPCs; ?></td>
                                                                </tr>
                                                            <?php }?>
                                                        <?php }else{ ?>
                                                            <tr><td colspan="2"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td></tr>
                                                        <?php } ?>			    			
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" style="padding-left:50px;">
                        <br /><br /><br />
                        <h4><strong><?php echo $this->lang->line('BE_LBL_230'); ?></strong></h4>
                        <br />
                        <div class="portlet-body">
                            <table class="table" width="100%">
                                <tbody>
                                    <?php if($rsStngs->IMEIServices == '1'){ ?>
                                    <tr>
                                        <td width="30%">
                                            <a href="<?php echo base_url('admin/services/');?>newimeiorders?frmId=96&fTypeId=16">
                                                <span class="pull-left" style="font-size:16px; color:#3D3D3D;">
                                                    <b><?php echo $NEW_IMEI_ORDERS;?></b>
                                                </span>
                                            </a>
                                        </td>
                                        <td width="70%">New IMEI Orders</td>
                                    </tr>
                                    <tr>
                                        <td width="30%">
                                            <a href="verifyorders.php?frmId=100&fTypeId=16">
                                                <span class="pull-left" style="font-size:16px; color:#3D3D3D;">
                                                    <b><?php echo $IMEI_ORDERS_2_B_VERIFIED;?></b>
                                                </span>
                                            </a>
                                        </td>
                                        <td width="70%">IMEI Orders For<br />Verification</td>
                                    </tr>
                                    <?
                                    }
                                    if($rsStngs->FileServices == '1')
                                    { ?>
                                    <tr>
                                        <td width="30%">
                                            <a href="<?php echo base_url('admin/services/newfileorders?frmId=103&fTypeId=17');?>">
                                                <span class="pull-left" style="font-size:16px; color:#3D3D3D;">
                                                    <b><?php echo $NEW_FILE_ORDERS;?></b>
                                                </span>
                                            </a>
                                        </td>
                                        <td width="70%">New File Orders</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="<?php echo base_url('admin/services/verifyfileorders?frmId=106&fTypeId=17');?>">
                                                <span class="pull-left" style="font-size:16px; color:#3D3D3D;">
                                                    <strong><?php echo $FILE_ORDERS_2_B_VERIFIED;?></strong>
                                                </span>
                                            </a>
                                        </td>
                                        <td>File Orders For<br />Verification</td>
                                    </tr>
                                    <?
                                    }
                                    if($rsStngs->ServerServices == '1')
                                    { ?>
                                    <tr>
                                        <td width="30%">
                                            <a href="<?php echo base_url('admin/services/');?>newserverorders?frmId=107&fTypeId=18">
                                                <span class="pull-left" style="font-size:16px; color:#3D3D3D;">
                                                    <b><?php echo $NEW_SERVER_ORDERS;?></b>
                                                </span>
                                            </a>
                                        </td>
                                        <td width="70%">New Server Orders</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="<?php echo base_url('admin/services/verifyserverorders?frmId=110&fTypeId=18');?>">
                                                <span class="pull-left" style="font-size:16px; color:#3D3D3D;">
                                                    <strong><?php echo $SERVER_ORDERS_2_B_VERIFIED;?></strong>
                                                </span>
                                            </a>
                                        </td>
                                        <td>Server Orders For<br />Verification</td>
                                    </tr>
                                    <?php
                                    } ?>
                                    <tr>
                                        <td colspan="2" style="color:#3D3D3D;"><strong><?php echo $this->lang->line('BE_LBL_242');?></strong></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="<?php echo base_url('admin/clients/payments?pStatusId=1&byAdmin=0&frmId=37&fTypeId=7');?>">
                                                <span class="pull-left" style="font-size:16px; color:#3D3D3D;">
                                                    <strong><?php echo $PENDING_INV;?></strong>
                                                </span>
                                            </a>
                                        </td>
                                        <td><?php echo roundMe($UNPAID_USR_INV).' '.$DEFAULT_CURR_ABB;?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="color:#3D3D3D;"><strong><?php echo $this->lang->line('BE_LBL_241');?></strong></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="<?php echo base_url('admin/clients/payments?pStatusId=1&byAdmin=1&frmId=37&fTypeId=7');?>">
                                                <span class="pull-left" style="font-size:16px; color:#3D3D3D;">
                                                    <strong><?php echo $PENDING_INV_ADMIN;?></strong>
                                                </span>
                                            </a>
                                        </td>
                                        <td><?php echo roundMe($UNPAID_ADMIN_INV).' '.$DEFAULT_CURR_ABB;?></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="<?php echo base_url('admin/tickets/tickets?sId=1&frmId=52&fTypeId=12');?>">
                                                <span class="pull-left" style="font-size:16px; color:#3D3D3D;">
                                                    <strong><?php echo isset($TICKETS->NEW) ? $TICKETS->NEW : 0; ?></strong>
                                                </span>
                                            </a>
                                        </td>
                                        <td>New Tickets</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="<?php echo base_url('admin/clients/');?>users?du=1&frmId=26&fTypeId=5">
                                                <span class="pull-left" style="font-size:16px; color:#3D3D3D;">
                                                    <strong><?php echo $USER_ACT;?></strong>
                                                </span>
                                            </a>
                                        </td>
                                        <td>Clients Awaiting For<br />Activation</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script language="javascript" src="<?php echo base_url('assets/js/dashboard.js');?>"></script>

