<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_371'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>                <?php echo form_open(base_url('admin/settings/maintenance'), array('name' => 'frm', 'id' => 'frm', 'class' => 'form-horizontal')); ?>
					<?php /*if (isset($message) && $message != '') { */?><!--
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php /*echo $message; */?></div>
							</div>
						</div>
					--><?php /*} */?>
					<form action="#" class="form-horizontal" name="frm" method="post" id="frm">
						<div class="form-group row">
							<label class="col-lg-3 col-form-label"><? echo $this->lang->line('BE_LBL_372'); ?>:</label>
							<div class="col-lg-9">
								<textarea class="ckeditor" name="txtText" rows="6"><? echo $text; ?></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-3 control-label">Put Site On Maintenance:</label>
							<div class="col-lg-4">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="disable" id="disable" <?php echo $disable == 1 ? 'checked' : ''; ?> class="toggle"/><span><label for="disable"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-3"></div>
							<div class="col-lg-9">
								<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
										class="btn btn-primary" name="btnSave"
										value="1"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
							</div>
						</div>
						<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

		<script src="<?php echo base_url('assets/plugins/ckeditor/ckeditor.js') ?>"></script>
