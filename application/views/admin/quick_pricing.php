<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_514'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<!-- Copy till this to paste to other pages -->
					<form action="#">
						<fieldset>
							<div class="form-group row">
								<label class="col-lg-3 col-form-label">Service Category</label>
								<div class="col-lg-9">
									<?php $package_data = fetch_custom_log_package_category($tblCat , $strWhereCat);?>
									<select name="categoryId" id="categoryId" class="js-select2 form-control select2me" data-placeholder="<?php echo $this->lang->line('BE_LBL_256'); ?>">
										<option value="0"><?php echo $this->lang->line('BE_LBL_255'); ?></option>
										<?php FillCombo(0 , $package_data); ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-lg-3 col-form-label">Type:</label>
								<div class="col-lg-9">
									<fieldset id="group1">
										<div class="form-check form-check-inline">
											<div class="radiobuttons">
												<div class="rdio rdio-primary radio-inline"> <input name="group1" value="1" id="radio1" type="radio" checked>
													<label for="radio1">Increase By</label>
												</div>
											</div>
										</div>
										<div class="form-check form-check-inline">
											<div class="radiobuttons">
												<div class="rdio rdio-primary radio-inline">
													<input name="group1" value="2" id="radio2" type="radio">
													<label for="radio2">Decrease By</label>
												</div>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-lg-3 col-form-label">Price:</label>
								<div class="col-lg-9">
									<input type="number" class="form-control" placeholder="Enter Price">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-lg-3 col-form-label">Pricing:</label>
								<div class="col-lg-9">
									<fieldset id="group2">
										<div class="form-check form-check-inline">
											<div class="radiobuttons">
												<div class="rdio rdio-primary radio-inline"> <input name="group2" value="1" id="radio4" type="radio" checked>
													<label for="radio4">Fixed</label>
												</div>
											</div>
										</div>
										<div class="form-check form-check-inline">
											<div class="radiobuttons">
												<div class="rdio rdio-primary radio-inline">
													<input name="group2" value="2" id="radio3" type="radio">
													<label for="radio3">Percentage</label>
												</div>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</fieldset>
						<div class="form-group row">
							<label class="col-lg-3 col-form-label">Update Prices Of Other Currencies:</label>
							<div class="col-lg-9">
								<div class="switch-button switch-button-lg">
									<input type="checkbox" name="item3" id="item3" />
									<span><label for="item3"></label></span>
								</div>
							</div>
						</div>
						<div class="text-right">
							<button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
						</div>
					</form>
					<!-- Our Working Area End -->
				</div>
			</div>
		</div>
	</div>

</div>
<!-- /content area -->
