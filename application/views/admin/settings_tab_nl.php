<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_29'); ?></h5>
			</div>
			<div class="card-body">
            <div class="form-group row">
                <label class="col-lg-4 col-form-label"><?php echo $this->lang->line('BE_LBL_656'); ?>:</label>
                <div class="col-md-4">
                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                        <input type="checkbox" name="chkSNL" id="chkSNL" <?php echo $sendNL == 1 ? 'checked' : '';?> class="toggle"/><span><label for="chkSNL"></label></span>
                    </div>
                </div>
            </div>
			<div class="form-group row">
				<div class="col-md-offset-4 col-md-8">
					<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> id="btnSendNL" name="btnSendNL" class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
					&nbsp;<img style="display:none;" id="dvLoaderNL" src="<?php echo base_url('assets/images/loading.gif');?>" border="0" alt="Please wait..." />
				</div>
			</div>
		</div>
	</div>
</div>
</div>
