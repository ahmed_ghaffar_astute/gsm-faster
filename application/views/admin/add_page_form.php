<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_MR_HD_12'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div class="form-check form-check-right form-check-switchery form-check-switchery-sm">
			<a href="<?php echo base_url('admin/cms/') ?>pages?frmId=<? echo $this->input->post_get('frmId');?>&fTypeId=<? echo $this->input->post_get('fTypeId')?>&type=<? echo $this->input->post_get('type')?>" class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<?php echo form_open(base_url('admin/cms/page?id=' . $id), array('class' => "form-horizontal", 'name' => "frm", 'method' => "post", 'id' => "frm", 'enctype' => "multipart/form-data")) ?>
					<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
					<input type="hidden" name="type" value="<? echo $type; ?>"/>
					<input type="hidden" id="existingImage" name="existingImage" value="<?php echo $existingImage; ?>">

						<div class="form-group row">
							<label class="col-md-3 control-label"><? echo $this->lang->line('BE_MR_34'); ?>:<span class="required_field">*</span></label>
							<div class="col-md-4">
								<input type="text" class="form-control" placeholder="Enter Title" maxlength="100"
									   name="txtLnkTitle" id="txtLnkTitle" value="<? echo($lnkTitle); ?>"/>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 control-label"><? echo $this->lang->line('BE_MR_35'); ?>:<span class="required_field">*</span></label>
							<div class="col-md-4">
								<input type="text" class="form-control" placeholder="Enter Page Title" maxlength="100"
									   name="txtPageTitle" id="txtPageTitle" value="<? echo($pageTitle); ?>"/>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 control-label"><? echo $this->lang->line('BE_LBL_212'); ?>:</label>
							<div class="col-md-4">
								<input type="text" class="form-control" placeholder="Enter HTML Title" maxlength="100"
									   name="txtHTMLTitle" id="txtHTMLTitle" value="<? echo($htmlTitle); ?>"/>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 control-label"><? echo $this->lang->line('BE_LBL_213'); ?>:</label>
							<div class="col-md-4">
								<input type="text" class="form-control" placeholder="Enter SEO Name" maxlength="100"
									   name="txtSEOName" id="txtSEOName" value="<? echo($seoName); ?>"/>
							</div>
						</div>
						<div class="form-group row">
							<label for="txtImage"
								   class="col-md-3 control-label"><? echo $this->lang->line('BE_PCK_3'); ?>:</label>
							<div class="col-md-9">
								<input type="file" id="txtImage" name="txtImage">
								<p class="form-text text-muted">
									.jpg, .gif, .png
								</p>
								<div>
									<?php if($existingImage != '') { ?>
										<p valign="middle"><img src="<?php echo base_url().$existingImage ?>"  width="400px" height="auto"/></p>
									<?php } ?>

								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 control-label"><? echo $this->lang->line('BE_LBL_104'); ?>:</label>
							<div class="col-md-4">
								<input type="text" class="form-control" placeholder="Enter File Name" maxlength="100"
									   name="txtFileName" id="txtFileName" value="<? echo($fileName); ?>"/>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 control-label">URL Other than Your website:</label>
							<div class="col-md-4">
								<input type="text" class="form-control" placeholder="Enter URL" maxlength="100"
									   name="txtURL" value="<? echo($url); ?>"/>
							</div>
							<span class="form-text text-muted">
                                            http://www.example.com/
                                        </span>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-3"><? echo $this->lang->line('BE_MR_39'); ?>:</label>
							<div class="col-md-9">
								<textarea class="ckeditor form-control" name="txtPageText"
										  rows="6"><? echo $pageText; ?></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 control-label"><? echo $this->lang->line('BE_MR_38'); ?>:</label>
							<div class="col-md-4">
								<textarea class="form-control" rows="3" id="txtMetaTags"
										  name="txtMetaTags"><? echo $metaTags; ?></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 control-label"><? echo $this->lang->line('BE_LBL_214'); ?>:</label>
							<div class="col-md-4">
								<textarea class="form-control" rows="3" id="txtMetaKW"
										  name="txtMetaKW"><? echo $metaKW; ?></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 control-label"><? echo $this->lang->line('BE_MR_40'); ?>:</label>
							<div class="col-md-4">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="chkHeaderLnk"
										   id="chkHeaderLnk" <?php echo $headerLnk == 1 ? 'checked' : ''; ?>
										   class="toggle"/><span><label for="chkHeaderLnk"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 control-label"><? echo $this->lang->line('BE_MR_41'); ?>:</label>
							<div class="col-md-4">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="chkFooterLnk"
										   id="chkFooterLnk" <?php echo $footerLnk == 1 ? 'checked' : ''; ?>
										   class="toggle"/><span><label for="chkFooterLnk"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 control-label">Sub Menu Link:</label>
							<div class="col-md-4">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="chkSubMenuLnk"
										   id="chkSubMenuLnk" <?php echo $subMenuLnk == 1 ? 'checked' : ''; ?>
										   class="toggle"/><span><label for="chkSubMenuLnk"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 control-label"><? echo $this->lang->line('BE_LBL_734'); ?>:</label>
							<div class="col-md-4">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="chkHeaderLnkCP"
										   id="chkHeaderLnkCP" <?php echo $headerLnkCP == 1 ? 'checked' : ''; ?>
										   class="toggle"/><span><label for="chkHeaderLnkCP"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 control-label"><? echo $this->lang->line('BE_LBL_735'); ?>:</label>
							<div class="col-md-4">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="chkFooterLnkCP"
										   id="chkFooterLnkCP" <?php echo $footerLnkCP == 1 ? 'checked' : ''; ?>
										   class="toggle"/><span><label for="chkFooterLnkCP"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 control-label">Activate Retail:</label>
							<div class="col-md-4">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="chkActRtl"
										   id="chkActRtl" <?php echo $actRtl == 1 ? 'checked' : ''; ?>
										   class="toggle"/><span><label for="chkActRtl"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 control-label"><? echo $this->lang->line('BE_GNRL_1'); ?>:</label>
							<div class="col-md-4">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="chkDisable"
										   id="chkDisable" <?php echo $disablePage == 1 ? 'checked' : ''; ?>
										   class="toggle"/><span><label for="chkDisable"></label></span>
								</div>
							</div>
						</div>

					<div class="form-group row">
						<div class="col-md-3"></div>
						<div class="col-lg-9">
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary"><? echo $this->lang->line('BE_LBL_72'); ?></button>
						</div>
					</div>
					</form>
					<!-- END FORM-->
				</div>
			</div>

		</div>
	</div>
</div>
</div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/plugins/ckeditor/ckeditor.js"></script>
<script language="javascript" src="<?php echo base_url(); ?>/assets/js/page.js"></script>
