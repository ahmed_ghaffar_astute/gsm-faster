<?php echo form_open(base_url('admin/system/adminrole?mn=10#tab_0'), ' class="form-horizontal well" id="frmARole" name="frmARole"'); ?>
		<div class="form-group row">
			<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_636'); ?>:<span class="required_field">*</span> </label>
			<div class="col-lg-4">
				<input type="text" class="form-control" placeholder="Enter text" maxlength="100" name="txtARole" id="txtARole" value="<?php echo($aRole); ?>">
			</div>
		</div>
		<div class="form-group row">
			<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_GNRL_1'); ?>:</label>
			<div class="col-lg-4">
				<div class="switch-button switch-button-lg">
					<input type="checkbox" name="chkDisable" id="chkDisable" <?php echo $disable == 1 ? 'checked' : ''; ?>/>
        			<span><label for="chkDisable"></label></span>
				</div>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-lg-3">
				<div class="col-lg-9">
					<input type="hidden" name="id" id="id" value="<?php echo $id; ?>"/>
					<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> id="btnGeneral" name="btnGeneral" class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
				</div>
			</div>
		</div>

<?php echo form_close(); ?>
