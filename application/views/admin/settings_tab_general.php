<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_500'); ?></h5>
			</div>

			<div class="card-body">

				<div class="form-group row">
					<label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_MR_28'); ?>:<span class="required_field">*</span></label>
					<div class="col-lg-9">
						<input type="text" class="form-control" placeholder="Enter text" maxlength="100" name="txtCompany" id="txtCompany" value="<?php echo($company);?>" >
						<input type="hidden" id="customDsgn" name="customDsgn" value="<? echo $customDesign; ?>" />
					</div>
				</div>

				<div class="form-group row">
					<label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_MR_29'); ?>:<span class="required_field">*</span></label>
					<div class="col-lg-9">
						<input type="text" class="form-control" placeholder="Enter text" maxlength="100" name="txtFName" id="txtFName" value="<?php echo($fromName);?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_MR_31'); ?>:<span class="required_field">*</span></label>
					<div class="col-lg-9">
						<input type="text" class="form-control" placeholder="Enter text" name="txtURL" maxlength="100" id="txtURL" value="<?php echo($url);?>">
						<span class="form-text text-muted">
							www.example.com
						</span>
					</div>

				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-form-label">Admin Panel Title:<span class="required_field">*</span></label>
					<div class="col-lg-9">
						<input type="text" class="form-control" placeholder="Enter Admin Panel Title" maxlength="100" name="txtATitle" id="txtATitle" value="<? echo $aTitle;?>" />
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-form-label">Website Title:<span class="required_field">*</span></label>
					<div class="col-lg-9">
						<input type="text" class="form-control" placeholder="Enter Website Title" maxlength="100" name="txtSTitle" id="txtSTitle" value="<? echo $sTitle;?>" />
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-form-label">Shop Website Title:<span class="required_field">*</span></label>
					<div class="col-lg-9">
						<input type="text" class="form-control" placeholder="Enter Shop Website Title" maxlength="100" name="txtShTitle" id="txtShTitle" value="<? echo $shopSTitle;?>" />
					</div>
				</div>

				<div class="form-group row">
					<label class="col-lg-3 col-form-label">Copyrights:<span class="required_field">*</span></label>
					<div class="col-lg-9">
						<input type="text" class="form-control" placeholder="Enter Copyrights" maxlength="100" name="txtCopyright" id="txtCopyright" value="<? echo $copyRights;?>" />
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_MR_30'); ?>:<span class="required_field">*</span></label>
					<div class="col-lg-9">
						<div class="form-group form-group-feedback form-group-feedback-left">
							<input type="email" class="form-control" placeholder="Email Address" name="txtFEmail" id="txtFEmail" maxlength="50" value="<?php echo($fromEmail);?>">
							<div class="form-control-feedback form-control-feedback-lg">
								<i class="fa fa-envelope"></i>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-form-label">To Email:<span class="required_field">*</span></label>
					<div class="col-lg-9">
						<div class="form-group form-group-feedback form-group-feedback-left">
							<input type="email" class="form-control" placeholder="To Email Address" name="txtTEmail" id="txtTEmail" maxlength="50" value="<?php echo($toEmail);?>" >
							<div class="form-control-feedback form-control-feedback-lg">
								<i class="fa fa-envelope"></i>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-form-label">Contact Us Email:<span class="required_field">*</span></label>
					<div class="col-lg-9">
						<div class="form-group form-group-feedback form-group-feedback-left">
							<input type="email" class="form-control" placeholder="To Email Address" name="txtCUEmail" id="txtCUEmail" maxlength="50" value="<?php echo($contactEmail);?>" >
							<div class="form-control-feedback form-control-feedback-lg">
								<i class="fa fa-envelope"></i>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_USR_6'); ?>:</label>
					<div class="col-lg-9">
						<input type="text" class="form-control" placeholder="Enter text" name="txtPhone" maxlength="50" id="txtPhone" value="<?php echo($phone);?>" />
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-form-label">Google Captcha Site Key:<span class="required_field">*</span></label>
					<div class="col-lg-9">
						<input type="text" class="form-control" placeholder="Enter Google Captcha Site Key" name="txtGCSiteKey" maxlength="100" id="txtGCSiteKey" required="required" value="<? echo($gCaptchaSiteKey);?>" />
						<span class="form-text text-muted">
							<a target="_blank" href="https://www.google.com/recaptcha/admin#list" style="text-decoration:underline;">Get Google Captcha Keys</a>
							<br />Register your site here by choosing the option reCAPTCHA V2
						</span>
					</div>

				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-form-label">Google Captcha Secret Key:<span class="required_field">*</span></label>
					<div class="col-lg-9">
						<input type="text" class="form-control" placeholder="Enter Google Captcha Secret Key" name="txtGCSecKey" maxlength="100" id="txtGCSecKey" required="required" value="<? echo($gCaptchaSecretKey);?>" />
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-form-label"><? echo $this->lang->line('BE_LBL_39'); ?>:</label>
						<div class="col-lg-3">
							<select id="dtTmDiffType" name="dtTmDiffType" class="form-control select2me">
								<option value="0" selected >Choose</option>
								<option value="+" <?php if($dtTmDiffType == '+') echo 'selected';?>>Add</option>
								<option value="-" <?php if($dtTmDiffType == '-') echo 'selected';?>>Subtract</option>
							</select>
						</div>
						<div class="col-lg-3">
							<select id="dtTmDiffHours" name="dtTmDiffHours" class="form-control select2me">
								<option value="0" selected >Choose</option>
								<?
								for($z = 1; $z <= 24; $z++)
								{
									$selected = '';
									if($dtTmDiffHours == $z)
										$selected = 'selected';
									echo '<option value="'.$z.'" '.$selected.'>'.$z.'</option>';
								}
								?>
							</select>
						</div>
						<div class="col-lg-3">
							<select id="dtTmDiffMins" name="dtTmDiffMins" class="form-control select2me">
								<option value="0" selected >Choose</option>
								<?
								for($z = 1; $z <= 60; $z++)
								{
									$selected = '';
									if($dtTmDiffMins == $z)
										$selected = 'selected';
									echo '<option value="'.$z.'" '.$selected.'>'.$z.'</option>';
								}
								?>
							</select>

						</div>
					<div class="col-lg-3">&nbsp;</div>
					<div class="col-lg-9">
						<span class="form-text text-muted">
							<? echo $this->lang->line('BE_LBL_607'). ":".date('Y-m-d H:i:s'); ?>
						</span>
					</div>

				</div>
				<!--
					<div class="form-group row">
						<label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_39'); ?>:</label>
						<div class="col-lg-9">
							<input class="form-control form-control-inline input-largest date-picker" type="text" name="txtLclDt" id="txtLclDt" value="<? echo($lclDt);?>" readonly />
						</div>
						<div class="col-md-2">
							<div class="input-group bootstrap-timepicker">
								<input type="text" class="form-control timepicker-24" name="txtLclTm" id="txtLclTm" value="<? echo $lclTm; ?>">
								<span class="input-group-btn">
									<button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
								</span>
							</div>
						</div>
						<span class="form-text text-muted">
							<? echo "$this->lang->line('BE_LBL_607'): ".date('Y-m-d H:i:s'); ?>
						</span>
					</div>-->
				<div class="form-group row">
					<label class="col-lg-3 col-form-label">Email Signature:</label>
					<div class="col-lg-9">
						<textarea class="form-control" rows="5" id="txtSignature" name="txtSignature"><? echo replaceBRTag($signature); ?></textarea>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_633'); ?>:</label>
					<div class="col-lg-9">
						<textarea class="form-control" rows="3" id="txtInvPayTo" name="txtInvPayTo"><?php echo $payTo; ?></textarea>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_153'); ?>:</label>
					<div class="col-lg-9">
						<textarea class="form-control" rows="3" id="txtAddress" name="txtAddress"><?php echo $address; ?></textarea>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_581'); ?>:</label>
					<div class="col-lg-9">
						<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
							<input type="checkbox" name="chkRtl" id="chkRtl" <?php echo $retail == 1 ? 'checked' : '';?> class="toggle"/><span><label for="chkRtl"></label></span>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_804'); ?>:</label>
					<div class="col-lg-9">
						<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
							<input type="checkbox" name="chkKB" id="chkKB" <?php echo $knowledgeBase == 1 ? 'checked' : '';?> class="toggle"/><span><label for="chkKB"></label></span>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_805'); ?>:</label>
					<div class="col-lg-9">
						<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
							<input type="checkbox" name="chkTcktSys" id="chkTcktSys" <?php echo $ticketSys == 1 ? 'checked' : '';?> class="toggle"/><span><label for="chkTcktSys"></label></span>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_MR_HD_9'); ?>:</label>
					<div class="col-lg-9">
						<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
							<input type="checkbox" name="chkFAQs" id="chkFAQs" <?php echo $faqs == 1 ? 'checked' : '';?> class="toggle"/><span><label for="chkFAQs"></label></span>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-form-label"><?php echo $this->lang->line('BE_LBL_806'); ?>:</label>
					<div class="col-lg-9">
						<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
							<input type="checkbox" name="chkVdeos" id="chkVdeos" <?php echo $videos == 1 ? 'checked' : '';?> class="toggle"/><span><label for="chkVdeos"></label></span>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-offset-4 col-md-8">
						<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> id="btnGeneral" name="btnGeneral" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
						<div style="display:none;" id="dvLoader"><img src="<?php echo base_url('assets/images/loading.gif');?>" border="0" alt="Please wait..." /></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

