<div class="block-web">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
				<h4 style="color:#D12610;"><i class="fa fa-reorder"></i><b>Newsletter Email</b></h4>
            </div>
        </div>
        <div class="portlet-body form">
            <div class="form-body">
                <div class="form-group row">
                    <label class="col-md-4 control-label" style="text-align:right;"><?php echo $this->lang->line('BE_LBL_30'); ?>:</label>
                    <div class="col-md-6">
                        <select name="ltrId" id="ltrId" class="form-control select2me" data-placeholder="Select...">
                            <?php include APPPATH.'scripts/newsltrsdropdown.php';?>
                        </select>
                    </div>
                </div>
            </div>
			<br /><br /><br />
            <div class="form-group row">
                <div class="col-md-3"></div><div class="col-lg-9">
                    <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" id="btnSendEml">Send Newsletter</button>
                    <img style="display:none;" id="statusLoader97" src="<?php echo base_url('assets/img/loading.gif') ?>" border="0" alt="Please wait..." />
                </div>
            </div>
        </div>
    </div>

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
				<h4 style="color:#D12610;"><i class="fa fa-reorder"></i><b>Custom Email</b></h4>
            </div>
        </div>
        <div class="portlet-body form">
            <div class="form-body">
                <div class="form-group row">
                    <label class="col-md-4 control-label" style="text-align:right;">Subject:</label>
                    <div class="col-md-6">
                        <input type='text' placeholder="Enter Subject" name="txtCEmailSubj" id="txtCEmailSubj" required="required" maxlength="100" class="form-control" />
                    </div>
                </div>
            </div>
            <div class="form-body">
                <div class="form-group row">
                    <label class="col-md-4 control-label" style="text-align:right;">Message:</label>
                    <div class="col-md-6">
                        <textarea class="form-control" rows="3" name="txtCEmailMsg" id="txtCEmailMsg" placeholder="Enter Message" required="required"></textarea>
                    </div>
                </div>
            </div>
            <br /><br /><br />
            <div class="form-group row">
                <div class="col-md-3"></div><div class="col-lg-9">
                    <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" id="btnSendCEml">Send Custom Email</button>
                    <img style="display:none;" id="statusLoader99" src="<?php echo base_url('assets/img/loading.gif') ?>" border="0" alt="Please wait..." />
                </div>
            </div>
        </div>
    </div>
</div>
