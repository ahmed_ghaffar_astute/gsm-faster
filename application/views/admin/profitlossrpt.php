<!--include("include/php/crypt.php");-->
<style>
	input[type="text"] {
		border: 1px solid #ccc;
	}
</style>
<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_813') . ' - ' . $heading; ?>
			</h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<div>
								<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> class="btn <?php echo $sc !== '0' ? 'btn-dark' : 'btn-primary'; ?>" onclick="window.location.href='profitlossrpt?frmId=82&fTypeId=15'"><?php echo $this->lang->line('BE_MENU_PCKGS'); ?></button>
								<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> class="btn <?php echo $sc !== '1' ? 'btn-dark' : 'btn-primary'; ?>" onclick="window.location.href='profitlossrpt?sc=1&frmId=82&fTypeId=15'"><?php echo $this->lang->line('BE_PCK_25'); ?></button>
								<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> class="btn <?php echo $sc !== '2' ? 'btn-dark' : 'btn-primary'; ?>" onclick="window.location.href='profitlossrpt?sc=2&frmId=82&fTypeId=15'"><?php echo $this->lang->line('BE_LBL_299'); ?></button>
							</div>
						</div>
					</div>
					<?php echo form_open(base_url('admin/reports/profitlossrpt?frmId=82&fTypeId=15'), array('class' => "horizontal-form", 'method' => "post")); ?>
						<div class="form-group row">
							<div class="col-lg-6">
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('BE_CAT_1'); ?>
										<img style="display:none;" id="imgSrvcLoader" src="<?php echo base_url('assets/images/loading.gif');?>" border="0" alt="Please wait..."/>
									</label>
									<select id="categoryId" name="categoryId" class="form-control select2me" data-placeholder="Select...">
										<option value="0">Select...</option>
										<?php FillCombo($categoryId, $fileComboResult); ?>
									</select>

								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label
										class="control-label"><?php echo $this->lang->line('BE_PCK_HD_4'); ?></label>
									<select id="serviceId" name="serviceId" class="form-control select2me" data-placeholder="Select...">
										<option value="0">Select...</option>
										<?php FillCombo($serviceId, $fileComboResult2); ?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<label class="control-label"><?php echo $this->lang->line('BE_PM_7'); ?></label><br/>
								<input class="form-control form-control-inline input-largest date-picker" type="text" name="txtFromDt" value="<?php echo($dtFrom); ?>"/>
							</div>
							<div class="col-lg-6">
								<label class="control-label"><?php echo $this->lang->line('BE_PM_8'); ?></label><br/>
								<input class="form-control form-control-inline input-largest date-picker" type="text" name="txtToDt" value="<?php echo($dtTo); ?>"/>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-12">
								<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> class="btn btn-primary"><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
								<input type="hidden" name="sc" id="sc" value="<?php echo $sc; ?>"/>
							</div>
						</div>
					<?php echo form_close(); ?>
					<div class="form-group row">
						<div class="col-lg-12">
					<div class="page-title d-flex">
						<h4><i class="mr-2"></i> <?php echo $this->lang->line('BE_LBL_813') . ' - ' . $heading; ?>
						</h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th><?php echo $this->lang->line('BE_PCK_HD_4'); ?></th>
										<th nowrap><?php echo $this->lang->line('BE_LBL_814'); ?></th>
										<th nowrap><?php echo $this->lang->line('BE_LBL_657'); ?></th>
										<th nowrap><?php echo $this->lang->line('BE_LBL_815'); ?>
										<th></th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ($rsRpt) {
										$totalPackCostPrice = 0;
										$totalOrderCostPrice = 0;
										$totalProfitLoss = 0;
										foreach ($rsRpt as $row) {
											$totalPackCostPrice += roundMe($row->PackCostPrice);
											$totalOrderCostPrice += roundMe($row->OrderCostPrice);
											$profitLoss = roundMe($row->ProfitLoss);//roundMe($row->OrderCostPrice - $row->PackCostPrice);
											$totalProfitLoss += roundMe($profitLoss);
											?>
											<tr>
												<td><?php echo stripslashes($row->$colTitle); ?></td>
												<td><?php echo roundMe($row->PackCostPrice); ?></td>
												<td><?php echo roundMe($row->OrderCostPrice); ?></td>
												<td><?php echo roundMe($profitLoss); ?></td>
												<td valign="middle">
													<a href="profitlossorders.php?sc=<?php echo $sc; ?>&packId=<?php echo($row->$colId); ?>&pk=<?php echo($row->$colTitle); ?>"
													   class="btn default btn-xs purple"><?php echo $this->lang->line('LBL_816'); ?></a>
												</td>
											</tr>
										<?php } ?>
										<tr>
											<td>&nbsp;</td>
											<td nowrap>
												<strong><?php echo $DEFAULT_CURRENCY; ?><?php echo roundMe($totalPackCostPrice); ?></strong>
											</td>
											<td nowrap>
												<strong><?php echo $DEFAULT_CURRENCY; ?><?php echo roundMe($totalOrderCostPrice); ?></strong>
											</td>
											<td nowrap>
												<strong><?php echo $DEFAULT_CURRENCY; ?><?php echo roundMe($totalProfitLoss); ?></strong>
											</td>
											<td>&nbsp;</td>
										</tr>
										<?
									} else {
										?>
										<tr>
											<td colspan='5'><?php echo $this->lang->line('BE_GNRL_9') ?></td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<script language="javascript" src="<?php echo base_url(); ?>assets/js/general.js"></script>
