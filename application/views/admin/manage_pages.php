<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_MR_HD_11'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<input type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="<?php echo $this->lang->line('BE_GNRL_14').' '. $this->lang->line('BE_MR_HD_12'); ?>" onclick="window.location.href='<?php echo base_url('admin/cms/') ?>page?type=<?php echo $type;?>&frmId=<?php echo $this->input->post_get('frmId');?>&fTypeId=<?php echo $this->input->post_get('fTypeId')?>'" class="btn btn-primary" />
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-advance table-hover">
							<thead>
							<tr class="bg-primary">
								<th><?php echo $this->lang->line('BE_MR_34'); ?></th>
								<th><?php echo $this->lang->line('BE_MR_35'); ?></th>
								<th><?php echo $this->lang->line('BE_MR_36'); ?></th>
								<th><?php echo $this->lang->line('BE_MR_37'); ?></th>
								<th><?php echo $this->lang->line('BE_GNRL_3'); ?></th>
								<th></th>
							</tr>
							</thead>
							<tbody>
							<?php
							if ($rsCats) {
								foreach ($rsCats as $row) {
									?>
									<tr>
										<td class="highlight">
											<div class="success"></div>
											<a href="<?php echo base_url('admin/cms/') ?>page?id=<?php echo $row->PageId; ?>&type=<?php echo $type; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><?php echo stripslashes($row->LinkTitle); ?></a>
										</td>
										<td><?php echo stripslashes($row->PageTitle); ?></td>
										<td><?php echo $row->HeaderLink == '1' ? 'Yes' : 'No'; ?></td>
										<td><?php echo $row->FooterLink == '1' ? 'Yes' : 'No'; ?></td>
										<td><?php echo $row->DisablePage == '1' ? 'Yes' : 'No'; ?></td>
										<td>
											<a href="<?php echo base_url('admin/cms/') ?>page?id=<?php echo $row->PageId; ?>&type=<?php echo $type; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><i
													class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> </a>
										</td>
									</tr>
								<?php } ?>
							<?php } else { ?>
								<tr>
									<td colspan="6"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
