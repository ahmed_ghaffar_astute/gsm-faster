<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_MR_HD_10'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<a href="<?php echo base_url() ?>admin/Miscellaneous/viewfaqs?frmId=<? echo $this->input->post_get('frmId'); ?>&fTypeId=<? echo $this->input->post_get('fTypeId') ?>"
			   class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>

					<?php echo form_open(base_url('admin/Miscellaneous/addFaqs'), array("name" => "frm", "method" => "post", "id" => "frm")); ?>
					<input type="hidden" id="id" name="id" value="<? echo($id); ?>">

					<div class="form-group row">
						<label class="control-label col-lg-2"><? echo $this->lang->line('BE_MR_32'); ?>:</label>
						<div class="col-lg-9">
							<textarea class="ckeditor form-control" name="txtQuestion"
									  rows="6"><? echo $question; ?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-lg-2"><? echo $this->lang->line('BE_MR_33'); ?>:</label>
						<div class="col-lg-9">
							<textarea class="ckeditor form-control" name="txtAnswer"
									  rows="6"><? echo $answer; ?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-2 control-label"><? echo $this->lang->line('BE_GNRL_1'); ?>:</label>
						<div class="col-lg-9">
							<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
								<input type="checkbox" name="chkDisableFAQ" id="chkDisableFAQ" <?php echo $disableFaq == 1 ? 'checked' : ''; ?> class="toggle"/><span><label for="chkDisableFAQ"></label></span>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-2">
						</div>
						<div class="col-lg-9">
							<div class="form-group row">
								<div class="col-lg-3"></div><div class="col-lg-g-9">
									<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
											class="btn btn-primary"><? echo $this->lang->line('BE_LBL_72'); ?></button>
								</div>
							</div>

						</div>
					</div>
				<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/plugins/ckeditor/ckeditor.js'); ?>"></script>
<script language="javascript" src="<?php base_url('assets/js/faqs.js') ?>"></script>
