<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4>
				<i class="icon-arrow-left52 mr-2"></i> 
				<?php echo $heading; ?>
			</h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<div class="content pt-0">
	<div class="card">
		<div class="row">
            <div class="col-12 col-sm-12 col-lg-12 col-lg-12 col-xl-12">
                <div class="card-body">
                    <?php echo form_open('' , 'class="form-horizontal" name="frmChangePass" id="frmChangePass"');?>
                        <fieldset>
                            <div class="form-group row">
                                <?php $price_data = fetch_price_plan_data(); ?>
                                <label class="col-lg-3 control-label">Client Group</label>
                                <div class="col-lg-4">
                                    <select name="groupId" id="groupId" class="form-control select2me">
                                        <option value="0">Choose a Client Group</option>
                                        <?php FillCombo(0, $price_data); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">                                    
                                <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_192'); ?>:</label>
                                <div class="col-lg-4 radio-list">
									<div class="form-check form-check-inline mt-0">
										<label class="form-check-label"><input type="radio" class="form-check-input" name="rdPrType" id="rdPrType0" value="0" checked/><?php echo $this->lang->line('BE_LBL_825'); ?></label>
									</div>
									<div class="form-check form-check-inline mt-0">
										<label class="form-check-label"><input type="radio" class="form-check-input" name="rdPrType" id="rdPrType1" value="1"/><?php echo $this->lang->line('BE_LBL_826'); ?></label>
									</div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PCK_14'); ?>:<span class="required_field">*</span></label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" placeholder="Enter Price" maxlength="50" name="txtPrice" id="txtPrice" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 control-label">Pricing:</label>
                                <div class="col-lg-4 radio-list">
									<div class="form-check form-check-inline mt-0">
										<label class="form-check-label"><input type="radio" class="form-check-input" name="rdPricing" id="rdPricing0" value="0" checked/>Fixed</label>
									</div>
									<div class="form-check form-check-inline mt-0">
										<label class="form-check-label"><input type="radio" class="form-check-input" name="rdPricing" id="rdPricing1" value="1"/>Percentage</label>
									</div>
                                </div>
                            </div>
                        </fieldset>      
                        <div class="form-group row">
                            <div class="col-lg-9">
                                <div class="text-right">
                                    <button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" id="btnSave" name="btnSave"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
                                </div>
                                <input type="hidden" name="srvc" id="srvc" value="<?php echo $srvc; ?>" />
                                <img style="display:none;" id="statusLoader" src="<?php echo base_url();?>assets/images/loading.gif" border="0" alt="Please wait..." />
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div> 
        </div>
    </div>
</div>


<script language="javascript" src="<?php echo base_url('assets/js/gpricing.js');?>"></script>
