
<div class="portlet-body">
	<?php if($message != '')
         echo '<div class="alert alert-success">'.$message.'</div>'; ?>
    <table class="table table-striped table-bordered table-advance table-hover">
        <thead>
        <tr>
            <th>
                <i class="fa fa-briefcase"></i> <?php echo $this->lang->line('BE_LBL_43'); ?>
            </th>
            <th>
                <i class="fa fa-bookmark"></i> <?php echo $this->lang->line('BE_CR_3'); ?>
            </th>
            <th>
                <i class="fa fa-comments"></i> <?php echo $this->lang->line('BE_GNRL'); ?>
            </th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            <?php
                if($rsBlockedIPs)
                {
                    foreach($rsBlockedIPs as $row)
                    {
                        echo "	<tr>
                                    <td><a href='JavaScript:void(0);'>".$row->IP."</a></td>
                                    <td><b>".$row->DtTm."</b></td>
                                    <td><b>".stripslashes($row->Comments)."</b></td>"; ?>
                                    <td align="center"><a href="<?php echo base_url('admin/services/');?>overview?del=1&id=<?php echo $USER_ID_BTNS;?>&rid=<?php echo($row->Id); ?>#tab_1_6" onclick="return confirm('<?php echo $this->lang->line('BE_LBL_32'); ?>')"><i class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></td>
                                <?php
                                "</tr>";
                    }
                }
                else
                {
                    echo "<tr><td colspan='4'>".$this->lang->line('BE_GNRL_9')."</td></tr>";
                }
            ?>
        </tbody>
    </table>
</div>
