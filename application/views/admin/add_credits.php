<!-- Page header -->
	
<div class="page-header border-bottom-0">
    <div class="page-header-content header-elements-md-inline">
        <div style="padding-top: 10px !important;" class="page-title d-flex">
            <h4>
                <i class="icon-arrow-left52 mr-2"></i>
                <?php echo $this->lang->line('BE_LBL_537'); ?>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

    </div>
</div>

<div class="form-group row">
    <div class="col-lg-12">
        <?php $USER_ID_BTNS = $userId;
        if($USER_ID_BTNS){
            include APPPATH.'scripts/userbtns.php';
        }
        ?>
    </div>
</div>

<!-- /page header -->


<div class="content pt-0">
    <div class="card">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card-body">
                    <!-- Our Working Area Start -->
                    <?php if (isset($message) && $message != '') { ?>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <div class="alert alert-success"><?php echo $message; ?></div>
                            </div>
                        </div>
                    <?php } ?>
        
                    <?php echo form_open(base_url('admin/clients/addcredits') , 'class="form-horizontal"  name="frmAddCredits" id="frmAddCredits"'); ?>
                        <input type="hidden" value="<?php echo $userId; ?>" id="userId" name="userId" />
                        <input type="hidden" value="<?php echo $USER_EMAIL; ?>" id="userName" name="userName" />
                        <input type="hidden" value="<?php echo $name; ?>" id="uName" name="uName" />
                        <input type="hidden" value="<?php echo $currency; ?>" id="curr" name="curr" />				 
                        <input type="hidden" value="<?php echo $name; ?>" name="user_name" />
                        <input type="hidden" value="" id="hdPSTXT" name="hdPSTXT" />
                        

                            <div class="form-group row">
                                <label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_374'); ?>:</label>
                                <div class="col-lg-4">
                                    <p class="form-control-static"><?php echo $currency.' '.roundMe($myCredits); ?></p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_USR_25'); ?>:</label>
                                <div class="col-lg-4 radio-list">
                                    <label class="radio-inline">
                                    <input type="radio" class="chkSelect" name="rdCrdType" id="rdCrdType1" value="0" checked /><?php echo $this->lang->line('BE_GNRL_14'); ?> </label>
                                    <label class="radio-inline">
                                    <input type="radio" class="chkSelect" name="rdCrdType" id="rdCrdType2" value="1" /><?php echo $this->lang->line('BE_USR_26'); ?> </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_USR_10'); ?>:</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" maxlength="6" name="txtCredits" id="txtCredits" value="<?php echo($newCredits);?>" />
                                </div>
                            </div>
                            <div class="form-group row" id="dvPS">
                                <?php $payment_data = fetch_payment_status(); ?>
                                <label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LS_7'); ?>:<span class="required_field">*</span></label>
                                <div class="col-lg-4">
                                    <select name="pStatusId" id="pStatusId" class="form-control select2me" data-placeholder="<?php echo $this->lang->line('BE_LBL_257'); ?>">
                                        <option value="0"><?php echo $this->lang->line('BE_LBL_257'); ?></option>
                                        <?php FillCombo(0 , $payment_data); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" id="scPMethod" name="scPMethod" style="display:none;">
                                <?php $payment_method=fetch_payment_methods(); ?>
                                <label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_PM_5'); ?>:<span class="required_field">*</span></label>
                                <div class="col-lg-4">
                                    <select name="pMethodId" id="pMethodId" class="form-control select2me" data-placeholder="<?php echo $this->lang->line('BE_LBL_256'); ?>">
                                        <option value="0"><?php echo $this->lang->line('BE_LBL_256'); ?></option>
                                        <?php FillCombo(0,  $payment_method); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" id="dvTrans">
                                <label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_PM_12'); ?>:</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" name="txtTransactionId" id="txtTransactionId" maxlength="50" value="<?php echo($transactionId);?>" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_GNRL'); ?>:</label>
                                <div class="col-lg-4">
                                    <textarea class="form-control" rows="3" id="txtComments" name="txtComments"><?php echo @$comments; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 control-label"><?php echo $this->lang->line('BE_LBL_376'); ?>:</label>
                                <div class="col-lg-4">
                                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                                        <input type="checkbox" class="chkSelect" name="chkEmail" id="chkEmail" checked="checked" class="toggle"/><span><label for="chkEmail"></label></span>
                                    </div>
                                </div>
                            </div>

                        <div class="form-group row">
							<div class="col-lg-4"></div>
							<div class="col-lg-4">
                                <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                    <h2>&nbsp;Set Overdraft Limit</h2>
                    <hr />
                    <?php echo form_open(base_url('admin/clients/addcredits') ,'class="form-horizontal" name="frmODL" id="frmODL" ' );?>
                        <input type="hidden" value="<?php echo $userId; ?>" id="userId" name="userId" />
                            <div class="form-group row">
                                <label class="col-lg-4 control-label">Allow Overdraft:</label>
                                <div class="col-lg-4">
                                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                                        <input type="checkbox" id="chkODL" name="chkODL" <?php if($allowODL == '1') echo "checked"; ?> class="toggle"/><span><label for="chkODL"></label></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 control-label">Overdraft Limit:</label>
                                <div class="col-lg-4">
                                    <input type="text" required="required" class="form-control" maxlength="6" name="txtODL" id="txtODL" value="<?php echo $odl;?>" />
                                </div>
                            </div>
                            <div class="form-group row">
								<div class="col-lg-4"></div>
								<div class="col-lg-4">
                                    <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" name="btnODL"><i class="fa fa-plus"></i> Set Limit</button>
                                </div>
                            </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script language="javascript" src="<?php echo base_url('assets/js/languages/english.js');?>"></script>
<script language="javascript" src="<?php echo base_url('assets/js/user.js');?>"></script>
