			<!-- Page header -->
			<div class="page-header border-bottom-0">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_693'); ?></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>
					<div align="right">
						<a href="<?php base_url('admin/smsgateway/') ?>smsgateways?frmId=<?php echo $this->input->post_get('frmId');?>&fTypeId=<? echo $this->input->post_get('fTypeId')?>" class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
					</div>
				</div>
			</div>
			<!-- /page header -->
			<!-- Content area -->
			<div class="content pt-0">
				<div class="card">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
							<div class="card-body">
								<!-- Our Working Area Start -->
								<?php if (isset($message) && $message != '') { ?>
									<div class="form-group row">
										<div class="col-lg-12">
											<div class="alert alert-success"><?php echo $message; ?></div>

										</div>
									</div>
								<?php } ?>
						<?php echo form_open(base_url('admin/smsgateway/ajxsmsgateway'),array('class'=>"form-horizontal",'name'=>"frm",'method'=>"post",'id'=>"frm_smsgateway",'onsubmit'=>"return false")); ?>
							<input type="hidden" id="id" name="id" value="<?php echo($id); ?>" />
							
								<div class="form-group row">
									<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_693'); ?>:<span class="required_field">*</span></label>
									<div class="col-lg-4">
										<input type="text" class="form-control" placeholder="Enter SMS Gateway" maxlength="100" name="txtGateway" id="txtGateway" value="<?php echo($gateway);?>" />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PM_3'); ?>:</label>
									<div class="col-lg-4">
										<input type="text" class="form-control" placeholder="Enter Username" maxlength="100" name="txtUName" id="txtUName" value="<?php echo($userName);?>"  />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_INDEX_PWD'); ?>:</label>
									<div class="col-lg-4">
										<input type="text" class="form-control" placeholder="Enter Password" maxlength="100" name="txtPwd" id="txtPwd" value="<?php echo($password);?>" />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_GNRL_1'); ?>:</label>
									<div class="col-lg-4">
										<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
											<input type="checkbox" name="chkDisable" id="chkDisable" <?php echo $disable == 1 ? 'checked' : '';?> class="toggle"/><span><label for="chkDisable"></label></span>
										</div>
									</div>
								</div>

							<div class="form-group row">
								<div class="col-lg-3"></div>
								<div class="col-lg-9">
									<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" id="btnSave" name="btnSave"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
									<div style="display:none;" id="dvLdr"><img src="<?php echo base_url(); ?>/assets/images/loading.gif" border="0" alt="Please wait..." /></div>
								</div>
							</div>
							<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<script language="javascript" src="<?php echo base_url(); ?>assets/js/smsgateway.js"></script>
