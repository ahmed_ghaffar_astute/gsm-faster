<?php echo form_open(base_url("admin/services/serverservice?tab=1&id=") . $id, 'class="form-horizontal well"  id="frm" name="frm"'); ?>
<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
<div class="form-group row">
	<div class="col-lg-12">
		<h4><i class="mr-2"></i>
			<?php echo $this->lang->line('BE_LBL_38'); ?>
		</h4>
	</div>
</div>
<div class="form-group row">
	<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_295'); ?>:*</label>
	<div class="col-lg-5">
		<input type="text" class="form-control" placeholder="Enter Title" maxlength="100" name="txtTitle" id="txtTitle"
			   value="<?php echo($packageTitle); ?>"/>
	</div>
</div>
<?php if ($id == 0) { ?>
	<div class="form-group row">
		<?php $packages_data = fetch_log_packages(); ?>
		<label class="col-lg-3 control-label">Duplicate:</label>
		<div class="col-lg-5">
			<select name="dupSrvcId" id="dupSrvcId" class="form-control select2me">
				<option value="0">Select a Service</option>
				<?php FillCombo(0, $packages_data); ?>
			</select>
		</div>
	</div>
<?php } ?>
	<div class="form-group row">
		<?php $pack_cat = fetch_log_package_category_by_id(); ?>
		<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_294'); ?>:*</label>
		<div class="col-lg-5">
			<select name="categoryId" id="categoryId" class="form-control select2me"
					data-placeholder="<?php echo $this->lang->line('BE_LBL_255'); ?>">
				<option value="0"><?php echo $this->lang->line('BE_LBL_255'); ?></option>
				<?php FillCombo($categoryId, $pack_cat); ?>
			</select>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_296'); ?>:*</label>
		<div class="col-lg-5">
			<input type="text" class="form-control" placeholder="Enter Price"
				   name="txtPrice" <?php if ($id == '0') echo 'onblur="calculatePrices();"'; ?> <?php if ($id > 0) { ?> onblur="if(document.getElementById('chkCnvrtPrices').checked) convertPrices();" <?php } ?>
				   id="txtPrice" maxlength="10" value="<?php echo($price); ?>"/>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_651'); ?>:</label>
		<div class="col-lg-5">
			<input type="text" class="form-control" placeholder="Enter Cost Price" name="txtCostPrice" id="txtCostPrice"
				   maxlength="10" value="<?php echo roundMe($costPrice); ?>"/>
		</div>
		<?php if ($onlyAPIId > 0 && $extNetworkId > 0 && ($apiType == '4' || $apiType == '6')) { ?>
			<div class="col-lg-4">
				<table width="100%">
					<tr>
						<td width="5%"><input type="checkbox" class="chkSelect" name="chkFetchCPr"
											  id="chkFetchCPr" <?php if ($costPrFrmAPI == '1') echo 'checked'; ?> />
						</td>
						<td width="95%">
							Get Cost Price From API &nbsp;<img style="display:none;" id="imgCPLdr"
															   src="<?php echo base_url('assets/img/loading.gif') ?>"
															   border="0" alt="Please wait..."/>
							<?php
							$rw = fetch_currency_data();
							$DEFAULT_CURRENCY = $rw->CurrencyAbb;
							$PRICES = array();
							$rsPrices = fetch_service_api_pricing(2, $id, $onlyAPIId);
							foreach ($rsPrices as $row) {
								$PRICES[$row->ServiceId][$row->GroupId] = $row->PriceMargin . '|' . $row->MarginType;
							}
							$rsGroups = fetch_price_plans();

							foreach ($rsGroups as $row) {
								$PLANS[$row->PricePlanId] = stripslashes($row->PricePlan);
							}
							$defaultPrice = '0';
							$defaultMarginType = '';
							if (isset($PRICES[$id][0]) && $PRICES[$id][0] != '') {
								$val = $PRICES[$id][0];
								$arr = explode('|', $val);
								if (isset($arr[0]) && is_numeric($arr[0]))
									$defaultPrice = roundMe($arr[0]);
								if (isset($arr[1]) && is_numeric($arr[1])) {
									$defaultMarginType = $arr[1] == '0' ? '' : '%';
								}
							}
							$strPriceMargins = '';
							$icon = 'upload';
							if (sizeof($PLANS) > 0) {
								foreach ($PLANS as $key => $value) {
									$priceVal = '0';
									$marginTypeVal = '';
									if (isset($PRICES[$id][$key]) && $PRICES[$id][$key] != '') {
										$val = $PRICES[$id][$key];
										$arr = explode('|', $val);
										if (isset($arr[0]) && is_numeric($arr[0]))
											$priceVal = roundMe($arr[0]);
										if (isset($arr[1]) && is_numeric($arr[1]))
											$marginTypeVal = $arr[1] == '0' ? '' : '%';
									}
									$symbol = $priceVal >= 0 ? "+" : "-";
									$strPriceMargins .= '<strong>' . $value . '</strong> ' . $symbol . ' ' . $priceVal . $marginTypeVal . ' ' . $DEFAULT_CURRENCY . '<br />';
									if ($priceVal > 0 || $defaultPrice > 0)
										$icon = 'chevron-down';
								}
							}
							?>
							<a data-target="#collapseList_<?php echo $id; ?>" class=" btn btn-xs pull-right"
							   data-toggle="collapse" id="list_auto_update">
								<i class="fa fa-<?php echo $icon; ?>"></i>
							</a>
							<div class="collapse" id="collapseList_<?php echo $id; ?>">
								<div class="well">
									<strong>Default</strong>
									<?php echo $defaultPrice >= 0 ? '+' : '-' ?> <?php echo $defaultPrice . $defaultMarginType . ' ' . $DEFAULT_CURRENCY; ?>
									<br/>
									<?php echo $strPriceMargins; ?>
									<a href="<?php echo base_url('admin/services/serverservice?PMAPIId=' . $onlyAPIId . '&id=' . $id . '&del=1'); ?>"
									   class="btn btn-danger btn-xs pull-right" title="Delete Prices"
									   onclick="return confirm('Are you sure you want to delete these price margins?')">
										<i class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i>
									</a>
									<a href="<?php echo base_url('admin/services/setpriceswithapi?srvcAPIId=' . $onlyAPIId . '&serviceId=' . $id . '&sType=2'); ?>"
									   class="fancybox fancybox.ajax btn btn-info btn-xs pull-right"
									   title="Set Services Price Margins">
										<i class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i>
									</a>
									<br/>
								</div>
							</div>
						</td>
					</tr>
				</table>
			</div>
		<?php } ?>
	</div>
	<div class="form-group row">
		<label class="col-lg-3 control-label">Minimum Quantity (If Any):</label>
		<div class="col-lg-5">
			<input type="text" class="form-control" placeholder="Enter Minimum quantity customer will send"
				   maxlength="3" name="txtMinQty" value="<?php echo $minQty; ?>"/>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_344'); ?>:</label>
		<div class="col-lg-5">
			<input type="text" class="form-control" placeholder="Enter Supplier" maxlength="100" name="txtSupplier"
				   value="<?php echo($supplier); ?>"/>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PCK_10'); ?>:</label>
		<div class="col-lg-5">
			<input type="text" class="form-control" placeholder="Enter Delivery Time" name="txtDelTm" id="txtDelTm"
				   maxlength="100" value="<?php echo($deliveryTime); ?>"/>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_839'); ?>:</label>
		<div class="col-lg-5">
			<input type="text" class="form-control" placeholder="Enter <?php echo $this->lang->line('BE_LBL_839'); ?>"
				   name="txtRedirection" maxlength="100" value="<?php echo($redirect); ?>"/>
			<span class="form-text text-muted">
                                Sample URL: http://www.example.com/<br/>
                                Client will be sent to this URL on choosing this service.
                            </span>
		</div>

	</div>
	<div class="form-group row">
		<label class="col-lg-3 control-label">Notification Emails:</label>
		<div class="col-lg-5">
			<input type="text" class="form-control" placeholder="Enter Emails separated by Commas" name="txtEmails"
				   maxlength="100" value="<?php echo($emailIDs); ?>"/>

			<span class="form-text text-muted">
                                Email Addresses here to send New order emails for<br/>this service.
                            </span>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_PCK_21'); ?>*:</label>
		<div class="col-lg-7 radio-list">
			<label class="radio-inline">
				<input type="radio" class="chkSelect" name="rdServiceType" id="rdServiceType0" value="0"
					   checked/><?php echo $this->lang->line('BE_LBL_832'); ?> </label>
			<label class="radio-inline">
				<input type="radio" class="chkSelect" name="rdServiceType" id="rdServiceType1"
					   value="1" <?php if ($srvType == '1') echo 'checked'; ?> /><?php echo $this->lang->line('BE_LBL_833'); ?>
			</label>
		</div>
	</div>
	<div class="form-group row" <?php if ($srvType != '0') echo 'style="display:none;"'; ?> id="dvDelayTm">
		<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_835'); ?>:</label>
		<div class="col-lg-3">
			<input type="text" class="form-control" placeholder="Enter <?php echo $this->lang->line('BE_LBL_835'); ?>"
				   name="txtResDelayTm" id="txtResDelayTm" maxlength="4" value="<?php echo($responseDelayTm); ?>"/>

			<span class="form-text text-muted">
                                In minutes
                            </span>
		</div>
	</div>
	<div class="form-group row">
		<label class="control-label col-md-3"><?php echo $this->lang->line('BE_LBL_297'); ?>:</label>
		<div class="col-lg-9">
			<textarea class="ckeditor form-control" name="txtDetail" rows="6"><?php echo $detail; ?></textarea>
		</div>
	</div>
	<?php if ($id > 0) { ?>
		<div class="form-group row">
			<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_389'); ?>:</label>
			<div class="col-lg-4">
				<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
					<input type="checkbox" class="chkSelect" name="chkResetDiscounts" id="chkResetDiscounts"
						   class="toggle"/><span><label for="chkResetDiscounts"></label></span>
				</div>
			</div>
		</div>
	<?php } ?>
	<div class="form-group row">
		<label class="col-lg-3 control-label">Calculate Price Per Quantity:</label>
		<div class="col-lg-4">
			<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
				<input type="checkbox" name="chkPricePerQty"
					   id="chkPricePerQty" <?php echo $pricePerQty == 1 ? 'checked' : ''; ?>
					   class="toggle"/><span><label for="chkPricePerQty"></label></span>
			</div>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_630'); ?>:</label>
		<div class="col-lg-4">
			<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
				<input type="checkbox" name="cancelOrders"
					   id="cancelOrders" <?php echo $cancelOrders == 1 ? 'checked' : ''; ?> class="toggle"/><span><label
						for="cancelOrders"></label></span>
			</div>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_736'); ?>:</label>
		<div class="col-lg-2">
			<input type="text" class="form-control" name="txtCancelTime" maxlength="6" id="txtCancelTime"
				   value="<?php echo($cancelMins); ?>"/>
		</div>
		<span class="form-text text-muted">
                                Please enter time to verify an order in minutes!
                            </span>
	</div>
	<div class="form-group row">
		<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_675'); ?>:</label>
		<div class="col-lg-4">
			<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
				<input type="checkbox" name="verifyOrders"
					   id="verifyOrders" <?php echo $verifyOrders == 1 ? 'checked' : ''; ?> class="toggle"/><span><label
						for="verifyOrders"></label></span>
			</div>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_737'); ?>:</label>
		<div class="col-lg-2">
			<input type="text" class="form-control" placeholder="Order Verification Time in Minutes"
				   name="txtVerifyTime" maxlength="6" id="txtVerifyTime" value="<?php echo($verifyMins); ?>"/>
		</div>
		<span class="form-text text-muted">
                                Please enter time to verify an order in minutes!
                            </span>
	</div>
	<div class="form-group row">
		<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_GNRL_1'); ?>:</label>
		<div class="col-lg-4">
			<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
				<input type="checkbox" class="chkSelect" name="chkDisableLogPackage"
					   id="chkDisableLogPackage" <?php echo $disablePackage == 1 ? 'checked' : ''; ?>
					   class="toggle"/><span><label for="chkDisableLogPackage"></label></span>
			</div>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-lg-3 control-label">Features:</label>
		<div class="col-lg-9" style="padding-left:30px;">
			<?php
			foreach ($rsFtrs as $rwF) {
				$chckd = '';
				if ($rwF->Checked == '1')
					$chckd = 'checked';
				?>
				<label class="checkbox">
					<input type="checkbox" class="chkSelect" name="packFeatures[]"
						   value="<?php echo $rwF->FeatureId; ?>" <?php echo $chckd; ?> /><?php echo $rwF->Feature; ?>
				</label>
				<?php
			}
			?>
		</div>
	</div>
<div class="form-group row">
	<div class="col-lg-3"></div>
	<div class="col-lg-9">
		<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
				class="btn btn-primary"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
	</div>
</div>
<?php echo form_close(); ?>
