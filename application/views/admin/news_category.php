<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_CAT_HD_1'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div class="form-group" align="right">
			<input type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
				   value="<?php echo $this->lang->line('BE_GNRL_14') . ' ' . $this->lang->line('BE_CAT_1'); ?>"
				   onclick="window.location.href=baseurl+'admin/Settings/lookuplist?iFrm=18&frmId=<?php echo $_GET['frmId']; ?>&fTypeId=<?php echo $_GET['fTypeId'] ?>'"
				   class="btn btn-primary"/>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>

					<div class="form-group row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th><?php echo $this->lang->line('BE_CAT_1'); ?></th>
										<th><?php echo $this->lang->line('BE_GNRL_3'); ?></th>
										<th></th>
									</tr>
									</thead>
									<tbody id="sortableLst">
									<?php
									if ($count != 0)
									{
										foreach ($newsCat as $row) {
											?>
											<tr id="listItem_<?php echo $row->CategoryId; ?>">
												<td><i class="fa fa-sort"></i> &nbsp;&nbsp;<a
														href="<? echo base_url(); ?>admin/settings/lookuplist?iFrm=18&id=<?php echo $row->CategoryId; ?>&frmId=<?php echo $_REQUEST['frmId']; ?>&fTypeId=<?php echo $_GET['fTypeId'] ?>"><?php echo stripslashes($row->Category); ?></a>
												</td>
												<td><?php echo $row->DisableCategory == '1' ? 'Yes' : 'No'; ?></td>
												<td style="text-align:center" valign="middle">
													<a href="<? echo base_url(); ?>admin/settings/lookuplist?iFrm=18&id=<?php echo $row->CategoryId; ?>&frmId=<?php echo $_REQUEST['frmId']; ?>&fTypeId=<?php echo $_GET['fTypeId'] ?>"><i
															class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> </a>
												</td>
											</tr>
											<?php
										}
									}
									else
									?>
									<tr>
										<td colspan='4'> <?php echo $this->lang->line('BE_GNRL_9'); ?> </td>
									</tr>
									</tbody>
								</table>
								<input type="hidden" name="hw" id="hw" value="5"/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
