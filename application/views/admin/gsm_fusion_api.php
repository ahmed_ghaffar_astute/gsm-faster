<?php

// The location of the PDF file
// on the server
$filename = base_url("assets/api_files/$file_name");
// Store the file name into variable
$file = $filename;
// Header content type
header('Content-type: application/pdf');

header('Content-Disposition: inline; filename="' . $filename . '"');

header('Content-Transfer-Encoding: binary');

header('Accept-Ranges: bytes');

// Read the file
@readfile($file);
?>
