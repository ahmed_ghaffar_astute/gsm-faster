<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4>
				<i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_GNRL_BTN_1') . ' ' . $this->lang->line('BE_LBL_634'); ?>
			</h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<?php echo form_open(base_url('admin/system/subadmins?frmId=34&fTypeId=6'), 'class="horizontal-form"  id="frm" name="frm"'); ?>
						<div class="form-group row">
							<div class="col-lg-6">

								<label class="control-label"><?php echo $this->lang->line('BE_USR_4'); ?></label>
								<input type="text" placeholder="Enter First Name" name="txtFName"  value="<?php echo($fName); ?>" class="form-control">

							</div>
							<!--/span-->
							<div class="col-lg-6">
								<label class="control-label"><?php echo $this->lang->line('BE_USR_5'); ?></label>
								<input type="text" name="txtLName" placeholder="Enter Last Name" value="<?php echo($lName); ?>" class="form-control">
							</div>
							<!--/span-->
						</div>
						<!--/row-->
						<div class="form-group row">
							<div class="col-lg-6">
								<label class="control-label"><?php echo $this->lang->line('BE_USR_1'); ?></label>
								<input type="text" name="txtUName" placeholder="Enter Username"  value="<?php echo($uName); ?>" class="form-control">
							</div>
							<div class="col-lg-6">
								<label class="control-label"><?php echo $this->lang->line('BE_LBL_636'); ?></label>
								<select name="roleId" class="form-control select2me">
									<option
										value="0"><?php echo $this->lang->line('BE_LBL_623') . ' ' . $this->lang->line('BE_LBL_636'); ?></option>
									<?php FillCombo($roleId, $roles); ?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-12">
								<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> class="btn btn-primary"><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-advance table-hover">
								<thead>
									<tr class="bg-primary">
										<th><?php echo $this->lang->line('BE_USR_1'); ?></th>
										<th><?php echo $this->lang->line('BE_USR_9'); ?></th>
										<th><?php echo $this->lang->line('BE_LBL_636'); ?></th>
										<th><?php echo $this->lang->line('BE_USR_6'); ?></th>
										<th>Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
									<?php
									if ($rsAdmins) {
										$i = 0;
										$strAdminIds = '0';
										foreach ($rsAdmins as $row) {
											$strAdminIds .= ',' . $row->AdminId;
											$fontColor = '';
											if ($row->DisableAdmin == '1')
												$fontColor = ' style="color:grey;"';
											?>
											<tr>
												<td class="highlight">
													<div class="success"></div>
													<a href="<?php echo base_url('admin/system/'); ?>subadmin?id=<?php echo $row->AdminId; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><?php echo stripslashes($row->AdminUserName); ?></a>
												</td>
												<td><?php echo stripslashes($row->Name); ?></td>
												<td><?php echo $row->Role == '' ? '-' : stripslashes($row->Role); ?></td>
												<td><?php echo $row->Phone == '' ? '-' : stripslashes($row->Phone); ?></td>
												<td style="text-align:center" valign="middle">
													<a href="<?php echo base_url('admin/system/'); ?>subadmin?id=<?php echo($row->AdminId); ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><i
															class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> </a></td>
												<td style="text-align:center" valign="middle">
													<?php if ($row->AdminId != '1') { ?>
														<a href="<?php echo base_url('admin/system/'); ?>subadmins?del=1&id=<?php echo($row->AdminId); ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"
														onclick="return confirm('<?php echo $this->lang->line('BE_LBL_32'); ?>')"><i
																class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>
													<?php } else echo '-'; ?>
												</td>

											</tr>
											<?php
											$i++;
										}
									} else
										echo "<tr><td colspan='9'>" . $this->lang->line('BE_GNRL_9') . "</td></tr>";
									if ($this->input->post_get('du') && $this->input->post_get('du') == '1')
										echo '<input type="hidden" name="du" value="1" />';
									?>
									<input type="hidden" value="<?php echo $strAdminIds; ?>" name="strAdminIds"/>
								</tbody>
							</table>
						</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

