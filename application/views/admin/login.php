<div class="middle-login">
    <div class="block-web">
        <div class="head">
            <h3 class="text-center"><?php echo $this->config->item('app_name'); ?></h3>
        </div>
        <div style="background:#fff;">
            <?php echo form_open(base_url('admin/login'), array('class' => 'form-horizontal', 'style'=>"margin-bottom: 0px !important;")); ?>
               <div class="content">
                    <h3 class="form-title">Login to your account</h3>
                    <?php /*if ($message != ''){ */?><!--
                            <span style='color:red;'>
                                <b><?php /*echo $message */?></b>
                            </span>
                    <?php /*}
                        if($errorMsg != ''){ */?>
                            <span style='color:red;'>
                                <b><?php /*echo $errorMsg */?></b>
                            </span>
                    --><?php /*} */?>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                            <input class="form-control placeholder-no-fix" type="text" maxlength="50" autocomplete="off" placeholder="Username" name="username" id="username" value="<?php if($usrNm != '') echo $usrNm;?>"/>
                         </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input class="form-control placeholder-no-fix" type="password" maxlength="25" autocomplete="off" placeholder="Password" name="password" id="password" value="<?php if($usrPwd != '') echo $usrPwd;?>" />
                        </div>
                    </div>
                </div>
                <?php if($this->input->post('HTTP_HOST') != 'localhost' && $LOGIN_CAPTCHA == '1' && 1==2) { ?>
                    <div class="form-group">
                        <div class="col-md-8">
                            <div class="g-recaptcha" data-sitekey="<?php echo $GOOGLE_CAPTCHA_SITE_KEY;?>"></div>
                        </div>
                    </div>
                <?php } ?>
                <div class="foot" style="padding-top:15px;">
                    <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"">
                        Login
                    </button>
		        </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
