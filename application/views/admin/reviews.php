<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_514'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<input type="button" <?php if ($IS_DEMO) ;//echo 'disabled="disabled"'; ?>
				   value="<?php echo $this->lang->line('BE_LBL_513'); ?>"
				   onclick="window.location.href='<?php echo base_url('admin/cms/'); ?>review?type=<? echo $type; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<? echo $_REQUEST['fTypeId'] ?>'"
				   class="btn btn-primary"/>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">

	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">

				<?php if (isset($message) && $message != '') { ?>
				<div class="form-group row">
					<div class="col-lg-12">
						<div class="alert alert-success"><?php echo $message; ?></div>
										
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="form-group row">
				<div class="col-lg-12">
					<table class="table table-striped table-bordered table-advance table-hover">
						<thead>
						<tr class="bg-primary">
							<th><? echo $this->lang->line('BE_USR_9'); ?></th>
							<th><? echo $this->lang->line('BE_LBL_516'); ?></th>
							<th><? echo $this->lang->line('BE_CODE_7'); ?></th>
							<th><? echo $this->lang->line('BE_LBL_515'); ?></b></th>
							<th></th>
							<? if (!$IS_DEMO) { ?>
								<th></th>
							<? } ?>
						</tr>
						</thead>
						<tbody>
						<?php
						if ($rsReviews)
						{
						foreach ($rsReviews as $row)
						{
						?>
						<tr>
							<td class="highlight">
								<div class="success"></div>
								<a href="<?php echo base_url('admin/cms/') ?>review?id=<?php echo $row->ReviewId; ?>&type=<? echo $type; ?>&frmId=<? echo $this->input->post_get('frmId'); ?>&fTypeId=<? echo $this->input->post_get('fTypeId') ?>"><?php echo stripslashes($row->CustomerName); ?></a>
							</td>
							<td><?php echo stripslashes($row->Review); ?></td>
							<td><?php echo $row->ReviewDate; ?></td>
							<td><?php echo $row->Enabled == '1' ? 'Yes' : 'No'; ?></td>
							<td style="text-align:center" valign="middle">
								<a href="<?php echo base_url('admin/cms/') ?>review?id=<?php echo $row->ReviewId; ?>&type=<? echo $type; ?>&frmId=<? echo $this->input->post_get('frmId'); ?>&fTypeId=<? echo $this->input->post_get('fTypeId') ?>"><i
										class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> </a>
							</td>
							<? if (!$IS_DEMO) { ?>
							<td style="text-align:center" valign="middle">
								<a href="<?php echo base_url('admin/cms/') ?>reviews?del=1&id=<?php echo ($row->ReviewId); ?>&type=<? echo $type; ?>&frmId=<? echo $this->input->post_get('frmId'); ?>&fTypeId=<? echo $this->input->post_get('fTypeId') ?>"
								   onclick="return confirm('<?php echo $$this->lang->line('BE_LBL_32'); ?>')"><i
										class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a></td>
							<? } ?>
						</tr>
						<? } ?>
											<? }else{ ?>
						<tr>
							<td colspan="6"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td>
						</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
