<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_512'); ?></h5>
			</div>
			<div class="card-body">

				<!-- BEGIN FORM-->
				<?php echo form_open(base_url('admin/settings/manageapi'), 'class="form-horizontal" id="frmSync" name="frmSync"'); ?>
				<div class="form-group row" style="display:none;">
					<label class="col-lg-7 control-label"><? echo $this->lang->line('BE_LBL_508'); ?>:</label>
					<div class="col-lg-4">
						<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
							<input type="checkbox" name="chkIntAll" value="1" disabled checked id="chkIntAll"
								   class="form-input-styled"/><span><label for="chkIntAll"></label></span>
						</div>
					</div>
				</div>
				<? //if($apiType == '2' || $id == '204') {
				if ($id == '204') { ?>
					<div class="form-group row">
						<label class="col-lg-4 col-form-label"><? echo $this->lang->line('BE_LBL_672'); ?>:</label>
						<div class="col-lg-6">
							<div style="padding-top:8px;">
								<input type="checkbox" name="chkSyncModels" id="chkSyncModels"
									   class="form-input-styled"/><span><label for="chkSyncModels"></label></span>
							</div>
						</div>
					</div>
				<? } ?>
				<div class="form-group row">
					<label class="col-lg-4 col-form-label"><? echo $this->lang->line('BE_PCK_HD_5'); ?>:</label>
					<div class="col-lg-6">
						<div class="form-check form-check-inline mt-0">
							<label class="form-check-label mr-3">
							<input type="radio" name="rdServices" id="rdServices1" value="0" checked
								   class="form-check-input"" /><? echo $this->lang->line('BE_LBL_865'); ?></label>
						</div>
						<!--<label class="radio">
							<input type="radio" name="rdServices" id="rdServices2" value="1" />Partial Services
							<a style="display:none;" id="lnkSrvcs" href="JavaScript:void(0);" class="btn default btn-xs dark"><i class="fa fa-service"></i> Choose Services</a>
						</label>-->
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-4 col-form-label"><? echo $this->lang->line('BE_LBL_864'); ?>:</label>
					<div class="col-lg-8 radio-list">
						<div class="form-check form-check-inline mt-0">
							<label class="form-check-label mr-3">
							<input type="radio" name="rdSyncType" id="rdSyncType1" value="0" class="form-check-input"
							checked />Normal Synchronization to connect <br>your services with API service</label>
						</div>
						<div class="form-check form-check-inline mt-0">
							<label class="form-check-label mr-3">
							<input type="radio" name="rdSyncType" id="rdSyncType2" value="1" class="form-check-input"
							/>DELETE Existing Groups & Services and ADD new services from
							</label>
							<b><? echo $apiTitle; ?></b>
						</div>
					</div>
					<div id="dvPrices" style="display:none;">
						<br/><br/>
						<hr>
						<h3 style="padding-left:10px;">Synchronization Details</h3>
						<div class="form-group row">
							<label class="col-lg-4 col-form-label">CONNECT API services with My Services:</label>
							<div class="col-lg-4" style="padding-left:40px;">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="chkAttachSrv" checked id="chkAttachSrv"
										   class="form-input-styled"/><span><label for="chkAttachSrv"></label></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4 col-form-label">Price Type:</label>
							<div class="col-lg-6 radio-list" style="padding-left:40px;">
								<label class="radio-inline">
									<input type="radio" name="rdPrType" id="rdPrType0" value="0"
										   class="form-input-styled"" checked
									/><? echo $this->lang->line('BE_LBL_825'); ?> </label>
								<label class="radio-inline">
									<input type="radio" name="rdPrType" id="rdPrType1" value="1"
										   class="form-input-styled"" /><? echo $this->lang->line('BE_LBL_826'); ?>
									<label class="radio-inline">
										<input type="radio" name="rdPrType" id="rdPrType2" value="2"
											   class="form-input-styled"" />Keep Same
									</label>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4 col-form-label"><?php echo $this->lang->line('BE_PCK_14'); ?>
								:*</label>
							<div class="col-lg-4" style="padding-left:40px;">
								<input type="text" class="form-control" placeholder="Enter Price" maxlength="5"
									   name="txtPrice" id="txtPrice"/>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4 col-form-label">Pricing:</label>
							<div class="col-lg-4 radio-list" style="padding-left:40px;">
								<label class="radio-inline">
									<input type="radio" class="form-input-styled"" name="rdPricing" id="rdPricing0"
									value="0" checked />Fixed</label>
								<label class="radio-inline">
									<input type="radio" class="form-input-styled"" name="rdPricing" id="rdPricing1"
									value="1" />Percentage</label>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4 col-form-label">Update Prices Of Other Currencies:</label>
							<div class="col-lg-4" style="padding-left:40px;">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="chkOtherCurrPrices" checked id="chkOtherCurrPrices"
										   class="form-input-styled"/><label for="chkOtherCurrPrices"></label></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<? if ($id == '229' || $id == '260') { ?>
					<div class="form-group row">
						<label class="col-lg-7 control-label"><? echo $this->lang->line('BE_LBL_511'); ?>:</label>
						<div class="col-lg-4">
							<div>
								<input type="checkbox" name="chkSyncSS" id="chkSyncSS" class="toggle"/><span><label for="chkSyncSS"></label></span>
							</div>
						</div>
					</div>
				<? } ?>
			</div>
			<div class="form-actions fluid" style="padding-left:40px;">
				<div class="col-lg-12" align="center">
					<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> class="btn btn-primary"
							id="btnSync" name="btnSync">Synchronize IMEI Services
					</button>
					<img style="display:none;" id="syncLoader"
						 src="<?php echo base_url('assets/images/loading.gif'); ?>" border="0" alt="Please wait..."/>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
