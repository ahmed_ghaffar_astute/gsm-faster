<div class="page-header border-bottom-0 pt-4 content ">
	<div class="form-group row">
		<div class="col-lg-12">
			<?php $USER_ID_BTNS = $uId;
			if($USER_ID_BTNS){
				include APPPATH.'scripts/userbtns.php';
			} else{ ?>
				<div class="btn-toolbar">
					<div class="btn-group">
						<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary dropdown-toggle mr-1" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><?php echo $this->lang->line('BE_LBL_285'); ?></button>
						<ul class="dropdown-menu" role="menu" style="text-align:left;">
							<li class="dropdown-item"><a href="<?php echo base_url('admin/services/logrequests?codeStatusId=1&searchType=1&uId='.$uId);?>"><?php echo $this->lang->line('BE_LBL_286'); ?></a></li>
							<li class="dropdown-item"><a href="<?php echo base_url('admin/services/newserverorders?frmId=107&fTypeId=18');?>"><?php echo $this->lang->line('BE_LBL_287'); ?></a></li>
						</ul>
					</div>
					<div class="btn-group">
						<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary dropdown-toggle mr-1" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><?php echo $this->lang->line('BE_LBL_288'); ?></button>
						<ul class="dropdown-menu" role="menu" style="text-align:left;">
							<li class="dropdown-item"><a href="<?php echo base_url('admin/services/logrequests?codeStatusId=4&searchType=3&uId='.$uId);?>"><?php echo $this->lang->line('BE_LBL_288'); ?></a></li>
						</ul>
					</div>
					<div class="btn-group">
						<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary dropdown-toggle mr-1" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><?php echo $this->lang->line('BE_LBL_291'); ?></button>
						<ul class="dropdown-menu" role="menu" style="text-align:left;">
							<li class="dropdown-item"><a href="<?php echo base_url('admin/services/logrequests?uId='.$uId);?>"><?php echo $this->lang->line('BE_LBL_291'); ?></a></li>
						</ul>
					</div>
					<div class="btn-group">
						<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary dropdown-toggle mr-1" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><?php echo $this->lang->line('BE_LBL_292'); ?></button>
						<ul class="dropdown-menu" role="menu" style="text-align:left;">
							<li class="dropdown-item"><a href="<?php echo base_url('admin/services/verifyserverorders?uId='.$uId);?>"><?php echo $this->lang->line('BE_LBL_292'); ?></a></li>
						</ul>
					</div>
				</div>
			<?php }  ?>
		</div>
	</div>
</div>
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>
							</div>
						</div>
					<?php } ?>
					<?php if (isset($errorMsg) && $errorMsg != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $errorMsg; ?></div>
							</div>
						</div>
					<?php } ?>
					<?php
					if($this->input->post('hdsbmt') && $cldFrm == '4'){
						$strIMEIs = showAcceptedServerOrders($arcIds);
						?>
						<div class="page-header border-bottom-0">
							<div class="page-header-content header-elements-md-inline">
								<div style="padding-top: 10px !important;" class="page-title d-flex">
									<h4>
										<i class="icon-arrow-left52 mr-2"></i>
										<?php echo $this->lang->line('BE_LBL_712'); ?>
									</h4>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<?php echo $strIMEIs;?>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-2">
								<div class="btn-group">
									<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" onclick="downloadSOrders('<?php echo $arcIds;?>');"><i class="fa fa-download"></i> Download Orders</button>
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<input type="text" class="form-control" id="txtEmlAddrss" name="txtEmlAddrss" placeholder="Email Address">
									<input type="hidden" name="acceptedIds" id="acceptedIds" value="<?php echo $arcIds; ?>" />
									<span class="input-group-btn">
                                    <button class="btn dark" type="button" id="btnSendEmail" name="btnSendEmail"><i class="fa fa-envelope"></i> Send Email</button>
                                    <img style="display:none;" id="imgSendEmail" src="<?php echo base_url();?>assets/img/loading.gif" border="0" alt="Please wait..." />
                                </span>
								</div>
							</div>
						</div>

					<?php }
					if($searchType != '1') { ?>
						<?php echo form_open(base_url('admin/services/logrequests') , 'name="frm" , id="frm"');?>
						<input type="hidden" name="applyAPI" value="<?php echo $applyAPI; ?>" />
						<input type="hidden" name="records" value="<?php echo $limit; ?>" />
						<input type="hidden" name="searchType" id="searchType" value="<?php echo $searchType; ?>" />
						<input type="hidden" name="uId" value="<?php echo $uId;?>" />
						<input type="hidden" name="planId" value="<?php echo $planId;?>" />
						<input type="hidden" name="packId" value="<?php echo $packId;?>" />
						<?php include APPPATH.'scripts/serverservicessearch.php'; ?>
						<?php echo form_close(); ?>
					<?php } ?>
					<?php echo form_open(base_url('admin/services/logrequests') , 'onsubmit="return validate(-1)"') ?>
					<?php if($applyAPI == '1') {

					$row =  $row = get_api_srv_history($packId , $sc);
					if(isset($row->APIId) && $row->APIId != '')
					{
						$lastAPIId = $row->APIId;
						$lastAPIServiceId = $row->APIServiceId;
					}
					?>
					<div class="page-header border-bottom-0">
						<div class="page-header-content header-elements-md-inline">
							<div style="padding-top: 10px !important;" class="page-title d-flex">
								<h4>
									<i class="icon-arrow-left52 mr-2"></i>
									Apply API
								</h4>
							</div>
						</div>
					</div>
					<div class="table-responsive">
						<div class="form-group row">
							<div class="col-md-6">
								<?php $api_data=get_api_data() ;?>
								<label class="control-label">Choose API</label>
								<select name="apiId" id="apiId" class="form-control select2me" data-placeholder="<?php echo $this->lang->line('BE_LBL_278'); ?>">
									<option value="0"><?php echo $this->lang->line('BE_LBL_278'); ?></option>
									<?php FillCombo($lastAPIId, $api_data); ?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<label class="control-label">Choose Supplier Service</label>
								<select name="supplierPackId" id="supplierPackId" class="form-control select2me" data-placeholder="<?php echo $this->lang->line('BE_LBL_274'); ?>">
									<option value="0"><?php echo $this->lang->line('BE_LBL_274'); ?></option>
									<?php
									if($lastAPIId > 0)
									{
										$rsSrvcs = fetch_supplier_services($lastAPIId ,$sc);
										while($row = $objDBCD14->fetchNextObject($rsSrvcs))
										{
											$selected = '';
											if($row->Id == $lastAPIServiceId)
												$selected = 'selected';
											$optionVal = stripslashes($row->ServiceName);
											if($row->ServicePrice != '')
												$optionVal .= ' - '.stripslashes($row->ServicePrice).' Credits';
											if($row->ServiceTime != '')
												$optionVal .= ' - '.stripslashes($row->ServiceTime);
											echo "<option value='".$row->Id."' $selected>".$optionVal."</option>";
										}
									}
									?>
								</select><img style="display:none;" id="imgSrvcLoader" src="<?php echo base_url();?>assets/img/loading.gif" border="0" alt="Please wait..." />
							</div>
						</div>
					</div>
				</div>

				<?php } ?>
				<div class="page-header border-bottom-0">
					<div class="page-header-content header-elements-md-inline">
						<div style="padding-top: 10px !important;" class="page-title d-flex">
							<h4>
								<i class="icon-arrow-left52 mr-2"></i>
								<?php echo $this->lang->line('BE_CODE_HD_2'); ?>
							</h4>
						</div>
					</div>
				</div>

				<div class="table-responsive">
					<?php if($searchType == '3') { ?>
						<div class="form-group row">
							<div class="col-md-11">
								<div class="form-group">
									<label class="control-label"><strong>Enter code data to enter against all selected jobs</strong></label>
									<input type="text" class="form-control" placeholder="Enter code data to enter against all jobs" name="txtBulkCodeVal" />
								</div>
							</div>
							<div class="col-md-1" style="padding-top:26px;">
								<input type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="<?php echo $this->lang->line('BE_LBL_194'); ?>" class="btn btn-primary" onClick="setValue('6');" />
							</div>
						</div>
					<?php } ?>
					<table class="table table-striped table-bordered table-advance table-hover">
						<thead>
						<tr class="bg-primary">
							<th nowrap="nowrap" width="3%">Sr. #</th>
							<?php if($searchType == '3') { ?>
								<th>
									<?php echo $this->lang->line('BE_LBL_194'); ?><input type="checkbox" class="chkSelect" id="chkSelect1" name="chkSelect1" onClick="selectAllChxBxs('chkSelect1', 'chkReply', <?php echo $count; ?>);" value="true">
								</th>
							<?php } ?>
							<th width="10%"><?php echo $this->lang->line('BE_LBL_589'); ?></th>
							<th><?php echo $this->lang->line('BE_PCK_HD_4'); ?></th>
							<th><?php echo $this->lang->line('BE_INDEX_USRNAME'); ?></th>
							<th width="16%">Date</th>
							<?php
							echo '<th>Job Data</th>';
							if($searchType == '3')
								echo '<th>'.$this->lang->line('BE_CODE_6').'</th>';
							if($searchType != '0')
							{
								echo '<th style="text-align:center">';
								if($searchType == '3') echo $this->lang->line('BE_LBL_308').'/Download';
								?>
								<input type="checkbox" class="chkSelect" id="chkSelect" name="chkSelect" onClick="selectAllChxBxs('chkSelect', 'chkCodes', <?php echo $count; ?>);" value="true">

								</th>
							<?php } ?>
						</tr>
						</thead>
						<tbody>
						<?php
						if($count != 0)
						{
							$i = 0;
							foreach($rsCodes as $row)
							{
								$strAPI = '';
								$apiError = '';
								$strAPIError = 'Order Did not go through API - ';
								$strAPIError .= $row->MessageFromServer == '' ? 'No Error reported' : stripslashes($row->MessageFromServer);
								if($row->API != '-')
									$strAPI = '<br />Replied By: <font style="color:#ABABAB;">'.stripslashes($row->API).'</font>';
								if($row->OrderAPIId > 0 && $row->StatusId =='1' && $row->CodeSentToOtherServer == '0')
									$apiError = '<img src="'.base_url().'assets/images/red-flags-icon-png-7.png" width="20" height="20" title="'.$strAPIError.'" />';

								$strCustVals = '';
								for($z = 1; $z < $colIndex; $z++)
								{
									$colName = 'Col'.$z;
									if(isset($row->$colName) && trim($row->$colName) != '')
									{
										if(isset($CUST_COL_LBL[$colName]) && $CUST_COL_LBL[$colName] != '')
										{
											if($strCustVals != '')
												$strCustVals .= '<br />';
											$strCustVals .= $CUST_COL_LBL[$colName].':';
										}
										$strCustVals .= ' '.$row->$colName;
									}
								}
								?>
								<tr class="bg-primary">
									<td><?php echo $i+1; ?>.</td>
									<?php if($searchType == '3') { ?>
										<td align="center">
											<input type="checkbox" class="chkSelect" class="chkSelect" id="chkReply<?php echo $i; ?>" name="chkReply[]" value="<?php echo $row->LogRequestId.'|'.$row->UserId.'|'.$row->Credits.'|'.$i.'|'.$row->LogPackageId.'|'.stripslashes($row->LogPackageTitle); ?>">
										</td>
									<?php } ?>

									<td><a href="<?php echo base_url('admin/services/logrequest?id='.$row->LogRequestId);?>">#<?php echo $row->LogRequestId;?></a> <?php echo $apiError;?></td>
									<td width="200px;"><?php echo stripslashes($row->LogPackageTitle);?><br /><b><?php echo roundMe($row->Credits).'</b> '.$row->CurrencyAbb;?>
											<br />
											<?php if($row->OrderCostPrice > 0) { ?>
											<span id="spnPrft_<?php echo $row->LogRequestId;?>" class="badge badge-<?php echo $row->StatusId == '3' ? 'danger' : 'info'; ?>">
                                                        <?php echo roundMe($row->Profit);?>
                                                    </span>
											<br />
										<?php } echo stripslashes($row->DeliveryTime);?></td>
									<td><a href="overview.php?id=<?php echo($row->UserId);?>"><?php echo $row->UserName;?></a><br /><?php echo $row->IP;?></td>
									<td>Ordered:<br /><font style="color:#ABABAB;"><?php echo $row->RequestedAt; ?></font><br />
										Replied:<br /><font style="color:#ABABAB;"><?php echo $row->ReplyDtTm; ?></font><?php echo $strAPI; ?></td>
									<td>
										<?php //echo $strCustVals == '' ? '-' : $strCustVals; ?>
										<?php echo $row->OrderData == '' ? '-' : stripslashes($row->OrderData); ?>
									</td>

									<?php if($searchType == '3') { ?>
										<td>
											<?php //echo stripslashes($row->Code); ?>
											<textarea class="form-control" placeholder="Enter Data" style="width:150px; font-size:11px; height:100px;" name="txtCode<?php echo $i; ?>"><?php echo strip_tags($row->Code); ?></textarea>
										</td>
									<?php }
									if($searchType != '0') { ?>
										<td align="center">
											<input type="checkbox" class="chkSelect" id="chkCodes<?php echo $i; ?>" name="chkCodes[]" value="<?php echo $row->LogRequestId.'|'.$row->UserId.'|'.$row->Credits.'|'.$i.'|'.$row->LogPackageId.'|'.stripslashes($row->LogPackageTitle); ?>">
											<br /><font <?php echo getOrderBGColor($row->StatusId); ?>>&nbsp;&nbsp;&nbsp;<?php echo stripslashes($row->CodeStatus); ?>&nbsp;&nbsp;&nbsp;</font>
											<?
											if($row->LinkedOrderId != 0)
												echo '<span class="badge badge-warning">Linked To: '.$row->LinkedOrderId.'</span>';
											?>
											<?php if($row->Profit != 0) { ?>
												&nbsp;<a title="Remove profit for this order" class="btn btn-xs" style="background-color:RED; color:#FFFFFF" href="JavaScript:void(0);" onclick="removeProfit('<?php echo $row->LogRequestId?>', '2');"><i class="fa fa-minus"></i></a>
											<?php } ?>
										</td>
									<?php } ?>
								</tr>
								<?php
								$i++;
							}
						}
						else
							echo '<tr><td colspan="10">'.$this->lang->line('BE_GNRL_9').'</td></tr>';
						?>
						</tbody>
					</table>
				</div>
				<div class="form-group row">
					<div class="col-md-12">
						<?php if($searchType == '1' || $searchType == '2') { ?>
							<div class="btn-toolbar margin-bottom-10">
								<div class="btn">

									<button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" onClick="setValue('4');"><i class="fa fa-check"></i> <?php echo $this->lang->line('BE_LBL_304'); ?></button>
									<button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" onClick="document.getElementById('chkSelect').checked = true;selectAllChxBxs('chkSelect', 'chkCodes', <?php echo $count; ?>);setValue('4');"><i class="fa fa-check"></i> <?php echo $this->lang->line('BE_LBL_305'); ?></button>
									<button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-danger" onClick="setValue('5');"><i class="fa fa-ban"></i> <?php echo $this->lang->line('BE_LBL_306'); ?></button>

									<?php if($applyAPI == '1') { ?>
										<input type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="Apply API" class="btn dark" onClick="setValue('7');" />
									<?php } ?>

								</div>
							</div>
						<?php } else { ?>
							<div class="btn">
								<button type="button" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" onClick="downloadOrders();"><i class="fa fa-download"></i> <?php echo $this->lang->line('BE_LBL_40'); ?></button>
								<button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary" onClick="setValue('2');"><i class="fa fa-check"></i> <?php echo $this->lang->line('BE_LBL_49'); ?></button>
							</div>
						<?php } ?>
						<input type="hidden" name="oB" id="oB" value="<?php echo $orderBy; ?>" />
						<input type="hidden" value="0" name="cldFrm" id="cldFrm" />
						<input type="hidden" name="uId" value="<?php echo $uId;?>" />
						<input type="hidden" name="planId" value="<?php echo $planId;?>" />
						<input type="hidden" name="hdsbmt" />
						<input type="hidden" name="txtFromDt" id="txtFromDt" value="<?php echo $dtFrom; ?>" />
						<input type="hidden" name="searchType" id="searchType" value="<?php echo $searchType; ?>" />
						<input type="hidden" name="codeStatusId" id="codeStatusId" value="<?php echo $codeStatusId; ?>" />
						<input type="hidden" name="packId" value="<?php echo $packId;?>" />
						<input type="hidden" name="applyAPI" value="<?php echo $applyAPI; ?>" />
						<input type="hidden" name="sc" id="sc" value="<?php echo $sc; ?>" />
						<input type="hidden" id="records" name="records" value="<?php echo $limit; ?>" />
						<input type="hidden" name="start" id="start" value="<?php echo $start; ?>" />
						<!--============================= SEARCH FIELDS ==================================-->
					</div>
				</div>
				<?php echo form_close();?>
				<?php echo form_open(base_url('admin/services/logrequests') , 'name="frmPaging" , id="frmPaging"');?>
				<input type="hidden" name="applyAPI" value="<?php echo $applyAPI; ?>" />
				<input type="hidden" name="searchType" id="searchType" value="<?php echo $searchType; ?>" />
				<input type="hidden" name="uId" value="<?php echo $uId;?>" />
				<input type="hidden" name="planId" value="<?php echo $planId;?>" />
				<input type="hidden" name="packId" value="<?php echo $packId;?>" />
				<input type="hidden" name="logPackageId" value="<?php echo $logPackageId; ?>" />
				<input type="hidden" name="codeStatusId" value="<?php echo $codeStatusId; ?>" />
				<input type="hidden" name="txtFromDt" value="<?php echo $dtFrom; ?>" />
				<input type="hidden" name="txtToDt" value="<?php echo $dtTo; ?>" />
				<input type="hidden" name="txtOrderNo" value="<?php echo $orderNo; ?>" />
				<input type="hidden" name="txtUName" value="<?php echo $uName; ?>" />
				<div class="form-group row">
					<div class="col-md-2">
						<label class="control-label"><strong>Records Per Page</strong></label>
						<table>
							<tr>
								<td>
									<input type="text" class="form-control" placeholder="Enter Custom Page Size" style="width:180px;" name="records" value="<?php echo $limit?>" />
								</td>
								<td>
									<button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn blue" onClick="setValue('5');"><i class="fa fa-check"></i> Set</button>
								</td>
							</tr>
						</table>
					</div>
					<div class="col-md-10">
						<?php
						if($count != 0)
						{
							$row = fetch_count_log_requests($strWhere);

							$totalRows = $row->TotalRecs;
							if($totalRows > $limit)
							{
								$whichFrm = $searchType != '1' ? 'frm' : 'frmPaging';
								doPages_DropDown($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next, $whichFrm);
							}
						}
						?>
					</div>
				</div>
				<?php echo form_close();?>
				<?php echo form_open(base_url('admin/services/logrequests?searchType=3&frmId=110&fTypeId=18') , 'name="frmIds" , id="frmIds"');?>
				<input type="hidden" name="hdnOrdrIds" id="hdnOrdrIds" value="<?php echo $arcIds;?>" />
				<?php echo form_close();?>

			</div>
		</div>
	</div>
</div>
</div>



<script>

	function downloadOrders()
	{
		var strIds = validate('0');
		if(strIds != false)
			window.location.href = '<?php echo base_url();?>admin/services/downloadfileorders?ids='+strIds;
	}
	function downloadSOrders(ids)
	{
		window.location.href = '<?php echo base_url();?>admin/services/downloadfileorders?ids='+ids;

	}

	function validate(cldFrm)
	{
		var totalCodes = <?php echo $count; ?>;
		var searchType = <?php echo $searchType; ?>;
		var strIds = '0';
		var ctrlName = 'chkCodes';
		if(document.getElementById('cldFrm').value == '6')
			ctrlName = 'chkReply';
		for(var i=0; i<totalCodes; i++)
		{
			if(document.getElementById(ctrlName+i).checked)
			{
				arr = document.getElementById(ctrlName+i).value.split('|');
				strIds += ',' + arr[0];
			}
		}
		if(strIds == '0')
		{
			alert('<?php echo $this->lang->line('BE_LBL_53'); ?>');
			return false;
		}
		if(cldFrm == '-1' && document.getElementById('cldFrm').value == '3')
		{
			if(document.getElementById('blkCodeStatusId').value == '0')
			{
				alert('<?php echo $this->lang->line('BE_LBL_69'); ?>');
				document.getElementById('blkCodeStatusId').focus();
				return false;
			}
		}
		if(cldFrm == 0)
			return strIds;
		else
			return true;
	}

	function setValue(i)
	{
		document.getElementById('cldFrm').value = i;
	}
	function backToList()
	{
		document.getElementById('frmIds').submit();
	}

</script>
<script language="javascript" src="<?php echo base_url('assets/js/pendingorders.js');?>"></script>
