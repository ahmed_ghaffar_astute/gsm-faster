<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo 'Email Integration'; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<?php echo form_open('', 'class="form-horizontal" name="frm"  id="frm"'); ?>
					<input type="hidden" id="id" name="id" value="<?php echo($id); ?>"/>
					<div class="form-group row">
						<label class="col-lg-3 control-label">Email Provider:<span class="required_field">*</span> </label>
						<div class="col-lg-4">
							<input type="text" class="form-control" readonly="readonly" placeholder="Enter Email Provider" maxlength="100" name="txtAPIName" id="txtAPIName" value="<?php echo($apiName); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label">API Key:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter API Key" maxlength="100" name="txtAPIKey" id="txtAPIKey" value="<?php echo($apiKey); ?>"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label">Group ID:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter <?php echo $this->lang->line('BE_LBL_841'); ?>" maxlength="100" name="txtGroupId" id="txtGroupId" value="<?php echo($groupId); ?>"/>
						</div>
					</div>

					<div class="form-group row">
						<?php $price_plans = fetch_price_plans_2(); ?>
						<label class="col-lg-3 control-label">Clients List:<span class="required_field">*</span></label>
						<div class="col-lg-4">
							<select name="planId" id="planId" class="form-control select2me"
									data-placeholder="Select...">
								<option value="0"><?php echo $this->lang->line('BE_LBL_407'); ?></option>
								<?php FillCombo(0, $price_plans); ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3"></div>
						<div class="col-lg-9">
							<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> class="btn btn-primary" id="btnSave" name="btnSave"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
							<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> class="btn btn-primary" id="btnSync" name="btnSync">Sync Emails </button>
							<img style="display:none;" id="dvLdr" src="<?php echo base_url('assets/images/loading.gif'); ?>" border="0"  alt="Please wait..."/>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
				<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
<script language="javascript" src="<?php echo base_url('assets/js/languages/english.js'); ?>"></script>
<script language="javascript" src="<?php echo base_url('assets/js/emailint.js'); ?>"></script>
