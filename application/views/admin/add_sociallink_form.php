<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_522'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<a href="<?php echo base_url("admin/cms/socialmedialinks") ?>?frmId=<? echo $this->input->post_get('frmId'); ?>&fTypeId=<? echo $this->input->post_get('fTypeId') ?>&type=<? echo $this->input->post_get('type') ?>"
			   class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Back To
				List</a>
		</div>


	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
							<form class="form-horizontal" name="frm" method="post" id="frm"
								  enctype="multipart/form-data">
								<?php echo form_open('', array('class' => "form-horizontal", 'name' => "frm", 'method' => "post", 'id' => "frm", 'enctype' => "multipart/form-data")) ?>
								<input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
								<input type="hidden" id="existingImage" name="existingImage"
									   value="<?php echo $existingImage; ?>">
									<div class="form-group row">
										<label class="col-lg-3 control-label"><? echo $this->lang->line('BE_MR_26'); ?>
											:<span class="required_field">*</span></label>
										<div class="col-lg-4">
											<input type="text" class="form-control" placeholder="Enter Title"
												   maxlength="100" name="txtTitle" id="txtTitle"
												   value="<? echo($title); ?>"/>
										</div>
									</div>
									<div class="form-group row">
										<label for="txtImage"
											   class="col-lg-3 control-label"><? echo $this->lang->line('BE_PCK_3'); ?>
											:</label>
										<div class="col-lg-9">
											<input type="file" id="txtImage" name="txtImage">
											<p class="form-text text-muted">
												.jpg, .gif, .png
											</p>
											<?php if ($existingImage != '') { ?>
												<p valign="middle"><img src="<?php echo $existingImage ?>"/></p>
											<?php } ?>

										</div>
									</div>
									<div class="form-group row">
										<label class="col-lg-3 control-label"><? echo $this->lang->line('BE_N_4'); ?>
											:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" placeholder="Enter Image URL"
												   maxlength="100" name="txtURL" id="txtURL" value="<? echo($url); ?>"/>
											<span class="form-text text-muted"><? echo $this->lang->line('BE_N_6'); ?>&nbsp;http://www.anylink.com</span>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-lg-3 control-label">APP ID:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" placeholder="Enter Facebook APP ID"
												   maxlength="40" name="txtAppId" value="<? echo($appId); ?>"/>
											<span class="form-text text-muted">Click <a
													style="text-decoration:underline" target="_blank"
													href="https://developers.facebook.com/docs/plugins/like-button/"><strong>here</strong></a> to get your appId </span>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-lg-3 control-label"><? echo $this->lang->line('BE_GNRL_1'); ?>
											:</label>
										<div class="col-lg-4">
											<div class="switch-button switch-button-lg" data-on-label="YES"
												 data-off-label="NO">
												<input type="checkbox" name="chkDisableRecord"
													   id="chkDisableRecord" <?php echo $disable == 1 ? 'checked' : ''; ?>
													   class="toggle"/><span><label
														for="chkDisableRecord"></label></span>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-lg-3"></div>
									<div class="col-lg-9">
										<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
												class="btn btn-primary"><? echo $this->lang->line('BE_LBL_72'); ?></button>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<script language="javascript" src="<?php echo base_url('assets') ?>/js/socialmedia.js"></script>
