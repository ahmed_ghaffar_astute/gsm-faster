			<!-- Page header -->
			<div class="page-header border-bottom-0">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4><i class="icon-arrow-left52 mr-2">123</i> <?php echo $strLabel; ?></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>
					<div align="right">
						<a href="JavaScript:void(0);" onClick="window.history.back();" class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
					</div>

				</div>
			</div>
			<!-- /page header -->
			<!-- Content area -->
			<div class="content pt-0">
				<div class="card">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
							<div class="card-body">
								<!-- Our Working Area Start -->
								<?php if (isset($message) && $message != '') { ?>
									<div class="form-group row">
										<div class="col-lg-12">
											<div class="alert alert-success"><?php echo $message; ?></div>

										</div>
									</div>
								<?php } ?>
								<?php echo  form_open('' ,array('class' => 'form-horizontal','name' => 'frmChangePass','id' => 'frmChangePass','onsubmit'=>'return false'));
                        ?>
                        <input type="hidden" id="hdTdLbl" name="hdTdLbl" value="<?php echo($strLabel); ?>">
                        <input type="hidden" id="idField" name="idField" value="<?php echo($idCol); ?>">
                        <input type="hidden" id="nameField" name="nameField" value="<?php echo($textCol); ?>">
                        <input type="hidden" id="disableField" name="disableField" value="<?php echo($disableCol); ?>">
                        <input type="hidden" id="tblName" name="tblName" value="<?php echo($tbl); ?>">
                        <input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
                        <div class="form-body">
                            <div class="form-group row">
                                <label class="col-lg-3 control-label"><?php echo $strLabel; ?>:<span class="required_field">*</span> </label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" placeholder="Enter <?php echo $strLabel; ?>" maxlength="100" name="txtBx" id="txtBx" value="<?php echo($txtVal);?>" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_GNRL_1'); ?>:</label>
                                <div class="col-lg-4">
                                    <div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
                                        <input type="checkbox" name="disable" id="disable" <?php echo $disable == 1 ? 'checked' : '';?> class="toggle"/><span><label for="disable"></label></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3"></div><div class="col-lg-9">
                                <?php echo form_submit(array('class'=>'btn btn-primary green', 'id' => 'btnSave','name'=>'btnSave','value'=>$this->lang->line('BE_LBL_72'))); ?>
                                <div style="display:none;" id="statusLoader"><img src="<?php echo base_url('assets/images/loading.gif')?>" border="0" alt="Please wait..." /></div>

                            </div>
                        </div>
                        <?php echo  form_close(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="display:none" id="baseurl"><?php echo base_url(); ?></div>
<script src="<?php echo base_url('assets/js/lookuplist.js')?>"></script>
