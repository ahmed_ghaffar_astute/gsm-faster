<!-- Page header -->
	
<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_PM_HD_1'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>

<div class="form-group row">
	<div class="col-lg-12">
		<?php $USER_ID_BTNS = $userId;
		if($USER_ID_BTNS){
			include APPPATH.'scripts/userbtns.php';
		}
		?>
	</div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php echo form_open(base_url('admin/clients/payments?frmId=37&fTypeId=7'), 'class="horizontal-form"  id="frmSrch" name="frmSrch"'); ?>
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label"><?php echo $this->lang->line('BE_PM_1'); ?></label>
								<input type="text" name="txtFName" value="<?php echo($fName); ?>"
									   class="form-control">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label"><?php echo $this->lang->line('BE_PM_2'); ?></label>
								<input type="text" name="txtLName" value="<?php echo($lName); ?>"
									   class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label"><?php echo $this->lang->line('BE_PM_3'); ?></label>
								<input type="text" name="txtUName" value="<?php echo($uName); ?>"
									   class="form-control">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label"><?php echo $this->lang->line('BE_PM_4'); ?></label>
								<input type="text" name="txtPaymentId" value="<?php echo($paymentId); ?>"
									   class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<?php $payment_methods = fetch_payment_methods(); ?>
							<div class="form-group">
								<label class="control-label"><?php echo $this->lang->line('BE_PM_5'); ?></label>
								<select name="pMethodId" class="form-control select2me"
										data-placeholder="<?php echo $this->lang->line('BE_LBL_256'); ?>">
									<option value="0"><?php echo $this->lang->line('BE_LBL_256'); ?></option>
									<?php FillCombo($pMethodId, $payment_methods); ?>
								</select>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<?php $payment_status = fetch_payment_status_by_id($strWherePmnts); ?>
								<label class="control-label"><?php echo $this->lang->line('BE_PM_6'); ?></label>
								<select name="pStatusId" class="form-control select2me"
										data-placeholder="<?php echo $this->lang->line('BE_LBL_257'); ?>">
									<option value="0"><?php echo $this->lang->line('BE_LBL_257'); ?></option>
									<?php FillCombo($pStatusId, $payment_status); ?>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group">
								<label
									class="control-label"><?php echo $this->lang->line('BE_PM_7'); ?></label><br/>
								<input class="form-control form-control-inline input-largest date-picker"
									   type="text" name="txtFromDt" value="<?php echo($dtFrom); ?>"/>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label
									class="control-label"><?php echo $this->lang->line('BE_PM_8'); ?></label><br/>
								<input class="form-control form-control-inline input-largest date-picker"
									   type="text" name="txtToDt" value="<?php echo($dtTo); ?>"/>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label">Payer Email:</label>
								<input type="text" name="txtPyrEml" value="<?php echo($pyrEml); ?>"
									   class="form-control">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label"><?php echo $this->lang->line('BE_LBL_60'); ?></label>
								<select name="records" class="form-control select2me" data-placeholder="Select...">
									<?php
									$NO_OF_RECORDS = array("10" => 10, "50" => 50, "100" => 100, "200" => 200, "300" => 300, "400" => 400, "500" => 500, "1000" => 1000);
									foreach ($NO_OF_RECORDS as $key => $value) {
										$selected = '';
										if ($limit == $key)
											$selected = 'selected';
										echo '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
									}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label"><?php echo $this->lang->line('BE_LBL_622'); ?></label>
								<select name="invoicesBy" class="form-control select2me"
										data-placeholder="Select...">
									<option value="-1"
											selected><?php echo $this->lang->line('BE_LBL_623'); ?></option>
									<option
										value="1" <?php if ($invoicesBy == '1') echo 'selected'; ?> ><?php echo $this->lang->line('BE_LBL_624'); ?></option>
									<option
										value="0" <?php if ($invoicesBy == '0') echo 'selected'; ?> ><?php echo $this->lang->line('BE_MENU_USRS'); ?></option>
								</select>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="control-label"><?php echo $this->lang->line('BE_PM_12'); ?></label>
								<input type="text" name="txtTransNo" value="<?php echo($transNo); ?>"
									   class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary"><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
							<input type="hidden" name="cldFrm" id="cldFrm" value="0"/>
							<input type="hidden" name="byAdmin" id="byAdmin" value="<?php echo $byAdmin; ?>"/>
							<input type="hidden" name="userId" id="userId" value="<?php echo $userId; ?>"/>
							<input type="hidden" name="start" id="start" value="0"/>
						</div>
					</div>

					<?php
					if ($count != 0) {
						$row = fetch_users_payments($strWhere);

						$totalRows = $row->TotalRecs;
						if ($totalRows > $limit) {
							doPages_DropDown($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next, 'frmSrch');
						}
					}
					?>
					<div class="form-group row">
						<div class="col-lg-12">
							<p align="right">
								<input type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									   value="<?php echo $this->lang->line('BE_LBL_818'); ?>"
									   onClick="return validate(4);"
									   class="btn btn-danger" name="btnSubmit"/>
								<?php if (array_key_exists("320", $ARR_ADMIN_FORMS)) { ?>
									<input type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
										   value="<?php echo $this->lang->line('BE_LBL_673'); ?>"
										   onClick="return validate(2);" class="btn btn-primary" name="btnSubmit"/>
								<?php } ?>
								<input type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									   value="<?php echo $this->lang->line('BE_LBL_674'); ?>"
									   onClick="return validate(3);"
									   class="btn btn-dark" name="btnSubmit"/>
								<input type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									   value="<?php echo $this->lang->line('BE_LBL_102'); ?>"
									   onClick="window.location.href='<?php base_url('admin/clients/exportpayments'); ?>';"
									   name="btnSubmit" class="btn btn-warning"/>
								<?php if (array_key_exists("319", $ARR_ADMIN_FORMS)) { ?>
									<input type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
										   value="<?php echo $this->lang->line('BE_LBL_303'); ?>"
										   onClick="return validate(1);" class="btn btn-danger" name="btnSubmit"/>
								<?php } ?>
							</p>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">

							<div class="table-responsive">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr class="bg-primary">
										<th><?php echo $this->lang->line('BE_LBL_101'); ?></th>
										<th><?php echo $this->lang->line('BE_PM_3'); ?></th>
										<th><?php echo $this->lang->line('BE_PM_9'); ?></th>
										<th><?php echo $this->lang->line('BE_PM_10'); ?></th>
										<th>Vat</th>
										<th><?php echo $this->lang->line('BE_PM_11'); ?></th>
										<th><?php echo $this->lang->line('BE_PM_12'); ?></th>
										<th><?php echo $this->lang->line('BE_CODE_5'); ?></th>
										<th></th>
										<th style="text-align:center">
											<input type="checkbox" class="chkSelect" id="chkSelect" name="chkSelect"
												   onClick="selectAllChxBxs('chkSelect', 'chkPayments', <?php echo $count; ?>);"
												   value="true">
										</th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ($count != 0) {
										$i = 0;
										foreach ($rsPayments as $row) {
											$dtTm = explode(" ", $row->PaymentDtTm);
											$key = crypt_key($row->UserId);
											$myCredits = decrypt($row->Credits, $key);
											if ($myCredits == '')
												$myCredits = '-';
											$invoiceAmount = decrypt($row->Amount, $key);
											if ($invoiceAmount == '')
												$invoiceAmount = '-';
											if ($row->PaymentStatusId == '1') //Unpaid
												$badgeColor = 'warning';
											else if ($row->PaymentStatusId == '2') //Paid
												$badgeColor = 'success';
											else if ($row->PaymentStatusId == '3') //Cancel
												$badgeColor = 'danger';
											else if ($row->PaymentStatusId == '4') //Refunded
												$badgeColor = 'warning';
											else// if($row->PaymentStatusId == '5') //Review
												$badgeColor = 'info';
											?>
											<tr>
												<td class="highlight">
													<div class="success"></div>
													<a href="<?php echo base_url('admin/clients/'); ?>payment?id=<?php echo $row->PaymentId; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><?php echo($row->PaymentId); ?></a>
												</td>
												<td><?php echo($row->UserName); ?></td>
												<td><?php echo stripslashes($row->Name); ?></td>
												<td><?php echo $myCredits; ?></td>
												<td><?php echo $row->Currency . ' ' . $row->Vat; ?></td>
												<td><?php echo($row->Currency . ' ' . roundMe($invoiceAmount)); ?></td>
												<td>
													<input type="text" class="form-control" style="width:200px;"
														   name="txtTransID<?php echo $row->PaymentId; ?>"
														   value="<?php echo $row->TransactionId; ?>"/>
													<input type="hidden" name="hdTransID<?php echo $row->PaymentId; ?>"
														   value="<?php echo $row->TransactionId; ?>"/>
												</td>
												<td>
                                                    <span class="label label-<?php echo $badgeColor; ?>">
                                                        <?php echo($row->PaymentStatus); ?>
                                                    </span>
												</td>
												<td style="text-align:center" valign="middle">
													<a href="<?php echo base_url('admin/clients/'); ?>payment?id=<?php echo $row->PaymentId; ?>&frmId=<?php echo $this->input->post_get('frmId'); ?>&fTypeId=<?php echo $this->input->post_get('fTypeId') ?>"><i
															class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> </a>
												</td>
												<td align="center">
													<input type="checkbox" class="chkSelect"
														   id="chkPayments<?php echo $i; ?>"
														   name="chkPayments[]" value="<?php echo $row->PaymentId; ?>">
												</td>
											</tr>
											<tr>
												<td nowrap="nowrap"
													colspan="2"><?php echo $this->lang->line('BE_PM_5'); ?>
													: <?php echo($row->PaymentMethod == '' ? '-' : $row->PaymentMethod); ?>
												</td>
												<td colspan="2"><?php echo $this->lang->line('BE_LBL_594'); ?>
													: <?php echo($row->PayerEmail); ?></td>
												<td><?php echo $this->lang->line('BE_PM_13'); ?>
													: <?php echo convertDate($row->PaymentDtTm); ?></td>
												<td colspan="4"><?php echo $this->lang->line('BE_LBL_595'); ?>
													: <?php echo stripslashes($row->ShippingAddress); ?></td>
											</tr>
											<?php
											$i++;
										}
									} else
										echo "<tr><td colspan='10'>" . $this->lang->line('BE_GNRL_9') . "</td></tr>";
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	function openInvoice(id) {
		window.open(base_url + 'admin/clients/invoice?id=' + id, 'mywin', 'left=20,top=20,width=850,height=500,toolbar=1,resizable=0');
	}

	function validate(z) {
		var totalPayments = <?php echo $count; ?>;
		var strIds = '0';
		for (var i = 0; i < totalPayments; i++) {
			if (document.getElementById('chkPayments' + i).checked) {
				arr = document.getElementById('chkPayments' + i).value.split('|');
				strIds += ',' + arr[0];
			}
		}
		if (strIds == '0') {
			alert('<?php echo $this->lang->line('BE_LBL_53'); ?>');
			return false;
		}
		if (z == '1') {
			if (confirm('Are your sure you want to delete the payment(s)?')) {
				document.getElementById('cldFrm').value = '1';
				return true;
			} else
				return false;
		} else {
			document.getElementById('cldFrm').value = z;
			return true;
		}
	}
</script>
