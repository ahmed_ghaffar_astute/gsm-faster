<div class="page-content">
    <?php echo form_open_multipart('' , 'name="frm" id="frm"');?>
        <?php if($message != '')
            echo '<div class="alert alert-success">'.$message.'</div>'; ?>
        <?php if($cldFrm== '0' && ($strData != '' || $strRejData != '')) 
        { 
            echo '<div class="alert alert-success">'.$strData.'<br /><br />'.$strRejData.'<br />'; 
        ?>
                <input type="hidden" value="<?php echo $upldType; ?>" name="upldType" />
                <input type="hidden" value="<?php echo $file; ?>" name="hdfile" />
                <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> name="btnCSubmit" class="btn btn-primary" onClick="setValue('1');"><?php echo $this->lang->line('BE_LBL_662'); ?></button>
            </div>
        <?php } ?>

    <div class="row">
        <div class="col-md-12">
            <h2><i class="fa fa-reorder"></i> <?php echo $this->lang->line('BE_LBL_616');?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line('BE_PCK_HD'); ?>:</label>
                                <select name="packageId" id="packageId" class="form-control select2me" data-placeholder="Select...">
                                <?php
                                    $PCK_TITLE = 'PackageTitle';
                                    $CATEGORY = 'Category';
                                    include APPPATH.'scripts/packagesdropdown.php';
                                ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line('BE_LBL_659'); ?>:</label>
                                <input type="file" id="txtFile" name="txtFile">
                                <p class="form-text text-muted">
                                    .txt<br />
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="control-label"><strong>OR</strong></label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line('BE_LBL_660'); ?>:</label>
                                <textarea class="form-control" rows="10" id="txtData" name="txtData"><?php echo @$this->input->post_get('txtData'); ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line('BE_LBL_834'); ?>:*</label>
                                <input type="text" class="form-control" placeholder="<?php echo $this->lang->line('BE_LBL_834'); ?>" name="txtReplySep" id="txtReplySep" maxlength="10" value="<?php echo($replySep);?>" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="control-label">Reply Type:</label><br />
                                <label class="radio-inline">
                                <input type="radio" name="rdReplyType" value="0" checked />Completed </label>
                                <label class="radio-inline">
                                <input type="radio"  name="rdReplyType" value="1" />Rejected </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions center" style="padding-left:450px;">
                    <input type="hidden" value="0" name="cldFrm" id="cldFrm" />
                    <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> name="btnBSubmit" class="btn btn-primary" onClick="setValue('0');"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close();?>
    
    
</div>
