<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header header-elements-inline">
				<h5 class="card-title"><?php echo $this->lang->line('BE_LBL_194'); ?></h5>
			</div>
        
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_LBL_194'); ?>:<span class="required_field">*</span></label>
                    <div class="col-lg-4">
                        <textarea class="form-control" rows="3" id="txtReply" name="txtReply"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-9">
                        <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> id="btnReply" name="btnReply" class="btn btn-primary">Submit</button>
                        <input type="hidden" id="id" name="id" value="<?php echo($id); ?>">
                        <input type="hidden" id="tcktNo" name="tcktNo" value="<?php echo($tcktNo); ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
