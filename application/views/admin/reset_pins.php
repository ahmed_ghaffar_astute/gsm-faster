<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_648'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<button type="button" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> name="btnRSetPins"
									id="btnRSetPins" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-warning"><?php echo $this->lang->line('BE_LBL_649'); ?></button>
							<div style="display:none;" id="statusLoader"><img
									src="<?php echo base_url('assets/images/loading.gif'); ?>" border="0"
									alt="Please wait..."/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script language="javascript" src="<?php echo base_url('assets/js/resetpwds.js'); ?>"></script>
