<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_665'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<!-- BEGIN FORM-->
					<?php echo form_open('', array('class' => "form-horizontal", 'name' => "frm", 'method' => "post", 'id' => "frm")); ?>
					<!--<form class="form-horizontal" name="frm" method="post" id="frm">-->
						<div class="form-group row">
							<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_CODE_6'); ?>:</label>
							<div class="col-lg-4">
								<textarea class="form-control" rows="13" style="width:500px;" id="txtCode"
										  name="txtCode"><? echo $code; ?></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-3 control-label"><?php echo $this->lang->line('BE_GNRL_1'); ?>:</label>
							<div class="col-lg-4">
								<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
									<input type="checkbox" name="chkDisable"
										   id="chkDisable" <?php echo $disable == 1 ? 'checked' : ''; ?>
										   class="toggle"/><span><label for="chkDisable"></label></span>
								</div>
							</div>
						</div>
					<div class="form-group row">
						<div class="col-lg-3"></div>
						<div class="col-md-9">
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?> value="btnSave"
									class="btn btn-primary"
									name="btnSave"><?php echo $this->lang->line('BE_LBL_72'); ?></button>
						</div>
					</div>
					<?php echo form_close(); ?>
					<!--</form>-->
					<!-- END FORM-->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->
