<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_MR_HD_6'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">

	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<!-- Copy till this to paste to other pages -->
					<div class="form-group row">
						<div class="col-lg-12">
							<table class="table table-striped table-bordered table-advance table-hover">
								<thead>
								<tr>
									<th><?php echo $this->lang->line('BE_MR_26'); ?></th>
									<th><?php echo $this->lang->line('BE_MR_27'); ?></th>
									<th><?php echo $this->lang->line('BE_GNRL_3'); ?></th>
									<th></th>
								</tr>
								</thead>
								<tbody>
								<?php
								if($rsLogos)
								{
									foreach($rsLogos as $row)
									{
										if($row->ForAdminPanel == '0')
											$category = $this->lang->line('BE_LBL_520');
										else if($row->ForAdminPanel == '1')
											$category = $this->lang->line('BE_LBL_78');
										else if($row->ForAdminPanel == '2')
											$category = $this->lang->line('BE_LBL_519');
										else if($row->ForAdminPanel == '3')
											$category = $this->lang->line('BE_LBL_521');
										else
											$category = 'Other';
										?>
										<tr>
											<td class="highlight"><div class="success"></div><a href="<?php echo base_url('admin/cms/')?>logo?id=<?php echo $row->LogoId;?>&frmId=<? echo $this->input->post_get('frmId');?>&fTypeId=<? echo $this->input->post_get('fTypeId')?>"><?php echo stripslashes($row->LogoTitle);?></a></td>
											<td><?php echo $category; ?></td>
											<td><?php echo $row->DisableLogo == '1' ? 'Yes' : 'No';?></td>
											<td style="text-align:center" valign="middle">
												<a href="<?php echo base_url('admin/cms/')?>logo?id=<?php echo $row->LogoId;?>&frmId=<? echo $this->input->post_get('frmId');?>&fTypeId=<? echo $this->input->post_get('fTypeId')?>" ><i class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> </a>
											</td>
										</tr>
									<? }?>
								<? }else{ ?>
									<tr><td colspan="6"><strong><?php echo $this->lang->line('BE_GNRL_9'); ?></strong></td></tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
