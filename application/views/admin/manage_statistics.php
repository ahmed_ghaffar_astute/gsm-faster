<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_MR_HD_1'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php }
					echo form_open(base_url('admin/reports/statistics?frmId=80&fTypeId=15'), array('class' => "horizontal-form", 'method' => "post")); ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<div class="caption">
								<i class="fa fa-reorder"></i><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?>
							</div>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-lg-2">

							<label
								class="control-label"><?php echo $this->lang->line('BE_PM_7'); ?></label>
						</div>
						<div class="col-lg-4">
							<input class="form-control form-control-inline input-largest date-picker"
								   type="text" name="txtFromDt" value="<?php echo($dtFrom); ?>"/>
						</div>
						<div class="col-lg-1">

							<label
								class="control-label"><?php echo $this->lang->line('BE_PM_8'); ?></label>
						</div>
						<div class="col-lg-4">
							<input class="form-control form-control-inline input-largest date-picker"
								   type="text" name="txtToDt" value="<?php echo($dtTo); ?>"/>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-lg-2"></div>
						<div class="col-lg-9">
							<button type="submit" <?php if ($IS_DEMO) echo 'disabled="disabled"'; ?>
									class="btn btn-primary"><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
						</div>
					</div>

					<!--</form>-->
					<?php
					echo form_close();
					//stats_todaysummary.php
					if ($check != 'No') {
						?>
						<h4 style="color:#D12610;"><b><?php echo $this->lang->line('BE_LBL_404'); ?></b></h4>
						<div class="tabbable tabbable-custom tabbable-custom-profile">
							<ul class="nav nav-tabs nav-tabs-solid border-0">
								<li class="nav-item">
									<a href="#tab_5_11"
									   data-toggle="tab" class="nav-link active"><?php echo $this->lang->line('BE_LBL_397'); ?></a>
								</li>
								<li class="nav-item">
									<a href="#tab_5_22"
									   data-toggle="tab" class="nav-link"><?php echo $this->lang->line('BE_LBL_398'); ?></a>
								</li>
								<li class="nav-item">
									<a href="#tab_5_33"
									   data-toggle="tab" class="nav-link"><?php echo $this->lang->line('BE_LBL_399'); ?></a>
								</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="tab_5_11">
									<div class="portlet-body">
										<table class="table table-striped table-bordered table-advance table-hover">
											<thead>
											<tr>
												<th style="text-align:center;"><i
														class="fa fa-stop"></i><?php echo $this->lang->line('BE_LBL_400'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-lock"></i><?php echo $this->lang->line('BE_MR_7'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-unlock"></i><?php echo $this->lang->line('BE_LBL_401'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-warning"></i><?php echo $this->lang->line('BE_LBL_337'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-shopping-cart"></i><?php echo $this->lang->line('BE_MR_4'); ?>
												</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<td align="center"
													valign="middle"><?php echo $pending_Srchd; ?></td>
												<td align="center"
													valign="middle"><?php echo $inProcess_Srchd; ?></td>
												<td align="center"
													valign="middle"><?php echo $available_Srchd; ?></td>
												<td align="center"
													valign="middle"><?php echo $notAvailable_Srchd; ?></td>
												<td align="center"
													valign="middle"><?php echo $pending_Srchd + $inProcess_Srchd + $available_Srchd + $notAvailable_Srchd; ?></td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
								<!--tab-pane-->
								<div class="tab-pane" id="tab_5_22">
									<div class="portlet-body">
										<table class="table table-striped table-bordered table-advance table-hover">
											<thead>
											<tr>
												<th style="text-align:center;"><i
														class="fa fa-stop"></i><?php echo $this->lang->line('BE_LBL_400'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-lock"></i><?php echo $this->lang->line('BE_MR_7'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-unlock"></i><?php echo $this->lang->line('BE_LBL_401'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-warning"></i><?php echo $this->lang->line('BE_LBL_337'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-shopping-cart"></i><?php echo $this->lang->line('BE_MR_4'); ?>
												</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<td align="center"
													valign="middle"><?php echo $pending_Srchd_File; ?></td>
												<td align="center"
													valign="middle"><?php echo $inProcess_Srchd_File; ?></td>
												<td align="center"
													valign="middle"><?php echo $available_Srchd_File; ?></td>
												<td align="center"
													valign="middle"><?php echo $notAvailable_Srchd_File; ?></td>
												<td align="center"
													valign="middle"><?php echo $pending_Srchd_File + $inProcess_Srchd_File + $available_Srchd_File + $notAvailable_Srchd_File; ?></td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="tab-pane" id="tab_5_33">
									<div class="portlet-body">
										<table class="table table-striped table-bordered table-advance table-hover">
											<thead>
											<tr>
												<th style="text-align:center;"><i
														class="fa fa-stop"></i><?php echo $this->lang->line('BE_LBL_400'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-lock"></i><?php echo $this->lang->line('BE_MR_7'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-unlock"></i><?php echo $this->lang->line('BE_LBL_401'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-warning"></i><?php echo $this->lang->line('BE_LBL_337'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-shopping-cart"></i><?php echo $this->lang->line('BE_MR_4'); ?>
												</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<td align="center"
													valign="middle"><?php echo $pending_Srchd_Server; ?></td>
												<td align="center"
													valign="middle"><?php echo $inProcess_Srchd_Server; ?></td>
												<td align="center"
													valign="middle"><?php echo $available_Srchd_Server; ?></td>
												<td align="center"
													valign="middle"><?php echo $notAvailable_Srchd_Server; ?></td>
												<td align="center"
													valign="middle"><?php echo $pending_Srchd_Server + $inProcess_Srchd_Server + $available_Srchd_Server + $notAvailable_Srchd_Server; ?></td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
								<!--tab-pane-->
							</div>
						</div>

						<h4 style="color:#D12610;"><b><?php echo $this->lang->line('BE_LBL_405'); ?></b></h4>
						<div class="tabbable tabbable-custom tabbable-custom-profile">
							<ul class="nav nav-tabs nav-tabs-solid border-0">
								<li class="nav-item">
									<a href="#tab_6_11"
									   data-toggle="tab" class="nav-link active"><?php echo $this->lang->line('BE_LBL_397'); ?></a>
								</li>
								<li class="nav-item">
									<a href="#tab_6_22"
									   data-toggle="tab" class="nav-link"><?php echo $this->lang->line('BE_LBL_398'); ?></a>
								</li>
								<li class="nav-item">
									<a href="#tab_6_33"
									   data-toggle="tab" class="nav-link"><?php echo $this->lang->line('BE_LBL_399'); ?></a>
								</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="tab_6_11">
									<div class="portlet-body">
										<table class="table table-striped table-bordered table-advance table-hover">
											<thead>
											<tr>
												<th>
													<i class="fa fa-reorder"></i><?php echo $this->lang->line('BE_PCK_HD'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-stop"></i><?php echo $this->lang->line('BE_LBL_400'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-lock"></i><?php echo $this->lang->line('BE_MR_7'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-unlock"></i><?php echo $this->lang->line('BE_LBL_401'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-warning"></i><?php echo $this->lang->line('BE_LBL_337'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-shopping-cart"></i><?php echo $this->lang->line('BE_MR_4'); ?>
												</th>
											</tr>
											</thead>
											<tbody>
											<?php
											if ($rsIMEIOrdersSrchd) {
												foreach ($rsIMEIOrdersSrchd as $row) {
													$totalOrders = $row->P + $row->CA + $row->IP + $row->NA;
													?>
													<tr>
														<td valign="middle"><?php echo stripslashes($row->PackageTitle); ?></td>
														<td align="center"
															valign="middle"><?php echo $row->P; ?></td>
														<td align="center"
															valign="middle"><?php echo $row->IP; ?></td>
														<td align="center"
															valign="middle"><?php echo $row->CA; ?></td>
														<td align="center"
															valign="middle"><?php echo $row->NA; ?></td>
														<td align="center"
															valign="middle"><?php echo $totalOrders; ?></td>
													</tr>
													<?
												}
											} else {
												?>
												<tr>
													<td colspan='6'><?php echo $this->lang->line('BE_LBL_403'); ?></td>
												</tr>
											<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
								<!--tab-pane-->
								<div class="tab-pane" id="tab_6_22">
									<div class="portlet-body">
										<table class="table table-striped table-bordered table-advance table-hover">
											<thead>
											<tr>
												<th>
													<i class="fa fa-reorder"></i><?php echo $this->lang->line('BE_LBL_373'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-stop"></i><?php echo $this->lang->line('BE_LBL_400'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-lock"></i><?php echo $this->lang->line('BE_MR_7'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-unlock"></i><?php echo $this->lang->line('BE_LBL_401'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-warning"></i><?php echo $this->lang->line('BE_LBL_337'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-shopping-cart"></i><?php echo $this->lang->line('BE_MR_4'); ?>
												</th>
											</tr>
											</thead>
											<tbody>
											<?php
											if ($rsFileOrdersSrchd) {
												foreach ($rsFileOrdersSrchd as $row) {
													$totalOrders = $row->P + $row->CA + $row->IP + $row->NA;
													?>
													<tr>
														<td valign="middle"><?php echo stripslashes($row->PackageTitle); ?></td>
														<td align="center"
															valign="middle"><?php echo $row->P; ?></td>
														<td align="center"
															valign="middle"><?php echo $row->IP; ?></td>
														<td align="center"
															valign="middle"><?php echo $row->CA; ?></td>
														<td align="center"
															valign="middle"><?php echo $row->NA; ?></td>
														<td align="center"
															valign="middle"><?php echo $totalOrders; ?></td>
													</tr>
													<?php
												}
											} else {
												?>
												<tr>
													<td colspan='6'><?php echo $this->lang->line('BE_LBL_403'); ?></td>
												</tr>
											<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
								<div class="tab-pane" id="tab_6_33">
									<div class="portlet-body">
										<table class="table table-striped table-bordered table-advance table-hover">
											<thead>
											<tr>
												<th>
													<i class="fa fa-reorder"></i><?php echo $this->lang->line('BE_PCK_HD_6'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-stop"></i><?php echo $this->lang->line('BE_LBL_400'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-lock"></i><?php echo $this->lang->line('BE_MR_7'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-unlock"></i><?php echo $this->lang->line('BE_LBL_401'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-warning"></i><?php echo $this->lang->line('BE_LBL_337'); ?>
												</th>
												<th style="text-align:center;"><i
														class="fa fa-shopping-cart"></i><?php echo $this->lang->line('BE_MR_4'); ?>
												</th>
											</tr>
											</thead>
											<tbody>
											<?php
											if ($rsServerOrdersSrchd) {
												foreach ($rsServerOrdersSrchd as $row) {
													$totalOrders = $row->P + $row->CA + $row->IP + $row->NA;
													?>
													<tr>
														<td valign="middle"><?php echo stripslashes($row->LogPackageTitle); ?></td>
														<td align="center"
															valign="middle"><?php echo $row->P; ?></td>
														<td align="center"
															valign="middle"><?php echo $row->IP; ?></td>
														<td align="center"
															valign="middle"><?php echo $row->CA; ?></td>
														<td align="center"
															valign="middle"><?php echo $row->NA; ?></td>
														<td align="center"
															valign="middle"><?php echo $totalOrders; ?></td>
													</tr>
													<?
												}
											} else {
												?>
												<tr>
													<td colspan='6'><?php echo $this->lang->line('BE_LBL_403'); ?></td>
												</tr>
											<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
								<!--tab-pane-->
							</div>
						</div>

					<?php } ?>
					<!---todaysummary start--->
					<h4 style="color:#D12610;"><b><?php echo $this->lang->line('BE_MR_1'); ?></b></h4>
					<div class="tabbable tabbable-custom tabbable-custom-profile">
						<ul class="nav nav-tabs nav-tabs-solid border-0">
							<li class="nav-item">
								<a href="#tab_1_11"
								   data-toggle="tab" class="nav-link active"><?php echo $this->lang->line('BE_LBL_397'); ?></a>
							</li>
							<li class="nav-item">
								<a href="#tab_1_22"
								   data-toggle="tab" class="nav-link"><?php echo $this->lang->line('BE_LBL_398'); ?></a>
							</li>
							<li class="nav-item">
								<a href="#tab_1_33"
								   data-toggle="tab" class="nav-link"><?php echo $this->lang->line('BE_LBL_399'); ?></a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1_11">
								<div class="portlet-body">
									<table class="table table-striped table-bordered table-advance table-hover">
										<thead>
										<tr>
											<th style="text-align:center;"><i
													class="fa fa-stop"></i> <?php echo $this->lang->line('BE_LBL_400'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-lock"></i> <?php echo $this->lang->line('BE_MR_7'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-unlock"></i> <?php echo $this->lang->line('BE_LBL_401'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-warning"></i> <?php echo $this->lang->line('BE_LBL_337'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-shopping-cart"></i> <?php echo $this->lang->line('BE_MR_4'); ?>
											</th>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td align="center" valign="middle"><?php echo $pending_Today; ?></td>
											<td align="center" valign="middle"><?php echo $inProcess_Today; ?></td>
											<td align="center" valign="middle"><?php echo $available_Today; ?></td>
											<td align="center"
												valign="middle"><?php echo $notAvailable_Today; ?></td>
											<td align="center"
												valign="middle"><?php echo $pending_Today + $inProcess_Today + $available_Today + $notAvailable_Today; ?></td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!--tab-pane-->
							<div class="tab-pane" id="tab_1_22">
								<div class="portlet-body">
									<table class="table table-striped table-bordered table-advance table-hover">
										<thead>
										<tr>
											<th style="text-align:center;"><i
													class="fa fa-stop"></i> <?php echo $this->lang->line('BE_LBL_400'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-lock"></i> <?php echo $this->lang->line('BE_MR_7'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-unlock"></i> <?php echo $this->lang->line('BE_LBL_401'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-warning"></i> <?php echo $this->lang->line('BE_LBL_337'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-shopping-cart"></i> <?php echo $this->lang->line('BE_MR_4'); ?>
											</th>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td align="center"
												valign="middle"><?php echo $pending_Today_File; ?></td>
											<td align="center"
												valign="middle"><?php echo $inProcess_Today_File; ?></td>
											<td align="center"
												valign="middle"><?php echo $available_Today_File; ?></td>
											<td align="center"
												valign="middle"><?php echo $notAvailable_Today_File; ?></td>
											<td align="center"
												valign="middle"><?php echo $pending_Today_File + $inProcess_Today_File + $available_Today_File + $notAvailable_Today_File; ?></td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane" id="tab_1_33">
								<div class="portlet-body">
									<table class="table table-striped table-bordered table-advance table-hover">
										<thead>
										<tr>
											<th style="text-align:center;"><i
													class="fa fa-stop"></i> <?php echo $this->lang->line('BE_LBL_400'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-lock"></i> <?php echo $this->lang->line('BE_MR_7'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-unlock"></i> <?php echo $this->lang->line('BE_LBL_401'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-warning"></i> <?php echo $this->lang->line('BE_LBL_337'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-shopping-cart"></i> <?php echo $this->lang->line('BE_MR_4'); ?>
											</th>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td align="center"
												valign="middle"><?php echo $pending_Today_Server; ?></td>
											<td align="center"
												valign="middle"><?php echo $inProcess_Today_Server; ?></td>
											<td align="center"
												valign="middle"><?php echo $available_Today_Server; ?></td>
											<td align="center"
												valign="middle"><?php echo $notAvailable_Today_Server; ?></td>
											<td align="center"
												valign="middle"><?php echo $pending_Today_Server + $inProcess_Today_Server + $available_Today_Server + $notAvailable_Today_Server; ?></td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!--tab-pane-->
						</div>
					</div>

					<?php
					//stats_todaysummary.php end
					echo '<br />';
					?>
					<h4 style="color:#D12610;"><b><?php echo $this->lang->line('BE_LBL_402'); ?></b></h4>
					<div class="tabbable tabbable-custom tabbable-custom-profile">
						<ul class="nav nav-tabs nav-tabs-solid border-0">
							<li class="nav-item">
								<a href="#tab_2_11"
								   data-toggle="tab" class="nav-link active"><?php echo $this->lang->line('BE_LBL_397'); ?></a>
							</li>
							<li class="nav-item">
								<a href="#tab_2_22"
								   data-toggle="tab" class="nav-link"><?php echo $this->lang->line('BE_LBL_398'); ?></a>
							</li>
							<li class="nav-item">
								<a href="#tab_2_33"
								   data-toggle="tab" class="nav-link"><?php echo $this->lang->line('BE_LBL_399'); ?></a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_2_11">
								<div class="portlet-body">
									<table class="table table-striped table-bordered table-advance table-hover">
										<thead>
										<tr>
											<th>
												<i class="fa fa-reorder"></i> <?php echo $this->lang->line('BE_PCK_HD'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-stop"></i> <?php echo $this->lang->line('BE_LBL_400'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-lock"></i> <?php echo $this->lang->line('BE_MR_7'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-unlock"></i> <?php echo $this->lang->line('BE_LBL_401'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-warning"></i> <?php echo $this->lang->line('BE_LBL_337'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-shopping-cart"></i> <?php echo $this->lang->line('BE_MR_4'); ?>
											</th>
										</tr>
										</thead>
										<tbody>
										<?php
										if ($rsIMEIOrdersToday) {
											foreach ($rsIMEIOrdersToday as $row) {
												$totalOrders = $row->P + $row->CA + $row->IP + $row->NA;
												?>
												<tr>
													<td valign="middle"><?php echo stripslashes($row->PackageTitle); ?></td>
													<td align="center" valign="middle"><?php echo $row->P; ?></td>
													<td align="center" valign="middle"><?php echo $row->IP; ?></td>
													<td align="center" valign="middle"><?php echo $row->CA; ?></td>
													<td align="center" valign="middle"><?php echo $row->NA; ?></td>
													<td align="center"
														valign="middle"><?php echo $totalOrders; ?></td>
												</tr>
												<?php
											}
										} else {
											?>
											<tr>
												<td colspan='6'><?php echo $this->lang->line('BE_LBL_403'); ?></td>
											</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
							<!--tab-pane-->
							<div class="tab-pane" id="tab_2_22">
								<div class="portlet-body">
									<table class="table table-striped table-bordered table-advance table-hover">
										<thead>
										<tr>
											<th>
												<i class="fa fa-reorder"></i> <?php echo $this->lang->line('BE_LBL_373'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-stop"></i><?php echo $this->lang->line('BE_LBL_400'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-lock"></i><?php echo $this->lang->line('BE_MR_7'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-unlock"></i><?php echo $this->lang->line('BE_LBL_401'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-warning"></i><?php echo $this->lang->line('BE_LBL_337'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-shopping-cart"></i><?php echo $this->lang->line('BE_MR_4'); ?>
											</th>
										</tr>
										</thead>
										<tbody>
										<?php
										if ($rsFileOrdersToday) {
											foreach ($rsFileOrdersToday as $row) {
												$totalOrders = $row->P + $row->CA + $row->IP + $row->NA;
												?>
												<tr>
													<td valign="middle"><?php echo stripslashes($row->PackageTitle); ?></td>
													<td align="center" valign="middle"><?php echo $row->P; ?></td>
													<td align="center" valign="middle"><?php echo $row->IP; ?></td>
													<td align="center" valign="middle"><?php echo $row->CA; ?></td>
													<td align="center" valign="middle"><?php echo $row->NA; ?></td>
													<td align="center"
														valign="middle"><?php echo $totalOrders; ?></td>
												</tr>
												<?php
											}
										} else {
											?>
											<tr>
												<td colspan='6'><?php echo $this->lang->line('BE_LBL_403'); ?></td>
											</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane" id="tab_2_33">
								<div class="portlet-body">
									<table class="table table-striped table-bordered table-advance table-hover">
										<thead>
										<tr>
											<th>
												<i class="fa fa-reorder"></i><?php echo $this->lang->line('BE_PCK_HD_6'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-stop"></i><?php echo $this->lang->line('BE_LBL_400'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-lock"></i><?php echo $this->lang->line('BE_MR_7'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-unlock"></i><?php echo $this->lang->line('BE_LBL_401'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-warning"></i><?php echo $this->lang->line('BE_LBL_337'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-shopping-cart"></i><?php echo $this->lang->line('BE_MR_4'); ?>
											</th>
										</tr>
										</thead>
										<tbody>
										<?php
										if ($rsServerOrdersToday) {
											foreach ($rsServerOrdersToday as $row) {
												$totalOrders = $row->P + $row->CA + $row->IP + $row->NA;
												?>
												<tr>
													<td valign="middle"><?php echo stripslashes($row->LogPackageTitle); ?></td>
													<td align="center" valign="middle"><?php echo $row->P; ?></td>
													<td align="center" valign="middle"><?php echo $row->IP; ?></td>
													<td align="center" valign="middle"><?php echo $row->CA; ?></td>
													<td align="center" valign="middle"><?php echo $row->NA; ?></td>
													<td align="center"
														valign="middle"><?php echo $totalOrders; ?></td>
												</tr>
												<?
											}
										} else {
											?>
											<tr>
												<td colspan='6'><?php echo $this->lang->line('BE_LBL_403'); ?></td>
											</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
							<!--tab-pane-->
						</div>
					</div>
					<?php //stats_todayorders.php end ?>

					<?php //stats_allordersummary start ?>
					<br>
					<h4 style="color:#D12610;"><b><?php echo $this->lang->line('BE_MR_3'); ?></b></h4>
					<div class="tabbable tabbable-custom tabbable-custom-profile">
						<ul class="nav nav-tabs nav-tabs-solid border-0">
							<li class="nav-item">
								<a href="#tab_3_11"
								   data-toggle="tab" class="nav-link  active"><?php echo $this->lang->line('BE_LBL_397'); ?></a>
							</li>
							<li class="nav-item">
								<a href="#tab_3_22"
								   data-toggle="tab" class="nav-link"><?php echo $this->lang->line('BE_LBL_398'); ?></a>
							</li>
							<li class="nav-item">
								<a href="#tab_3_33"
								   data-toggle="tab" class="nav-link"><?php echo $this->lang->line('BE_LBL_399'); ?></a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_3_11">
								<div class="portlet-body">
									<table class="table table-striped table-bordered table-advance table-hover">
										<thead>
										<tr>
											<th style="text-align:center;"><i
													class="fa fa-stop"></i> <?php echo $this->lang->line('BE_LBL_400'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-lock"></i> <?php echo $this->lang->line('BE_MR_7'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-unlock"></i> <?php echo $this->lang->line('BE_LBL_401'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-warning"></i> <?php echo $this->lang->line('BE_LBL_337'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-shopping-cart"></i> <?php echo $this->lang->line('BE_MR_4'); ?>
											</th>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td align="center" valign="middle"><?php echo $pending_All; ?></td>
											<td align="center" valign="middle"><?php echo $inProcess_All; ?></td>
											<td align="center" valign="middle"><?php echo $available_All; ?></td>
											<td align="center" valign="middle"><?php echo $notAvailable_All; ?></td>
											<td align="center"
												valign="middle"><?php echo $pending_All + $inProcess_All + $available_All + $notAvailable_All; ?></td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!--tab-pane-->
							<div class="tab-pane" id="tab_3_22">
								<div class="portlet-body">
									<table class="table table-striped table-bordered table-advance table-hover">
										<thead>
										<tr>
											<th style="text-align:center;"><i
													class="fa fa-stop"></i><?php echo $this->lang->line('BE_LBL_400'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-lock"></i><?php echo $this->lang->line('BE_MR_7'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-unlock"></i><?php echo $this->lang->line('BE_LBL_401'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-warning"></i><?php echo $this->lang->line('BE_LBL_337'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-shopping-cart"></i><?php echo $this->lang->line('BE_MR_4'); ?>
											</th>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td align="center" valign="middle"><?php echo $pending_All_File; ?></td>
											<td align="center"
												valign="middle"><?php echo $inProcess_All_File; ?></td>
											<td align="center"
												valign="middle"><?php echo $available_All_File; ?></td>
											<td align="center"
												valign="middle"><?php echo $notAvailable_All_File; ?></td>
											<td align="center"
												valign="middle"><?php echo $pending_All_File + $inProcess_All_File + $available_All_File + $notAvailable_All_File; ?></td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane" id="tab_3_33">
								<div class="portlet-body">
									<table class="table table-striped table-bordered table-advance table-hover">
										<thead>
										<tr>
											<th style="text-align:center;"><i
													class="fa fa-stop"></i><?php echo $this->lang->line('BE_LBL_400'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-lock"></i><?php echo $this->lang->line('BE_MR_7'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-unlock"></i><?php echo $this->lang->line('BE_LBL_401'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-warning"></i><?php echo $this->lang->line('BE_LBL_337'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-shopping-cart"></i><?php echo $this->lang->line('BE_MR_4'); ?>
											</th>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td align="center"
												valign="middle"><?php echo $pending_All_Server; ?></td>
											<td align="center"
												valign="middle"><?php echo $inProcess_All_Server; ?></td>
											<td align="center"
												valign="middle"><?php echo $available_All_Server; ?></td>
											<td align="center"
												valign="middle"><?php echo $notAvailable_All_Server; ?></td>
											<td align="center"
												valign="middle"><?php echo $pending_All_Server + $inProcess_All_Server + $available_All_Server + $notAvailable_All_Server; ?></td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!--tab-pane-->
						</div>
					</div>
					<?php //stats_allordersummary ends ?>

					<?php //stats_allorders start ?>
					<br>
					<h4 style="color:#D12610;"><b><?php echo $this->lang->line('BE_MR_13'); ?></b></h4>
					<div class="tabbable tabbable-custom tabbable-custom-profile">
						<ul class="nav nav-tabs nav-tabs-solid border-0">
							<li class="nav-item">
								<a href="#tab_4_11"
								   data-toggle="tab" class="nav-link active"><?php echo $this->lang->line('BE_LBL_397'); ?></a>
							</li>
							<li class="nav-item">
								<a href="#tab_4_22"
								   data-toggle="tab" class="nav-link"><?php echo $this->lang->line('BE_LBL_398'); ?></a>
							</li>
							<li class="nav-item">
								<a href="#tab_4_33"
								   data-toggle="tab" class="nav-link"><?php echo $this->lang->line('BE_LBL_399'); ?></a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_4_11">
								<div class="portlet-body">
									<table class="table table-striped table-bordered table-advance table-hover">
										<thead>
										<tr>
											<th>
												<i class="fa fa-reorder"></i> <?php echo $this->lang->line('BE_PCK_HD'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-stop"></i> <?php echo $this->lang->line('BE_LBL_400'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-lock"></i> <?php echo $this->lang->line('BE_MR_7'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-unlock"></i> <?php echo $this->lang->line('BE_LBL_401'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-warning"></i> <?php echo $this->lang->line('BE_LBL_337'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-shopping-cart"></i> <?php echo $this->lang->line('BE_MR_4'); ?>
											</th>
										</tr>
										</thead>
										<tbody>
										<?
										if ($rsIMEIOrdersAll) {
											foreach ($rsIMEIOrdersAll as $row) {
												$totalOrders = $row->P + $row->CA + $row->IP + $row->NA;
												?>
												<tr>
													<td valign="middle"><?php echo stripslashes($row->PackageTitle); ?></td>
													<td align="center" valign="middle"><?php echo $row->P; ?></td>
													<td align="center" valign="middle"><?php echo $row->IP; ?></td>
													<td align="center" valign="middle"><?php echo $row->CA; ?></td>
													<td align="center" valign="middle"><?php echo $row->NA; ?></td>
													<td align="center"
														valign="middle"><?php echo $totalOrders; ?></td>
												</tr>
												<?
											}
										} else {
											?>
											<tr>
												<td colspan='6'><?php echo $this->lang->line('BE_LBL_403'); ?></td>
											</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
							<!--tab-pane-->
							<div class="tab-pane" id="tab_4_22">
								<div class="portlet-body">
									<table class="table table-striped table-bordered table-advance table-hover">
										<thead>
										<tr>
											<th>
												<i class="fa fa-reorder"></i><?php echo $this->lang->line('BE_LBL_373'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-stop"></i><?php echo $this->lang->line('BE_LBL_400'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-lock"></i><?php echo $this->lang->line('BE_MR_7'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-unlock"></i><?php echo $this->lang->line('BE_LBL_401'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-warning"></i><?php echo $this->lang->line('BE_LBL_337'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-shopping-cart"></i><?php echo $this->lang->line('BE_MR_4'); ?>
											</th>
										</tr>
										</thead>
										<tbody>
										<?
										if ($rsFileOrdersAll) {
											foreach ($rsFileOrdersAll as $row) {
												$totalOrders = $row->P + $row->CA + $row->IP + $row->NA;
												?>
												<tr>
													<td valign="middle"><?php echo stripslashes($row->PackageTitle); ?></td>
													<td align="center" valign="middle"><?php echo $row->P; ?></td>
													<td align="center" valign="middle"><?php echo $row->IP; ?></td>
													<td align="center" valign="middle"><?php echo $row->CA; ?></td>
													<td align="center" valign="middle"><?php echo $row->NA; ?></td>
													<td align="center"
														valign="middle"><?php echo $totalOrders; ?></td>
												</tr>
												<?
											}
										} else {
											?>
											<tr>
												<td colspan='6'><?php echo $this->lang->line('BE_LBL_403'); ?></td>
											</tr>";
										<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane" id="tab_4_33">
								<div class="portlet-body">
									<table class="table table-striped table-bordered table-advance table-hover">
										<thead>
										<tr>
											<th>
												<i class="fa fa-reorder"></i><?php echo $this->lang->line('BE_PCK_HD_6'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-stop"></i><?php echo $this->lang->line('BE_LBL_400'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-lock"></i><?php echo $this->lang->line('BE_MR_7'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-unlock"></i><?php echo $this->lang->line('BE_LBL_401'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-warning"></i><?php echo $this->lang->line('BE_LBL_337'); ?>
											</th>
											<th style="text-align:center;"><i
													class="fa fa-shopping-cart"></i><?php echo $this->lang->line('BE_MR_4'); ?>
											</th>
										</tr>
										</thead>
										<tbody>
										<?
										if ($rsServerOrdersAll) {
											foreach ($rsServerOrdersAll as $row) {
												$totalOrders = $row->P + $row->CA + $row->IP + $row->NA;
												?>
												<tr>
													<td valign="middle"><?php echo stripslashes($row->LogPackageTitle); ?></td>
													<td align="center" valign="middle"><?php echo $row->P; ?></td>
													<td align="center" valign="middle"><?php echo $row->IP; ?></td>
													<td align="center" valign="middle"><?php echo $row->CA; ?></td>
													<td align="center" valign="middle"><?php echo $row->NA; ?></td>
													<td align="center"
														valign="middle"><?php echo $totalOrders; ?></td>
												</tr>
												<?
											}
										} else {
											?>
											<tr>
												<td colspan='6'><?php echo $this->lang->line('BE_LBL_403'); ?></td>
											</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
							<!--tab-pane-->
						</div>
					</div>
					<?php //stats_allorders end ?>

				</div>
			</div>
		</div>
	</div>
	<?php /*include 'jscomponents.php'; */ ?>
