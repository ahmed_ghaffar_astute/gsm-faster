<!-- Page header -->
<div class="page-header border-bottom-0">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_191'); ?></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

    </div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">

    <div class="card">
        <div class="form-group row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
                <div class="card-body">
                    <!-- Our Working Area Start -->
                    <?php if (isset($message) && $message != '') { ?>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <div class="alert alert-success"><?php echo $message; ?></div>

                            </div>
                        </div>
                    <?php } ?>
                    <!-- Copy till this to paste to other pages -->

                <?php echo form_open(base_url('admin/tickets/tickets?frmId=52&fTypeId=12') , 'class="horizontal-form"  id="frm" name="frm"');?>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo $this->lang->line('BE_PM_3'); ?></label>
                                            <input type="text" name="txtUName" value="<?php echo($uName);?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo $this->lang->line('BE_LBL_727'); ?></label>
                                            <input type="text" name="txtTcktNo" value="<?php echo($tcktNo);?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo $this->lang->line('BE_LBL_726'); ?></label>
                                            <select name="dId" class="form-control select2me" data-placeholder="<?php echo $this->lang->line('BE_LBL_623'); ?>">
                                                <option value="0"><?php echo $this->lang->line('BE_LBL_623'); ?></option>
                                                <?php FillCombo($dId, $dept_data); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo $this->lang->line('BE_CAT_1'); ?></label>
                                            <select name="cId" class="form-control select2me" data-placeholder="<?php echo $this->lang->line('BE_LBL_623'); ?>">
                                                <option value="0"><?php echo $this->lang->line('BE_LBL_623'); ?></option>
                                                <?php FillCombo($cId, $dept_cat); ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo $this->lang->line('BE_LBL_189'); ?></label>
                                            <select name="pId" class="form-control select2me" data-placeholder="<?php echo $this->lang->line('BE_LBL_623'); ?>">
                                                <option value="0"><?php echo $this->lang->line('BE_LBL_623'); ?></option>
                                                <?php FillCombo($pId, $tckt_priority); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo $this->lang->line('BE_LBL_60'); ?></label>
                                            <select name="records" class="form-control select2me" data-placeholder="Select...">
                                                <?php
                                                    $NO_OF_RECORDS = array("10"=>10, "50"=>50, "100"=>100, "200"=>200, "300"=>300, "400"=>400, "500"=>500, "1000"=>1000); 
                                                    foreach ($NO_OF_RECORDS as $key => $value)
                                                    {
                                                        $selected = '';
                                                        if($limit == $key)
                                                            $selected = 'selected';
                                                        echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo $this->lang->line('BE_PM_7'); ?></label><br />
                                            <input class="form-control form-control-inline input-largest date-picker" type="text" name="txtFromDt" value="<?php echo($dtFrom);?>" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo $this->lang->line('BE_PM_8'); ?></label><br />
                                            <input class="form-control form-control-inline input-largest date-picker" type="text" name="txtToDt" value="<?php echo($dtTo);?>" />
                                        </div>
                                    </div>
                                </div>
                             <div class="form-group row">
                                    <div class="col-lg-12">
                                <button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><?php echo $this->lang->line('BE_GNRL_BTN_1'); ?></button>
                                <input type="hidden" name="cldFrm" id="cldFrm" value="0" />
                                <input type="hidden" name="start" id="start" value="<?php echo $start; ?>" />
                            </div>
                        </div>
                            </div>
                            
                        </div>
                    </div>

                    <?php
                    if($count != 0)
                    {
                        if($totalRows > $limit)
                            doPages_DropDown($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, $pLast, $thisp, $next, 'frm');
                    }
                    ?>
                    <div class="portlet box blue">
                     
                        <div class="portlet-body">
                            <div class="table-responsive">
                                <p align="right">
                                    <input type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="<?php echo $this->lang->line('BE_LBL_729'); ?>" onClick="return validate(3);" class="btn btn-primary" name="btnSubmit" />
                                    <input type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> value="<?php echo $this->lang->line('BE_LBL_728'); ?>" onClick="return validate(1);" class="btn btn-primary" name="btnSubmit" />
                                </p>
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                    <tr>
                                        <th nowrap="nowrap" width="3%">Sr. #</th>
                                        <th><?php echo $this->lang->line('BE_CAT_1'); ?></th>
                                        <th><?php echo $this->lang->line('BE_LBL_189'); ?></th>
                                        <th><?php echo $this->lang->line('BE_LBL_726'); ?></th>
                                        <th><?php echo $this->lang->line('BE_LBL_179'); ?></th>
                                        <th><?php echo $this->lang->line('BE_CODE_7'); ?></th>
                                        <th><?php echo $this->lang->line('BE_LBL_382'); ?></th>
                                        <th><?php echo $this->lang->line('BE_CODE_5'); ?></th>
                                        <th style="text-align:center">
                                            <input type="checkbox" class="chkSelect" id="chkSelect" name="chkSelect" onClick="selectAllChxBxs('chkSelect', 'chkTckts', <?php echo $count; ?>);" value="true">
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if($count != 0)
                                    {
                                        $i = 0;
                                        foreach($rsTckts as $row)
                                        {
                                            $name = $row->Name;
                                            if($name == '')
                                                $name = $row->ClientName;
                                        ?>
                                            <tr>
                                                <td><?php echo $i+1; ?>.</td>
                                                <td><?php echo stripslashes($row->TcktCategory);?></td>
                                                <td <?php echo getTicketBGColor($row->PriorityId); ?>><?php echo($row->TcktPriority);?></td>
                                                <td><?php echo stripslashes($row->DeptName);?></td>
                                                <td><?php if($row->ReadByAdmin == '0') echo '<b>';?>
                                                <a href="<?php echo base_url('admin/tickets/');?>ticket?id=<?php echo $row->TicketId;?>&frmId=<?php echo $this->input->post_get('frmId');?>&fTypeId=<?php echo $this->input->post_get('fTypeId')?>"><i class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i>
                                                <?php echo '#['.$row->TicketNo.'] '.$row->Subject; ?><?php if($row->ReadByAdmin == '0') echo '</b>';?></a></td>
                                                <td><?php echo($row->DtTm);?></td>
                                                <td><?php echo $name;?></td>
                                                <td><b><?php echo($row->TcktStatus);?></b></td>
                                                <td align="center">
                                                    <input type="checkbox" class="chkSelect" id="chkTckts<?php echo $i; ?>" name="chkTckts[]" value="<?php echo $row->TicketNo; ?>">
                                                </td>
                                            </tr>
                                    <?php
                                            $i++;
                                        }
                                    }
                                    else
                                        echo "<tr><td colspan='9'>".$this->lang->line('BE_GNRL_9')."</td></tr>";
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>



<script type="text/javascript">
function validate(z)
{
	var totalTckts = <?php echo $count; ?>;
	var strIds = '0';
	for(var i=0; i<totalTckts; i++)
	{
		if(document.getElementById('chkTckts'+i).checked)
		{
			arr = document.getElementById('chkTckts'+i).value.split('|');
			strIds += ',' + arr[0];
		}
	}
	if(strIds == '0')
	{
		alert('<?php echo $this->lang->line('BE_LBL_53'); ?>');
		return false;
	}
	if(z == '1')
	{
		if(confirm('Are your sure you want to delete the ticket(s)?'))
		{
			document.getElementById('cldFrm').value = '1';
			return true;
		}
		else
			return false;
	}
	else
	{
		document.getElementById('cldFrm').value = z;
		return true;
	}
}
</script>
