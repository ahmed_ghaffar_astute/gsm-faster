<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_58'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<a href="<?php echo base_url('admin/cms/') ?>banners?frmId=<? echo $this->input->post_get('frmId');?>&fTypeId=<? echo $this->input->post_get('fTypeId')?>&type=<? echo $type?>" class="btn btn-sm btn-primary"><span class="fa fa-angle-double-left"></span> Back To List</a>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">

	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<!-- Copy till this to paste to other pages -->
					<?php if($existingBanner != '') { ?>
						<p align="center" valign="middle"><img  style="max-width: 700px" src="<?php echo base_url().$existingBanner ?>" /></p>
					<?php } ?>
					<?php echo form_open(base_url('admin/cms/banner'),array('class'=>"form-horizontal",'name'=>"frm",'method'=>"post",'id'=>"frm",'enctype'=>"multipart/form-data"));?>
					<form >
						<input type="hidden" id="id" name="id" value="<?php echo($id); ?>" />
						<input type="hidden" name="type" value="<? echo $type; ?>" />
						<?php if($existingBanner!=''){ ?>
						<input type="hidden" id="existingBanner" name="existingBanner" value="<?php echo $existingBanner; ?>">
						<?php }else{?>
						<input type="hidden" id="existingBanner" name="existingBanner" value="add">
					<?php	} ?>
						<div class="form-body">
							<div class="form-group">
								<label class="col-md-3 control-label"><? echo $this->lang->line('BE_MR_26'); ?>:<span class="required_field">*</span></label>
								<div class="col-md-4">
									<input type="text" class="form-control" placeholder="Enter Title" maxlength="100" name="txtTitle" id="txtTitle" value="<? echo($title);?>"  />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">URL:</label>
								<div class="col-md-4">
									<input type="text" class="form-control" placeholder="Enter URL (If Any)" maxlength="100" name="txtURL" value="<? echo($url);?>"  />
								</div>
							</div>
							<div class="form-group">
								<label for="txtImage" class="col-md-3 control-label"><? echo $this->lang->line('BE_LBL_58'); ?>:</label>
								<div class="col-md-9">
									<input type="file" id="txtBanner" name="txtBanner">
									<p class="form-text text-muted">
										.jpg, .gif, .png
									</p>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><? echo $this->lang->line('BE_LBL_298'); ?>:</label>
								<div class="col-md-9">
									<textarea class="ckeditor form-control" name="txtDetail" rows="6"><? echo $detail; ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label"><? echo $this->lang->line('BE_GNRL_1'); ?>:</label>
								<div class="col-md-4">
									<div class="switch-button switch-button-lg" data-on-label="YES" data-off-label="NO">
										<input type="checkbox" name="chkDisableBanner" id="chkDisableBanner" <?php echo $disableBanner == 1 ? 'checked' : '';?> class="toggle"/><span><label for="chkDisableBanner"></label></span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-3"></div><div class="col-lg-9">
								<button type="submit" <?php if($IS_DEMO) echo 'disabled="disabled"';?> class="btn btn-primary"><? echo $this->lang->line('BE_LBL_72'); ?></button>
							</div>
						</div>
						<?php echo form_close();?>
						<!-- END FORM-->
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript" src="<?php echo base_url('assets') ?>/plugins/ckeditor/ckeditor.js"></script>
<script language="javascript" src="<?php echo base_url('assets/');?>js/banner.js"></script>
