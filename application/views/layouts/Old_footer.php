</div>
<?php

$this->load->view('layouts/notifications'); ?>
    <!--/page-container end-->

    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/accordion.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/common-script.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.nicescroll.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/gallery/jquery.magnific-popup.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/data-tables/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/data-tables/DT_bootstrap.js');?>"></script>
    <script src="<?php echo base_url('assets/js/chosen.jquery.js')?>"></script>
    <script>
        $(document).ready(function() {
            if ($.fn.magnificPopup) {
                $('.image-zoom').magnificPopup({
                    type: 'image',
                    closeOnContentClick: false,
                    closeBtnInside: true,
                    fixedContentPos: true,
                    mainClass: 'mfp-no-margins mfp-with-zoom',
                    image: {
                        verticalFit: true,
                        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
                    }
                });

            }

            $('#datatable').dataTable();

        });
    </script>
    <script src="<?php echo base_url('assets/js/jquery.sparkline.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/sparkline-chart.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/graph.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/edit-graph.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/kalendar/kalendar.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/kalendar/edit-kalendar.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/knob/jquery.knob.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/select2/js/select2.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-validation/dist/jquery.validate.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-validation/dist/additional-methods.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.fancybox.js?v=2.1.5')?>"></script>

<!-- New calls for Ecommerce -->
	<script type="text/javascript" src="<?=base_url('assets/site_assets/js/jquery-ui.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/site_assets/js/jquery.scrollUp.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/site_assets/js/jquery.meanmenu.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/site_assets/js/jquery.nivo.slider.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/site_assets/js/owl.carousel.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/site_assets/js/slick.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/site_assets/js/wow.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/site_assets/js/plugins.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/site_assets/js/custom_jquery.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/site_assets/js/cust_javascript.js')?>"></script>

</body>

</html>
