<?php if($this->router->fetch_method() != 'view' || $this->router->fetch_method()!= 'transfer_credits') { ?>
<?php if($this->session->flashdata('message')) { ?>
    <div class="alert alert-success"><?php echo $this->session->flashdata('message'); ?></div>
<?php } ?>
<?php if($this->session->flashdata('error_message')) { ?>
    <div class="alert alert-danger"><?php echo $this->session->flashdata('error_message'); ?></div>
<?php } } ?>