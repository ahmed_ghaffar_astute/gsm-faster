<!-- Main navigation -->
<div class="card card-sidebar-mobile">
	<ul class="nav nav-sidebar" data-nav-type="accordion">

		<!-- Main -->
		<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
		<li class="nav-item nav-item-submenu">
			<a href="<?php echo base_url('page/add_credits'); ?>" class="nav-link"><i class="icon-copy"></i> <span><?php echo $this->lang->line('CUST_LBL_232');?></span></a>
		</li>
		<li class="nav-item nav-item-submenu">
			<a href="<?php echo base_url('page/myprofile') ?>" class="nav-link"><i class="icon-color-sampler"></i> <span><?php echo $this->lang->line('CUST_LBL_223');?></span></a>
		</li>
		<li class="nav-item nav-item-submenu">
			<a href="#" class="nav-link"><i class="icon-stack"></i> <span>Google Authentication</span></a>
		</li>
		<li class="nav-item nav-item-submenu"><a href="<?php echo base_url('page/login_security');?>" class="nav-link"><i class="icon-width"></i> <span>Login Security</span></a></li>
		<li class="nav-item nav-item-submenu">
			<a href="#" class="nav-link"><i class="icon-stack2"></i> <span>Order Now</span></a>
			<ul class="nav nav-group-sub" data-submenu-title="Page layouts">
				<li class="nav-item"><a href="<?php echo base_url('page/imei_services');?>" class="nav-link"><?php echo $this->lang->line('CUST_CH_146');?></a></li>
				<li class="nav-item"><a href="<?php echo base_url('page/file_services');?>" class="nav-link"><?php echo $this->lang->line('CUST_LBL_1');?></a></li>
				<li class="nav-item"><a href="<?php echo base_url('page/server_services');?>" class="nav-link">Server Services</a></li>
			</ul>
		</li>
		<li class="nav-item nav-item-submenu">
			<a href="#" class="nav-link"><i class="icon-stack2"></i> <span>Orders History</span></a>
			<ul class="nav nav-group-sub" data-submenu-title="Page layouts">
				<li class="nav-item"><a href="<?php echo base_url('page/imei_orders');?>" class="nav-link"><?php echo $this->lang->line('CUST_DB_ICON3');?></a></li>
				<li class="nav-item"><a href="<?php echo base_url('page/file_orders');?>" class="nav-link"><?php echo $this->lang->line('CUST_LBL_8');?></a></li>
				<li class="nav-item"><a href="<?php echo base_url('page/server_orders');?>" class="nav-link"><?php echo $this->lang->line('CUST_LBL_13');?></a></li>
			</ul>
		</li>
		<?php if($userDetails->TransferCredits == '1') { ?>
			<li class="nav-item nav-item-submenu"><a href="<?php echo base_url('page/transfer_credits');?>" class="nav-link"><i class="icon-width"></i> <span><?php echo $this->lang->line('CUST_LBL_313');?></span></a></li>
		<?php } ?>
		<li class="nav-item nav-item-submenu"><a href="<?php echo base_url('page/changepassword');?>" class="nav-link"><i class="icon-width"></i> <span><?php echo $this->lang->line('CUST_MENU_PWD_1');?></span></a></li>
		<li class="nav-item nav-item-submenu"><a href="<?php echo base_url('page/threshold_amount'); ?>" class="nav-link"><i class="icon-width"></i> <span><?php echo $this->lang->line('CUST_LBL_324')  ; ?></span></a></li>
		<li class="nav-item nav-item-submenu"><a href="<?php echo base_url('page/client_invoices');?>" class="nav-link"><i class="icon-width"></i> <span><?php echo $this->lang->line('CUST_LBL_217');?></span></a></li>
		<li class="nav-item nav-item-submenu"><a href="<?php echo base_url('page/admin_invoices');?>" class="nav-link"><i class="icon-width"></i> <span><?php echo $this->lang->line('CUST_LBL_188')  ; ?></span></a></li>
		<li class="nav-item nav-item-submenu"><a href="<?php echo base_url('page/my_statements');?>" class="nav-link"><i class="icon-width"></i> <span><?php echo $this->lang->line('CUST_LBL_287')  ; ?></span></a></li>
		<?php if($settings->TicketSystem == '1'){ ?>
			<li class="nav-item nav-item-submenu"><a href="<?php echo base_url('page/submit_ticket');?>" class="nav-link"><i class="icon-width"></i> <span><?php echo $this->lang->line('CUST_LBL_285'); ?></span></a></li>
			<li class="nav-item nav-item-submenu"><a href="<?php echo base_url('page/my_tickets');?>" class="nav-link"><i class="icon-width"></i> <span><?php echo $this->lang->line('CUST_LBL_292');?></span></a></li>
		<?php } ?>
		<li class="nav-item nav-item-submenu"><a href="<?php echo base_url('page/product_and_services');?>" class="nav-link"><i class="icon-width"></i> <span><?php echo $this->lang->line('CUST_CH_65');?></span></a></li>
		<li class="nav-item nav-item-submenu"><a href="<?php echo base_url('page/login_history');?>" class="nav-link"><i class="icon-width"></i> <span><?php echo $this->lang->line('CUST_LBL_289');?></span></a></li>
		<li class="nav-item nav-item-submenu"><a href="<?php echo base_url('page/downloads');?>" class="nav-link"><i class="icon-width"></i> <span>Downloads</span></a></li>
		<li class="nav-item nav-item-submenu"><a href="<?php echo base_url('login/logout');?>" class="nav-link"><i class="icon-width"></i> <span><?php echo $this->lang->line('CUST_LBL_38');?></span></a></li>
		<!-- /main -->
	</ul>
</div>
<!-- /main navigation -->
