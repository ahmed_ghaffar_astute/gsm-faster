<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?php echo $this->config->item('app_name'); ?></title>
	<?php switchlanguage(); ?>
	<?php if ($this->session->userdata('AdminFavIcon') != '') { ?>
		<link rel="shortcut icon" href="<?php echo $this->session->userdata('AdminFavIcon'); ?>" type="image/x-icon" />
	<?php } ?>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/AstuteSol.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/global_assets/css/icons/icomoon/styles.min.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/global_assets/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/global_assets/css/colors.min.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/global_assets/css/bootstrap_limitless.min.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/global_assets/css/layout.min.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/global_assets/css/components.min.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/plugins/jquery-multi-select/css/multi-select.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('assets/plugins/bootstrap-toastr/toastr.min.css')?>" rel="stylesheet" type="text/css" >
	<link href="<?php echo base_url('assets/css/jquery.fancybox.css?v=2.1.5')?>"  rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->
	<link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">

	<!-- Core JS files -->
	<script src="<?php echo base_url('assets/global_assets/js/main/jquery.min.js') ?>"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
	<script src="<?php echo base_url('assets/global_assets/js/main/bootstrap.bundle.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/global_assets/js/fullcalendar_basic.js') ?>"></script>
	<script src="<?php echo base_url('assets/global_assets/js/calender/main.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/global_assets/js/calender/1main.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/global_assets/js/calender/2main.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/global_assets/js/calender/3main.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/global_assets/js/calender/4main.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/global_assets/js/plugins/loaders/blockui.min.js') ?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.js"></script>
	<!-- /core JS files -->
	<style>
		#main-content{
			width: 100%;
		}
	</style>
	<script>
		var base_url = '<?php echo base_url(); ?>';
		var baseurl = '<?php echo base_url(); ?>';
		$(document).ready(function() {

			$(".select2me").select2();

			$(".js-select2-multi").select2();

			$(".large").select2({
				dropdownCssClass: "big-drop",
				scrollAfterSelect: true
			});


		});
	</script>
	<script>
		$(document).ready(function(){
			$.ajaxSetup({
              beforeSend:function(jqXHR, Obj){
                  var value = "; " + document.cookie;
                  var parts = value.split("; csrf_cookie_name=");
                  if(parts.length == 2)   
                  Obj.data += '&csrf_test_name='+parts.pop().split(";").shift();
              },
              complete:function(){
                var value = "; " + document.cookie;
                  var parts = value.split("; csrf_cookie_name=");
                  if(parts.length == 2){
                      $('form [name="csrf_test_name"]').val(parts.pop().split(";").shift());
                  }
              }
          	});
		});
	</script>
</head>
<body>

<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-dark navbar-static">
	<div class="navbar-brand">
		<a href="<?php echo base_url('home');?>" class="d-inline-block">
			<img src="<?php echo base_url('assets/images/logos') ?>/1.png" alt="">
		</a>
	</div>

	<div class="d-md-none">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
			<i class="icon-tree5"></i>
		</button>
		<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
			<i class="icon-paragraph-justify3"></i>
		</button>
	</div>

	<div class="collapse navbar-collapse" id="navbar-mobile">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a href="<?php echo base_url('dashboard');?>" class="navbar-nav-link"><i class="fa fa-desktop mr-2"></i>Dashboard</a>
			</li>
			<li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-thumbs-o-up mr-2"></i>Order Now</a>
				<div class="dropdown-menu">
					<a href="<?php echo base_url('page/imei_services');?>" class="nav-link dropdown-item"><!-- <i class="icon-align-center-horizontal"></i> --><?php echo $this->lang->line('CUST_CH_146');?></a>
					<a href="<?php echo base_url('page/file_services');?>" class="nav-link dropdown-item"><?php echo $this->lang->line('CUST_LBL_1');?></a>
					<a href="<?php echo base_url('page/server_services');?>" class="nav-link dropdown-item">Server Services</a>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-lock mr-2"></i>Orders History</a>
				<div class="dropdown-menu">
					<a href="<?php echo base_url('page/imei_orders');?>" class="nav-link dropdown-item"><?php echo $this->lang->line('CUST_DB_ICON3');?></a>
					<a href="<?php echo base_url('page/file_orders');?>" class="nav-link dropdown-item"><?php echo $this->lang->line('CUST_LBL_8');?></a>
					<a href="<?php echo base_url('page/server_orders');?>" class="nav-link dropdown-item"><?php echo $this->lang->line('CUST_LBL_13');?></a>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ticket mr-2"></i> <span><?php echo $this->lang->line('CUST_LBL_292');?></span></a>
				<?php if($settings->TicketSystem == '1'){ ?>
				<div class="dropdown-menu">
					<a href="<?php echo base_url('page/submit_ticket');?>" class="nav-link dropdown-item"><span><?php echo $this->lang->line('CUST_LBL_285'); ?></span></a>
					<a href="<?php echo base_url('page/my_tickets');?>" class="nav-link dropdown-item"><span><?php echo $this->lang->line('CUST_LBL_292');?></span></a>
				</div>
				<?php } ?>
			</li>
			<li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-folder-open-o mr-2"></i><span>Invoices</span></a>
				<div class="dropdown-menu">
					<a href="<?php echo base_url('page/client_invoices');?>" class="nav-link dropdown-item"><span><?php echo $this->lang->line('CUST_LBL_217');?></span></a>
					<a href="<?php echo base_url('page/admin_invoices');?>" class="nav-link dropdown-item"><span><?php echo $this->lang->line('CUST_LBL_188')  ; ?></span></a>
				</div>
			</li>
<!--
			<li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-list-ul mr-2"></i> <span>Other</span></a>
				<div class="dropdown-menu">
					<a href="<?php echo base_url('page/transfer_credits');?>" class="nav-link dropdown-item"><span><?php echo $this->lang->line('CUST_LBL_313');?></span></a>
					<a href="<?php echo base_url('page/threshold_amount'); ?>" class="nav-link dropdown-item"><span><?php echo $this->lang->line('CUST_LBL_324')  ; ?></span></a>
					<a href="<?php echo base_url('page/my_statements');?>" class="nav-link dropdown-item"><span><?php echo $this->lang->line('CUST_LBL_287')  ; ?></span></a>
					<a href="<?php echo base_url('page/product_and_services');?>" class="nav-link dropdown-item"><span><?php echo $this->lang->line('CUST_CH_65');?></span></a>
					<a href="<?php echo base_url('page/downloads');?>" class="nav-link dropdown-item"><span>Downloads</span></a>
					<a href="#" class="nav-link dropdown-item"><span>Google Authentication</span></a>
				</div>
			</li>
			-->
			<li class="nav-item">
				<a href="<?php echo base_url('page/add_credits'); ?>" class="navbar-nav-link">+ Add Credits</a>
			</li>
			<li class="nav-item dropdown dropdown-user">
				<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
					<!--<img src="<?php echo base_url('assets/images/avatar1.jpg'); ?>" class="rounded-circle mr-2" height="34" alt="">-->
					<span>Welcome <?php echo $this->session->userdata('UserFName'); ?></span>
				</a>

				<div class="dropdown-menu dropdown-menu-right">

					<a href="<?php echo base_url('page/myprofile') ?>" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
					<a href="<?php echo base_url('page/login_history');?>" class="dropdown-item"><i class="icon-user-plus"></i> <span><?php echo $this->lang->line('CUST_LBL_289');?></span></a>
					<a href="<?php echo base_url('page/transfer_credits');?>" class="nav-link dropdown-item"><span><?php echo $this->lang->line('CUST_LBL_313');?></span></a>
					<a href="<?php echo base_url('page/threshold_amount'); ?>" class="nav-link dropdown-item"><span><?php echo $this->lang->line('CUST_LBL_324')  ; ?></span></a>
					<a href="<?php echo base_url('page/my_statements');?>" class="nav-link dropdown-item"><span><?php echo $this->lang->line('CUST_LBL_287')  ; ?></span></a>
					<a href="<?php echo base_url('page/product_and_services');?>" class="nav-link dropdown-item"><span><?php echo $this->lang->line('CUST_CH_65');?></span></a>
					<a href="<?php echo base_url('page/downloads');?>" class="nav-link dropdown-item"><span>Downloads</span></a>
					<a href="#" class="nav-link dropdown-item"><span>Google Authentication</span></a>
					<a href="<?php echo base_url('page/login_security');?>" class="dropdown-item"><i class="icon-width"></i> <span>Login Security</span></a>

					<div class="dropdown-divider"></div>
					<a href="<?php echo base_url('login/logout');?>" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
				</div>
			</li>
		</ul>
	</div>
</div>
<!-- /main navbar -->

<!-- Page content -->
<div class="page-content">
