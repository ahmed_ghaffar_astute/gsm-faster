<div class="header navbar navbar-inverse box-shadow navbar-fixed-top">
    <div class="navbar-inner">
        <div class="header-seperation">
            <ul class="nav navbar-nav">
                <li class="sidebar-toggle-box"> <a href="#"><i class="fa fa-bars"></i></a> </li>
                <li> <a href="<?php echo base_url('dashboard'); ?>"><strong><?php echo $this->config->item('app_name'); ?></strong></a> </li>
                <li class="hidden-xs"> <a href="#"><i class="fa fa-angle-double-left"></i> Go to the front page</a> </li>
                <li class="">
                    <div class="hov">
                        <div class="btn-group"> <a data-toggle="dropdown" href="" class="con"><span class="fa fa-bell"></span><span class="label label-danger">33</span></a>
                            <ul role="menu" class="dropdown-menu pull-right dropdown-alerts">
                                <li class="title"><span class="icon icon-bell"></span>&nbsp;&nbsp;There are 5 new alerts in the system...</li>
                                <li class="alert">
                                    <div class="alert-icon alt-default"><span class="fa fa-check-square"></span></div>
                                    <div class="alert-content">Quality check was successful</div>
                                    <div class="alert-time">32 sec ago</div>
                                </li>
                                <li class="alert">
                                    <div class="alert-icon alt-primary"><span class="fa fa-plus-square"></span></div>
                                    <div class="alert-content">New user added (John Doe)</div>
                                    <div class="alert-time">11 min ago</div>
                                </li>
                                <li class="alert">
                                    <div class="alert-icon alt-warning"><span class="fa icon-pencil-square"></span></div>
                                    <div class="alert-content">User profile updated (John Doe)</div>
                                    <div class="alert-time">3 hours ago</div>
                                </li>
                                <li class="alert" style="position:relative;">
                                    <div class="alert-icon alt-danger"><span class="fa fa-warning"></span></div>
                                    <div class="alert-content">System failure reported</div>
                                    <div class="alert-time">2 days ago</div>
                                </li>
                            </ul>
                        </div>
                        <div class="btn-group"> <a data-toggle="dropdown" href="" class="con"><span class="fa fa-envelope"></span><span class="label label-success">7</span></a>
                            <ul role="menu" class="dropdown-menu pull-right dropdown-messages">
                                <li class="title"><span class="fa fa-envelope"></span>&nbsp;&nbsp;You have 13 new messages to read...</li>
                                <li class="message">
                                    <div class="message-icon"><img src="images/avatar2.jpg"></div>
                                    <div class="message-content"><a href="#">John Doe</a>
                                        <br> Lorem ipsum dolor sit amet...</div>
                                    <div class="message-time">32 sec ago</div>
                                </li>
                                <li class="message">
                                    <div class="message-icon"><img src="images/avatar2.jpg"></div>
                                    <div class="message-content"><a href="#">John Doe</a>
                                        <br> Quisque commodo sed ipsum...</div>
                                    <div class="message-time">11 min ago</div>
                                </li>
                                <li class="message">
                                    <div class="message-icon"><img src="images/avatar2.jpg"></div>
                                    <div class="message-content"><a href="#">John Doe</a>
                                        <br> Consectetur adipiscing elit...</div>
                                    <div class="message-time">3 hours ago</div>
                                </li>
                                <li class="message" style="position:relative;">
                                    <div class="message-icon"><img src="images/avatar2.jpg"></div>
                                    <div class="message-content"><a href="#">John Doe</a>
                                        <br> Quisque commodo sed ipsum...</div>
                                    <div class="message-time">2 days ago</div>
                                </li>
                            </ul>
                        </div>
                        <div class="btn-group"> <a data-toggle="dropdown" href="" class="con"><span class="fa fa-user" aria-hidden="true"></span><span class="label label-primary">+5</span></a>
                            <ul role="menu" class="dropdown-menu pull-right dropdown-profile">
                                <li class="title"><span class="icon icon-user"></span>&nbsp;&nbsp;Welcome, John!</li>
                                <li><a href="#"><span class="fa fa-gears"></span>Settings</a></li>
                                <li><a href="#"><span class="fa fa-user" aria-hidden="true"></span>Profile</a></li>
                                <li><a href="#"><span class="fa fa-envelope"></span>Messages</a></li>
                                <li><a href="#"><span class="fa fa-power-off"></span>Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="hidden-xs">
                    <form method="post" action="index.html" class="searchform">
                        <input type="text" placeholder="Search here..." name="keyword" class="form-control">
                    </form>
                </li>
				<li>
					<div class="col-md-2 col-sm-3 col-xs-12">
						<div class="mini-cart-area">
							<ul>
								<?php
								$ci =& get_instance();
								?>
								<li>
									<?php
									if(!isUserLoggedIn()){
										?>
										<a href="javascript:void(0)">
											<i class="ion-android-person"></i>
										</a>
										<?php
									}
									else{

										$user_img='';//$this->db->get_where('users', array('UserId' => $this->session->userdata('GSM_FUS_UserId')))->row()->user_image;

										if($user_img=='' OR !file_exists('assets/images/users/'.$user_img)){
											$user_img=base_url('assets/images/photo.jpg');
										}
										else{
											$user_img=base_url('assets/images/users/'.$user_img);
										}


										?>
										<a href="javascript:void(0)" style="background-image: url('<?=$user_img?>');background-size: cover;">
										</a>
										<?php
									}
									?>
									<ul class="cart-dropdown user_login">
										<?php
										if(!check_user_login()){
											?>
											<li class="cart-button"> <a href="<?php echo site_url('login-register'); ?>" class="button2">Login &amp; Register</a>
											</li>
										<?php }else{ ?>
											<li class="cart-item"><a href="<?php echo site_url('my-account'); ?>"><i class="ion-android-person"></i> My Account</a></li>
											<li class="cart-item"><a href="<?php echo site_url('my-orders'); ?>"><i class="ion-bag"></i> My Orders</a></li>
											<li class="cart-item"><a href="<?php echo site_url('my-cart'); ?>"><i class="ion-ios-cart-outline"></i> Shopping Cart</a></li>
											<li class="cart-item"><a href="<?php echo site_url('wishlist'); ?>"><i class="ion-ios-list-outline"></i> Wishlist</a></li>
											<li class="cart-item"><a href="<?=site_url('site/logout')?>" onclick="return confirm('Are you sure to logout?')"><i class="ion-log-out"></i> Logout</a></li>
										<?php } ?>
									</ul>
								</li>
								<li style="<?php if(!check_user_login() OR empty(get_cart())){ echo 'pointer-events: none;';}?>">
									<a href="javascript:void(0)">
										<i class="ion-android-cart"></i>
										<span class="cart-add"><?=count(get_cart())?></span>
									</a>
									<ul class="cart-dropdown">
										<?php

										if(check_user_login()){

											$row=get_cart(3);
											if(!empty($row))
											{
												foreach ($row as $key => $value) {

													// for offers
													$data_ofr=$ci->calculate_offer($ci->get_single_info(array('id' => $value->product_id),'offer_id','tbl_product'),$value->product_mrp);

													$arr_ofr=json_decode($data_ofr);

													$img_file=$ci->_create_thumbnail('assets/images/products/',$ci->get_single_info(array('id' => $value->product_id),'product_slug','tbl_product'),$value->featured_image,50,50);


													?>
													<li class="cart-item">
														<div class="cart-img" style="width: auto"> <a href=""><img src="<?=base_url().$img_file?>" alt="" style="width: 68px;height: 68px"></a> </div>
														<div class="cart-content">
															<h4>
																<a href="<?php echo site_url('product/'.$ci->get_single_info(array('id' => $value->product_id),'product_slug','tbl_product')); ?>" title="<?=$ci->get_single_info(array('id' => $value->product_id),'product_title','tbl_product')?>">
																	<?php
																	if(strlen($value->product_title) > 20){
																		echo substr(stripslashes($value->product_title), 0, 20).'...';
																	}else{
																		echo $value->product_title;
																	}
																	?>
																</a>
															</h4>
															<p class="cart-quantity">Qty: <?=$value->product_qty?></p>
															<p class="cart-price"><?=CURRENCY_CODE.' '.$arr_ofr->selling_price*$value->product_qty?></p>
														</div>
														<div class="cart-close"> <a href="<?php echo site_url('remove-to-cart/'.$value->id); ?>" class="btn_remove_cart" title="Remove"><i class="ion-android-close"></i></a> </div>
													</li>

												<?php } ?>
												<?php
												if(count(get_cart()) > 3){
													echo '<li class="cart-item text-center">
                                      <h4 style="font-weight: 500">You have '.(count(get_cart())-3).' more items your cart..</h4>
                                    </li>';
												}
												?>

												<li class="cart-button"> <a href="<?php echo site_url('my-cart'); ?>" class="button2">View cart</a> <a href="<?php echo site_url('checkout'); ?>" class="button2">Check out</a>
												</li>
												<?php
											}
											else{
												?>
												<li class="cart-item text-center">
													<h4 style="font-weight: 500">Cart is empty !!!</h4>
												</li>
												<?php
											}
										}   // end of session check
										else{
											?>
											<li class="cart-item text-center">
												<h4 style="font-weight: 500">You have not login now !!!</h4>
											</li>
											<?php
										}
										?>

									</ul>
								</li>
							</ul>
						</div>
					</div>

				</li>
                <li id="last-one"> <a href="<?php echo base_url('dashboard/logout'); ?>">Log Out <i class="fa fa-angle-double-right"></i></a> </li>
            </ul>
            <!--/nav navbar-nav-->
        </div>
        <!--/header-seperation-->
    </div>
    <!--/navbar-inner-->
</div>
<!--/header-->
