<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <title><?php echo $this->config->item('app_name'); ?></title>

	<?php
	// for dynamic css files
	echo put_headers();
	?>
    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/custom.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/style-responsive.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/kalendar/kalendar.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/jquerysctipttop.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/plugins/gallery/magnific-popup.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/data-tables/DT_bootstrap.css'); ?>" rel="stylesheet" ">
    <link href="<?php echo base_url('assets/plugins/select2/css/select2.min.css');?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/bootstrap-colorpicker/css/colorpicker.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/material-design/materialdesignicons.scss')?>" type="text/css">
    <link href="<?php echo base_url('assets/css/jquery.fancybox.css?v=2.1.5')?>"  rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/colorpicker_custom.css')?>" rel="stylesheet" type="text/css" >
    <link href="<?php echo base_url('assets/css/chosen.css')?>" rel="stylesheet" type="text/css" >

	<!-- New files for products -->
	<link rel="stylesheet" href="<?=base_url('assets/site_assets/css/default.css')?>">
	<link rel="stylesheet" href="<?=base_url('assets/site_assets/css/owl_carousel.css')?>">
	<link rel="stylesheet" href="<?=base_url('assets/site_assets/css/style.css')?>">
	<link rel="stylesheet" href="<?=base_url('assets/site_assets/css/cust_style.css')?>">

	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/sweetalert/sweetalert.css')?>">
	<script type="text/javascript" src="<?=base_url('assets/sweetalert/sweetalert.min.js')?>"></script>


	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url('assets/js/jquery-2.0.2.min.js'); ?>"></script>
	<script src="https://kit.fontawesome.com/f0f4d5fea5.js" crossorigin="anonymous"></script>
    <?php switchlanguage();?>
    <script>
		var base_url = '<?php echo base_url(); ?>';
        $(document).ready(function(){
          $.ajaxSetup({
              beforeSend:function(jqXHR, Obj){
                  var value = "; " + document.cookie;
                  var parts = value.split("; csrf_cookie_name=");
                  if(parts.length == 2)   
                  Obj.data += '&csrf_test_name='+parts.pop().split(";").shift();
              },
              complete:function(){
                var value = "; " + document.cookie;
                  var parts = value.split("; csrf_cookie_name=");
                  if(parts.length == 2){
                      $('form [name="csrf_test_name"]').val(parts.pop().split(";").shift());
                  }
              }
          });
      });
    </script>
    <style>
        [name="rdIMEIType"] {
            margin: inherit !important;
            height: auto !important;
            opacity: 1 !important;
            position: static !important;
            width: auto !important;
        }
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
        }
        .fancybox-margin{
            margin-right:17px;
        }
    </style>

</head>

<body class="light-theme">
<div class="process_loader"></div>
    <?php $this->load->view('layouts/navbar'); ?>
    <?php $this->load->view('layouts/right-bar'); ?>
    <div class="page-container">
        <?php
        $data['userDetails'] = $userDetails;
        $data['settings'] = $settings;
       
$this->load->view('layouts/sidebar', $data); ?>
