<div class="nav-collapse top-margin fixed box-shadow2 hidden-xs" id="sidebar">
    <div class="leftside-navigation">
        <div class="sidebar-section sidebar-user clearfix">
            <div class="sidebar-user-avatar">
                <a href="#"> <img alt="avatar" src="<?php echo base_url('assets/images/avatar1.jpg'); ?>"> 
            </div>
            <div class="sidebar-user-name"><?php echo $this->session->userdata('UserFullName'); ?></div>
            <div class="sidebar-user-links">
                <a title="" data-placement="bottom" data-toggle="" href="profile.html" data-original-title="User">
                    <i class="fa fa-user" aria-hidden="true"></i>
                </a> 
                <a title="" data-placement="bottom" data-toggle="" href="inbox.html" data-original-title="Messages">
                    <i class="fa fa-envelope-o"></i>
                </a> 
                <a title="" data-placement="bottom" data-toggle="" href="lockscreen.html" data-original-title="Logout">
                    <i class="fa fa-sign-out"></i>
                </a> 
            </div>
        </div>
        <ul id="nav-accordion" class="sidebar-menu" style="border-radius:5px 0 5px 5px" >
            <li><a class="<?php echo $this->router->fetch_method() == 'add_credits' ? 'active' : '' ?>" href="<?php echo base_url('page/add_credits'); ?>"><i class="fa fa-plus-circle" aria-hidden="true"></i><?php echo $this->lang->line('CUST_LBL_232');?></a></li>
            <li><a  class="<?php echo $this->router->fetch_method() == 'myprofile' ? 'active' : '' ?>" href="<?php echo base_url('page/myprofile') ?>"><i class="fa fa-user" aria-hidden="true" aria-hidden="true"></i><?php echo $this->lang->line('CUST_LBL_223');?></a></li>
            <li><a href=""><i class="fab fa-google"></i>Google Authentication</a></li>
            <li><a href="<?php echo base_url('page/login_security');?>"><i class="fa fa-shield" aria-hidden="true"></i>Login Security</a></li>
            <li class="sub-menu dcjq-parent-li">
                <a href="javascript:;" class="dcjq-parent"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span>Order Now</span></a>
                <ul class="sub">
                    <li>
                        <a class="<?php echo $this->router->fetch_method() == 'imei_services' ? 'active' : '' ?>" href="<?php echo base_url('page/imei_services');?>">
                            <i class="fa fa-angle-right"></i>
                            <?php echo $this->lang->line('CUST_CH_146');?>
                        </a>
                        <a class="<?php echo $this->router->fetch_method() == 'file_services' ? 'active' : '' ?>" href="<?php echo base_url('page/file_services');?>">
                            <i class="fa fa-angle-right"></i>
                            <?php echo $this->lang->line('CUST_LBL_1');?>
                        </a>
                        <a class="<?php echo $this->router->fetch_method() == 'server_services' ? 'active' : '' ?>" href="<?php echo base_url('page/server_services');?>">
                            <i class="fa fa-angle-right"></i>
                            Server Services
                        </a>
                    </li>
                </ul>
            </li>






            <li class="sub-menu dcjq-parent-li">
                <a href="javascript:;" class="dcjq-parent"><i class="fas fa-history"></i><span>Orders History</span></a>
                <ul class="sub">
                    <li>
                        <a class="<?php echo $this->router->fetch_method() == 'imei_orders' ? 'active' : '' ?>" href="<?php echo base_url('page/imei_orders');?>">
                            <i class="fa fa-angle-right"></i>
                            <?php echo $this->lang->line('CUST_DB_ICON3');?>
                        </a>
                        <a class="<?php echo $this->router->fetch_method() == 'file_orders' ? 'active' : '' ?>" href="<?php echo base_url('page/file_orders');?>">
                            <i class="fa fa-angle-right"></i>
                            <?php echo $this->lang->line('CUST_LBL_8');?>
                        </a>
                        <a class="<?php echo $this->router->fetch_method() == 'server_orders' ? 'active' : '' ?>" href="<?php echo base_url('page/server_orders');?>">
                            <i class="fa fa-angle-right"></i>
                            <?php echo $this->lang->line('CUST_LBL_13');?>
                        </a>
                    </li>
                </ul>
            </li>
           
            <?php if($userDetails->TransferCredits == '1') { ?>
                <li><a class="<?php echo $this->router->fetch_method() == 'transfer_credits' ? 'active' : '' ?>"  href="<?php echo base_url('page/transfer_credits');?>"><i class="far fa-share-square"></i><?php echo $this->lang->line('CUST_LBL_313');?></a></li>
            <?php } ?>
            <li><a class="<?php echo $this->router->fetch_method() == 'changepassword' ? 'active' : '' ?>"  href="<?php echo base_url('page/changepassword');?>"><i class="fas fa-key"></i><?php echo $this->lang->line('CUST_MENU_PWD_1');?></a></li>
            <li>
                <a class="<?php echo $this->router->fetch_method() == 'threshold_amount' ? 'active' : '' ?>" href="<?php echo base_url('page/threshold_amount'); ?>"><i class="fas fa-dollar-sign"></i><?php echo $this->lang->line('CUST_LBL_324')  ; ?></a>
            </li>
            <li>
                <a class="<?php echo $this->router->fetch_method() == 'client_invoices' ? 'active' : '' ?>" href="<?php echo base_url('page/client_invoices');?>"><i class="fas fa-file-invoice"></i><?php echo $this->lang->line('CUST_LBL_217');?></a>
            </li>
            <li>
                <a class="<?php echo $this->router->fetch_method() == 'admin_invoices' ? 'active' : '' ?>" href="<?php echo base_url('page/admin_invoices');?>"><i class="fas fa-file-invoice-dollar"></i><?php echo $this->lang->line('CUST_LBL_188')  ; ?></a>
            </li>
            <li>
                <a class="<?php echo $this->router->fetch_method() == 'my_statements' ? 'active' : '' ?>" href="<?php echo base_url('page/my_statements');?>"><i class="fas fa-file-upload"></i><?php echo $this->lang->line('CUST_LBL_287')  ; ?></a>
            </li>
            <?php if($settings->TicketSystem == '1'){ ?>
                <li>
                    <a class="<?php echo $this->router->fetch_method() == 'submit_ticket' ? 'active' : '' ?>" href="<?php echo base_url('page/submit_ticket');?>"><i class="fas fa-envelope-open-text"></i><?php echo $this->lang->line('CUST_LBL_285'); ?></a>
                </li>
                <li>
                    <a class="<?php echo $this->router->fetch_method() == 'my_tickets' ? 'active' : '' ?>" href="<?php echo base_url('page/my_tickets');?>"><i class="fas fa-clipboard-list"></i><?php echo $this->lang->line('CUST_LBL_292');?></a>
                </li>
            <?php } ?>
            <li>
                <a class="<?php echo $this->router->fetch_method() == 'product_and_services' ? 'active' : '' ?>" href="<?php echo base_url('page/product_and_services');?>"><i class="fas fa-globe"></i><?php echo $this->lang->line('CUST_CH_65');?></a>
            </li>
            <li>
                <a class="<?php echo $this->router->fetch_method() == 'login_history' ? 'active' : '' ?>" href="<?php echo base_url('page/login_history');?>"><i class="fas fa-sign-in-alt"></i><?php echo $this->lang->line('CUST_LBL_289');?></a>
            </li>
            <li>
                <a class="<?php echo $this->router->fetch_method() == 'downloads' ? 'active' : '' ?>" href="<?php echo base_url('page/downloads');?>"><i class="fas fa-download"></i>Downloads</a>
            </li>
            <li class="last">
                <a class="<?php echo $this->router->fetch_method() == 'logout' ? 'active' : '' ?>" href="<?php echo base_url('login/logout');?>"><i class="fas fa-power-off"></i><?php echo $this->lang->line('CUST_LBL_38');?></a>
            </li>
        </ul>
    </div>
</div>
