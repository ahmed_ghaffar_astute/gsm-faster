<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('CUST_LBL_292'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>
							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
								   style="border-collapse: collapse; border-spacing: 0; width: 100%;">
								<thead>
								<tr class="bg-primary">
									<th>#ID</th>
									<th><?php echo $this->lang->line('CUST_CH_6'); ?></th>
									<th><?php echo $this->lang->line('CUST_LBL_295'); ?></th>
									<th><?php echo $this->lang->line('CUST_CH_5'); ?></th>
								</tr>
								</thead>
								<tbody>
								<? if ($rsTckts) {
									foreach ($rsTckts as $row) {
										$cls = 'info';
										if ($row->StatusId == '2')
											$cls = 'info';
										else if ($row->StatusId == '3')
											$cls = 'success';
										else if ($row->StatusId == '4')
											$cls = 'warning';
										else if ($row->StatusId == '5')
											$cls = 'danger';
										?>
										<tr>
											<td><?php echo $row->TicketNo; ?></td>
											<td nowrap="nowrap"><? echo $row->DtTm; ?></td>
											<td align="center">
												<a href="<?php echo base_url("ticket/view/"); ?><?php echo($row->TicketId); ?>"
												   class="btn btn-pink btn-sm waves-effect waves-light"><? echo stripslashes($row->Subject); ?></a>
											</td>
											<td align="center"><span
													class="badge badge-pill badge-<?php echo $cls; ?>"><? echo($row->TcktStatus); ?></span>
											</td>
										</tr>
										<?
									}
								} else
									echo "<tr><td colspan='4'>".$this->lang->line('CUST_LBL_10')."</td></tr>";
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

