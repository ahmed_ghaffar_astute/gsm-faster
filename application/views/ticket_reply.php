<h2>
	<i class="fa fa-dashboard"></i> <?php echo $this->lang->line('CUST_LBL_296') . ' [#' . $ticket_row->TicketNo . ']'; ?>
</h2>
<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4>
				<i class="fa fa-dashboard mr-2"></i> <?php echo $this->lang->line('CUST_LBL_296') . ' [#' . $ticket_row->TicketNo . ']'; ?>
			</h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<h4>
								<i class="fa fa-ticket"></i> Ticket ID: <?php echo $ticket_row->TicketNo; ?>
								<?php if ($statusId != '2') { ?>
									- <input type="submit" name="btnClose" class="btn btn-primary btn-sm" value="Close"
											 onclick="window.location.href = '<?php echo base_url(); ?>page/ticket_detail?id=<?php echo $tcktId ?>&close=1'"/>
								<?php } ?>
							</h4>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">

							<p class="card-description">
								<strong>Subject:</strong> <?php echo stripslashes($ticket_row->Subject); ?><br/><br/>
								<strong>Status:</strong> <?php echo $status; ?><br/><br/>
								<strong>Department:</strong> <?php echo stripslashes($ticket_row->DeptName); ?>
							</p>
						</div>
					</div>
					<?php if ($statusId != '2') { ?>
						<hr/>
						<div>
							<h4><i class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> Reply</h4>
							<?php echo form_open(base_url('page/ticketdtls'), 'class="cmxform" id="myFrm" name="myFrm" '); ?>
							<input type="hidden" name="tcktId" value="<?php echo($tcktId); ?>">
							<input type="hidden" id="cldFrm" name="cldFrm" value="0">
							<fieldset>
								<div class="form-group row">
									<div class="col-lg-3">
										<label for="cname">Message*</label>
									</div>
									<div class="col-lg-9">
									<textarea name="txtReply" rows="7" required="required"
											  class="form-control"></textarea>
									</div>
								</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-3"></div>
							<div class="col-lg-9">

								<input class="btn btn-primary" name="btnReply" type="submit"
									   onclick="document.getElementById('cldFrm').value = '1';" value="Submit">
							</div>
						</div>
						</fieldset>
						<?php echo form_close(); ?>
					<?php } ?>
					<?php if ($rsTckts) { ?>
						<hr/>
						<div>
							<section id="cd-timeline" class="cd-container">
								<?php foreach ($rsTckts as $row) {
									$name = $row->Name;
									if ($name == '')
										$name = $row->ClientName;
									$class = 'danger';
									if ($row->ByAdmin == '1') {
										$name = 'Admin';
										$class = 'primary';
									}
									$arrDt = explode(' ', $row->DtTm);
									?>
									<div class="form-group row">
										<div class="col-lg-12">

											<div class="cd-timeline-block">
												<div class="cd-timeline-img bg-<?php echo $class; ?>">
													<i class="mdi mdi-adjust"></i>
												</div>
												<div class="cd-timeline-content">
													<h3><?php echo convertDate($arrDt[0]); ?></h3>
													<p class="mb-0 text-muted"><?php echo stripslashes($row->Message); ?></p>
													<span class="cd-date">By <?php echo $name; ?></span>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
							</section>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
