<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('CUST_LBL_23'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">

							<table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
								   style="border-collapse: collapse; border-spacing: 0; width: 100%;">
								<thead>
								<tr class="bg-primary">
									<th><?php echo $this->lang->line('CUST_LBL_24'); ?></th>
									<th><?php echo $this->lang->line('CUST_LBL_27'); ?></th>
									<th><?php echo $this->lang->line('CUST_LBL_28'); ?></th>
								</tr>
								</thead>
								<tbody>
								<?php foreach ($rsLogRecrds as $row) { ?>
									<tr>
										<td><?php echo($row->IP); ?></td>
										<td><?php echo $row->LoginTime; ?></td>
										<td><?php echo $row->LogoutTime == '' ? '-' : $row->LogoutTime; ?></td>
									</tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
