<style>
	.register-bottom a{
		color: #fff !important;
	}
</style>
<div class="middle-login" style="top:30%;">
    <div class="block-web">
        <div class="head">
            <h3 class="text-center"><?php echo $this->config->item('app_name'); ?></h3>
        </div>
        <div style="background:#fff;">
            <?php echo form_open(base_url('register'), array('class' => 'form-horizontal', 'style' => 'margin-bottom: 0px !important;')); ?>
                <div class="content">
                    <h4 class="title">Registration is FREE*</h4>

                    <fieldset>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                    <input type="text" placeholder="Username*" value="<?php echo set_value('txtUName'); ?>" name="txtUName" class="form-control">
                                </div>
                                <?php echo form_error('txtUName'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input type="email" placeholder="Email Address*" value="<?php echo set_value('txtEmail'); ?>" name="txtEmail" class="form-control">
                                </div>
                                <?php echo form_error('txtEmail'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="password" placeholder="Password*" value="<?php echo set_value('txtPassword'); ?>" name="txtPassword" class="form-control">
                                </div>
                                <?php echo form_error('txtPassword'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="password" placeholder="Confirm Password*" value="<?php echo set_value('txtRePassword'); ?>" name="txtRePassword" class="form-control">
                                </div>
                                <?php echo form_error('txtRePassword'); ?>
                            </div>
                        </div>

                    </fieldset>
                    <hr>
                    <fieldset>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="input">
                                    <input class="inputWidth" type="text" placeholder="First name*" value="<?php echo set_value('txtFName'); ?>" name="txtFName">
                                    <?php echo form_error('txtFName'); ?>
                                </label>
                            </div>
                            <div class="col-sm-6">
                                <label class="input">
                                    <input class="inputWidth" type="text" placeholder="Last name*" value="<?php echo set_value('txtLName'); ?>" name="txtLName">
                                    <?php echo form_error('txtLName'); ?>
                                </label>
                            </div>
						</div>
						<div class="form-group">
                            <div class="col-sm-12">
                                <label class="input d-block">
                                    <input class="w-100" type="text" placeholder="Phone*" value="<?php echo set_value('txtPhone'); ?>" name="txtPhone">
                                    <?php echo form_error('txtPhone'); ?>
                                </label>
                            </div>
						</div>
						<div class="form-group">
                            <div class="col-sm-12">
                                <?php $countries = fetchCountries(); ?>
                                <label class="select w-100">
                                    <select name="countryId" class="w-100">
                                        <option disabled="" selected="" value="">Country*</option>
                                        <?php foreach($countries as $country) { ?>
                                            <option value="<?php echo $country->CountryId; ?>|<?php echo stripslashes($country->Country);?>" <?php echo set_value('countryId') == ($country->CountryId.'|'.stripslashes($country->Country)) ? 'selected' : ''; ?>><?php echo stripslashes($country->Country); ?></option>
                                        <?php } ?>
                                    </select>
                                    <i></i>
                                </label>
                                <?php echo form_error('countryId'); ?>
                            </div>
						</div>
						<div class="form-group">
                            <div class="col-sm-12">
                                <?php $currencies = fetchCurrencies(); ?>
                                <label class="select w-100">
                                    <select name="currencyId" class="w-100">
                                        <option disabled="" selected="" value="">Currency*</option>
                                        <?php foreach($currencies as $currency) { ?>
                                            <option value="<?php echo $currency->Id; ?>" <?php echo set_value('currencyId') == $currency->Id ? 'selected' : ''; ?>><?php echo $currency->Value; ?></option>
                                        <?php } ?>
                                    </select>
                                    <i></i>
                                </label>
                                <?php echo form_error('currencyId'); ?>
                            </div></div>
						<div class="form-group">
                            <div class="col-sm-12">
                                <?php $languages = fetchLanguages(); ?>
                                <label class="select w-100">
                                    <select name="language" class="w-100">
                                        <option disabled="" selected="" value="">Language*</option>
                                        <?php foreach($languages as $key => $language) { ?>
                                            <option value="<?php echo $key; ?>" <?php echo set_value('language') == $key ? 'selected' : ''; ?>><?php echo $language; ?></option>
                                        <?php } ?>
                                    </select>
                                    <i></i>
                                </label>
                                <?php echo form_error('language'); ?>
                            </div>
						</div>
                    </fieldset>
                </div>

                <div class="register-bottom">
                    <a href="<?php echo base_url('login'); ?>" class="btn btn-primary">Log in</a>
                    <button class="btn btn-primary" type="submit">Register</button>
                </div>
            <?php echo form_close(); ?>

        </div>
    </div>
    <?php $this->load->view('layouts/login_copyright'); ?>
</div>
