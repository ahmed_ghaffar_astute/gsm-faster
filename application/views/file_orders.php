<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('CUST_CODE_HEADING'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<?php echo form_open('page/file_orders', "name=frm id=frm class=form-horizontal"); ?>
			<?php include_once APPPATH . 'scripts/codessearchsection.php'; ?>
			<?php echo form_close(); ?>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-lg-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-12 form-group" align="right">
                            <a href="JavaScript:void(0);" class="btn btn-primary" onclick="exportOrders();"><?php echo $this->lang->line('CUST_LBL_33'); ?></a>&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12" align="right">
                            <table class="table table-striped">
                                <thead>
                                    <tr class="bg-primary">
                                        <th>#ID</th>
                                        <th nowrap="nowrap"><?php echo $this->lang->line('CUST_CH_3'); ?></th>
                                        <th><?php echo $this->lang->line('CUST_MENU_CODES_1'); ?></th>
                                        <th><?php echo $this->lang->line('CUST_CH_1'); ?></th>
                                        <th align="left"> <?php echo $this->lang->line('CUST_LBL_187'); ?></th>
                                        <th style="text-align:center;">Reply File</th>
                                        <th style="text-align:center;">Verify</th>
                                        <th style="text-align:center;">Cancel</th>
                                        <th>
                                            <input class="chkSelect" type="checkbox" id="chkSelect" name="chkSelect" onClick="selectAllChxBxs('chkSelect', 'chkCodes', <? echo $totalCodes; ?>);" value="true">
                                        </th>                                                    
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if($totalCodes != 0)
                                        {
                                            $i = 0;
                                            $currDtTm = setDtTmWRTYourCountry();
                                            foreach($rsCodes as $row)
                                            {
                                                $showCancel = '0';
                                                $showVerify = '0';
                                                if($row->ReplyDtTm != '')
                                                {
                                                    $diffInMinsV = minutesDiffInDates($row->ReplyDtTm, $currDtTm);
                                                    if($diffInMinsV <= $row->OrderVerifyMins)
                                                        $showVerify = '1';
                                                }
                                                if($row->RequestedAt != '')
                                                {
                                                    $diffInMinsC = minutesDiffInDates($row->RequestedAt, $currDtTm);
                                                    if($diffInMinsC <= $row->OrderCancelMins)
                                                        $showCancel = '1';
                                                }
                                                $orderStatus = stripslashes($row->CodeStatus);
                                                if($row->HiddenStatus == '1' && $row->CodeSentToOtherServer == '0' && $row->OrderAPIId > 0 && $row->CodeStatusId == '1')
                                                    $orderStatus = 'Processing';
                                        ?>
                                        <tr>
                                            <td>
                                                <a href="<?php echo base_url();?>page/fodetail?id=<?php echo $row->CodeId; ?>&cr=<?php echo $userDetails->CurrencySymbol; ?>" class="fancybox fancybox.ajax" title="Order Details"><i class="fa fa-plus"></i> <?php echo $row->CodeId; ?></a>
                                            </td>
                                            <?php
                                                switch($row->CodeStatusId)
                                                {
                                                    case "1":
                                                        $class = 'warning';
                                                        break;
                                                    case "2":
                                                        $class = 'success';
                                                        break;
                                                    case "3":
                                                        $class = 'danger';
                                                        break;
                                                    case "4":
                                                        $class = 'info';
                                                        break;
                                                }	
                                            ?>
                                            <td align="left"><span class="badge badge-pill badge-<?php echo $class;?>"><?php echo $orderStatus;?></span></td>
                                            <td style="white-space: normal;"><? echo stripslashes($row->PackageTitle); ?></td>
                                            <td><?php echo $row->IMEINo != '' ? stripslashes($row->IMEINo) : '<b>Serial #: </b>'.stripslashes(isset($row->SerialNo)  && $row->SerialNo ? $row->SerialNo : '');?></td>
                                            <td style="white-space: normal;" align="left" title="<?php echo str_replace('"', "'", stripslashes($row->Code));?>"><? echo $row->Code == '' ? '-' : substr(stripslashes($row->Code), 0, 100); ?><? echo strlen($row->Code) > 100 ? '<b>...</b>' : '';?></td>
                                            <td style="text-align:center;">
                                                <? 
                                                if($row->ReplyFile == '')
                                                    echo '-';
                                                else
                                                { ?>
                                                    <a href="<?php echo base_url();?>page/downloadsl3replyfile?id=<?php echo($row->CodeId);?>"><i class="fa fa-download"></i></a>
                                                <? } ?>
                                            </td>
                                            <td valign="middle">
                                                <?php
                                                    if($row->VerifyOrders == '1' && $showVerify == '1') 
                                                    {
                                                        if($row->CodeStatusId == 2 && $row->Verify==0) 
                                                        {
                                                            $till = addMinutesToDtTm($row->ReplyDtTm, $row->OrderVerifyMins);
                                                            $strData = daysHoursMinsDifferenceInDates($till, $currDTTM)
                                                ?>
                                                    <a href="<?php echo base_url();?>page/file_orders?verify=1&CodeId=<?php echo($row->CodeId);?>" onclick="return confirm('<? echo $this->lang->line('CUST_LBL_186'); ?>')"><i class="fa fa-refresh"></i></a>
                                                    <?php if($strData != '') echo '&nbsp;Expires in<br />'.$strData;?>
                                                <?php 
                                                    } 
                                                    else if($row->Verify==1) 
                                                    {
                                                        echo '<span class="badge badge-pill badge-warning">Under Verification</span>';
                                                    }
                                                    else { echo '-'; } 
                                                    } 
                                                    else 
                                                    {
                                                    echo '-';
                                                    }
                                                ?>
                                            </td>
                                            <td valign="middle" style="text-align:center">
                                                <?php
                                                    if($row->CancelOrders == '1' && $row->CodeStatusId == '1' && $showCancel == '1' && $orderStatus != 'Processing')
                                                    { 
                                                ?>
                                                    <a href="<?php echo base_url();?>file_orders?cancel=1&orderId=<?php echo($row->CodeId);?>" onclick="return confirm('<? echo $this->lang->line('CUST_LBL_230'); ?>')"><i class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>
                                                <?php
                                                    }
                                                else
                                                    echo '-';
                                                ?>
                                            </td>
                                            <td align="center">
                                                <input type="checkbox" class="chkSelect" id="chkCodes<? echo $i; ?>" name="chkCodes[]" value="<? echo $row->CodeId; ?>">
                                            </td>                                                          
                                        </tr>
                                        <?php
                                            $i++;
                                        }
                                    }
                                    else
                                        echo '<tr><td colspan="9">'.$this->lang->lien('CUST_LBL_10').'</td></tr>';
                                        ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if($totalCodes != 0){ ?>
                        <div class="form-group row">
                            <div class="col-lg-12" align="right">
                                <?php
                                $row = get_totalrecs_codes($strWhere);
                                $totalRows = $row->TotalRecs;
                                if($totalRows > $limit)
                                {
                                    if($totalRows > $limit)
                                        doPages_DropDown($page_name, $back, $start, $txtlqry, $totalRows, $limit, $eu, '', $thisp, $next, 'frm');
                                }
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo base_url('assets/js/functions.js');?>"></script>

<script>
    $('document').ready(function(){
        $(".select2").select2();

        $('#datepicker-autoclose').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });
        $('#datepicker1-autoclose').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });
        
        $('.fancybox').fancybox();

        $("#btnSearch").click(function(){
            $("#dvSearch").show();
        });
        $("#btnClose").click(function(){
            $("#dvSearch").hide();
        });

    });
        
        function callme(id)
        {
            $("div#"+id).toggle();
            /*$( "div.ajax_area" ).html(' ');
            $( "div.ajax_area" ).removeClass( "in" );
            $("div.ajax_area").removeAttr("aria-hidden");*/
            
        }
        function submitMe(timeoutPeriod) {
            setTimeout("document.getElementById('frm').submit();", timeoutPeriod);
        }
        function exportOrders()
        {
            var totalCodes = <?php echo $totalCodes; ?>;
            var strIds = '0';
            for(var i=0; i<totalCodes; i++)
            {
                if(document.getElementById('chkCodes'+i).checked)
                {
                    arr = document.getElementById('chkCodes'+i).value.split('|');
                    strIds += ',' + arr[0];
                }
            }
            if(strIds == '0')
            {
                alert("Please select a record!");
                return false;
            }
            else
                window.location.href = '<?php echo base_url();?>page/exportimeiorders?ids='+strIds;
        }
        function downloadIMEIs()
        {
            var strIds = validate();
            if(strIds != false)
                window.location.href = '<?php echo base_url();?>page/downloadIMEIs?ids='+strIds;
               
        }
        function sortBy(sortBy, orderByType)
        {
            if(orderByType == 'ASC')
                document.getElementById('orderByType').value = 'DESC';
            else
                document.getElementById('orderByType').value = 'ASC';
            document.getElementById('sortBy').value = sortBy;
            document.getElementById('frm').submit();
        }
       

    
</script>
