<div class="table-responsive big-table" style="width:750px;">
    <table class="table">
    	<tr><td colspan="3"><div style="font-size:30px;color:#2B85D8;">Credits: <?php echo $credits.' '.$curr; ?></div></td></tr>
        <tr>
            <td width="20%">Order #</td>
            <td width="1%">:</td>
            <td width="79%"><?php echo $id; ?></td>
        </tr>
        <tr>
            <td>IMEI #</td>
            <td>:</td>
            <td><?php echo $imeiNo; ?></td>
        </tr>
		<?
        foreach($rsFields as $rowFld)
        {
            $fldColName = $rowFld->FieldColName;
            if(isset($row->$fldColName) && $row->$fldColName != '')
            {
                echo '
					<tr>
						<td>'.$rowFld->FieldLabel.'</td>
						<td>:</td>
						<td>'.$row->$fldColName.'</td>
	                <tr>';
            }
        }
		?>
        <tr>
            <td>Code</td>
            <td>:</td>
            <td class="statusA"><?php echo $code;?></td>
        </tr>
        <tr>
            <td>Submited At</td>
            <td>:</td>
            <td><?php echo $oDate; ?></td>
        </tr>
        <tr>
            <td>Replied At</td>
            <td>:</td>
            <td><?php echo $replyDtTm; ?></td>
        </tr>
        <tr>
            <td>Comments</td>
            <td>:</td>
            <td><?php echo $comments == '' ? '-' : $comments; ?></td>
        </tr>
        <tr>
            <td>Notes</td>
            <td>:</td>
            <td><?php echo $notes == '' ? '-' : $notes; ?></td>
        </tr>
    </table>
</div>