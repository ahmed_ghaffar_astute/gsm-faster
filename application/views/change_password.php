<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('CUST_PWD_HEADING'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if ((isset($message) && $message != '') || (isset($errorMsg) && $errorMsg != '')) { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<?php
								if ($message != '')
									echo '<div class="alert alert-success" role="alert">' . $message . '</div>';
								if ($errorMsg != '')
									echo '<div class="alert alert-danger" role="alert">' . $errorMsg . '</div>';
								?>
							</div>
						</div>
					<?php } ?>
					<?php echo form_open(base_url('page/changepassword')); ?>
					<div class="form-group row">
						<label for="example-text-input"
							   class="col-sm-2 col-form-label"><?php echo $this->lang->line('CUST_PWD_2'); ?>*</label>
						<div class="col-sm-10">
							<input class="form-control" type="password" autocomplete="off" maxlength="15"
								   name="txtOldPass" id="txtOldPass" required/>
						</div>
					</div>
					<div class="form-group row">
						<label for="example-text-input"
							   class="col-sm-2 col-form-label"><?php echo $this->lang->line('CUST_PWD_3'); ?>*</label>
						<div class="col-sm-10">
							<input class="form-control" type="password" autocomplete="off" maxlength="15"
								   name="txtNewPass" id="txtNewPass" required/>
						</div>
					</div>
					<div class="form-group row">
						<label for="example-text-input"
							   class="col-sm-2 col-form-label"><?php echo $this->lang->line('CUST_PWD_4'); ?>*</label>
						<div class="col-sm-10">
							<input class="form-control" autocomplete="off" type="password" maxlength="15"
								   name="txtConfirmPass" id="txtConfirmPass" required/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">&nbsp;</label>
						<div class="col-sm-10">
							<button type="submit" class="btn btn-primary waves-effect waves-light">
								Submit
							</button>
							<a href="<?php echo base_url('dashboard'); ?>" class="btn btn-secondary waves-effect m-l-5">Cancel</a>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
