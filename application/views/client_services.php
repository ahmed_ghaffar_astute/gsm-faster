<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $heading; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
								   style="border-collapse: collapse; border-spacing: 0; width: 100%;">
								<thead>
								<tr class="bg-primary">
									<th><?php echo $this->lang->line('CUST_CH_67'); ?></th>
									<th><?php echo $this->lang->line('CUST_CODE_2'); ?></th>
									<th><?php echo $this->lang->line('CUST_CH_68'); ?></th>
									<th style="width: 15%"><?php echo $this->lang->line('CUST_LBL_300'); ?></th>
									<th></th>
								</tr>
								</thead>
								<tbody>
								<?php foreach ($products as $row) {
									$myPrice = 0;
									if (isset($PACK_PRICES_USER[$row->PackageId]))
										$myPrice = $PACK_PRICES_USER[$row->PackageId];
									else if (isset($PACK_PRICES_PLAN[$row->PackageId]))
										$myPrice = $PACK_PRICES_PLAN[$row->PackageId];
									else {
										if (isset($PACK_PRICES_BASE[$row->PackageId]))
											$myPrice = $PACK_PRICES_BASE[$row->PackageId];
									}
									if ($row->DisablePackage == 0) {
										$class = 'success';
										$lbl = 'Active';
									} else {
										$class = 'danger';
										$lbl = 'Inactive';
									}
									?>
									<tr>
										<td><?php echo stripslashes($row->Category); ?></td>
										<td><?php echo stripslashes($row->PackageTitle); ?></td>
										<td><?php echo stripslashes($row->DeliveryTime); ?></td>
										<td><?php echo $userDetails->CurrencySymbol . roundMe($myPrice); ?></td>
										<td align="center"><span
												class="badge badge-<?php echo $class; ?>"><?php echo $lbl; ?></span>
										</td>
									</tr>
									<?php
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
					<!--/col-md-3-->
					<!--/col-md-3-->
				</div>
				<!--/row-->
			</div>
		</div>
	</div>
</div>
