<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('CUST_LBL_324'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->

					<div style="display:none;" class="alert alert-success" id="dvError"></div>
					<div style="display:none;" class="alert alert-success" id="dvMsg"></div>
						
					<?php echo form_open(); ?>
						<div class="form-group row">
							<div class="col-lg-6">
								<label class="control-label"><? echo $this->lang->line('CUST_LBL_324'); ?><span class="required_field">*</span></label>
								<input name="txtAmount" id="txtAmount" type="text" class="form-control" value='<?php echo $amount_row->ThresholdAmount == '0' ? '' : $amount_row->ThresholdAmount; ?>'/>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="form-group">
									<button type="button" name="btnTSubmit" id="btnTSubmit" class="btn btn-primary waves-effect waves-light"> 
										<?php echo $this->lang->line('CUST_LBL_41'); ?>
                                    </button>
                                    <img src="<?php echo base_url('assets/images/loading.gif');?>" id="imgLdr" style="display:none; padding-top:13px;" border="0" alt="Please wait..." />	
								</div>
							</div>
						</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url('assets/js/functions.js') ;?>"></script>
<script src="<?php echo base_url('assets/js/threshold.js') ;?>"></script>
