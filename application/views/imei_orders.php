<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('CUST_CODE_HEADING'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<?php echo form_open('page/submitpayment', "name=frm id=frm class=form-horizontal"); ?>
			<?php include_once APPPATH . 'scripts/codessearchsection.php'; ?>
			<?php echo form_close(); ?>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-lg-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>

					<div class="form-group row">
						<div class="col-lg-12 form-group" align="right">
							<input type="button" value="<?php echo $this->lang->line('CUST_LBL_316'); ?>"
								   class="btn btn-primary" onClick="exportAllOrders();"/>&nbsp;&nbsp;
							<input type="button" value="<?php echo $this->lang->line('CUST_LBL_33'); ?>"
								   class="btn btn-info" onClick="exportOrders();"/>&nbsp;&nbsp;
							<input type="button" value="<?php echo $this->lang->line('CUST_LBL_220'); ?>"
								   class="btn btn-primary" onClick="downloadIMEIs();"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<table class="table table-striped table-responsive">
								<thead>
								<tr class="bg-primary">
									<th>#ID</th>
									<th nowrap="nowrap"><?php echo $this->lang->line('CUST_CH_3'); ?></th>
									<th><?php echo $this->lang->line('CUST_MENU_CODES_1'); ?></th>
									<th><?php echo $this->lang->line('CUST_CH_1'); ?></th>
									<th><?php echo $this->lang->line('CUST_MENU_CREDITS'); ?></th>
									<th align="left" width="25%"> <?php echo $this->lang->line('CUST_LBL_187'); ?></th>
									<th style="text-align:center;">Verify</th>
									<th style="text-align:center;">Cancel</th>
									<th>
										<input type="checkbox" class="chkSelect" id="chkSelect" name="chkSelect"
											   onClick="selectAllChxBxs('chkSelect', 'chkCodes', <?php echo $totalCodes; ?>);"
											   value="true">
									</th>
								</tr>
								</thead>
								<tbody>
								<?php if ($rsCodes) {
									$i = 0;
									$currDtTm = setDtTmWRTYourCountry();
									foreach ($rsCodes as $row) {
										$showVerify = '0';
										$showCancel = '0';
										$showVerify = '0';
										$showCancel = '0';
										$pckTitle = '';
										$cancelOrder = 0;
										$verifyOrder = 0;
										$orderVerifyMins = 0;
										$orderCancelMins = 0;
										$orderStatus = '';
										if (isset($PACKS[$row->PackageId]) && $PACKS[$row->PackageId] != '') {
											$arrPackDtls = explode('-->', $PACKS[$row->PackageId]);
											$pckTitle = fixTextInTD($arrPackDtls[0], 3, 15);
											$cancelOrder = $arrPackDtls[1];
											$verifyOrder = $arrPackDtls[2];
											$orderVerifyMins = $arrPackDtls[3];
											$orderCancelMins = $arrPackDtls[4];
										}
										if (isset($STATUSES[$row->CodeStatusId]) && $STATUSES[$row->CodeStatusId] != '') {
											$orderStatus = $STATUSES[$row->CodeStatusId];
										}
										if ($row->ReplyDtTm != '') {
											$diffInMinsV = minutesDiffInDates($row->ReplyDtTm, $currDtTm);
											if ($diffInMinsV <= $orderVerifyMins)
												$showVerify = '1';
										}
										if ($row->RequestedAt != '') {
											$diffInMinsC = minutesDiffInDates($row->RequestedAt, $currDtTm);
											if ($diffInMinsC <= $orderCancelMins)
												$showCancel = '1';
										}
										$orderCode = html_entity_decode(stripslashes($row->Code));
										switch ($row->CodeStatusId) {
											case "1":
												$class = 'warning';
												break;
											case "2":
												$class = 'success';
												break;
											case "3":
												$class = 'danger';
												break;
											case "4":
												$class = 'info';
												break;
										}
										?>
										<tr>
											<td>
												<a class="fancybox fancybox.ajax"
												   href="<?php echo base_url(); ?>page/iodetail?id=<?php echo $row->CodeId; ?>"
												   title="Order Details"><i
														class="fa fa-plus"></i> <?php echo $row->CodeId; ?></a>
											</td>
											<?
											$strData = '';
											if ($row->HiddenStatus == '1' && $row->CodeSentToOtherServer == '0' && $row->OrderAPIId > 0 && $row->CodeStatusId == '1')
												$orderStatus = 'Processing';
											?>
											<td align="left" nowrap><span
													class="badge badge-pill badge-<?php echo $class; ?>"><?php echo $orderStatus; ?></span>
											</td>
											<td style="white-space: normal;"><?php echo trim($pckTitle); ?></td>
											<td style="white-space: normal;"><?php echo $row->IMEINo != '' ? stripslashes($row->IMEINo) : '<b>Serial #: </b>' . stripslashes($row->SerialNo); ?></td>
											<td align="left"><?php echo $row->Credits; ?></td>
											<td style="white-space: normal;"
												align="left"><?php echo $orderCode == '' ? '-' : substr($orderCode, 0, 50); ?></td>
											<td valign="middle">
												<?php if ($verifyOrder == '1' && $showVerify == '1') {
													if ($row->CodeStatusId == 2 && $row->Verify == 0) {
														$till = addMinutesToDtTm($row->ReplyDtTm, $orderVerifyMins);
														$strData = daysHoursMinsDifferenceInDates($till, $currDtTm);
														?>
														<a class="fancybox fancybox.ajax"
														   href="verifyorder.php?id=<?php echo($row->CodeId); ?>"><i
																class="fa fa-refresh"></i></a>
														<?php if ($strData != '') echo '&nbsp;Expires in<br />' . $strData; ?>
														<?php
													} else if ($row->Verify == 1) {
														echo '<span class="badge badge-pill badge-warning">Under Verification</span>';
													} else {
														echo '-';
													}
												} else {
													echo '-';
												}
												?>
											</td>
											<td valign="middle" style="text-align:center">
												<?php
												if ($cancelOrder == '1' && $row->CodeStatusId == '1' && $showCancel == '1' && $orderStatus != 'Processing') { ?>
													<a href="imeiorders.php?cancel=1&orderId=<?php echo($row->CodeId); ?>"
													   onclick="return confirm('<?php echo $this->lang->line('CUST_LBL_230'); ?>')"><i
															class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>
													<?php
												} else
													echo '-';
												?>
											</td>
											<td align="center">
												<input type="checkbox" class="chkSelect" id="chkCodes<?php echo $i; ?>"
													   name="chkCodes[]" value="<?php echo $row->CodeId; ?>">
											</td>
										</tr>
										<?php $i++;
									}
								} else {
									echo '<tr><td colspan="8">' . $this->lang->line('CUST_LBL_10') . '</td></tr>';
								} ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	<script src="<?php echo base_url('assets/js/functions.js'); ?>"></script>


	<script>
		$('document').ready(function () {
			$(".select2").select2();

			$('#datepicker-autoclose').datepicker({
				format: "yyyy-mm-dd",
				autoclose: true,
				todayHighlight: true
			});
			$('#datepicker1-autoclose').datepicker({
				format: "yyyy-mm-dd",
				autoclose: true,
				todayHighlight: true
			});

			$('.fancybox').fancybox();


		});


		function submitMe(timeoutPeriod) {
			setTimeout("document.getElementById('frm').submit();", timeoutPeriod);
		}

		function validate() {
			var totalCodes = <?php echo $totalCodes; ?>;
			var strIds = '0';
			for (var i = 0; i < totalCodes; i++) {
				if (document.getElementById('chkCodes' + i).checked) {
					arr = document.getElementById('chkCodes' + i).value.split('|');
					strIds += ',' + arr[0];
				}
			}
			if (strIds == '0') {
				alert("Please select a record!");
				return false;
			}
			return strIds;
		}

		function exportOrders() {
			var strIds = validate();
			if (strIds != false)
				window.location.href = '<?php echo base_url();?>page/exportimeiorders?ids=' + strIds;
		}

		function exportAllOrders() {
			window.location.href = '<?php echo base_url();?>page/exportimeiorders?cFrm=1';
		}

		function downloadIMEIs() {
			var strIds = validate();
			if (strIds != false)
				window.location.href = '<?php echo base_url();?>page/downloadIMEIs?ids=' + strIds;
		}

		function sortBy(sortBy, orderByType) {
			if (orderByType == 'ASC')
				document.getElementById('orderByType').value = 'DESC';
			else
				document.getElementById('orderByType').value = 'ASC';
			document.getElementById('sortBy').value = sortBy;
			document.getElementById('frm').submit();
		}

		//submitMe(120000);// 309 seconds. = 300 * 1000 where 300 are the number of seconds. Total 5 minutes
		$("#btnSearch1").click(function () {
			$("#frm").show();
		});
		$("#btnClose1").click(function () {
			$("#frm").hide();
		});
		$("#btnSearch").click(function () {
			$("#dvSearch").show();
		});
		$("#btnClose").click(function () {
			$("#dvSearch").hide();
		});
	</script>
