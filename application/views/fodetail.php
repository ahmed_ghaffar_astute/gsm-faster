<div class="table-responsive big-table" style="width:750px;">
    <table class="table">
    	<tr><td colspan="3"><div style="font-size:30px;color:#2B85D8;">Credits: <? echo $credits.' '.$curr; ?></div></td></tr>
        <tr>
            <td width="17%">Order #</td>
            <td width="1%">:</td>
            <td width="82%"><? echo $id; ?></td>
        </tr>
        <tr>
            <td>IMEI</td>
            <td>:</td>
            <td><? echo $imeiNo; ?></td>
        </tr>
        <tr>
            <td>Code</td>
            <td>:</td>
            <td class="statusA"><? echo $code;?></td>
        </tr>
        <tr>
            <td>Submited On</td>
            <td>:</td>
            <td><? echo $oDate; ?></td>
        </tr>
        <tr>
            <td>Replied on</td>
            <td>:</td>
            <td><? echo $replyDtTm; ?></td>
        </tr>
        <tr>
            <td>Comments</td>
            <td>:</td>
            <td><? echo $comments == '' ? '-' : $comments; ?></td>
        </tr>
        <tr>
            <td>Notes</td>
            <td>:</td>
            <td><? echo $notes == '' ? '-' : $notes; ?></td>
        </tr>
    </table>
</div>