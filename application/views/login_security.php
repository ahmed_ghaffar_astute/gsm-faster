<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('BE_LBL_869'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-lg-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message;?></div>
							</div>
						</div>
					<?php } ?>
					<?php if (isset($errorMsg) && $errorMsg != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-warning"><?php echo $errorMsg; ?></div>
					<?php } ?>

					<?php echo form_open(base_url('page/login_security')); ?>
						<div class="form-group row">
							<label class="col-lg-3 control-label">Country</label>
							<div class="col-lg-9">
								<?php $data = get_all_countries(); ?>
								<select id="countryId" name="countryId" class="chosen select2me" style="width:470px;">
									<option value="0" selected="selected">Select Country...</option>
									<?php FillCombo('', $data); ?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-3"></div>
							<div class="col-lg-4">
								<input type="submit" value="<?php echo $this->lang->line('CUST_LBL_41'); ?>"  class="btn btn-primary">
							</div>
						</div>
					<?php echo form_close() ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<table class="table table-bordered table-striped">
								<thead>
								<tr class="bg-primary">
									<th width="80%">Country</th>
									<th width="20%">Delete</th>
								</tr>
								</thead>
								<?php if ($rsCntrs) {
									foreach ($rsCntrs as $row){
								?>
								<tbody>
									<tr>
										<td><?php echo stripslashes($row->Country); ?></td>
										<td>
											<a onclick="return confirm('Are you sure you want to remove this country?')" href="<?php echo base_url('page/login_security?id=' . $row->ISO); ?>"  data-original-title="Delete this country" data-toggle="tooltip" class="icon-bin"></a>
										</td>
									</tr>
								<?php
								}
								} else {
									?>
									<tr>
										<td colspan='2'><?php $this->lang->line('CUST_LBL_10') ?></td>
									</tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function () {
		$(".chosen").data("placeholder", "Select Service...").chosen();
	});
</script>
