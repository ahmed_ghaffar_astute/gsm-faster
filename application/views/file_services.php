<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('CUST_LBL_181'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-lg-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
                    <div class="form-group row">
						<div class="col-lg-12">
                            <?php if($success){
                                echo '<div class="alert alert-success">'.$this->lang->line('CUST_LBL_181').'</div>'; 
                            } ?>
                            <?php if($dupMsg != '')
                                echo '<div class="alert alert-danger">IMEI(s) '.$dupMsg.' are already in the system, so can not be placed again!</div>';
                            if($message != '')
                                echo '<div class="alert alert-success">'.$message.'</div>';
                            if($message == '' && $msg != '' && $_POST['isError'] == '1')
                                echo '<div class="alert alert-danger">'.$msg.'</div>';
                            ?>
                        </div>
                    </div>
                    <?php echo form_open_multipart(base_url('page/file_services') , "name=frm id=frm");?>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label><?php echo $this->lang->line('CUST_LBL_1'); ?>:</label>
                                        <select class="form-control select2" id="packageId" name="packageId" required style="width: 100%; color: inherit;">
                                        <?php
                                            $rsPackages = get_rsPackages($strPackIds);
                                            $prevCatId = 0;
                                            foreach($rsPackages as $row)
                                            {
                                                $changeBGColor = '';
                                                $myPackPrice = 1;
                                                if(isset($PACK_PRICES_USER[$row->PackageId]))
                                                    $myPackPrice = $PACK_PRICES_USER[$row->PackageId];
                                                else if(isset($PACK_PRICES_PLAN[$row->PackageId]))
                                                    $myPackPrice = $PACK_PRICES_PLAN[$row->PackageId];
                                                else
                                                {
                                                    if (isset($PACK_PRICES_BASE[$row->PackageId]))
                                                    $myPackPrice = $PACK_PRICES_BASE[$row->PackageId];
                                                }

                                                    if($myPackPrice < $PACK_PRICES_BASE[$row->PackageId])
                                                    $changeBGColor = 'class="discount_text"';
                                                if($prevCatId > 0)
                                                {
                                                    if($row->CategoryId != $prevCatId)
                                                    echo '</optgroup><optgroup label="'.$row->Category.'"> ';
                                                }
                                                else
                                                    echo '<option value="" selected>Select...</option><optgroup label="'.$row->Category.'">';
                                            ?>
                                                <option details2='<?php echo $myPackPrice;?>' value="<?php echo $row->PackageId?>" <?php if($myNetworkId == $row->PackageId) echo 'selected';?> <?php echo $changeBGColor; ?>>
                                                <?php echo stripslashes($row->PackageTitle)." - ".$myPackPrice." Credits"; ?>
                                                </option>
                                            <?php
                                                    $prevCatId = $row->CategoryId;
                                                }
                                                echo '</optgroup>';
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-12 dot-opacity-loader" align="center">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                    <?php if($settings->IMEIOrderDropDownType == '0' || $settings->IMEI_ORDER_DD_TYPE == '1') { ?>
                                    <br />
                                    <div class="col-lg-12" id="tblInfo" style="display:none;">
                                        <div class="card m-b-30 card-info">
                                            <div class="card-body" style="background-color:#F5F5F5;">
                                                <blockquote class="card-bodyquote">
                                                    <p id="dvSN" style="font-weight:bold; font-size:20px;">&nbsp;</p>
                                                    <footer class="blockquote-footer" style="color:#E81516; font-size:20px; font-weight:bold;" id="lblCustPrice"></footer>
                                                    <p id="dvSrvImg" style="display:none;"><img id="imgSrv" src="" /></p>
                                                    <p style="color:green;"><span id="dvTT" style="color:#000000;"></span></p>
                                                    <p id="dvFeatures"></p>
                                                    <div id="dvMR">
                                                        <?php 
                                                            if($mustRead){
                                                                echo $mustRead;
                                                            }; 
                                                        ?>
                                                    </div>
                                                </blockquote>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <div class="edit-cont mb-3">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('CUST_LBL_159'); ?>:<span class="required_field"><span class="required_field">*</span></span></label>
                                            <div class="input-group">
                                                <input required="required" type="file" id="txtFile" name="txtFile">
                                                <small><?php echo $this->lang->line('CUST_LBL_171');?> .log .fnx .bcl .txt .sha .ask</small>
                                            </div>
                                        </div>	<!-- end col -->
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('CUST_CODE_10'); ?>:</label>
                                            <textarea rows="1" class="form-control" id="txtNotes" name="txtNotes" placeholder="Notes"><?php echo $notes; ?></textarea>
                                        </div>	<!-- end col -->
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('CUST_CODE_11'); ?>:</label>
                                            <textarea rows="1" class="form-control" id="txtComments" name="txtComments" placeholder="Comments"><?php echo $comments; ?></textarea>
                                        </div>	<!-- end col -->
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('CUST_CH_98'); ?>:</label>
                                            <input type="text" class="form-control" placeholder="Response E-mail:" name="txtAltEmail" maxlength="100" id="txtAltEmail" value="<?php echo($altEmail);?>">
                                        </div>	<!-- end col -->
                                        <?php if($userDetails->PinCode != '') { ?>
                                            <div class="form-group">
                                                <label><?php echo $this->lang->line('CUST_LBL_178'); ?>:<span class="required_field"><span class="required_field">*</span></span></label>
                                                <input type="text" autocomplete="off" class="form-control" placeholder="Enter your Pin Code" name="txtPinCode" maxlength="4" id="txtPinCode" value="" />
                                            </div>	<!-- end col -->
                                        <?php } ?>
                                        <div class="form-group">
                                            <button type="submit" class="clsBigBtn btn btn-primary" onClick="setValue('2');"><?php echo $this->lang->line('CUST_MENU_CODES'); ?></button>
                                            <input type="hidden" id="cldFrm" name="cldFrm" />
                                            <input type="hidden" id="ddType" name="ddType" value="<?php echo $settings->FileOrderDropDownType; ?>" />
                                            <input type="hidden" id="currAbb" name="currAbb" value="<?php echo $userDetails->CurrencyAbb; ?>" />
                                            <input type="hidden" id="currSymbol" name="currSymbol" value="<?php echo $userDetails->CurrencySymbol; ?>" />
                                        </div>
                                        <div class="form-group">
                                            <div class="alert alert-danger clsError" id="dvError" style="display:none;">&nbsp;</div>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    
                        
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script> var base_url = '<?php echo base_url();?>' </script>
<script language="javascript" src="<?php echo base_url('assets/js/functions.js');?>"></script>
<script language="javascript" src="<?php echo base_url('assets/js/fileorder.js?v=1');?>"></script>
