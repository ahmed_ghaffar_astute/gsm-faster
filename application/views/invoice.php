<?php
$keys = crypt_key($UserId);
	
$invCredits = decrypt($Credits , $keys);
if($invCredits == '')
    $invCredits = '0';

    $invAmount = decrypt($Amount , $keys);
if($invAmount == '')
    $invAmount = '0'; 
?>
<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('CUST_LBL_233');?>#<?php echo $id; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->

<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-lg-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="invoice-title">
                                <h4 class="pull-right font-16"><strong><?php echo $this->lang->line('CUST_LBL_233'); ?> #<?php echo $id; ?></strong></h4>
                                <h3 class="mt-0">
                                    <img src="<?php echo base_url('assets/images/logos/2.png'); ?>" alt="logo" />
                                </h3>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <address>
                                        <b><?php echo stripslashes($ADMIN_COMPANY); ?></b><br>
                                        <?php echo $ADMIN_ADD; ?><br>
                                        <?php echo stripslashes($ADMIN_EMAIL); ?>
                                    </address>
                                </div>
                                <div class="col-6 text-right">
                                    <address>
                                        <strong>Invoice To:</strong><br>
                                        <?
                                        $arrPayTo = explode("\n", $INV_PAY_TO);
                                        foreach ($arrPayTo as $value)
                                        {
                                            if($value != '')
                                                echo "$value<br />";
                                        } 
                                        ?>
                                    </address>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6 m-t-30">
                                    <address>
                                        <strong>Payment Method:</strong><br>
                                        <?php echo $pMethod; ?>
                                    </address>
                                </div>
                                <div class="col-lg-6 m-t-30 text-right">
                                    <address>
                                        <strong>Invoice Date:</strong><br>
                                        <?php echo convertDate($invDate); ?><br />
                                        <?php if($paidDtTm != '') { ?>
                                            <?php echo $this->lang->line('CUST_CH_125'); ?>: <?php echo convertDate($paidDtTm); ?><br /><br />
                                        <?php } ?>
                                    </address>
                                </div>
                            </div>
                            <?php if($pMethodTypeId == '2' && $pStatusId == '1') { ?>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <?php echo form_open(base_url('page/invoice')) ?>
                                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                            <input type='text' class="form-control" style="width:250px;" required="required" name='txtTransID' id='txtTransID' placeholder="Enter Transaction ID" />
                                            <?php if($rsPPRcvrs) { ?>
                                                <select name="paypalId" id="paypalId" class="form-control mt-2" required="required" style="width:250px;">
                                                    <option value="">Please Select PayPal ID</option>
                                                    <?php foreach($rsPPRcvrs as $row){ ?>
                                                        <option value="<?php echo $row->ReceiverId;?>"><?php echo stripslashes($row->Username); ?></option>
                                                    <?php } ?>
                                                </select>
                                            <?php } ?>
                                            <input type="submit" value="<?php echo $this->lang->line('CUST_LBL_41'); ?>"  name="dlfile" class="btn btn-primary btn-sm clsButton mt-2"  />
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="p-2">
                                    <h3 class="panel-title font-20"><strong>Order summary</strong></h3>
                                </div>
                                <div class="">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <td class="text-center"><strong><?php echo $this->lang->line('CUST_CRP_1'); ?></strong></td>
                                                <td class="text-center"><strong><?php echo $this->lang->line('CUST_LBL_247'); ?></strong></td>
                                                <td class="text-center"><strong><?php echo $this->lang->line('CUST_LBL_248'); ?></strong></td>
                                                <td class="text-right"><strong><?php echo $this->lang->line('CUST_LBL_249'); ?></strong></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="text-left"><?php echo $byAdmin == '1' ? $this->lang->line('CUST_LBL_241') : $this->lang->line('CUST_LBL_242'); ?></td>
                                                <td class="text-center">1</td>
                                                <td class="text-center"><?php echo $currency.' '.roundMe($invCredits); ?></td>
                                                <td class="text-right"><?php echo $currency.' '.roundMe($invCredits); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="thick-line"></td>
                                                <td class="thick-line"></td>
                                                <td class="thick-line text-center">
                                                    <strong><?php echo $this->lang->line('CUST_LBL_250'); ?></strong></td>
                                                <td class="thick-line text-right"><?php echo $currency.' '.roundMe($invCredits); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="no-line"></td>
                                                <td class="no-line"></td>
                                                <td class="no-line text-center">
                                                    <strong><?php echo $this->lang->line('CUST_BC_5'); ?></strong></td>
                                                <td class="no-line text-right"><?php echo $currency.' '; echo $invAmount - $invCredits; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="no-line"></td>
                                                <td class="no-line"></td>
                                                <td class="no-line text-center">
                                                    <strong><?php echo $this->lang->line('CUST_LBL_243'); ?></strong></td>
                                                <td class="no-line text-right"><h4 class="m-0"><?php echo $currency.' '.roundMe($invAmount); ?></h4></td>
                                            </tr>
                                            <?php if($amountPaid > 0) { ?>
                                                <tr>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line text-center">
                                                        <strong><?php echo $this->lang->line('CUST_LBL_320'); ?></strong></td>
                                                    <td class="no-line text-right"><h4 class="m-0"><?php echo $currency.' '.roundMe($amountPaid); ?></h4></td>
                                                </tr>
                                            <?php } ?>
                                            <?php if($balance > 0) { ?>
                                                <tr>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line text-center">
                                                        <strong><?php echo $this->lang->line('CUST_LBL_319'); ?></strong></td>
                                                    <td class="no-line text-right"><h4 class="m-0"><?php echo $currency.' '.roundMe($balance); ?></h4></td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                        
                                        
                                        <?php if($rsPayments) { ?>
                                            <br />
                                            <table class="table">
                                                <tr><td colspan="5"><h4><?php echo $this->lang->line('CUST_LBL_318'); ?></h4></td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo  $this->lang->line('CUST_LBL_192'); ?></td>
                                                    <td><?php echo  $this->lang->line('CUST_CH_127'); ?></td>
                                                    <td><?php echo  $this->lang->line('CUST_CH_128'); ?></td>
                                                    <td><?php echo  $this->lang->line('CUST_CH_6'); ?></td>
                                                    <td><?php echo  $this->lang->line('CUST_CODE_11'); ?></td>
                                                </tr>
                                                <?php foreach($rsPayments as $row){ ?>
                                                <tr>
                                                    <td><?php echo $currency.' '.$row->InvAmount;?></a></td>
                                                    <td><?php echo stripslashes($row->PaymentMethod); ?></td>
                                                    <td><?php echo stripslashes($row->InvTransactionId); ?></td>
                                                    <td><?php echo $row->InvDtTm; ?></td>
                                                    <td><?php echo $row->InvComments == '' ? '-' : stripslashes($row->InvComments); ?></td>
                                                </tr>
                                                <?php } ?>
                                        <?php } ?>
                                            </table>
                                            <br />
                                    </div>
                                    <div class="d-print-none">
                                        <div class="pull-right">
                                            <a href="javascript:window.print()" class="btn btn-primary waves-effect waves-light"><i class="fa fa-print"></i> Print</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>
