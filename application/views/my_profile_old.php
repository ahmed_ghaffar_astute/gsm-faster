<div class="page-content">
	<div class="row">
		<div class="col-md-12">
			<h2><i class="fa fa-dashboard"></i> </h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="block-web">

				<?php echo form_open(base_url('page/myprofile'))?>
				<div class="form-group row">
					<label for="example-text-input" class="col-sm-2 col-form-label"><?php echo $this->lang->line('CUST_ACC_2'); ?>*</label>
					<div class="col-sm-10">
						<input class="form-control" type="text" autocomplete="off" name='txtFName' id="txtFName" value='<?php echo $fName; ?>' required />
					</div>
				</div>
				<div class="form-group row">
					<label for="example-text-input" class="col-sm-2 col-form-label"><?php echo $this->lang->line('CUST_ACC_3'); ?>*</label>
					<div class="col-sm-10">
						<input class="form-control" type="text" autocomplete="off" name='txtLName' id="txtLName" value='<? echo $lName; ?>' required />
					</div>
				</div>
				<div class="form-group row">
					<label for="example-text-input" class="col-sm-2 col-form-label"><?php echo  $this->lang->line('CUST_CH_114'); ?>*</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" size='20'  name='txtPhone' id="txtPhone" value='<? echo($phone);?>' />
						<em>Mobile No. start with country code without + e.g. (11234512345 for US)</em>
					</div>
				</div>
				<div class="form-group row">
					<label for="example-text-input" class="col-sm-2 col-form-label"><?php echo $this->lang->line('CUST_CODE_9'); ?>*</label>
					<div class="col-sm-10">
						<select name="countryId" id="countryId" class="form-control">
							<?php $country_data = get_country_data(); ?>
							<?php FillCombo($countryId, $country_data); ?>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label for="example-text-input" class="col-sm-2 col-form-label"><?php echo  $this->lang->line('CUST_LBL_224'); ?>*</label>
					<div class="col-sm-10">
						<select name="language" id="language" class="form-control">
							<?php
							$LANGUAGES_ARR = array("en"=>'English', "fr"=>'French', "ru"=>'Russian', "sp"=>'Spanish', "hu"=>'Hungarian', "de"=>'German', "pl"=>'Polish', "pt"=>'Portuguese', "cns"=>'Chinese (Simplified)', "ro"=>'Romanian', "vn"=>'Vietnamese', "vn"=>'Italian');
							foreach ($LANGUAGES_ARR as $key => $value)
							{
								$selected = '';
								if($lang == $key)
									$selected = 'selected';
								echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
							}
							?>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label for="example-text-input" class="col-sm-2 col-form-label">Client Panel</label>
					<div class="col-sm-10">
						<select name="cPanel" class="form-control">
							<option value="0" <?php if($cPanel == '0') echo 'selected';?>>Panel 1</option>
							<option value="1" <?php if($cPanel == '1') echo 'selected';?>>Panel 2</option>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label for="example-text-input" class="col-sm-2 col-form-label">Theme Color</label>
					<div class="col-sm-10">
						<select name="themeColor" class="form-control">
							<option value="blue" <?php if($themeColor == 'blue') echo 'selected';?>>Blue</option>
							<option value="green" <?php if($themeColor == 'green') echo 'selected';?>>Green</option>
							<option value="orange" <?php if($themeColor == 'orange') echo 'selected';?>>Orange</option>
							<option value="purple" <?php if($themeColor == 'purple') echo 'selected';?>>Purple</option>
							<option value="red" <?php if($themeColor == 'red') echo 'selected';?>>Red</option>
							<option value="teal" <?php if($themeColor == 'teal') echo 'selected';?>>Teal</option>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label for="example-text-input" class="col-sm-2 col-form-label">&nbsp;</label>
					<div class="col-sm-10">
						<input class="" type="checkbox" name="chkSubscribe" id="chkSubscribe" <?php echo $subscribed == 1 ? 'checked' : '';?> /> <span class="labelSub"><? echo $this->lang->line('CUST_LBL_228'); ?></span>
					</div>
				</div>
				<div class="form-group row">
					<label for="example-text-input" class="col-sm-2 col-form-label">&nbsp;</label>
					<div class="col-sm-10">
						<input class="" type="checkbox" name="chkLoginVerification" id="chkLoginVerification" <?php echo $loginVerification == 1 ? 'checked' : '';?> /><span class="labelSub">Two Step Login Verification</span>
					</div>
				</div>
				<?php $i = 1;
				foreach($rsFields as $row) { ?>
					<div class="form-group row">
						<label for="example-text-input" class="col-sm-2 col-form-label"><?php echo $row->FieldLabel; ?><? if($row->Mandatory == '1') echo '*'; ?></label>
						<div class="col-sm-10">
							<?php if($row->FieldType == 'Text Box') { ?>
								<input type='text' class="form-control" <?php if($row->Mandatory == '1') echo ' required'; ?> name="fld<?php echo $i; ?>" maxlength="100" size="30" placeholder='<?php echo $row->FieldLabel; ?>' value="<?php echo $USER_VALUES[$row->FieldColName]; ?>" />
							<? } else if($row->FieldType == 'Text Area') { ?>
								<textarea name="fld<?php echo $i; ?>" class="form-control" placeholder='<?php echo $row->FieldLabel; ?>' <? if($row->Mandatory == '1') echo ' required="1"'; ?> style="height:124px;"><?php echo $USER_VALUES[$row->FieldColName]; ?></textarea>
							<? } else if($row->FieldType == 'Drop Down') { ?>
								<select name="fld<? echo $i; ?>" class="form-control">
									<?php
									$rsValues = get_reg_field_values($row->FieldId);
									foreach($rsValues as $rw)
									{
										$slctd = '';
										if($rw->RegValue == $USER_VALUES[$row->FieldColName])
											$slctd = 'selected';
										echo '<option value="'.$rw->RegValue.'" '.$slctd.'>'.$rw->RegValue.'</option>';
									}
									?>
								</select>
							<?php }
							else if($row->FieldType == 'Radio Button') { ?>
								<?php
								$rsValues = get_reg_field_values($row->FieldId);
								foreach($rsValues as  $rw)
								{
									$chckd = '';
									if($rw->RegValue == $USER_VALUES[$row->FieldColName])
										$chckd = 'checked';
									echo '<input type="radio" class="form-control" name="fld'.$i.'"  '.$chckd.' value="'.$rw->RegValue.'">'.$rw->RegValue.'&nbsp;&nbsp;&nbsp;';
								}
							}?>
						</div>
					</div>;
					<?php
					echo '<input type="hidden" name="colNm'.$i.'" value="'.$row->FieldColName.'" />';
					echo '<input type="hidden" name="mndtry'.$i.'" value="'.$row->Mandatory.'" />';
					echo '<input type="hidden" name="lbl'.$i.'" value="'.$row->FieldLabel.'" />';
					$i++;
				} ?>
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">&nbsp;</label>
					<div class="col-sm-10">
						<input type="hidden" name="totalRegFields" value="<?php echo $totalFields; ?>" />
						<input type="hidden" name="olderPanel" value="<?php echo $cPanel; ?>" />
						<button type="submit" class="btn btn-primary waves-effect waves-light RedBtn">
							Submit
						</button>
						<a href="<?php echo base_url('dashboard');?>" class="btn btn-secondary waves-effect m-l-5 BtnBlack">Cancel</a>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
