<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('CUST_LBL_313'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-lg-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<?php if (isset($error_message) && $error_message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $error_message; ?></div>

							</div>
						</div>
					<?php } ?>
					<?php echo form_open(base_url('page/transfer_credits')); ?>
					<div class="form-group row">
						<div class="col-lg-2">
							<label for="example-text-input"><?php echo $this->lang->line('CUST_INDEX_USRNAME'); ?>
								<span class="required_field"><span class="required_field">*</span></span></label>
						</div>
						<div class="col-lg-3">
							<input required="required" type='text'
								   placeholder="<?php echo $this->lang->line('CUST_INDEX_USRNAME'); ?>"
								   name='txtUName' id="txtUName" value='' class="form-control"/>
							<?php echo form_error('txtUName', '<div class="error">', '</div>'); ?>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-2">
							<label for="example-text-input"
								   class="col-form-label"><?php echo $this->lang->line('CUST_LBL_222'); ?><span class="required_field"><span class="required_field">*</span></span></label>
						</div>
						<div class="col-lg-3">
							<input type='text' placeholder="<?php echo $this->lang->line('CUST_LBL_222'); ?>"
								   required="required" name='txtEmail' id="txtEmail" class="form-control" value=''/>
							<?php echo form_error('txtEmail', '<div class="error">', '</div>'); ?>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-2">
							<label for="example-text-input"
								   class="col-form-label"><? echo $this->lang->line('CUST_MENU_CREDITS'); ?>
								<span class="required_field"><span class="required_field">*</span></span> </label>
						</div>
						<div class="col-lg-3">
							<input placeholder="<?php echo $this->lang->line('CUST_MENU_CREDITS'); ?>" type='text'
								   required="required" size='20' name='txtCredits' class="form-control"
								   id="txtCredits" value=''/>
							<?php echo form_error('txtCredits', '<div class="error">', '</div>'); ?>
							<div class="form-text text-muted">Please enter the amount
								in <?php echo $userDetails->CurrencyAbb; ?></div>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-lg-2">
							<label for="example-text-input"
								   class="col-form-label"><?php echo $this->lang->line('CUST_CODE_11'); ?></label>
						</div>
						<div class="col-lg-3">
							<textarea class="form-control"
									  placeholder="<?php echo $this->lang->line('CUST_CODE_11'); ?>"
									  name='txtComments' style="height:120px;"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-2"></div>
						<div class="col-lg-3">
							<button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
