<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4>
				<i class="icon-arrow-left52 mr-2"></i>
				<?php echo $this->lang->line('CUST_DB_HEADER'); ?>
			</h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-lg-12 col-lg-12 col-xl-12">
				<div class="card-body">

					<div class="form-group row">
						<div class="col-lg-12">
							<h3 class="content-header"> Your Current Setup
								<div data-toggle="buttons" class="button-group pull-right"><a
										class="btn active small border-gray right-margin" href="javascript:;"> <span
											class="button-content">
          <input style="display: none;" type="radio" name="radio-toggle-1">
          Today </span> </a> <a class="btn small border-gray" href="javascript:;"> <span class="button-content">
          <input style="display: none;" type="radio" name="radio-toggle-1">
        Weekly </span> </a></div>
							</h3>
							<table class="table margin-top-20" width="100%" cellspacing="0" cellpadding="0"
								   border="0">
								<tbody>
								<tr>
									<td class="fa-border">
										<button class="btn btn-danger padd-adj"
												type="button"><?php echo $userDetails->CurrencySymbol . roundMe($userDetails->Credits); ?></button>
										<?php echo $this->lang->line('CUST_LBL_55'); ?></td>
									<td class="fa-border">
										<button class="btn btn-primary padd-adj" type="button"><?php echo $userDetails->CurrencySymbol . roundMe($totalReceipts); ?></button>
										<?php echo $this->lang->line('CUST_LBL_215'); ?></td>
									<td class="fa-border">
										<button class="btn btn-info padd-adj" type="button">15</button>
										<?php echo $this->lang->line('CUST_LBL_216'); ?></td>
									<td class="fa-border">
										<button class="btn btn-info padd-adj"
												type="button"><?php echo $userLockedCount; ?></button>
										<?php echo $this->lang->line('CUST_LBL_323'); ?></td>
								</tr>
								<tr>
									<td class="fa-border">
										<button class="btn btn-primary padd-adj"
												type="button"><?php echo $ordersTotal['pending']; ?></button>
										<?php echo $this->lang->line('CUST_LBL_205'); ?></td>
									<td class="fa-border">
										<button class="btn btn-primary padd-adj"
												type="button"><?php echo $ordersTotal['completed']; ?></button>
										<?php echo $this->lang->line('CUST_LBL_208'); ?></td>
									<td class="fa-border">
										<button class="btn btn-warning padd-adj"
												type="button"><?php echo $ordersTotal['rejected']; ?></button>
										<?php echo $this->lang->line('CUST_LBL_207'); ?></td>
									<td class="fa-border">
										<button class="btn btn-warning padd-adj"
												type="button"><?php echo $ordersTotal['inprocess']; ?></button>
										<?php echo $this->lang->line('CUST_LBL_57'); ?></td>
								</tr>
								</tbody>
							</table>
							<div class="bottom">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
									<tr>
										<td><strong>Savings</strong></td>
										<td>
											<div id="work-progress1">
												<canvas
													style="display: inline-block; width: 124px; height: 25px; vertical-align: top;"
													width="124" height="25"></canvas>
											</div>
										</td>
										<td width="100">&nbsp;</td>
										<td><strong>Expenses</strong></td>
										<td>
											<div id="work-progress2">
												<canvas
													style="display: inline-block; width: 131px; height: 25px; vertical-align: top;"
													width="131" height="25"></canvas>
											</div>
										</td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!--/col-md-3-->
					<!--/col-md-3-->

					<!--/row-->

					<!--/row end-->

					<div class="form-group row">
						<div class="col-lg-8">
							<div class="block-web">
								<h4><?php echo $this->lang->line('CUST_LBL_30'); ?></h4>
								<div class="table-responsive">
									<table class="table table-striped table-bordered media-table"
										   style="table-layout: fixed;">
										<!--<thead>
											<tr>
												<th style="width: 150px">Image</th>
												<th>Description</th>
												<th class="text-center">Actions</th>
											</tr>
										</thead>-->
										<tbody>
										<?php foreach ($latestFiveImeiOrders as $order) { ?>
											<tr>
												<td style="word-wrap:break-word">
													#<?php echo $order->CodeId; ?>
												</td>
												<td style="word-wrap:break-word">
													<?php echo $order->PackageTitle; ?>
												</td>
												<td style="word-wrap:break-word">
													<?php echo $order->CodeStatus; ?>
												</td>
												<td style="word-wrap:break-word">
													<?php echo $order->RequestedAt; ?>
												</td>
												<td style="word-wrap:break-word">
													<?php echo $order->IMEINo; ?>
													<div>
														<small><?php echo $order->Credits; ?> Credits</small>
													</div>
												</td>
											</tr>
										<?php } ?>
										<?php if (!count($latestFiveImeiOrders)) { ?>
											<tr>
												<td align="center" colspan="5">No Orders Found!</td>
											</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
								<!--/table-responsive-->
							</div>
							<!--/block-web-->
						</div>
						<!--/col-md-8-->
						<div class="col-lg-4">
							<h4><?php echo $this->lang->line('CUST_LBL_292'); ?></h4>
							<div class="table-responsive">
								<table class="table table-striped table-bordered media-table"
									   style="table-layout: fixed;">
									<tbody>
									<?php
									$iCount = 0;
									foreach ($latestFiveTickets as $ticket) {

										$cls = 'info';
										$clsLI = '';

										if ($ticket->StatusId == '2')
											$cls = 'success';
										else if ($ticket->StatusId == '3')
											$cls = 'info';
										else if ($ticket->StatusId == '4')
											$cls = 'danger';
										else if ($ticket->StatusId == '5')
											$cls = 'warning';
										if ($iCount == count($latestFiveTickets) - 1)
											$clsLI = ' pb-2';

										?>
										<li class="feed-item<?php echo $clsLI; ?>">
												<span class="date"><a
														href="ticket/view/<?php echo($ticket->TicketId); ?>">#<?php echo stripslashes($ticket->TicketId); ?></a> - <?php echo convertDate($ticket->DtTm); ?></span>
											<span class="activity-text">
					<?php echo stripslashes($ticket->Subject); ?>
                                    <span
										class="badge badge-<?php echo $cls; ?>"><?php echo($ticket->TcktStatus); ?></span>
</span>
										</li>

										<?php $iCount++;
									} ?>
									<?php if (!count($latestFiveImeiOrders)) { ?>
										<tr>
											<td align="center" colspan="5">No Orders Found!</td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
<!--
					<div class="form-group row">
						<div class="col-lg-8">
							<div class="fullcalendar-basic fc fc-ltr fc-unthemed" style=""></div>

						</div>
						<div class="col-lg-4">
							<h3 class="content-header">Note</h3>
							<div class="block widget-notes">
								<div class="paper" contenteditable="true"> Send e-mail to supplier
									<br>
									<s>Conference at 4 pm.</s>
									<br>
									<s>Order a pizza</s>
									<br>
									<s>Buy flowers</s>
									<br> Buy some coffee.
									<br> Dinner at Plaza.
									<br> Take Alex for walk.
									<br> Buy some coffee.
									<br>
								</div>
							</div>

						</div>
					</div>
					-->
					<!--/row end-->
				</div>
				<!--/page-content end-->
