<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $label; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<div class="form-group row">
						<div class="col-lg-12">
							<table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
								   style="border-collapse: collapse; border-spacing: 0; width: 100%;">
								<thead>
								<tr class="bg-primary">
									<th><?php echo $this->lang->line('CUST_LBL_191'); ?></th>
									<th><?php echo $this->lang->line('CUST_CH_6'); ?></th>
									<th><?php echo $this->lang->line('CUST_MENU_CREDITS'); ?></th>
									<th>Vat</th>
									<th><?php echo $this->lang->line('CUST_LBL_192'); ?></th>
									<th><?php echo $this->lang->line('CUST_CH_127'); ?></th>
									<th><?php echo $this->lang->line('CUST_CH_126'); ?></th>
									<th><?php echo $this->lang->line('CUST_LBL_301'); ?></th>
								</tr>
								</thead>
								<tbody>
								<?php foreach ($invoices as $row) {
									$dtTm = explode(" ", $row->PaymentDtTm);
									$invCredits = $controller_instanse->decrypt($row->Credits);
									if ($invCredits == '')
										$invCredits = '-';
									$invoiceAmount = $controller_instanse->decrypt($row->Amount);
									if ($invoiceAmount == '')
										$invoiceAmount = '-';
									?>
									<tr>
										<td>#<?php echo $row->PaymentId; ?></td>
										<td><?php echo $row->PaymentDtTm; ?></td>
										<td><?php echo $invCredits; ?></td>
										<td><?php echo $row->Currency . ' ' . $row->Vat; ?></td>
										<td><?php echo $row->Currency . ' ' . $invoiceAmount; ?></td>
										<td><?php echo($row->PaymentMethod == '' ? '-' : $row->PaymentMethod); ?></td>
										<td align="center"><span
												class="badge badge-pill badge-<? echo $row->PaymentStatusId == '2' ? 'success' : 'danger'; ?>"><?php echo $row->PaymentStatus; ?></span>
										</td>
										<td>
											<a href="<?php echo base_url('page/invoice_detail/') . $row->PaymentId . '/' . $type; ?>"
											   class="btn btn-primary waves-effect waves-light" role="button">Manage</a>
										</td>
									</tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
					<!--/col-md-3-->
					<!--/col-md-3-->
				</div>
				<!--/row-->
			</div>
			<!--/page-content end-->
		</div>
	</div>
</div>
