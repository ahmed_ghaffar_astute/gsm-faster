<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('CUST_LBL_184'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-lg-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<!-- Our Working Area Start -->
                    <div class="form-group row">
						<div class="col-lg-12">
                        <?php   
                        if('success')
                            echo $success ;
                        if($message != '')
                            echo '<div class="alert alert-success">'.$message.'</div>'; 
                        if($errorMsg != '')
                            echo '<div class="alert alert-danger">'.$errorMsg.'</div>'; 
                        ?>
                    </div>
                </div>
					<?php echo form_open_multipart(base_url('page/server_services') , "name=frm id=frm");?>
					<div class="form-group row">
						<div class="col-lg-6">
							<div class="form-group row">
								<div class="col-md-12">
									<label><?php echo $this->lang->line('CUST_LBL_48'); ?>:</label>
									<select class="form-control select2" id="packageId" name="packageId" required style="width: 100%; color: inherit;">
										<?php
										$rsPackages = get_log_rsPackages($strPackIds);
										$prevCatId = 0;
										foreach($rsPackages as $row)
										{
											$changeBGColor = '';
											$myPackPrice = 0;
											if(isset($PACK_PRICES_USER[$row->PackageId]))
												$myPackPrice = $PACK_PRICES_USER[$row->PackageId];
											else if(isset($PACK_PRICES_PLAN[$row->PackageId]))
												$myPackPrice = $PACK_PRICES_PLAN[$row->PackageId];
											else
											{
												if (isset($PACK_PRICES_BASE[$row->PackageId]))
													$myPackPrice = $PACK_PRICES_BASE[$row->PackageId];
											}
											if($myPackPrice < $PACK_PRICES_BASE[$row->PackageId])
												$changeBGColor = 'class="discount_text"';
											if($prevCatId > 0)
											{
												if($row->CategoryId != $prevCatId)
													echo '</optgroup><optgroup label="'.$row->Category.'"> ';
											}
											else
												echo '<option value="" selected>Choose ...</option><optgroup label="'.$row->Category.'">';
											?>
											<option <?php if($myNetworkId == $row->PackageId) echo 'selected';?> details2='<?php echo $myPackPrice;?>' value="<?php echo $row->PackageId?>" <?php echo $changeBGColor; ?> >
												<?php
												$optionText = $row->LogPackageTitle." - ".$myPackPrice." "."Credits";
												/*
												if($row->DeliveryTime != '')
													$optionText .= " (".$row->DeliveryTime.")";*/
												echo $optionText; ?>
											</option>
											<?php
											$prevCatId = $row->CategoryId;
										}
										echo '</optgroup>';
										?>
									</select>
								</div>
								<div class="col-md-12 dot-opacity-loader" align="center">
									<span></span>
									<span></span>
									<span></span>
								</div>
								<div class="col-md-12" id="tblInfo" style="display:none;">
									<div class="card m-b-30 card-info">
										<div class="card-body" style="background-color:#F5F5F5;">
											<blockquote class="card-bodyquote">
												<p id="dvSN" style="font-weight:bold; font-size:20px;">&nbsp;</p>
												<footer class="blockquote-footer" style="color:#E81516; font-size:20px; font-weight:bold;" id="lblCustPrice"></footer>
												<p id="dvSrvImg" style="display:none;"><img id="imgSrv" src="" /></p>
												<p style="color:green;"><strong>Delivery Time: </strong><span id="dvTT" style="color:#000000;"><?php echo $timeTaken; ?></span></p>
												<p id="dvFeatures"></p>
												<p id="lblQtyPrice"></p>
												<p id="dvBlkQty">&nbsp;</p>
												<p id="dvMR"></p>
												<p id="dvBlkQty">&nbsp;</p>
											</blockquote>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="edit-cont form-group row">
								<div class="col-lg-12">
									<div id="dvCstmFlds"></div>
									<div class="form-group row">
										<div class="col-lg-12">
											<label><?php echo $this->lang->line('CUST_CODE_10'); ?>:</label>
											<textarea rows="1" class="form-control" id="txtNotes" name="txtNotes" placeholder="Notes"><?php echo $notes; ?></textarea>
										</div>
									</div>	<!-- end col -->
									<div class="form-group row">
										<div class="col-lg-12">
											<label><?php echo $this->lang->line('CUST_CODE_11'); ?>:</label>
											<textarea rows="1" class="form-control" id="txtComments" name="txtComments" placeholder="Comments"><?php echo $comments; ?></textarea>
										</div>
									</div>	<!-- end col -->
									<div class="form-group row">
										<div class="col-lg-12">
											<label><?php echo $this->lang->line('CUST_CH_98'); ?>:</label>
											<input type="text" class="form-control" placeholder="Response E-mail:" name="txtAltEmail" maxlength="100" id="txtAltEmail" value="<?php echo($altEmail);?>">
										</div>
									</div>	<!-- end col -->
									<?php if($userDetails->PinCode != '') { ?>
										<div class="form-group row">
											<div class="col-lg-12">
												<label><?php echo $this->lang->line('CUST_LBL_178'); ?>:*</label>
												<input type="text" autocomplete="off" class="form-control" placeholder="Enter your Pin Code" name="txtPinCode" maxlength="4" id="txtPinCode" value="" />
											</div>
										</div>	<!-- end col -->
									<?php } ?>
									<div class="form-group row">
										<div class="col-lg-10">
											<button type="submit" class="btn btn-primary" onClick="setValue('2');"><?php echo $this->lang->line('CUST_MENU_CODES'); ?></button>
										</div>
									</div>
									<input type="hidden" id="hdPreOrder" name="hdPreOrder" value="0" />
									<input type="hidden" id="cldFrm" name="cldFrm" />
									<input type="hidden" id="ddType" name="ddType" value="<?php echo $this->data['settings']->ServerOrderDropDownType; ?>" />
									<input type="hidden" id="ppl" name="ppl" value="<?php echo $this->data['userDetails']->PricePlanId;?>" />
									<input type="hidden" id="cldFm" name="cldFm" value="<?php echo $cldFm; ?>" />
									<input type="hidden" id="currAbb" name="currAbb" value="<?php echo $this->data['userDetails']->CurrencyAbb; ?>" />
									<input type="hidden" id="currSymbol" name="currSymbol" value="<?php echo $this->data['userDetails']->CurrencySymbol; ?>" />
									<input type="hidden" id="themeStyle" name="themeStyle" value="<?php echo 0 ; ?>" />
									<input type="hidden" id="min_qty" name="min_qty" value="1" />
									<div class="form-group row">
										<div class="alert alert-danger clsError" id="dvError" style="display:none;">&nbsp;</div>
									</div>
									<!--<div class="form-group row" id="dvCstmFlds"></div>-->
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close(); ?>
            	</div>
        	</div>
    	</div>
	</div>
</div>
<script> var base_url = '<?php echo base_url();?>' </script>
<script language="javascript" src="<?php echo base_url('assets/js/functions.js');?>"></script>
<script language="javascript" src="<?php echo base_url('assets/js/serverorder.js?v='.random_string('alnum', 16));?>"></script>
