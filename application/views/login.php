<style>
	.foot a{
		color: #fff !important;
	}
</style>
<div class="middle-login">
    <div class="block-web">
        <div class="head">
            <h3 class="text-center"><?php echo $this->config->item('app_name'); ?></h3>
        </div>
        <div style="background:#fff;">
            <?php echo form_open(base_url('login'), array('class' => 'form-horizontal', 'style'=>"margin-bottom: 0px !important;")); ?>
                <div class="content">
                    <h4 class="title">Login Access</h4>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="input-group"> <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                <input type="text" value="<?php echo set_value('username'); ?>" name="username" class="form-control" id="username" placeholder="Username">
                            </div>
                            <?php echo form_error('username'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="input-group"> <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="password" value="<?php echo set_value('password'); ?>" name="password" class="form-control" id="password" placeholder="Password">
                            </div>
                            <?php echo form_error('password'); ?>
                        </div>
                    </div>
                </div>
                <div class="foot">
                    <a href="<?php echo base_url('register'); ?>" class="btn btn-primary">Register</a>
                    <button type="submit" class="btn btn-primary">Log in</button>
                </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <?php $this->load->view('layouts/login_copyright'); ?>
</div>
