<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <h1>Pay With Stripe</h1>
        </div>
        <!--/col-md-12-->
    </div>
    <?php if($invId){ ?>
        <div class="row">
            <div class="col-md-12">
                <div class="block-web">
                    <section class="single_page_wrap mt mb"  style="clear: both;margin-top: 14px;background: #fff;">
                        <div id="main" class="container volt_hidden volt_visible animated fadeInUp" role="main">
                            <div class="row">
                                <div>
                                    <form action=<?php echo base_url();?>"page/charge_credits" method="POST">
                                        <script src="https://checkout.stripe.com/checkout.js" class="stripe-button" data-key="<?php echo $dKey; ?>" data-name="<?php echo $stripslashes($this->data['settings']->Company); ?>" data-email="<?php echo $this->session->userdata('UserEmail');?>" data-description="<?php echo $srvcTitle;?>" data-amount="<?php echo ($amount * 100);?>"></script>
                                        <input type="hidden" name="invId" value="<?php echo $invId;?>"/>
                                        <input type="hidden" name="amount" value="<?php echo $amount;?>"/>
                                        <input type="hidden" name="service" value="<?php echo $srvcTitle;?>"/>
                                        <input type="hidden" name="pMId" value="<?php echo $pMId;?>"/>
                                        <input type="hidden" name="currency" value="<?php echo $currency;?>"/>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    <?php } 
    else 
    {
        echo 'Invoice # is not correct' ;
    }
    ?>
</div>