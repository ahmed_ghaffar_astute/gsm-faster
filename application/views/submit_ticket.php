<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('CUST_LBL_294'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>

					<?php echo form_open(base_url('page/submit_ticket')); ?>
					<div class="form-group row">
						<div class="col-lg-2">
							<label class="control-label">Subject<span class="required_field">*</span></label>
						</div>
						<div class="col-lg-3">
							<input class="form-control" type="text" autocomplete="off" maxlength="30"
								   name="txtSubject" id="txtSubject" required>
							<?php echo form_error('txtSubject', '<div class="error">', '</div>'); ?>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-2">
							<label class="col-form-label">Department<span class="required_field">*</span></label>
						</div>
						<div class="col-lg-3">

							<select name="dId" class="form-control">
								<option value="">Please Select Department</option>
								<?php foreach ($departments as $dep) { ?>
									<?php if ($dep->Id == $id) { ?>
										<option value="<?php echo $dep->Id; ?>"><?php echo $dep->Value; ?></option>
									<?php } else { ?>
										<option value="<?php echo $dep->Id; ?>"><?php echo $dep->Value; ?></option>
									<?php } ?>
								<?php } ?>
							</select>
							<?php echo form_error('dId', '<div class="error">', '</div>'); ?>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-lg-2">
							<label class="col-form-label">Category<span class="required_field">*</span></label>
						</div>
						<div class="col-lg-3">
							<select name="catId" class="form-control">
								<option value="">Please Select Category</option>
								<?php foreach ($categories as $cat) { ?>
									<?php if ($cat->Id == $id) { ?>
										<option value="<?php echo $cat->Id; ?>"><?php echo $cat->Value; ?></option>
									<?php } else { ?>
										<option value="<?php echo $cat->Id; ?>"><?php echo $cat->Value; ?></option>
									<?php } ?>
								<?php } ?>
							</select>
							<?php echo form_error('catId', '<div class="error">', '</div>'); ?>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-2">
							<label class="col-form-label">Priority<span class="required_field">*</span></label>
						</div>
						<div class="col-lg-3">
							<select name="priorityId" class="form-control">
								<?php foreach ($priorities as $pri) { ?>
									<?php if ($pri->Id == $id) { ?>
										<option value="<?php echo $pri->Id; ?>"
												selected><?php echo $pri->Value; ?></option>
									<?php } else { ?>
										<option value="<?php echo $pri->Id; ?>"><?php echo $pri->Value; ?></option>
									<?php } ?>
								<?php } ?>
							</select>
							<?php echo form_error('priorityId', '<div class="error">', '</div>'); ?>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-2">
							<label class="col-form-label">Message<span class="required_field">*</span></label>
						</div>
						<div class="col-lg-4">
						<textarea class="form-control" name='txtMessage' cols='50' rows='5'
								  required="required"></textarea>
							<?php echo form_error('txtMessage', '<div class="error">', '</div>'); ?>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-2"></div>
						<div class="col-lg-3">
						<button class="btn btn-primary" type="submit">Submit</button>
					</div>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
	<!--/col-md-3-->
	<!--/col-md-3-->
</div>
<!--/row-->
</div>
<!--/page-content end-->
