<div role="main" class="main">
    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Order Placed</h1>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
	            <div class="alert alert-success mt-lg" id="contactSuccess">
	           		<?php if($message != ''){
                        echo $message;
                    } 
                    else{
                         echo '<strong>Success!</strong> Your order has been placed successfully!';
                    }?>
                </div>
            </div>
        </div>
    </div>
</div>