<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <?php 
                    if($error){
                        echo $error;
                    }else if($Detailed_Error_Message){
                        echo $Detailed_Error_Message;
                    }else if($Short_Error_Message){
                        echo $Short_Error_Message;
                    }else if($Error_Code){
                        echo $Error_Code;
                    }else if($Error_Severity_Code){
                        echo $Error_Severity_Code;
                    }      
                ?>
            </div>
        </div>
    </div>
</div>