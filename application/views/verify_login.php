<div class="middle-login">
    <div class="block-web">
        <div class="head">
            <h3 class="text-center"><?php echo $this->config->item('app_name'); ?></h3>
        </div>
        <div style="background:#fff;">
            <?php echo form_open(base_url('login/loginverification?'.$this->input->server('QUERY_STRING')), array('class' => 'form-horizontal', 'style'=>"margin-bottom: 0px !important;")); ?>
                <div class="content">
                    <h4 class="title">Verify Login</h4>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="input-group"> <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="text" value="<?php echo set_value('passcode'); ?>" name="passcode" class="form-control" id="passcode" placeholder="Login Verification Code">
                            </div>
                            <?php echo form_error('passcode'); ?>
                        </div>
                    </div>
                </div>
                <div class="foot">
                    <button type="submit" data-dismiss="modal" class="btn btn-primary">Log in</button>
                </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <?php $this->load->view('layouts/login_copyright'); ?>
</div>