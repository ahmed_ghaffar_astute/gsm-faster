
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<ul class="page-sidebar-menu">
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone">
					</div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
                <li class="sidebar-search-wrapper">
                	&nbsp;<br />
				</li>
				<li class="start <?php if($this->router->fetch_class('dashboard') == 'dashboard') echo 'active'; ?>">
					<a href="<?php echo base_url();?>dashboard?frmId=1">
					<i class="fa fa-home"></i>
					<span class="title">
						<?php echo $this->lang->line('BE_LBL_301'); ?>
					</span>
					<span class="selected">
					</span>
					</a>
				</li>
				
			</ul>
		</div>
	</div>
	<div class="page-content-wrapper">
		<div class="page-content">
        <div class="row">
				<div class="col-md-10">
					<h3 class="page-title"><?php echo $this->lang->line('BE_DB_ICON4'); if($id > 0) echo " # $id";?></h3>
				</div>
				<div class="col-md-2" style="text-align:right;">
                    <a href="<?php echo base_url();?>payments?frmId=<?php echo $this->input->post_get('frmId');?>&fTypeId=<?php echo $this->input->post_get('fTypeId')?>" class="btn btn default"><span class="fa fa-angle-double-left"></span> Back To List</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
	                <?php
                    if($message != '')
                    	echo '<div class="alert alert-success">'.$message.'</div>';
					?>
                    <div class="tabbable tabbable-custom boxless">
                        <ul class="nav nav-tabs nav-tabs-solid border-0">
                            <li class="active">
                                <a href="#tab_0" data-toggle="tab"><?php echo $this->lang->line('BE_LBL_380'); ?></a>
                            </li>
                            <?php if($pStatusId == '1' && $byAdmin == '1') { ?>
                                <li>
                                    <a href="#tab_1" data-toggle="tab"><?php echo $this->lang->line('BE_LBL_626'); ?></a>
                                </li>
                            <?php } ?>
                            <li>
                                <a href="#tab_2" data-toggle="tab"><?php echo $this->lang->line('BE_LBL_227'); ?></a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_0">
                                <?php include APPPATH.'scripts/invoice_tab_general.php'; ?>
                            </div>
                            <?php if($pStatusId == '1' && $byAdmin == '1') { ?>
                                <div class="tab-pane" id="tab_1">
                                    <?php include APPPATH.'scripts/invoice_tab_addpmnt.php'; ?>
                                </div>
                            <?php } ?>
                            <div class="tab-pane" id="tab_2">
                                <?php include APPPATH.'scripts/invoice_tab_payments.php'; ?>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <script language="javascript" src="include/js/modules/payment.js?v=<?=filemtime('include/js/modules/payment.js');?>"></script> -->
