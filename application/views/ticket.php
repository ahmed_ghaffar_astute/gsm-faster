<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('CUST_LBL_296') . ' [#' . $ticket->TicketNo . ']'; ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="row">
			<div class="col-12 col-lg-12 col-md-12 col-lg-10 col-xl-10">
				<div class="card-body">
					<!-- Our Working Area Start -->
					<?php if (isset($message) && $message != '') {
						$this->session->set_flashdata('message', "");
						?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-success"><?php echo $message; ?></div>

							</div>
						</div>
					<?php } ?>
					<?php if (isset($errorMsg) && $errorMsg != '') { ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<div class="alert alert-warning"><?php echo $errorMsg; ?></div>

							</div>
						</div>
					<?php } ?>
					<?php echo form_open(base_url('ticket/update/' . $ticket->TicketId)); ?>
						<div class="form-group row">
							<div class="col-lg-12">
								<h4 class="card-title">
									<i class="fa fa-ticket"></i> Ticket ID: <?php echo "[#$ticket->TicketNo]"; ?>
									<?php if ($ticket->StatusId != '2') { ?>
										- <a class="btn btn-primary btn-sm"  href="<?php echo base_url("ticket/update/") . $ticket->TicketId; ?>?close=1">Close</a>
									<?php } ?>
								</h4>
								<p class="card-description">
									<strong>Subject:</strong> <?php echo $ticketDepartment->Subject; ?><br/><br/>
									<strong>Status:</strong> <?php echo $ticket->TcktStatus; ?><br/><br/>
									<strong>Department:</strong> <?php echo $ticketDepartment->DeptName; ?></p>


								<?php if ($ticket->StatusId != '2') { ?>
									<hr/>
									<div class="card-body">
										<h4 class="card-title"><i class="fa icon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i> Reply</h4>
										<!--form class="cmxform" id="commentForm" method="post" id="myFrm" name="myFrm"-->
										<input type="hidden" name="tcktId" value="<?php echo($ticket->TicketId); ?>">
										<input type="hidden" id="cldFrm" name="cldFrm" value="0">
										<fieldset>
											<div class="form-group">
												<label for="cname">Message*</label>
												<textarea name="txtReply" rows="7" required="required"
														class="form-control"></textarea>
											</div>
											<input class="btn btn-primary" name="btnReply" type="submit"
												onclick="document.getElementById('cldFrm').value = '1';"
												value="Submit">
										</fieldset>
										<!--/form-->
									</div>
								<?php } ?>


								<?php
								//print_r($ticketReplies);
								if (count($ticketReplies) > 0) { ?>
									<hr/>
									<div class="card-body">

										<section id="cd-timeline" class="cd-container">

											<?php

											foreach ($ticketReplies as $ticketReply) {
												$name = $ticketReply->Name;
												if ($name == '')
													$name = $ticketReply->ClientName;
												$class = 'danger';
												if ($ticketReply->ByAdmin == '1') {
													$name = 'Admin';
													$class = 'primary';
												}
												$arrDt = explode(' ', $ticketReply->DtTm);
												?>
												<div class="cd-timeline-block">
													<div class="cd-timeline-img bg-<?php echo $class; ?>">
														<i class="mdi mdi-adjust"></i>
													</div> <!-- cd-timeline-img -->
													<div class="cd-timeline-content">
														<h3><?php echo convertDate($arrDt[0]); ?></h3>
														<p class="mb-0 text-muted"><?php echo stripslashes($ticketReply->Message); ?></p>
														<span class="cd-date">By <?php echo $name; ?> at <?php
															$date = new DateTime($ticketReply->DtTm);
															echo $result = $date->format('d-m-Y H:i:s');
															  ?></span>
													</div> <!-- cd-timeline-content -->
												</div> <!-- cd-timeline-block -->
												<?php
											}

											?>
										</section>
									</div>
								<?php } ?>
							</div> <!-- end col -->
						</div> <!-- end row -->
					<?php echo form_close(); ?>
				</div>
			</div>
			<!--/col-md-3-->
			<!--/col-md-3-->
		</div>
		<!--/row-->
	</div>
</div>
