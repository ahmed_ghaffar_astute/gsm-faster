<!-- Page header -->
<div class="page-header border-bottom-0">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <?php echo $this->lang->line('CUST_LBL_13'); ?></h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div align="right">
			<?php echo form_open('server_orders', "name=frm id=frm class=form-horizontal"); ?>
			<?php include_once APPPATH . 'scripts/servercodessearchsection.php'; ?>
			<?php echo form_close(); ?>
		</div>

	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content pt-0">
	<div class="card">
		<div class="form-group row">
			<div class="col-12 col-lg-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-12 form-group" align="right">
                            <a href="JavaScript:void(0);" class="btn btn-primary" onclick="exportOrders();"><?php echo $this->lang->line('CUST_LBL_33'); ?></a>&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12" align="right">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th><?php echo $this->lang->line('CUST_LBL_48'); ?></th>
                                    <th>Custom Values</th>
                                    <th align="left"> <?php echo $this->lang->line('CUST_LBL_187'); ?></th>
                                    <th nowrap="nowrap"><?php echo $this->lang->line('CUST_CH_3'); ?></th>
                                    <th>Verify</th>
                                    <th style="text-align:center;">Cancel</th>
                                    <th>
                                        <input type="checkbox" class="chkSelect" id="chkSelect" name="chkSelect" onClick="selectAllChxBxs('chkSelect', 'chkCodes', <?php echo $totalCodes; ?>);" value="true">
                                    </th>                                                    
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if($totalCodes != 0)
                                        {
                                            $k = 0;
                                            $currDtTm = setDtTmWRTYourCountry();
                                            foreach($rsCodes as $row)
                                            {
                                                $showCancel = '0';
                                                $showVerify = '0';
                                                if($row->ReplyDtTm != '')
                                                {
                                                    $diffInMinsV = minutesDiffInDates($row->ReplyDtTm, $currDtTm);
                                                    if($diffInMinsV <= $row->OrderVerifyMins)
                                                        $showVerify = '1';
                                                }
                                                if($row->RequestedAt != '')
                                                {
                                                    $diffInMinsC = minutesDiffInDates($row->RequestedAt, $currDtTm);
                                                    if($diffInMinsC <= $row->OrderCancelMins)
                                                        $showCancel = '1';
                                                }
                                                switch($row->StatusId)
                                                {
                                                    case "1":
                                                        $class = 'warning';
                                                        break;
                                                    case "2":
                                                        $class = 'success';
                                                        break;
                                                    case "3":
                                                        $class = 'danger';
                                                        break;
                                                    case "4":
                                                        $class = 'info';
                                                        break;
                                                }	
                                                $strCustVals = '';
                                                for($i = 1; $i < $colIndex; $i++)
                                                {
                                                    $colName = 'Col'.$i;
                                                    if(isset($row->$colName) && trim($row->$colName) != '')
                                                    {
                                                        if(isset($CUST_COL_LBL[$colName]) && $CUST_COL_LBL[$colName] != '')
                                                        {
                                                            if($strCustVals != '')
                                                                $strCustVals .= '<br />';
                                                            $strCustVals .= '<b>'.$CUST_COL_LBL[$colName].':</b>';
                                                        }
                                                        $strCustVals .= ' '.$row->$colName;
                                                    }
                                                }
                                                $orderStatus = stripslashes($row->CodeStatus);
                                                if($row->HiddenStatus == '1' && $row->CodeSentToOtherServer == '0' && $row->OrderAPIId > 0 && $row->CodeStatusId == '1')
                                                    $orderStatus = 'Processing';
                                            ?>
                                            <tr>
                                                <td>
                                                    <a class="fancybox fancybox.ajax" href="<?php echo base_url();?>page/sodetail?id=<?php echo $row->LogRequestId; ?>" title="Order Details"><i class="fa fa-plus"></i> <?php echo $row->LogRequestId; ?></a>
                                                </td>
                                                <td style="white-space: normal;"><? echo stripslashes($row->LogPackageTitle); ?></td>
                                                <td style="white-space: normal;"><? echo $strCustVals == '' ? '-' : $strCustVals; ?></td>
                                                <td style="white-space: normal;" align="left" title="<? echo str_replace('"', "'", stripslashes($row->Code));?>"><? echo $row->Code == '' ? '-' : substr(stripslashes($row->Code), 0, 100); ?><? echo strlen($row->Code) > 100 ? '<b>...</b>' : '';?></td>
                                                <td align="left"><span class="badge badge-pill badge-<? echo $class;?>"><? echo $orderStatus;?></span></td>
                                                <td valign="middle">
                                                    <?php
                                                        if($row->VerifyOrders == '1' && $showVerify == '1') 
                                                        {
                                                            if($row->StatusId == 2 && $row->Verify==0) 
                                                            {
                                                                $till = addMinutesToDtTm($row->ReplyDtTm, $row->OrderVerifyMins);
                                                                $strData = daysHoursMinsDifferenceInDates($till, $currDTTM)
                                                    ?>
                                                                <a href="<?php echo base_url();?>page/server_orders.php?verify=1&CodeId=<?php echo($row->LogRequestId);?>" onclick="return confirm('<?php echo $this->lang->line('CUST_LBL_186'); ?>')"><i class="fa fa-refresh"></i></a>
                                                                <? if($strData != '') echo '&nbsp;Expires in<br />'.$strData;?>
                                                    <?php   } 
                                                            else if($row->Verify==1)
                                                            {
                                                                echo '<span class="badge badge-pill badge-warning">Under Verification</span>';
                                                            }
                                                            else 
                                                            {
                                                                echo '-'; 
                                                            } 
                                                        } 
                                                        else 
                                                        {
                                                            echo '-';
                                                        }
                                                    ?>
                                                </td>
                                                <td valign="middle" style="text-align:center">
                                                    <?php
                                                        if($row->CancelOrders == '1' && $row->StatusId == '1' && $showCancel == '1' && $orderStatus != 'Processing') 
                                                        { 
                                                    ?>
                                                        <a href="<?php echo base_url();?>page/serverorders?cancel=1&orderId=<? echo($row->LogRequestId);?>" onclick="return confirm('<?php echo $this->lang->line('CUST_LBL_230'); ?>')"><i class="fa icon-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>
                                                    <?php
                                                        }
                                                    else
                                                        echo '-';
                                                    ?>
                                                </td>
                                                <td>
                                                    <input type="checkbox" class="chkSelect" id="chkCodes<?php echo $k; ?>" name="chkCodes[]" value="<? echo $row->LogRequestId; ?>">
                                                </td>                                                          
                                            </tr>
                                            <?php
                                                $k++;
                                            }
                                        }
                                        else{
                                            echo '<tr><td colspan="8">'.$this->lang->line('CUST_LBL_10').'</td></tr>';
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/js/functions.js');?>"></script>
<script>
    $('document').ready(function(){
        
        $(".select2").select2();
        $('#datepicker-autoclose').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });
        $('#datepicker1-autoclose').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });
        $('.fancybox').fancybox();

        $("#btnSearch").click(function(){
            $("#dvSearch").show();
        });
        $("#btnClose").click(function(){
            $("#dvSearch").hide();
        });
    });

        
    function callme(id)
    {
        $("div#"+id).toggle();   
    }
    function downloadOrders()
    {
        var strIds = validate();
        if(strIds != 0)
            window.location.href = '<?php echo base_url();?>page/downloadSOrders?ids='+strIds;
    }
    function exportOrders()
    {
        var strIds = validate();
        if(strIds != 0)
            window.location.href = '<?php echo base_url();?>page/exportserverorders?ids='+strIds;
    }
    function validate()
    {
        var totalCodes = <?php echo $totalCodes; ?>;
        var strIds = '0';
        for(var i=0; i<totalCodes; i++)
        {
            if(document.getElementById('chkCodes'+i).checked)
            {
                strIds += ',' + document.getElementById('chkCodes'+i).value;
            }
        }
        if(strIds == '0')
        {
            alert('Please select a record!');
            return 0;
        }
        else
            return strIds;
    }

    function submitMe(timeoutPeriod) {
        setTimeout("document.getElementById('frm').submit();", timeoutPeriod);
    }
    function sortBy(sortBy, orderByType)
    {
        if(orderByType == 'ASC')
            document.getElementById('orderByType').value = 'DESC';
        else
            document.getElementById('orderByType').value = 'ASC';
        document.getElementById('sortBy').value = sortBy;
        document.getElementById('frm').submit();
    }
    
</script>
