
<div role="main" class="main shop">
	<?php
    if($rsStngs->Retail == '1' && $actRtl == '1' ) { ?>
        <div class="slider-with-overlay">
            <div class="slider-container rev_slider_wrapper" style="height: 560px;">
                <div class="slider rev_slider">
                    <ul>
                        <li data-transition="fade">
                            <img src="<?php echo base_url()."uplds1/".$rwPB->BannerPath; ?>"
                                alt=""
                                data-bgposition="center center" 
                                data-bgfit="cover" 
                                data-bgrepeat="no-repeat" 
                                class="rev-slidebg">
                            
                            <div style="font-size:40px;" class="tp-caption main-label"
                                data-x="80"
                                data-y="180"
                                data-start="500"
                                data-transform_in="y:[-300%];opacity:0;s:500;"><?php echo $pTitle; ?></div>
                        </li>
                    </ul>
                </div>
            </div>
    <?php 
    }
    else
	{
	?>
        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1><?php echo $pTitle; ?></h1>
                    </div>
                </div>
            </div>
        </section>
	<? } ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="lead">
                    <?php echo $pText; ?>
                </p>
            </div>
        </div>
    </div>
</div>
