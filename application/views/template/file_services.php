<div role="main" class="main">
    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>File Services</h1>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <section class="call-to-action call-to-action-primary mb-xl" style="text-align:center;">
            <div class="container">
                <div class="row center">
                    <div class="col-md-12" style="text-align:center;">
                        <h2 class="mb-none word-rotator-title mt-lg" style="color:#FFFFFF;">
                            If you're a 
                            <strong>
                                <span class="word-rotate" data-plugin-options="{'delay': 3500, 'animDelay': 400}">
                                    <span class="word-rotate-items">
                                        <span>Wholesaler</span>
                                        <span>Reseller</span>
                                    </span>
                                </span>
                            </strong>
                            then following prices are for you...
                        </h2>
                        <p class="lead" style="color:#F7F7F7;">Please purchase the code service(s) below after creating an account with us.</p>
                    </div>
                </div>
            </div>
        </section>
        <div class="row">
            <div class="col-md-12">
                <div class="exp-collap pull-right">
                    [ <a href="JavaScript:void(0);" onclick="expandall();">Expand All</a>
                      <small class="separator">|</small>
                      <a href="JavaScript:void(0);" onclick="collapseall();">Collapse All</a> ]
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12">
				<div id="tabcontent">
                    <div class="accordion">
                        <?
						$i = 0;
						$prevCatId = 0;
						$clsRow = 'clsGridRow1';
						foreach($rsPackages as $row )
						{
							$link = '';
							if($clsRow == 'clsGridRow')
								$clsRow = 'clsGridRow1';
							else
								$clsRow = 'clsGridRow';
							$category = '';
							if($row->SEOURLName != '')
								$link = 'unlock/service/'.$row->SEOURLName;
							else
								$link = "home/servicedtl?id=".$row->PackageId;
							if($prevCatId > 0)
							{
								if($row->CategoryId != $prevCatId)
								{
									echo '<tbody></table></div></div></div>'; ?>
                                    <br />
									<div class="accordion-group">
										<div class="accordion-heading" style="font-size:14px;">
											<a href="#g<?php echo $i; ?>"  data-toggle="collapse" class="accordion-toggle">
												<strong><?php echo stripslashes($row->Category); ?></strong>
												<i class="icon-chevron-down pull-right"></i>
											</a>
										</div>
										<div class="accordion-body collapse" id="g<?php echo $i; ?>">
											<div class="accordion-inner">              
                                                <table class="table table-striped" width="100%">
                                                    <tr>
                                                        <th width="50%">Service</th>
                                                        <th width="5%">&nbsp;</th>
                                                        <?php if($rsStngs->ShowPricesAtWeb) 
														{
															echo '<th width="15%">'.$this->lang->line('CDVAR4').'</th>'; 
															echo $strGroups;
														}
														?>
                                                        <th width="5%">&nbsp;</th>
                                                        <th width="20%" nowrap="nowrap"> <?php echo $this->lang->line('CDVAR5'); ?></th>
                                                        <?
                                                        if(isset($_SESSION['GSM_FSN_AdmId']))
                                                            echo '<th>&nbsp;</th>';
                                                        ?>
                                                    </tr>
								<?
								}
							}
							else
							{
								?>
								<div class="accordion-group">
									<div class="accordion-heading" style="font-size:14px;">
										<a href="#g<?php echo $i; ?>"  data-toggle="collapse" class="accordion-toggle">
											<strong><?php echo stripslashes($row->Category); ?></strong>
											<i class="icon-chevron-down pull-right"></i>
										</a>
									</div>
									<div class="accordion-body collapse" id="g<?php echo $i; ?>">
										<div class="accordion-inner">
                                            <table class="table table-striped" width="100%">
                                                <tr style="height:30px;">
                                                    <th width="50%">Service</th>
                                                    <th width="5%">&nbsp;</th>
													<?php if($rsStngs->ShowPricesAtWeb) 
                                                    {
                                                        echo '<th width="15%">'.$this->lang->line('CDVAR4').'</th>'; 
                                                        echo $strGroups;
                                                    }
                                                    ?>
                                                    <th width="5%">&nbsp;</th>
                                                    <th width="20%" nowrap="nowrap"> <?php echo $this->lang->line('CDVAR5'); ?></th>
                                                    <?
                                                    if(isset($_SESSION['GSM_FSN_AdmId']))
                                                        echo '<th>&nbsp;</th>';
                                                    ?>
                                                </tr>
							<?
							}
							$price = $row->PackagePrice * $this->session->userdata('CurrencyRate');
							$price = roundMe($price);
							$prevCatId = $row->CategoryId;
							?>
                                <tr class="<?php echo $clsRow; ?>" style="height:40px;">
                                    <td valign="middle" style="font-size:13px;padding-top:10px; padding-left:5px;"><a href="<?php echo base_url().$link; ?>" style="text-decoration:none;"><?php echo stripslashes($row->PackageTitle); ?></a></td>
                                    <td>&nbsp;</td>
                                    <?php if($rsStngs->ShowPricesAtWeb)
									{
										echo '<td valign="middle" style="padding-left:3px;font-size:13px;">'.$this->session->userdata('CurrencySymbol').' '.$price.'</td>';
										if(is_array($arrGroups) && sizeof($arrGroups) > 0)
										{
											foreach ($arrGroups as $key => $value)
											{
												if(isset($arrGPrices[$key][$row->PackageId]))
												{
													$groupPrice = roundMe($arrGPrices[$key][$row->PackageId] * $this->session->userdata('CurrencyRate'));
												}
												else
													$groupPrice = $price;
												echo '<td valign="middle" style="padding-left:3px;font-size:13px;">'.$this->session->userdata('CurrencySymbol').' '.$groupPrice.'</td>';
											}
										}
									}
									?>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:3px;font-size:13px;" valign="middle"><?php echo stripslashes($row->TimeTaken); ?></td>
                                    <?php if($this->session->userdata('GSM_FSN_AdmId')) { ?>
	                                    <td nowrap="nowrap" style="padding-left:3px;padding-right:5px;font-size:13px;" valign="middle"><a href="<?php echo base_url().'admin/services/package?fs=1&id='.$row->PackageId; ?>" style="text-decoration:none;" target="_blank">Edit Service</a></td>
                                    <?php } ?>
                                </tr>
							<?php
							$i++;
						}
						echo '</tbody></table></div></div></div>';
						?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
function expandall(){
   $('.accordion-body').addClass('in').attr('style','');
}

function collapseall(){
	$('.accordion-body').removeClass('in');            
}	
</script>