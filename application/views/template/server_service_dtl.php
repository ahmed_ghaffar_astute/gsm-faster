
<div role="main" class="main">

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php echo $packTitle; ?></h1>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="lead">
                    <?php if($packImg != '') { ?>
                        <p align="center"><img align="middle" src="<?php echo "uplds1/".$packImg; ?>" /></p><br /><br />
                    <? } ?>
                        <p><b>Delivery Time:</b> <?php echo $delTime; ?><br /></p>
                        <?php if($rsStngs->ShowPricesAtWeb) echo '<p><b>Price:</b> '.$currSymbol.' '.$packPrice.'<br /></p>'; ?>
                        <?php echo $packDesc; ?>
                </p>
            </div>
        </div>
    </div>
</div>
