<div role="main" class="main">
    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>FAQ</h1>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <h2>Frequently Asked <strong>Questions</strong></h2>
        <div class="row">
            <div class="col-md-12">
				<div class="panel-group panel-group-quaternary" id="accordion">
					<?php
                    $z = 0;
                    foreach($rsFaqs as $row)
                    {
                    ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1<? echo $z;?>">
                                        <?php echo stripslashes($row->Question); ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse1<? echo $z;?>" class="accordion-body collapse <?php if($z == '0') echo 'in'; ?>">
                                <div class="panel-body">
                                    <p align="justify"><?php echo stripslashes($row->Answer); ?></p>
                                </div>
                            </div>
                        </div>
                    <?php
                        $z++;
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
