<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">	

        <title><?php echo $this->config->item('app_name'); ?></title>
        <meta name="keywords" content="<?php echo $META_KW_CONTENTS != '' ? $META_KW_CONTENTS : $META_KW_CNTNTS; ?>" />
        <meta name="description" content="<?php echo $META_DESC_CONTENTS != '' ? $META_DESC_CONTENTS : $META_DESC_CNTNTS; ?>" />

        <?php if($FAV_ICO != '') { ?>
        <link rel="shortcut icon" href="<?php echo $FAV_ICO; ?>" type="image/x-icon" />
        <?php } ?>
        
        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>template1/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>template1/vendor/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>template1/vendor/animate/animate.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>template1/vendor/simple-line-icons/css/simple-line-icons.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>template1/vendor/owl.carousel/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>template1/vendor/owl.carousel/assets/owl.theme.default.min.css">
       
        <!-- Theme CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>template1/css/theme.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>template1/css/theme-elements.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>template1/css/theme-shop.css">

        <!-- Current Page CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>template1/vendor/rs-plugin/css/settings.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>template1/vendor/rs-plugin/css/layers.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>template1/vendor/rs-plugin/css/navigation.css">

        <!-- Skin CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>template1/css/skins/skin-corporate-6.css?v=<?=rand_string(10);?>">

        <!-- Theme Custom CSS -->

		<script src="<?php echo base_url('assets/'); ?>template1/vendor/jquery/jquery.min.js"></script>
		
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>css_pr/jquery.fancybox.css?v=2.1.5" media="screen" />
        
        <!-- Head Libs -->
        <script src="<?php echo base_url('assets/'); ?>template1/vendor/modernizr/modernizr.min.js"></script>
        
        <?php if($LIVE_CHAT_CODE != '' && $AT_WEBSITE == '1') echo $LIVE_CHAT_CODE; ?>
        <?php if($GANALYTICS_CODE != '') echo $GANALYTICS_CODE; ?>
		<?php if ($MARQUEE_CODE != '' && $MARQUEE_POS == '1') { ?>
            <style>
                html.sticky-header-active #header .header-body{
                    margin: 0;
                    padding: 0;
                }
                .marqueewrap{
                    display: block;
                    margin: 0;
                    padding: 0;
                    overflow: hidden;
                }
                .marqueewrap marquee{
                    display: block;
                }
                #header .header-top.header-top-style-3{
                    margin-top: 0;
                }
            </style>
        <?php } ?>
        
		<script type="text/javascript">
            $(document).ready(function() {
                $('.fancybox').fancybox();
            });
        </script>
        <style type="text/css">
            .fancybox-custom .fancybox-skin {
                box-shadow: 0 0 50px #222;
            }
        </style>
    </head>
    <body>
        <div class="body">
            <header id="header">
                <div class="header-body">
			        <?php if($MARQUEE_CODE != '' && $MARQUEE_POS == '1') echo $MARQUEE_CODE; ?>
                    <div class="header-top header-top-secondary header-top-style-3">
                        <div class="container">
                            <p class="text-color-light" style="padding-top:7px;" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a href="'.base_url('admin/').'settings/settings?frmId=57&fTypeId=13" target="blank">Edit Text</a>', $EDIT_TXT); ?>>
                                <span class="ml-xs"><i class="fa fa-phone"></i> <?php echo $rsStngs->Phone;?></span><span> | <a href="mailto:<?php echo $rsStngs->ContactUsEmail; ?>"><?php echo  $rsStngs->ContactUsEmail;?></a></span>
                            </p>
                        </div>
                    </div>
                    <div class="header-container container">
                        <div class="header-row">
                            <div class="header-column">
                                <div class="header-logo" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a href="'.base_url('admin/cms/').'logos?id=4&frmId=41&fTypeId=8" target="blank">Edit Logo</a>', $EDIT_TXT); ?>>
                                    <a href="<?php echo base_url('home');?>">
                                        <img src="<?php echo $LOGO_PATH ; ?>" />
                                    </a>
                                </div>
                            </div>
                            <div class="header-column">
                                <div class="header-row">
                                    <div class="header-nav">
                                        <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
                                            <i class="fa fa-bars"></i>
                                        </button>
                                        <?php 
                                            $totalCartItms = 0;
                                            if($this->session->userdata("CART_PRODUCTS"))
                                                $totalCartItms = count($this->session->userdata["CART_PRODUCTS"]);
										?>
                                        <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
                                            <nav>
                                                <ul class="nav nav-pills" id="mainNav">
                                                    <li>
                                                        <a href="<?php echo base_url('home');?>">Home</a>
                                                    </li>
                                                    <?php 
                                                        if($USERVICES != ',,') { 
                                                            $showIMEIAtWeb = 0;
                                                            $showFlAtWeb = 0;
                                                            $showSrvrAtWeb = 0;
                                                            $ARRSRV = explode(',', $USERVICES);
                                                            if(isset($ARRSRV[0]))
                                                                $showIMEIAtWeb = $ARRSRV[0];
                                                            if(isset($ARRSRV[1]))
                                                                $showFlAtWeb = $ARRSRV[1];
                                                            if(isset($ARRSRV[2]))
                                                                $showSrvrAtWeb = $ARRSRV[2];
                                                    ?>
                                                    <li class="dropdown" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a href="'.base_url('admin/cms/').'page?id=12&frmId=44&fTypeId=8" target="blank">Edit Link</a>', $EDIT_TXT); ?>>
                                                        <a class="dropdown-toggle" href="#">
                                                            <?php echo $USERVICESTITLE;?>
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <?php if($showIMEIAtWeb != '') { ?>
                                                            <li>
                                                                <a href="<?php echo base_url('home/imei_services');?>">
                                                                    <?php echo $showIMEIAtWeb == '1' ? 'IMEI Services' : $showIMEIAtWeb;?>
                                                                </a>
                                                            </li>
                                                            <?php } ?>
                                                            <?php if($showFlAtWeb != '') { ?>
                                                            <li>
                                                                <a href="<?php echo base_url('home/file_services');?>">
                                                                    <?php echo $showFlAtWeb == '1' ? 'File Services' : $showFlAtWeb;?>
                                                                </a>
                                                            </li>
                                                            <?php } ?>
                                                            <?php if($showSrvrAtWeb != '') { ?>
                                                            <li>
                                                                <a href="<?php echo base_url('home/server_services');?>">
                                                                    <?php echo $showSrvrAtWeb == '1' ? 'Server Services' : $showSrvrAtWeb;?>
                                                                </a>
                                                            </li>
                                                            <?php } ?>
                                                        </ul>
                                                    </li>
                                                    <?php }
                                                    echo $strHeaderLnks; ?>
                                                    <?php 
                                                    if($this->session->userdata('GSM_FUS_UserId') && $this->session->userdata('GSM_FUS_UserId') > 0)
                                                    { ?>
                                                        <li><a href="<?php echo base_url('dashboard');?>"> Dashboard</a></li>
                                                        <li><a href="<?php echo base_url('dashboard/logout');?>">Logout</a></li>
                                                    <?
                                                    }
                                                    else
                                                    { ?>
                                                        <li <?php if ($this->router->fetch_method() == 'register') echo 'class="active"'; ?>><a href="<?php echo base_url('register');?>"><i class="fa fa-user" aria-hidden="true"></i> Register</a></li>
                                                        <li><a href="<?php echo base_url('login');?>"><i class="fa fa-lock"></i> Login</a></li>
                                                    <?php } ?>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <?php if(trim($strSubMenuLnks) != '') { ?>
                <aside class="nav-secondary" style="height:50px;" id="navSecondary" data-plugin-sticky data-plugin-options="{'minWidth': 991}">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <ul class="nav nav-pills justify-content-left">
	                                <?php echo $strSubMenuLnks;?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </aside>
			<?php } ?>
