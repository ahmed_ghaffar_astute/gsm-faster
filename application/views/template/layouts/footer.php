        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="footer-ribbon">
                        <span>Get in Touch</span>
                    </div>
                    <div class="col-md-3" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a style="color:#000;" href="'.base_url().'admin/page?id=25&frmId=44&fTypeId=8" target="blank">Edit Text</a>', $EDIT_TXT); ?>>
                        <div class="newsletter">
                            <h4><?php echo $title25; ?></h4>
                            <?php echo $text25; ?>
                            <?php if($FLAG_COUNTER_CODE != '') { ?>
                                <div <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a style="color:#000;" href="'.base_url().'flagcounter?frmId=74&fTypeId=14" target="blank">Edit Flag Counter</a>', $EDIT_TXT); ?>>
                                    <?php echo $FLAG_COUNTER_CODE; ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h4>Main Pages</h4>
                        <ul class="features">
                            <?php echo $strFtrLnks; ?>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <div class="contact-details" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a style="color:#000;" href="'.base_url().'settings/settings?frmId=57&fTypeId=13" target="blank">Edit Details</a>', $EDIT_TXT); ?>>
                            <h4>Contact Us</h4>
                            <ul class="contact">
                                <li><p><i class="fa fa-map-marker"></i> <strong>Address:</strong> <?php echo $rsStngs->Address; ?></p></li>
                                <li><p><i class="fa fa-volume-control-phone"></i> <strong>Phone:</strong> <?php echo $rsStngs->Phone; ?></p></li>
                                <li><p><i class="fa fa-envelope"></i> <strong>E-Mail:</strong> <a href="mailto:<?php echo $rsStngs->ContactUsEmail; ?>"><?php echo  $rsStngs->ContactUsEmail; ?></a></p></li>
                            </ul>
                            <ul class="social-icons" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a style="color:#000;" href="'.base_url().'cms/socialmedialinks?frmId=43&fTypeId=8" target="blank">Edit Social Media Links</a>', $EDIT_TXT); ?>>
                                <li><i class="fa fa-group"></i> <strong>&nbsp;&nbsp;Follow Us:</strong>&nbsp;&nbsp;&nbsp;&nbsp;</li>
                                <?php echo $strSocialMedia; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="newsletter">
                            <h4>Be the First to Know</h4>
                            <p class="newsletter-info">Get all the latest information on Products,<br> Sales and Offers. Sign up for newsletter today.</p>
                            <div class="alert alert-success" style="display:none;" id="newsletterSuccess"></div>
                            <div class="alert alert-danger" style="display:none;" id="newsletterError"></div>
                            <p>Enter your e-mail Address:</p>
                            <img style="display:none;" id="fLdrNL" src="<?php echo base_url();?>assets/images/loading.gif" border="0" alt="Please wait..." />
                            <?php echo form_open('' , 'id="fNL"');?>
                                <div class="input-group">
                                    <input class="form-control" maxlength="50" placeholder="Email Address" name="txtNLEmail" id="txtNLEmail" type="text" required="required" />
                                    <span class="input-group-btn">
                                        <input type="hidden" name="nlType" id="nlType" value="0" />
                                        <button class="btn btn-primary" type="button" id="btnFNL">Submit</button>
                                    </span>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a style="color:#000;" href="'.base_url().'setting/lookuplist.php?iFrm=4&frmId=47&fTypeId=8" target="blank">Edit Copyright Text</a>', $EDIT_TXT); ?>>
                            <p><?php echo stripslashes($rsStngs->Copyrights); ?></p>
                        </div>

                        <div class="col-md-7">
                            <nav id="sub-menu">
                                <ul>
                                    <?php if($BRANDING_NAME != '') { ?>
                                    <li>Powered By <a href="<?php echo $BRANDING_URL; ?>" target="_blank"><?php echo $BRANDING_NAME; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- Vendor -->
        <script type="text/javascript" src="<?php echo base_url('assets/'); ?>js/jquery.fancybox.js?v=2.1.5"></script>
        <script src="<?php echo base_url('assets/'); ?>template1/vendor/jquery.appear/jquery.appear.min.js"></script>
        <script src="<?php echo base_url('assets/'); ?>template1/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url('assets/'); ?>template1/vendor/isotope/jquery.isotope.min.js"></script>
        <script src="<?php echo base_url('assets/'); ?>template1/vendor/owl.carousel/owl.carousel.min.js"></script>

        <!-- Theme Base, Components and Settings -->
        <script src="<?php echo base_url('assets/'); ?>template1/js/theme.js"></script>

        <!-- Current Page Vendor and Views -->
        <script src="<?php echo base_url('assets/'); ?>template1/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
        <script src="<?php echo base_url('assets/'); ?>template1/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
        <!-- Theme Custom -->

        <!-- Theme Initialization Files -->
        <script src="<?php echo base_url('assets/');?>js/functions.js"></script>
        <script src="<?php echo base_url('assets/');?>js/newsletter.js"></script>
        <script>
            var originalLeave = $.fn.popover.Constructor.prototype.leave;
            $.fn.popover.Constructor.prototype.leave = function (obj) {
                var self = obj instanceof this.constructor ?
                        obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
                var container, timeout;

                originalLeave.call(this, obj);

                if (obj.currentTarget) {
                    container = $(obj.currentTarget).siblings('.popover')
                    timeout = self.timeout;
                    container.one('mouseenter', function () {
                        //We entered the actual popover – call off the dogs
                        clearTimeout(timeout);
                        //Let's monitor popover content instead
                        container.one('mouseleave', function () {
                            $.fn.popover.Constructor.prototype.leave.call(self, self);
                        });
                    })
                }
            };
            $('body').popover({selector: '[data-popover]', trigger: 'click hover', placement: 'auto', delay: {show: 50, hide: 400}});
        </script>
        <script src="<?php echo base_url('assets/'); ?>template1/js/theme.init.js"></script>
    </body>
</html>
