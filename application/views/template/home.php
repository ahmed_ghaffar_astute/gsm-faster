<div role="main" class="main shop">
    <div class="slider-with-overlay" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a href="'.base_url().'admin/cms/banners?frmId=42&fTypeId=8" target="blank">Edit Banners</a>', $EDIT_TXT); ?>>
		<div class="slider-container rev_slider_wrapper" style="height: 560px;">
            <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options='{"delay": 9000, "gridwidth": 1170, "gridheight": 560}'>
                <ul>
                    <?php
                    foreach($rsBanners as $row)
                    {
                    ?>
                        <li data-transition="fade">
							<?php if(trim($row->URL) != '')	echo '<a border="0" style="border-style: none; border:none;" href="'.trim($row->URL).'"'; ?>
                                <img src="<?php echo base_url()."uplds1/".$row->BannerPath; ?>"
                                    alt=""
                                    data-bgposition="center center"
                                    data-bgfit="cover"
                                    data-bgrepeat="no-repeat"
                                    class="rev-slidebg">
							<?php if(trim($row->URL) != '')	echo '</a>'; ?>

                            <div class="tp-caption featured-label"
                                data-x="center"
                                data-y="center" data-voffset="-45"
                                data-start="500"
                                style="z-index: 5"
                                data-transform_in="y:[100%];s:500;"
                                data-transform_out="opacity:0;s:500;"><?php echo stripslashes($row->BannerTitle); ?></div>

                            <div class="tp-caption bottom-label"
                                data-x="center"
                                data-y="center" data-voffset="5"
                                data-start="1000"
                                data-transform_idle="o:1;"
                                data-transform_in="y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;s:600;e:Power4.easeInOut;"
                                data-transform_out="opacity:0;s:500;"
                                data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                data-splitin="chars"
                                data-splitout="none"
                                data-responsive_offset="on"
                                style="font-size: 23px; line-height: 30px; z-index: 5"
                                data-elementdelay="0.05"><?php echo stripslashes(strip_tags($row->Detail)); ?></div>

                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div class="home-intro" id="home-intro" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a href="'.base_url().'admin/cms/page?id=17&frmId=44&fTypeId=8" target="blank">Edit Text</a>', $EDIT_TXT); ?>>
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <?php echo $text7; ?>
                    </div>
                    <div class="col-md-4">
                        <div class="get-started">
                            <a href="<?php echo base_url(); ?>home/register" class="btn btn-lg btn-primary">Sign Up Now!</a>
                            <div class="learn-more">&nbsp;&nbsp;<a href="#">&nbsp;</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<?php if($img19 != '') { ?>
        <div class="container" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a href="'.base_url().'admin/cms/page?id=19&frmId=44&fTypeId=8" target="blank">Edit Image</a>', $EDIT_TXT); ?>>
            <div class="row center">
                <div class="col-md-12">
                    <div style="margin: 45px 0px -30px; overflow: hidden;">
                        <img src="<?php echo base_url() ."uplds1/$img19"; ?>" class="img-responsive appear-animation" data-appear-animation="fadeInUp">
                    </div>
                </div>
            </div>
        </div>
	<?php } ?>
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="feature-box feature-box-style-2">
                                <div class="feature-box-icon">
                                    <i class="fa fa-group"></i>
                                </div>
                                <div class="feature-box-info" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a href="'.base_url().'admin/cms/page?id=6&frmId=44&fTypeId=8" target="blank">Edit Text</a>', $EDIT_TXT); ?>>
                                    <h4 class="mb-none"><?php echo $title1; ?></h4>
                                    <p class="tall" align="justify"><?php echo strip_tags($text1); ?></p>
                                </div>
                            </div>
                            <div class="feature-box feature-box-style-2">
                                <div class="feature-box-icon">
                                    <i class="fa fa-file"></i>
                                </div>
                                <div class="feature-box-info" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a href="'.base_url().'admin/cms/page?id=7&frmId=44&fTypeId=8" target="blank">Edit Text</a>', $EDIT_TXT); ?>>
                                    <h4 class="mb-none"><?php echo $title2; ?></h4>
                                    <p class="tall" align="justify"><?php echo strip_tags($text2); ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="feature-box feature-box-style-2">
                                <div class="feature-box-icon">
                                    <i class="fa fa-film"></i>
                                </div>
                                <div class="feature-box-info" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a href="'.base_url().'admin/cms/page?id=8&frmId=44&fTypeId=8" target="blank">Edit Text</a>', $EDIT_TXT); ?>>
                                    <h4 class="mb-none"><?php echo $title3; ?></h4>
                                    <p class="tall" align="justify"><?php echo strip_tags($text3); ?></p>
                                </div>
                            </div>
                            <div class="feature-box feature-box-style-2">
                                <div class="feature-box-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="feature-box-info" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a href="'.base_url().'admin/cms/page?id=11&frmId=44&fTypeId=8" target="blank">Edit Text</a>', $EDIT_TXT); ?>>
                                    <h4 class="mb-none"><?php echo $title11; ?> </h4>
                                    <p class="tall" align="justify"><?php echo strip_tags($text11); ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="feature-box feature-box-style-2">
                                <div class="feature-box-icon">
                                    <i class="fa fa-bars"></i>
                                </div>
                                <div class="feature-box-info" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a href="'.base_url().'admin/cms/page?id=14&frmId=44&fTypeId=8" target="blank">Edit Text</a>', $EDIT_TXT); ?>>
                                    <h4 class="mb-none"><?php echo $title4; ?></h4>
                                    <p class="tall" align="justify"><?php echo strip_tags($text4); ?></p>
                                </div>
                            </div>
                            <div class="feature-box feature-box-style-2">
                                <div class="feature-box-icon">
                                    <i class="fa fa-desktop"></i>
                                </div>
                                <div class="feature-box-info" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a href="'.base_url().'admin/cms/page?id=16&frmId=44&fTypeId=8" target="blank">Edit Text</a>', $EDIT_TXT); ?>>
                                    <h4 class="mb-none"><?php echo $title16; ?></h4>
                                    <p class="tall" align="justify"><?php echo strip_tags($text16); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    if ($rsProds)
	{ ?>
        <div class="container">
            <div class="row">
                <div class="col-md-12 center">
                    <h2 class="mb-xl"><strong>Shop</strong> With <strong>Us</strong></h2>
                </div>
            </div>
            <div class="row">
                <div class="masonry-loader masonry-loader-showing">
                    <ul class="products product-thumb-info-list" data-plugin-masonry>
						<?
                        foreach($rsProds as $row)
                        {
							if($row->SEOURLName != '')
								$prodURL = 'shop/product/'.$row->SEOURLName;
							else
								$prodURL = "shop-product.php?id=".$row->ProductId;
                            $prodImg = base_url().'template1/img/products/product-1.jpg';
                            if($row->Picture != '')
                                $prodImg = base_url()."uplds1/".$row->Picture;
                            $productPrice = getProductPrice($row->ProductId, $row->ProductPrice, $row->PromoStartDate, $row->PromoEndDate, $row->PromoDiscount, $TODAY_DT_TM, 1, $objDBCD14); ?>
							<li class="col-md-3 col-sm-6 col-xs-12 product">
								<? if($row->IsNew == '1') { ?>
                                	<a href="<?php echo base_url().$prodURL;?>">
										<span class="onsale">New</span>
									</a>
								<? } ?>
								<span class="product-thumb-info">
									<a href="<?php echo base_url();?>addtocart?id=<? echo $row->ProductId; ?>" class="add-to-cart-product">
										<span><i class="fa fa-shopping-cart"></i> Add to Cart</span>
									</a>
									<a href="<?php echo base_url().$prodURL;?>">
										<span class="product-thumb-info-image">
											<span class="product-thumb-info-act">
												<span class="product-thumb-info-act-left"><em>View</em></span>
												<span class="product-thumb-info-act-right"><em><i class="fa fa-plus"></i> Details</em></span>
											</span>
											<img alt="<?php echo stripslashes($row->ProductName); ?>" class="img-responsive" src="<? echo $prodImg;?>">
										</span>
									</a>
									<span class="product-thumb-info-content">
										<a href="<?php echo base_url().$prodURL;?>">
											<h4><? echo stripslashes($row->ProductName); ?></h4>
											<span class="price">
												<!--<del><span class="amount">$325</span></del>-->
												<ins><span class="amount"><? echo $this->session->userdata('CurrencySymbol').$productPrice; ?></span></ins>
											</span>
										</a>
									</span>
								</span>
							</li>
                        <?
                        }
                        ?>
					</ul>
				</div>
			</div>
        </div>
	<?
	}
    $rsBrandLogos = fetch_brand_logos();
    if($rsBrandLogos )
    {
    ?>
        <div class="container">
            <div class="row center" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a href="'.$BASE_URL.$BACKEND_FOLDER.'page.php?id=18&frmId=44&fTypeId=8" target="blank">Edit Text</a>', $EDIT_TXT); ?>>
                <div class="col-md-12">
                    <h2 class="mb-none word-rotator-title mt-lg">
                        <?php echo $text18; ?>
                    </h2>
                </div>
            </div>
            <div class="row center mt-xl" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a href="'.$BASE_URL.$BACKEND_FOLDER.'brandlogos.php?frmId=44&fTypeId=8" target="blank">Edit Brand Logos</a>', $EDIT_TXT); ?>>
                <div class="owl-carousel owl-theme" data-plugin-options="{'items': 6, 'autoplay': true, 'autoplayTimeout': 3000}">
					<?php
                    foreach($rsBrandLogos as $row )
                    { ?>
                        <div>
                            <img class="img-responsive" src="<?php echo base_url()."uplds1/".$row->BrandImage; ?>" />
                        </div>
					<?php
					}?>
                </div>
            </div>
        </div>
	<?php
    }

    $rsReviews = fetch_reviews();
    if($rsReviews)
    {
    ?>
        <section class="parallax section section-text-light section-parallax section-center mt-none" data-plugin-parallax data-plugin-options='{"speed": 1.5}' data-image-src="<?php echo base_url(); ?>assets/template1/img/parallax-2.jpg">
            <div class="container" <?php if(trim($EDIT_TXT) != '') echo str_replace('ANCHORLINK', '<a href="'.base_url().'cms/reviews?frmId=40&fTypeId=8" target="blank">Manage Reviews</a>', $EDIT_TXT); ?>>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="owl-carousel owl-theme nav-bottom rounded-nav" data-plugin-options='{"items": 1, "loop": false}'>
							<?php
                            $rv = 0;
                            foreach($rsReviews as $row)
                            { ?>
                                <div>
                                    <div class="col-md-12">
                                        <div class="testimonial testimonial-style-2 testimonial-with-quotes mb-none">
                                            <div class="testimonial-author">
                                                <img src="<?php echo base_url(); ?>assets/template1/img/avatar1.png" class="img-responsive img-circle" alt="">
                                            </div>
                                            <blockquote>
                                                <p><?php echo stripslashes($row->Review); ?></p>
                                            </blockquote>
                                            <div class="testimonial-author">
                                                <p><strong><?php echo stripslashes($row->CustomerName); ?></strong><span><?php echo $row->ReviewDate;?></span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
	<?php } ?>
	<?php
    $rsNews = fetch_news();
    if($rsNews)
    {
    ?>
        <div class="container">
            <div class="row mt-xl">
                <div class="col-md-12 center">
                    <h2 class="mt-xl mb-xl">Latest <strong>Posts</strong></h2>
                </div>
            </div>
            <div class="row">
            	<?php
                foreach($rsNews as $row)
                {
                    if($row->SEOURLName != '')
                        $newsURL = 'unlock/news/'.$row->SEOURLName;
                    else
                        $newsURL = "home/news?id=".$row->NewsId;
                ?>
                    <div class="col-md-4">
                        <div class="recent-posts mt-xl">
                            <article class="post">
                                <div class="date">
                                    <span class="day"><?php echo $row->NewsDate;?></span>
                                    <span class="month"><?php echo substr($row->NewsMon, 0, 3); ?></span>
                                </div>
                                <h4><a href="<?php echo base_url().$newsURL; ?>"><?php echo stripslashes($row->NewsTitle);?></a></h4>
                                <p><?php echo stripslashes($row->ShortDescription); ?></p>
                                <a href="<?php echo base_url().$newsURL; ?>" class="btn btn-borders btn-default mt-md mb-xl">Read More</a>
                            </article>
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
	<?php } ?>

</div>
